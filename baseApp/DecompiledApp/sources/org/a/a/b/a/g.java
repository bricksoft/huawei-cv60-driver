package org.a.a.b.a;

import org.a.a.a.a.d;

/* access modifiers changed from: package-private */
public class g implements d {

    /* renamed from: a  reason: collision with root package name */
    Class f1267a;
    String b;
    int c;

    g(Class cls, String str, int i) {
        this.f1267a = cls;
        this.b = str;
        this.c = i;
    }

    public String a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public String toString() {
        return new StringBuffer().append(a()).append(":").append(b()).toString();
    }
}
