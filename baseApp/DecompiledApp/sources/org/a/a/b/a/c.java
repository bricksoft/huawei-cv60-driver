package org.a.a.b.a;

import org.a.a.a.a;
import org.a.a.a.d;

/* access modifiers changed from: package-private */
public class c implements org.a.a.a.c {

    /* renamed from: a  reason: collision with root package name */
    Object f1263a;
    Object b;
    Object[] c;
    a.AbstractC0099a d;

    /* access modifiers changed from: package-private */
    public static class a implements a.AbstractC0099a {

        /* renamed from: a  reason: collision with root package name */
        String f1264a;
        d b;
        org.a.a.a.a.d c;
        private int d;

        public a(int i, String str, d dVar, org.a.a.a.a.d dVar2) {
            this.f1264a = str;
            this.b = dVar;
            this.c = dVar2;
            this.d = i;
        }

        public String a() {
            return this.f1264a;
        }

        public d b() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public String a(h hVar) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(hVar.a(a()));
            stringBuffer.append("(");
            stringBuffer.append(((f) b()).b(hVar));
            stringBuffer.append(")");
            return stringBuffer.toString();
        }

        @Override // org.a.a.a.a.AbstractC0099a
        public final String toString() {
            return a(h.k);
        }
    }

    public c(a.AbstractC0099a aVar, Object obj, Object obj2, Object[] objArr) {
        this.d = aVar;
        this.f1263a = obj;
        this.b = obj2;
        this.c = objArr;
    }

    @Override // org.a.a.a.a
    public Object a() {
        return this.b;
    }

    public final String toString() {
        return this.d.toString();
    }
}
