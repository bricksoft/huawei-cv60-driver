package org.a.a.b.a;

import java.lang.ref.SoftReference;
import java.util.StringTokenizer;
import org.a.a.a.d;

/* access modifiers changed from: package-private */
public abstract class f implements d {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1265a = true;
    static String[] k = new String[0];
    static Class[] l = new Class[0];
    private String b;
    int e = -1;
    String f;
    String g;
    Class h;
    a i;
    ClassLoader j = null;

    /* access modifiers changed from: private */
    public interface a {
        String a(int i);

        void a(int i, String str);
    }

    /* access modifiers changed from: protected */
    public abstract String a(h hVar);

    f(int i2, String str, Class cls) {
        this.e = i2;
        this.f = str;
        this.h = cls;
    }

    /* access modifiers changed from: package-private */
    public String b(h hVar) {
        String str = null;
        if (f1265a) {
            if (this.i == null) {
                try {
                    this.i = new b();
                } catch (Throwable th) {
                    f1265a = false;
                }
            } else {
                str = this.i.a(hVar.i);
            }
        }
        if (str == null) {
            str = a(hVar);
        }
        if (f1265a) {
            this.i.a(hVar.i, str);
        }
        return str;
    }

    public final String toString() {
        return b(h.k);
    }

    public int d() {
        if (this.e == -1) {
            this.e = b(0);
        }
        return this.e;
    }

    public String e() {
        if (this.f == null) {
            this.f = a(1);
        }
        return this.f;
    }

    public Class f() {
        if (this.h == null) {
            this.h = c(2);
        }
        return this.h;
    }

    public String g() {
        if (this.g == null) {
            this.g = f().getName();
        }
        return this.g;
    }

    private ClassLoader a() {
        if (this.j == null) {
            this.j = getClass().getClassLoader();
        }
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public String a(int i2) {
        int i3 = 0;
        int indexOf = this.b.indexOf(45);
        while (true) {
            int i4 = i2 - 1;
            if (i2 <= 0) {
                break;
            }
            i3 = indexOf + 1;
            indexOf = this.b.indexOf(45, i3);
            i2 = i4;
        }
        if (indexOf == -1) {
            indexOf = this.b.length();
        }
        return this.b.substring(i3, indexOf);
    }

    /* access modifiers changed from: package-private */
    public int b(int i2) {
        return Integer.parseInt(a(i2), 16);
    }

    /* access modifiers changed from: package-private */
    public Class c(int i2) {
        return b.a(a(i2), a());
    }

    /* access modifiers changed from: package-private */
    public Class[] d(int i2) {
        StringTokenizer stringTokenizer = new StringTokenizer(a(i2), ":");
        int countTokens = stringTokenizer.countTokens();
        Class[] clsArr = new Class[countTokens];
        for (int i3 = 0; i3 < countTokens; i3++) {
            clsArr[i3] = b.a(stringTokenizer.nextToken(), a());
        }
        return clsArr;
    }

    /* access modifiers changed from: private */
    public static final class b implements a {

        /* renamed from: a  reason: collision with root package name */
        private SoftReference f1266a;

        public b() {
            b();
        }

        @Override // org.a.a.b.a.f.a
        public String a(int i) {
            String[] a2 = a();
            if (a2 == null) {
                return null;
            }
            return a2[i];
        }

        @Override // org.a.a.b.a.f.a
        public void a(int i, String str) {
            String[] a2 = a();
            if (a2 == null) {
                a2 = b();
            }
            a2[i] = str;
        }

        private String[] a() {
            return (String[]) this.f1266a.get();
        }

        private String[] b() {
            String[] strArr = new String[3];
            this.f1266a = new SoftReference(strArr);
            return strArr;
        }
    }
}
