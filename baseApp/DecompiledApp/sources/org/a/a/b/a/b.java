package org.a.a.b.a;

import java.util.Hashtable;
import java.util.StringTokenizer;
import org.a.a.a.a;
import org.a.a.a.d;
import org.a.a.b.a.c;

public final class b {
    static Hashtable e = new Hashtable();
    static Class f;
    private static Object[] g = new Object[0];

    /* renamed from: a  reason: collision with root package name */
    Class f1262a;
    ClassLoader b;
    String c;
    int d = 0;

    static {
        e.put("void", Void.TYPE);
        e.put("boolean", Boolean.TYPE);
        e.put("byte", Byte.TYPE);
        e.put("char", Character.TYPE);
        e.put("short", Short.TYPE);
        e.put("int", Integer.TYPE);
        e.put("long", Long.TYPE);
        e.put("float", Float.TYPE);
        e.put("double", Double.TYPE);
    }

    static Class a(String str, ClassLoader classLoader) {
        if (str.equals("*")) {
            return null;
        }
        Class cls = (Class) e.get(str);
        if (cls != null) {
            return cls;
        }
        if (classLoader != null) {
            return Class.forName(str, false, classLoader);
        }
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e2) {
            if (f != null) {
                return f;
            }
            Class a2 = a("java.lang.ClassNotFoundException");
            f = a2;
            return a2;
        }
    }

    static Class a(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e2) {
            throw new NoClassDefFoundError(e2.getMessage());
        }
    }

    public b(String str, Class cls) {
        this.c = str;
        this.f1262a = cls;
        this.b = cls.getClassLoader();
    }

    public a.AbstractC0099a a(String str, d dVar, int i) {
        int i2 = this.d;
        this.d = i2 + 1;
        return new c.a(i2, str, dVar, a(i, -1));
    }

    public static a a(a.AbstractC0099a aVar, Object obj, Object obj2) {
        return new c(aVar, obj, obj2, g);
    }

    public static a a(a.AbstractC0099a aVar, Object obj, Object obj2, Object obj3) {
        return new c(aVar, obj, obj2, new Object[]{obj3});
    }

    public org.a.a.a.a.c a(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        int parseInt = Integer.parseInt(str, 16);
        Class a2 = a(str3, this.b);
        StringTokenizer stringTokenizer = new StringTokenizer(str4, ":");
        int countTokens = stringTokenizer.countTokens();
        Class[] clsArr = new Class[countTokens];
        for (int i = 0; i < countTokens; i++) {
            clsArr[i] = a(stringTokenizer.nextToken(), this.b);
        }
        StringTokenizer stringTokenizer2 = new StringTokenizer(str5, ":");
        int countTokens2 = stringTokenizer2.countTokens();
        String[] strArr = new String[countTokens2];
        for (int i2 = 0; i2 < countTokens2; i2++) {
            strArr[i2] = stringTokenizer2.nextToken();
        }
        StringTokenizer stringTokenizer3 = new StringTokenizer(str6, ":");
        int countTokens3 = stringTokenizer3.countTokens();
        Class[] clsArr2 = new Class[countTokens3];
        for (int i3 = 0; i3 < countTokens3; i3++) {
            clsArr2[i3] = a(stringTokenizer3.nextToken(), this.b);
        }
        return new e(parseInt, str2, a2, clsArr, strArr, clsArr2, a(str7, this.b));
    }

    public org.a.a.a.a.d a(int i, int i2) {
        return new g(this.f1262a, this.c, i);
    }
}
