package org.a.a.b.a;

import java.lang.reflect.Modifier;

/* access modifiers changed from: package-private */
public class h {
    static h j = new h();
    static h k = new h();
    static h l = new h();

    /* renamed from: a  reason: collision with root package name */
    boolean f1268a = true;
    boolean b = true;
    boolean c = false;
    boolean d = false;
    boolean e = false;
    boolean f = true;
    boolean g = true;
    boolean h = true;
    int i;

    h() {
    }

    static {
        j.f1268a = true;
        j.b = false;
        j.c = false;
        j.d = false;
        j.e = true;
        j.f = false;
        j.g = false;
        j.i = 0;
        k.f1268a = true;
        k.b = true;
        k.c = false;
        k.d = false;
        k.e = false;
        j.i = 1;
        l.f1268a = false;
        l.b = true;
        l.c = false;
        l.d = true;
        l.e = false;
        l.h = false;
        l.i = 2;
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        int lastIndexOf = str.lastIndexOf(45);
        return lastIndexOf == -1 ? str : str.substring(lastIndexOf + 1);
    }

    /* access modifiers changed from: package-private */
    public String a(int i2) {
        if (!this.d) {
            return "";
        }
        String modifier = Modifier.toString(i2);
        if (modifier.length() == 0) {
            return "";
        }
        return new StringBuffer().append(modifier).append(" ").toString();
    }

    /* access modifiers changed from: package-private */
    public String b(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf == -1 ? str : str.substring(lastIndexOf + 1);
    }

    /* access modifiers changed from: package-private */
    public String a(Class cls, String str, boolean z) {
        if (cls == null) {
            return "ANONYMOUS";
        }
        if (cls.isArray()) {
            Class<?> componentType = cls.getComponentType();
            return new StringBuffer().append(a(componentType, componentType.getName(), z)).append("[]").toString();
        } else if (z) {
            return b(str).replace('$', '.');
        } else {
            return str.replace('$', '.');
        }
    }

    public String a(Class cls) {
        return a(cls, cls.getName(), this.f1268a);
    }

    public String a(Class cls, String str) {
        return a(cls, str, this.e);
    }

    public void a(StringBuffer stringBuffer, Class[] clsArr) {
        for (int i2 = 0; i2 < clsArr.length; i2++) {
            if (i2 > 0) {
                stringBuffer.append(", ");
            }
            stringBuffer.append(a(clsArr[i2]));
        }
    }

    public void b(StringBuffer stringBuffer, Class[] clsArr) {
        if (clsArr != null) {
            if (this.b) {
                stringBuffer.append("(");
                a(stringBuffer, clsArr);
                stringBuffer.append(")");
            } else if (clsArr.length == 0) {
                stringBuffer.append("()");
            } else {
                stringBuffer.append("(..)");
            }
        }
    }

    public void c(StringBuffer stringBuffer, Class[] clsArr) {
        if (this.c && clsArr != null && clsArr.length != 0) {
            stringBuffer.append(" throws ");
            a(stringBuffer, clsArr);
        }
    }
}
