package org.a.a.b.a;

abstract class a extends d implements org.a.a.a.a.a {

    /* renamed from: a  reason: collision with root package name */
    Class[] f1261a;
    String[] b;
    Class[] c;

    a(int i, String str, Class cls, Class[] clsArr, String[] strArr, Class[] clsArr2) {
        super(i, str, cls);
        this.f1261a = clsArr;
        this.b = strArr;
        this.c = clsArr2;
    }

    public Class[] a() {
        if (this.f1261a == null) {
            this.f1261a = d(3);
        }
        return this.f1261a;
    }

    public Class[] b() {
        if (this.c == null) {
            this.c = d(5);
        }
        return this.c;
    }
}
