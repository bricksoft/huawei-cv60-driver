package org.a.a.a;

public class b extends RuntimeException {

    /* renamed from: a  reason: collision with root package name */
    Throwable f1260a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(String str, Throwable th) {
        super(th != null ? new StringBuffer().append("Exception while initializing ").append(str).append(": ").append(th).toString() : str);
        this.f1260a = th;
    }

    public b() {
    }

    public Throwable getCause() {
        return this.f1260a;
    }
}
