package GeneralFunction;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.exoplayer.ExoPlayer;
import com.huawei.cvIntl60.R;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final int f60a;
    private Toast b;
    private Toast c;
    private Handler d;
    private int e;
    private ImageView f;
    private ImageView g;
    private TextView h;
    private LinearLayout i;
    private LinearLayout j;
    private TextView k;
    private Bitmap l;
    private Runnable m;

    public a() {
        this.f60a = 0;
        this.d = new Handler();
        this.e = -1;
        this.l = null;
        this.m = new Runnable() {
            /* class GeneralFunction.a.AnonymousClass1 */

            public void run() {
                a.this.a();
            }
        };
    }

    public a(Context context, String str, int i2, boolean z) {
        this.f60a = 0;
        this.d = new Handler();
        this.e = -1;
        this.l = null;
        this.m = new Runnable() {
            /* class GeneralFunction.a.AnonymousClass1 */

            public void run() {
                a.this.a();
            }
        };
        this.e = 2;
        if (this.c == null) {
            View inflate = LayoutInflater.from(context).inflate(R.layout.style_long_toast_layout, (ViewGroup) null);
            this.k = (TextView) inflate.findViewById(R.id.tvLongToastText);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            this.k.setTextSize((float) new c(15).d((int) (((double) 15) * 0.9d)).f((int) (((double) 15) * 0.8d)).g((int) (((double) 15) * 0.9d)).h((int) (((double) 15) * 0.9d)).j((int) (((double) 15) * 0.9d)).m((int) (((double) 15) * 0.9d)).o((int) (((double) 15) * 0.8d)).l((int) (((double) 15) * 0.8d)).n((int) (((double) 15) * 0.9d)).i((int) (((double) 15) * 0.9d)).a());
            this.k.setTextColor(Color.argb(170, 255, 255, 255));
            inflate.setLayoutParams(layoutParams);
            this.c = new Toast(context);
            this.c.setView(inflate);
        }
        this.k.setText(" " + str + " ");
        if (z) {
            this.c.setGravity(17, 0, 300);
        }
        this.c.setDuration(0);
        this.c.show();
        if (a((long) i2)) {
            b((long) i2);
        }
    }

    public a(Context context, String str, int i2) {
        this.f60a = 0;
        this.d = new Handler();
        this.e = -1;
        this.l = null;
        this.m = new Runnable() {
            /* class GeneralFunction.a.AnonymousClass1 */

            public void run() {
                a.this.a();
            }
        };
        this.e = 0;
        if (this.b == null) {
            View inflate = LayoutInflater.from(context).inflate(R.layout.style_toast_layout, (ViewGroup) null);
            this.i = (LinearLayout) inflate.findViewById(R.id.LL_ThumbnailLayout);
            this.j = (LinearLayout) inflate.findViewById(R.id.LL_ToastIconLayout);
            this.f = (ImageView) inflate.findViewById(R.id.ivThumbnailView);
            this.h = (TextView) inflate.findViewById(R.id.tvToastText);
            this.g = (ImageView) inflate.findViewById(R.id.ivToastIcon);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            this.h.setTextSize((float) new c(15).d((int) (((double) 15) * 0.9d)).n((int) (((double) 15) * 0.9d)).g((int) (((double) 15) * 0.9d)).h((int) (((double) 15) * 0.9d)).j((int) (((double) 15) * 0.9d)).a());
            this.h.setTextColor(Color.argb(170, 255, 255, 255));
            inflate.setLayoutParams(layoutParams);
            this.b = new Toast(context);
            this.b.setView(inflate);
        }
        this.i.setVisibility(8);
        this.f.setVisibility(8);
        this.g.setVisibility(4);
        this.j.setVisibility(8);
        this.h.setText(str);
        this.b.setDuration(0);
        this.b.show();
        if (a((long) i2)) {
            b((long) i2);
        }
    }

    public a(Context context, boolean z, String str, int i2) {
        this.f60a = 0;
        this.d = new Handler();
        this.e = -1;
        this.l = null;
        this.m = new Runnable() {
            /* class GeneralFunction.a.AnonymousClass1 */

            public void run() {
                a.this.a();
            }
        };
        this.e = 1;
        if (this.b == null) {
            View inflate = LayoutInflater.from(context).inflate(R.layout.style_toast_layout, (ViewGroup) null);
            this.i = (LinearLayout) inflate.findViewById(R.id.LL_ThumbnailLayout);
            this.j = (LinearLayout) inflate.findViewById(R.id.LL_ToastIconLayout);
            this.f = (ImageView) inflate.findViewById(R.id.ivThumbnailView);
            this.h = (TextView) inflate.findViewById(R.id.tvToastText);
            this.g = (ImageView) inflate.findViewById(R.id.ivToastIcon);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            this.h.setTextSize((float) new c(15).f((int) (((double) 15) * 0.9d)).d((int) (((double) 15) * 0.9d)).a());
            this.h.setTextColor(Color.argb(170, 255, 255, 255));
            inflate.setLayoutParams(layoutParams);
            this.b = new Toast(context);
            this.b.setView(inflate);
        }
        this.i.setVisibility(8);
        this.f.setVisibility(8);
        this.j.setVisibility(0);
        this.g.setImageResource(R.drawable.system_ep_white);
        this.h.setText(str);
        this.h.setGravity(17);
        this.b.setDuration(0);
        this.b.show();
        if (a((long) i2)) {
            b((long) i2);
        }
    }

    public void a() {
        if (this.b != null) {
            this.b.cancel();
        }
        if (this.l != null) {
            this.l.recycle();
            this.l = null;
        }
    }

    public int b() {
        return this.e;
    }

    private boolean a(long j2) {
        int i2;
        boolean z = false;
        switch (z) {
            case false:
                i2 = ExoPlayer.Factory.DEFAULT_MIN_BUFFER_MS;
                break;
            case true:
                i2 = 3500;
                break;
            default:
                i2 = 0;
                break;
        }
        if (j2 < ((long) i2)) {
            return true;
        }
        return false;
    }

    private void b(long j2) {
        this.d.postDelayed(this.m, j2);
    }
}
