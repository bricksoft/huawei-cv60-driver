package GeneralFunction.r;

import android.media.MediaCodec;
import java.nio.ByteBuffer;

public class d {

    /* renamed from: a  reason: collision with root package name */
    public int f149a;
    public ByteBuffer b;
    public MediaCodec.BufferInfo c;

    public d() {
        this.f149a = 0;
        this.b = null;
        this.c = null;
    }

    public d(int i, ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) {
        this.f149a = i;
        this.b = byteBuffer;
        this.c = bufferInfo;
    }
}
