package GeneralFunction.r;

import GeneralFunction.d;
import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.view.Surface;
import com.google.android.exoplayer.util.MimeTypes;
import java.io.IOException;
import java.nio.ByteBuffer;
import tv.danmaku.ijk.media.player.IjkMediaMeta;
import tv.danmaku.ijk.media.player.misc.IMediaFormat;

public class a {

    /* renamed from: a  reason: collision with root package name */
    final int f144a = 50;
    AbstractC0005a b;
    private MediaCodec c;
    private AudioRecord d;
    private boolean e = false;
    private long f = 0;
    private long g = 0;
    private boolean h = false;

    /* renamed from: GeneralFunction.r.a$a  reason: collision with other inner class name */
    public interface AbstractC0005a {
        void a();

        void a(MediaFormat mediaFormat);

        void a(ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo);
    }

    public void a(String str, int i) {
        d.a("AudioProcess", str, i);
    }

    public a(AbstractC0005a aVar) {
        a(aVar);
        d();
        e();
    }

    public void a(boolean z) {
        this.h = z;
    }

    private void d() {
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString(IMediaFormat.KEY_MIME, MimeTypes.AUDIO_AAC);
        mediaFormat.setInteger("aac-profile", 2);
        mediaFormat.setInteger("sample-rate", 44100);
        mediaFormat.setInteger("channel-count", 1);
        mediaFormat.setInteger(IjkMediaMeta.IJKM_KEY_BITRATE, 128000);
        mediaFormat.setInteger("max-input-size", 16384);
        try {
            this.c = MediaCodec.createEncoderByType(MimeTypes.AUDIO_AAC);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.c.configure(mediaFormat, (Surface) null, (MediaCrypto) null, 1);
        this.c.start();
    }

    private void e() {
        int minBufferSize = AudioRecord.getMinBufferSize(44100, 16, 2);
        int i = 10240;
        if (10240 < minBufferSize) {
            i = ((minBufferSize / 1024) + 1) * 1024 * 2;
        }
        this.d = new AudioRecord(5, 44100, 16, 2, i);
    }

    public void a() {
        if (this.d != null) {
            this.e = false;
            this.g = 0;
            new Thread(new Runnable() {
                /* class GeneralFunction.r.a.AnonymousClass1 */

                public void run() {
                    a.this.d.startRecording();
                    a.this.f = System.currentTimeMillis() * 1000;
                    while (!a.this.e) {
                        a.this.a((a) a.this.c, (MediaCodec) a.this.e);
                        a.this.b(false);
                    }
                    a.this.a("Audio loop caught fullStopReceived " + a.this.e, 2);
                    a.this.a((a) a.this.c, (MediaCodec) false);
                    a.this.b(true);
                    a.this.a("Stopping AudioRecord", 2);
                    a.this.a((a) a.this.c, (MediaCodec) a.this.e);
                    a.this.d.stop();
                    a.this.f();
                }
            }).start();
        }
    }

    public void b() {
        this.e = true;
    }

    public void b(boolean z) {
        try {
            ByteBuffer[] inputBuffers = this.c.getInputBuffers();
            int dequeueInputBuffer = this.c.dequeueInputBuffer(50);
            if (dequeueInputBuffer >= 0) {
                ByteBuffer byteBuffer = inputBuffers[dequeueInputBuffer];
                byteBuffer.clear();
                int read = this.d.read(byteBuffer, 2048);
                if (this.h) {
                    byteBuffer.clear();
                    byteBuffer.put(new byte[read]);
                    byteBuffer.clear();
                }
                if (read != 2048) {
                    a("audioInputLength not correct:" + read, 0);
                }
                if (read == -3) {
                    a("Audio read error: invalid operation", 0);
                } else if (read == -2) {
                    a("Audio read error: bad value", 0);
                }
                long j = this.f + ((((this.g * 1024) * 1000) * 1000) / 44100);
                if (z) {
                    a("EOS received in sendAudioToEncoder", 2);
                    this.c.queueInputBuffer(dequeueInputBuffer, 0, read, j, 4);
                } else {
                    this.c.queueInputBuffer(dequeueInputBuffer, 0, read, j, 0);
                }
                this.g++;
                return;
            }
            a("dequeueInputBuffer fail, inputBufferIndex:" + dequeueInputBuffer + ", endOfStream:" + z, 0);
        } catch (Throwable th) {
            a("_offerAudioEncoder exception", 0);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(MediaCodec mediaCodec, boolean z) {
        a("pollAudioEncodeOutputAndFeedToMuxer:" + z, 4);
        ByteBuffer[] outputBuffers = mediaCodec.getOutputBuffers();
        int i = 0;
        while (true) {
            i++;
            if (i % 100 == 0) {
                a("lTempCount: " + i + ", endOfStream: " + z, 1);
            }
            MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
            int dequeueOutputBuffer = mediaCodec.dequeueOutputBuffer(bufferInfo, 50);
            a("encoderStatus:" + dequeueOutputBuffer + " endOfStream:" + z, 4);
            if (dequeueOutputBuffer == -1) {
                if (!z) {
                    return;
                }
            } else if (dequeueOutputBuffer == -3) {
                outputBuffers = mediaCodec.getOutputBuffers();
            } else if (dequeueOutputBuffer == -2) {
                if (this.b != null) {
                    this.b.a(mediaCodec.getOutputFormat());
                    return;
                }
                return;
            } else if (dequeueOutputBuffer < 0) {
                a("unexpected result from encoder.dequeueOutputBuffer: " + dequeueOutputBuffer, 1);
            } else {
                ByteBuffer byteBuffer = outputBuffers[dequeueOutputBuffer];
                if (byteBuffer == null) {
                    throw new RuntimeException("encoderOutputBuffer " + dequeueOutputBuffer + " was null");
                }
                if ((bufferInfo.flags & 2) != 0) {
                    bufferInfo.size = 0;
                }
                if (bufferInfo.size != 0) {
                    if (bufferInfo.presentationTimeUs < 0) {
                        bufferInfo.presentationTimeUs = 0;
                    }
                    byteBuffer.position(bufferInfo.offset);
                    byte[] bArr = new byte[bufferInfo.size];
                    byteBuffer.get(bArr, 0, bufferInfo.size);
                    ByteBuffer wrap = ByteBuffer.wrap(bArr);
                    wrap.position(0);
                    bufferInfo.offset = 0;
                    if (this.b != null) {
                        this.b.a(wrap, bufferInfo);
                    }
                }
                mediaCodec.releaseOutputBuffer(dequeueOutputBuffer, false);
                if ((bufferInfo.flags & 4) != 0) {
                    a("endOfStream:" + z, 2);
                    if (!z) {
                        a("reached end of stream unexpectedly", 0);
                        return;
                    } else if (this.b != null) {
                        this.b.a();
                        return;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public void c() {
        if (this.c != null) {
            this.c.stop();
            this.c.release();
            this.c = null;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void f() {
        if (this.d != null) {
            this.d.release();
            this.d = null;
        }
    }

    public void a(AbstractC0005a aVar) {
        this.b = aVar;
    }
}
