package GeneralFunction.r;

import GeneralFunction.d;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f146a = null;
    private String b = null;
    private String c = null;
    private c d = null;
    private MediaMuxer e = null;
    private HandlerThread f = null;
    private Handler g = null;
    private boolean h = true;
    private a i = null;

    public interface a {
        void a(long j);

        void a(String str);

        void a(boolean z);
    }

    private static void a(String str, int i2) {
        d.a("Mp4Muxer", str, i2);
    }

    public b(String str, boolean z, String str2, String str3) {
        this.f146a = str;
        this.h = z;
        this.b = str2;
        this.c = str3;
        this.d = new c();
        c();
        a(1, 0);
    }

    public int a(MediaFormat mediaFormat) {
        if (!this.h) {
            a("Should not do AddAudioTrack.", 0);
            return -1;
        }
        Message message = new Message();
        message.what = 3;
        message.obj = mediaFormat;
        a(message, 0);
        return 0;
    }

    public void b(MediaFormat mediaFormat) {
        Message message = new Message();
        message.what = 2;
        message.obj = mediaFormat;
        a(message, 0);
    }

    public void a(int i2) {
        this.d.l = i2;
    }

    public int a() {
        if (!this.h) {
            a("Should not do FinishAudioTrack.", 0);
            return -1;
        }
        a(7, 0);
        return 0;
    }

    public void b() {
        a(6, 0);
    }

    public int a(ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) {
        if (!this.h) {
            a("Should not do InputAudioFrame.", 0);
            return -1;
        }
        d dVar = new d(-1, byteBuffer, bufferInfo);
        Message message = new Message();
        message.what = 4;
        message.obj = dVar;
        a(message, 0);
        return 0;
    }

    public int a(byte[] bArr, int i2, long j, boolean z) {
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        wrap.position(0);
        wrap.limit(i2);
        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
        if (z) {
            bufferInfo.set(this.d.l, bArr.length - this.d.l, j * 1000, 1);
        } else {
            bufferInfo.set(0, i2, j * 1000, 0);
        }
        return b(wrap, bufferInfo);
    }

    public int b(ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) {
        d dVar = new d(-1, byteBuffer, bufferInfo);
        Message message = new Message();
        message.what = 5;
        message.obj = dVar;
        a(message, 0);
        return 0;
    }

    private void c() {
        this.f = new HandlerThread("RecordingServer");
        this.f.start();
        this.g = new Handler(this.f.getLooper()) {
            /* class GeneralFunction.r.b.AnonymousClass1 */

            public void handleMessage(Message message) {
                super.handleMessage(message);
                b.this.a((b) message);
            }
        };
    }

    public void a(int i2, long j) {
        Message message = new Message();
        message.what = i2;
        a(message, j);
    }

    public void a(Message message, long j) {
        this.g.sendMessageDelayed(message, j);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(Message message) {
        boolean z = false;
        switch (message.what) {
            case 1:
                try {
                    this.e = new MediaMuxer(this.f146a, 0);
                    if (this.e != null) {
                        z = true;
                    }
                    this.i.a(z);
                    return;
                } catch (IOException e2) {
                    e2.printStackTrace();
                    a("MediaMuxer init fail with IOException:" + this.f146a, 2);
                    this.i.a(false);
                    return;
                } catch (IllegalArgumentException e3) {
                    e3.printStackTrace();
                    a("MediaMuxer init fail with IllegalArgumentException:" + this.f146a, 2);
                    this.i.a(false);
                    return;
                }
            case 2:
                if (this.e == null) {
                    a("MSG_RECORDING_ADD_VIDEO_TRACK fail: muxer == null", 0);
                    return;
                }
                this.d.b = (MediaFormat) message.obj;
                if (this.d.b != null && this.d.d < 0) {
                    this.d.d = this.e.addTrack(this.d.b);
                }
                if (this.d.c >= 0 && this.d.d >= 0) {
                    this.e.start();
                    this.d.i = true;
                    return;
                }
                return;
            case 3:
                if (this.e == null) {
                    a("MSG_RECORDING_ADD_AUDIO_TRACK fail: muxer == null", 0);
                    return;
                }
                this.d.f148a = (MediaFormat) message.obj;
                if (!(this.d.f148a == null || this.e == null || this.d.c >= 0)) {
                    this.d.c = this.e.addTrack(this.d.f148a);
                }
                if (this.d.c >= 0 && this.d.d >= 0) {
                    this.e.start();
                    this.d.i = true;
                    return;
                }
                return;
            case 4:
                d dVar = (d) message.obj;
                if (this.d.i && this.d.j && dVar.c.presentationTimeUs > this.d.k) {
                    this.d.e++;
                    dVar.f149a = this.d.c;
                    this.e.writeSampleData(dVar.f149a, dVar.b, dVar.c);
                    return;
                }
                return;
            case 5:
                d dVar2 = (d) message.obj;
                if (!this.d.i) {
                    a("InputVideoFrame Fail: Muxer not ready!!! Skip this frame.  TimeStamp:" + dVar2.c.presentationTimeUs, 1);
                    return;
                } else if (this.d.j) {
                    this.d.f++;
                    dVar2.f149a = this.d.d;
                    this.e.writeSampleData(dVar2.f149a, dVar2.b, dVar2.c);
                    return;
                } else if ((dVar2.c.flags & 1) != 0) {
                    this.d.j = true;
                    this.d.k = dVar2.c.presentationTimeUs;
                    this.d.f++;
                    dVar2.f149a = this.d.d;
                    this.e.writeSampleData(dVar2.f149a, dVar2.b, dVar2.c);
                    if (this.i != null) {
                        this.i.a(this.d.k);
                        return;
                    }
                    return;
                } else {
                    a("InputVideoFrame Fail: First I frame not ready!!! Skip this frame.  TimeStamp:" + dVar2.c.presentationTimeUs, 1);
                    return;
                }
            case 6:
            case 7:
                if (message.what == 6) {
                    this.d.h = true;
                } else {
                    this.d.g = true;
                }
                if (this.d.g && this.d.h) {
                    try {
                        d();
                        if (this.d.f != 0) {
                            a("createThumbnail Start: " + System.currentTimeMillis(), 3);
                            e();
                            a("createThumbnail End, addMetaDataInfo Start: " + System.currentTimeMillis(), 3);
                            f();
                            a("addMetaDataInfo End: " + System.currentTimeMillis(), 3);
                        } else {
                            z = true;
                        }
                    } catch (IllegalStateException e4) {
                        e4.printStackTrace();
                        a("Muxer Stop Fail", 2);
                        z = true;
                    }
                    if (z) {
                        File file = new File(this.f146a);
                        if (file.exists()) {
                            file.delete();
                        }
                        this.f146a = null;
                    }
                    if (this.i != null) {
                        this.i.a(this.f146a);
                    }
                    this.f.quit();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void d() {
        if (this.e != null) {
            if (this.d.i) {
                this.e.stop();
            }
            this.e.release();
            this.e = null;
            this.d.i = false;
            this.d.j = false;
        }
    }

    private void e() {
        if (new File(this.f146a).exists()) {
            GeneralFunction.g.a.d(this.f146a, GeneralFunction.g.a.d(this.f146a));
            return;
        }
        a("Original File Not Exist!", 1);
    }

    private void f() {
        if (new File(this.f146a).exists()) {
            GeneralFunction.s.a aVar = new GeneralFunction.s.a();
            GeneralFunction.s.b bVar = new GeneralFunction.s.b();
            bVar.f151a = this.b;
            bVar.b = this.c;
            int a2 = aVar.a(this.f146a, bVar);
            if (a2 != 0) {
                a("addMetaDataInfo fail: " + a2, 1);
                return;
            }
            return;
        }
        a("Original File Not Exist!", 1);
    }

    public void a(a aVar) {
        this.i = aVar;
    }
}
