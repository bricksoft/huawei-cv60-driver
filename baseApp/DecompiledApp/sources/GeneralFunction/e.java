package GeneralFunction;

import com.google.android.exoplayer.text.eia608.ClosedCaptionCtrl;

public class e {
    public static int a(byte[] bArr) {
        byte[] bArr2;
        if (bArr.length < 5 || bArr == null) {
            return -1;
        }
        switch (bArr[4] & ClosedCaptionCtrl.TAB_OFFSET_CHAN_2) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
            case 8:
            case 10:
            case 11:
            case 12:
                return 1;
            case 5:
            case 7:
                return 0;
            case 9:
                int i = 5;
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    if (i < bArr.length) {
                        if (i3 == 0 && bArr[i] == 0) {
                            i3++;
                            i2 = i;
                        } else if (i3 < 3 && bArr[i] == 0) {
                            i3++;
                        } else if (i3 == 3 && bArr[i] == 1) {
                            bArr2 = new byte[5];
                            System.arraycopy(bArr, i2, bArr2, 0, 5);
                        } else {
                            i2 = 0;
                            i3 = 0;
                        }
                        i++;
                    } else {
                        bArr2 = null;
                    }
                }
                return a(bArr2);
            default:
                return -1;
        }
    }

    public static boolean b(byte[] bArr) {
        switch (a(bArr)) {
            case 0:
                return true;
            default:
                return false;
        }
    }

    private static int a(byte[] bArr, int i) {
        int i2 = 0;
        int i3 = 0;
        while (i < bArr.length) {
            if (i3 == 0 && bArr[i] == 0) {
                i3++;
                i2 = i;
            } else if (i3 < 3 && bArr[i] == 0) {
                i3++;
            } else if (i3 == 3 && bArr[i] == 1) {
                return i2;
            } else {
                i2 = 0;
                i3 = 0;
            }
            i++;
        }
        return -1;
    }

    public static byte[] c(byte[] bArr) {
        int i = 0;
        while (bArr != null && bArr.length >= i + 5) {
            int a2 = a(bArr, i);
            if (a2 == -1) {
                return null;
            }
            i = a2 + 1;
            if ((bArr[a2 + 4] & ClosedCaptionCtrl.TAB_OFFSET_CHAN_2) == 7) {
                int a3 = a(bArr, i);
                if (a3 == -1) {
                    return null;
                }
                byte[] bArr2 = new byte[(a3 - a2)];
                System.arraycopy(bArr, a2, bArr2, 0, a3 - a2);
                return bArr2;
            }
        }
        return null;
    }

    public static byte[] d(byte[] bArr) {
        int i = 0;
        while (bArr != null && bArr.length >= i + 5) {
            int a2 = a(bArr, i);
            if (a2 == -1) {
                return null;
            }
            i = a2 + 1;
            if ((bArr[a2 + 4] & ClosedCaptionCtrl.TAB_OFFSET_CHAN_2) == 8) {
                int a3 = a(bArr, i);
                if (a3 == -1) {
                    return null;
                }
                byte[] bArr2 = new byte[(a3 - a2)];
                System.arraycopy(bArr, a2, bArr2, 0, a3 - a2);
                return bArr2;
            }
        }
        return null;
    }
}
