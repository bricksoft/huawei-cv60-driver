package GeneralFunction.k;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import java.io.InputStream;
import java.io.InputStreamReader;

public class a {
    private static a d = null;

    /* renamed from: a  reason: collision with root package name */
    NetworkInfo f115a = null;
    private ConnectivityManager b = null;
    private Network c = null;
    private Context e = null;
    private NetworkCapabilities f = null;

    private a(Context context) {
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (d == null) {
                d = new a(context);
            }
            aVar = d;
        }
        return aVar;
    }

    public double a(int i) {
        String str;
        if (i == 0) {
            str = "live-360.facebook.com";
        } else {
            str = "a.rtmp.youtube.com";
        }
        double d2 = 0.0d;
        for (int i2 = 0; i2 < 4; i2++) {
            try {
                Process start = new ProcessBuilder(new String[0]).command("/system/bin/ping", "-c 1", str).redirectErrorStream(true).start();
                String str2 = "";
                try {
                    InputStream inputStream = start.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
                    while (true) {
                        int read = inputStream.read();
                        if (read == -1) {
                            break;
                        }
                        str2 = str2 + ((char) read);
                    }
                    inputStream.close();
                    inputStreamReader.close();
                    if (str2.contains("time=")) {
                        String substring = str2.substring(str2.indexOf("time=") + 5, str2.length());
                        d2 += Double.valueOf(substring.substring(0, substring.indexOf(" ms"))).doubleValue();
                        start.destroy();
                    } else {
                        start.destroy();
                        return -1.0d;
                    }
                } catch (Exception e2) {
                    return -1.0d;
                } finally {
                    start.destroy();
                }
            } catch (Exception e3) {
                return -1.0d;
            }
        }
        return d2 / 4.0d;
    }
}
