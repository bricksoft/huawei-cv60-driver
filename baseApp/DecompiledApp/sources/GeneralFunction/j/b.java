package GeneralFunction.j;

import GeneralFunction.d;
import ThirdParty.Rtmp.a;
import ThirdParty.a.a;
import ThirdParty.b.b;
import a.c.a;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.InputDeviceCompat;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.Profile;
import com.facebook.d;
import com.facebook.h;
import com.facebook.k;
import com.facebook.login.f;
import com.facebook.login.g;
import com.facebook.p;
import com.facebook.q;
import com.facebook.w;
import com.google.android.exoplayer.hls.HlsChunkSource;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import com.google.android.gms.plus.PlusShare;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import java.util.Collections;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import ui_Controller.b.n;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private UI_ModeMain f106a = null;
    private a b = null;
    private long c = -1;
    private a.AbstractC0008a d = new a.AbstractC0008a() {
        /* class GeneralFunction.j.b.AnonymousClass4 */

        @Override // ThirdParty.a.a.AbstractC0008a
        public void a(int i, String str) {
            b.b("onAccountLogin result: " + i, 3);
            b.b("accountName " + str, 3);
            if (i == 0) {
                b.this.a(str);
                b.this.a(true);
                b.this.j();
                b.this.f106a.a(10835, 0);
                b.this.m();
                return;
            }
            b.this.a(str);
            b.this.a(false);
            b.this.f106a.a(10914, 0);
        }

        @Override // ThirdParty.a.a.AbstractC0008a
        public void a(int i) {
            b.b("onAccountLogout result: " + i, 3);
            if (i == 0) {
                b.this.a((String) null);
                b.this.a(false);
                GeneralFunction.n.a.a(b.this.f106a).edit().remove("googleAccountName").commit();
                b.this.f106a.a(10836, 0);
            }
        }
    };
    private a.AbstractC0007a e = new a.AbstractC0007a() {
        /* class GeneralFunction.j.b.AnonymousClass5 */

        @Override // ThirdParty.Rtmp.a.AbstractC0007a
        public void a(int i, int i2) {
            switch (i) {
                case 8192:
                    b.b("MSG_LIVE_STREAMING_RTMP_STREAMER_INIT " + i2, 0);
                    if (i2 == 0) {
                        b.this.f106a.a(10845, 0);
                        return;
                    } else {
                        b.this.f106a.a(10922, 0);
                        return;
                    }
                case 8193:
                    if (i2 < 0) {
                        b.b("MSG_LIVE_STREAMING_RTMP_STREAMER_WRITE_VIDEO_FRAME ***ERROR*** " + i2, 0);
                        if (i2 == -999) {
                            b.this.f106a.a(10921, 0);
                            return;
                        }
                        if (b.this.c == -1) {
                            b.this.c = System.currentTimeMillis();
                        }
                        if (System.currentTimeMillis() - b.this.c > HlsChunkSource.DEFAULT_MIN_BUFFER_TO_SWITCH_UP_MS) {
                            b.this.f106a.a(10921, 0);
                            return;
                        }
                        return;
                    }
                    b.this.c = -1;
                    return;
                case 8194:
                    if (i2 < 0) {
                        b.b("MSG_LIVE_STREAMING_RTMP_STREAMER_WRITE_AUDIO_FRAME ***ERROR*** " + i2, 0);
                        return;
                    }
                    return;
                case 8195:
                    if (i2 == 0 && b.this.b.k != 1) {
                        if (b.this.b.k == 2) {
                            b.this.f106a.a(10844, 0);
                            return;
                        } else {
                            b.b("ERROR FILE_STREAMING_TO_THE_END StreamingSns!!!", 0);
                            return;
                        }
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    };
    private b.a f = new b.a() {
        /* class GeneralFunction.j.b.AnonymousClass6 */

        @Override // ThirdParty.b.b.a
        public void a(int i, int i2) {
            switch (i) {
                case 4096:
                    if (i2 == 0) {
                        b.this.f106a.a(10843, 0);
                        return;
                    }
                    return;
                case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
                    if (i2 == 0) {
                        b.this.f106a.a(10838, 0);
                        return;
                    } else {
                        b.this.f106a.a(10916, 0);
                        return;
                    }
                case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
                    if (i2 == 0) {
                        b.this.f106a.a(10837, 0);
                        return;
                    } else {
                        b.this.f106a.a(10915, 0);
                        return;
                    }
                case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
                    if (i2 == 0) {
                        if (!ThirdParty.Rtmp.a.a().b()) {
                            ThirdParty.Rtmp.a.a().a(1440, 720, 44100, n.c, 27, 30, 3145728, 15);
                            ThirdParty.Rtmp.a.a().a(8192);
                        }
                        b.this.f106a.a(10839, 0);
                        return;
                    }
                    a.c.a aVar = new a.c.a(10917);
                    aVar.a("youtubeApiResult", i2);
                    b.this.f106a.a(aVar);
                    return;
                case 4100:
                    if (i2 == 0) {
                        ThirdParty.b.b.a().a(4105);
                        return;
                    } else {
                        b.this.f106a.a(10918, 0);
                        return;
                    }
                case 4101:
                    if (i2 == 0) {
                        b.this.f106a.a(10841, 0);
                        return;
                    } else {
                        b.this.f106a.a(10919, 0);
                        return;
                    }
                case 4102:
                case 4103:
                case 4104:
                default:
                    return;
                case 4105:
                    if (i2 == 0) {
                        b.this.f106a.a(10840, 0);
                        return;
                    } else {
                        b.this.f106a.a(10918, 0);
                        return;
                    }
            }
        }

        @Override // ThirdParty.b.b.a
        public void a(String str, String str2) {
            if (str != null) {
                b.this.b.c = str;
                b.this.b.d = str2;
                a.c.a aVar = new a.c.a(10846);
                aVar.a("streamingLink", b.this.b.d);
                b.this.f106a.a(aVar);
            }
        }

        @Override // ThirdParty.b.b.a
        public void a(boolean z) {
            a.c.a aVar = new a.c.a(10842);
            aVar.a("havePermission", z);
            b.this.f106a.a(aVar);
        }
    };

    /* access modifiers changed from: private */
    public static void b(String str, int i) {
        d.a("UI_LiveStreamingController", str, i);
    }

    public b(UI_ModeMain uI_ModeMain) {
        this.f106a = uI_ModeMain;
        this.b = new a();
        k.a(this.f106a);
        this.b.f = d.a.a();
        this.b.i = new ThirdParty.a.a(this.f106a);
        this.b.i.a(this.d);
        ThirdParty.Rtmp.a.a().a(this.e);
        ThirdParty.b.b.a().a(this.f);
        ThirdParty.b.b.a().a(this.f106a.c.b);
        k();
        l();
    }

    public boolean a(Message message) {
        new a.c.a(message);
        switch (message.what) {
            case 10752:
                List singletonList = Collections.singletonList("publish_actions");
                f.a().a(com.facebook.login.a.EVERYONE);
                f.a().a(this.f106a.c.b, singletonList);
                f.a().a(this.b.f, new com.facebook.f<g>() {
                    /* class GeneralFunction.j.b.AnonymousClass1 */

                    public void a(g gVar) {
                        Profile a2 = Profile.a();
                        b.b("FacebookCallback onSuccess", 3);
                        if (a2 != null) {
                            b.b("profile: " + a2.d(), 3);
                            b.this.b(a2.d());
                            b.this.c(a2.c());
                            b.this.f106a.a(10832, 0);
                            return;
                        }
                        b.b("profile is null...", 3);
                        b.this.b(" ");
                        b.this.c(" ");
                        new w() {
                            /* class GeneralFunction.j.b.AnonymousClass1.AnonymousClass1 */

                            /* access modifiers changed from: protected */
                            @Override // com.facebook.w
                            public void a(Profile profile, Profile profile2) {
                                b.b("onCurrentProfileChanged(): " + profile2.d(), 3);
                                b.this.b(profile2.d());
                                b.this.c(profile2.c());
                                b();
                                b.this.f106a.a(10832, 0);
                            }
                        }.a();
                    }

                    @Override // com.facebook.f
                    public void a() {
                        b.b("FacebookCallback onCancel", 3);
                        b.this.f106a.a(10912, 0);
                    }

                    @Override // com.facebook.f
                    public void a(h hVar) {
                        b.b("FacebookCallback onError", 3);
                        b.this.f106a.a(10912, 0);
                    }
                });
                break;
            case 10753:
                f.a().b();
                this.f106a.a(10833, 0);
                break;
            case 10754:
                String str = "/" + e() + "/live_videos";
                String d2 = new a.c.a(message).d("liveStreamDescription");
                Bundle bundle = new Bundle();
                bundle.putString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, d2);
                bundle.putBoolean("save_vod", true);
                bundle.putBoolean("is_spherical", true);
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("value", "EVERYONE");
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                bundle.putString("privacy", jSONObject.toString());
                new GraphRequest(AccessToken.a(), str, bundle, q.POST, new GraphRequest.b() {
                    /* class GeneralFunction.j.b.AnonymousClass2 */

                    @Override // com.facebook.GraphRequest.b
                    public void a(p pVar) {
                        if (pVar == null) {
                            b.b("GraphRequest's response == null", 3);
                            a.c.a aVar = new a.c.a(10913);
                            aVar.a("httpStatusCode", -1);
                            b.this.f106a.a(aVar);
                            return;
                        }
                        b.b("GraphRequest's response =/= null ", 3);
                        b.this.a((b) pVar);
                    }
                }).j();
                break;
            case 10755:
                this.b.i.a();
                break;
            case 10756:
                this.b.i.b();
                break;
            case 10757:
                this.b.m = false;
                message.what = InputDeviceCompat.SOURCE_TOUCHSCREEN;
                ThirdParty.b.b.a().a(new a.c.a(message).b());
                break;
            case 10758:
                this.b.m = true;
                message.what = FragmentTransaction.TRANSIT_FRAGMENT_OPEN;
                ThirdParty.b.b.a().a(new a.c.a(message).b());
                break;
            case 10759:
                this.b.m = true;
                message.what = FragmentTransaction.TRANSIT_FRAGMENT_FADE;
                ThirdParty.b.b.a().a(new a.c.a(message).b());
                break;
            case 10760:
                a.c.a aVar = new a.c.a(message);
                this.b.k = 1;
                this.b.l = aVar.b("StreamingType");
                if (this.b.l == 1) {
                    if (this.b.m) {
                        ThirdParty.b.b.a().a(4100);
                        break;
                    }
                } else {
                    b("ERROR START_EVENT StreamingType!!!", 0);
                    break;
                }
                break;
            case 10761:
                if (ThirdParty.Rtmp.a.a().b()) {
                    if (this.b.l != 1) {
                        b("ERROR END_EVENT StreamingType!!!", 0);
                        break;
                    } else {
                        ThirdParty.b.b.a().a(4101);
                        ThirdParty.Rtmp.a.a().a(8195);
                        break;
                    }
                } else {
                    this.f106a.a(10841, 0);
                    break;
                }
            case 10762:
                message.what = 4102;
                ThirdParty.b.b.a().a(new a.c.a(message).b());
                break;
            case 10763:
                ThirdParty.b.b.a().a(h());
                break;
            case 10764:
                a.c.a aVar2 = new a.c.a(message);
                this.b.k = 2;
                this.b.l = aVar2.b("StreamingType");
                if (this.b.l != 1) {
                    b("ERROR START_LIVE_VIDEOS StreamingType!!!", 0);
                    break;
                } else {
                    b("STREAMING_TYPE_LIVEVIEW", 2);
                    if (!ThirdParty.Rtmp.a.a().b()) {
                        ThirdParty.Rtmp.a.a().a(aVar2.b("Width"), aVar2.b("Height"), aVar2.b("AudioSampleRate"), aVar2.e("SpsPpsBuffer"), aVar2.b("SpsPpsSize"), aVar2.b("VideoFps"), aVar2.b("VideoBitrate"), aVar2.b("VideoGOP"));
                        ThirdParty.Rtmp.a.a().a(aVar2.b("Width"), aVar2.b("Height"), aVar2.b("AudioSampleRate"), aVar2.e("SpsPpsBuffer"), aVar2.b("SpsPpsSize"), aVar2.b("VideoFps"), aVar2.b("VideoBitrate"), aVar2.b("VideoGOP"), aVar2.d("rtmpURL"));
                        ThirdParty.Rtmp.a.a().a(8192);
                        break;
                    }
                }
                break;
            case 10765:
                if (this.b.l != 1) {
                    b("ERROR END_LIVE_VIDEOS StreamingType!!!", 0);
                    break;
                } else {
                    ThirdParty.Rtmp.a.a().a(8195);
                    break;
                }
            case 10768:
                ThirdParty.b.b.a().a(4103);
                break;
            case 10769:
                ThirdParty.b.b.a().a(4104);
                break;
            case 10773:
                if (!ThirdParty.Rtmp.a.a().b()) {
                    this.f106a.a(10844, 0);
                    break;
                } else {
                    Bundle bundle2 = new Bundle();
                    bundle2.putBoolean("end_live_video", true);
                    new GraphRequest(AccessToken.a(), "/" + f(), bundle2, q.POST, new GraphRequest.b() {
                        /* class GeneralFunction.j.b.AnonymousClass3 */

                        @Override // com.facebook.GraphRequest.b
                        public void a(p pVar) {
                            if (pVar == null) {
                                b.b("GraphRequest's response == null", 3);
                            } else {
                                b.b("GraphRequest's response =/= null ", 3);
                            }
                        }
                    }).j();
                    if (this.b.l != 1) {
                        b("ERROR END_EVENT StreamingType!!!", 0);
                        break;
                    } else {
                        ThirdParty.Rtmp.a.a().a(8195);
                        break;
                    }
                }
            case 10992:
                if (this.b.k != 1) {
                    if (this.b.k != 2) {
                        b("ERROR FILE_STREAMING_TO_THE_END StreamingSns!!!", 0);
                        break;
                    } else {
                        this.f106a.a(10765, 0);
                        break;
                    }
                } else {
                    this.f106a.a(10761, 0);
                    break;
                }
        }
        return false;
    }

    public static boolean a() {
        return AccessToken.a() != null;
    }

    private com.facebook.d i() {
        return this.b.f;
    }

    public void a(boolean z) {
        b("setGoogleAccountName: " + z, 1);
        this.b.j = z;
    }

    public boolean b() {
        return this.b.j;
    }

    public void a(String str) {
        this.b.g = str;
    }

    public String c() {
        return this.b.g;
    }

    public void b(String str) {
        this.b.h = str;
    }

    public void c(String str) {
        this.b.b = str;
    }

    public String d() {
        return this.b.h;
    }

    public String e() {
        return this.b.b;
    }

    public void d(String str) {
        this.b.e = str;
    }

    public String f() {
        return this.b.e;
    }

    public ThirdParty.a.a g() {
        return this.b.i;
    }

    public GoogleAccountCredential h() {
        return this.b.i.f159a;
    }

    public void a(int i, int i2, Intent intent) {
        g().a(i, i2, intent);
        i().a(i, i2, intent);
    }

    public void a(GeneralFunction.c.d dVar, long j) {
        a.c.a aVar = new a.c.a(8194);
        aVar.a("streamAudioData", new a.C0010a(dVar));
        aVar.a("streamAudioPts", (int) j);
        ThirdParty.Rtmp.a.a().a(aVar.b());
    }

    public void b(GeneralFunction.c.d dVar, long j) {
        a.c.a aVar = new a.c.a(8193);
        aVar.a("streamVideoData", new a.C0010a(dVar));
        aVar.a("streamVideoPts", (int) j);
        ThirdParty.Rtmp.a.a().a(aVar.b());
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void j() {
        b("[DBG] saveAccount", 3);
        GeneralFunction.n.a.b(this.f106a).putString("googleAccountName", c()).apply();
    }

    private void k() {
        b("loadAccount", 3);
        a(GeneralFunction.n.a.a(this.f106a).getString("googleAccountName", null));
        if (c() == null) {
            b(" GoogleAccountName: " + this.b.g, 3);
            a(false);
            return;
        }
        b(" GoogleAccountName:" + this.b.g, 3);
        g().a(c());
        a(true);
    }

    private void l() {
        if (a()) {
            b(Profile.a().d());
            c(Profile.a().c());
            return;
        }
        b("");
        c("");
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void m() {
        g().a(c());
        ThirdParty.b.b.a().a(this.f106a.c.b);
        ThirdParty.b.b.a().a(g().f159a);
        ThirdParty.b.b.a().a(4102);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(p pVar) {
        if (pVar.a() != null) {
            b("StatusCode: " + pVar.a().a(), 3);
            b(pVar.a().e(), 3);
            a.c.a aVar = new a.c.a(10913);
            aVar.a("httpStatusCode", pVar.a().a());
            this.f106a.a(aVar);
            return;
        }
        JSONObject b2 = pVar.b();
        try {
            this.b.c = b2.getString("stream_url");
            b("LiveStreamingURL: " + this.b.c, 3);
            d(b2.getString(TtmlNode.ATTR_ID));
            a.c.a aVar2 = new a.c.a(10834);
            aVar2.a("rtmpURL", this.b.c);
            this.f106a.a(aVar2.b(), 0);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }
}
