package GeneralFunction;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.SoundPool;
import com.huawei.cvIntl60.R;

public class n {
    private static n h = null;

    /* renamed from: a  reason: collision with root package name */
    private SoundPool.Builder f126a;
    private AudioAttributes.Builder b;
    private int c;
    private SoundPool d;
    private int e;
    private int f;
    private int g;

    public n() {
        this.f126a = null;
        this.b = null;
        this.c = 1;
        this.d = null;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.f126a = new SoundPool.Builder();
        this.f126a.setMaxStreams(this.c);
        this.b = new AudioAttributes.Builder();
        this.b.setLegacyStreamType(3);
        this.f126a.setAudioAttributes(this.b.build());
        this.d = this.f126a.build();
    }

    public static synchronized n a() {
        n nVar;
        synchronized (n.class) {
            if (h == null) {
                h = new n();
            }
            nVar = h;
        }
        return nVar;
    }

    public void a(Context context) {
        this.e = this.d.load(context, R.raw.shutter_sound, 0);
        this.f = this.d.load(context, R.raw.video_sound, 0);
        this.g = this.d.load(context, R.raw.selftimer, 0);
    }

    public void b() {
        this.d.play(this.e, 1.0f, 1.0f, 0, 0, 1.0f);
    }

    public void c() {
        this.d.play(this.f, 1.0f, 1.0f, 0, 0, 1.0f);
    }

    public void d() {
        this.d.play(this.g, 1.0f, 1.0f, 0, 0, 1.0f);
    }
}
