package GeneralFunction.i.a;

import GeneralFunction.d;
import GeneralFunction.k;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.huawei.cvIntl60.R;

public class a {
    private int A;
    private boolean B;
    private int C;
    private int D;

    /* renamed from: a  reason: collision with root package name */
    public boolean f102a;
    private int b;
    private int c;
    private LinearLayout d;
    private LinearLayout e;
    private TextView f;
    private TextView g;
    private LinearLayout h;
    private LinearLayout i;
    private float j;
    private float k;
    private Button l;
    private ViewGroup m;
    private AbstractC0002a n;
    private b o;
    private int p;
    private int q;
    private int r;
    private float s;
    private float t;
    private int u;
    private int v;
    private float w;
    private float x;
    private float y;
    private int z;

    /* renamed from: GeneralFunction.i.a.a$a  reason: collision with other inner class name */
    public interface AbstractC0002a {
        void a(int i, int i2);
    }

    public interface b {
        boolean a(int i, int i2, MotionEvent motionEvent);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        d.a("HorizontalScrollRangeSelectBar", str, i2);
    }

    public a(Context context, ViewGroup viewGroup, boolean z2, int i2, int i3, int i4, int i5, boolean z3) {
        this(context, viewGroup, z2, i2, i3, i4, i5, z3, i3);
    }

    public a(Context context, ViewGroup viewGroup, boolean z2, int i2, int i3, int i4, int i5, boolean z3, int i6) {
        this.b = R.drawable.gallery_gif_dot_1;
        this.c = R.drawable.gallery_gif_dot_2;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = 0.0f;
        this.k = 0.0f;
        this.l = null;
        this.m = null;
        this.p = 2;
        this.q = 1;
        this.r = 0;
        this.s = 0.0f;
        this.t = 0.0f;
        this.u = 0;
        this.v = 0;
        this.w = 0.4f;
        this.x = (1.0f - this.w) / 2.0f;
        this.y = this.w * 0.25f;
        this.z = 0;
        this.A = 0;
        this.B = true;
        this.C = 1;
        this.f102a = true;
        this.D = 0;
        this.m = viewGroup;
        this.p = i2;
        this.q = i3;
        this.C = i6;
        this.u = this.m.getWidth();
        this.v = this.m.getHeight();
        View inflate = LayoutInflater.from(context).inflate(R.layout.style_horizontal_scroll_range_select_bar, (ViewGroup) null);
        viewGroup.addView(inflate);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), this.b, options);
        int i7 = options.outHeight;
        int i8 = options.outWidth;
        this.d = (LinearLayout) inflate.findViewById(R.id.horizontal_scroll_style_start_pointer);
        float f2 = (((float) this.v) * this.w) / ((float) i7);
        ViewGroup.LayoutParams layoutParams = this.d.getLayoutParams();
        layoutParams.width = (int) (f2 * ((float) i8));
        layoutParams.height = (int) (((float) this.v) * this.w);
        this.d.setLayoutParams(layoutParams);
        this.e = (LinearLayout) inflate.findViewById(R.id.horizontal_scroll_style_end_pointer);
        float f3 = (((float) this.v) * this.w) / ((float) i7);
        ViewGroup.LayoutParams layoutParams2 = this.e.getLayoutParams();
        layoutParams2.width = (int) (f3 * ((float) i8));
        layoutParams2.height = (int) (((float) this.v) * this.w);
        this.e.setLayoutParams(layoutParams2);
        this.f102a = z3;
        if (z3) {
            this.d.setBackgroundResource(this.b);
            this.e.setBackgroundResource(this.c);
        } else if (!z3) {
            this.d.setBackgroundResource(this.c);
            this.e.setBackgroundResource(this.b);
        }
        this.z = layoutParams2.width;
        this.z = ((this.z + 1) / 2) * 2;
        this.f = (TextView) inflate.findViewById(R.id.horizontal_scroll_style_start_label);
        ViewGroup.LayoutParams layoutParams3 = this.f.getLayoutParams();
        layoutParams3.height = (int) (((float) this.v) * this.x);
        layoutParams3.width = this.z * 4;
        this.f.setTextSize((float) k.a(context, (float) (this.v / 5)));
        this.g = (TextView) inflate.findViewById(R.id.horizontal_scroll_style_end_label);
        ViewGroup.LayoutParams layoutParams4 = this.g.getLayoutParams();
        layoutParams4.height = (int) (((float) this.v) * this.x);
        layoutParams4.width = this.z * 4;
        this.g.setTextSize((float) k.a(context, (float) (this.v / 5)));
        if (this.B) {
            this.f.setVisibility(4);
            this.g.setVisibility(4);
            this.A = this.z;
        } else {
            this.A = layoutParams4.width;
        }
        this.r = this.u - this.A;
        this.s = (((float) this.q) * ((float) this.r)) / ((float) (this.p - 1));
        this.t = (((float) this.C) * ((float) this.r)) / ((float) (this.p - 1));
        float c2 = c(i4 + 1);
        float c3 = c(i5 + 1);
        this.j = c(c2);
        this.k = c(c3);
        float f4 = (1.0f * ((float) this.r)) / ((float) (this.p - 1));
        while (d(this.j) != i4) {
            if (d(this.j) < i4) {
                this.j += f4 / 100.0f;
            } else {
                this.j -= f4 / 100.0f;
            }
        }
        while (d(this.k) != i5) {
            if (d(this.k) < i5) {
                this.k += f4 / 100.0f;
            } else {
                this.k -= f4 / 100.0f;
            }
        }
        a(this.d, (int) this.j);
        a(this.e, (int) this.k);
        a(this.f, e(c2));
        this.f.setText("" + d(this.j));
        a(this.g, e(c3));
        this.g.setText("" + d(this.k));
        this.i = (LinearLayout) inflate.findViewById(R.id.horizontal_scroll_style_select_range_bar);
        this.i.getLayoutParams().height = (int) (((float) this.v) * this.y);
        a(this.i, (int) this.j, (int) this.k);
        this.h = (LinearLayout) inflate.findViewById(R.id.horizontal_scroll_style_select_range_bg);
        this.h.getLayoutParams().height = (int) (((float) this.v) * this.y);
        a(this.h, (int) c(0.0f), (int) c((float) this.u));
        this.l = (Button) inflate.findViewById(R.id.horizontal_scroll_style_touch_area);
        if (z2) {
            this.l.setOnTouchListener(new View.OnTouchListener() {
                /* class GeneralFunction.i.a.a.AnonymousClass1 */

                public boolean onTouch(View view, MotionEvent motionEvent) {
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    if (a.this.D == 1) {
                        a.this.f102a = true;
                        if (a.this.a((a) a.this.k) - x < a.this.t) {
                            x = a.this.a((a) a.this.k) - a.this.t;
                        }
                        while (a.this.d((a) a.this.k) - a.this.d((a) a.this.c((a) x)) < a.this.C) {
                            a.this.a((a) ("make sure label gap:" + x + " " + a.this.k), (String) 2);
                            x = a.this.b((a) (x - (a.this.t / 100.0f)));
                        }
                    } else if (a.this.D == 2) {
                        a.this.f102a = false;
                        if (x - a.this.a((a) a.this.j) < a.this.t) {
                            x = a.this.a((a) a.this.j) + a.this.t;
                        }
                        while (a.this.d((a) a.this.c((a) x)) - a.this.d((a) a.this.j) < a.this.C) {
                            a.this.a((a) ("make sure label gap:" + x + " " + a.this.j), (String) 2);
                            x = a.this.b((a) (x + (a.this.t / 100.0f)));
                        }
                    }
                    if (a.this.o == null || !a.this.o.a(a.this.D, a.this.d((a) a.this.c((a) x)), motionEvent)) {
                        if (motionEvent.getAction() == 0) {
                            a.this.D = a.this.a((a) x, y);
                            if (a.this.D == 1) {
                                a.this.d.setBackgroundResource(a.this.b);
                                a.this.e.setBackgroundResource(a.this.c);
                            } else if (a.this.D == 2) {
                                a.this.d.setBackgroundResource(a.this.c);
                                a.this.e.setBackgroundResource(a.this.b);
                            }
                        } else if (motionEvent.getAction() == 2) {
                            if (a.this.D == 1) {
                                a.this.j = a.this.c((a) x);
                                a.this.a((a) a.this.d, (LinearLayout) ((int) a.this.j));
                                a.this.a((a) a.this.i, (LinearLayout) ((int) a.this.j), (int) a.this.k);
                                a.this.a((a) a.this.f, (TextView) a.this.e((a) x));
                                a.this.f.setText("" + a.this.d((a) a.this.j));
                            } else if (a.this.D == 2) {
                                a.this.k = a.this.c((a) x);
                                a.this.a((a) a.this.e, (LinearLayout) ((int) a.this.k));
                                a.this.a((a) a.this.i, (LinearLayout) ((int) a.this.j), (int) a.this.k);
                                a.this.a((a) a.this.g, (TextView) a.this.e((a) x));
                                a.this.g.setText("" + a.this.d((a) a.this.k));
                            }
                        } else if (motionEvent.getAction() == 1) {
                            if (a.this.D == 1) {
                                if (a.this.n != null) {
                                    a.this.n.a(1, a.this.d((a) a.this.j));
                                }
                            } else if (a.this.D == 2) {
                                if (a.this.n != null) {
                                    a.this.n.a(2, a.this.d((a) a.this.k));
                                }
                            } else if (a.this.n != null) {
                                a.this.n.a(0, 0);
                            }
                            a.this.D = 0;
                        }
                    }
                    return false;
                }
            });
        }
    }

    public void a(AbstractC0002a aVar) {
        this.n = aVar;
    }

    public void a(b bVar) {
        this.o = bVar;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int a(float f2, float f3) {
        int i2 = (this.z / 2) * 2;
        int i3 = this.v / 2;
        float c2 = c(f2);
        if (Math.abs(c2 - this.j) < ((float) i2)) {
            if (Math.abs(c2 - this.k) < ((float) i2)) {
                return f3 > ((float) i3) ? 2 : 1;
            }
            return 1;
        } else if (Math.abs(c2 - this.k) >= ((float) i2)) {
            return 0;
        } else {
            return 2;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float a(float f2) {
        return ((float) (this.A / 2)) + (f2 - ((float) ((this.A - this.z) / 2)));
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float b(float f2) {
        if (f2 < 0.0f) {
            return 0.0f;
        }
        return f2;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float c(float f2) {
        int i2 = this.A;
        int i3 = i2 / 2;
        int i4 = (this.A - this.z) / 2;
        if (f2 < ((float) i3)) {
            return (float) i4;
        }
        if (f2 > ((float) (this.u - i3))) {
            return (float) ((this.u - i2) + i4);
        }
        return (((f2 - ((float) i3)) / this.s) * this.s) + ((float) i4);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(LinearLayout linearLayout, int i2) {
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(linearLayout.getLayoutParams());
        marginLayoutParams.setMargins(i2, (int) (((float) this.v) * this.x), 0, 0);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(marginLayoutParams));
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int d(float f2) {
        return (int) ((((float) (this.p - 1)) * f2) / ((float) this.r));
    }

    private float c(int i2) {
        return a((float) (((this.A - this.z) / 2) + ((this.r * (i2 - 1)) / (this.p - 1))));
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int e(float f2) {
        float f3;
        int i2 = this.A / 2;
        if (f2 < ((float) i2)) {
            f3 = 0.0f;
        } else if (f2 > ((float) (this.u - i2))) {
            f3 = (float) (this.u - this.A);
        } else {
            f3 = f2 - ((float) i2);
        }
        return (int) f3;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(TextView textView, int i2) {
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(textView.getLayoutParams());
        if (textView.equals(this.g)) {
            marginLayoutParams.setMargins(i2, (int) (((float) this.v) * (this.w + this.x)), 0, 0);
        } else {
            marginLayoutParams.setMargins(i2, 0, 0, 0);
        }
        textView.setLayoutParams(new RelativeLayout.LayoutParams(marginLayoutParams));
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(LinearLayout linearLayout, int i2, int i3) {
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(linearLayout.getLayoutParams());
        marginLayoutParams.setMargins((this.z / 2) + i2, (int) (((float) this.v) * (((1.0f - this.w) - this.x) + ((this.w - this.y) / 2.0f))), 0, 0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginLayoutParams);
        layoutParams.width = i3 - i2;
        linearLayout.setLayoutParams(layoutParams);
    }

    public void a(int i2) {
        float c2 = c(i2 + 1);
        this.j = c(c2);
        a(this.d, (int) this.j);
        a(this.f, e(c2));
        this.f.setText("" + d(c2));
        a(this.i, (int) this.j, (int) this.k);
    }

    public void b(int i2) {
        float c2 = c(i2 + 1);
        this.k = c(c2);
        a(this.e, (int) this.k);
        a(this.g, e(c2));
        this.g.setText("" + d(c2));
        a(this.i, (int) this.j, (int) this.k);
    }

    public void a() {
        this.d.setBackgroundDrawable(null);
        this.d = null;
        this.l.setOnTouchListener(null);
        this.l = null;
        this.d = null;
        this.m = null;
    }
}
