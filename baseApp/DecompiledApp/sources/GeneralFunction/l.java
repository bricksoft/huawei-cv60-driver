package GeneralFunction;

import java.lang.reflect.Method;

public class l {

    /* renamed from: a  reason: collision with root package name */
    private static volatile Method f116a = null;
    private static volatile Method b = null;

    public static String a(String str, String str2) {
        try {
            if (b == null) {
                synchronized (l.class) {
                    if (b == null) {
                        b = Class.forName("android.os.SystemProperties").getDeclaredMethod("get", String.class, String.class);
                    }
                }
            }
            return (String) b.invoke(null, str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            return str2;
        }
    }
}
