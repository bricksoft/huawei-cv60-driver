package GeneralFunction;

import android.media.ExifInterface;
import java.io.IOException;

public class b {
    public static int a(String str) {
        return a(str, "ImageWidth");
    }

    public static int b(String str) {
        return a(str, "ImageLength");
    }

    private static int a(String str, String str2) {
        try {
            return new ExifInterface(str).getAttributeInt(str2, -1);
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
