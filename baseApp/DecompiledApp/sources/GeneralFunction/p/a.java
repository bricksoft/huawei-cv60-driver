package GeneralFunction.p;

import android.media.MediaExtractor;
import android.media.MediaFormat;
import tv.danmaku.ijk.media.player.misc.IMediaFormat;

public class a {
    public static MediaFormat a(MediaExtractor mediaExtractor, String str) {
        for (int i = 0; i < mediaExtractor.getTrackCount(); i++) {
            MediaFormat trackFormat = mediaExtractor.getTrackFormat(i);
            if (trackFormat.getString(IMediaFormat.KEY_MIME).startsWith(str)) {
                mediaExtractor.selectTrack(i);
                return trackFormat;
            }
        }
        return null;
    }
}
