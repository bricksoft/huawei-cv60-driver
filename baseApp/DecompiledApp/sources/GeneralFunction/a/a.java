package GeneralFunction.a;

import android.app.Activity;
import android.content.Context;
import android.os.Message;
import java.util.Locale;

public class a extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static Locale f62a = null;
    private int b = 0;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.b = 0;
    }

    public void onPause() {
        super.onPause();
        this.b = 1;
    }

    public void onStop() {
        super.onStop();
        this.b = 2;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.b = 3;
    }

    public int a() {
        return this.b;
    }

    public void a(int i) {
        a(i, 0);
    }

    public void a(int i, long j) {
    }

    public void a(a.c.a aVar) {
        a(aVar, 0);
    }

    public void a(a.c.a aVar, long j) {
    }

    public void a(Message message) {
    }

    public void b(Message message) {
    }

    public void b(int i) {
        b(i, 0);
    }

    public void b(int i, long j) {
    }

    public void b(a.c.a aVar) {
        b(aVar, 0);
    }

    public void b(a.c.a aVar, long j) {
    }

    public boolean b() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }
}
