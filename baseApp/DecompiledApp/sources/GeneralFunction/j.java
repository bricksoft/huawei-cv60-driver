package GeneralFunction;

import java.util.concurrent.Semaphore;

public class j {

    /* renamed from: a  reason: collision with root package name */
    Semaphore f104a = new Semaphore(1, true);

    public int a() {
        try {
            this.f104a.acquire();
            return 0;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void b() {
        this.f104a.release();
    }
}
