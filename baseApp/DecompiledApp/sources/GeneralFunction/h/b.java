package GeneralFunction.h;

import GeneralFunction.e.d;
import a.c.a;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private HandlerThread f100a = null;
    private Handler b = null;
    private a c = null;
    private UI_ModeMain d = null;
    private d e = null;
    private d f = null;
    private int g = 0;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i) {
        GeneralFunction.d.a("UI_FileControl", str, i);
    }

    public b(UI_ModeMain uI_ModeMain) {
        this.d = uI_ModeMain;
    }

    public void a(ui_Controller.b.b bVar) {
        this.c = new a();
        this.c.b = 0;
        this.c.d = new ArrayBlockingQueue(20);
        this.c.e = bVar;
        this.f100a = new HandlerThread("FileControlServer");
        this.f100a.start();
        this.b = a(this.f100a);
        this.e = bVar.b.f1320a;
        this.f = bVar.f1316a.f1332a;
    }

    public void a(a aVar, long j) {
        a(aVar.b(), j);
    }

    public void a(Message message, long j) {
        this.c.b++;
        this.b.sendMessageDelayed(message, j);
    }

    public boolean a() {
        if (this.c.b == 0) {
            return true;
        }
        return false;
    }

    public void b() {
        this.c.c = true;
        this.d.d(new a(12291));
    }

    public void c() {
        this.g = 0;
    }

    public int d() {
        return this.g;
    }

    public void a(ArrayList<ui_Controller.b.a> arrayList) {
        if (arrayList.size() == 0) {
            a("UI_ExcuteAction no get data", 4);
            return;
        }
        a aVar = new a(12290);
        aVar.a("ActionList", new a.C0010a(arrayList));
        this.d.d(aVar);
    }

    public boolean a(String str) {
        return new File(str).exists();
    }

    private Handler a(HandlerThread handlerThread) {
        return new Handler(handlerThread.getLooper()) {
            /* class GeneralFunction.h.b.AnonymousClass1 */

            public void handleMessage(Message message) {
                int i;
                boolean z;
                super.handleMessage(message);
                a aVar = new a(message);
                new ArrayList();
                b.this.a((b) ("File Server HandleMessage: 0x" + Integer.toHexString(message.what)), (String) 3);
                switch (message.what) {
                    case 12290:
                        if (!b.this.c.c) {
                            b.this.c.f98a = 5;
                            ArrayList arrayList = (ArrayList) aVar.f("ActionList").a();
                            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                                b.this.a((b) "======================", (String) 4);
                                b.this.a((b) ((ui_Controller.b.a) arrayList.get(i2)).a(), (String) 4);
                            }
                            ArrayList arrayList2 = new ArrayList();
                            ArrayList arrayList3 = new ArrayList();
                            int i3 = 0;
                            while (true) {
                                if (i3 < arrayList.size()) {
                                    if (b.this.c.c) {
                                        b.this.a((b) "Delete Cancel", (String) 4);
                                    } else {
                                        ui_Controller.b.a aVar2 = (ui_Controller.b.a) arrayList.get(i3);
                                        ArrayList<Integer> arrayList4 = aVar2.b;
                                        boolean z2 = false;
                                        if (arrayList4.size() == 0) {
                                            i = 1;
                                        } else {
                                            int size = arrayList4.size();
                                            b.this.a((b) ("ulRunCount " + size), (String) 4);
                                            i = size;
                                        }
                                        int i4 = 0;
                                        while (true) {
                                            if (i4 < i) {
                                                if (b.this.c.c) {
                                                    b.this.a((b) "Delete Cancel", (String) 4);
                                                } else {
                                                    int i5 = aVar2.f1315a;
                                                    int intValue = arrayList4.get(i4).intValue();
                                                    int a2 = b.this.e.a(i5, intValue);
                                                    arrayList2.add(b.this.e.h(i5, intValue));
                                                    arrayList3.add(Integer.valueOf(a2));
                                                    if (b.this.e.q(b.this.e.u(i5)) <= 1) {
                                                        z = true;
                                                    } else {
                                                        z = z2;
                                                    }
                                                    b.this.a((b) i5, intValue);
                                                    a aVar3 = new a(12323);
                                                    aVar3.a("parent index", i5);
                                                    aVar3.a("child index", intValue);
                                                    aVar3.a("delete group", z);
                                                    b.this.d.a(aVar3);
                                                    i4++;
                                                    z2 = z;
                                                }
                                            }
                                        }
                                        i3++;
                                    }
                                }
                            }
                            b.this.d.a(new a(12324));
                            for (int i6 = 0; i6 < arrayList3.size(); i6++) {
                                b.this.a((b) ((Integer) arrayList3.get(i6)).intValue(), (int) ((String) arrayList2.get(i6)));
                            }
                            break;
                        }
                        break;
                    case 12291:
                        b.this.a((b) "File server cancel complete", (String) 4);
                        b.this.c.c = false;
                        b.this.c.f98a = 0;
                        break;
                }
                a aVar4 = b.this.c;
                aVar4.b--;
                b.this.a((b) ("IsFileControllerIdle:" + b.this.a()), (String) 3);
                b.this.c.f98a = 0;
            }
        };
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i, int i2) {
        this.e.a();
        this.e.b();
        this.e.c();
        this.c.e.e.a(i, i2);
        this.d.x();
        switch (this.c.e.b.d) {
            case 1:
                this.e.a(this.c.e.e.a(), this.c.e.f);
                break;
            case 2:
                this.e.a(this.c.e.e.a(), this.c.e.g);
                break;
            case 3:
                this.e.a(this.c.e.e.a(), this.c.e.h);
                break;
        }
        a("Delete Update", 4);
        this.e.a();
        this.e.b();
        this.e.c();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i, String str) {
        GeneralFunction.g.a.a(str, this.d);
        if (i == 1 || i == 5) {
            String d2 = GeneralFunction.g.a.d(str);
            a("Delete File " + d2, 4);
            GeneralFunction.g.a.a(d2, this.d);
        }
    }

    public void a(Message message) {
        switch (message.what) {
            case 17924:
                a("MSG_REMOTE_EVENT_CAPTURE_DONE2", 4);
                if (this.g == 0) {
                    a("[CaptureDBG] NeedDLMainPicNum = 0, Start download the new captured pic", 2);
                    a aVar = new a(18465);
                    aVar.a("IsMiddleData", 0);
                    this.d.c(aVar);
                }
                this.g++;
                return;
            case 17933:
            default:
                return;
            case 18185:
                a aVar2 = new a(message);
                int x = GeneralFunction.m.a.x();
                int y = GeneralFunction.m.a.y();
                int c2 = this.c.e.e.c() + 1;
                File file = new File(aVar2.d("storage_path") + aVar2.d("storage_name"));
                int c3 = GeneralFunction.d.c(file.getAbsolutePath());
                if (GeneralFunction.m.a.a() == 1) {
                    this.c.e.e.a(aVar2.d("storage_name"), aVar2.d("storage_path"), file.length(), 0, c3, x, y, (long) 0, aVar2.c("current_time"), aVar2.c("current_time"), c2, 0, true, false, false, true, false, true, true, true, false);
                } else {
                    this.c.e.e.a(aVar2.d("storage_name"), aVar2.d("storage_path"), file.length(), 0, c3, x, y, (long) 0, aVar2.c("current_time"), aVar2.c("current_time"), c2, 0, true, false, false, true, false, true, true);
                }
                this.e.a();
                this.d.x();
                if (GeneralFunction.m.a.a() == 1) {
                    this.e.a(this.c.e.e.a(), this.c.e.g);
                } else {
                    this.e.a(this.c.e.e.a(), this.c.e.f);
                }
                this.g--;
                a("[CaptureDBG] NeedDLMainPicNum: " + this.g, 2);
                if (this.g > 0) {
                    a aVar3 = new a(18465);
                    aVar3.a("IsMiddleData", 0);
                    this.d.c(aVar3);
                    return;
                }
                this.d.a(8730, 0);
                return;
        }
    }
}
