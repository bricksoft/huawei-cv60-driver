package GeneralFunction.d;

import GeneralFunction.d;
import a.c.a;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Message;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private UI_ModeMain f72a = null;
    private b b = null;
    private String c = null;

    private static void a(String str, int i) {
        d.a("UI_ConnectionController", str, i);
    }

    public c(UI_ModeMain uI_ModeMain) {
        this.f72a = uI_ModeMain;
        this.b = new b();
        b();
    }

    public String a() {
        return this.c;
    }

    public boolean a(Message message) {
        String str;
        a aVar = new a(message);
        switch (message.what) {
            case 18432:
                if (aVar.b("result") == 0) {
                    a("[UsbRemote] USB Core init success", 2);
                    break;
                } else {
                    a("[UsbRemote] USB Core init fail!", 0);
                    break;
                }
            case 18433:
                a("[UsbRemote] USB deinit done", 2);
                break;
            case 18434:
                if (aVar.b("result") != 0) {
                    a("[UsbRemote] USB Core reset fail!", 0);
                    break;
                }
                break;
            case 18442:
                if (aVar.b("result") == 0) {
                    a("[UsbRemote] Start communication...", 2);
                    break;
                } else {
                    a("[UsbRemote] Start communication fail", 0);
                    break;
                }
            case 18443:
                if (aVar.b("result") == 0) {
                    a("[UsbRemote] Close communication...", 2);
                    break;
                } else {
                    a("[UsbRemote] Close communication fail", 0);
                    break;
                }
            case 18448:
                if (aVar.b("result") == 0) {
                    this.f72a.c(new a(18544));
                    break;
                } else {
                    a("[UsbRemote] reset camera fail!", 0);
                    break;
                }
            case 18449:
                if (aVar.b("result") == 0) {
                    a("[UsbRemote] start live view success", 2);
                    this.f72a.c(new a(18545));
                    break;
                } else {
                    a("[UsbRemote] start live view fail!", 0);
                    break;
                }
            case 18450:
                if (aVar.b("result") == 0) {
                    a("[UsbRemote] stop live view success", 2);
                    this.f72a.a(new a(8748));
                    this.f72a.c(new a(18546));
                    break;
                } else {
                    a("[UsbRemote] stop live view fail!", 0);
                    break;
                }
            case 18451:
                if (aVar.b("result") == 0) {
                    this.f72a.a(new a(8749));
                    this.f72a.c(new a(18547), 5);
                    break;
                } else {
                    a("[UsbRemote] start recording fail!", 0);
                    this.f72a.c(new a(17935));
                    break;
                }
            case 18452:
                if (aVar.b("result") == 0) {
                    this.f72a.a(new a(8749));
                    this.f72a.c(new a(18548), 5);
                    break;
                } else {
                    a("[UsbRemote] stop recording fail!", 0);
                    break;
                }
            case 18453:
                if (aVar.b("result") == 0) {
                    this.f72a.a(new a(8749));
                    this.f72a.c(new a(18549), 50);
                    break;
                } else {
                    a("[UsbRemote] take picture fail!", 0);
                    break;
                }
            case 18456:
                if (aVar.b("result") == 0) {
                    this.f72a.c(new a(18551), 5);
                    break;
                } else {
                    a("[UsbRemote] enter fw upgrade mode fail!", 0);
                    this.f72a.a(new a(9258));
                    break;
                }
            case 18457:
                if (aVar.b("result") == 0) {
                    this.f72a.c(new a(18552), 5);
                    break;
                } else {
                    a("[UsbRemote] examine FW binary fail", 0);
                    this.f72a.a(new a(9258));
                    break;
                }
            case 18458:
                if (aVar.b("result") == 0) {
                    this.f72a.c(new a(18553), 5);
                    break;
                } else {
                    a("[UsbRemote] start update camera fw fail", 0);
                    break;
                }
            case 18459:
                a("[UsbRemote] Clear camera picture buffer result: " + aVar.b("result"), 1);
                break;
            case 18464:
                if (aVar.b("result") == -1) {
                    a("[UsbRemote] get frame fail!", 0);
                    break;
                }
                break;
            case 18465:
                if (aVar.b("result") != 1) {
                    if (aVar.b("result") == 0) {
                        a.a.a.d dVar = (a.a.a.d) aVar.f("PictureData").a();
                        int i = dVar.f169a.f68a;
                        String str2 = "PIC_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date(aVar.c("CurrentTime"))) + ".jpg";
                        String a2 = d.a(this.f72a.c.b);
                        a("MSG_REMOTE_USB_CMD_READ_GET_PICTURE szSdPath: " + a2, 3);
                        if (GeneralFunction.m.a.a() == 1) {
                            str = a2 + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/";
                        } else {
                            str = ui_Controller.a.c.c;
                        }
                        this.c = str + str2;
                        FileOutputStream a3 = a(str, str2, false);
                        if (a3 != null) {
                            if (i != 0) {
                                try {
                                    a3.write(dVar.f169a.a(), 0, i);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e2) {
                                    e2.printStackTrace();
                                }
                            }
                            try {
                                a3.close();
                            } catch (FileNotFoundException e3) {
                                e3.printStackTrace();
                            } catch (IOException e4) {
                                e4.printStackTrace();
                            }
                            dVar.f169a.d();
                            if (aVar.b("IsDataEnd") != 0) {
                                this.c = null;
                                a aVar2 = new a(18185);
                                aVar2.a("storage_name", str2);
                                aVar2.a("storage_path", str);
                                aVar2.a(IjkMediaPlayer.OnNativeInvokeListener.ARG_FILE_SIZE, i);
                                aVar2.a("current_time", aVar.c("CurrentTime"));
                                this.f72a.a(aVar2);
                                break;
                            } else {
                                a aVar3 = new a(18465);
                                aVar3.a("IsMiddleData", 1);
                                aVar3.a("CurrentTime", aVar.c("CurrentTime"));
                                this.f72a.c(aVar3);
                                break;
                            }
                        } else {
                            a("[UsbRemote] create saving file fail", 0);
                            dVar.f169a.d();
                            break;
                        }
                    }
                } else {
                    a aVar4 = new a(18465);
                    aVar4.a("IsMiddleData", 0);
                    this.f72a.c(aVar4, 5);
                    break;
                }
                break;
            case 18466:
                if (aVar.b("result") != 1) {
                    if (aVar.b("result") == 0) {
                        a.a.a.d dVar2 = (a.a.a.d) aVar.f("ThumbnailData").a();
                        int i2 = dVar2.f169a.f68a;
                        String str3 = ui_Controller.a.c.f;
                        a("[CaptureDBG] Get thumb", 1);
                        FileOutputStream a4 = a(str3, "QuickView.jpg", true);
                        if (a4 != null) {
                            try {
                                a4.write(dVar2.f169a.a(), 0, i2);
                            } catch (FileNotFoundException e5) {
                                e5.printStackTrace();
                            } catch (IOException e6) {
                                e6.printStackTrace();
                            }
                            try {
                                a4.close();
                            } catch (FileNotFoundException e7) {
                                e7.printStackTrace();
                            } catch (IOException e8) {
                                e8.printStackTrace();
                            }
                            dVar2.f169a.d();
                            this.f72a.a(new a(17924));
                            break;
                        } else {
                            a("[UsbRemote] create saving file fail", 0);
                            dVar2.f169a.d();
                            break;
                        }
                    }
                } else {
                    this.f72a.c(new a(18466), 5);
                    break;
                }
                break;
            case 18480:
                if (aVar.b("result") == 0) {
                    a("[UsbRemote] FWVersion: " + aVar.d("fw_version"), 0);
                    break;
                } else {
                    a("[UsbRemote] get fw version fail", 0);
                    this.f72a.a(8733, 0);
                    break;
                }
            case 18483:
                a("[UsbRemote] Camera remained picture number: " + aVar.b("remained_pic_num"), 1);
                break;
            case 18484:
                if (aVar.b("result") == 0) {
                    a("[UsbRemote] SCSI Version: " + aVar.d("scsi_version"), 0);
                    break;
                }
                break;
            case 18496:
                if (aVar.b("result") == 0) {
                    a("[UsbRemote] Get photo resolution: " + aVar.b("photo_resolution"), 0);
                    break;
                } else {
                    a("[UsbRemote] get photo resolution fail", 0);
                    break;
                }
            case 18547:
                if (aVar.b("result") != 0) {
                    if (aVar.b("result") != 1) {
                        a("[UsbRemote] check start recording status: fail", 0);
                        this.f72a.c(new a(17935));
                        break;
                    } else {
                        this.f72a.c(new a(18547), 5);
                        break;
                    }
                } else {
                    this.f72a.a(new a(17934));
                    break;
                }
            case 18548:
                if (aVar.b("result") != 0) {
                    if (aVar.b("result") != 1) {
                        a("[UsbRemote] check stop recording status: fail", 0);
                        break;
                    } else {
                        this.f72a.c(new a(18548), 5);
                        break;
                    }
                } else {
                    this.f72a.a(new a(17967));
                    break;
                }
            case 18549:
                if (aVar.a("play_shutter_sound")) {
                    this.f72a.a(new a(8729));
                }
                if (aVar.b("result") != 0) {
                    if (aVar.b("result") != 1) {
                        if (aVar.b("result") != 4) {
                            a("[UsbRemote] check take picture status: fail", 0);
                            break;
                        } else {
                            a("[CaptureDBG] Camera capture done", 1);
                            this.f72a.a(8710, 0);
                            break;
                        }
                    } else {
                        this.f72a.c(new a(18549), 5);
                        break;
                    }
                } else {
                    this.f72a.c(new a(18466));
                    this.f72a.c(new a(18549), 50);
                    break;
                }
        }
        return false;
    }

    private void b() {
        try {
            ZipFile zipFile = new ZipFile(this.f72a.getPackageManager().getApplicationInfo(this.f72a.getPackageName(), 0).sourceDir);
            ZipEntry entry = zipFile.getEntry("classes.dex");
            long j = 0;
            if (entry != null) {
                j = entry.getTime();
            }
            a("app time:" + SimpleDateFormat.getInstance().format(new Date(j)), 0);
            zipFile.close();
            this.b.f71a.b = Build.VERSION.RELEASE;
            this.b.f71a.c = Build.VERSION.SDK;
            this.b.f71a.d = this.f72a.getPackageManager().getPackageInfo(this.f72a.getPackageName(), 0).versionName;
            this.b.f71a.f70a = this.f72a.getPackageManager().getPackageInfo(this.f72a.getPackageName(), 0).versionCode;
            String str = "";
            String[] split = this.b.f71a.b.split("\\.");
            for (int i = 0; i < split.length; i++) {
                str = str + split[i];
            }
            this.b.f71a.e += "0" + str;
            a("appname = " + this.b.f71a.d + " || lAppCode = " + this.b.f71a.f70a, 3);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            a("getPackageInfo NameNotFoundException", 3);
        } catch (IOException e2) {
            e2.printStackTrace();
            a("getPackageInfo IOException", 3);
        }
    }

    private FileOutputStream a(String str, String str2, boolean z) {
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(str + str2);
        if (!file2.exists()) {
            try {
                file2.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (z) {
            file2.delete();
            try {
                file2.createNewFile();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        try {
            return new FileOutputStream(str + str2, true);
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            return null;
        }
    }
}
