package GeneralFunction.e;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class g extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private static int f80a = 0;
    private static String b = "Default";

    public g(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, i);
        f80a = i;
        b = str;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        String str = "";
        for (int i = 1; i < f.f78a.length; i++) {
            str = str + ", " + f.f78a[i].f79a + " " + f.f78a[i].b;
        }
        sQLiteDatabase.execSQL(a("PhonePicture_INF", str));
        sQLiteDatabase.execSQL(a("CameraPicture_INF", str));
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    private String a(String str, String str2) {
        if (str.isEmpty() || str2.isEmpty()) {
            return "";
        }
        return "CREATE TABLE IF NOT EXISTS " + str + "( _id INTEGER PRIMARY KEY AUTOINCREMENT" + str2 + ");";
    }
}
