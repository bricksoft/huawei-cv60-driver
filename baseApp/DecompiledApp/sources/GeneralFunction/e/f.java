package GeneralFunction.e;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public static final a[] f78a = {new a("_id", "INT"), new a("FileName", "TEXT"), new a("FilePath", "TEXT"), new a("FileSize", "INT"), new a("FileType", "INT"), new a("Orientation", "INT"), new a("Width", "INT"), new a("Height", "INT"), new a("VideoDurationTime", "INT"), new a("CaptureTime", "INT"), new a("DownloadTime", "INT"), new a("GroupIndex", "INT"), new a("FileIndexInGroup", "INT"), new a("HaveTN", "INT"), new a("HaveLRG", "INT"), new a("HaveQuickView", "INT"), new a("IsCaptureDuringAppON", "INT"), new a("IsFileProtected", "INT"), new a("Is360File", "INT"), new a("Have360Stitch", "INT"), new a("Reserved", "TEXT"), new a("IsSDCard", "INT"), new a("IsMyAlbum", "INT")};
    public static final a[] b = {new a("_id", "INT"), new a("FileName", "TEXT"), new a("FilePath", "TEXT"), new a("FileSize", "INT"), new a("FileType", "INT"), new a("Orientation", "INT"), new a("Width", "INT"), new a("Height", "INT"), new a("VideoDurationTime", "INT"), new a("CaptureTime", "INT"), new a("DownloadTime", "INT"), new a("GroupIndex", "INT"), new a("FileIndexInGroup", "INT"), new a("Is360File", "INT"), new a("Have360Stitch", "INT"), new a("IsSDCard", "INT"), new a("IsMyAlbum", "INT")};
    private static long c = -1;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f79a = null;
        public String b = null;

        public a(String str, String str2) {
            this.f79a = str;
            this.b = str2;
        }
    }

    public String a(boolean z) {
        if (z) {
            return " AND CaptureTime>=" + c("" + c);
        }
        return "CaptureTime>=" + c("" + c);
    }

    public void a(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, long j, int i, int i2, int i3, int i4, long j2, long j3, long j4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("FileName", str2);
        contentValues.put("FilePath", str3);
        contentValues.put("FileSize", Long.valueOf(j));
        contentValues.put("FileType", Integer.valueOf(i));
        contentValues.put("Orientation", Integer.valueOf(i2));
        contentValues.put("Width", Integer.valueOf(i3));
        contentValues.put("Height", Integer.valueOf(i4));
        contentValues.put("VideoDurationTime", Long.valueOf(j2));
        contentValues.put("CaptureTime", Long.valueOf(j3));
        contentValues.put("DownloadTime", Long.valueOf(j4));
        contentValues.put("GroupIndex", Integer.valueOf(i5));
        contentValues.put("FileIndexInGroup", Integer.valueOf(i6));
        contentValues.put("HaveTN", Integer.valueOf(i7));
        contentValues.put("HaveLRG", Integer.valueOf(i8));
        contentValues.put("HaveQuickView", Integer.valueOf(i9));
        contentValues.put("IsCaptureDuringAppON", Integer.valueOf(i10));
        contentValues.put("IsFileProtected", Integer.valueOf(i11));
        contentValues.put("Is360File", Integer.valueOf(i12));
        contentValues.put("Have360Stitch", Integer.valueOf(i13));
        contentValues.put("Reserved", "");
        contentValues.put("IsSDCard", Integer.valueOf(i14));
        contentValues.put("IsMyAlbum", Integer.valueOf(i15));
        sQLiteDatabase.insert(str, null, contentValues);
    }

    public long a(SQLiteDatabase sQLiteDatabase, String str) {
        int i;
        Cursor query = sQLiteDatabase.query(str, new String[]{"CaptureTime", "FileName"}, null, null, null, null, "CaptureTime ASC");
        int count = query.getCount();
        if (5000 >= count) {
            i = 0;
        } else {
            i = count - 5000;
        }
        if (i == 0) {
            c = 0;
            return 0;
        }
        query.moveToPosition(i);
        long j = query.getLong(query.getColumnIndex("CaptureTime"));
        query.close();
        c = j;
        return j;
    }

    public ArrayList<String> a(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, int i, int i2) {
        ArrayList<String> arrayList = new ArrayList<>();
        Cursor query = sQLiteDatabase.query(str, new String[]{str2}, "IsSDCard = ? AND IsMyAlbum = ? AND GroupIndex >= ? AND GroupIndex <= ?", new String[]{str3, "0", c("" + i), c("" + i2)}, null, null, null);
        if (query.moveToFirst()) {
            int i3 = 0;
            while (!query.isAfterLast()) {
                arrayList.add(query.getString(query.getColumnIndex(str2)));
                query.moveToNext();
                i3++;
            }
        }
        query.close();
        return arrayList;
    }

    public ArrayList<String> b(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, int i, int i2) {
        ArrayList<String> arrayList = new ArrayList<>();
        Cursor query = sQLiteDatabase.query(str, new String[]{str2}, "IsMyAlbum = ? AND GroupIndex >= ? AND GroupIndex <= ?", new String[]{str3, c("" + i), c("" + i2)}, null, null, null);
        if (query.moveToFirst()) {
            int i3 = 0;
            while (!query.isAfterLast()) {
                arrayList.add(query.getString(query.getColumnIndex(str2)));
                query.moveToNext();
                i3++;
            }
        }
        query.close();
        return arrayList;
    }

    public int b(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor query = sQLiteDatabase.query(str, new String[]{b("GroupIndex")}, a(false), null, null, null, null);
        int i = 0;
        while (query.moveToNext()) {
            i = query.getInt(query.getColumnIndex(b("GroupIndex")));
        }
        query.close();
        return i;
    }

    public Cursor a(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3) {
        return sQLiteDatabase.query(str, new String[]{"GroupIndex", a("FileIndexInGroup"), "IsSDCard", "IsMyAlbum"}, a(false), null, "GroupIndex", a("FileIndexInGroup") + " >=0", str2 + " " + str3);
    }

    public Cursor a(SQLiteDatabase sQLiteDatabase, String str, String str2) {
        return sQLiteDatabase.query(str, a(), a(false), null, null, null, "GroupIndex " + str2 + "," + "FileIndexInGroup" + " " + str2);
    }

    public void a(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String str4, String str5) {
        sQLiteDatabase.delete(str, str2 + "=" + c(str3) + " AND " + str4 + "=" + c(str5), null);
    }

    public boolean b(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String str4, String str5) {
        boolean z;
        Cursor query = sQLiteDatabase.query(str, new String[]{"_id"}, str2 + "=" + c(str3) + " AND " + str4 + "=" + c(str5), null, null, null, null);
        if (query.getCount() > 0) {
            z = true;
        } else {
            z = false;
        }
        query.close();
        return z;
    }

    public void a(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        ContentValues contentValues = new ContentValues();
        if (d(str7)) {
            contentValues.put(str6, Integer.valueOf(g(str7)));
        } else {
            contentValues.put(str6, str7);
        }
        sQLiteDatabase.update(str, contentValues, str2 + " = " + c(str3) + " AND " + str4 + " = " + c(str5), null);
    }

    private static String[] a() {
        String[] strArr = new String[b.length];
        for (int i = 0; i < b.length; i++) {
            strArr[i] = b[i].f79a;
        }
        return strArr;
    }

    public static String a(String str) {
        return "MIN(" + str + ")";
    }

    public static String b(String str) {
        return "MAX(" + str + ")";
    }

    private String c(String str) {
        if (e(str)) {
            return "" + str;
        }
        if (f(str)) {
            return "" + h(str);
        }
        return "'" + str + "'";
    }

    private boolean d(String str) {
        if (e(str) || f(str)) {
            return true;
        }
        return false;
    }

    private boolean e(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean f(String str) {
        if (str.toLowerCase().equals("true") || str.toLowerCase().equals("false")) {
            return true;
        }
        return false;
    }

    private int g(String str) {
        if (e(str)) {
            return Integer.parseInt(str);
        }
        return h(str);
    }

    private int h(String str) {
        if (str.toLowerCase().equals("true")) {
            return 1;
        }
        return 0;
    }
}
