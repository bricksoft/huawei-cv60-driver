package GeneralFunction.e;

import java.util.ArrayList;
import java.util.HashMap;

public class b implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<a> f74a = new ArrayList<>();
    public HashMap<Integer, c> b = new HashMap<>();

    public void a() {
        this.f74a.clear();
        this.b.clear();
    }

    @Override // java.lang.Object
    public Object clone() {
        b bVar = new b();
        bVar.f74a.addAll(this.f74a);
        bVar.b.putAll(this.b);
        return bVar;
    }
}
