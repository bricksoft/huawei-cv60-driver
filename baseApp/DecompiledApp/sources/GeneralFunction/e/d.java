package GeneralFunction.e;

import GeneralFunction.g.a;
import GeneralFunction.j;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.HashMap;
import ui_Controller.a.c;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private Cursor f76a = null;
    private ArrayList<a> b;
    private int c = 0;
    private HashMap<Integer, c> d;
    private final j e = new j();

    private void a(String str, int i) {
        GeneralFunction.d.a(getClass().getSimpleName(), str, i);
    }

    public void a() {
    }

    public void b() {
    }

    public void c() {
    }

    public void a(Cursor cursor, b bVar, int i) {
        this.e.a();
        this.f76a = cursor;
        b bVar2 = (b) bVar.clone();
        this.b = bVar2.f74a;
        this.d = bVar2.b;
        this.c = i;
        this.e.b();
    }

    public void a(Cursor cursor, b bVar) {
        this.e.a();
        this.f76a = cursor;
        b bVar2 = (b) bVar.clone();
        this.b = bVar2.f74a;
        this.d = bVar2.b;
        this.e.b();
    }

    public b d() {
        this.e.a();
        b bVar = new b();
        bVar.f74a = this.b;
        bVar.b = this.d;
        this.e.b();
        return bVar;
    }

    private int w(int i) {
        if (this.f76a.moveToPosition(i)) {
            return Integer.parseInt(this.f76a.getString(this.f76a.getColumnIndex("FileType")));
        }
        a("GetFileType lPosition Error " + i, 4);
        return -1;
    }

    public int a(int i, int i2) {
        this.e.a();
        int w = w(j(i, i2));
        this.e.b();
        return w;
    }

    public int a(int i) {
        this.e.a();
        int w = w(O(i));
        this.e.b();
        return w;
    }

    private int x(int i) {
        if (this.f76a.moveToPosition(i)) {
            return Integer.parseInt(this.f76a.getString(this.f76a.getColumnIndex("Width")));
        }
        a("GetWidth lPosition Error " + i, 4);
        return -1;
    }

    public int b(int i, int i2) {
        this.e.a();
        int x = x(j(i, i2));
        this.e.b();
        return x;
    }

    public int b(int i) {
        this.e.a();
        int x = x(O(i));
        this.e.b();
        return x;
    }

    private int y(int i) {
        if (this.f76a.moveToPosition(i)) {
            return Integer.parseInt(this.f76a.getString(this.f76a.getColumnIndex("Height")));
        }
        a("GetHeight lPosition Error " + i, 4);
        return -1;
    }

    public int c(int i, int i2) {
        this.e.a();
        int y = y(j(i, i2));
        this.e.b();
        return y;
    }

    public int c(int i) {
        this.e.a();
        int y = y(O(i));
        this.e.b();
        return y;
    }

    private int z(int i) {
        if (this.f76a.moveToPosition(i)) {
            return Integer.parseInt(this.f76a.getString(this.f76a.getColumnIndex("VideoDurationTime")));
        }
        a("GetVideoDurationTime lPosition Error " + i, 4);
        return -1;
    }

    public int d(int i, int i2) {
        this.e.a();
        int z = z(j(i, i2));
        this.e.b();
        return z;
    }

    public int d(int i) {
        this.e.a();
        int z = z(O(i));
        this.e.b();
        return z;
    }

    private long A(int i) {
        if (this.f76a.moveToPosition(i)) {
            return Long.parseLong(this.f76a.getString(this.f76a.getColumnIndex("FileSize")));
        }
        a("GetFileSize lPosition Error " + i, 4);
        return -1;
    }

    public long e(int i) {
        this.e.a();
        long A = A(O(i));
        this.e.b();
        return A;
    }

    public long f(int i) {
        this.e.a();
        long j = 0;
        int i2 = 0;
        while (true) {
            if (i2 >= this.b.get(i).c.size()) {
                break;
            }
            int i3 = this.b.get(i).f73a + i2;
            if (!this.f76a.moveToPosition(i3)) {
                a("GetGroupTotalSize lPosition Error " + i3, 4);
                j = -1;
                break;
            }
            j += Long.parseLong(this.f76a.getString(this.f76a.getColumnIndex("FileSize")));
            i2++;
        }
        this.e.b();
        return j;
    }

    private long B(int i) {
        if (this.f76a.moveToPosition(i)) {
            return Long.parseLong(this.f76a.getString(this.f76a.getColumnIndex("CaptureTime")));
        }
        a("GetCaptureTime lPosition Error " + i, 4);
        return -1;
    }

    public long g(int i) {
        this.e.a();
        long B = B(O(i));
        this.e.b();
        return B;
    }

    private long C(int i) {
        if (this.f76a.moveToPosition(i)) {
            return Long.parseLong(this.f76a.getString(this.f76a.getColumnIndex("DownloadTime")));
        }
        a("GetDownloadTime lPosition Error " + i, 4);
        return -1;
    }

    public long h(int i) {
        this.e.a();
        long C = C(O(i));
        this.e.b();
        return C;
    }

    private boolean D(int i) {
        return false;
    }

    private boolean E(int i) {
        return false;
    }

    private boolean F(int i) {
        if (!this.f76a.moveToPosition(i)) {
            a("Is360File lPosition Error " + i, 4);
            return false;
        } else if (this.f76a.getInt(this.f76a.getColumnIndex("Is360File")) != 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean i(int i) {
        this.e.a();
        boolean F = F(O(i));
        this.e.b();
        return F;
    }

    private boolean G(int i) {
        if (!this.f76a.moveToPosition(i)) {
            a("Have360Stitch lPosition Error " + i, 4);
            return false;
        } else if (this.f76a.getInt(this.f76a.getColumnIndex("Have360Stitch")) != 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean j(int i) {
        this.e.a();
        boolean G = G(O(i));
        this.e.b();
        return G;
    }

    private boolean H(int i) {
        switch (w(i)) {
            case 0:
            case 3:
            case 4:
                return true;
            case 1:
            default:
                return false;
            case 2:
                return N(i).toLowerCase().equals("jpg");
        }
    }

    public boolean k(int i) {
        this.e.a();
        boolean H = H(O(i));
        this.e.b();
        return H;
    }

    private boolean I(int i) {
        switch (w(i)) {
            case 1:
            case 5:
                return true;
            case 2:
                return !N(i).toLowerCase().equals("jpg");
            case 3:
            case 4:
            default:
                return false;
        }
    }

    public boolean l(int i) {
        this.e.a();
        boolean I = I(O(i));
        this.e.b();
        return I;
    }

    private String J(int i) {
        if (this.c == 1) {
            return K(i);
        }
        if (!D(i)) {
            return null;
        }
        String M = M(i);
        String L = L(i);
        if (M == null || L == null) {
            return null;
        }
        String str = c.g + M.substring(M.lastIndexOf("DCIM/") + 5, M.lastIndexOf("/")) + "/";
        if (I(i)) {
            return str + a.b(L, "thm");
        }
        return str + L;
    }

    public String e(int i, int i2) {
        this.e.a();
        String J = J(j(i, i2));
        this.e.b();
        return J;
    }

    public String m(int i) {
        this.e.a();
        String J = J(O(i));
        this.e.b();
        return J;
    }

    private String K(int i) {
        if (this.c == 1) {
            if (!I(i)) {
                return M(i);
            }
            String M = M(i);
            if (M == null) {
                return null;
            }
            return a.d(M);
        } else if (I(i)) {
            return J(i);
        } else {
            if (!E(i)) {
                return null;
            }
            String M2 = M(i);
            String L = L(i);
            if (M2 == null || L == null) {
                return null;
            }
            return (c.h + M2.substring(M2.lastIndexOf("DCIM/") + 5, M2.lastIndexOf("/")) + "/") + L;
        }
    }

    public String f(int i, int i2) {
        this.e.a();
        String K = K(j(i, i2));
        this.e.b();
        return K;
    }

    private String L(int i) {
        if (this.f76a.moveToPosition(i)) {
            return this.f76a.getString(this.f76a.getColumnIndex("FileName"));
        }
        a("GetFileName lPosition Error " + i, 4);
        return null;
    }

    public String g(int i, int i2) {
        this.e.a();
        String L = L(j(i, i2));
        this.e.b();
        return L;
    }

    public String n(int i) {
        this.e.a();
        String L = L(O(i));
        this.e.b();
        return L;
    }

    private String M(int i) {
        if (!this.f76a.moveToPosition(i)) {
            a("GetFullPath lPosition Error " + i, 0);
            return null;
        }
        String string = this.f76a.getString(this.f76a.getColumnIndex("FilePath"));
        return string + this.f76a.getString(this.f76a.getColumnIndex("FileName"));
    }

    public String h(int i, int i2) {
        this.e.a();
        String M = M(j(i, i2));
        this.e.b();
        return M;
    }

    public String o(int i) {
        this.e.a();
        String M = M(O(i));
        this.e.b();
        return M;
    }

    private String N(int i) {
        String L = L(i);
        if (L != null) {
            return L.substring(L.lastIndexOf(46) + 1);
        }
        return L;
    }

    public int e() {
        return this.b.size();
    }

    public boolean p(int i) {
        if (i >= this.b.size() || i < 0) {
            return false;
        }
        return true;
    }

    public int q(int i) {
        if (P(i)) {
            return -1;
        }
        return this.b.get(i).c.size();
    }

    public ArrayList<a> f() {
        return this.b;
    }

    public a r(int i) {
        if (P(i)) {
            return null;
        }
        return this.b.get(i);
    }

    private int j(int i, int i2) {
        int v = v(i);
        if (v == -1) {
            return -1;
        }
        while (v < this.b.size()) {
            if (this.b.get(v).b == i) {
                ArrayList<Integer> arrayList = this.b.get(v).c;
                for (int i3 = 0; i3 < arrayList.size(); i3++) {
                    if (arrayList.get(i3).intValue() == i2) {
                        return this.b.get(v).f73a + i3;
                    }
                }
                continue;
            }
            v++;
        }
        a("Error: Not Found In Table " + i + " " + i2, 4);
        return -1;
    }

    private int O(int i) {
        if (P(i)) {
            return -1;
        }
        return this.b.get(i).f73a;
    }

    public int s(int i) {
        if (P(i)) {
            return -1;
        }
        return this.b.get(i).b;
    }

    public int g() {
        int i = -1;
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            if (this.b.get(i2).b > i) {
                i = this.b.get(i2).b;
            }
        }
        return i;
    }

    public int t(int i) {
        if (P(i)) {
            return -1;
        }
        if (this.b.get(i).c.size() == 0) {
            return -1;
        }
        return this.b.get(i).c.get(0).intValue();
    }

    public int i(int i, int i2) {
        if (!P(i) && i2 < q(i)) {
            return this.b.get(i).c.get(i2).intValue();
        }
        return -1;
    }

    public int u(int i) {
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            if (this.b.get(i2).b == i) {
                return i2;
            }
        }
        a("LinkTable_GetLinkIndex error", 4);
        return -1;
    }

    private boolean P(int i) {
        if (i < this.b.size() && i >= 0) {
            return false;
        }
        a("Error: LinkTable Over Size", 4);
        return true;
    }

    public int v(int i) {
        c cVar = this.d.get(Integer.valueOf(i));
        if (cVar != null) {
            return cVar.f75a;
        }
        a("Error: GroupIndexMap_GetLinkIndex no mapping " + i, 4);
        return -1;
    }
}
