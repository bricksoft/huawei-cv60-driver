package GeneralFunction.e;

import GeneralFunction.d;
import GeneralFunction.j;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private Cursor f77a = null;
    private g b;
    private SQLiteDatabase c;
    private final j d = new j();

    private void a(String str, int i) {
        d.a(getClass().getSimpleName(), str, i);
    }

    public e(Context context) {
        this.d.a();
        a(context);
        this.c = this.b.getWritableDatabase();
        this.d.b();
    }

    private void a(Context context) {
        this.b = new g(context, "PICO2nd_DB", null, 1);
    }

    public Cursor a() {
        this.d.a();
        this.d.b();
        return this.f77a;
    }

    private void a(b bVar, int i) {
        a aVar = new a();
        aVar.b = i;
        bVar.f74a.add(aVar);
        c cVar = new c();
        cVar.f75a = bVar.f74a.size() - 1;
        bVar.b.put(Integer.valueOf(i), cVar);
    }

    private int a(b bVar, int i, int i2, int i3) {
        c cVar = bVar.b.get(Integer.valueOf(i));
        if (cVar == null) {
            a("Error: Group Map Get Null " + i, 4);
            this.d.b();
            return -1;
        }
        a aVar = bVar.f74a.get(cVar.f75a);
        if (aVar.f73a == -1) {
            aVar.f73a = i3;
            aVar.c.add(Integer.valueOf(i2));
        } else {
            aVar.c.add(Integer.valueOf(i2));
        }
        return 0;
    }

    public int a(int i, b bVar, b bVar2, b bVar3) {
        Cursor a2;
        this.d.a();
        long currentTimeMillis = System.currentTimeMillis();
        f fVar = new f();
        fVar.a(this.c, "PhonePicture_INF");
        this.f77a = fVar.a(this.c, "PhonePicture_INF", "ASC");
        if (this.f77a == null) {
            a("Error: LocalAllFileCursor is not init", 0);
            this.d.b();
            return -1;
        }
        Cursor cursor = this.f77a;
        if (i == 1) {
            a2 = fVar.a(this.c, "PhonePicture_INF", "DownloadTime", "DESC");
        } else {
            a2 = fVar.a(this.c, "PhonePicture_INF", "DownloadTime", "ASC");
        }
        bVar.a();
        bVar2.a();
        bVar3.a();
        if (!a2.moveToFirst()) {
            a("Error: Database Group Empty", 0);
            this.d.b();
            return -2;
        }
        int i2 = 0;
        int columnIndex = a2.getColumnIndex("GroupIndex");
        int columnIndex2 = a2.getColumnIndex("IsSDCard");
        int columnIndex3 = a2.getColumnIndex("IsMyAlbum");
        do {
            int parseInt = Integer.parseInt(a2.getString(columnIndex));
            a("GroupCursor[" + i2 + "]lGroupIndex:" + parseInt + " " + a2.getInt(columnIndex2) + " " + a2.getInt(columnIndex3), 4);
            if (a2.getInt(columnIndex2) == 1) {
                a(bVar2, parseInt);
            } else if (a2.getInt(columnIndex3) == 1) {
                a(bVar3, parseInt);
            } else {
                a(bVar, parseInt);
            }
            i2++;
        } while (a2.moveToNext());
        if (!cursor.moveToFirst()) {
            a("Error: Database Empty", 0);
            bVar.a();
            bVar2.a();
            bVar3.a();
            this.d.b();
            return -3;
        }
        int i3 = 0;
        int columnIndex4 = cursor.getColumnIndex("GroupIndex");
        int columnIndex5 = cursor.getColumnIndex("FileIndexInGroup");
        int columnIndex6 = cursor.getColumnIndex("IsSDCard");
        int columnIndex7 = cursor.getColumnIndex("IsMyAlbum");
        do {
            int parseInt2 = Integer.parseInt(cursor.getString(columnIndex4));
            int parseInt3 = Integer.parseInt(cursor.getString(columnIndex5));
            a("AllFileCursor " + parseInt2 + " " + parseInt3, 4);
            if (cursor.getInt(columnIndex6) == 1) {
                a(bVar2, parseInt2, parseInt3, i3);
            } else if (cursor.getInt(columnIndex7) == 1) {
                a(bVar3, parseInt2, parseInt3, i3);
            } else {
                a(bVar, parseInt2, parseInt3, i3);
            }
            i3++;
        } while (cursor.moveToNext());
        a("GetLinkList cost time: " + (System.currentTimeMillis() - currentTimeMillis), 4);
        this.d.b();
        return 0;
    }

    public void b() {
        this.d.a();
        f fVar = new f();
        fVar.a(this.c, "PhonePicture_INF");
        this.f77a = fVar.a(this.c, "PhonePicture_INF", "ASC");
        this.d.b();
    }

    public void a(String str, String str2, long j, int i, int i2, int i3, int i4, long j2, long j3, long j4, int i5, int i6, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        a(str, str2, j, i, i2, i3, i4, j2, j3, j4, i5, i6, z, z2, z3, z4, z5, z6, z7, false, false);
    }

    public void a(String str, String str2, long j, int i, int i2, int i3, int i4, long j2, long j3, long j4, int i5, int i6, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9) {
        this.d.a();
        long currentTimeMillis = System.currentTimeMillis();
        new f().a(this.c, "PhonePicture_INF", str, str2, j, i, i2, i3, i4, j2, j3, j4, i5, i6, z ? 1 : 0, z2 ? 1 : 0, z3 ? 1 : 0, z4 ? 1 : 0, z5 ? 1 : 0, z6 ? 1 : 0, z7 ? 1 : 0, z8 ? 1 : 0, z9 ? 1 : 0);
        a("Local_AddToTable cost time: " + (System.currentTimeMillis() - currentTimeMillis), 4);
        this.d.b();
    }

    public void a(int i, int i2) {
        this.d.a();
        long currentTimeMillis = System.currentTimeMillis();
        new f().a(this.c, "PhonePicture_INF", "GroupIndex", "" + i, "FileIndexInGroup", "" + i2);
        a("Local_DeleteFile cost time: " + (System.currentTimeMillis() - currentTimeMillis), 4);
        this.d.b();
    }

    public void a(String str, String str2) {
        this.d.a();
        long currentTimeMillis = System.currentTimeMillis();
        new f().a(this.c, "PhonePicture_INF", "FileName", str, "FileSize", str2);
        a("Local_DeleteFile cost time: " + (System.currentTimeMillis() - currentTimeMillis), 4);
        this.d.b();
    }

    public boolean b(String str, String str2) {
        this.d.a();
        boolean b2 = new f().b(this.c, "PhonePicture_INF", "FilePath", str, "FileName", str2);
        this.d.b();
        return b2;
    }

    public int c() {
        this.d.a();
        int b2 = new f().b(this.c, "PhonePicture_INF");
        this.d.b();
        return b2;
    }

    public ArrayList<String> a(String str, boolean z, int i, int i2) {
        new ArrayList();
        this.d.a();
        ArrayList<String> a2 = new f().a(this.c, "PhonePicture_INF", str, Integer.toString(z ? 1 : 0), i, i2);
        this.d.b();
        return a2;
    }

    public ArrayList<String> a(String str, int i, int i2) {
        new ArrayList();
        this.d.a();
        ArrayList<String> b2 = new f().b(this.c, "PhonePicture_INF", str, "1", i, i2);
        this.d.b();
        return b2;
    }

    public void a(int i, int i2, boolean z, String str) {
        this.d.a();
        f fVar = new f();
        fVar.a(this.c, "PhonePicture_INF", "GroupIndex", Integer.toString(i), "FileIndexInGroup", Integer.toString(i2), "FilePath", str);
        fVar.a(this.c, "PhonePicture_INF", "GroupIndex", Integer.toString(i), "FileIndexInGroup", Integer.toString(i2), "IsSDCard", Integer.toString(z ? 1 : 0));
        this.d.b();
    }

    public void a(int i, int i2, long j) {
        this.d.a();
        new f().a(this.c, "PhonePicture_INF", "GroupIndex", Integer.toString(i), "FileIndexInGroup", Integer.toString(i2), "FileSize", Long.toString(j));
        this.d.b();
    }

    public static b a(ArrayList<a> arrayList, int i) {
        b bVar = new b();
        int i2 = arrayList.get(i).b;
        for (int i3 = 0; i3 < arrayList.get(i).c.size(); i3++) {
            int intValue = arrayList.get(i).c.get(i3).intValue();
            a aVar = new a();
            aVar.f73a = arrayList.get(i).f73a + i3;
            aVar.b = i2;
            aVar.c.add(Integer.valueOf(intValue));
            bVar.f74a.add(aVar);
        }
        c cVar = new c();
        cVar.f75a = 0;
        bVar.b.put(Integer.valueOf(i2), cVar);
        return bVar;
    }
}
