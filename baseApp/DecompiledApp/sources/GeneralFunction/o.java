package GeneralFunction;

import GeneralFunction.a.a;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import java.util.ArrayList;

public class o {

    /* renamed from: a  reason: collision with root package name */
    private static Dialog f127a = null;
    private static ProgressDialog b = null;
    private static a c = null;
    private static int d = 0;

    public static void a() {
        if (f127a != null && f127a.isShowing()) {
            f127a.hide();
            f127a.show();
        }
    }

    public static boolean a(Context context) {
        if (context.equals(c)) {
            if (b != null && b.isShowing()) {
                return true;
            }
            if (f127a == null || !f127a.isShowing()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static int b() {
        return d;
    }

    public static void b(Context context) {
        if (context.equals(c)) {
            if (b != null) {
                b.dismiss();
            }
            b = null;
            if (f127a != null) {
                f127a.dismiss();
            }
            f127a = null;
        }
    }

    public static void a(Context context, boolean z, boolean z2, String str, String str2, String str3, int i) {
        a(context, z, z2, str, str2, str3, i, i, false, false);
    }

    public static void a(Context context, boolean z, boolean z2, String str, String str2, String str3, int i, boolean z3) {
        a(context, z, z2, str, str2, str3, i, i, z3, false);
    }

    public static void a(final Context context, boolean z, boolean z2, String str, String str2, String str3, final int i, final int i2, boolean z3, boolean z4) {
        int a2;
        Spanned fromHtml;
        d = 2;
        b(context);
        if (z) {
            View inflate = LayoutInflater.from(context).inflate(R.layout.style_dialog2_layout, (ViewGroup) null);
            f127a = new Dialog(context);
            f127a.setCancelable(z2);
            if (z2) {
                f127a.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    /* class GeneralFunction.o.AnonymousClass4 */

                    public void onCancel(DialogInterface dialogInterface) {
                        ((a) context).a(i2, 0);
                    }
                });
                f127a.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    /* class GeneralFunction.o.AnonymousClass5 */

                    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                        if (i != 4) {
                            return o.f127a.onKeyUp(i, keyEvent);
                        }
                        ((a) context).a(i2, 0);
                        o.f127a.dismiss();
                        return true;
                    }
                });
            }
            f127a.setContentView(inflate);
            f127a.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            TextView textView = (TextView) inflate.findViewById(R.id.TV_DialogTitle);
            TextView textView2 = (TextView) inflate.findViewById(R.id.TV_DialogDialog);
            Button button = (Button) inflate.findViewById(R.id.B_DialogSingleButton);
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            int i3 = displayMetrics.widthPixels;
            int i4 = displayMetrics.heightPixels;
            int max = Math.max(i3, i4);
            int min = Math.min(i3, i4);
            if (i4 > i3) {
                a2 = k.a(context, (float) (i4 / 35));
            } else {
                a2 = k.a(context, (float) (i4 / 20));
            }
            int i5 = (((int) (((double) min) * 0.95d)) / 2) * 2;
            textView.setTextSize((float) a2);
            textView2.setTextSize((float) a2);
            button.setTextSize((float) a2);
            ViewGroup.LayoutParams layoutParams = textView.getLayoutParams();
            layoutParams.height = max / 16;
            layoutParams.width = i5;
            int b2 = d.b();
            if (b2 == 15 || b2 == 17) {
                layoutParams.height = max / 12;
                layoutParams.width = i5;
                textView.setPadding(i5 / 18, 0, i5 / 18, 0);
            }
            textView.setText(str);
            ViewGroup.LayoutParams layoutParams2 = textView2.getLayoutParams();
            layoutParams2.height = max / 5;
            layoutParams2.width = i5;
            if (b2 == 16) {
                layoutParams2.height = (int) (((double) max) / 4.5d);
                layoutParams2.width = i5;
            }
            textView2.setPadding(i5 / 14, 0, i5 / 14, 0);
            if (z4) {
                if (Build.VERSION.SDK_INT >= 24) {
                    fromHtml = Html.fromHtml(str2, 0);
                } else {
                    fromHtml = Html.fromHtml(str2);
                }
                textView2.setText(fromHtml);
                textView2.setMovementMethod(LinkMovementMethod.getInstance());
            } else {
                textView2.setText(str2);
            }
            if (z3) {
                textView2.setGravity(17);
            }
            ViewGroup.LayoutParams layoutParams3 = button.getLayoutParams();
            layoutParams3.height = max / 15;
            layoutParams3.width = i5;
            button.setLayoutParams(layoutParams3);
            button.setText(str3);
            button.setOnClickListener(new View.OnClickListener() {
                /* class GeneralFunction.o.AnonymousClass6 */

                public void onClick(View view) {
                    ((a) context).a(i, 0);
                    o.f127a.dismiss();
                }
            });
            f127a.show();
            c = (a) context;
        }
    }

    public static void a(Context context, boolean z, boolean z2, String str, ArrayList<String> arrayList, String str2, int i) {
        a(context, z, z2, str, arrayList, str2, i, i, false);
    }

    public static void a(final Context context, boolean z, boolean z2, String str, ArrayList<String> arrayList, String str2, final int i, final int i2, boolean z3) {
        View inflate;
        d = 3;
        b(context);
        if (z) {
            int i3 = context.getResources().getConfiguration().orientation;
            if (i3 == 1) {
                inflate = LayoutInflater.from(context).inflate(R.layout.style_dialog_info_layout, (ViewGroup) null);
            } else {
                inflate = LayoutInflater.from(context).inflate(R.layout.style_dialog_info_land_layout, (ViewGroup) null);
            }
            if (inflate == null) {
                Log.e("UiDialog", "DialogWithSingleButtonInformation can't find view");
                return;
            }
            f127a = new Dialog(context);
            f127a.setCancelable(z2);
            if (z2) {
                f127a.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    /* class GeneralFunction.o.AnonymousClass7 */

                    public void onCancel(DialogInterface dialogInterface) {
                        ((a) context).a(i2, 0);
                    }
                });
                f127a.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    /* class GeneralFunction.o.AnonymousClass8 */

                    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                        if (i != 4) {
                            return o.f127a.onKeyUp(i, keyEvent);
                        }
                        ((a) context).a(i2, 0);
                        o.f127a.dismiss();
                        return true;
                    }
                });
            }
            f127a.setContentView(inflate);
            f127a.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            TextView textView = (TextView) inflate.findViewById(R.id.TV_DialogTitle);
            TextView textView2 = (TextView) inflate.findViewById(R.id.TV_DialogDialog_Name);
            TextView textView3 = (TextView) inflate.findViewById(R.id.TV_DialogDialog_Date);
            TextView textView4 = (TextView) inflate.findViewById(R.id.TV_DialogDialog_Wide);
            TextView textView5 = (TextView) inflate.findViewById(R.id.TV_DialogDialog_Height);
            TextView textView6 = (TextView) inflate.findViewById(R.id.TV_DialogDialog_Size);
            TextView textView7 = (TextView) inflate.findViewById(R.id.TV_DialogDialog_Path);
            TextView textView8 = (TextView) inflate.findViewById(R.id.TV_DialogDialog_EVorType);
            TextView textView9 = (TextView) inflate.findViewById(R.id.TV_DialogDialog_WBorDuration);
            TextView textView10 = (TextView) inflate.findViewById(R.id.TV_DialogDialog_ISOorNull);
            Button button = (Button) inflate.findViewById(R.id.B_DialogSingleButton);
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            int i4 = displayMetrics.widthPixels;
            int i5 = displayMetrics.heightPixels;
            int a2 = k.a(context, (float) (Math.max(i4, i5) / 36));
            int a3 = k.a(context, (float) (Math.max(i4, i5) / 40));
            int i6 = (((int) (((double) i4) * 0.95d)) / 2) * 2;
            textView.setTextSize((float) a2);
            textView2.setTextSize((float) a3);
            textView3.setTextSize((float) a3);
            textView4.setTextSize((float) a3);
            textView5.setTextSize((float) a3);
            textView6.setTextSize((float) a3);
            textView7.setTextSize((float) a3);
            textView8.setTextSize((float) a3);
            textView9.setTextSize((float) a3);
            textView10.setTextSize((float) a3);
            button.setTextSize((float) a2);
            ViewGroup.LayoutParams layoutParams = textView.getLayoutParams();
            layoutParams.height = Math.max(i4, i5) / 16;
            layoutParams.width = i6;
            textView.setText(str);
            ViewGroup.LayoutParams layoutParams2 = ((LinearLayout) inflate.findViewById(R.id.TV_DialogDialogLayout)).getLayoutParams();
            if (i3 == 1) {
                if (arrayList.size() <= 6) {
                    layoutParams2.height = (int) (((double) i5) / 3.1d);
                } else if (arrayList.size() <= 8) {
                    layoutParams2.height = (int) (((double) i5) / 2.5d);
                } else {
                    layoutParams2.height = (int) (((double) i5) / 2.3d);
                }
            } else if (i3 == 2) {
                layoutParams2.height = (int) (((double) i5) / 2.1d);
            }
            layoutParams2.width = i6;
            int i7 = i6 / 140;
            int min = Math.min(i4, i5) / 27;
            if (i3 == 1) {
                textView2.setPadding(i6 / 14, min, i6 / 14, 0);
                textView3.setPadding(i6 / 14, i7, i6 / 14, 0);
                textView8.setPadding(i6 / 14, i7, i6 / 14, 0);
                textView9.setPadding(i6 / 14, i7, i6 / 14, 0);
                textView10.setPadding(i6 / 14, i7, i6 / 14, 0);
            } else if (i3 == 2) {
                textView2.setPadding(i6 / 14, min, 0, 0);
                textView3.setPadding(0, min, i6 / 14, 0);
                textView8.setPadding(0, i7, i6 / 14, 0);
                textView9.setPadding(0, i7, i6 / 14, 0);
                textView10.setPadding(0, i7, i6 / 14, 0);
            }
            textView4.setPadding(i6 / 14, i7, i6 / 14, 0);
            textView5.setPadding(i6 / 14, i7, i6 / 14, 0);
            textView6.setPadding(i6 / 14, i7, i6 / 14, 0);
            textView7.setPadding(i6 / 14, i7, i6 / 14, 0);
            textView2.setText(arrayList.get(0));
            textView3.setText(arrayList.get(1));
            textView4.setText(arrayList.get(2));
            textView5.setText(arrayList.get(3));
            textView6.setText(arrayList.get(4));
            textView7.setText(arrayList.get(5));
            if (arrayList.size() > 6) {
                textView8.setText(arrayList.get(6));
                textView9.setText(arrayList.get(7));
                if (arrayList.size() <= 8) {
                    textView10.setText("");
                } else {
                    textView10.setText(arrayList.get(8));
                }
            } else {
                textView8.setVisibility(8);
                textView9.setVisibility(8);
                textView10.setVisibility(8);
            }
            if (z3) {
                textView2.setGravity(17);
                textView3.setGravity(17);
                textView4.setGravity(17);
                textView5.setGravity(17);
                textView6.setGravity(17);
                textView7.setGravity(17);
                textView8.setGravity(17);
                textView9.setGravity(17);
                textView10.setGravity(17);
            }
            ViewGroup.LayoutParams layoutParams3 = button.getLayoutParams();
            layoutParams3.height = Math.max(i4, i5) / 15;
            layoutParams3.width = i6;
            button.setLayoutParams(layoutParams3);
            button.setText(str2);
            button.setOnClickListener(new View.OnClickListener() {
                /* class GeneralFunction.o.AnonymousClass9 */

                public void onClick(View view) {
                    ((a) context).a(i, 0);
                    o.f127a.dismiss();
                }
            });
            f127a.show();
            c = (a) context;
        }
    }

    public static void a(Context context, boolean z, boolean z2, String str, String str2, String[] strArr, int[] iArr) {
        a(context, z, z2, str, str2, strArr, iArr, false, (int) R.color.black);
    }

    public static void a(Context context, boolean z, boolean z2, String str, String str2, String[] strArr, int[] iArr, boolean z3) {
        a(context, z, z2, str, str2, strArr, iArr, z3, (int) R.color.black);
    }

    public static void a(final Context context, boolean z, boolean z2, String str, String str2, String[] strArr, final int[] iArr, boolean z3, int i) {
        d = 4;
        b(context);
        if (z) {
            View inflate = LayoutInflater.from(context).inflate(R.layout.style_dialog_layout, (ViewGroup) null);
            f127a = new Dialog(context);
            f127a.setCancelable(z2);
            if (z2) {
                f127a.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    /* class GeneralFunction.o.AnonymousClass10 */

                    public void onCancel(DialogInterface dialogInterface) {
                        ((a) context).a(iArr[0], 0);
                    }
                });
                f127a.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    /* class GeneralFunction.o.AnonymousClass1 */

                    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                        if (i != 4) {
                            return o.f127a.onKeyUp(i, keyEvent);
                        }
                        ((a) context).a(iArr[0], 0);
                        o.f127a.dismiss();
                        return true;
                    }
                });
            }
            f127a.setContentView(inflate);
            f127a.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            TextView textView = (TextView) inflate.findViewById(R.id.TV_DialogTitle);
            TextView textView2 = (TextView) inflate.findViewById(R.id.TV_DialogDialog);
            Button button = (Button) inflate.findViewById(R.id.B_DialogLeftButton);
            Button button2 = (Button) inflate.findViewById(R.id.B_DialogRightButton);
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            int i2 = displayMetrics.widthPixels;
            int i3 = displayMetrics.heightPixels;
            int max = Math.max(i2, i3);
            int min = Math.min(i2, i3);
            int a2 = k.a(context, (float) (min / 21));
            int i4 = (((int) (((double) min) * 0.95d)) / 2) * 2;
            textView.setTextSize((float) a2);
            textView2.setTextSize((float) a2);
            button.setTextSize((float) a2);
            button2.setTextSize((float) a2);
            ViewGroup.LayoutParams layoutParams = textView.getLayoutParams();
            layoutParams.height = max / 16;
            layoutParams.width = i4;
            textView.setText(str);
            ViewGroup.LayoutParams layoutParams2 = textView2.getLayoutParams();
            layoutParams2.height = max / 5;
            layoutParams2.width = i4;
            if (z3) {
                textView2.setGravity(17);
            } else {
                textView2.setPadding(i4 / 14, 0, i4 / 14, 0);
            }
            textView2.setText(str2);
            textView2.setMovementMethod(new ScrollingMovementMethod());
            ViewGroup.LayoutParams layoutParams3 = button.getLayoutParams();
            layoutParams3.height = max / 15;
            layoutParams3.width = i4 / 2;
            ViewGroup.LayoutParams layoutParams4 = button2.getLayoutParams();
            layoutParams4.height = max / 15;
            layoutParams4.width = i4 / 2;
            button.setLayoutParams(layoutParams4);
            button.setText(strArr[0]);
            button.setOnClickListener(new View.OnClickListener() {
                /* class GeneralFunction.o.AnonymousClass2 */

                public void onClick(View view) {
                    ((a) context).a(iArr[0], 0);
                    o.f127a.dismiss();
                }
            });
            button2.setLayoutParams(layoutParams4);
            button2.setText(strArr[1]);
            button2.setTextColor(context.getColor(i));
            button2.setOnClickListener(new View.OnClickListener() {
                /* class GeneralFunction.o.AnonymousClass3 */

                public void onClick(View view) {
                    ((a) context).a(iArr[1], 0);
                    o.f127a.dismiss();
                }
            });
            f127a.show();
            c = (a) context;
        }
    }
}
