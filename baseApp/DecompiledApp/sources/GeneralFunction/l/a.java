package GeneralFunction.l;

import GeneralFunction.l.d;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.InputDeviceCompat;
import java.util.ArrayList;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private GeneralFunction.l.a.a f117a = null;
    private d b = null;
    private AbstractC0003a c = null;
    private boolean d = false;
    private d.b e = new d.b() {
        /* class GeneralFunction.l.a.AnonymousClass1 */

        @Override // GeneralFunction.l.d.b
        public void a(c cVar) {
            a.this.a((a) ("WechatShareResultListener result:" + cVar.f122a), (String) 3);
            a.this.a(cVar);
        }
    };
    private d.a f = new d.a() {
        /* class GeneralFunction.l.a.AnonymousClass2 */
    };

    /* renamed from: GeneralFunction.l.a$a  reason: collision with other inner class name */
    public interface AbstractC0003a {
        void a(c cVar);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i) {
        GeneralFunction.d.a("SNSManager", str, i);
    }

    public void a(Context context) {
        this.b = new d();
        LocalBroadcastManager instance = LocalBroadcastManager.getInstance(context);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("ShareResultCallback");
        instance.registerReceiver(this.b, intentFilter);
    }

    public void a(AbstractC0003a aVar) {
        this.c = aVar;
    }

    public void a(c cVar) {
        if (this.c != null) {
            this.c.a(cVar);
        }
    }

    private int b(b bVar, Activity activity) {
        switch (bVar.b) {
            case 4096:
                return l(bVar, activity);
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
                return l(bVar, activity);
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
                return l(bVar, activity);
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
            case 4100:
            default:
                return 5;
            case 4101:
                return l(bVar, activity);
        }
    }

    private static boolean a(Context context, String str) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        if (installedPackages == null) {
            return false;
        }
        for (int i = 0; i < installedPackages.size(); i++) {
            if (installedPackages.get(i).packageName.equals(str)) {
                return true;
            }
        }
        return false;
    }

    private int c(b bVar, Activity activity) {
        switch (bVar.b) {
            case 4096:
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
            case 4101:
                l(bVar, activity);
                return 0;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
            case 4100:
            default:
                return 6;
        }
    }

    private int d(b bVar, Activity activity) {
        switch (bVar.b) {
            case 4096:
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
            case 4101:
                l(bVar, activity);
                return 0;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
            case 4100:
            default:
                return 6;
        }
    }

    private int e(b bVar, Activity activity) {
        switch (bVar.b) {
            case 4096:
                if (bVar.c == 12288) {
                    return l(bVar, activity);
                }
                return 6;
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
            case 4101:
                return l(bVar, activity);
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
            case 4100:
            default:
                return 6;
        }
    }

    private int f(b bVar, Activity activity) {
        switch (bVar.b) {
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
            case 4101:
                l(bVar, activity);
                return 0;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
            case 4100:
            default:
                return 6;
        }
    }

    private int g(b bVar, Activity activity) {
        switch (bVar.b) {
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
                l(bVar, activity);
                return 0;
            default:
                return 6;
        }
    }

    private int h(b bVar, Activity activity) {
        switch (bVar.b) {
            case 4096:
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
            case 4101:
                l(bVar, activity);
                return 0;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
            case 4100:
            default:
                return 6;
        }
    }

    private int i(b bVar, Activity activity) {
        switch (bVar.b) {
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
            case 4101:
                l(bVar, activity);
                return 0;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
            case 4100:
            default:
                return 6;
        }
    }

    private int j(b bVar, Activity activity) {
        switch (bVar.b) {
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
            case 4101:
                this.d = true;
                l(bVar, activity);
                return 0;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
            case 4100:
            default:
                return 6;
        }
    }

    private int k(b bVar, Activity activity) {
        switch (bVar.b) {
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("youku://upload".trim()));
                intent.putExtra("isVr", 1);
                activity.startActivity(intent);
                return 0;
            default:
                return 6;
        }
    }

    private int l(b bVar, Activity activity) {
        int i = 0;
        Intent intent = null;
        int i2 = Build.VERSION.SDK_INT;
        switch (bVar.b) {
            case 4096:
                intent = new Intent("android.intent.action.SEND");
                intent.putExtra("android.intent.extra.TEXT", bVar.f);
                intent.setType("text/plain");
                break;
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
                Intent intent2 = new Intent("android.intent.action.SEND");
                if (i2 >= 24) {
                    intent2.putExtra("android.intent.extra.STREAM", bVar.n.get(0));
                    intent2.addFlags(3);
                    if (this.d) {
                        intent2.addFlags(268435456);
                        this.d = false;
                    }
                } else {
                    intent2.putExtra("android.intent.extra.STREAM", bVar.n.get(0));
                }
                intent2.setType("image/*");
                intent = intent2;
                break;
            case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
                if (bVar.l.size() > 1) {
                    Intent intent3 = new Intent("android.intent.action.SEND_MULTIPLE");
                    ArrayList<? extends Parcelable> arrayList = new ArrayList<>();
                    arrayList.clear();
                    for (int i3 = 0; i3 < bVar.l.size(); i3++) {
                        arrayList.add(bVar.l.get(i3));
                    }
                    intent3.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList);
                    intent = intent3;
                } else {
                    Intent intent4 = new Intent("android.intent.action.SEND");
                    if (i2 >= 24) {
                        intent4.putExtra("android.intent.extra.STREAM", bVar.l.get(0));
                        intent4.addFlags(3);
                        intent = intent4;
                    } else {
                        intent4.putExtra("android.intent.extra.STREAM", bVar.l.get(0));
                        intent = intent4;
                    }
                }
                intent.setType("video/*");
                break;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
            case 4100:
            default:
                i = 6;
                break;
            case 4101:
                Intent intent5 = new Intent("android.intent.action.SEND_MULTIPLE");
                ArrayList<? extends Parcelable> arrayList2 = new ArrayList<>();
                arrayList2.clear();
                for (int i4 = 0; i4 < bVar.n.size(); i4++) {
                    arrayList2.add(bVar.n.get(i4));
                }
                intent5.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList2);
                intent5.setType("image/*");
                intent = intent5;
                break;
        }
        switch (bVar.f121a) {
            case 8192:
                intent.setPackage("com.facebook.katana");
                break;
            case 8193:
                intent.setPackage("com.google.android.youtube");
                break;
            case 8194:
                intent.setPackage("jp.naver.line.android");
                break;
            case 8195:
                intent.setPackage("com.twitter.android");
                break;
            case 8196:
                intent.setPackage("com.instagram.android");
                break;
            case 8197:
                intent.setClassName("com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI");
                break;
            case 8198:
                intent.setClassName("com.tencent.mm", "com.tencent.mm.ui.tools.ShareToTimeLineUI");
                break;
            case 8199:
                intent.setClassName("com.tencent.mobileqq", "com.tencent.mobileqq.activity.JumpActivity");
                break;
            case 8200:
                intent.setPackage("com.qzone");
                break;
            case 8201:
                intent.setPackage("com.sina.weibo");
                break;
            case 8202:
                intent.setPackage("com.whatsapp");
                break;
            case 8203:
                intent.setPackage("com.youku.phone");
                intent.putExtra("isVr", 1);
                break;
            case 8204:
                intent.setPackage("com.vkontakte.android");
                break;
            case 8205:
                intent.setClassName("com.snapchat.android", "com.snapchat.android.LandingPageActivity");
                break;
            case 8206:
                intent.setPackage("com.google.android.apps.photos");
                break;
            default:
                i = 6;
                break;
        }
        if (i != 0) {
            return i;
        }
        try {
            activity.startActivityForResult(intent, 36864);
            return i;
        } catch (Exception e2) {
            a("SNS: startActivity is error.", 3);
            return 5;
        }
    }

    public int a(b bVar, Activity activity) {
        switch (bVar.f121a) {
            case 8192:
                if (a(activity, "com.facebook.katana")) {
                    return b(bVar, activity);
                }
                return 4;
            case 8193:
                if (a(activity, "com.google.android.youtube")) {
                    return g(bVar, activity);
                }
                return 4;
            case 8194:
                if (a(activity, "jp.naver.line.android")) {
                    return d(bVar, activity);
                }
                return 4;
            case 8195:
                if (a(activity, "com.twitter.android")) {
                    return c(bVar, activity);
                }
                return 4;
            case 8196:
                if (a(activity, "com.instagram.android")) {
                    return f(bVar, activity);
                }
                return 4;
            case 8197:
            case 8198:
            case 8199:
            case 8200:
            default:
                return 6;
            case 8201:
                if (a(activity, "com.sina.weibo")) {
                    return e(bVar, activity);
                }
                return 4;
            case 8202:
                if (a(activity, "com.whatsapp")) {
                    return h(bVar, activity);
                }
                return 4;
            case 8203:
                if (a(activity, "com.youku.phone")) {
                    return k(bVar, activity);
                }
                return 4;
            case 8204:
                if (a(activity, "com.vkontakte.android")) {
                    return i(bVar, activity);
                }
                return 4;
            case 8205:
                if (a(activity, "com.snapchat.android")) {
                    return j(bVar, activity);
                }
                return 4;
            case 8206:
                if (a(activity, "com.google.android.apps.photos")) {
                    return h(bVar, activity);
                }
                return 4;
        }
    }

    public void a(int i, int i2, Intent intent, Context context) {
        a("onActivityResultHandler requestCode" + i, 0);
        c cVar = new c();
        cVar.f122a = 0;
        switch (i) {
            case 0:
            default:
                return;
            case 36864:
                a(cVar);
                return;
            case 64207:
                GeneralFunction.l.a.a aVar = this.f117a;
                GeneralFunction.l.a.a.f120a.a(i, i2, intent);
                a(cVar);
                return;
        }
    }
}
