package GeneralFunction.l;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class d extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private b f123a = null;
    private a b = null;

    public interface a {
    }

    public interface b {
        void a(c cVar);
    }

    public void a(c cVar) {
        if (this.f123a != null) {
            this.f123a.a(cVar);
        }
    }

    public void onReceive(Context context, Intent intent) {
        c cVar = new c();
        String action = intent.getAction();
        char c = 65535;
        switch (action.hashCode()) {
            case 738726177:
                if (action.equals("ShareResultCallback")) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                cVar.f122a = intent.getIntExtra("ShareResultCallback", 0);
                a(cVar);
                return;
            default:
                return;
        }
    }
}
