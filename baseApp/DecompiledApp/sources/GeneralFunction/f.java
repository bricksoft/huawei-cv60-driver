package GeneralFunction;

import android.content.Context;
import android.support.annotation.StringRes;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private Context f81a;
    private String b = "";
    private b c;

    public interface b {
        void a(String str);
    }

    public f(Context context) {
        this.f81a = context;
    }

    public static f a(Context context) {
        return new f(context);
    }

    public f a(@StringRes int i) {
        this.b += this.f81a.getString(i);
        return this;
    }

    public SpannableStringBuilder a(boolean z) {
        Spanned fromHtml = Html.fromHtml(this.b);
        URLSpan[] uRLSpanArr = (URLSpan[]) fromHtml.getSpans(0, fromHtml.length(), URLSpan.class);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(fromHtml);
        spannableStringBuilder.clearSpans();
        for (URLSpan uRLSpan : uRLSpanArr) {
            spannableStringBuilder.setSpan(new a(uRLSpan.getURL(), z), fromHtml.getSpanStart(uRLSpan), fromHtml.getSpanEnd(uRLSpan), 33);
        }
        return spannableStringBuilder;
    }

    public f a(b bVar) {
        this.c = bVar;
        return this;
    }

    private class a extends ClickableSpan {
        private String b;
        private boolean c;

        public a(String str, boolean z) {
            this.b = str;
            this.c = z;
        }

        public void updateDrawState(TextPaint textPaint) {
            super.updateDrawState(textPaint);
            textPaint.setUnderlineText(this.c);
        }

        public void onClick(View view) {
            if (f.this.c != null) {
                f.this.c.a(this.b);
            }
        }
    }
}
