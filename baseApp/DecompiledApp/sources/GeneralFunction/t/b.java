package GeneralFunction.t;

import java.io.File;
import java.nio.ByteBuffer;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private File f154a = null;
    private a b = new a();

    public int a(String str) {
        this.f154a = new File(str);
        if (this.f154a.exists()) {
            return 0;
        }
        return -1;
    }

    public a a() {
        a(this.b, 0, this.f154a.length());
        return this.b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x008e A[SYNTHETIC, Splitter:B:14:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a9 A[SYNTHETIC, Splitter:B:27:0x00a9] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b6 A[SYNTHETIC, Splitter:B:34:0x00b6] */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(GeneralFunction.t.a r11, long r12, long r14) {
        /*
        // Method dump skipped, instructions count: 198
        */
        throw new UnsupportedOperationException("Method not decompiled: GeneralFunction.t.b.a(GeneralFunction.t.a, long, long):void");
    }

    private int a(byte[] bArr, int i, int i2) {
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.put(bArr, i, i2);
        allocate.flip();
        return allocate.getInt();
    }

    private long b(byte[] bArr, int i, int i2) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.put(bArr, i, i2);
        allocate.flip();
        return allocate.getLong();
    }
}
