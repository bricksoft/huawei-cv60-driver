package GeneralFunction.t;

import java.util.ArrayList;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public long f153a = 0;
    public long b = 0;
    public String c = "";
    public boolean d = false;
    public ArrayList<a> e = new ArrayList<>();

    public a a(String str) {
        ArrayList<a> b2 = b(str);
        if (b2.isEmpty()) {
            return null;
        }
        return b2.get(0);
    }

    public ArrayList<a> b(String str) {
        ArrayList<a> arrayList = new ArrayList<>();
        for (int i = 0; i < this.e.size(); i++) {
            if (this.e.get(i).c.equals(str)) {
                arrayList.add(this.e.get(i));
            }
        }
        return arrayList;
    }

    public String toString() {
        String str = "" + this.c + " " + this.f153a + " " + this.b + "\r\n";
        for (int i = 0; i < this.e.size(); i++) {
            str = str + this.e.get(i);
        }
        return str;
    }
}
