package GeneralFunction;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private static float f114a = 1.0f;

    public static int a(Context context, float f) {
        return (int) ((((double) f) / Math.ceil((double) context.getResources().getDisplayMetrics().scaledDensity)) + 0.5d);
    }

    public static void a() {
        Paint paint = new Paint();
        Rect rect = new Rect();
        paint.setTextSize(36.0f);
        paint.getTextBounds("P", 0, 1, rect);
        f114a = 36.0f / ((float) rect.height());
        d.a("", "CCC_InitFontSize_TextSize:" + 36.0f + ",result.height():" + rect.height(), 4);
    }
}
