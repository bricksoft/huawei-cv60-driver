package GeneralFunction;

import java.io.IOException;
import java.io.RandomAccessFile;

public class q {

    /* renamed from: a  reason: collision with root package name */
    private RandomAccessFile f140a = null;
    private String b = null;
    private String c = null;
    private String d = null;
    private int e = 7;

    /* access modifiers changed from: private */
    public class a extends Exception {
        public a(String str) {
            super(str);
        }
    }

    private void a(String str, int i) {
        if (i != 7) {
            d.a("XMPReader", str, i);
        }
    }

    public int a(String str) {
        int i;
        int i2;
        a("szPath:" + str, this.e);
        byte[] bArr = new byte[1024];
        try {
            this.f140a = new RandomAccessFile(str, "rw");
            this.f140a.seek(2);
            String num = Integer.toString(this.f140a.read(), 16);
            String num2 = Integer.toString(this.f140a.read(), 16);
            String str2 = "" + num + num2;
            if (!str2.equals("ffe1")) {
                a("not ffe1, break", 1);
                throw new a("");
            }
            a("tmp1:" + num, this.e);
            a("tmp2:" + num2, this.e);
            a("tmp:" + str2, this.e);
            String num3 = Integer.toString(this.f140a.read(), 16);
            String num4 = Integer.toString(this.f140a.read(), 16);
            if (num3.length() != 2) {
                num3 = "0" + num3;
            }
            if (num4.length() != 2) {
                num4 = "0" + num4;
            }
            String str3 = "" + num3 + num4;
            a("tmp1:" + num3, this.e);
            a("tmp2:" + num4, this.e);
            a("tmp:" + str3, this.e);
            long parseLong = Long.parseLong(str3, 16);
            a("offset:" + parseLong, this.e);
            long j = parseLong + 4;
            if (j > this.f140a.length()) {
                a("size > pFile size, break", 1);
                throw new a("");
            }
            this.f140a.seek(j);
            String num5 = Integer.toString(this.f140a.read(), 16);
            String num6 = Integer.toString(this.f140a.read(), 16);
            String str4 = "" + num5 + num6;
            if (!str4.equals("ffe1")) {
                a("not ffe1, break", 1);
                throw new a("");
            }
            a("tmp1:" + num5, this.e);
            a("tmp2:" + num6, this.e);
            a("tmp:" + str4, this.e);
            if (j + ((long) bArr.length) > this.f140a.length()) {
                a("size > pFile size, break", 1);
                throw new a("");
            }
            this.f140a.read(bArr);
            String str5 = new String(bArr);
            a("xmp:" + str5, this.e);
            a("PoseRollDegreesStart:" + str5.contains("<GPano:PoseRollDegrees>"), this.e);
            a("PoseRollDegreesStart:" + str5.indexOf("<GPano:PoseRollDegrees>"), this.e);
            a("PoseRollDegreesEnd:" + str5.indexOf("</GPano:PoseRollDegrees>"), this.e);
            if (str5.indexOf("<GPano:PoseRollDegrees>") == -1 || str5.indexOf("</GPano:PoseRollDegrees>") == -1) {
                throw new a("");
            }
            this.c = str5.substring(str5.indexOf("<GPano:PoseRollDegrees>") + "<GPano:PoseRollDegrees>".length(), str5.indexOf("</GPano:PoseRollDegrees>"));
            a("PoseRollDegrees:" + this.c, this.e);
            i = (int) Float.parseFloat(this.c);
            if (this.f140a != null) {
                try {
                    this.f140a.close();
                    i2 = 0;
                } catch (IOException e2) {
                    e2.printStackTrace();
                    i2 = 0;
                }
            } else {
                i2 = 0;
            }
            if (i2 == 0) {
                return a(i);
            }
            return i2;
        } catch (IOException e3) {
            e3.printStackTrace();
            if (this.f140a != null) {
                try {
                    this.f140a.close();
                    i = 0;
                    i2 = -1;
                } catch (IOException e4) {
                    e4.printStackTrace();
                    i = 0;
                    i2 = -1;
                }
            }
            i = 0;
            i2 = -1;
        } catch (NumberFormatException e5) {
            e5.printStackTrace();
            if (this.f140a != null) {
                try {
                    this.f140a.close();
                    i = 0;
                    i2 = -1;
                } catch (IOException e6) {
                    e6.printStackTrace();
                    i = 0;
                    i2 = -1;
                }
            }
            i = 0;
            i2 = -1;
        } catch (a e7) {
            if (this.f140a != null) {
                try {
                    this.f140a.close();
                    i = 0;
                    i2 = -1;
                } catch (IOException e8) {
                    e8.printStackTrace();
                    i = 0;
                    i2 = -1;
                }
            }
            i = 0;
            i2 = -1;
        } catch (Throwable th) {
            if (this.f140a != null) {
                try {
                    this.f140a.close();
                } catch (IOException e9) {
                    e9.printStackTrace();
                }
            }
            throw th;
        }
    }

    private int a(int i) {
        switch (i) {
            case -90:
                return 8;
            case 0:
            default:
                return 1;
            case 90:
                return 6;
            case 180:
                return 3;
        }
    }
}
