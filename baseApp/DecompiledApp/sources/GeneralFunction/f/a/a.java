package GeneralFunction.f.a;

import GeneralFunction.d;
import android.media.MediaFormat;
import android.media.MediaMuxer;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f83a = a.class.getSimpleName();
    private int b = 0;
    private long c = 0;
    private b d = new b();

    /* renamed from: GeneralFunction.f.a.a$a  reason: collision with other inner class name */
    public interface AbstractC0000a {
    }

    public void a(String str, int i) {
        d.a(f83a, str, i);
    }

    public void a() {
        a("user cancel", 4);
        this.d.h = true;
    }

    private class b {
        private MediaMuxer b;
        private MediaFormat c;
        private MediaFormat d;
        private int e;
        private int f;
        private AbstractC0000a g;
        private boolean h;

        private b() {
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = -1;
            this.f = -1;
            this.g = null;
            this.h = false;
        }
    }
}
