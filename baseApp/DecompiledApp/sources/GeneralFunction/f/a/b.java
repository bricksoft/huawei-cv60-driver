package GeneralFunction.f.a;

import android.annotation.SuppressLint;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.media.MediaMuxer;
import android.os.AsyncTask;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f85a = b.class.getSimpleName();
    private int b = 0;
    private long c = 0;
    private c d = new c();

    /* renamed from: GeneralFunction.f.a.b$b  reason: collision with other inner class name */
    public interface AbstractC0001b {
        void a(int i);
    }

    public void a(String str, int i) {
        GeneralFunction.d.a(f85a, str, i);
    }

    public void a() {
        a("user cancel", 4);
        this.d.m = true;
    }

    public void a(ArrayList<String> arrayList, String str, AbstractC0001b bVar, int i, int i2) {
        this.d.l = bVar;
        new a().execute(new d(arrayList), str, this.d.l);
        this.b = i;
        this.c = (long) (i2 - i);
        if (this.c == 0) {
            this.c = 2147483647L;
        }
    }

    public int a(ArrayList<String> arrayList, String str) {
        int i = -1;
        long currentTimeMillis = System.currentTimeMillis();
        this.d.m = false;
        try {
            this.d.b = new MediaMuxer(str, 0);
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(arrayList.get(0));
            String extractMetadata = mediaMetadataRetriever.extractMetadata(24);
            if (extractMetadata != null) {
                this.d.b.setOrientationHint(Integer.valueOf(extractMetadata).intValue());
            }
            mediaMetadataRetriever.release();
            int i2 = 0;
            while (true) {
                if (i2 >= arrayList.size()) {
                    break;
                }
                i = a(this.d, arrayList.get(i2));
                if (!this.d.m) {
                    if (i < 0) {
                        break;
                    }
                    i2++;
                } else {
                    i = -5;
                    break;
                }
            }
            a("total video frame:" + this.d.o, 4);
            if (this.d.b != null) {
                try {
                    this.d.b.stop();
                    this.d.b.release();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                this.d.b = null;
            }
            if (this.d.m) {
                File file = new File(str);
                if (file.exists() && !file.delete()) {
                    a("can't delete file:" + str, 0);
                }
            }
            a("time cost:" + (System.currentTimeMillis() - currentTimeMillis), 4);
            return i;
        } catch (IOException e2) {
            e2.printStackTrace();
            return -4;
        }
    }

    private int a(c cVar) {
        cVar.g = cVar.b.addTrack(cVar.c);
        cVar.h = cVar.b.addTrack(cVar.d);
        cVar.b.start();
        return 0;
    }

    private void b(c cVar) {
        if (cVar.f != null) {
            cVar.f.release();
            cVar.f = null;
        }
        if (cVar.e != null) {
            cVar.e.release();
            cVar.e = null;
        }
    }

    private int a(c cVar, String str) {
        long j;
        int i;
        a("extractFileAndFeedToMuxer:" + str, 4);
        cVar.f = new MediaExtractor();
        try {
            cVar.f.setDataSource(str);
            MediaFormat a2 = GeneralFunction.p.a.a(cVar.f, "audio/");
            if (a2 == null) {
                a("No audio track!", 0);
                b(cVar);
                return -3;
            }
            a("audio format:" + a2, 4);
            cVar.e = new MediaExtractor();
            try {
                cVar.e.setDataSource(str);
                MediaFormat a3 = GeneralFunction.p.a.a(cVar.e, "video/");
                if (a3 == null) {
                    a("No video track!", 0);
                    b(cVar);
                    return -2;
                }
                a("video format:" + a3, 4);
                if (!cVar.n) {
                    cVar.n = true;
                    cVar.j = a2.getInteger("channel-count");
                    cVar.k = a2.getInteger("sample-rate");
                    cVar.i = a3.getInteger("frame-rate");
                    cVar.c = a3;
                    cVar.d = a2;
                    a("lAudioChannel:" + cVar.j, 4);
                    a("lSampleRate:" + cVar.k, 4);
                    a("lVideoFrameRate:" + cVar.i, 4);
                    a("sVideoFormat:" + cVar.c, 4);
                    a("sAudioFormat:" + cVar.d, 4);
                    int a4 = a(cVar);
                    if (a4 < 0) {
                        return a4;
                    }
                }
                int integer = a3.getInteger("max-input-size");
                if (integer <= 0) {
                    a("Not have lVideoMaxInputSize:" + integer, 1);
                    integer = 1048576;
                }
                long j2 = ((cVar.o * 1000) * 1000) / ((long) cVar.i);
                ByteBuffer allocate = ByteBuffer.allocate(integer);
                cVar.e.seekTo((long) (this.b * 1000000), 2);
                long j3 = -1;
                int i2 = 0;
                while (!cVar.m) {
                    int readSampleData = cVar.e.readSampleData(allocate, 0);
                    if (readSampleData < 0) {
                        j = j3;
                        i = i2;
                    } else {
                        if (j3 == -1) {
                            j = cVar.e.getSampleTime();
                        } else {
                            j = j3;
                        }
                        long j4 = ((cVar.o * 1000) * 1000) / ((long) cVar.i);
                        int sampleFlags = cVar.e.getSampleFlags();
                        c.m(cVar);
                        allocate.position(0);
                        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
                        bufferInfo.set(0, readSampleData, j4, sampleFlags);
                        cVar.b.writeSampleData(cVar.g, allocate, bufferInfo);
                        i = i2 + 1;
                        if (cVar.e.advance() && ((long) i) < this.c * ((long) cVar.i)) {
                            j3 = j;
                            i2 = i;
                        }
                    }
                    a("video frame count:" + i, 4);
                    long j5 = (((cVar.o - 1) * 1000) * 1000) / ((long) cVar.i);
                    int i3 = 0;
                    ByteBuffer allocate2 = ByteBuffer.allocate(cVar.j * 1024 * 2);
                    cVar.f.seekTo((long) (this.b * 1000000), 2);
                    while (!cVar.m) {
                        int readSampleData2 = cVar.f.readSampleData(allocate2, 0);
                        if (readSampleData2 >= 0) {
                            long sampleTime = (cVar.f.getSampleTime() - j) + j2;
                            if (sampleTime >= j2) {
                                if (sampleTime <= j5) {
                                    int sampleFlags2 = cVar.f.getSampleFlags();
                                    allocate2.position(0);
                                    MediaCodec.BufferInfo bufferInfo2 = new MediaCodec.BufferInfo();
                                    bufferInfo2.set(0, readSampleData2, sampleTime, sampleFlags2);
                                    cVar.b.writeSampleData(cVar.h, allocate2, bufferInfo2);
                                    i3++;
                                }
                            }
                            if (cVar.f.advance()) {
                                if (((float) i3) >= ((float) this.c) * (((float) cVar.k) / 1024.0f)) {
                                }
                            }
                        }
                        b(cVar);
                        a("audio frame count:" + i3, 4);
                        return 0;
                    }
                    b(cVar);
                    return -5;
                }
                b(cVar);
                return -5;
            } catch (IOException e) {
                e.printStackTrace();
                b(cVar);
                return -1;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            b(cVar);
            return -1;
        }
    }

    /* access modifiers changed from: private */
    public class c {
        private MediaMuxer b;
        private MediaFormat c;
        private MediaFormat d;
        private MediaExtractor e;
        private MediaExtractor f;
        private int g;
        private int h;
        private int i;
        private int j;
        private int k;
        private AbstractC0001b l;
        private boolean m;
        private boolean n;
        private long o;

        static /* synthetic */ long m(c cVar) {
            long j2 = cVar.o;
            cVar.o = 1 + j2;
            return j2;
        }

        private c() {
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = null;
            this.f = null;
            this.g = -1;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = null;
            this.m = false;
            this.n = false;
            this.o = 0;
        }
    }

    @SuppressLint({"StaticFieldLeak"})
    private class a extends AsyncTask<Object, Object, Object> {
        private a() {
        }

        /* access modifiers changed from: protected */
        @Override // android.os.AsyncTask
        public Object doInBackground(Object... objArr) {
            int a2 = b.this.a(((d) objArr[0]).f88a, (String) objArr[1]);
            if (objArr[2] != null) {
                ((AbstractC0001b) objArr[2]).a(a2);
            }
            return Integer.valueOf(a2);
        }
    }

    private class d {

        /* renamed from: a  reason: collision with root package name */
        ArrayList<String> f88a = null;

        d(ArrayList<String> arrayList) {
            this.f88a = arrayList;
        }
    }
}
