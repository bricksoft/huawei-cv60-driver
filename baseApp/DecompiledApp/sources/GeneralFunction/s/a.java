package GeneralFunction.s;

import GeneralFunction.d;
import GeneralFunction.t.b;
import com.b.a.a.e;
import com.b.a.a.f;
import com.b.a.a.g;
import com.b.a.a.h;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

public class a {

    /* renamed from: GeneralFunction.s.a$a  reason: collision with other inner class name */
    private class C0006a extends Exception {
        public C0006a(String str) {
            super(str);
        }
    }

    private static void a(String str, int i) {
        d.a("MetaDataTool", str, i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x005a A[SYNTHETIC, Splitter:B:13:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e2 A[SYNTHETIC, Splitter:B:31:0x00e2] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fe A[SYNTHETIC, Splitter:B:39:0x00fe] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GeneralFunction.s.c a(java.lang.String r13) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: GeneralFunction.s.a.a(java.lang.String):GeneralFunction.s.c");
    }

    public int a(String str, b bVar) {
        GeneralFunction.t.a aVar;
        int b = b(str);
        if (b < 0) {
            a("Move error:" + b, 0);
            return -4;
        }
        int c = c(str);
        if (c < 0) {
            a("Remove error:" + c, 0);
            return -4;
        }
        File file = new File(str);
        g gVar = new g();
        e eVar = new e();
        h hVar = new h();
        hVar.a("<APP_VER>" + bVar.f151a + "</APP_VER>\n<CAMERA_VER>" + bVar.b + "</CAMERA_VER>");
        eVar.a(hVar);
        gVar.a((com.b.a.a.a) eVar);
        int a2 = (int) gVar.a();
        try {
            byte[] array = gVar.a(0, (long) (a2 - 8)).array();
            UUID fromString = UUID.fromString("ffcc8263-f855-4a93-8814-587a02521fdd");
            ByteBuffer wrap = ByteBuffer.wrap(new byte[16]);
            wrap.putLong(fromString.getMostSignificantBits());
            wrap.putLong(fromString.getLeastSignificantBits());
            f fVar = new f(wrap.array());
            fVar.a("<?xml version=\"1.0\"?><rdf:SphericalVideo\nxmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\nxmlns:GSpherical=\"http://ns.google.com/videos/1.0/spherical/\"><GSpherical:Spherical>true</GSpherical:Spherical><GSpherical:Stitched>true</GSpherical:Stitched><GSpherical:StitchingSoftware>Spherical Metadata Tool</GSpherical:StitchingSoftware><GSpherical:ProjectionType>equirectangular</GSpherical:ProjectionType></rdf:SphericalVideo>".getBytes());
            int a3 = (int) fVar.a();
            byte[] g = fVar.g();
            byte[] b2 = fVar.b();
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(str, "rw");
                b bVar2 = new b();
                bVar2.a(str);
                GeneralFunction.t.a a4 = bVar2.a().a("moov");
                int i = ((int) a4.b) + a2 + a3;
                bVar2.a(a4, a4.f153a, a4.b - 8);
                GeneralFunction.t.a a5 = a4.a("trak");
                Iterator<GeneralFunction.t.a> it = a4.b("trak").iterator();
                while (true) {
                    if (!it.hasNext()) {
                        aVar = a5;
                        break;
                    }
                    aVar = it.next();
                    bVar2.a(aVar, aVar.f153a, aVar.b - 8);
                    GeneralFunction.t.a a6 = aVar.a("mdia");
                    bVar2.a(a6, a6.f153a, a6.b - 8);
                    GeneralFunction.t.a a7 = a6.a("hdlr");
                    try {
                        randomAccessFile.seek(a7.f153a);
                        byte[] bArr = new byte[(((int) a7.b) - 8)];
                        randomAccessFile.read(bArr);
                        com.b.a.a.d dVar = new com.b.a.a.d();
                        dVar.a(ByteBuffer.wrap(bArr));
                        if (dVar.b().equals("vide")) {
                            a("vide trak found", 3);
                            break;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                int i2 = ((int) aVar.b) + a3;
                if (a4.a("udta") != null) {
                    a("udta still exist!", 3);
                }
                long j = (aVar.f153a - 8) + aVar.b;
                try {
                    randomAccessFile.seek(a4.f153a - 8);
                    randomAccessFile.write(ByteBuffer.allocate(4).putInt(i).array(), 0, 4);
                    randomAccessFile.seek(aVar.f153a - 8);
                    randomAccessFile.write(ByteBuffer.allocate(4).putInt(i2).array(), 0, 4);
                    randomAccessFile.seek(j);
                    byte[] bArr2 = new byte[((int) (file.length() - j))];
                    randomAccessFile.read(bArr2);
                    randomAccessFile.seek(j);
                    randomAccessFile.write(ByteBuffer.allocate(4).putInt(a3).array(), 0, 4);
                    randomAccessFile.write("uuid".getBytes(), 0, 4);
                    randomAccessFile.write(g, 0, g.length);
                    randomAccessFile.write(b2, 0, b2.length);
                    randomAccessFile.write(bArr2, 0, bArr2.length);
                    randomAccessFile.seek(file.length());
                    randomAccessFile.write(ByteBuffer.allocate(4).putInt(a2).array(), 0, 4);
                    randomAccessFile.write("udta".getBytes(), 0, 4);
                    randomAccessFile.write(array, 0, array.length);
                    if (randomAccessFile != null) {
                        try {
                            randomAccessFile.close();
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    }
                    return 0;
                } catch (IOException e3) {
                    e3.printStackTrace();
                    if (randomAccessFile == null) {
                        return -3;
                    }
                    try {
                        randomAccessFile.close();
                        return -3;
                    } catch (IOException e4) {
                        e4.printStackTrace();
                        return -3;
                    }
                } catch (Throwable th) {
                    if (randomAccessFile != null) {
                        try {
                            randomAccessFile.close();
                        } catch (IOException e5) {
                            e5.printStackTrace();
                        }
                    }
                    throw th;
                }
            } catch (FileNotFoundException e6) {
                e6.printStackTrace();
                return -2;
            }
        } catch (IOException e7) {
            e7.printStackTrace();
            return -1;
        }
    }

    private int b(String str) {
        File file = new File(str);
        b bVar = new b();
        bVar.a(str);
        GeneralFunction.t.a a2 = bVar.a().a("moov");
        if ((a2.f153a - 8) + a2.b >= file.length()) {
            return 0;
        }
        a("Move moov: " + a2.toString(), 3);
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(str, "rw");
            try {
                randomAccessFile.seek(a2.f153a - 8);
                byte[] bArr = new byte[((int) a2.b)];
                randomAccessFile.read(bArr);
                randomAccessFile.seek(a2.f153a - 4);
                randomAccessFile.write("free".getBytes(), 0, 4);
                randomAccessFile.seek(file.length());
                randomAccessFile.write(bArr, 0, bArr.length);
                if (randomAccessFile == null) {
                    return 0;
                }
                try {
                    randomAccessFile.close();
                    return 0;
                } catch (IOException e) {
                    e.printStackTrace();
                    return 0;
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                if (randomAccessFile == null) {
                    return -2;
                }
                try {
                    randomAccessFile.close();
                    return -2;
                } catch (IOException e3) {
                    e3.printStackTrace();
                    return -2;
                }
            } catch (Throwable th) {
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException e5) {
            e5.printStackTrace();
            return -1;
        }
    }

    private int c(String str) {
        new File(str);
        b bVar = new b();
        bVar.a(str);
        GeneralFunction.t.a a2 = bVar.a().a("moov");
        bVar.a(a2, a2.f153a, a2.b - 8);
        ArrayList<GeneralFunction.t.a> b = a2.b("udta");
        if (b.isEmpty()) {
            return 0;
        }
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(str, "rw");
            try {
                Iterator<GeneralFunction.t.a> it = b.iterator();
                while (it.hasNext()) {
                    GeneralFunction.t.a next = it.next();
                    a("Set udta to free: " + next.toString(), 3);
                    randomAccessFile.seek(next.f153a - 4);
                    randomAccessFile.write("free".getBytes(), 0, 4);
                }
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return 0;
            } catch (IOException e2) {
                e2.printStackTrace();
                if (randomAccessFile == null) {
                    return -2;
                }
                try {
                    randomAccessFile.close();
                    return -2;
                } catch (IOException e3) {
                    e3.printStackTrace();
                    return -2;
                }
            } catch (Throwable th) {
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException e5) {
            e5.printStackTrace();
            return -1;
        }
    }
}
