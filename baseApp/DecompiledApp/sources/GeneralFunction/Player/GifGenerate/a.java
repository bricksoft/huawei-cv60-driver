package GeneralFunction.Player.GifGenerate;

import GeneralFunction.Player.a.c;
import GeneralFunction.Player.a.d;
import GeneralFunction.Player.a.e;
import GeneralFunction.Player.player.b;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import com.huawei.cvIntl60.R;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f14a = e.class.getSimpleName();
    private c b;
    private int c;
    private int d;
    private int e;
    private int f;
    private float[] g = new float[16];
    private float[] h = new float[16];
    private d i;
    private FloatBuffer j = ByteBuffer.allocateDirect(48).order(ByteOrder.nativeOrder()).asFloatBuffer().put(new float[]{1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f});

    public a(Context context, int i2) {
        b.a(f14a, "SphericalSceneRenderer VideoMode:" + Integer.toString(i2), 3);
        if (i2 == 1 || i2 == 2) {
            this.b = new c(a(context, (int) R.raw.player_360_vertex_shader), a(context, (int) R.raw.player_360_fragment_shader_for_video));
            b.a(f14a, "Load the video_fragment_shader", 3);
        } else if (i2 == 3) {
            this.b = new c(a(context, (int) R.raw.player_360_vertex_shader), a(context, (int) R.raw.player_360_fragment_shader_for_bitmap));
            b.a(f14a, "Load the bitmap_fragment_shader", 3);
        } else {
            b.a(f14a, "Not correct VideoMode:" + i2, 3);
            return;
        }
        this.c = this.b.a("aPosition");
        this.d = this.b.b("uMVPMatrix");
        this.e = this.b.b("uTextureMatrix");
        this.f = this.b.a("aTextureCoord");
        GLES20.glDisable(2929);
        this.i = new d(180, 0.0f, 0.0f, 0.0f, 500.0f, 1, 1);
        GLES20.glUseProgram(this.b.a());
        GLES20.glEnableVertexAttribArray(this.c);
        GeneralFunction.Player.a.b.a("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(this.c, 3, 5126, false, this.i.b(), (Buffer) this.i.a());
        GeneralFunction.Player.a.b.a("glVertexAttribPointer");
        GLES20.glEnableVertexAttribArray(this.f);
        GeneralFunction.Player.a.b.a("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(this.f, 2, 5126, false, this.i.b(), this.i.a().duplicate().position(3));
        GeneralFunction.Player.a.b.a("glVertexAttribPointer");
        this.j.position(0);
    }

    public void a(int i2, float[] fArr, float[] fArr2, float[] fArr3, float[] fArr4, float[] fArr5, int i3, int i4, int i5, int i6, boolean z) {
        b();
        a(fArr, fArr2, fArr3, fArr4, fArr5, i6, z);
        b(i3, i2);
        a(i5, i4);
    }

    private void b() {
        GLES20.glClear(16384);
        GeneralFunction.Player.a.b.a("glClear");
    }

    private void a(int i2, int i3) {
        GLES20.glViewport(0, 0, i3, i2);
        GLES20.glEnable(3089);
        GLES20.glScissor(0, 0, i3, i2);
        GLES20.glDisable(3089);
        for (int i4 = 0; i4 < this.i.d().length; i4++) {
            GLES20.glDrawElements(4, this.i.d()[i4], 5123, this.i.c()[i4]);
            GeneralFunction.Player.a.b.a("glDrawElements");
        }
    }

    private void a(float[] fArr, float[] fArr2, float[] fArr3, float[] fArr4, float[] fArr5, int i2, boolean z) {
        float[] fArr6 = new float[16];
        GLES20.glVertexAttribPointer(this.c, 3, 5126, false, this.i.b(), (Buffer) this.i.a());
        GeneralFunction.Player.a.b.a("glVertexAttribPointer");
        GLES20.glEnableVertexAttribArray(this.f);
        GeneralFunction.Player.a.b.a("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(this.f, 2, 5126, false, this.i.b(), this.i.a().duplicate().position(3));
        GeneralFunction.Player.a.b.a("glVertexAttribPointer");
        Matrix.translateM(fArr, 0, 0.0f, 1.0f, 0.0f);
        float[] fArr7 = (float[]) fArr3.clone();
        if (z) {
            Matrix.multiplyMM(this.g, 0, fArr4, 0, a(i2, fArr7), 0);
        } else {
            Matrix.multiplyMM(this.g, 0, fArr4, 0, fArr7, 0);
        }
        Matrix.multiplyMM(fArr7, 0, fArr5, 0, fArr2, 0);
        Matrix.multiplyMM(this.h, 0, this.g, 0, fArr7, 0);
        GLES20.glUniformMatrix4fv(this.e, 1, false, fArr, 0);
        GeneralFunction.Player.a.b.a("glUniformMatrix4fv");
        GLES20.glUniformMatrix4fv(this.d, 1, false, this.h, 0);
        GeneralFunction.Player.a.b.a("glUniformMatrix4fv");
    }

    private float[] a(int i2, float[] fArr) {
        switch (i2) {
            case 3:
                Matrix.rotateM(fArr, 0, 180.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(fArr, 0, -1.0f, -1.0f, 0.0f);
                break;
            case 6:
                Matrix.rotateM(fArr, 0, 90.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(fArr, 0, 0.0f, -1.0f, 0.0f);
                break;
            case 8:
                Matrix.rotateM(fArr, 0, 270.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(fArr, 0, -1.0f, 0.0f, 0.0f);
                break;
        }
        return fArr;
    }

    private void b(int i2, int i3) {
        switch (i2) {
            case 1:
                GLES20.glBindTexture(36197, i3);
                GeneralFunction.Player.a.b.a("glBindTexture");
                return;
            case 2:
                GLES20.glBindTexture(36197, i3);
                GeneralFunction.Player.a.b.a("glBindTexture");
                return;
            case 3:
                GLES20.glBindTexture(3553, i3);
                GeneralFunction.Player.a.b.a("glBindTexture");
                return;
            default:
                return;
        }
    }

    public void a() {
        this.b.b();
    }

    public static String a(Context context, int i2) {
        InputStream openRawResource = context.getResources().openRawResource(i2);
        InputStreamReader inputStreamReader = new InputStreamReader(openRawResource);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine).append('\n');
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            try {
                break;
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        bufferedReader.close();
        try {
            inputStreamReader.close();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        try {
            openRawResource.close();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
        return sb.toString();
    }
}
