package GeneralFunction.Player.GifGenerate;

import GeneralFunction.Player.player.h;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Choreographer;
import android.view.Surface;
import android.view.TextureView;
import android.view.WindowManager;
import android.widget.MediaController;
import com.huawei.cvIntl60.R;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IjkMediaCodecInfo;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public class GifGenerate extends TextureView implements TextureView.SurfaceTextureListener, MediaController.MediaPlayerControl {
    private static final String c = GifGenerate.class.getSimpleName();
    private c A;

    /* renamed from: a  reason: collision with root package name */
    protected IjkMediaPlayer f1a;
    int b;
    private h d;
    private int e;
    private int f;
    private int g;
    private int h;
    private boolean i;
    private boolean j;
    private boolean k;
    private int l;
    private int m;
    private int n;
    private int o;
    private Context p;
    private final String q;
    private b r;
    private e s;
    private String t;
    private WindowManager u;
    private SurfaceTexture v;
    private int w;
    private BlockingQueue<Message> x;
    private d y;
    private g z;

    public interface c {
    }

    public interface d {
        void a(int i);
    }

    /* access modifiers changed from: private */
    public class f {

        /* renamed from: a  reason: collision with root package name */
        float f13a;
        float b;
    }

    public interface g {
        void a(int i);
    }

    /* access modifiers changed from: private */
    public static void c(String str, int i2) {
        GeneralFunction.Player.player.b.a(c, str, i2);
    }

    public void a(String str, int i2) {
        c("setGifSavePathAndWatermarkIndex:" + i2, 3);
        Message obtain = Message.obtain();
        obtain.what = 14;
        obtain.obj = str;
        obtain.arg1 = i2;
        a(obtain);
    }

    public void a(int i2, int i3) {
        Message obtain = Message.obtain();
        obtain.what = 23;
        obtain.obj = Integer.valueOf(i2);
        obtain.arg1 = i3;
        a(obtain);
    }

    public void setGifFromImageDirect(int i2) {
        Message obtain = Message.obtain();
        obtain.what = 13;
        obtain.arg1 = i2;
        a(obtain);
    }

    public void a(int i2) {
        c("isPreplay:" + i2, 3);
        this.k = false;
        Message obtain = Message.obtain();
        obtain.what = 15;
        obtain.arg1 = i2;
        a(obtain);
    }

    public void a(int i2, int i3, int i4) {
        c("getGifFromVideo:" + i4, 3);
        c("getGifFromVideo:startTime" + i2, 3);
        c("getGifFromVideo:endTime" + i3, 3);
        this.m = i2;
        this.l = i3;
        this.n = this.l - this.m;
        this.k = false;
        seekTo(i2 * 1000);
        Message obtain = Message.obtain();
        obtain.what = 16;
        obtain.arg1 = i4;
        obtain.arg2 = i2 * 1000;
        a(obtain);
        start();
    }

    public void a() {
        this.k = true;
        Message obtain = Message.obtain();
        obtain.what = 19;
        a(obtain);
    }

    public void setPlayerStatusListener(d dVar) {
        this.y = dVar;
    }

    public void setGifGenerateStatusListener(g gVar) {
        this.z = gVar;
    }

    public void setOnClickListener(c cVar) {
        this.A = cVar;
    }

    public void setEnable(boolean z2) {
        this.i = z2;
        this.j = z2;
    }

    public void setSphericalParameter(h hVar) {
        if (hVar == null) {
            c("parameter is null", 1);
            return;
        }
        Message obtain = Message.obtain();
        obtain.what = 21;
        obtain.obj = hVar;
        a(obtain);
    }

    public boolean isEnabled() {
        return this.i;
    }

    public void setZoomInEnable(boolean z2) {
        this.j = z2;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void b(int i2) {
        if (this.y != null) {
            c("StutasChange:" + i2, 3);
            this.y.a(i2);
        }
    }

    public void b() {
        Message obtain = Message.obtain();
        obtain.what = 20;
        a(obtain);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void c(int i2) {
        if (this.z != null) {
            c("gifGenerateStatusChange:" + i2, 3);
            this.z.a(i2);
        }
    }

    public int c() {
        return this.e;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void d(int i2) {
        this.e = i2;
    }

    public void start() {
        if (this.d.m == 1 && this.f1a != null) {
            this.f1a.start();
        }
    }

    public void pause() {
        if (this.d.m == 1 && this.f1a != null && this.f1a.isPlaying()) {
            this.f1a.pause();
        }
    }

    public int getDuration() {
        if (this.d.m != 1 || this.f1a == null) {
            return 0;
        }
        return (int) this.f1a.getDuration();
    }

    public int getCurrentPosition() {
        int currentPosition;
        if (this.d.m != 1 || this.f1a == null) {
            return 0;
        }
        if (this.f1a.getDuration() != this.f1a.getCurrentPosition()) {
            currentPosition = (((int) this.f1a.getCurrentPosition()) / 1000) * 1000;
        } else {
            currentPosition = (int) this.f1a.getCurrentPosition();
        }
        c("lCurrentPosition+++++-+" + currentPosition, currentPosition);
        return currentPosition;
    }

    public void seekTo(int i2) {
        if (this.d.m == 1 && this.f1a != null) {
            this.f1a.seekTo((long) i2);
        }
    }

    public boolean isPlaying() {
        if (this.d.m != 1 || this.f1a == null) {
            return false;
        }
        return this.f1a.isPlaying();
    }

    public int getBufferPercentage() {
        if (this.d.m != 1 || this.f1a == null || this.f1a.getDuration() == 0) {
            return 0;
        }
        return (((((int) this.f1a.getCurrentPosition()) * 100) / ((int) this.f1a.getDuration())) / 2) * 2;
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return true;
    }

    public boolean canSeekForward() {
        return true;
    }

    public int getAudioSessionId() {
        return 0;
    }

    private class a {

        /* renamed from: a  reason: collision with root package name */
        Bitmap f7a;
        int b;
        boolean c;

        a(Bitmap bitmap, int i, boolean z) {
            this.f7a = bitmap;
            this.b = i;
            this.c = z;
        }
    }

    public GifGenerate(Context context) {
        this(context, null);
        this.p = context;
        setSurfaceTextureListener(this);
    }

    public GifGenerate(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.p = context;
        setSurfaceTextureListener(this);
    }

    public GifGenerate(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = new h();
        this.e = 0;
        this.f = 30;
        this.g = 640;
        this.h = 480;
        this.i = true;
        this.j = true;
        this.k = false;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = 0;
        this.q = null;
        this.x = new ArrayBlockingQueue(90);
        this.y = null;
        this.z = null;
        this.A = null;
        this.b = 0;
        this.d.a(context);
        this.p = context;
        setSurfaceTextureListener(this);
        d(0);
        this.u = (WindowManager) context.getSystemService("window");
        this.w = this.u.getDefaultDisplay().getRotation();
    }

    public void d() {
        if (this.s == null) {
            this.s = new e("GifRenderThread");
            this.s.start();
        }
    }

    public int e() {
        c("Play:" + c(), 3);
        if (c() != 1) {
            c("Invalid operation in status:" + c(), 0);
            return -1;
        } else if (this.d.m == 0) {
            c("Invalid operation in mode:" + this.d.m, 0);
            return -1;
        } else {
            c("[Video Player]Play", 2);
            a(this.v, this.d.o, this.d.p);
            return 0;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void i() {
        this.r = new b();
        this.r.a(this.f);
        this.r.start();
        c("MJPGThread is start", 3);
        d(2);
        b(2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str) {
        if (this.s == null) {
            throw new IllegalStateException("RenderThread has not been initialized");
        } else if (TextUtils.isEmpty(str)) {
            throw new RuntimeException("Cannot begin playback: video path is empty");
        } else {
            this.f1a = new IjkMediaPlayer();
            if (this.d.m != 3) {
                this.f1a.setSurface(this.s.a());
            }
            this.f1a.setVolume(0.0f, 0.0f);
            this.f1a.setAudioStreamType(3);
            this.f1a.setOption(4, "mediacodec", 1);
            this.f1a.setOption(4, "mediacodec-handle-resolution-change", 1);
            this.f1a.setOption(4, "mediacodec-avc", 1);
            this.f1a.setOption(4, "start-on-prepared", 1);
            this.f1a.setLogEnabled(false);
            this.f1a.setKeepInBackground(true);
            try {
                this.f1a.setDataSource(getContext(), Uri.parse(str), (Map<String, String>) null);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.f1a.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
                /* class GeneralFunction.Player.GifGenerate.GifGenerate.AnonymousClass1 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnPreparedListener
                public void onPrepared(IMediaPlayer iMediaPlayer) {
                    Message obtain = Message.obtain();
                    obtain.what = 11;
                    obtain.arg1 = GifGenerate.this.f1a.getVideoWidth();
                    obtain.arg2 = GifGenerate.this.f1a.getVideoHeight();
                    GifGenerate.this.a((GifGenerate) obtain);
                    GifGenerate.this.seekTo(0);
                    GifGenerate.this.j();
                    GifGenerate.c("getCurrentPosition()" + GifGenerate.this.getCurrentPosition(), 3);
                    if (GifGenerate.this.getCurrentPosition() != 0) {
                        GifGenerate.this.seekTo(0);
                        GifGenerate.c("twice_getCurrentPosition()" + GifGenerate.this.getCurrentPosition(), 3);
                        GifGenerate.this.b();
                    }
                }
            });
            this.f1a.setOnBufferingUpdateListener(new IMediaPlayer.OnBufferingUpdateListener() {
                /* class GeneralFunction.Player.GifGenerate.GifGenerate.AnonymousClass2 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnBufferingUpdateListener
                public void onBufferingUpdate(IMediaPlayer iMediaPlayer, int i) {
                }
            });
            this.f1a.setOnErrorListener(new IMediaPlayer.OnErrorListener() {
                /* class GeneralFunction.Player.GifGenerate.GifGenerate.AnonymousClass3 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnErrorListener
                public boolean onError(IMediaPlayer iMediaPlayer, int i, int i2) {
                    GifGenerate.c("ijkplayer+++++++onError:" + iMediaPlayer.isPlaying() + ",what " + i + ",extra " + i2, 1);
                    return false;
                }
            });
            this.f1a.setOnInfoListener(new IMediaPlayer.OnInfoListener() {
                /* class GeneralFunction.Player.GifGenerate.GifGenerate.AnonymousClass4 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnInfoListener
                public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i2) {
                    switch (i) {
                        case IMediaPlayer.MEDIA_INFO_BUFFERING_END /*{ENCODED_INT: 702}*/:
                            if (iMediaPlayer.isPlaying()) {
                                return false;
                            }
                            GifGenerate.c("+++++++++OnInfoListener:" + iMediaPlayer.isPlaying() + ",what " + i + ",extra " + i2, 3);
                            GifGenerate.this.b();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.f1a.prepareAsync();
            this.f1a.setOnCompletionListener(new IMediaPlayer.OnCompletionListener() {
                /* class GeneralFunction.Player.GifGenerate.GifGenerate.AnonymousClass5 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnCompletionListener
                public void onCompletion(IMediaPlayer iMediaPlayer) {
                    GifGenerate.this.f1a.pause();
                    Log.e(GifGenerate.c, "isPrePlay" + GifGenerate.this.o);
                    if (!(GifGenerate.this.o == 4 || GifGenerate.this.o == 3)) {
                        Message obtain = Message.obtain();
                        obtain.what = 22;
                        GifGenerate.this.a((GifGenerate) obtain);
                    }
                    GifGenerate.c("Video play complete", 3);
                    GifGenerate.this.d((GifGenerate) 3);
                    GifGenerate.this.b((GifGenerate) 3);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void j() {
        if (!this.f1a.isPlaying()) {
            this.f1a.start();
        }
        d(2);
        b(2);
    }

    public void a(SurfaceTexture surfaceTexture, int i2, int i3) {
        Message obtain = Message.obtain();
        obtain.what = 1;
        obtain.obj = surfaceTexture;
        obtain.arg1 = i2;
        obtain.arg2 = i3;
        a(obtain);
    }

    public void a(Bitmap bitmap, int i2, boolean z2) {
        if (c() == 2) {
            Message message = new Message();
            message.what = 8;
            message.obj = new a(bitmap, i2, z2);
            try {
                this.x.put(message);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        } else {
            c("360 Player can't receive Bitmap ", 0);
        }
    }

    public void f() {
        a(false);
    }

    private void a(boolean z2) {
        c("releaseResources " + z2, 3);
        if (this.s != null) {
            Message message = new Message();
            message.what = 4;
            message.obj = Boolean.valueOf(z2);
            a(message);
        }
    }

    public void setVideoFilePath(String str) {
        this.t = str;
    }

    public int g() {
        if (c() != 2) {
            c("Invalid operation in status:" + c(), 0);
            return -1;
        }
        switch (this.d.m) {
            case 1:
                if (this.f1a.isPlaying()) {
                    this.f1a.pause();
                    break;
                }
                break;
            case 3:
                this.r.a();
                if (this.f1a != null) {
                    this.f1a.pause();
                    break;
                }
                break;
        }
        d(4);
        b(4);
        return 0;
    }

    public void setFrameRate(int i2) {
        this.f = i2;
        switch (this.d.m) {
            case 1:
            case 2:
            default:
                return;
            case 3:
                this.r.a(i2);
                return;
        }
    }

    public void setPlayerMode(int i2) {
        if (i2 < 4) {
            this.d.m = i2;
        } else {
            c("Not support player mode:" + i2, 0);
        }
        Message obtain = Message.obtain();
        obtain.what = 17;
        obtain.arg1 = i2;
        a(obtain);
    }

    public int getPlayerMode() {
        return this.d.m;
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        c("onSurfaceTextureAvailable:" + i2 + "x" + i3, 3);
        this.d.o = i2;
        this.d.p = i3;
        this.v = surfaceTexture;
        try {
            Thread.sleep(30);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        d(1);
        b(1);
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        c("onSurfaceTextureSizeChanged:" + i2 + "x" + i3, 3);
        this.d.o = i2;
        this.d.p = i3;
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        c("onSurfaceTextureDestroyed", 3);
        this.k = true;
        if (!(c() == 0 || c() == 5)) {
            a(true);
        }
        d(0);
        b(0);
        return false;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int a(Message message) {
        if (this.s != null) {
            this.s.a(message);
            return 0;
        }
        c("objRenderThread is null!", 0);
        return -1;
    }

    /* access modifiers changed from: private */
    public class e extends HandlerThread {
        private int A = 360;
        private int B = 0;
        private ArrayList<Bitmap> C;
        private int D = IjkMediaCodecInfo.RANK_LAST_CHANCE;
        private int E = 300;
        private int F = 212;
        private int G = 30;
        private a H;

        /* renamed from: a  reason: collision with root package name */
        int f9a = 0;
        private int c = 0;
        private Handler d;
        private Choreographer.FrameCallback e = new a();
        private GeneralFunction.Player.a.a f = new GeneralFunction.Player.a.a();
        private SurfaceTexture g;
        private int h = -1;
        private int i = 1;
        private int j = 1;
        private float[] k = new float[16];
        private float[] l = new float[16];
        private float[] m = new float[16];
        private float[] n = new float[16];
        private float[] o = new float[3];
        private boolean p = false;
        private boolean q = false;
        private boolean r;
        private boolean s;
        private boolean t = false;
        private int u = 0;
        private int v = 0;
        private int w = 0;
        private String x = null;
        private int y = 0;
        private int z = 360;

        public int a(Message message) {
            if (this.d == null) {
                return -1;
            }
            this.d.sendMessage(message);
            return 0;
        }

        private class a implements Choreographer.FrameCallback {
            private a() {
            }

            public void doFrame(long j) {
                Message obtain = Message.obtain();
                obtain.what = 2;
                e.this.a(obtain);
            }
        }

        public e(String str) {
            super(str);
        }

        public synchronized void start() {
            super.start();
            this.d = new Handler(getLooper()) {
                /* class GeneralFunction.Player.GifGenerate.GifGenerate.e.AnonymousClass1 */

                public void handleMessage(Message message) {
                    super.handleMessage(message);
                    switch (message.what) {
                        case 1:
                            e.this.a((e) ((SurfaceTexture) message.obj), (SurfaceTexture) message.arg1, message.arg2);
                            return;
                        case 2:
                            e.this.b();
                            return;
                        case 3:
                            e.this.j();
                            return;
                        case 4:
                            e.this.A = e.this.z;
                            e.this.a((e) ((Boolean) message.obj).booleanValue());
                            return;
                        case 5:
                            e.this.a((e) ((f) message.obj));
                            return;
                        case 6:
                            e.this.a((e) ((Float) message.obj).floatValue(), 0.0f);
                            return;
                        case 7:
                            GifGenerate.this.d.z = (float[]) ((float[]) message.obj).clone();
                            e.this.s = true;
                            return;
                        case 8:
                            a aVar = (a) message.obj;
                            e.this.c = aVar.b;
                            e.this.a((e) aVar.f7a);
                            e.this.p = aVar.c;
                            return;
                        case 9:
                        default:
                            return;
                        case 10:
                            e.this.a((e) ((Integer) message.obj).intValue());
                            e.this.s = true;
                            return;
                        case 11:
                            e.this.c = 0;
                            e.this.i = message.arg1;
                            e.this.j = message.arg2;
                            return;
                        case 12:
                            e.this.a((e) 0.0f, Float.MIN_VALUE);
                            return;
                        case 13:
                            e.this.u = message.arg1;
                            return;
                        case 14:
                            e.this.x = (String) message.obj;
                            e.this.v = message.arg1;
                            return;
                        case 15:
                            GifGenerate.this.o = message.arg1;
                            GifGenerate.this.c((GifGenerate) GifGenerate.this.o);
                            e.this.s = true;
                            e.this.b((e) e.this.u);
                            if (GifGenerate.this.o == 2) {
                                e.this.e();
                                return;
                            }
                            return;
                        case 16:
                            GifGenerate.c("MSG_GIF_FROM_VIDEO", 3);
                            e.this.y = message.arg2;
                            GifGenerate.this.o = message.arg1;
                            GifGenerate.this.c((GifGenerate) GifGenerate.this.o);
                            e.this.s = true;
                            if (GifGenerate.this.o == 2) {
                                e.this.e();
                                return;
                            }
                            return;
                        case 17:
                            GifGenerate.this.d.m = message.arg1;
                            return;
                        case 18:
                            e.this.w = message.arg1;
                            if (e.this.w == 3) {
                                if (e.this.C.size() != 0 && !GifGenerate.this.k) {
                                    e.this.a(e.this.x, e.this.C);
                                }
                                e.this.f();
                                if (GifGenerate.this.o == 1) {
                                    GifGenerate.this.o = 0;
                                    GifGenerate.this.c((GifGenerate) 0);
                                    return;
                                } else if (!GifGenerate.this.k) {
                                    GifGenerate.this.o = 4;
                                    GifGenerate.this.c((GifGenerate) 4);
                                    return;
                                } else {
                                    GifGenerate.this.o = 3;
                                    GifGenerate.this.c((GifGenerate) 3);
                                    return;
                                }
                            } else {
                                return;
                            }
                        case 19:
                            e.this.t = true;
                            return;
                        case 20:
                            e.this.r = true;
                            e.this.s = true;
                            e.this.b();
                            return;
                        case 21:
                            h hVar = (h) message.obj;
                            if (hVar.m != 0 && hVar.f51a != 0) {
                                GifGenerate.this.d.f51a = ((h) message.obj).f51a;
                                GifGenerate.this.d.l = ((h) message.obj).l;
                                GifGenerate.this.d.s = ((h) message.obj).s;
                                if (hVar.s != 1) {
                                    Matrix.setIdentityM(GifGenerate.this.d.z, 0);
                                    GifGenerate.this.d.z = (float[]) ((h) message.obj).z.clone();
                                }
                                GifGenerate.this.d.q = ((h) message.obj).q;
                                GifGenerate.this.d.r = ((h) message.obj).r;
                                GifGenerate.this.d.f = ((h) message.obj).e - 35.0f;
                                e.this.s = true;
                                return;
                            }
                            return;
                        case 22:
                            GifGenerate.c("MSG_SET_PLAY_COMPLETE", 3);
                            if (GifGenerate.this.o == 2) {
                                if (e.this.C.size() != 0 && !GifGenerate.this.k) {
                                    e.this.a(e.this.x, e.this.C);
                                }
                                e.this.f();
                            }
                            e.this.s = false;
                            GifGenerate.this.f1a.pause();
                            GifGenerate.this.seekTo(e.this.y);
                            e.this.A = e.this.z;
                            if (GifGenerate.this.o == 1) {
                                GifGenerate.this.o = 0;
                                GifGenerate.this.c((GifGenerate) 0);
                                return;
                            } else if (!GifGenerate.this.k) {
                                GifGenerate.this.o = 4;
                                GifGenerate.this.c((GifGenerate) 4);
                                return;
                            } else {
                                GifGenerate.this.o = 3;
                                GifGenerate.this.c((GifGenerate) 3);
                                return;
                            }
                        case 23:
                            e.this.D = ((Integer) message.obj).intValue();
                            e.this.E = message.arg1;
                            return;
                    }
                }
            };
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(int i2) {
            if (GifGenerate.this.d.p > GifGenerate.this.d.o) {
                GifGenerate.this.d.a(i2, false);
            } else {
                GifGenerate.this.d.a(i2, true);
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(Bitmap bitmap) {
            this.i = bitmap.getWidth();
            this.j = bitmap.getHeight();
            GLES20.glBindTexture(3553, this.h);
            if (!bitmap.isRecycled()) {
                GLUtils.texImage2D(3553, 0, bitmap, 0);
            } else {
                GifGenerate.c("updateBitmap with recycle bitmap!", 1);
            }
            this.s = true;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private Surface a() {
            if (!this.f.e()) {
                throw new IllegalStateException("Cannot get video decode surface without GL context");
            }
            this.h = GeneralFunction.Player.a.b.a(GifGenerate.this.d.m);
            this.g = new SurfaceTexture(this.h);
            this.g.setOnFrameAvailableListener(new SurfaceTexture.OnFrameAvailableListener() {
                /* class GeneralFunction.Player.GifGenerate.GifGenerate.e.AnonymousClass2 */

                public void onFrameAvailable(SurfaceTexture surfaceTexture) {
                    Message obtain = Message.obtain();
                    obtain.what = 3;
                    e.this.a(obtain);
                }
            });
            return new Surface(this.g);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(SurfaceTexture surfaceTexture, int i2, int i3) {
            if (!this.q) {
                this.q = true;
                GifGenerate.c("surfacePrepare w: " + i2 + " h: " + i3, 3);
                this.f.a(surfaceTexture);
                Choreographer.getInstance().postFrameCallback(this.e);
                GLES20.glViewport(0, 0, i2, i3);
                GeneralFunction.Player.a.b.a("glViewport");
                h();
                Matrix.setIdentityM(this.m, 0);
                Matrix.setRotateM(this.l, 0, 90.0f, 1.0f, 0.0f, 0.0f);
                GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
                this.H = new a(GifGenerate.this.getContext(), GifGenerate.this.d.m);
                switch (GifGenerate.this.d.m) {
                    case 1:
                        if (GifGenerate.this.f1a != null) {
                            GifGenerate.this.f1a.setSurface(GifGenerate.this.s.a());
                            GifGenerate.this.d((GifGenerate) 2);
                            GifGenerate.this.b((GifGenerate) 2);
                            break;
                        } else {
                            GifGenerate.this.a((GifGenerate) GifGenerate.this.t);
                            break;
                        }
                    case 3:
                        this.h = GeneralFunction.Player.a.b.a(GifGenerate.this.d.m);
                        GifGenerate.this.i();
                        if (GifGenerate.this.q != null) {
                            GifGenerate.this.a((GifGenerate) GifGenerate.this.q);
                            break;
                        }
                        break;
                }
                if (GifGenerate.this.d.s == 1 || GifGenerate.this.d.s == 0) {
                    Matrix.setIdentityM(GifGenerate.this.d.z, 0);
                }
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00c9  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x011e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void b() {
            /*
            // Method dump skipped, instructions count: 378
            */
            throw new UnsupportedOperationException("Method not decompiled: GeneralFunction.Player.GifGenerate.GifGenerate.e.b():void");
        }

        private void c() {
            if (!this.s) {
                return;
            }
            if (GifGenerate.this.d.m != 3 || this.t) {
                if (this.t) {
                    GifGenerate.this.o = 0;
                    if (GifGenerate.this.f1a != null) {
                        GifGenerate.this.f1a.pause();
                        GifGenerate.this.seekTo(this.y);
                    }
                    c(this.u);
                    f();
                    this.A = this.z;
                    this.t = false;
                    this.s = true;
                    GifGenerate.this.c((GifGenerate) 0);
                    GifGenerate.this.o = 0;
                    return;
                }
                this.s = false;
            } else if (GifGenerate.this.o != 1 && GifGenerate.this.o != 2) {
                this.s = false;
            } else if (this.A > this.B) {
                b(this.u);
            } else {
                if (GifGenerate.this.o == 2) {
                    if (this.C.size() != 0) {
                        GifGenerate.c("bitmapArrayList.size()" + this.C.size(), 2);
                        if (!GifGenerate.this.k) {
                            a(this.x, this.C);
                        }
                    }
                    f();
                }
                this.A = this.z;
                this.s = false;
                if (GifGenerate.this.o == 1) {
                    GifGenerate.this.o = 0;
                    GifGenerate.this.c((GifGenerate) 0);
                } else if (!GifGenerate.this.k) {
                    GifGenerate.this.o = 4;
                    GifGenerate.this.c((GifGenerate) 4);
                } else {
                    GifGenerate.this.o = 3;
                    GifGenerate.this.c((GifGenerate) 3);
                }
            }
        }

        private void d() {
            if (GifGenerate.this.d.m == 1) {
                if (!this.t) {
                    Log.e(GifGenerate.c, "getCurrentPosition" + GifGenerate.this.f1a.getCurrentPosition());
                    Log.e(GifGenerate.c, "playVideoEndTime" + GifGenerate.this.l);
                    if (GifGenerate.this.getCurrentPosition() / 1000 >= GifGenerate.this.l) {
                        if (GifGenerate.this.o == 2) {
                            if (this.C.size() != 0 && !GifGenerate.this.k) {
                                a(this.x, this.C);
                            }
                            f();
                        }
                        this.s = false;
                        GifGenerate.this.f1a.pause();
                        GifGenerate.this.seekTo(this.y);
                        this.A = this.z;
                        if (GifGenerate.this.o == 1) {
                            GifGenerate.this.o = 0;
                            GifGenerate.this.c((GifGenerate) 0);
                        } else if (!GifGenerate.this.k) {
                            GifGenerate.this.o = 4;
                            GifGenerate.this.c((GifGenerate) 4);
                        } else {
                            GifGenerate.this.o = 3;
                            GifGenerate.this.c((GifGenerate) 3);
                        }
                    }
                } else {
                    GifGenerate.this.o = 0;
                    this.s = false;
                    if (GifGenerate.this.f1a != null) {
                        GifGenerate.this.f1a.pause();
                        GifGenerate.this.seekTo(this.y);
                    }
                    f();
                    this.A = this.z;
                    this.t = false;
                    GifGenerate.this.c((GifGenerate) 0);
                    GifGenerate.this.o = 0;
                }
                this.s = false;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x0022  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0038 A[SYNTHETIC, Splitter:B:20:0x0038] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0044 A[SYNTHETIC, Splitter:B:26:0x0044] */
        /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(java.lang.String r4, java.util.ArrayList<android.graphics.Bitmap> r5) {
            /*
                r3 = this;
                r2 = 0
                java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0031, all -> 0x0041 }
                r0.<init>(r4)     // Catch:{ IOException -> 0x0031, all -> 0x0041 }
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0031, all -> 0x0041 }
                r1.<init>(r0)     // Catch:{ IOException -> 0x0031, all -> 0x0041 }
                byte[] r0 = r3.a(r5)     // Catch:{ IOException -> 0x0050 }
                if (r0 == 0) goto L_0x0014
                r1.write(r0)     // Catch:{ IOException -> 0x0050 }
            L_0x0014:
                if (r1 == 0) goto L_0x0019
                r1.close()     // Catch:{ IOException -> 0x002c }
            L_0x0019:
                GeneralFunction.Player.GifGenerate.GifGenerate r0 = GeneralFunction.Player.GifGenerate.GifGenerate.this
                boolean r0 = GeneralFunction.Player.GifGenerate.GifGenerate.d(r0)
                r1 = 1
                if (r0 == r1) goto L_0x002b
                GeneralFunction.Player.GifGenerate.GifGenerate r0 = GeneralFunction.Player.GifGenerate.GifGenerate.this
                android.content.Context r0 = GeneralFunction.Player.GifGenerate.GifGenerate.j(r0)
                GeneralFunction.g.a.a(r0, r4)
            L_0x002b:
                return
            L_0x002c:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0019
            L_0x0031:
                r0 = move-exception
                r1 = r2
            L_0x0033:
                r0.printStackTrace()     // Catch:{ all -> 0x004d }
                if (r1 == 0) goto L_0x0019
                r1.close()     // Catch:{ IOException -> 0x003c }
                goto L_0x0019
            L_0x003c:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0019
            L_0x0041:
                r0 = move-exception
            L_0x0042:
                if (r2 == 0) goto L_0x0047
                r2.close()     // Catch:{ IOException -> 0x0048 }
            L_0x0047:
                throw r0
            L_0x0048:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0047
            L_0x004d:
                r0 = move-exception
                r2 = r1
                goto L_0x0042
            L_0x0050:
                r0 = move-exception
                goto L_0x0033
            */
            throw new UnsupportedOperationException("Method not decompiled: GeneralFunction.Player.GifGenerate.GifGenerate.e.a(java.lang.String, java.util.ArrayList):void");
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void b(int i2) {
            if (GifGenerate.this.o == 1) {
                try {
                    Thread.sleep(230);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            if ((this.A >= this.B && GifGenerate.this.o == 1) || (this.A >= this.B && GifGenerate.this.o == 2)) {
                if (i2 == 0) {
                    GifGenerate.this.d.q += 10.0f;
                } else {
                    GifGenerate.this.d.q -= 10.0f;
                }
                this.A -= 10;
            }
            h();
            GifGenerate.this.d.r = Math.max(-85.0f, Math.min(85.0f, GifGenerate.this.d.r));
            float radians = (float) Math.toRadians((double) (90.0f - GifGenerate.this.d.r));
            float radians2 = (float) Math.toRadians((double) GifGenerate.this.d.q);
            this.o[0] = (float) (((double) GifGenerate.this.d.l) * Math.sin((double) radians) * Math.cos((double) radians2));
            this.o[1] = (float) (((double) GifGenerate.this.d.l) * Math.cos((double) radians));
            this.o[2] = (float) (((double) GifGenerate.this.d.l) * Math.sin((double) radians) * Math.sin((double) radians2));
            float[] fArr = {(float) (((double) (GifGenerate.this.d.j - 100.0f)) * Math.sin((double) radians) * Math.cos((double) radians2)), (float) (((double) (GifGenerate.this.d.j - 100.0f)) * Math.cos((double) radians)), (float) (Math.sin((double) radians2) * ((double) (GifGenerate.this.d.j - 100.0f)) * Math.sin((double) radians))};
            Matrix.setLookAtM(this.m, 0, this.o[0], this.o[1], this.o[2], fArr[0], fArr[1], fArr[2], 0.0f, 1.0f, 0.0f);
        }

        private void c(int i2) {
            if (i2 == 0) {
                GifGenerate.this.d.q -= (float) (this.z - this.A);
                return;
            }
            GifGenerate.this.d.q += (float) (this.z - this.A);
        }

        public byte[] a(ArrayList<Bitmap> arrayList) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            com.a.a.c.a aVar = new com.a.a.c.a();
            this.f9a = 0;
            aVar.b(0);
            aVar.c(18);
            if (this.v == 2 || this.v == 4) {
                com.a.a.c.d[] dVarArr = {new com.a.a.c.d(10.0f, 10, this.E - ((this.E / 7) - 10), this.F - 20, this.G)};
                aVar.a(new int[]{255, 255, 255, 254, 254, 254});
                aVar.a(dVarArr);
            }
            if (GifGenerate.this.d.m == 3) {
                aVar.a(230);
            } else {
                aVar.a(100);
            }
            GifGenerate.c("bitmaps.size()" + arrayList.size(), 3);
            aVar.a(byteArrayOutputStream);
            Iterator<Bitmap> it = arrayList.iterator();
            while (it.hasNext()) {
                Bitmap next = it.next();
                GifGenerate.c("count" + this.f9a, 3);
                if (!GifGenerate.this.k) {
                    if (next != null) {
                        aVar.a(next);
                        next.recycle();
                    }
                    this.f9a++;
                } else {
                    byteArrayOutputStream.reset();
                    aVar.a();
                    return null;
                }
            }
            aVar.a();
            return byteArrayOutputStream.toByteArray();
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void e() {
            this.C = new ArrayList<>();
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void f() {
            GifGenerate.c("imageListClean", 2);
            if (this.C != null) {
                if (this.C.size() != 0) {
                    for (int i2 = 0; i2 < this.C.size(); i2++) {
                        this.C.get(i2).recycle();
                    }
                    this.C.clear();
                }
                GifGenerate.c("imageListCleaEnd", 2);
            }
            GifGenerate.this.b = 0;
        }

        private void a(int i2, int i3) {
            Bitmap bitmap;
            if (!this.t) {
                if (!GifGenerate.this.k) {
                    bitmap = GifGenerate.this.getBitmap();
                } else {
                    bitmap = null;
                }
                if (bitmap != null) {
                    Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, this.D, this.E, true);
                    if (!GifGenerate.this.k) {
                        try {
                            Thread.sleep(40);
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                    }
                    Bitmap b2 = b(createScaledBitmap);
                    if (!GifGenerate.this.k) {
                        this.C.add(b2);
                    }
                    if (!GifGenerate.this.k) {
                        try {
                            Thread.sleep(40);
                        } catch (InterruptedException e3) {
                            e3.printStackTrace();
                        }
                    }
                    bitmap.recycle();
                    if (createScaledBitmap != b2) {
                        createScaledBitmap.recycle();
                    }
                    if (!GifGenerate.this.k) {
                        try {
                            Thread.sleep(40);
                        } catch (InterruptedException e4) {
                            e4.printStackTrace();
                        }
                    }
                    GifGenerate.c("++++Finish++++++", 2);
                }
            }
            GifGenerate.c("++++Finish++++++", 2);
        }

        private Bitmap b(Bitmap bitmap) {
            Bitmap d2 = d(this.v);
            if (d2 != null) {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(d2, this.F, this.G, false);
                Canvas canvas = new Canvas(bitmap);
                canvas.drawBitmap(bitmap, new android.graphics.Matrix(), null);
                canvas.drawBitmap(createScaledBitmap, 10.0f, (float) (this.E - (this.E / 7)), (Paint) null);
                canvas.save();
                canvas.restore();
                d2.recycle();
                createScaledBitmap.recycle();
            } else {
                GifGenerate.c("No WarterMark", 3);
            }
            return bitmap;
        }

        private Bitmap d(int i2) {
            switch (i2) {
                case 0:
                default:
                    return null;
                case 1:
                    return BitmapFactory.decodeResource(GifGenerate.this.p.getResources(), R.drawable.gallery_logolist_logo_1_500x70);
                case 2:
                    return BitmapFactory.decodeResource(GifGenerate.this.p.getResources(), R.drawable.gallery_logolist_logo_2_original);
                case 3:
                    return BitmapFactory.decodeResource(GifGenerate.this.p.getResources(), R.drawable.gallery_logolist_logo_3_500x70);
                case 4:
                    return BitmapFactory.decodeResource(GifGenerate.this.p.getResources(), R.drawable.gallery_logolist_logo_4_original);
            }
        }

        private float g() {
            if (GifGenerate.this.d.b == 0) {
                return ((float) GifGenerate.this.d.o) / ((float) GifGenerate.this.d.p);
            }
            return (((float) GifGenerate.this.d.o) / 2.0f) / ((float) GifGenerate.this.d.p);
        }

        private void h() {
            float g2 = g();
            int i2 = GifGenerate.this.d.f51a;
            h unused = GifGenerate.this.d;
            if (i2 != 5) {
                int i3 = GifGenerate.this.d.f51a;
                h unused2 = GifGenerate.this.d;
                if (i3 != 3) {
                    int i4 = GifGenerate.this.d.f51a;
                    h unused3 = GifGenerate.this.d;
                    if (i4 != 6) {
                        if (GifGenerate.this.d.l < 0.0f) {
                            Matrix.perspectiveM(this.n, 0, GifGenerate.this.d.f, g2, 0.1f, GifGenerate.this.d.c);
                            return;
                        } else {
                            Matrix.perspectiveM(this.n, 0, GifGenerate.this.d.f, g2, GifGenerate.this.d.l, GifGenerate.this.d.c);
                            return;
                        }
                    }
                }
                if (GifGenerate.this.d.l < 0.0f) {
                    Matrix.perspectiveM(this.n, 0, GifGenerate.this.d.f, g2, 0.1f, GifGenerate.this.d.c);
                } else {
                    Matrix.perspectiveM(this.n, 0, GifGenerate.this.d.f, g2, GifGenerate.this.d.l - 300.0f, GifGenerate.this.d.c);
                }
            } else {
                Matrix.perspectiveM(this.n, 0, GifGenerate.this.d.f, g2, 0.1f, GifGenerate.this.d.c);
            }
        }

        private void i() {
            float radians;
            h();
            GifGenerate.this.d.r = Math.max(-85.0f, Math.min(85.0f, GifGenerate.this.d.r));
            float radians2 = (float) Math.toRadians((double) (90.0f - GifGenerate.this.d.r));
            if (GifGenerate.this.d.s == 1) {
                radians = (float) Math.toRadians((double) (GifGenerate.this.d.q + 90.0f));
            } else if (GifGenerate.this.d.s == 3) {
                radians = (float) Math.toRadians(90.0d);
            } else {
                radians = (float) Math.toRadians((double) GifGenerate.this.d.q);
            }
            this.o[0] = (float) (((double) GifGenerate.this.d.l) * Math.sin((double) radians2) * Math.cos((double) radians));
            this.o[1] = (float) (((double) GifGenerate.this.d.l) * Math.cos((double) radians2));
            this.o[2] = (float) (((double) GifGenerate.this.d.l) * Math.sin((double) radians2) * Math.sin((double) radians));
            float[] fArr = {(float) (((double) (GifGenerate.this.d.j - 100.0f)) * Math.sin((double) radians2) * Math.cos((double) radians)), (float) (((double) (GifGenerate.this.d.j - 100.0f)) * Math.cos((double) radians2)), (float) (Math.sin((double) radians2) * ((double) (GifGenerate.this.d.j - 100.0f)) * Math.sin((double) radians))};
            Matrix.setLookAtM(this.m, 0, this.o[0], this.o[1], this.o[2], fArr[0], fArr[1], fArr[2], 0.0f, 1.0f, 0.0f);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void j() {
            this.r = true;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(boolean z2) {
            GifGenerate.c("surfaceDestroy", 3);
            if (this.q) {
                this.q = false;
                GifGenerate.c("start_surfaceDestroy", 3);
                GifGenerate.this.o = 3;
                GifGenerate.this.c((GifGenerate) 3);
                GifGenerate.this.d.m = 0;
                GifGenerate.this.o = 0;
                Choreographer.getInstance().removeFrameCallback(this.e);
                if (!z2) {
                    this.d = null;
                    if (this.f != null) {
                        k();
                        this.f.d();
                        this.f = null;
                    }
                    if (GifGenerate.this.s != null) {
                        GifGenerate.this.s.quit();
                        GifGenerate.this.s = null;
                    }
                    GifGenerate.this.d.f51a = 0;
                    if (GifGenerate.this.f1a != null) {
                        try {
                            Thread.sleep(30);
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                        GifGenerate.this.f1a.stop();
                        GifGenerate.this.f1a.release();
                        GifGenerate.this.f1a = null;
                    }
                    this.C = null;
                } else {
                    if (this.f != null) {
                        this.f.d();
                    }
                    if (GifGenerate.this.f1a != null) {
                        try {
                            Thread.sleep(30);
                        } catch (InterruptedException e3) {
                            e3.printStackTrace();
                        }
                        GifGenerate.this.f1a.setSurface(null);
                    }
                }
                if (GifGenerate.this.r != null) {
                    GifGenerate.this.r.a();
                    GifGenerate.this.r.interrupt();
                    GifGenerate.this.r = null;
                }
                if (this.h != -1) {
                    GLES20.glDeleteTextures(1, new int[]{this.h}, 0);
                    this.h = -1;
                }
                if (this.g != null) {
                    this.g.release();
                    this.g = null;
                    this.r = false;
                }
                Matrix.setIdentityM(GifGenerate.this.d.z, 0);
                this.s = false;
                if (this.H != null) {
                    this.H.a();
                    this.H = null;
                }
                GifGenerate.this.x.clear();
                if (!z2) {
                    GifGenerate.this.d((GifGenerate) 5);
                    GifGenerate.this.b((GifGenerate) 5);
                    GifGenerate.this.d((GifGenerate) 1);
                    GifGenerate.this.b((GifGenerate) 1);
                }
                GifGenerate.c("finish_surfaceDestroy", 3);
            }
        }

        private void k() {
            GLES20.glClear(16384);
            GeneralFunction.Player.a.b.a("glClear");
            this.f.b();
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(f fVar) {
            GifGenerate.this.d.q = (((-fVar.f13a) * 0.1f) + GifGenerate.this.d.q) % 360.0f;
            GifGenerate.this.d.r = ((-fVar.b) * 0.1f) + GifGenerate.this.d.r;
            this.s = true;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(float f2, boolean z2) {
            float f3 = 50.0f;
            if (z2) {
                GifGenerate.this.d.b();
            } else {
                if (((int) f2) == 1) {
                    f3 = -50.0f;
                }
                GifGenerate.this.d.a(f3 + GifGenerate.this.d.l);
            }
            this.s = true;
        }
    }

    public class b extends Thread {
        private int b = 30;
        private int c = a(this.b, 0);
        private long d = 0;
        private boolean e = false;

        public b() {
        }

        public void a(int i) {
            this.b = i;
            this.c = a(this.b, 0);
        }

        private int a(int i, int i2) {
            float f = (float) i;
            return (int) (1000.0f / ((f + ((((float) i2) / (f / 6.0f)) * (f / 15.0f))) + 1.0f));
        }

        public void run() {
            while (!Thread.interrupted()) {
                if (!this.e && !GifGenerate.this.x.isEmpty()) {
                    try {
                        Message message = (Message) GifGenerate.this.x.poll(50, TimeUnit.MICROSECONDS);
                        this.c = a(this.b, GifGenerate.this.x.size());
                        long currentTimeMillis = ((long) this.c) - (System.currentTimeMillis() - this.d);
                        if (currentTimeMillis > 0) {
                            Thread.sleep(currentTimeMillis);
                        }
                        this.d = System.currentTimeMillis();
                        if (message != null) {
                            GifGenerate.this.a((GifGenerate) message);
                        }
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

        public void a() {
            this.e = true;
        }
    }
}
