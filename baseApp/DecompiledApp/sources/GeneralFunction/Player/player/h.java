package GeneralFunction.Player.player;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;

public class h {
    private static final String C = h.class.getSimpleName();
    public float[] A = new float[16];
    public boolean B = false;
    private float D = 715.0f;
    private float E = 200.0f;
    private float F = 920.0f;
    private float G = 920.0f;
    private float H = 650.0f;
    private float I = 1040.0f;
    private float J = 1040.0f;
    private float K = 1025.0f;
    private float L = 250.0f;
    private float M = 505.0f;
    private float N = 690.0f;
    private float O = 870.0f;
    private float P = 870.0f;
    private float Q = 600.0f;
    private float R = 950.0f;
    private float S = 950.0f;
    private float T = 950.0f;
    private float U = 310.0f;
    private float V = 480.0f;
    private float W = -188.0f;
    private float X = 10.0f;
    private float Y = 10.0f;
    private float Z = 10.0f;

    /* renamed from: a  reason: collision with root package name */
    public int f51a = 0;
    private float aA = 0.1f;
    private float aB = 3000.0f;
    private float aC = 3000.0f;
    private float aD = 5000.0f;
    private float aE = 10000.0f;
    private Context aF = null;
    private ValueAnimator aG;
    private float aa = 10.0f;
    private float ab = -50.0f;
    private float ac = 1400.0f;
    private float ad = 300.0f;
    private float ae = 450.0f;
    private float af = 400.0f;
    private float ag = 0.0f;
    private float ah = 200.0f;
    private float ai = 1400.0f;
    private float aj = 550.0f;
    private float ak = 450.0f;
    private float al = 320.0f;
    private float am = 400.0f;
    private float an = 480.0f;
    private float ao = 950.0f;
    private float ap = 400.0f;
    private float aq = 106.0f;
    private float ar = 118.0f;
    private float as = 85.0f;
    private float at = 50.0f;
    private float au = 100.0f;
    private float av = 85.0f;
    private float aw = 145.0f;
    private float ax = 100.0f;
    private float ay = 150.0f;
    private float az = 50.0f;
    public int b = 0;
    public float c = this.aC;
    public float d = this.ay;
    public float e = this.at;
    public float f = this.at;
    public float g = this.aj;
    public float h = this.F;
    public float i = this.O;
    public float j = this.X;
    public float k = this.ad;
    public float l = this.aj;
    public int m = 0;
    public boolean n = false;
    public int o = 0;
    public int p = 0;
    public float q = 0.0f;
    public float r = 0.0f;
    public int s = 0;
    public float t = 25.0f;
    public float u = 0.0f;
    public float v = 0.0f;
    public float w = 10.0f;
    public float x = 20.0f;
    public boolean y;
    public float[] z = new float[16];

    private static void a(String str, int i2) {
        b.a(C, str, i2);
    }

    public void a(Context context) {
        this.aF = context;
    }

    public void a(int i2, boolean z2) {
        if (this.f51a == i2) {
            a("already same type", 2);
        } else if (this.f51a == 0) {
            this.f51a = i2;
            a(false);
            f();
        } else {
            this.f51a = i2;
            a(true);
            f();
        }
    }

    public int a() {
        return this.f51a;
    }

    public void b(int i2, boolean z2) {
        this.b = i2;
        a(false);
        f();
        c();
        if (this.l < this.j) {
            this.l = this.j;
        } else if (this.l > this.h) {
            this.l = this.h;
        }
    }

    public void b() {
        if (this.l != this.i) {
            this.l = this.i;
        } else {
            this.l = this.k;
        }
    }

    public void a(float f2) {
        this.l = f2;
        if (this.l < this.j) {
            this.l = this.j;
        } else if (this.l > this.h) {
            this.l = this.h;
        }
    }

    public void c() {
        switch (this.f51a) {
            case 1:
                this.l = this.ai;
                return;
            case 2:
                this.l = this.aj;
                return;
            case 3:
                if (this.b == 1) {
                    this.l = this.am;
                    return;
                } else if (this.m == 2) {
                    this.l = this.al;
                    return;
                } else {
                    this.l = this.ak;
                    return;
                }
            case 4:
            default:
                return;
            case 5:
                this.l = this.an;
                return;
            case 6:
                if (this.b == 1) {
                    this.l = this.ap;
                    return;
                } else {
                    this.l = this.ao;
                    return;
                }
        }
    }

    public void b(float f2) {
        if (f2 < -89.0f) {
            this.r = -89.0f;
        } else if (f2 > 89.0f) {
            this.r = 89.0f;
        } else {
            this.r = f2;
        }
    }

    public void a(boolean z2) {
        boolean z3;
        float f2;
        switch (((WindowManager) this.aF.getSystemService("window")).getDefaultDisplay().getRotation()) {
            case 1:
            case 3:
                z3 = true;
                break;
            case 2:
            default:
                z3 = false;
                break;
        }
        if (this.m == 2) {
            if (a() != 3) {
                a("FORMAT_LIVE parameter not handle view type:" + a(), 0);
            }
            this.d = this.az;
            this.c = this.aD;
            this.q = 0.0f;
            b(0.0f);
            this.e = this.av;
            this.l = this.al;
            this.g = this.al;
            this.j = this.aa;
            this.k = this.af;
            this.h = this.K;
            this.i = this.T;
        } else if (this.b == 1) {
            switch (a()) {
                case 2:
                    this.d = this.ay;
                    this.c = this.aC;
                    this.q = 0.0f;
                    b(0.0f);
                    this.e = this.at;
                    this.l = this.aj;
                    this.g = this.aj;
                    this.j = this.X;
                    this.k = this.ad;
                    this.h = this.H;
                    this.i = this.Q;
                    return;
                case 3:
                    this.d = this.az;
                    this.c = this.aD;
                    this.q = 0.0f;
                    b(0.0f);
                    this.e = this.aq;
                    this.l = this.am;
                    this.g = this.am;
                    this.j = this.ab;
                    this.k = this.ag;
                    this.h = this.L;
                    this.i = this.U;
                    return;
                case 4:
                default:
                    return;
                case 5:
                    this.d = this.aA;
                    this.c = this.aE;
                    this.q = 0.0f;
                    b(0.0f);
                    this.e = this.ar;
                    this.l = this.an;
                    this.g = this.an;
                    this.j = this.Z;
                    this.k = this.ah;
                    this.h = this.M;
                    this.i = this.V;
                    return;
                case 6:
                    this.d = this.az;
                    this.c = this.aD;
                    this.q = 0.0f;
                    b(0.0f);
                    this.e = this.aq;
                    this.l = this.am;
                    this.g = this.am;
                    this.j = this.ab;
                    this.k = this.ag;
                    this.h = this.L;
                    this.i = this.U;
                    return;
            }
        } else {
            switch (a()) {
                case 2:
                    this.d = this.ay;
                    this.c = this.aC;
                    float f3 = this.aj;
                    this.g = this.aj;
                    this.j = this.X;
                    this.k = this.ad;
                    float f4 = this.at;
                    if (z3) {
                        this.h = this.F;
                        this.i = this.O;
                    } else {
                        this.h = this.G;
                        this.i = this.P;
                    }
                    if (z2) {
                        a(f4, f3, 0.0f, 0.0f);
                        return;
                    }
                    this.q = 0.0f;
                    b(0.0f);
                    this.e = f4;
                    this.l = f3;
                    return;
                case 3:
                    this.d = this.az;
                    this.c = this.aD;
                    float f5 = this.ak;
                    this.g = this.ak;
                    this.j = this.Y;
                    this.k = this.ae;
                    float f6 = this.au;
                    if (z3) {
                        this.h = this.I;
                        this.i = this.R;
                    } else {
                        a("lSurfaceHeight" + this.p, 0);
                        a("lSurfaceWidth" + this.o, 0);
                        if (((double) (this.o / 9)) == ((double) this.p) / 18.5d || this.o / 135 == this.p / 271 || this.o / 1 == this.p / 2 || ((double) (((float) this.p) / ((float) this.o))) > 1.9d) {
                            this.h = this.J + 150.0f;
                            this.i = this.S + 150.0f;
                        } else {
                            this.h = this.J;
                            this.i = this.S;
                        }
                    }
                    if (z2) {
                        a(f6, f5, 0.0f, 0.0f);
                        return;
                    }
                    this.q = 0.0f;
                    b(0.0f);
                    this.e = f6;
                    this.l = f5;
                    return;
                case 4:
                default:
                    return;
                case 5:
                    this.d = this.aA;
                    this.c = this.aE;
                    float f7 = this.an;
                    this.g = this.an;
                    this.j = this.Z;
                    this.k = this.ah;
                    this.h = this.M;
                    this.i = this.V;
                    float f8 = this.aw;
                    if (z2) {
                        a(f8, f7, 0.0f, -89.0f);
                        return;
                    }
                    this.q = 0.0f;
                    b(-89.0f);
                    this.e = f8;
                    this.l = f7;
                    return;
                case 6:
                    this.d = this.az;
                    this.c = this.aD;
                    this.j = this.Y;
                    this.k = this.ae;
                    float f9 = this.au;
                    if (z3) {
                        f2 = this.ao;
                        this.g = this.ao;
                        this.h = this.I;
                        this.i = this.R;
                    } else if (((double) (this.o / 9)) == ((double) this.p) / 18.5d || this.o / 135 == this.p / 271 || this.o / 1 == this.p / 2 || ((double) (((float) this.p) / ((float) this.o))) > 1.9d) {
                        f2 = this.ao + 150.0f;
                        this.g = this.ao + 150.0f;
                        this.h = this.J + 150.0f;
                        this.i = this.S + 150.0f;
                    } else {
                        f2 = this.ao;
                        this.g = this.ao;
                        this.h = this.J;
                        this.i = this.S;
                    }
                    if (z2) {
                        a(f9, f2, 0.0f, 0.0f);
                        return;
                    }
                    this.q = 0.0f;
                    b(0.0f);
                    this.e = f9;
                    this.l = f2;
                    return;
            }
        }
    }

    private void f() {
        switch (this.f51a) {
            case 1:
                this.t = 25.0f;
                return;
            case 2:
                this.t = 15.0f;
                return;
            case 3:
                this.t = 25.0f;
                return;
            case 4:
            default:
                return;
            case 5:
                this.t = 25.0f;
                return;
            case 6:
                this.t = 25.0f;
                return;
        }
    }

    public void b(boolean z2) {
        a("setRotationFefault", 2);
        switch (this.f51a) {
            case 1:
                this.e = this.as;
                return;
            case 2:
                this.e = this.at;
                if (z2) {
                    this.h = this.F;
                    if (this.b == 1) {
                        this.h = this.H;
                        this.i = this.Q;
                    } else {
                        this.i = this.O;
                    }
                } else {
                    this.h = this.G;
                    this.i = this.P;
                }
                if (this.l > this.i) {
                    this.l = this.i;
                    return;
                }
                return;
            case 3:
                if (this.b == 1) {
                    this.h = this.L;
                    this.g = this.am;
                    this.k = this.ag;
                    this.i = this.U;
                    this.g = this.am;
                    this.e = this.aq;
                } else if (this.m != 2) {
                    if (z2) {
                        this.h = this.I;
                        this.i = this.R;
                    } else {
                        if (((double) (this.o / 9)) == ((double) this.p) / 18.5d || this.o / 135 == this.p / 271 || this.o / 1 == this.p / 2 || ((double) (((float) this.p) / ((float) this.o))) > 1.9d) {
                            this.h = this.J + 150.0f;
                            this.i = this.S + 150.0f;
                        } else {
                            this.h = this.J;
                            this.i = this.S;
                        }
                        if (this.m == 2) {
                            this.e = this.av;
                        } else {
                            this.e = this.au;
                        }
                        this.j = this.Y;
                        this.k = this.ae;
                    }
                }
                if (this.l > this.i) {
                    this.l = this.i;
                }
                if (this.l < this.k) {
                    this.l = this.k;
                    return;
                }
                return;
            case 4:
            default:
                return;
            case 5:
                this.d = this.aA;
                this.c = this.aE;
                this.j = this.Z;
                if (this.b == 1) {
                    this.h = this.L;
                    this.g = this.am;
                    this.q = 0.0f;
                    b(0.0f);
                    this.e = this.ar;
                } else {
                    this.e = this.aw;
                    this.h = this.M;
                    this.i = this.V;
                    this.g = this.an;
                    this.k = this.ah;
                    this.l = this.an;
                    this.q = 0.0f;
                    b(-89.0f);
                }
                if (this.l > this.i) {
                    this.l = this.i;
                }
                if (this.l < this.k) {
                    this.l = this.k;
                    return;
                }
                return;
            case 6:
                if (this.b == 1) {
                    this.h = this.L;
                    this.g = this.am;
                    this.k = this.ag;
                    this.i = this.U;
                    this.g = this.am;
                    this.e = this.aq;
                } else if (z2) {
                    this.h = this.I;
                    this.i = this.R;
                } else {
                    if (((double) (this.o / 9)) == ((double) this.p) / 18.5d || this.o / 135 == this.p / 271 || this.o / 1 == this.p / 2 || ((double) (((float) this.p) / ((float) this.o))) > 1.9d) {
                        this.h = this.J + 150.0f;
                        this.i = this.S + 150.0f;
                    } else {
                        this.h = this.J;
                        this.i = this.S;
                    }
                    if (this.m == 2) {
                        this.e = this.av;
                    } else {
                        this.e = this.au;
                    }
                }
                if (this.l > this.i) {
                    this.l = this.i;
                }
                if (this.l < this.k) {
                    this.l = this.k;
                    return;
                }
                return;
        }
    }

    public void d() {
        PropertyValuesHolder ofFloat;
        if (this.l > this.i) {
            ofFloat = PropertyValuesHolder.ofFloat("focalLength", this.l, this.i);
        } else if (this.l < this.k) {
            ofFloat = PropertyValuesHolder.ofFloat("focalLength", this.l, this.k);
        } else {
            this.B = false;
            return;
        }
        ValueAnimator duration = ValueAnimator.ofPropertyValuesHolder(ofFloat).setDuration(500L);
        duration.setInterpolator(new DecelerateInterpolator());
        duration.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            /* class GeneralFunction.Player.player.h.AnonymousClass1 */

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                h.this.l = ((Float) valueAnimator.getAnimatedValue("focalLength")).floatValue();
                h.this.y = true;
            }
        });
        duration.addListener(new AnimatorListenerAdapter() {
            /* class GeneralFunction.Player.player.h.AnonymousClass2 */

            public void onAnimationEnd(Animator animator) {
                h.this.B = false;
            }
        });
        duration.start();
    }

    private void a(float f2, float f3, float f4, float f5) {
        PropertyValuesHolder ofFloat = PropertyValuesHolder.ofFloat("fovY", this.e, f2);
        PropertyValuesHolder ofFloat2 = PropertyValuesHolder.ofFloat("focalLength", this.l, f3);
        ValueAnimator duration = ValueAnimator.ofPropertyValuesHolder(PropertyValuesHolder.ofFloat("xAxis", this.q, f4), PropertyValuesHolder.ofFloat("yAxis", this.r, f5), ofFloat).setDuration(900L);
        ValueAnimator duration2 = ValueAnimator.ofPropertyValuesHolder(ofFloat2).setDuration(400L);
        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator();
        duration.setInterpolator(decelerateInterpolator);
        duration2.setInterpolator(decelerateInterpolator);
        duration.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            /* class GeneralFunction.Player.player.h.AnonymousClass3 */

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                h.this.q = ((Float) valueAnimator.getAnimatedValue("xAxis")).floatValue();
                h.this.b(((Float) valueAnimator.getAnimatedValue("yAxis")).floatValue());
                h.this.e = ((Float) valueAnimator.getAnimatedValue("fovY")).floatValue();
                h.this.y = true;
            }
        });
        duration.start();
        duration2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            /* class GeneralFunction.Player.player.h.AnonymousClass4 */

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                h.this.l = ((Float) valueAnimator.getAnimatedValue("focalLength")).floatValue();
                h.this.y = true;
            }
        });
        duration2.start();
    }

    public void e() {
        if (this.aG != null) {
            this.aG.cancel();
        }
    }

    public void a(float f2, float f3) {
        e();
        this.aG = ValueAnimator.ofPropertyValuesHolder(PropertyValuesHolder.ofFloat("vx", f2, 0.0f), PropertyValuesHolder.ofFloat("vy", f3, 0.0f)).setDuration(1000L);
        this.aG.setInterpolator(new DecelerateInterpolator());
        this.aG.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            /* class GeneralFunction.Player.player.h.AnonymousClass5 */
            private long b = 0;

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                long currentPlayTime = valueAnimator.getCurrentPlayTime();
                long j = currentPlayTime - this.b;
                float floatValue = 0.9f * ((((Float) valueAnimator.getAnimatedValue("vx")).floatValue() * ((float) j)) / -1000.0f);
                float floatValue2 = ((((Float) valueAnimator.getAnimatedValue("vy")).floatValue() * ((float) j)) / -1000.0f) * 0.5f;
                this.b = currentPlayTime;
                if (h.this.s == 3) {
                    h.this.q = (((-floatValue) * 0.1f) + h.this.q) % 360.0f;
                    h.this.b(((-floatValue2) * 0.1f) + h.this.r);
                } else if (Math.abs(floatValue) > Math.abs(floatValue2)) {
                    h.this.q = (((-floatValue) * 0.1f) + h.this.q) % 360.0f;
                } else {
                    h.this.b(((-floatValue2) * 0.1f) + h.this.r);
                }
                h.this.y = true;
                h.this.y = true;
            }
        });
        this.aG.start();
    }
}
