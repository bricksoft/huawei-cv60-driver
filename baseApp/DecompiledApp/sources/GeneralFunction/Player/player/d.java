package GeneralFunction.Player.player;

import GeneralFunction.Player.player.SphericalVideoPlayer;
import android.graphics.Bitmap;
import android.os.Message;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private static final String f39a = d.class.getSimpleName();
    private BlockingQueue<Message> b = new ArrayBlockingQueue(90);
    private a c;
    private SphericalVideoPlayer.g d;
    private SphericalVideoPlayer.c e;

    private static void a(String str, int i) {
        b.a(f39a, str, i);
    }

    public void a(int i, SphericalVideoPlayer.g gVar) {
        this.d = gVar;
        this.c = new a();
        this.c.a(i);
        this.c.start();
    }

    public void a(Bitmap bitmap, int i, boolean z) {
        if (bitmap != null) {
            Message message = new Message();
            SphericalVideoPlayer.g gVar = this.d;
            message.what = 8;
            message.obj = new a(bitmap, i, z);
            try {
                this.b.put(message);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        } else {
            a("360 Player can't receive Bitmap ", 0);
        }
    }

    public void a(SphericalVideoPlayer.c cVar) {
        this.e = cVar;
    }

    public void a() {
        this.c.a();
    }

    public void b() {
        this.c.b();
    }

    public void a(int i) {
        this.c.a(i);
    }

    public void c() {
        if (this.c != null) {
            this.c.a();
            this.c.interrupt();
            this.c = null;
            this.b.clear();
        }
    }

    private class a extends Thread {
        private int b;
        private int c;
        private long d;
        private boolean e;

        private a() {
            this.b = 30;
            this.c = a(this.b, 0);
            this.d = 0;
            this.e = false;
        }

        public void a(int i) {
            this.b = i;
            this.c = a(this.b, 0);
        }

        private int a(int i, int i2) {
            float f = (float) i;
            return (int) (1000.0f / ((f + ((((float) i2) / (f / 6.0f)) * (f / 15.0f))) + 1.0f));
        }

        public void run() {
            while (!Thread.interrupted()) {
                if (!this.e && !d.this.b.isEmpty()) {
                    try {
                        Message message = (Message) d.this.b.poll(50, TimeUnit.MICROSECONDS);
                        this.c = a(this.b, d.this.b.size());
                        long currentTimeMillis = ((long) this.c) - (System.currentTimeMillis() - this.d);
                        if (currentTimeMillis > 0) {
                            Thread.sleep(currentTimeMillis);
                        }
                        this.d = System.currentTimeMillis();
                        d.this.e.a(message);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

        public void a() {
            this.e = true;
        }

        public void b() {
            this.e = false;
        }
    }
}
