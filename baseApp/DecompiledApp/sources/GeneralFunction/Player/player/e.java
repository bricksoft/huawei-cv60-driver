package GeneralFunction.Player.player;

import GeneralFunction.d;
import GeneralFunction.k;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import java.util.Formatter;
import java.util.Locale;

public class e extends MediaController {
    private static final String b = e.class.getSimpleName();
    private ImageButton A = null;
    private ImageButton B = null;
    private LinearLayout C = null;
    private LinearLayout D = null;
    private LinearLayout E = null;
    private LinearLayout F = null;
    private boolean G = false;
    private boolean H = false;
    private long I = -1;
    private boolean J = false;
    private Handler K = new Handler(Looper.getMainLooper()) {
        /* class GeneralFunction.Player.player.e.AnonymousClass1 */

        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 1:
                    e.this.o.o();
                    e.this.H = false;
                    long currentTimeMillis = System.currentTimeMillis() - e.this.I;
                    if (currentTimeMillis >= 200) {
                        e.this.I = System.currentTimeMillis();
                        if (e.this.o != null) {
                            e.this.G = false;
                            e.this.o.seekTo(e.this.u);
                            e.this.g();
                            return;
                        }
                        return;
                    } else if (!e.this.H) {
                        e.this.H = true;
                        e.this.G = true;
                        e.this.a((e) 1, (int) (200 - currentTimeMillis));
                        return;
                    } else {
                        return;
                    }
                case 2:
                    e.this.c.setImageResource(R.drawable.gallery_bottom_play);
                    return;
                case 3:
                    e.this.d.setEnabled(true);
                    return;
                case 4:
                    e.this.d.setEnabled(false);
                    return;
                default:
                    return;
            }
        }
    };
    private SeekBar.OnSeekBarChangeListener L = new SeekBar.OnSeekBarChangeListener() {
        /* class GeneralFunction.Player.player.e.AnonymousClass2 */

        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            if (z) {
                long duration = (((long) e.this.o.getDuration()) * ((long) i)) / 1000;
                if (!e.this.H) {
                    e.this.H = true;
                    e.this.a((e) 1, 0);
                }
                e.this.u = (int) duration;
                e.this.a((e) ("================" + Integer.toString((int) duration) + "==========="), (String) 4);
                if (e.this.f != null) {
                    e.this.f.setText(e.this.c((e) ((int) duration)));
                }
            }
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            e.this.show();
            e.this.k = true;
            e.this.removeCallbacks(e.this.M);
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            e.this.k = false;
            e.this.g();
            e.this.h();
            e.this.show();
            e.this.removeCallbacks(e.this.M);
            e.this.post(e.this.M);
        }
    };
    private final Runnable M = new Runnable() {
        /* class GeneralFunction.Player.player.e.AnonymousClass3 */

        public void run() {
            if (!e.this.k && e.this.p && e.this.l) {
                e.this.g();
                e.this.postDelayed(e.this.M, 50);
            }
        }
    };
    private View.OnClickListener N = new View.OnClickListener() {
        /* class GeneralFunction.Player.player.e.AnonymousClass4 */

        public void onClick(View view) {
            e.this.a();
            if (e.this.o.isPlaying()) {
                e.this.o.pause();
            } else {
                e.this.o.start();
            }
            e.this.show();
        }
    };
    private View.OnClickListener O = new View.OnClickListener() {
        /* class GeneralFunction.Player.player.e.AnonymousClass5 */

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.LL_customMediaController_LayoutViewType /*{ENCODED_INT: 2131755206}*/:
                case R.id.IB_customMediaController_btnViewType /*{ENCODED_INT: 2131755207}*/:
                    if (!e.this.x) {
                        e.this.o.c(0);
                        e.this.x = true;
                        e.this.e((e) true);
                        return;
                    }
                    e.this.x = false;
                    e.this.e((e) false);
                    return;
                case R.id.LL_customMediaController_LayoutSnapshot /*{ENCODED_INT: 2131755208}*/:
                case R.id.IB_customMediaController_btnSnapshot /*{ENCODED_INT: 2131755209}*/:
                    e.this.o.c(1);
                    return;
                case R.id.LL_customMediaController_LayoutEdit /*{ENCODED_INT: 2131755210}*/:
                case R.id.IB_customMediaController_btnEdit /*{ENCODED_INT: 2131755211}*/:
                    if (!e.this.x) {
                        e.this.o.c(2);
                        e.this.A.setImageResource(R.drawable.gallery_bottom_edit_green);
                        e.this.x = true;
                        return;
                    }
                    e.this.A.setImageResource(R.drawable.gallery_bottom_edit);
                    e.this.x = false;
                    return;
                case R.id.LL_customMediaController_LayoutDrag /*{ENCODED_INT: 2131755212}*/:
                case R.id.IB_customMediaController_btnDrag /*{ENCODED_INT: 2131755213}*/:
                    if (!e.this.x) {
                        e.this.o.c(3);
                        e.this.x = true;
                        e.this.d((e) true);
                        return;
                    }
                    e.this.x = false;
                    e.this.d((e) false);
                    return;
                default:
                    return;
            }
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public int f41a = 0;
    private ImageButton c = null;
    private ProgressBar d = null;
    private TextView e;
    private TextView f;
    private StringBuilder g;
    private Formatter h;
    private Context i;
    private View j;
    private boolean k = false;
    private boolean l = false;
    private int m = 0;
    private int n = 0;
    private SphericalVideoPlayer o;
    private boolean p = false;
    private int q = 3;
    private int r = 0;
    private boolean s = false;
    private boolean t = false;
    private int u = 0;
    private int v = 0;
    private boolean w = false;
    private boolean x = false;
    private ImageButton y = null;
    private ImageButton z = null;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        d.a(b, str, i2);
    }

    public e(Context context, SphericalVideoPlayer sphericalVideoPlayer) {
        super(context);
        this.i = context;
        this.o = sphericalVideoPlayer;
    }

    public void show() {
        show(this.f41a);
    }

    public void show(int i2) {
        removeCallbacks(this.M);
        c(true);
        post(this.M);
        super.show(this.f41a);
    }

    public void hide() {
        removeCallbacks(this.M);
        c(false);
        super.hide();
    }

    public void setControllerHide(boolean z2) {
        if (z2) {
            hide();
            this.p = false;
            return;
        }
        show();
        this.p = true;
    }

    public void a(boolean z2) {
        this.J = z2;
    }

    private void e() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(getLayoutParams());
        layoutParams.setMargins(0, 0, 0, (-this.n) / 20);
        setLayoutParams(layoutParams);
    }

    private void f() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(getLayoutParams());
        layoutParams.setMargins(0, 0, 0, 0);
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        if (!this.J || displayMetrics.heightPixels <= displayMetrics.widthPixels) {
            e();
        } else {
            f();
        }
    }

    /* access modifiers changed from: protected */
    public View makeControllerView() {
        this.j = ((LayoutInflater) this.i.getSystemService("layout_inflater")).inflate(R.layout.custom_media_controller, (ViewGroup) null);
        a(this.j);
        return this.j;
    }

    private void a(View view) {
        this.c = (ImageButton) view.findViewById(R.id.IB_Media_Control);
        if (this.c != null) {
            this.c.requestFocus();
            this.c.setOnClickListener(this.N);
        }
        this.d = (SeekBar) view.findViewById(R.id.SB_Mediacontroller_Progress);
        if (this.d != null) {
            if (this.d instanceof SeekBar) {
                ((SeekBar) this.d).setOnSeekBarChangeListener(this.L);
            }
            this.d.setMax(1000);
        }
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.m = displayMetrics.widthPixels;
        this.n = displayMetrics.heightPixels;
        int a2 = k.a(this.i, (float) (Math.max(this.m, this.n) / 38));
        this.e = (TextView) view.findViewById(R.id.TV_EndTime);
        this.e.setTextSize((float) a2);
        this.f = (TextView) view.findViewById(R.id.TV_CurrentTime);
        this.f.setTextSize((float) a2);
        this.g = new StringBuilder();
        this.h = new Formatter(this.g, Locale.getDefault());
        b(view);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        boolean z2 = keyEvent.getRepeatCount() == 0 && keyEvent.getAction() == 0;
        if (keyCode == 79 || keyCode == 85 || keyCode == 62) {
            if (!z2) {
                return true;
            }
            this.o.c(5);
            if (this.c == null) {
                return true;
            }
            this.c.requestFocus();
            return true;
        } else if (keyCode == 126) {
            if (!z2 || this.o.isPlaying()) {
                return true;
            }
            this.o.start();
            h();
            show();
            return true;
        } else if (keyCode == 86 || keyCode == 127) {
            if (!z2 || !this.o.isPlaying()) {
                return true;
            }
            this.o.pause();
            h();
            show();
            return true;
        } else if (keyCode == 25 || keyCode == 24 || keyCode == 164 || keyCode == 27) {
            return super.dispatchKeyEvent(keyEvent);
        } else {
            if (keyCode == 4) {
                if (!z2) {
                    return true;
                }
                this.o.c(4);
                return true;
            } else if (keyCode == 82) {
                return true;
            } else {
                show();
                return super.dispatchKeyEvent(keyEvent);
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, int i3) {
        Message message = new Message();
        message.what = i2;
        if (i3 > 0) {
            this.K.sendMessageDelayed(message, (long) i3);
        } else {
            this.K.sendMessage(message);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int g() {
        if (this.o == null || this.k) {
            return 0;
        }
        int currentPosition = this.o.getCurrentPosition();
        int duration = this.o.getDuration();
        if (currentPosition > duration) {
            currentPosition = duration;
        }
        if (duration == 0) {
            duration = 1;
        }
        if (this.w) {
            this.d.setProgress((int) ((((long) currentPosition) * 1000) / ((long) duration)));
            this.w = false;
            this.e.setText(c(duration));
            this.f.setText(c(currentPosition));
            this.u = currentPosition;
            return currentPosition;
        } else if (this.v > 0) {
            this.v--;
            return currentPosition;
        } else {
            if (this.t) {
                if (this.u > currentPosition) {
                    return this.u;
                }
                if (this.d != null) {
                    if (duration > 0) {
                        long j2 = (((long) currentPosition) * 1000) / ((long) duration);
                        if (this.l && !this.G) {
                            this.d.setProgress((int) j2);
                        }
                    }
                    int bufferPercentage = this.o.getBufferPercentage();
                    if (this.l) {
                        this.d.setSecondaryProgress(bufferPercentage * 10);
                    }
                }
                if (this.e != null) {
                    this.e.setText(c(duration));
                }
                if (this.f != null) {
                    this.f.setText(c(currentPosition));
                }
            }
            return currentPosition;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private String c(int i2) {
        int i3 = i2 / 1000;
        int i4 = i3 % 60;
        int i5 = (i3 / 60) % 60;
        int i6 = i3 / 3600;
        this.g.setLength(0);
        if (i6 > 0) {
            return this.h.format("%d:%02d:%02d", Integer.valueOf(i6), Integer.valueOf(i5), Integer.valueOf(i4)).toString();
        }
        return this.h.format("%02d:%02d", Integer.valueOf(i5), Integer.valueOf(i4)).toString();
    }

    public void a() {
        if (this.l) {
            this.l = false;
            this.v = 3;
        } else {
            this.l = true;
            this.v = 3;
        }
        h();
    }

    public void a(int i2) {
        this.v = i2;
    }

    public void b(boolean z2) {
        this.t = z2;
        if (!z2) {
            this.u = -1;
        }
    }

    public void c(boolean z2) {
        if (z2) {
            a(3, 0);
        } else {
            a(4, 0);
        }
    }

    public void b() {
        a(2, 0);
        if (this.d != null) {
            this.d.setProgress(0);
        }
        this.u = -1;
        this.l = false;
        this.k = false;
    }

    public void b(int i2) {
        if (i2 == 0) {
            this.d.setProgress(0);
        } else {
            this.w = true;
        }
        int duration = this.o.getDuration();
        if (i2 > duration) {
            i2 = duration;
        }
        if (this.f != null) {
            this.f.setText(c(i2));
        }
        if (this.e != null) {
            this.e.setText(c(duration));
        }
        g();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void h() {
        if (this.j != null && this.c != null) {
            this.o.o();
            if (this.l) {
                this.c.setImageResource(R.drawable.gallery_bottom_pause);
            } else {
                this.c.setImageResource(R.drawable.gallery_bottom_play);
            }
        }
    }

    public void setMediaControllerShowingStatus(boolean z2) {
        this.p = z2;
    }

    public boolean getMediaControllerShowingStatus() {
        return this.p;
    }

    private void b(View view) {
        this.y = (ImageButton) view.findViewById(R.id.IB_customMediaController_btnViewType);
        this.z = (ImageButton) view.findViewById(R.id.IB_customMediaController_btnSnapshot);
        this.A = (ImageButton) view.findViewById(R.id.IB_customMediaController_btnEdit);
        this.B = (ImageButton) view.findViewById(R.id.IB_customMediaController_btnDrag);
        this.C = (LinearLayout) view.findViewById(R.id.LL_customMediaController_LayoutViewType);
        this.D = (LinearLayout) view.findViewById(R.id.LL_customMediaController_LayoutSnapshot);
        this.E = (LinearLayout) view.findViewById(R.id.LL_customMediaController_LayoutEdit);
        this.F = (LinearLayout) view.findViewById(R.id.LL_customMediaController_LayoutDrag);
        this.y.setImageResource(R.drawable.gallery_bottom_fisheye_white);
        this.z.setImageResource(R.drawable.gallery_bottom_snapshot);
        this.A.setImageResource(R.drawable.gallery_bottom_edit);
        this.B.setImageResource(R.drawable.gallery_bottom_drag_white);
        this.y.setOnClickListener(this.O);
        this.z.setOnClickListener(this.O);
        this.A.setOnClickListener(this.O);
        this.B.setOnClickListener(this.O);
        this.C.setOnClickListener(this.O);
        this.D.setOnClickListener(this.O);
        this.E.setOnClickListener(this.O);
        this.F.setOnClickListener(this.O);
    }

    public void c() {
        this.y.setImageResource(R.drawable.gallery_bottom_fisheye_white);
        this.z.setImageResource(R.drawable.gallery_bottom_snapshot);
        this.A.setImageResource(R.drawable.gallery_bottom_edit);
        this.B.setImageResource(R.drawable.gallery_bottom_drag_white);
    }

    public void d() {
        this.A.setImageResource(R.drawable.gallery_bottom_edit);
        this.x = false;
        e(false);
        d(false);
    }

    private void setAdditionalButtonVRLock(boolean z2) {
        if (z2) {
            this.C.setEnabled(false);
            this.y.setEnabled(false);
            this.y.setImageAlpha(100);
            this.D.setEnabled(false);
            this.z.setEnabled(false);
            this.z.setImageAlpha(100);
            this.E.setEnabled(false);
            this.A.setEnabled(false);
            this.A.setImageAlpha(100);
            return;
        }
        this.C.setEnabled(true);
        this.y.setEnabled(true);
        this.y.setImageAlpha(255);
        this.D.setEnabled(true);
        this.z.setEnabled(true);
        this.z.setImageAlpha(255);
        this.E.setEnabled(true);
        this.A.setEnabled(true);
        this.A.setImageAlpha(255);
    }

    public void a(int i2, boolean z2) {
        this.r = i2;
        d(z2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void d(boolean z2) {
        switch (this.r) {
            case 0:
                setAdditionalButtonVRLock(false);
                return;
            case 1:
                setAdditionalButtonVRLock(false);
                if (z2) {
                    this.B.setImageResource(R.drawable.gallery_bottom_gyro_green);
                    return;
                } else {
                    this.B.setImageResource(R.drawable.gallery_bottom_gyro_white);
                    return;
                }
            case 2:
                setAdditionalButtonVRLock(false);
                if (z2) {
                    this.B.setImageResource(R.drawable.gallery_bottom_drag_green);
                    return;
                } else {
                    this.B.setImageResource(R.drawable.gallery_bottom_drag_white);
                    return;
                }
            case 3:
                if (z2) {
                    this.B.setImageResource(R.drawable.gallery_bottom_vr_green);
                } else {
                    this.B.setImageResource(R.drawable.gallery_bottom_vr_white);
                }
                setAdditionalButtonVRLock(true);
                return;
            default:
                return;
        }
    }

    public void b(int i2, boolean z2) {
        this.q = i2;
        e(z2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void e(boolean z2) {
        switch (this.q) {
            case 2:
                if (z2) {
                    this.y.setImageResource(R.drawable.gallery_bottom_perspective_green);
                    return;
                } else {
                    this.y.setImageResource(R.drawable.gallery_bottom_perspective_white);
                    return;
                }
            case 3:
                if (z2) {
                    this.y.setImageResource(R.drawable.gallery_bottom_fisheye_green);
                    return;
                } else {
                    this.y.setImageResource(R.drawable.gallery_bottom_fisheye_white);
                    return;
                }
            case 4:
            default:
                return;
            case 5:
                if (z2) {
                    this.y.setImageResource(R.drawable.gallery_bottom_littleplanet_green);
                    return;
                } else {
                    this.y.setImageResource(R.drawable.gallery_bottom_littleplanet_white);
                    return;
                }
            case 6:
                if (z2) {
                    this.y.setImageResource(R.drawable.gallery_bottom_crystalball_green);
                    return;
                } else {
                    this.y.setImageResource(R.drawable.gallery_bottom_crystalball_white);
                    return;
                }
        }
    }
}
