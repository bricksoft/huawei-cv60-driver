package GeneralFunction.Player.player;

import GeneralFunction.Player.player.f;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Choreographer;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.MediaController;
import java.io.IOException;
import java.util.Map;
import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public class SphericalVideoPlayer extends TextureView implements TextureView.SurfaceTextureListener, MediaController.MediaPlayerControl {
    private static final String b = SphericalVideoPlayer.class.getSimpleName();
    private i A;
    private int B;
    private int C;
    private Surface D;

    /* renamed from: a  reason: collision with root package name */
    protected IjkMediaPlayer f22a;
    private h c;
    private int d;
    private int e;
    private boolean f;
    private boolean g;
    private boolean h;
    private boolean i;
    private boolean j;
    private boolean k;
    private int l;
    private int m;
    private final String n;
    private e o;
    private c p;
    private d q;
    private g r;
    private String s;
    private long t;
    private SurfaceTexture u;
    private f v;
    private d w;
    private e x;
    private a y;
    private f z;

    public interface a {
        void a();

        void a(int i);
    }

    public interface b {
        void a(Message message);

        void a(MotionEvent motionEvent);

        void b(Message message);

        void c(Message message);

        void d(Message message);

        void e(Message message);
    }

    public interface c {
        void a(Message message);
    }

    public interface d {
        void a(MotionEvent motionEvent);
    }

    public interface e {
        void a();
    }

    public interface f {
        void a(int i);
    }

    /* access modifiers changed from: private */
    public static void b(String str, int i2) {
        b.a(b, str, i2);
    }

    public void setViewType(int i2) {
        if (this != null) {
            Message obtain = Message.obtain();
            obtain.what = 10;
            obtain.obj = Integer.valueOf(i2);
            a(obtain);
        }
    }

    public void setViewMode(int i2) {
        if (this.c.b == i2) {
            b("already same viewMode", 2);
            return;
        }
        Message obtain = Message.obtain();
        obtain.what = 13;
        obtain.obj = Integer.valueOf(i2);
        a(obtain);
    }

    public void a() {
        Message obtain = Message.obtain();
        obtain.what = 22;
        a(obtain);
    }

    public void setIsShowThumbNail(boolean z2) {
        if (this != null) {
            this.h = z2;
            b("isShow:" + z2, 3);
            Message obtain = Message.obtain();
            obtain.what = 14;
            obtain.obj = Boolean.valueOf(z2);
            a(obtain);
        }
    }

    public void a(int i2, int i3) {
        b("OrientationChange:" + i2 + " " + i3, 3);
    }

    public h getSphericalParameter() {
        return this.c;
    }

    public void setSphericalParameter(h hVar) {
        Message obtain = Message.obtain();
        obtain.what = 21;
        obtain.obj = hVar;
        a(obtain);
    }

    public void b() {
        Message obtain = Message.obtain();
        obtain.what = 20;
        a(obtain);
    }

    private void p() {
        Message obtain = Message.obtain();
        obtain.what = 24;
        a(obtain);
    }

    public void setPlayerStatusListener(f fVar) {
        this.v = fVar;
    }

    public void setPlayerDrawFirstFrameListener(e eVar) {
        this.x = eVar;
    }

    public void setOnClickListener(d dVar) {
        this.w = dVar;
    }

    public void setEnable(boolean z2) {
        this.f = z2;
        this.g = z2;
    }

    public boolean isEnabled() {
        return this.f;
    }

    public void setZoomInEnable(boolean z2) {
        this.g = z2;
    }

    public boolean c() {
        return this.g;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void d(int i2) {
        if (this.v != null) {
            this.v.a(i2);
        }
    }

    public int d() {
        return this.d;
    }

    public void a(int i2) {
        this.d = i2;
    }

    public void start() {
        if (this.c.m == 1 && this.f22a != null) {
            a(2);
            d(2);
            if (this.k) {
                this.f22a.setVolume(1.0f, 1.0f);
            } else {
                this.f22a.setVolume(0.0f, 0.0f);
            }
            this.f22a.start();
            if (this.o != null) {
                this.o.b(true);
            }
        }
    }

    public void e() {
        if (this.c.m == 1 && this.f22a != null) {
            this.o.a(3);
            this.f22a.start();
            if (this.k) {
                this.f22a.setVolume(1.0f, 1.0f);
            } else {
                this.f22a.setVolume(0.0f, 0.0f);
            }
        }
    }

    public void pause() {
        if (this.c.m == 1 && this.f22a != null && this.f22a.isPlaying()) {
            this.f22a.pause();
            this.f22a.setVolume(0.0f, 0.0f);
        }
    }

    public int getDuration() {
        if (this.c.m != 1 || this.f22a == null) {
            return 0;
        }
        return (int) this.f22a.getDuration();
    }

    public int getCurrentPosition() {
        if (this.c.m != 1 || this.f22a == null) {
            return 0;
        }
        if (this.f22a.getDuration() != this.f22a.getCurrentPosition()) {
            return (int) this.f22a.getCurrentPosition();
        }
        return (int) this.f22a.getCurrentPosition();
    }

    public void seekTo(int i2) {
        if (this.c.m == 1 && this.f22a != null) {
            try {
                this.f22a.seekTo((long) i2);
            } catch (IllegalStateException e2) {
                e2.printStackTrace();
            }
        }
    }

    public boolean isPlaying() {
        if (this.c.m != 1 || this.f22a == null) {
            return false;
        }
        return this.f22a.isPlaying();
    }

    public int getBufferPercentage() {
        if (this.c.m != 1 || this.f22a == null || this.f22a.getDuration() == 0) {
            return 0;
        }
        return (((((int) this.f22a.getCurrentPosition()) * 100) / ((int) this.f22a.getDuration())) / 2) * 2;
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return true;
    }

    public boolean canSeekForward() {
        return true;
    }

    public int getAudioSessionId() {
        return 0;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        b("motionStrategy unregisterSensor", 3);
        if (this.z != null) {
            this.z.a();
            this.z = null;
        }
    }

    public SphericalVideoPlayer(Context context) {
        this(context, null);
        setSurfaceTextureListener(this);
    }

    public SphericalVideoPlayer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        setSurfaceTextureListener(this);
    }

    public SphericalVideoPlayer(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = new h();
        this.d = 0;
        this.e = 30;
        this.f = true;
        this.g = true;
        this.h = false;
        this.i = true;
        this.j = false;
        this.k = true;
        this.l = 1920;
        this.m = 960;
        this.n = null;
        this.v = null;
        this.w = null;
        this.x = null;
        this.y = null;
        this.z = null;
        this.B = 0;
        this.C = 0;
        this.D = null;
        setSurfaceTextureListener(this);
        this.c.a(context);
        a(0);
        this.o = new e(context, this);
        this.o.setMediaPlayer(this);
        this.o.setAnchorView(this);
        this.o.setEnabled(true);
        this.o.a(ui_Controller.b.d.a().b());
        this.A = new i(context);
        this.A.a(new b() {
            /* class GeneralFunction.Player.player.SphericalVideoPlayer.AnonymousClass1 */

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.b
            public void a(Message message) {
                SphericalVideoPlayer.this.a((SphericalVideoPlayer) message);
            }

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.b
            public void b(Message message) {
                if (SphericalVideoPlayer.this.isEnabled()) {
                    SphericalVideoPlayer.this.a((SphericalVideoPlayer) message);
                }
            }

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.b
            public void a(MotionEvent motionEvent) {
                if (SphericalVideoPlayer.this.w != null) {
                    SphericalVideoPlayer.this.w.a(motionEvent);
                    if (SphericalVideoPlayer.this.c.m == 1) {
                        motionEvent.getAction();
                    }
                }
            }

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.b
            public void c(Message message) {
                SphericalVideoPlayer.this.a((SphericalVideoPlayer) message);
            }

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.b
            public void d(Message message) {
                if (SphericalVideoPlayer.this.c()) {
                    SphericalVideoPlayer.this.a((SphericalVideoPlayer) message);
                }
            }

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.b
            public void e(Message message) {
                SphericalVideoPlayer.this.a((SphericalVideoPlayer) message);
            }
        });
        this.A.a(this.c, this.r);
        setOnTouchListener(new View.OnTouchListener() {
            /* class GeneralFunction.Player.player.SphericalVideoPlayer.AnonymousClass2 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return SphericalVideoPlayer.this.A.a(motionEvent);
            }
        });
        if (this.z == null) {
            b("motionStrategy registerSensor", 3);
            this.z = new f();
            this.z.a(getContext(), this.c);
            this.z.a(new f.c() {
                /* class GeneralFunction.Player.player.SphericalVideoPlayer.AnonymousClass3 */

                @Override // GeneralFunction.Player.player.f.c
                public void a(Message message) {
                    if (SphericalVideoPlayer.this.r != null) {
                        SphericalVideoPlayer.this.a((SphericalVideoPlayer) message);
                    }
                }
            });
        }
    }

    public void a(boolean z2) {
        this.i = z2;
    }

    public void f() {
        if (this.r == null) {
            this.r = new g("360RenderThread");
            this.r.start();
        }
    }

    public int g() {
        b("Play:" + d(), 3);
        if (d() != 1) {
            b("Invalid operation in status:" + d(), 0);
            return -1;
        } else if (this.c.m == 0) {
            b("Invalid operation in mode:" + this.c.m, 0);
            return -1;
        } else {
            SurfaceTexture surfaceTexture = this.u;
            b("[Video Player]Play:" + surfaceTexture, 2);
            if (surfaceTexture != null) {
                a(surfaceTexture, this.c.o, this.c.p);
            }
            return 0;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void q() {
        this.q = new d();
        this.q.a(this.e, this.r);
        this.q.a(new c() {
            /* class GeneralFunction.Player.player.SphericalVideoPlayer.AnonymousClass4 */

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.c
            public void a(Message message) {
                SphericalVideoPlayer.this.a((SphericalVideoPlayer) message);
            }
        });
        b("MJPGThread is start", 3);
        a(2);
        d(2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void r() {
        Log.e(b, "livePlay++++");
        this.p = new c();
        this.p.a(this.r.a(), this.l, this.m, this.e);
        a(2);
        d(2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str) {
        if (this.r == null) {
            throw new IllegalStateException("RenderThread has not been initialized");
        } else if (TextUtils.isEmpty(str)) {
            throw new RuntimeException("Cannot begin playback: video path is empty");
        } else {
            this.f22a = new IjkMediaPlayer();
            this.f22a.setOption(4, "mediacodec", 1);
            this.f22a.setOption(4, "mediacodec-handle-resolution-change", 1);
            this.f22a.setOption(4, "mediacodec-avc", 1);
            this.f22a.setOption(4, "start-on-prepared", 1);
            this.f22a.setLogEnabled(false);
            this.f22a.setKeepInBackground(true);
            this.f22a.setVolume(0.0f, 0.0f);
            Surface a2 = this.r.a();
            if (a2.isValid()) {
                this.f22a.setSurface(a2);
            } else {
                b("is fail Surface", 1);
            }
            this.f22a.setAudioStreamType(3);
            try {
                this.f22a.setDataSource(getContext(), Uri.parse(str), (Map<String, String>) null);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.f22a.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
                /* class GeneralFunction.Player.player.SphericalVideoPlayer.AnonymousClass5 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnPreparedListener
                public void onPrepared(IMediaPlayer iMediaPlayer) {
                    SphericalVideoPlayer.b("ijkplayer+++++++onPreparedListener:" + iMediaPlayer.isPlaying(), 1);
                }
            });
            this.f22a.setOnErrorListener(new IMediaPlayer.OnErrorListener() {
                /* class GeneralFunction.Player.player.SphericalVideoPlayer.AnonymousClass6 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnErrorListener
                public boolean onError(IMediaPlayer iMediaPlayer, int i, int i2) {
                    SphericalVideoPlayer.b("ijkplayer+++++++onError:" + iMediaPlayer.isPlaying() + ",what " + i + ",extra " + i2, 1);
                    return false;
                }
            });
            this.f22a.setOnInfoListener(new IMediaPlayer.OnInfoListener() {
                /* class GeneralFunction.Player.player.SphericalVideoPlayer.AnonymousClass7 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnInfoListener
                public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i2) {
                    SphericalVideoPlayer.b("+++++++++OnInfoListener:" + iMediaPlayer.isPlaying() + ",what " + i + ",extra " + i2, 3);
                    switch (i) {
                        case 3:
                            if (iMediaPlayer.isPlaying()) {
                                return false;
                            }
                            SphericalVideoPlayer.this.b();
                            return false;
                        case IMediaPlayer.MEDIA_INFO_BUFFERING_END /*{ENCODED_INT: 702}*/:
                            if (iMediaPlayer.isPlaying()) {
                                return false;
                            }
                            SphericalVideoPlayer.this.b();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.f22a.setOnBufferingUpdateListener(new IMediaPlayer.OnBufferingUpdateListener() {
                /* class GeneralFunction.Player.player.SphericalVideoPlayer.AnonymousClass8 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnBufferingUpdateListener
                public void onBufferingUpdate(IMediaPlayer iMediaPlayer, int i) {
                }
            });
            this.f22a.prepareAsync();
            this.j = true;
            this.f22a.setOnCompletionListener(new IMediaPlayer.OnCompletionListener() {
                /* class GeneralFunction.Player.player.SphericalVideoPlayer.AnonymousClass9 */

                @Override // tv.danmaku.ijk.media.player.IMediaPlayer.OnCompletionListener
                public void onCompletion(IMediaPlayer iMediaPlayer) {
                    SphericalVideoPlayer.this.o.b(false);
                    SphericalVideoPlayer.this.f22a.pause();
                    SphericalVideoPlayer.this.f22a.setVolume(0.0f, 0.0f);
                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    SphericalVideoPlayer.this.a(2);
                    SphericalVideoPlayer.this.d((SphericalVideoPlayer) 3);
                }
            });
        }
    }

    public void b(int i2) {
        this.o.b(i2);
    }

    public void h() {
        this.o.a();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void s() {
        if (!this.f22a.isPlaying()) {
            this.f22a.start();
        }
        a(2);
        d(2);
    }

    public void a(SurfaceTexture surfaceTexture, int i2, int i3) {
        b("----StartPlay---", 3);
        Message obtain = Message.obtain();
        obtain.what = 1;
        obtain.obj = surfaceTexture;
        obtain.arg1 = i2;
        obtain.arg2 = i3;
        a(obtain);
    }

    public void a(GeneralFunction.c.d dVar, int i2, int i3) {
        if (!(i2 == this.l && i3 == this.m)) {
            this.l = i2;
            this.m = i3;
            Message obtain = Message.obtain();
            obtain.what = 11;
            obtain.arg1 = this.l;
            obtain.arg2 = this.m;
            a(obtain);
        }
        this.t = System.currentTimeMillis();
        if (d() == 2) {
            this.p.a(dVar, i2, i3);
        } else {
            b("360 Player can't receive Buffer", 0);
        }
    }

    public void a(Bitmap bitmap, int i2, boolean z2) {
        this.t = System.currentTimeMillis();
        if (bitmap == null) {
            Log.e(b, "bitmap is null");
        }
        if (d() != 2) {
            b("360 Player can't receive Bitmap ", 0);
        } else if (this.q != null) {
            this.q.a(bitmap, i2, z2);
        }
    }

    public void i() {
        c(false);
    }

    private void c(boolean z2) {
        b("releaseResources " + z2, 3);
        this.D = null;
        if (this.r != null) {
            Message message = new Message();
            message.what = 4;
            message.obj = Boolean.valueOf(z2);
            a(message);
        }
    }

    public void setVideoFilePath(String str) {
        this.s = str;
    }

    public int j() {
        if (d() != 2) {
            b("Invalid operation(flushDecoder) in status:" + d(), 0);
            return -1;
        }
        switch (this.c.m) {
            case 2:
                this.p.a();
                return 0;
            default:
                b("playerFormat not handle flushDecoder:" + this.c.m, 0);
                return -2;
        }
    }

    public int k() {
        if (d() != 2) {
            b("Invalid operation in status:" + d(), 0);
            return -1;
        }
        switch (this.c.m) {
            case 1:
                if (this.f22a != null) {
                    if (this.f22a.isPlaying()) {
                        this.f22a.pause();
                        break;
                    }
                } else {
                    b("objMediaPlayerInternal is null:", 0);
                    break;
                }
                break;
            case 2:
                this.p.b();
                p();
                break;
            case 3:
                this.q.a();
                if (this.f22a != null) {
                    this.f22a.pause();
                    break;
                }
                break;
        }
        a(4);
        d(4);
        return 0;
    }

    public int l() {
        if (d() != 4) {
            b("Invalid operation in status:" + d(), 0);
            return -1;
        }
        switch (this.c.m) {
            case 1:
                this.f22a.start();
                break;
            case 2:
                this.p.c();
                break;
            case 3:
                this.q.b();
                if (this.f22a != null) {
                    this.f22a.start();
                    break;
                }
                break;
        }
        a(2);
        d(2);
        return 0;
    }

    public void setFrameRate(int i2) {
        this.e = i2;
        switch (this.c.m) {
            case 1:
            default:
                return;
            case 2:
                this.p.a(i2);
                return;
            case 3:
                this.q.a(i2);
                return;
        }
    }

    public void b(int i2, int i3) {
        this.l = i2;
        this.m = i3;
        Message obtain = Message.obtain();
        obtain.what = 11;
        obtain.arg1 = this.l;
        obtain.arg2 = this.m;
        a(obtain);
    }

    public void setVideoAutoPlay(boolean z2) {
        this.c.n = z2;
    }

    public void setPlayerMode(int i2) {
        if (i2 < 4) {
            this.c.m = i2;
        } else {
            b("Not support player mode:" + i2, 0);
        }
    }

    public int getPlayerMode() {
        return this.c.m;
    }

    public void setInteractiveMode(int i2) {
        if (this.c.s == i2) {
            b("setInteractiveMode is the same:" + i2, 3);
        }
        Message obtain = Message.obtain();
        obtain.what = 9;
        obtain.arg1 = i2;
        a(obtain);
    }

    public int getInteractiveMode() {
        return this.c.s;
    }

    public int getViewType() {
        return this.c.f51a;
    }

    public void b(boolean z2) {
        this.k = z2;
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        this.B++;
        b("[PLAYER]onSurfaceTextureAvailable:" + i2 + "x" + i3 + "surfaceAvailableCount" + this.B + " " + this.c.m, 3);
        this.c.o = i2;
        this.c.p = i3;
        this.u = surfaceTexture;
        if (this.c.m == 2) {
            Message obtain = Message.obtain();
            obtain.what = 26;
            a(obtain);
        } else if (this.c.m != 0) {
            Message obtain2 = Message.obtain();
            obtain2.what = 26;
            a(obtain2);
        } else {
            try {
                Thread.sleep(30);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            a(1);
            d(1);
        }
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        b("[PLAYER]onSurfaceTextureSizeChanged:" + i2 + "x" + i3, 3);
        if (this.c.m != 2) {
            this.c.o = i2;
            this.c.p = i3;
            b();
            b();
            return;
        }
        this.c.o = i2;
        this.c.p = i3;
        Message obtain = Message.obtain();
        obtain.what = 16;
        a(obtain);
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        this.C++;
        this.u = null;
        b("[PLAYER]onSurfaceTextureDestroyed surfaceDestoryCount" + this.C, 1);
        b("GetPlayerStatus()" + d(), 3);
        if (d() == 0 || d() == 5) {
            return false;
        }
        c(true);
        return false;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int a(Message message) {
        if (this.r != null) {
            this.r.a(message);
            return 0;
        }
        b("objRenderThread is null! " + message.what, 0);
        return -1;
    }

    public int getMediaControllerHeight() {
        return this.o.getHeight();
    }

    public void setMediaControllerHide(boolean z2) {
        this.o.setControllerHide(z2);
    }

    public void m() {
        this.o.c();
    }

    public void n() {
        this.o.d();
    }

    public void a(int i2, boolean z2) {
        this.o.a(i2, z2);
    }

    public void b(int i2, boolean z2) {
        this.o.b(i2, z2);
    }

    /* access modifiers changed from: protected */
    public void c(int i2) {
        switch (i2) {
            case 0:
                e(0);
                return;
            case 1:
                e(1);
                return;
            case 2:
                e(2);
                return;
            case 3:
                e(3);
                return;
            case 4:
                e(4);
                return;
            case 5:
                e(5);
                return;
            default:
                return;
        }
    }

    public void setAdditionalButtonCallback(a aVar) {
        this.y = aVar;
    }

    private void e(int i2) {
        if (this.y != null) {
            b("AdditionalButtonClick:" + i2, 3);
            this.y.a(i2);
            o();
        }
    }

    /* access modifiers changed from: protected */
    public void o() {
        if (this.y != null) {
            b("UserInteraction:", 3);
            this.y.a();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private String getMatrixText() {
        String str = "";
        float[] fArr = new float[9];
        getMatrix().getValues(fArr);
        for (int i2 = 0; i2 < fArr.length; i2++) {
            str = str + " " + fArr[i2];
        }
        return str;
    }

    public class g extends HandlerThread {
        private Handler b;
        private Choreographer.FrameCallback c = new a();
        private GeneralFunction.Player.a.a d = new GeneralFunction.Player.a.a();
        private SurfaceTexture e;
        private int f = -1;
        private int g = 0;
        private int h = 1;
        private int i = 1;
        private float[] j = new float[16];
        private float[] k = new float[16];
        private float[] l = new float[16];
        private float[] m = new float[16];
        private boolean n;
        private boolean o = false;
        private boolean p = true;
        private boolean q = false;
        private boolean r = false;
        private boolean s = false;
        private GeneralFunction.Player.a.e t;
        private int u = 0;
        private long v = 0;
        private long w = 0;

        public int a(Message message) {
            if (this.b == null) {
                return -1;
            }
            this.b.sendMessage(message);
            return 0;
        }

        private class a implements Choreographer.FrameCallback {
            private a() {
            }

            public void doFrame(long j) {
                Message obtain = Message.obtain();
                obtain.what = 2;
                g.this.a(obtain);
            }
        }

        public g(String str) {
            super(str);
        }

        public synchronized void start() {
            super.start();
            this.b = new Handler(getLooper()) {
                /* class GeneralFunction.Player.player.SphericalVideoPlayer.g.AnonymousClass1 */

                public void handleMessage(Message message) {
                    super.handleMessage(message);
                    switch (message.what) {
                        case 1:
                            g.this.a((g) ((SurfaceTexture) message.obj), (SurfaceTexture) message.arg1, message.arg2);
                            return;
                        case 2:
                            g.this.d();
                            return;
                        case 3:
                            g.this.h();
                            return;
                        case 4:
                            SphericalVideoPlayer.this.a(0);
                            SphericalVideoPlayer.this.d((SphericalVideoPlayer) 0);
                            g.this.a((g) ((Boolean) message.obj).booleanValue());
                            return;
                        case 5:
                            g.this.a((g) ((g) message.obj));
                            return;
                        case 6:
                            if (SphericalVideoPlayer.this.c.f51a != 6) {
                                g.this.a((g) ((Float) message.obj).floatValue(), 0.0f);
                                return;
                            }
                            return;
                        case 7:
                            SphericalVideoPlayer.this.c.z = (float[]) ((float[]) message.obj).clone();
                            SphericalVideoPlayer.this.c.y = true;
                            return;
                        case 8:
                            SphericalVideoPlayer.b("MSG_ON_RECEIVE_BITMAP", 3);
                            a aVar = (a) message.obj;
                            g.this.g = aVar.b;
                            g.this.a((g) aVar.f36a);
                            g.this.q = aVar.c;
                            return;
                        case 9:
                            SphericalVideoPlayer.this.c.s = message.arg1;
                            SphericalVideoPlayer.this.c.y = true;
                            return;
                        case 10:
                            if (this != null) {
                                g.this.a((g) ((Integer) message.obj).intValue());
                                SphericalVideoPlayer.this.c.y = true;
                                return;
                            }
                            return;
                        case 11:
                            SphericalVideoPlayer.b("MSG_UPDATE_TEXTURE_SIZE:" + message.arg1 + " " + message.arg2, 2);
                            if (SphericalVideoPlayer.this.c.m == 1) {
                                g.this.g = 0;
                            }
                            g.this.h = message.arg1;
                            g.this.i = message.arg2;
                            SphericalVideoPlayer.this.c.y = true;
                            g.this.n = true;
                            return;
                        case 12:
                            g.this.a((g) 0.0f, Float.MIN_VALUE);
                            return;
                        case 13:
                            SphericalVideoPlayer.this.c.b = ((Integer) message.obj).intValue();
                            g.this.b((g) SphericalVideoPlayer.this.c.b);
                            SphericalVideoPlayer.this.c.y = true;
                            return;
                        case 14:
                            if (this != null) {
                                g.this.o = ((Boolean) message.obj).booleanValue();
                                SphericalVideoPlayer.this.c.y = true;
                                return;
                            }
                            return;
                        case 15:
                            if (SphericalVideoPlayer.this.c.f51a != 6) {
                                g.this.j();
                                SphericalVideoPlayer.this.c.y = true;
                                return;
                            }
                            return;
                        case 16:
                            SphericalVideoPlayer.b("[PLAYER]MSG_UPDATE_SURFACE_SIZE:" + SphericalVideoPlayer.this.c.o + " " + SphericalVideoPlayer.this.c.p, 0);
                            if (SphericalVideoPlayer.this.c.o > SphericalVideoPlayer.this.c.p) {
                                SphericalVideoPlayer.this.c.b(true);
                                return;
                            } else {
                                SphericalVideoPlayer.this.c.b(false);
                                return;
                            }
                        case 17:
                        case 25:
                        default:
                            return;
                        case 18:
                            g gVar = (g) message.obj;
                            SphericalVideoPlayer.this.c.a(gVar.f50a, gVar.b);
                            return;
                        case 19:
                            SphericalVideoPlayer.this.c.e();
                            SphericalVideoPlayer.this.c.u = 0.0f;
                            SphericalVideoPlayer.this.c.v = 0.0f;
                            return;
                        case 20:
                            g.this.n = true;
                            SphericalVideoPlayer.this.c.y = true;
                            g.this.d();
                            return;
                        case 21:
                            h hVar = (h) message.obj;
                            SphericalVideoPlayer.this.c.e();
                            if (hVar.m != 0 && hVar.f51a != 0) {
                                SphericalVideoPlayer.this.c.l = hVar.l;
                                SphericalVideoPlayer.this.c.q = hVar.q;
                                SphericalVideoPlayer.this.c.b(hVar.r);
                                g.this.n = true;
                                SphericalVideoPlayer.this.c.y = true;
                                return;
                            }
                            return;
                        case 22:
                            SphericalVideoPlayer.this.c.e();
                            g.this.e();
                            SphericalVideoPlayer.this.c.y = true;
                            return;
                        case 23:
                            SphericalVideoPlayer.this.c.A = (float[]) ((float[]) message.obj).clone();
                            return;
                        case 24:
                            g.this.s = true;
                            g.this.i();
                            return;
                        case 26:
                            SphericalVideoPlayer.this.a(1);
                            SphericalVideoPlayer.this.d((SphericalVideoPlayer) 1);
                            return;
                    }
                }
            };
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(int i2) {
            if (SphericalVideoPlayer.this.c.p > SphericalVideoPlayer.this.c.o) {
                SphericalVideoPlayer.this.c.a(i2, false);
            } else {
                SphericalVideoPlayer.this.c.a(i2, true);
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void b(int i2) {
            if (i2 == 1) {
                SphericalVideoPlayer.this.c.b(i2, true);
            } else if (SphericalVideoPlayer.this.c.p > SphericalVideoPlayer.this.c.o) {
                SphericalVideoPlayer.this.c.b(i2, false);
            } else {
                SphericalVideoPlayer.this.c.b(i2, true);
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(Bitmap bitmap) {
            SphericalVideoPlayer.b("updateBitmap", 3);
            this.h = bitmap.getWidth();
            this.i = bitmap.getHeight();
            GLES20.glBindTexture(3553, this.f);
            GeneralFunction.Player.a.b.a("glBindTexture");
            if (!bitmap.isRecycled()) {
                GLUtils.texImage2D(3553, 0, bitmap, 0);
            } else {
                SphericalVideoPlayer.b("updateBitmap with recycle bitmap!", 1);
            }
            SphericalVideoPlayer.this.c.y = true;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private Surface a() {
            if (SphericalVideoPlayer.this.D == null) {
                SphericalVideoPlayer.b("new surface is corrent", 3);
            } else {
                SphericalVideoPlayer.b("not new surface is error", 1);
            }
            if (!this.d.e()) {
                throw new IllegalStateException("Cannot get video decode surface without GL context");
            }
            this.f = GeneralFunction.Player.a.b.a(SphericalVideoPlayer.this.c.m);
            this.e = new SurfaceTexture(this.f);
            this.e.setOnFrameAvailableListener(new SurfaceTexture.OnFrameAvailableListener() {
                /* class GeneralFunction.Player.player.SphericalVideoPlayer.g.AnonymousClass2 */

                /* renamed from: a  reason: collision with root package name */
                boolean f34a = true;

                public void onFrameAvailable(SurfaceTexture surfaceTexture) {
                    Message obtain = Message.obtain();
                    obtain.what = 3;
                    g.this.a(obtain);
                    if (SphericalVideoPlayer.this.j) {
                        SphericalVideoPlayer.this.j = false;
                        g.this.b();
                    }
                    if (this.f34a) {
                        if (SphericalVideoPlayer.this.x != null) {
                            SphericalVideoPlayer.this.x.a();
                        }
                        this.f34a = false;
                    }
                }
            });
            SphericalVideoPlayer.this.D = new Surface(this.e);
            return SphericalVideoPlayer.this.D;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void b() {
            Message obtain = Message.obtain();
            obtain.what = 11;
            obtain.arg1 = SphericalVideoPlayer.this.f22a.getVideoWidth();
            obtain.arg2 = SphericalVideoPlayer.this.f22a.getVideoHeight();
            SphericalVideoPlayer.this.a((SphericalVideoPlayer) obtain);
            if (SphericalVideoPlayer.this.o != null) {
                SphericalVideoPlayer.this.o.b(true);
                SphericalVideoPlayer.this.o.b();
            }
            SphericalVideoPlayer.this.seekTo(0);
            SphericalVideoPlayer.this.s();
            if (!SphericalVideoPlayer.this.c.n) {
                SphericalVideoPlayer.this.f22a.pause();
                SphericalVideoPlayer.b("getCurrentPosition()" + SphericalVideoPlayer.this.getCurrentPosition(), 3);
                if (SphericalVideoPlayer.this.getCurrentPosition() != 0) {
                    SphericalVideoPlayer.this.seekTo(0);
                    SphericalVideoPlayer.b("twice_getCurrentPosition()" + SphericalVideoPlayer.this.getCurrentPosition(), 3);
                    SphericalVideoPlayer.this.b();
                }
            } else if (SphericalVideoPlayer.this.k) {
                SphericalVideoPlayer.this.f22a.setVolume(1.0f, 1.0f);
            } else {
                SphericalVideoPlayer.this.f22a.setVolume(0.0f, 0.0f);
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(SurfaceTexture surfaceTexture, int i2, int i3) {
            SphericalVideoPlayer.b("surfacePrepare w: " + i2 + " h: " + i3 + " " + this.r, 3);
            SphericalVideoPlayer.b("surfacePrepare:" + SphericalVideoPlayer.this.d(), 3);
            if (SphericalVideoPlayer.this.d() != 1) {
                SphericalVideoPlayer.b("Invalid operation in status:" + SphericalVideoPlayer.this.d(), 0);
            } else if (!this.r) {
                this.r = true;
                this.d.a(surfaceTexture);
                Choreographer.getInstance().postFrameCallback(this.c);
                GLES20.glViewport(0, 0, i2, i3);
                GeneralFunction.Player.a.b.a("glViewport");
                GLES20.glEnable(3024);
                GLES20.glEnable(3042);
                Matrix.perspectiveM(this.m, 0, SphericalVideoPlayer.this.c.e, ((float) i2) / ((float) i3), SphericalVideoPlayer.this.c.d, SphericalVideoPlayer.this.c.c);
                Matrix.setIdentityM(this.l, 0);
                c();
                GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
                this.t = new GeneralFunction.Player.a.e(SphericalVideoPlayer.this.getContext(), SphericalVideoPlayer.this.c.m, 500.0f);
                switch (SphericalVideoPlayer.this.c.m) {
                    case 1:
                        if (SphericalVideoPlayer.this.f22a != null) {
                            SphericalVideoPlayer.this.f22a.setSurface(SphericalVideoPlayer.this.r.a());
                            if (!SphericalVideoPlayer.this.c.n) {
                                SphericalVideoPlayer.this.a(2);
                                SphericalVideoPlayer.this.d((SphericalVideoPlayer) 2);
                                break;
                            } else {
                                SphericalVideoPlayer.this.s();
                                break;
                            }
                        } else {
                            SphericalVideoPlayer.this.a((SphericalVideoPlayer) SphericalVideoPlayer.this.s);
                            break;
                        }
                    case 2:
                        SphericalVideoPlayer.this.r();
                        break;
                    case 3:
                        this.f = GeneralFunction.Player.a.b.a(SphericalVideoPlayer.this.c.m);
                        SphericalVideoPlayer.this.q();
                        if (SphericalVideoPlayer.this.n != null) {
                            SphericalVideoPlayer.this.a((SphericalVideoPlayer) SphericalVideoPlayer.this.n);
                            break;
                        }
                        break;
                }
                Matrix.setIdentityM(SphericalVideoPlayer.this.c.z, 0);
                SphericalVideoPlayer.b("surfacePrepare finish", 3);
            }
        }

        private void c() {
            Matrix.setRotateM(this.k, 0, 90.0f, 1.0f, 0.0f, 0.0f);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x01f3  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0204  */
        /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void d() {
            /*
            // Method dump skipped, instructions count: 636
            */
            throw new UnsupportedOperationException("Method not decompiled: GeneralFunction.Player.player.SphericalVideoPlayer.g.d():void");
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void e() {
            SphericalVideoPlayer.this.c.b(0.0f);
            SphericalVideoPlayer.this.c.q = 0.0f;
            SphericalVideoPlayer.this.c.c();
        }

        private float f() {
            if (SphericalVideoPlayer.this.c.b == 0) {
                return ((float) SphericalVideoPlayer.this.c.o) / ((float) SphericalVideoPlayer.this.c.p);
            }
            return (((float) SphericalVideoPlayer.this.c.o) / 2.0f) / ((float) SphericalVideoPlayer.this.c.p);
        }

        private void a(float[] fArr, float f2, float f3, float f4) {
            float f5;
            float f6;
            float f7 = f();
            float f8 = f3 + f2 + 100.0f;
            if (f3 <= f2) {
                f5 = 2.0f;
            } else {
                f5 = (f3 - ((f2 * f2) / f3)) + 2.0f;
            }
            if (SphericalVideoPlayer.this.c.o > SphericalVideoPlayer.this.c.p) {
                f6 = (float) Math.toDegrees(Math.atan(Math.tan(Math.toRadians((double) (f4 / 2.0f))) / ((double) f7)) * 2.0d);
            } else {
                f6 = f4;
            }
            Matrix.perspectiveM(fArr, 0, f6, f7, f5, f8);
        }

        private void g() {
            float radians;
            float[] fArr = new float[3];
            float f2 = SphericalVideoPlayer.this.c.l;
            float f3 = SphericalVideoPlayer.this.c.e;
            float f4 = SphericalVideoPlayer.this.c.q;
            float f5 = SphericalVideoPlayer.this.c.r;
            a(this.m, 500.0f, f2, f3);
            float radians2 = (float) Math.toRadians((double) (90.0f - f5));
            if (SphericalVideoPlayer.this.c.s == 1) {
                radians = (float) Math.toRadians((double) (90.0f + f4));
            } else if (SphericalVideoPlayer.this.c.s == 3) {
                radians = (float) Math.toRadians(90.0d);
            } else {
                radians = (float) Math.toRadians((double) f4);
            }
            fArr[0] = (float) (((double) f2) * Math.sin((double) radians2) * Math.cos((double) radians));
            fArr[1] = (float) (((double) f2) * Math.cos((double) radians2));
            fArr[2] = (float) (((double) f2) * Math.sin((double) radians2) * Math.sin((double) radians));
            float[] fArr2 = {(float) (((double) (f2 - 100.0f)) * Math.sin((double) radians2) * Math.cos((double) radians)), (float) (((double) (f2 - 100.0f)) * Math.cos((double) radians2)), (float) (((double) (f2 - 100.0f)) * Math.sin((double) radians2) * Math.sin((double) radians))};
            Matrix.setLookAtM(this.l, 0, fArr[0], fArr[1], fArr[2], fArr2[0], fArr2[1], fArr2[2], 0.0f, 1.0f, 0.0f);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void h() {
            this.n = true;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(boolean z) {
            if (this.r) {
                SphericalVideoPlayer.b("start_surfaceDestroy:" + z, 3);
                this.r = false;
                Choreographer.getInstance().removeFrameCallback(this.c);
                if (!z) {
                    if (this.d != null) {
                        i();
                        GLES20.glDisable(3553);
                        this.d.d();
                        this.d = null;
                    }
                    if (SphericalVideoPlayer.this.r != null) {
                        SphericalVideoPlayer.this.r.quit();
                        SphericalVideoPlayer.this.r = null;
                    }
                    this.b = null;
                    try {
                        Thread.sleep(30);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                    SphericalVideoPlayer.this.c.f51a = 0;
                    if (SphericalVideoPlayer.this.f22a != null) {
                        SphericalVideoPlayer.this.f22a.reset();
                        SphericalVideoPlayer.this.f22a.stop();
                        SphericalVideoPlayer.this.f22a.release();
                        SphericalVideoPlayer.this.f22a = null;
                    }
                } else {
                    if (this.d != null) {
                        this.d.d();
                    }
                    if (SphericalVideoPlayer.this.f22a != null) {
                        try {
                            Thread.sleep(30);
                        } catch (InterruptedException e3) {
                            e3.printStackTrace();
                        }
                        if (SphericalVideoPlayer.this.c.n) {
                            SphericalVideoPlayer.this.f22a.pause();
                        }
                        SphericalVideoPlayer.this.f22a.setSurface(null);
                    }
                }
                if (!(SphericalVideoPlayer.this.o == null || SphericalVideoPlayer.this.f22a == null)) {
                    SphericalVideoPlayer.this.o.b(false);
                }
                if (SphericalVideoPlayer.this.p != null) {
                    SphericalVideoPlayer.this.p.b();
                    SphericalVideoPlayer.this.p.d();
                    SphericalVideoPlayer.this.p = null;
                }
                if (SphericalVideoPlayer.this.q != null) {
                    SphericalVideoPlayer.this.q.c();
                    SphericalVideoPlayer.this.q = null;
                }
                if (this.f != -1) {
                    GLES20.glDeleteTextures(1, new int[]{this.f}, 0);
                    this.f = -1;
                }
                if (this.e != null) {
                    this.e.release();
                    this.e = null;
                    this.n = false;
                }
                Matrix.setIdentityM(SphericalVideoPlayer.this.c.z, 0);
                SphericalVideoPlayer.this.c.y = false;
                if (this.t != null) {
                    this.t.a();
                    this.t = null;
                }
                if (!z) {
                    SphericalVideoPlayer.this.a(5);
                    SphericalVideoPlayer.this.d((SphericalVideoPlayer) 5);
                    SphericalVideoPlayer.this.a(1);
                    SphericalVideoPlayer.this.d((SphericalVideoPlayer) 1);
                }
                SphericalVideoPlayer.b("finish_surfaceDestroy", 3);
                return;
            }
            SphericalVideoPlayer.b("surfaceDestroy_fail", 3);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(g gVar) {
            SphericalVideoPlayer.this.c.q = (((-gVar.f50a) * 0.1f) + SphericalVideoPlayer.this.c.q) % 360.0f;
            SphericalVideoPlayer.this.c.b(((-gVar.b) * 0.1f) + SphericalVideoPlayer.this.c.r);
            SphericalVideoPlayer.this.c.y = true;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void i() {
            GLES20.glClear(16384);
            GeneralFunction.Player.a.b.a("glClear");
            this.d.b();
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(float f2, boolean z) {
            float f3 = SphericalVideoPlayer.this.c.t;
            if (z) {
                SphericalVideoPlayer.this.c.b();
            } else {
                if (((int) f2) >= 1) {
                    f3 = -f3;
                }
                SphericalVideoPlayer.this.c.a(f3 + SphericalVideoPlayer.this.c.l);
            }
            SphericalVideoPlayer.this.c.y = true;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void j() {
            SphericalVideoPlayer.this.c.d();
            this.p = true;
        }
    }
}
