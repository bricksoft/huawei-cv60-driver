package GeneralFunction.Player.player;

import GeneralFunction.d;
import GeneralFunction.i;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.Matrix;
import android.os.Message;
import android.view.WindowManager;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private String f47a = "MotionStrategy";
    private int b = 0;
    private SensorManager c;
    private Sensor d;
    private Sensor e;
    private boolean f = false;
    private a g;
    private b h;
    private h i = new h();
    private WindowManager j;
    private c k;

    public interface c {
        void a(Message message);
    }

    public void a(String str, int i2) {
        d.a(this.f47a, str, i2);
    }

    public void a(c cVar) {
        this.k = cVar;
    }

    /* access modifiers changed from: protected */
    public void a(Context context, h hVar) {
        if (this.f) {
            a("registerSensor 2 time!!", 0);
            return;
        }
        this.f = true;
        this.j = (WindowManager) context.getSystemService("window");
        a(context);
        b();
        this.i = hVar;
    }

    public void a() {
        if (!this.f) {
            a("unregisterSensor 2 time!!", 0);
            return;
        }
        this.f = false;
        switch (this.b) {
            case 1:
                this.c.unregisterListener(this.g);
                return;
            case 2:
                this.c.unregisterListener(this.h);
                return;
            default:
                return;
        }
    }

    private void a(Context context) {
        this.c = (SensorManager) context.getSystemService("sensor");
        this.b = 1;
        if (this.d == null) {
            this.d = this.c.getDefaultSensor(11);
            if (this.d == null) {
                this.b = 2;
                this.d = this.c.getDefaultSensor(2);
                this.e = this.c.getDefaultSensor(1);
            }
        }
    }

    private void b() {
        switch (this.b) {
            case 1:
                this.g = new a();
                this.c.registerListener(this.g, this.d, 0);
                return;
            case 2:
                this.h = new b();
                this.c.registerListener(this.h, this.d, 3);
                this.c.registerListener(this.h, this.e, 3);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float[] a(float[] fArr, int i2) {
        float[] fArr2 = new float[16];
        float[] fArr3 = new float[16];
        switch (i2) {
            case 0:
                SensorManager.getRotationMatrixFromVector(fArr3, fArr);
                SensorManager.remapCoordinateSystem(fArr3, 129, 130, fArr2);
                break;
            case 1:
                SensorManager.getRotationMatrixFromVector(fArr3, fArr);
                SensorManager.remapCoordinateSystem(fArr3, 130, 1, fArr2);
                break;
            case 2:
                SensorManager.getRotationMatrixFromVector(fArr3, fArr);
                SensorManager.remapCoordinateSystem(fArr3, 1, 2, fArr2);
                break;
            case 3:
                SensorManager.getRotationMatrixFromVector(fArr3, fArr);
                SensorManager.remapCoordinateSystem(fArr3, 2, 129, fArr2);
                break;
        }
        return fArr2;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float[] a(float[] fArr, float[] fArr2, int i2) {
        float[] fArr3 = new float[16];
        float[] fArr4 = new float[16];
        switch (i2) {
            case 0:
                SensorManager.getRotationMatrix(fArr3, null, fArr2, fArr);
                Matrix.rotateM(fArr3, 0, 90.0f, 1.0f, 0.0f, 0.0f);
                break;
            case 1:
                SensorManager.getRotationMatrix(fArr4, null, fArr2, fArr);
                SensorManager.remapCoordinateSystem(fArr4, 130, 131, fArr3);
                Matrix.rotateM(fArr3, 0, 270.0f, 1.0f, 0.0f, 0.0f);
                break;
            case 2:
                SensorManager.getRotationMatrix(fArr3, null, fArr2, fArr);
                Matrix.rotateM(fArr3, 0, 270.0f, 1.0f, 0.0f, 0.0f);
                break;
            case 3:
                SensorManager.getRotationMatrix(fArr4, null, fArr2, fArr);
                SensorManager.remapCoordinateSystem(fArr4, 2, 3, fArr3);
                Matrix.rotateM(fArr3, 0, 270.0f, 1.0f, 0.0f, 0.0f);
                break;
        }
        return fArr3;
    }

    public class a implements SensorEventListener {
        public a() {
        }

        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.accuracy != 0) {
                Message obtain = Message.obtain();
                obtain.what = 7;
                obtain.obj = f.this.a((f) sensorEvent.values, (float[]) f.this.j.getDefaultDisplay().getRotation());
                f.this.k.a(obtain);
            }
        }
    }

    public class b implements SensorEventListener {
        private float[] b = null;
        private float[] c = null;
        private float[] d = null;
        private int e = 0;

        public b() {
        }

        private float a(float f, float f2) {
            if (((double) Math.abs(f - f2)) <= 3.141592653589793d) {
                return (0.2f * (f - f2)) + f2;
            }
            if (f2 > 0.0f) {
                return (float) ((((double) 0.2f) * ((6.283185307179586d - ((double) f2)) + ((double) f))) + ((double) f2));
            }
            return (float) (((double) f2) - (((double) 0.2f) * ((((double) f2) + 6.283185307179586d) - ((double) f))));
        }

        private float a(float f, float f2, float f3) {
            return (float) Math.sqrt((double) ((f * f) + (f2 * f2) + (f3 * f3)));
        }

        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == 2) {
                this.b = sensorEvent.values;
            } else if (sensorEvent.sensor.getType() == 1) {
                this.c = sensorEvent.values;
            }
            if (this.b != null && this.c != null) {
                float[] a2 = f.this.a((f) this.b, this.c, (float[]) f.this.j.getDefaultDisplay().getRotation());
                if (this.d != null) {
                    this.e++;
                    float[] a3 = i.a(a2);
                    float[] a4 = i.a(this.d);
                    if (this.e > 0) {
                        this.e = 0;
                        if (((double) Math.abs(a(a3[0], a3[1], a3[2]) - a(a4[0], a4[1], a4[2]))) > Math.toRadians(2.0d)) {
                            this.d = i.a(a(a3[0], a4[0]), a(a3[1], a4[1]), a(a3[2], a4[2]));
                            Message obtain = Message.obtain();
                            obtain.what = 7;
                            obtain.obj = this.d.clone();
                            f.this.k.a(obtain);
                            return;
                        }
                        return;
                    }
                    return;
                }
                this.d = (float[]) a2.clone();
                Message obtain2 = Message.obtain();
                obtain2.what = 7;
                obtain2.obj = a2.clone();
                f.this.k.a(obtain2);
            }
        }
    }
}
