package GeneralFunction.Player.player;

import GeneralFunction.c.b;
import GeneralFunction.c.d;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.Message;
import android.view.Surface;
import com.google.android.exoplayer.util.MimeTypes;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f37a = c.class.getSimpleName();
    private BlockingQueue<Message> b = new ArrayBlockingQueue(90);
    private BlockingQueue<Integer> c = new ArrayBlockingQueue(16);
    private a d;
    private int e = 0;
    private long f = 0;
    private long g = 0;
    private long h = 0;
    private int i = 0;
    private boolean j = false;
    private long k = 0;
    private long l = 0;
    private long m = 0;
    private long n = 0;

    /* access modifiers changed from: private */
    public static void b(String str, int i2) {
        b.a(f37a, str, i2);
    }

    public void a(Surface surface, int i2, int i3, int i4) {
        this.d = new a(surface);
        this.d.a(i2, i3);
        this.d.a(i4);
        this.d.start();
    }

    public void a() {
        this.j = true;
    }

    public void b() {
        try {
            this.c.put(new Integer(1));
            b(2);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    public void c() {
        try {
            this.c.put(new Integer(2));
            b(1);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    public void a(int i2) {
        this.d.a(i2);
    }

    public void a(d dVar, int i2, int i3) {
        if (dVar != null) {
            Bundle bundle = new Bundle();
            Message message = new Message();
            message.obj = dVar;
            bundle.putInt("width", i2);
            bundle.putInt("height", i3);
            if (this.j) {
                this.j = false;
                bundle.putBoolean("needReinitial", true);
            } else {
                bundle.putBoolean("needReinitial", false);
            }
            message.setData(bundle);
            try {
                this.b.put(message);
                int size = this.b.size();
                if (size > this.i) {
                    this.i = size;
                    b("@-------currentQueyeCount:" + this.i, 2);
                }
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void b(int i2) {
        long currentTimeMillis = System.currentTimeMillis();
        while (this.e != i2) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            if (System.currentTimeMillis() - currentTimeMillis > 2000) {
                b("waitDecoderStatus error:" + this.e + " " + i2, 0);
            }
        }
    }

    public void d() {
        if (this.d != null) {
            this.d.a();
            b(0);
            this.d = null;
        }
    }

    private class a extends Thread {
        private MediaCodec b;
        private Surface c;
        private int d = 30;
        private int e = 1920;
        private int f = 960;
        private int g = b(this.d, 0);
        private long h = 0;
        private int i = 0;
        private boolean j = false;
        private boolean k = false;
        private int l = 0;
        private ArrayList<b> m = new ArrayList<>();

        public a(Surface surface) {
            super("MediaCodecThread");
            this.c = surface;
        }

        public void a(int i2) {
            this.d = i2;
            this.g = b(this.d, 0);
        }

        public void a(int i2, int i3) {
            this.e = i2;
            this.f = i3;
        }

        public void a() {
            this.k = true;
        }

        private int b(int i2, int i3) {
            float f2 = (float) i2;
            return (int) (1000.0f / ((f2 + ((((float) i3) / (f2 / 6.0f)) * (f2 / 15.0f))) + 1.0f));
        }

        private b a(long j2) {
            b bVar;
            b bVar2 = null;
            int size = this.m.size() - 1;
            while (size >= 0) {
                long a2 = this.m.get(size).a();
                if (a2 < j2) {
                    this.m.remove(size);
                    c.this.b.remove(Integer.valueOf(size));
                    c.b("Frame decode fail:" + a2 + "<" + j2, 0);
                    bVar = bVar2;
                } else if (a2 == j2) {
                    bVar = this.m.get(size);
                    this.m.remove(size);
                } else {
                    bVar = bVar2;
                }
                size--;
                bVar2 = bVar;
            }
            return bVar2;
        }

        private long b() {
            this.i++;
            return (long) this.i;
        }

        private MediaFormat c(int i2, int i3) {
            MediaFormat createVideoFormat = MediaFormat.createVideoFormat(MimeTypes.VIDEO_H264, i2, i3);
            createVideoFormat.setInteger("frame-rate", 60);
            return createVideoFormat;
        }

        private int d(int i2, int i3) {
            try {
                this.b.stop();
            } catch (IllegalStateException e2) {
                e2.printStackTrace();
            }
            this.b.release();
            MediaFormat c2 = c(i2, i3);
            try {
                this.b = MediaCodec.createDecoderByType(MimeTypes.VIDEO_H264);
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            if (this.c == null || !this.c.isValid()) {
                c.b("surface is " + this.c, 0);
                return -1;
            }
            this.b.configure(c2, this.c, (MediaCrypto) null, 0);
            this.b.start();
            return 0;
        }

        /* JADX WARNING: Removed duplicated region for block: B:128:0x0467  */
        /* JADX WARNING: Removed duplicated region for block: B:130:0x0122 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:133:0x01e1 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00a2  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00c6  */
        /* JADX WARNING: Removed duplicated region for block: B:70:0x01f1 A[LOOP:1: B:68:0x01eb->B:70:0x01f1, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:79:0x0222  */
        /* JADX WARNING: Removed duplicated region for block: B:95:0x02f2 A[SYNTHETIC, Splitter:B:95:0x02f2] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            // Method dump skipped, instructions count: 1156
            */
            throw new UnsupportedOperationException("Method not decompiled: GeneralFunction.Player.player.c.a.run():void");
        }
    }
}
