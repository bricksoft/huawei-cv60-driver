package GeneralFunction.Player.player;

import GeneralFunction.Player.player.SphericalVideoPlayer;
import android.content.Context;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private static final String f57a = i.class.getSimpleName();
    private GestureDetector b;
    private ScaleGestureDetector c;
    private int d = 0;
    private SphericalVideoPlayer.b e;
    private h f = new h();
    private boolean g = false;
    private SphericalVideoPlayer.g h;
    private GestureDetector.SimpleOnGestureListener i = new GestureDetector.SimpleOnGestureListener() {
        /* class GeneralFunction.Player.player.i.AnonymousClass1 */

        public boolean onDown(MotionEvent motionEvent) {
            if (i.this.f.s == 1 || i.this.f.s == 3) {
                Message obtain = Message.obtain();
                SphericalVideoPlayer.g unused = i.this.h;
                obtain.what = 19;
                i.this.e.a(obtain);
            }
            return true;
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            if (i.this.f.B) {
                if (i.this.f.f51a == 6) {
                    i.this.f.B = false;
                }
            } else if (i.this.f.s == 1 || i.this.f.s == 3) {
                Message obtain = Message.obtain();
                SphericalVideoPlayer.g unused = i.this.h;
                obtain.what = 5;
                int i = i.this.f.s;
                h unused2 = i.this.f;
                if (i == 3) {
                    obtain.obj = new g(f, 0.0f, false, false);
                } else {
                    obtain.obj = new g(f, f2, false, false);
                }
                i.this.e.b(obtain);
            }
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            i.this.e.a(motionEvent);
            return true;
        }

        public boolean onDoubleTap(MotionEvent motionEvent) {
            return false;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0020, code lost:
            if (r1 == 3) goto L_0x0022;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onFling(android.view.MotionEvent r7, android.view.MotionEvent r8, float r9, float r10) {
            /*
                r6 = this;
                r5 = 3
                r4 = 1
                r3 = 0
                r0 = 0
                GeneralFunction.Player.player.i r1 = GeneralFunction.Player.player.i.this
                GeneralFunction.Player.player.h r1 = GeneralFunction.Player.player.i.a(r1)
                int r1 = r1.s
                GeneralFunction.Player.player.i r2 = GeneralFunction.Player.player.i.this
                GeneralFunction.Player.player.i.a(r2)
                if (r1 == r4) goto L_0x0022
                GeneralFunction.Player.player.i r1 = GeneralFunction.Player.player.i.this
                GeneralFunction.Player.player.h r1 = GeneralFunction.Player.player.i.a(r1)
                int r1 = r1.s
                GeneralFunction.Player.player.i r2 = GeneralFunction.Player.player.i.this
                GeneralFunction.Player.player.i.a(r2)
                if (r1 != r5) goto L_0x005b
            L_0x0022:
                float r1 = java.lang.Math.abs(r9)
                float r2 = java.lang.Math.abs(r10)
                int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
                if (r1 <= 0) goto L_0x005c
                r10 = r0
            L_0x002f:
                GeneralFunction.Player.player.i r1 = GeneralFunction.Player.player.i.this
                GeneralFunction.Player.player.h r1 = GeneralFunction.Player.player.i.a(r1)
                int r1 = r1.s
                GeneralFunction.Player.player.i r2 = GeneralFunction.Player.player.i.this
                GeneralFunction.Player.player.i.a(r2)
                if (r1 != r5) goto L_0x005e
            L_0x003e:
                android.os.Message r1 = android.os.Message.obtain()
                GeneralFunction.Player.player.i r2 = GeneralFunction.Player.player.i.this
                GeneralFunction.Player.player.i.b(r2)
                r2 = 18
                r1.what = r2
                GeneralFunction.Player.player.g r2 = new GeneralFunction.Player.player.g
                r2.<init>(r9, r0, r3, r3)
                r1.obj = r2
                GeneralFunction.Player.player.i r0 = GeneralFunction.Player.player.i.this
                GeneralFunction.Player.player.SphericalVideoPlayer$b r0 = GeneralFunction.Player.player.i.c(r0)
                r0.c(r1)
            L_0x005b:
                return r4
            L_0x005c:
                r9 = r0
                goto L_0x002f
            L_0x005e:
                r0 = r10
                goto L_0x003e
            */
            throw new UnsupportedOperationException("Method not decompiled: GeneralFunction.Player.player.i.AnonymousClass1.onFling(android.view.MotionEvent, android.view.MotionEvent, float, float):boolean");
        }
    };

    public i(Context context) {
        this.b = new GestureDetector(context, this.i);
        this.c = new ScaleGestureDetector(context, new a());
    }

    public class a implements ScaleGestureDetector.OnScaleGestureListener {
        public a() {
        }

        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            i.this.g = true;
            return true;
        }

        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            i.this.f.B = true;
            float scaleFactor = scaleGestureDetector.getScaleFactor();
            if (((double) Math.abs((scaleGestureDetector.getCurrentSpan() - scaleGestureDetector.getPreviousSpan()) / ((float) (2 * scaleGestureDetector.getTimeDelta())))) > 0.03d && scaleFactor != 1.0f) {
                i.this.f.B = true;
                Message obtain = Message.obtain();
                SphericalVideoPlayer.g unused = i.this.h;
                obtain.what = 6;
                obtain.obj = Float.valueOf(scaleFactor);
                i.this.e.d(obtain);
            }
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
            i.this.g = false;
        }
    }

    public boolean a(MotionEvent motionEvent) {
        if (motionEvent.getPointerCount() >= 2) {
            this.c.onTouchEvent(motionEvent);
            switch (motionEvent.getAction()) {
                case 1:
                case 6:
                case 262:
                    if (!this.g) {
                        Message obtain = Message.obtain();
                        SphericalVideoPlayer.g gVar = this.h;
                        obtain.what = 15;
                        this.e.e(obtain);
                        break;
                    }
                    break;
            }
        } else if (motionEvent.getPointerCount() == 1) {
            this.b.onTouchEvent(motionEvent);
        }
        return true;
    }

    public void a(SphericalVideoPlayer.b bVar) {
        this.e = bVar;
    }

    public void a(h hVar, SphericalVideoPlayer.g gVar) {
        this.f = hVar;
        this.h = gVar;
    }
}
