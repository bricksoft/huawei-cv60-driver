package GeneralFunction.Player.a;

import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.GLUtils;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private EGLDisplay f15a = EGL14.EGL_NO_DISPLAY;
    private EGLConfig b;
    private EGLContext c = EGL14.EGL_NO_CONTEXT;
    private EGLSurface d = EGL14.EGL_NO_SURFACE;

    public a() {
        a();
    }

    public void a() {
        this.f15a = EGL14.eglGetDisplay(0);
        if (this.f15a == EGL14.EGL_NO_DISPLAY) {
            a("eglGetDisplay");
        }
        int[] iArr = new int[2];
        if (!EGL14.eglInitialize(this.f15a, iArr, 0, iArr, 1)) {
            a("eglInitialize");
        }
        EGLConfig[] eGLConfigArr = new EGLConfig[1];
        int[] iArr2 = new int[1];
        if (!EGL14.eglChooseConfig(this.f15a, new int[]{12324, 8, 12323, 8, 12322, 8, 12321, 8, 12325, 16, 12352, 4, 12344}, 0, eGLConfigArr, 0, eGLConfigArr.length, iArr2, 0)) {
            a("eglChooseConfig");
        }
        if (iArr2[0] <= 0) {
            a("No EGL config found for attribute list");
        }
        this.b = eGLConfigArr[0];
        this.c = EGL14.eglCreateContext(this.f15a, this.b, EGL14.EGL_NO_CONTEXT, new int[]{12440, 2, 12344}, 0);
        if (this.c == null) {
            a("eglCreateContext");
        }
    }

    public void a(SurfaceTexture surfaceTexture) {
        if (!e()) {
            a();
        }
        this.d = EGL14.eglCreateWindowSurface(this.f15a, this.b, surfaceTexture, new int[]{12344}, 0);
        if (this.d == null || this.d == EGL14.EGL_NO_SURFACE) {
            a("eglCreateWindowSurface");
        }
        c();
    }

    public void b() {
        if (!EGL14.eglSwapBuffers(this.f15a, this.d)) {
        }
    }

    private void a(String str) {
        int eglGetError = EGL14.eglGetError();
        throw new RuntimeException(str + ": EGL error: 0x" + Integer.toHexString(eglGetError) + ": " + GLUtils.getEGLErrorString(eglGetError));
    }

    public void c() {
        if (!EGL14.eglMakeCurrent(this.f15a, this.d, this.d, this.c)) {
            a("eglMakeCurrent");
        }
    }

    public void d() {
        if (this.f15a != EGL14.EGL_NO_DISPLAY) {
            EGL14.eglMakeCurrent(this.f15a, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
            EGL14.eglDestroySurface(this.f15a, this.d);
            EGL14.eglDestroyContext(this.f15a, this.c);
            EGL14.eglReleaseThread();
            EGL14.eglTerminate(this.f15a);
        }
        this.f15a = EGL14.EGL_NO_DISPLAY;
        this.d = EGL14.EGL_NO_SURFACE;
        this.c = EGL14.EGL_NO_CONTEXT;
        this.b = null;
    }

    public boolean e() {
        return this.c != EGL14.EGL_NO_CONTEXT;
    }

    public boolean f() {
        return this.d != EGL14.EGL_NO_SURFACE;
    }
}
