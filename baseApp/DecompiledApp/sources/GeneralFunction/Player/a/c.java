package GeneralFunction.Player.a;

import android.opengl.GLES20;
import android.util.Log;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public static final String f17a = c.class.getSimpleName();
    private int b;

    public c(String str, String str2) {
        this.b = a(str, str2);
    }

    public int a() {
        return this.b;
    }

    public void b() {
        GLES20.glDeleteProgram(this.b);
        this.b = -1;
    }

    private static void a(int i, String str) {
        if (i < 0) {
            throw new RuntimeException("Could not find location for " + str);
        }
    }

    public int a(String str) {
        int glGetAttribLocation = GLES20.glGetAttribLocation(this.b, str);
        a(glGetAttribLocation, str);
        return glGetAttribLocation;
    }

    public int b(String str) {
        int glGetUniformLocation = GLES20.glGetUniformLocation(this.b, str);
        a(glGetUniformLocation, str);
        return glGetUniformLocation;
    }

    private static int a(String str, String str2) {
        int b2 = b(35633, str);
        int b3 = b(35632, str2);
        int glCreateProgram = GLES20.glCreateProgram();
        b.a("glCreateProgram");
        if (glCreateProgram == 0) {
            Log.e(f17a, "Could not create program");
            return 0;
        }
        GLES20.glAttachShader(glCreateProgram, b2);
        b.a("glAttachShader");
        GLES20.glAttachShader(glCreateProgram, b3);
        b.a("glAttachShader");
        GLES20.glLinkProgram(glCreateProgram);
        int[] iArr = new int[1];
        GLES20.glGetProgramiv(glCreateProgram, 35714, iArr, 0);
        if (iArr[0] == 1) {
            return glCreateProgram;
        }
        Log.e(f17a, "Could not link program: ");
        Log.e(f17a, GLES20.glGetProgramInfoLog(glCreateProgram));
        GLES20.glDeleteProgram(glCreateProgram);
        return 0;
    }

    private static int b(int i, String str) {
        int glCreateShader = GLES20.glCreateShader(i);
        b.a("glCreateShader type=" + i);
        GLES20.glShaderSource(glCreateShader, str);
        GLES20.glCompileShader(glCreateShader);
        int[] iArr = new int[1];
        GLES20.glGetShaderiv(glCreateShader, 35713, iArr, 0);
        if (iArr[0] != 0) {
            return glCreateShader;
        }
        Log.e(f17a, "Could not compile shader " + i + ":");
        Log.e(f17a, " " + GLES20.glGetShaderInfoLog(glCreateShader));
        GLES20.glDeleteShader(glCreateShader);
        return 0;
    }
}
