package GeneralFunction.Player.a;

import GeneralFunction.Player.player.b;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import com.huawei.cvIntl60.R;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final String f21a = e.class.getSimpleName();
    private c b;
    private int c;
    private int d;
    private int e;
    private int f;
    private float[] g = new float[16];
    private float[] h = new float[16];
    private float[] i = new float[16];
    private float[] j = new float[16];
    private d k;
    private FloatBuffer l = null;
    private FloatBuffer m = ByteBuffer.allocateDirect(48).order(ByteOrder.nativeOrder()).asFloatBuffer().put(new float[]{1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f});

    public e(Context context, int i2, float f2) {
        b.a(f21a, "SphericalSceneRenderer VideoMode:" + Integer.toString(i2), 3);
        if (i2 == 1 || i2 == 2) {
            this.b = new c(a(context, (int) R.raw.player_360_vertex_shader), a(context, (int) R.raw.player_360_fragment_shader_for_video));
            b.a(f21a, "Load the video_fragment_shader", 3);
        } else if (i2 == 3) {
            this.b = new c(a(context, (int) R.raw.player_360_vertex_shader), a(context, (int) R.raw.player_360_fragment_shader_for_bitmap));
            b.a(f21a, "Load the bitmap_fragment_shader", 3);
        } else {
            b.a(f21a, "Not correct VideoMode:" + i2, 3);
            return;
        }
        this.c = this.b.a("aPosition");
        this.d = this.b.b("uMVPMatrix");
        this.e = this.b.b("uTextureMatrix");
        this.f = this.b.a("aTextureCoord");
        GLES20.glDisable(2929);
        this.k = new d(180, 0.0f, 0.0f, 0.0f, f2, 1, 1);
        GLES20.glUseProgram(this.b.a());
        GLES20.glEnableVertexAttribArray(this.c);
        b.a("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(this.c, 3, 5126, false, this.k.b(), (Buffer) this.k.a());
        b.a("glVertexAttribPointer");
        GLES20.glEnableVertexAttribArray(this.f);
        b.a("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(this.f, 2, 5126, false, this.k.b(), this.k.a().duplicate().position(3));
        b.a("glVertexAttribPointer");
        this.m.position(0);
    }

    public void a(int i2, float[] fArr, float[] fArr2, float[] fArr3, float[] fArr4, float[] fArr5, int i3, int i4, int i5, float f2, int i6, int i7, int i8, boolean z, int i9, boolean z2) {
        b();
        switch (i3) {
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
                a(fArr, fArr2, fArr3, fArr4, fArr5, i9, z2, i5, i4);
                b(i4, i2);
                a(i8, i7, i6);
                if (z) {
                    a(fArr2, fArr3, fArr4, fArr5, f2);
                    b(i4, i2);
                    a(i8, i7);
                    return;
                }
                return;
            case 4:
                a(fArr2, fArr3, fArr4, fArr5, f2);
                b(i4, i2);
                a(i3, i8, i7, i6);
                return;
            default:
                return;
        }
    }

    private void b() {
        GLES20.glClear(16384);
    }

    private void a(int i2, int i3, int i4, int i5) {
        GLES20.glViewport(0, 0, i4, i3);
        GLES20.glEnable(3089);
        GLES20.glScissor(0, 0, i4, i3);
        GLES20.glDisable(3089);
        GLES20.glDrawArrays(4, 0, 6);
        b.a("glDrawArrays");
    }

    private void a(int i2, int i3) {
        GLES20.glViewport(((-i3) * 4) / 7, (-i2) / 4, (i3 * 5) / 7, (i2 * 2) / 3);
        GLES20.glDrawArrays(4, 0, 6);
        GLES20.glViewport((i3 * 1) / 7, (-i2) / 4, (i3 * 5) / 7, (i2 * 2) / 3);
        GLES20.glDrawArrays(4, 0, 6);
        GLES20.glViewport(((i3 * 6) / 7) - 1, (-i2) / 4, (i3 * 5) / 7, (i2 * 2) / 3);
        GLES20.glDrawArrays(4, 0, 6);
    }

    private void a(int i2, int i3, int i4) {
        int i5 = 0;
        if (i4 == 1) {
            GLES20.glViewport(0, 0, i3 / 2, i2);
            GLES20.glEnable(3089);
            GLES20.glScissor(0, 0, i3 / 2, i2);
            GLES20.glDisable(3089);
            for (int i6 = 0; i6 < this.k.d().length; i6++) {
                GLES20.glDrawElements(4, this.k.d()[i6], 5123, this.k.c()[i6]);
                b.a("glDrawElements");
            }
            GLES20.glViewport(i3 / 2, 0, i3 / 2, i2);
            GLES20.glEnable(3089);
            GLES20.glScissor(i3 / 2, 0, i3 / 2, i2);
            GLES20.glDisable(3089);
            while (i5 < this.k.d().length) {
                GLES20.glDrawElements(4, this.k.d()[i5], 5123, this.k.c()[i5]);
                b.a("glDrawElements");
                i5++;
            }
            return;
        }
        GLES20.glViewport(0, 0, i3, i2);
        GLES20.glEnable(3089);
        GLES20.glScissor(0, 0, i3, i2);
        GLES20.glDisable(3089);
        while (i5 < this.k.d().length) {
            GLES20.glDrawElements(4, this.k.d()[i5], 5123, this.k.c()[i5]);
            b.a("glDrawElements");
            i5++;
        }
    }

    private void a(float[] fArr, float[] fArr2, float[] fArr3, float[] fArr4, float[] fArr5, int i2, boolean z, int i3, int i4) {
        float[] fArr6 = new float[16];
        System.arraycopy(fArr, 0, fArr6, 0, 16);
        GLES20.glVertexAttribPointer(this.c, 3, 5126, false, this.k.b(), (Buffer) this.k.a());
        b.a("glVertexAttribPointer");
        GLES20.glEnableVertexAttribArray(this.f);
        b.a("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(this.f, 2, 5126, false, this.k.b(), this.k.a().duplicate().position(3));
        b.a("glVertexAttribPointer");
        Matrix.translateM(fArr6, 0, 0.0f, 1.0f, 0.0f);
        float[] fArr7 = (float[]) fArr3.clone();
        if (z) {
            Matrix.multiplyMM(this.h, 0, fArr4, 0, a(i2, fArr7), 0);
        } else {
            Matrix.multiplyMM(this.h, 0, fArr4, 0, fArr7, 0);
        }
        if ((i4 != 3 || i3 != 2) && (i4 != 3 || i3 != 3)) {
            Matrix.multiplyMM(fArr7, 0, fArr5, 0, fArr2, 0);
        } else if (z) {
            Matrix.multiplyMM(fArr7, 0, fArr5, 0, fArr2, 0);
            a(i2);
            Matrix.multiplyMM(fArr7, 0, b(i2, (float[]) fArr5.clone()), 0, fArr2, 0);
        } else {
            Matrix.multiplyMM(fArr7, 0, fArr5, 0, fArr2, 0);
        }
        Matrix.multiplyMM(this.j, 0, this.h, 0, fArr7, 0);
        GLES20.glUniformMatrix4fv(this.e, 1, false, fArr6, 0);
        b.a("glUniformMatrix4fv");
        GLES20.glUniformMatrix4fv(this.d, 1, false, this.j, 0);
        b.a("glUniformMatrix4fv");
    }

    private void a(int i2) {
        switch (i2) {
            case 3:
                Matrix.rotateM(this.h, 0, 180.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(this.h, 0, 0.0f, -1.0f, 0.0f);
                return;
            case 4:
            case 5:
            case 7:
            default:
                return;
            case 6:
                Matrix.rotateM(this.h, 0, -90.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(this.h, 0, 0.0f, -1.0f, 0.0f);
                return;
            case 8:
                Matrix.rotateM(this.h, 0, 90.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(this.h, 0, 0.0f, -1.0f, 0.0f);
                return;
        }
    }

    private float[] a(int i2, float[] fArr) {
        switch (i2) {
            case 3:
                Matrix.rotateM(fArr, 0, 180.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(fArr, 0, 0.0f, -1.0f, 0.0f);
                break;
            case 6:
                Matrix.rotateM(fArr, 0, 90.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(fArr, 0, 0.0f, -1.0f, 0.0f);
                break;
            case 8:
                Matrix.rotateM(fArr, 0, 270.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(fArr, 0, -1.0f, 0.0f, 0.0f);
                break;
        }
        return fArr;
    }

    private float[] b(int i2, float[] fArr) {
        switch (i2) {
            case 3:
                Matrix.rotateM(fArr, 0, 180.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(fArr, 0, 0.0f, -1.0f, 0.0f);
                break;
            case 6:
                Matrix.rotateM(fArr, 0, 90.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(fArr, 0, 0.0f, -1.0f, 0.0f);
                break;
            case 8:
                Matrix.rotateM(fArr, 0, 270.0f, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(fArr, 0, -1.0f, 0.0f, 0.0f);
                break;
        }
        return fArr;
    }

    private void a(float[] fArr, float[] fArr2, float[] fArr3, float[] fArr4, float f2) {
        float[] fArr5 = new float[16];
        float[] fArr6 = new float[16];
        if (f2 < 1.0f) {
            this.l = ByteBuffer.allocateDirect(72).order(ByteOrder.nativeOrder()).asFloatBuffer().put(new float[]{1.0f, -f2, -8.0f, -1.0f, f2, -8.0f, 1.0f, f2, -8.0f, 1.0f, -f2, -8.0f, -1.0f, -f2, -8.0f, -1.0f, f2, -8.0f});
        } else {
            float f3 = 1.0f / f2;
            this.l = ByteBuffer.allocateDirect(72).order(ByteOrder.nativeOrder()).asFloatBuffer().put(new float[]{-1.0f, f3, -8.0f, 1.0f, -f3, -8.0f, 1.0f, f3, -8.0f, -1.0f, f3, -8.0f, -1.0f, -f3, -8.0f, 1.0f, -f3, -8.0f});
        }
        this.l.position(0);
        GLES20.glVertexAttribPointer(this.c, 3, 5126, false, 0, (Buffer) this.l);
        b.a("glVertexAttribPointer");
        GLES20.glEnableVertexAttribArray(this.f);
        b.a("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(this.f, 2, 5126, false, 0, (Buffer) this.m);
        b.a("glVertexAttribPointer");
        Matrix.translateM(fArr5, 0, 0.0f, 1.0f, 0.0f);
        Matrix.multiplyMM(this.g, 0, fArr3, 0, fArr2, 0);
        Matrix.multiplyMM(fArr6, 0, fArr4, 0, fArr, 0);
        Matrix.multiplyMM(this.i, 0, this.g, 0, fArr6, 0);
        this.i = new float[]{1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f};
        GLES20.glUniformMatrix4fv(this.e, 1, false, new float[]{1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f}, 0);
        b.a("glUniformMatrix4fv");
        GLES20.glUniformMatrix4fv(this.d, 1, false, this.i, 0);
        b.a("glUniformMatrix4fv");
    }

    private void b(int i2, int i3) {
        switch (i2) {
            case 1:
                GLES20.glBindTexture(36197, i3);
                b.a("glBindTexture");
                return;
            case 2:
                GLES20.glBindTexture(36197, i3);
                b.a("glBindTexture");
                return;
            case 3:
                GLES20.glBindTexture(3553, i3);
                b.a("glBindTexture");
                return;
            default:
                return;
        }
    }

    public void a() {
        this.b.b();
    }

    private static String a(Context context, int i2) {
        InputStream openRawResource = context.getResources().openRawResource(i2);
        InputStreamReader inputStreamReader = new InputStreamReader(openRawResource);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine).append('\n');
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            try {
                break;
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        bufferedReader.close();
        try {
            inputStreamReader.close();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        try {
            openRawResource.close();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
        return sb.toString();
    }
}
