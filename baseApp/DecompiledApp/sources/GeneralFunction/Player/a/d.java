package GeneralFunction.Player.a;

import GeneralFunction.j;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f18a = d.class.getSimpleName();
    private FloatBuffer b;
    private ShortBuffer[] c;
    private int[] d;
    private int e;
    private j f = new j();

    public d(int i, float f2, float f3, float f4, float f5, int i2, int i3) {
        int i4 = i + 1;
        int i5 = i4 * i4;
        if (i5 > 32767) {
            throw new RuntimeException("nSlices " + i + " too big for vertex");
        }
        this.e = i * i * 6;
        float f6 = 3.1415927f / ((float) i);
        float f7 = 6.2831855f / ((float) i);
        this.b = ByteBuffer.allocateDirect(i5 * 5 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        this.c = new ShortBuffer[i2];
        this.d = new int[i2];
        int i6 = ((this.e / i2) / 6) * 6;
        for (int i7 = 0; i7 < i2 - 1; i7++) {
            this.d[i7] = i6;
        }
        this.d[i2 - 1] = this.e - (i6 * (i2 - 1));
        for (int i8 = 0; i8 < i2; i8++) {
            this.c[i8] = ByteBuffer.allocateDirect(this.d[i8] * 2).order(ByteOrder.nativeOrder()).asShortBuffer();
        }
        if (i3 <= 1) {
            a(0, i4, i4, f6, f7, f2, f3, f4, f5, i);
            a(i, i4);
        } else {
            a(i4, f6, f7, f2, f3, f4, f5, i, i3);
        }
        this.b.position(0);
        for (int i9 = 0; i9 < i2; i9++) {
            this.c[i9].position(0);
        }
    }

    private void a(int i, float f2, float f3, float f4, float f5, float f6, float f7, int i2, int i3) {
        ArrayList<b> arrayList = new ArrayList<>();
        for (int i4 = 0; i4 < i3; i4++) {
            b bVar = new b((i * i4) / i3, ((i4 + 1) * i) / i3, i, f2, f3, f4, f5, f6, f7, i2);
            arrayList.add(bVar);
            bVar.start();
        }
        a aVar = new a(i2, i);
        aVar.start();
        while (true) {
            if (!a(arrayList) || !aVar.a()) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            } else {
                return;
            }
        }
    }

    private boolean a(ArrayList<b> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (!arrayList.get(i).a()) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i, int i2) {
        short[] sArr = new short[a(this.d)];
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i; i5++) {
            for (int i6 = 0; i6 < i; i6++) {
                int i7 = i5 + 1;
                int i8 = i6 + 1;
                if (i4 >= this.d[i3]) {
                    this.c[i3].put(sArr, 0, this.d[i3]);
                    i3++;
                    i4 = 0;
                }
                int i9 = i4 + 1;
                sArr[i4] = (short) ((i5 * i2) + i6);
                int i10 = i9 + 1;
                sArr[i9] = (short) ((i7 * i2) + i6);
                int i11 = i10 + 1;
                sArr[i10] = (short) ((i7 * i2) + i8);
                int i12 = i11 + 1;
                sArr[i11] = (short) ((i5 * i2) + i6);
                int i13 = i12 + 1;
                sArr[i12] = (short) ((i7 * i2) + i8);
                i4 = i13 + 1;
                sArr[i13] = (short) ((i5 * i2) + i8);
            }
        }
        this.c[i3].put(sArr, 0, this.d[i3]);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i, int i2, int i3, float f2, float f3, float f4, float f5, float f6, float f7, int i4) {
        float[] fArr = new float[(i3 * 5)];
        while (i < i2) {
            for (int i5 = 0; i5 < i3; i5++) {
                int i6 = i5 * 5;
                float sin = (float) Math.sin((double) (((float) i) * f2));
                float cos = (float) Math.cos((double) (((float) i) * f2));
                fArr[i6 + 0] = (((float) Math.sin((double) (((float) i5) * f3))) * f7 * sin) + f4;
                fArr[i6 + 1] = (sin * f7 * ((float) Math.cos((double) (((float) i5) * f3)))) + f5;
                fArr[i6 + 2] = (f7 * cos) + f6;
                fArr[i6 + 3] = ((float) i5) / ((float) i4);
                fArr[i6 + 4] = (0.0f - ((float) i)) / ((float) i4);
            }
            this.f.a();
            this.b.position(fArr.length * i);
            this.b.put(fArr, 0, fArr.length);
            this.f.b();
            i++;
        }
    }

    public FloatBuffer a() {
        return this.b;
    }

    public int b() {
        return 20;
    }

    public ShortBuffer[] c() {
        return this.c;
    }

    public int[] d() {
        return this.d;
    }

    private int a(int[] iArr) {
        int i = iArr[0];
        for (int i2 = 1; i2 < iArr.length; i2++) {
            if (iArr[i2] > i) {
                i = iArr[i2];
            }
        }
        return i;
    }

    /* access modifiers changed from: private */
    public class b extends Thread {
        private boolean b = false;
        private int c;
        private int d;
        private int e;
        private float f;
        private float g;
        private float h;
        private float i;
        private float j;
        private float k;
        private int l;

        public b(int i2, int i3, int i4, float f2, float f3, float f4, float f5, float f6, float f7, int i5) {
            this.c = i2;
            this.d = i3;
            this.e = i4;
            this.f = f2;
            this.g = f3;
            this.h = f4;
            this.i = f5;
            this.j = f6;
            this.k = f7;
            this.l = i5;
        }

        public void run() {
            d.this.a(this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l);
            this.b = true;
        }

        public boolean a() {
            return this.b;
        }
    }

    /* access modifiers changed from: private */
    public class a extends Thread {
        private boolean b = false;
        private int c = 0;
        private int d = 0;

        public a(int i, int i2) {
            this.c = i;
            this.d = i2;
        }

        public void run() {
            d.this.a(this.c, this.d);
            this.b = true;
        }

        public boolean a() {
            return this.b;
        }
    }
}
