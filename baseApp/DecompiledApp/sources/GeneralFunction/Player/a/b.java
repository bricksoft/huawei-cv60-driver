package GeneralFunction.Player.a;

import android.opengl.GLES20;
import android.util.Log;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f16a = b.class.getSimpleName();

    public static int a(int i) {
        int[] iArr = new int[1];
        GLES20.glGenTextures(1, iArr, 0);
        int i2 = iArr[0];
        GLES20.glActiveTexture(33984);
        if (i == 1 || i == 2) {
            try {
                GLES20.glBindTexture(36197, i2);
                GLES20.glTexParameteri(36197, 10241, 9729);
                GLES20.glTexParameteri(36197, 10240, 9729);
                GLES20.glTexParameteri(36197, 10242, 33071);
                GLES20.glTexParameteri(36197, 10243, 33071);
            } catch (RuntimeException e) {
                Log.e(f16a, e.toString(), e);
                if (i2 != -1) {
                    GLES20.glDeleteTextures(1, iArr, 0);
                }
                return -1;
            }
        }
        if (i != 3) {
            return i2;
        }
        GLES20.glBindTexture(3553, i2);
        GLES20.glTexParameteri(3553, 10241, 9729);
        GLES20.glTexParameteri(3553, 10240, 9729);
        GLES20.glTexParameteri(3553, 10242, 33071);
        GLES20.glTexParameteri(3553, 10243, 33071);
        return i2;
    }

    public static void a(String str) {
        int glGetError = GLES20.glGetError();
        if (glGetError != 0) {
            String str2 = str + ": glError 0x" + Integer.toHexString(glGetError);
            Log.e(f16a, str2);
            throw new RuntimeException(str2);
        }
    }
}
