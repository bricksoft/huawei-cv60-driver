package GeneralFunction.b;

import android.content.Context;
import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

public class a {
    private static String f = "Debug";
    private static double g = 100.0d;
    private static long h = 100;
    private static long m = 0;
    private static int n = 65;

    /* renamed from: a  reason: collision with root package name */
    private final long f63a = System.currentTimeMillis();
    private final String b = (GeneralFunction.g.a.a() + "/AutoDump/");
    private final String c = (GeneralFunction.g.a.a() + "/AutoDump/Log/");
    private final String d = (GeneralFunction.g.a.a() + "/AutoDump/Log/" + this.f63a + ".log");
    private final String e = (GeneralFunction.g.a.a() + "/AutoDump/Log/" + this.f63a + ".dbg");
    private Context i = null;
    private String j = "";
    private long k = 0;
    private long l = 0;

    public a(Context context) {
        this.i = context;
    }

    public a(Context context, String str, double d2, long j2) {
        this.i = context;
        f = str;
        g = d2;
        h = j2;
        File file = new File(this.b);
        if (!file.exists()) {
            file.mkdir();
        }
        File file2 = new File(this.c);
        if (!file2.exists()) {
            file2.mkdir();
        }
        e();
    }

    public void a(boolean z, boolean z2) {
        try {
            Thread.sleep((long) n);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    public void a() {
    }

    public void b() {
        File file = new File(this.c);
        long a2 = a(file);
        if (file.exists() && a2 != 0) {
            a(this.c, a2);
        }
    }

    private long a(File file) {
        File[] listFiles = file.listFiles();
        if (!file.isDirectory() || listFiles == null) {
            return 0;
        }
        return (long) listFiles.length;
    }

    private void a(String str, long j2) {
        File[] listFiles = new File(str).listFiles();
        Arrays.sort(listFiles, new Comparator<File>() {
            /* class GeneralFunction.b.a.AnonymousClass1 */

            /* renamed from: a */
            public int compare(File file, File file2) {
                long lastModified = file.lastModified() - file2.lastModified();
                if (lastModified > 0) {
                    return 1;
                }
                if (lastModified == 0) {
                    return 0;
                }
                return -1;
            }

            public boolean equals(Object obj) {
                return true;
            }
        });
        for (int i2 = 0; ((long) i2) < j2; i2++) {
            new File(str + listFiles[i2].getName()).delete();
        }
    }

    private void e() {
        long a2 = (a(new File(this.c)) + 1) - h;
        if (a2 > 0) {
            a(this.c, a2);
        }
    }

    public static double c() {
        return g;
    }

    public static long d() {
        return h;
    }
}
