package GeneralFunction.o;

import GeneralFunction.d;

public class a {
    private void a(String str, int i) {
        d.a("SystemDump", str, i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0046 A[SYNTHETIC, Splitter:B:13:0x0046] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r6) {
        /*
            r5 = this;
            r4 = 0
            java.lang.String r0 = "dumpTask S"
            r5.a(r0, r4)
            r1 = 0
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0054 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0054 }
            r2.<init>()     // Catch:{ IOException -> 0x0054 }
            java.lang.String r3 = "ps -t "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0054 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ IOException -> 0x0054 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0054 }
            java.lang.Process r2 = r0.exec(r2)     // Catch:{ IOException -> 0x0054 }
            if (r2 == 0) goto L_0x0058
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0054 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0054 }
            java.io.InputStream r2 = r2.getInputStream()     // Catch:{ IOException -> 0x0054 }
            r3.<init>(r2)     // Catch:{ IOException -> 0x0054 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0054 }
        L_0x0032:
            java.lang.String r1 = r0.readLine()     // Catch:{ IOException -> 0x003d }
            if (r1 == 0) goto L_0x0044
            r2 = 0
            r5.a(r1, r2)     // Catch:{ IOException -> 0x003d }
            goto L_0x0032
        L_0x003d:
            r1 = move-exception
            r2 = r1
            r3 = r0
        L_0x0040:
            r2.printStackTrace()
            r0 = r3
        L_0x0044:
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ IOException -> 0x004f }
        L_0x0049:
            java.lang.String r0 = "dumpTask E"
            r5.a(r0, r4)
            return
        L_0x004f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0049
        L_0x0054:
            r0 = move-exception
            r2 = r0
            r3 = r1
            goto L_0x0040
        L_0x0058:
            r0 = r1
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: GeneralFunction.o.a.a(int):void");
    }
}
