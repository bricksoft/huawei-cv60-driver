package GeneralFunction.m;

import GeneralFunction.d;
import a.c.b;
import android.os.Message;
import com.huawei.cvIntl60.R;
import ui_Controller.b.f;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static f f125a = null;
    public static UI_ModeMain b = null;

    public a(UI_ModeMain uI_ModeMain) {
        b = uI_ModeMain;
        f125a = uI_ModeMain.c.l;
    }

    private void a(String str, int i) {
        d.a("UI_SettingControl", str, i);
    }

    public void a(Message message) {
        a.c.a aVar = new a.c.a(message);
        if (aVar.b("result") == -1) {
            a("CONNECTION ERROR!", 0);
            return;
        }
        switch (message.what) {
            case 16898:
            case 17410:
                f125a.b = aVar.d("DscName");
                f125a.f1322a = aVar.d("DscVersion");
                f125a.c = aVar.d("DscGuid");
                f125a.Z = Integer.parseInt(f125a.f1322a.split("\\.")[1], 16);
                f125a.aa = Integer.parseInt(b.getResources().getResourceEntryName(R.raw.fw13b00).substring("fw".length()), 16);
                a("DscVersion:" + f125a.Z + "UpgradeVersion:" + f125a.aa, 4);
                return;
            case 16992:
            case 17504:
                C(b.q(aVar.d("value")));
                return;
            case 17153:
                D(aVar.b("value"));
                return;
            case 17154:
                E(aVar.b("value"));
                return;
            case 17157:
                m(b.c(aVar.d("value")));
                return;
            case 17159:
            case 17678:
                k(b.a(aVar.d("value")));
                return;
            case 17162:
            case 17681:
                q(b.e(aVar.d("value")));
                return;
            case 17165:
            case 17674:
                l(b.b(aVar.d("value")));
                return;
            case 17167:
            case 17676:
                p(b.d(aVar.d("value")));
                return;
            case 17169:
                r(b.f(aVar.d("value")));
                return;
            case 17171:
            case 17702:
                s(b.g(aVar.d("value")));
                return;
            case 17173:
            case 17696:
                t(b.h(aVar.d("value")));
                return;
            case 17175:
            case 17686:
                v(b.j(aVar.d("value")));
                return;
            case 17177:
                w(b.k(aVar.d("value")));
                return;
            case 17179:
                x(b.l(aVar.d("value")));
                return;
            case 17181:
            case 17692:
                y(b.m(aVar.d("value")));
                return;
            case 17183:
            case 17694:
                z(b.n(aVar.d("value")));
                return;
            case 17185:
                A(b.o(aVar.d("value")));
                return;
            case 17187:
            case 17688:
                u(b.i(aVar.d("value")));
                return;
            case 17189:
            case 17690:
                B(b.p(aVar.d("value")));
                return;
            case 17191:
            case 17669:
                a(aVar.d("value"));
                return;
            case 17193:
            case 17671:
                b(aVar.d("value"));
                return;
            case 18435:
                w();
                return;
            default:
                return;
        }
    }

    public static int a() {
        return f125a.d;
    }

    public static int b() {
        return f125a.e;
    }

    public static int c() {
        return f125a.f;
    }

    public static int d() {
        return f125a.g;
    }

    public static int e() {
        return f125a.h;
    }

    public static int f() {
        return f125a.i;
    }

    public static int g() {
        return f125a.j;
    }

    public static int h() {
        return f125a.l;
    }

    public static int i() {
        return f125a.k;
    }

    public static int j() {
        return f125a.I;
    }

    public static int k() {
        return f125a.J;
    }

    public static int l() {
        return f125a.K;
    }

    public static int m() {
        return f125a.m;
    }

    public static int n() {
        return f125a.I;
    }

    public static int o() {
        return f125a.J;
    }

    public static int p() {
        return f125a.K;
    }

    public static int q() {
        return f125a.M;
    }

    public static boolean r() {
        return f125a.R;
    }

    public static boolean s() {
        return f125a.S;
    }

    public static boolean t() {
        return f125a.ae;
    }

    public static boolean u() {
        return f125a.af;
    }

    public static void a(int i) {
        f125a.d = i;
    }

    public static void b(int i) {
        f125a.e = i;
    }

    public static void c(int i) {
        f125a.f = i;
    }

    public static void d(int i) {
        f125a.g = i;
    }

    public static void e(int i) {
        f125a.h = i;
    }

    public static void f(int i) {
        f125a.i = i;
    }

    public static void g(int i) {
        f125a.j = i;
    }

    public static void h(int i) {
        f125a.l = i;
    }

    public static void i(int i) {
        f125a.k = i;
    }

    public static void j(int i) {
        f125a.m = i;
    }

    public static void a(boolean z) {
        f125a.ae = z;
    }

    public static void b(boolean z) {
        f125a.af = z;
    }

    public static void v() {
        f125a.ae = false;
        f125a.af = false;
    }

    public static void w() {
        f125a.I = 6;
        f125a.J = 0;
        f125a.K = 0;
    }

    public static void k(int i) {
        f125a.f = i;
    }

    public static void l(int i) {
        f125a.v = i;
    }

    public static void m(int i) {
        f125a.I = i;
    }

    public static void n(int i) {
        f125a.J = i;
    }

    public static void o(int i) {
        f125a.K = i;
    }

    public static void p(int i) {
        f125a.w = i;
    }

    public static void q(int i) {
        f125a.e = i;
    }

    public static void r(int i) {
        f125a.L = i;
    }

    public static void s(int i) {
        f125a.E = i;
    }

    public static void t(int i) {
        f125a.D = i;
    }

    public static void u(int i) {
        f125a.x = i;
    }

    public static void v(int i) {
        f125a.y = i;
    }

    public static void w(int i) {
        f125a.F = i;
    }

    public static void x(int i) {
        f125a.G = i;
    }

    public static void y(int i) {
        f125a.A = i;
    }

    public static void z(int i) {
        f125a.B = i;
    }

    public static void A(int i) {
        f125a.H = i;
    }

    public static void B(int i) {
        f125a.z = i;
    }

    public static void a(String str) {
        b.c.j.d = str;
    }

    public static void b(String str) {
        b.c.j.e = str;
    }

    public static void C(int i) {
        f125a.N = i;
    }

    public static void D(int i) {
        f125a.O = i;
    }

    public static void E(int i) {
        f125a.P = i;
    }

    public static void c(boolean z) {
    }

    public static void d(boolean z) {
        f125a.S = z;
    }

    public static void F(int i) {
        a.c.a aVar = new a.c.a(18512);
        aVar.a("setting", i);
        b.c(aVar);
    }

    public static void G(int i) {
        a.c.a aVar = new a.c.a(18513);
        aVar.a("setting", i);
        b.c(aVar);
    }

    public static void H(int i) {
    }

    public static int c(String str) {
        if (b.b() != 33) {
            return -1;
        }
        a.c.a aVar = new a.c.a(17240);
        aVar.a("value", str);
        b.c(aVar);
        return 0;
    }

    public static int x() {
        switch (b.e(c())) {
            case 3:
            default:
                return 5376;
            case 4:
                return 3840;
        }
    }

    public static int y() {
        switch (b.e(c())) {
            case 3:
            default:
                return 2688;
            case 4:
                return 1920;
        }
    }

    public static int z() {
        switch (b.f(b())) {
            case 10:
                return 1280;
            default:
                return 1920;
        }
    }

    public static int A() {
        switch (b.f(b())) {
            case 10:
                return 640;
            default:
                return 960;
        }
    }
}
