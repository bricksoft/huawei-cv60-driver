package GeneralFunction;

import GeneralFunction.f;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.plus.PlusShare;
import com.huawei.cvIntl60.R;
import ui_Controller.ui_Setting.UI_SettingInformation;

public class h extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private b f93a;
    private View b;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(b bVar) {
        this.f93a = bVar;
    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        return layoutInflater.inflate(R.layout.dialog_launch, viewGroup, false);
    }

    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        WindowManager.LayoutParams attributes = getDialog().getWindow().getAttributes();
        attributes.gravity = 80;
        attributes.width = -1;
        attributes.height = -1;
        getDialog().getWindow().setAttributes(attributes);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        if (this.f93a != null) {
            this.b = view.findViewById(R.id.content);
            ((Button) view.findViewById(R.id.positive_button)).setText(this.f93a.f99a);
            view.findViewById(R.id.positive_button).setOnClickListener(new View.OnClickListener() {
                /* class GeneralFunction.h.AnonymousClass1 */

                public void onClick(View view) {
                    h.this.f93a.d.onClick(null, -1);
                    h.this.dismiss();
                }
            });
            ((Button) view.findViewById(R.id.negative_button)).setText(this.f93a.b);
            view.findViewById(R.id.negative_button).setOnClickListener(new View.OnClickListener() {
                /* class GeneralFunction.h.AnonymousClass2 */

                public void onClick(View view) {
                    h.this.f93a.e.onClick(null, -1);
                    h.this.dismiss();
                }
            });
            if (this.f93a.c) {
                ((TextView) view.findViewById(R.id.hyper_text)).setText(a());
                ((TextView) view.findViewById(R.id.hyper_text)).setMovementMethod(LinkMovementMethod.getInstance());
            } else {
                view.findViewById(R.id.hyper_text).setVisibility(4);
            }
            setCancelable(false);
        }
    }

    @Override // android.app.Fragment, android.app.DialogFragment
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void onDetach() {
        super.onDetach();
    }

    private CharSequence a() {
        return f.a(getContext()).a(R.string.launch_hypertext).a(new f.b() {
            /* class GeneralFunction.h.AnonymousClass3 */

            @Override // GeneralFunction.f.b
            public void a(String str) {
                char c = 65535;
                switch (str.hashCode()) {
                    case 96321:
                        if (str.equals("aaa")) {
                            c = 0;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        Intent intent = new Intent(h.this.getContext(), UI_SettingInformation.class);
                        intent.putExtra("url", h.this.getString(R.string.user_agreement_url));
                        intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, h.this.getString(R.string.agreement_page_title));
                        h.this.startActivity(intent);
                        return;
                    default:
                        return;
                }
            }
        }).a(false);
    }

    /* access modifiers changed from: private */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private CharSequence f99a;
        private CharSequence b;
        private boolean c;
        private DialogInterface.OnClickListener d;
        private DialogInterface.OnClickListener e;

        private b() {
            this.c = true;
        }
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private b f97a = new b();
        private Context b;

        public a(Context context) {
            this.b = context;
        }

        public a a(@StringRes int i, DialogInterface.OnClickListener onClickListener) {
            this.f97a.d = onClickListener;
            this.f97a.f99a = this.b.getString(i);
            return this;
        }

        public a b(@StringRes int i, DialogInterface.OnClickListener onClickListener) {
            this.f97a.b = this.b.getString(i);
            this.f97a.e = onClickListener;
            return this;
        }

        public h a() {
            h hVar = new h();
            hVar.a((h) this.f97a);
            return hVar;
        }
    }
}
