package GeneralFunction;

import a.c.a;
import java.util.Timer;
import java.util.TimerTask;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static m[] f91a = null;
    private static Timer b = null;
    private static UI_ModeMain c = null;

    public static void a(int i, int i2) {
        d.a("IsrTimer", "UI_SetIsrTimer:" + i + " " + i2, 4);
        f91a[i].f124a = 1;
        f91a[i].c = 1;
        f91a[i].b = i2 / 20;
    }

    public static void a(int i) {
        d.a("IsrTimer", "UI_DeleteIsrTimer:" + i, 4);
        f91a[i].f124a = 0;
    }

    public static void a(UI_ModeMain uI_ModeMain) {
        c = uI_ModeMain;
        f91a = new m[9];
        for (int i = 0; i < 9; i++) {
            f91a[i] = new m();
        }
        d();
    }

    public static void a() {
        if (b != null) {
            b.cancel();
            b = null;
        }
        d();
    }

    private static void d() {
        b = new Timer();
        b.scheduleAtFixedRate(new TimerTask() {
            /* class GeneralFunction.g.AnonymousClass1 */

            public void run() {
                for (int i = 0; i < 9; i++) {
                    if (g.f91a[i].c == 1 && g.f91a[i].f124a > 0) {
                        m mVar = g.f91a[i];
                        int i2 = mVar.f124a + 1;
                        mVar.f124a = i2;
                        if (i2 > g.f91a[i].b) {
                            g.c.a(new a(61440 + i));
                            g.f91a[i].f124a = 1;
                        }
                    }
                }
            }
        }, 20, 20);
    }
}
