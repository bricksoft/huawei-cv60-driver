package GeneralFunction.c;

public class d {

    /* renamed from: a  reason: collision with root package name */
    public int f68a;
    public long b;
    public long c;
    public long d;
    public long e;
    public long f = 0;
    private boolean g = false;
    private byte[] h;

    public d(byte[] bArr) {
        this.h = bArr;
        this.f68a = bArr.length;
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = 0;
    }

    public byte[] a() {
        return this.h;
    }

    public boolean b() {
        return this.g;
    }

    public void c() {
        this.g = true;
    }

    public void d() {
        this.g = false;
    }

    public void e() {
        this.h = null;
    }
}
