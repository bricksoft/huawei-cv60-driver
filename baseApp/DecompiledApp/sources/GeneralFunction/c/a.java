package GeneralFunction.c;

import GeneralFunction.j;
import java.util.ArrayList;

public class a {

    /* renamed from: a  reason: collision with root package name */
    final j f66a = new j();
    private int b = 0;
    private ArrayList<d> c = null;
    private int d = 0;

    public a(int i, int i2) {
        this.b = i;
        this.c = new ArrayList<>();
        for (int i3 = 0; i3 < i; i3++) {
            this.c.add(new d(new byte[i2]));
        }
    }

    public d a() {
        d dVar;
        this.f66a.a();
        int i = 0;
        while (true) {
            if (i >= this.b) {
                dVar = null;
                break;
            }
            int i2 = ((this.d + i) + 1) % this.b;
            if (!this.c.get(i2).b()) {
                dVar = this.c.get(i2);
                dVar.c();
                this.d = i2;
                break;
            }
            i++;
        }
        this.f66a.b();
        if (i == this.b) {
            return null;
        }
        return dVar;
    }

    public void b() {
        for (int i = 0; i < this.c.size(); i++) {
            this.c.get(i).e();
        }
        this.c.clear();
        this.c = null;
    }
}
