package GeneralFunction.g;

import GeneralFunction.d;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static String f92a = null;

    public static String a() {
        if (f92a != null) {
            return f92a;
        }
        if (new File("/storage/emulated/0").exists()) {
            f92a = "/storage/emulated/0";
            return f92a;
        }
        String[] list = new File("/storage").list();
        d.a("FS_API", "Can't find internal path, exist path as following!", 0);
        for (int i = 0; i < list.length; i++) {
            d.a("FS_API", "[" + i + "]" + list[i], 0);
        }
        f92a = Environment.getExternalStorageDirectory().getPath();
        return f92a;
    }

    public static boolean a(String str) {
        String lowerCase = str.toLowerCase();
        if (lowerCase.contains("mov") || lowerCase.contains("mp4") || lowerCase.contains("avi")) {
            return true;
        }
        return false;
    }

    public static String a(String str, String str2) {
        String str3 = null;
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
        if (!new File(str + str2).exists()) {
            return str + str2;
        }
        for (int i = 1; i < 100; i++) {
            str3 = str + (str2.substring(0, str2.lastIndexOf(".")) + "(" + i + ")" + str2.substring(str2.lastIndexOf(".")));
            if (!new File(str3).exists()) {
                return str3;
            }
        }
        return str3;
    }

    public static String b(String str) {
        return a(str.substring(0, str.lastIndexOf("/") + 1), str.substring(str.lastIndexOf("/") + 1, str.length()));
    }

    public static String a(Long l, String str) {
        return "PIC_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date(l.longValue())) + "." + str;
    }

    public static String c(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf != -1) {
            return str.substring(lastIndexOf + 1, str.length()).toLowerCase();
        }
        return null;
    }

    public static String b(String str, String str2) {
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf != -1) {
            return str.substring(0, lastIndexOf + 1) + str2;
        }
        return null;
    }

    public static String d(String str) {
        int lastIndexOf;
        String str2;
        d.a("FS_API", "ChangeVideoNameToThmName input result:" + str, 4);
        int lastIndexOf2 = str.lastIndexOf(46);
        boolean contains = str.toLowerCase().contains("thm");
        if (lastIndexOf2 != -1 && !contains) {
            String substring = str.substring(str.lastIndexOf(47) + 1, lastIndexOf2);
            if (str.contains("/Android/data/com.huawei.cvIntl60/")) {
                lastIndexOf = str.lastIndexOf("/Android/data/com.huawei.cvIntl60/");
            } else {
                lastIndexOf = str.contains("DCIM/CV60/") ? str.toUpperCase().lastIndexOf("/DCIM/CV60/") : -1;
            }
            if (lastIndexOf != -1) {
                String substring2 = str.substring(0, lastIndexOf);
                File file = new File(substring2 + "/Android/data/com.huawei.cvIntl60/VideoThumbnail/");
                if (!file.exists()) {
                    d.a("FS_API", "video thumbnail mkdir result: " + file.mkdirs(), 3);
                }
                str2 = substring2 + "/Android/data/com.huawei.cvIntl60/VideoThumbnail/" + substring + "_thm.jpg";
            } else {
                str2 = str.substring(0, lastIndexOf2) + "_thm.jpg";
            }
            str = str2;
        }
        d.a("FS_API", "ChangeVideoNameToThmName output result:" + str, 4);
        return str;
    }

    public static void c(String str, String str2) {
        if (str == null) {
            d.a("FS_API", "Null srcPath!! Skip FileCopy. srcPath: " + str, 0);
        } else if (str2 == null) {
            d.a("FS_API", "Null dstPath!! Skip FileCopy. dstPath: " + str2, 0);
        } else {
            File file = new File(str);
            File file2 = new File(str2);
            if (!file.exists()) {
                d.a("FS_API", "FileCopy err: src not exist! " + str, 1);
            }
            if (file2.exists()) {
                file2.delete();
            }
            FileChannel channel = new FileInputStream(file).getChannel();
            FileChannel channel2 = new FileOutputStream(file2).getChannel();
            channel2.transferFrom(channel, 0, channel.size());
            channel2.force(true);
            channel.close();
            channel2.close();
        }
    }

    public static void a(String str, Context context) {
        File file = new File(str);
        String lowerCase = str.substring(str.lastIndexOf(".") + 1).toLowerCase();
        if (lowerCase.equals("jpg")) {
            String[] strArr = {"_id"};
            String[] strArr2 = {str};
            if (Uri.fromFile(file) != null) {
                Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, strArr, "_data=?", strArr2, null);
                if (query == null || !query.moveToFirst()) {
                    file.delete();
                } else {
                    context.getContentResolver().delete(ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, query.getLong(query.getColumnIndexOrThrow("_id"))), null, null);
                }
                if (query != null) {
                    query.close();
                }
            }
        } else if (a(lowerCase)) {
            String[] strArr3 = {"_id"};
            String[] strArr4 = {str};
            if (Uri.fromFile(file) != null) {
                Cursor query2 = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, strArr3, "_data=?", strArr4, null);
                if (query2 == null || !query2.moveToFirst()) {
                    file.delete();
                } else {
                    context.getContentResolver().delete(ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, query2.getLong(query2.getColumnIndexOrThrow("_id"))), null, null);
                }
                if (query2 != null) {
                    query2.close();
                }
            }
        } else {
            file.delete();
        }
        context.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
    }

    public static Bitmap e(String str) {
        if (!new File(str).exists()) {
            return null;
        }
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            mediaMetadataRetriever.setDataSource(str);
            Bitmap frameAtTime = mediaMetadataRetriever.getFrameAtTime(0);
            mediaMetadataRetriever.release();
            if (frameAtTime == null) {
                d.a("FS_API", "Can not get first frame:" + str, 1);
                return null;
            }
            int width = frameAtTime.getWidth();
            int height = frameAtTime.getHeight();
            Matrix matrix = new Matrix();
            matrix.postScale(960.0f / ((float) width), 480.0f / ((float) height));
            Bitmap createBitmap = Bitmap.createBitmap(frameAtTime, 0, 0, width, height, matrix, false);
            if (frameAtTime != createBitmap) {
                frameAtTime.recycle();
            }
            return createBitmap;
        } catch (IllegalArgumentException e) {
            mediaMetadataRetriever.release();
            e.printStackTrace();
            return null;
        }
    }

    public static int d(String str, String str2) {
        Bitmap e = e(str);
        if (e == null) {
            d.a("FS_API", "Can not get first frame:" + str, 1);
            return -1;
        }
        File file = new File(str2);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e2) {
                e2.printStackTrace();
                return -1;
            }
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            e.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            e.recycle();
            try {
                fileOutputStream.flush();
                fileOutputStream.getFD().sync();
                fileOutputStream.close();
                return 0;
            } catch (IOException e3) {
                e3.printStackTrace();
                return -1;
            }
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            return -1;
        }
    }

    public static void a(Context context, String str) {
        MediaScannerConnection.scanFile(context, new String[]{str}, null, new MediaScannerConnection.OnScanCompletedListener() {
            /* class GeneralFunction.g.a.AnonymousClass1 */

            public void onScanCompleted(String str, Uri uri) {
            }
        });
    }
}
