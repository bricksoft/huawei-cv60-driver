package ThirdParty.OpenCV;

import android.content.Context;
import android.util.Log;

public class OpenCVFunc {

    /* renamed from: a  reason: collision with root package name */
    private static final String f155a = OpenCVFunc.class.getSimpleName();
    private Context b = null;

    public native boolean PanoramaPath(String str, String str2, double d, int i);

    public native boolean cancelStitchMatch();

    private void a(String str, int i) {
        Log.e(f155a, str);
    }

    public OpenCVFunc() {
        a("Load lib S", 0);
        System.loadLibrary("ViewController");
        a("Load lib E", 0);
    }
}
