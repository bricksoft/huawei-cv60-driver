package ThirdParty.a;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.youtube.YouTubeScopes;
import java.util.Arrays;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class a implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String[] b = {Scopes.PROFILE, YouTubeScopes.YOUTUBE};

    /* renamed from: a  reason: collision with root package name */
    public GoogleAccountCredential f159a;
    private String c;
    private GoogleApiClient d = null;
    private UI_ModeMain e;
    private AbstractC0008a f;
    private boolean g = false;

    /* renamed from: ThirdParty.a.a$a  reason: collision with other inner class name */
    public interface AbstractC0008a {
        void a(int i);

        void a(int i, String str);
    }

    public a(UI_ModeMain uI_ModeMain) {
        this.e = uI_ModeMain;
    }

    private void a(String str, int i) {
        if (i <= 5) {
            Log.e("GoogleLoginManager", str);
        }
    }

    public void a(String str) {
        this.f159a = GoogleAccountCredential.usingOAuth2(this.e, Arrays.asList(b));
        this.f159a.setBackOff(new ExponentialBackOff());
        this.f159a.setSelectedAccountName(str);
    }

    private void e() {
        Plus.AccountApi.clearDefaultAccount(this.d);
        this.f.a(0);
    }

    public void a() {
        a("[DEBUG] Enter accountLogin", 3);
        this.f159a = GoogleAccountCredential.usingOAuth2(this.e, Arrays.asList(b));
        this.f159a.setBackOff(new ExponentialBackOff());
        f();
        a("[DEBUG] accountLogin:" + this.f159a.getSelectedAccount(), 3);
    }

    public void b() {
        this.f.a(0);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
    public void onConnectionFailed(ConnectionResult connectionResult) {
        a("[DEBUG] onConnectionFailed", 3);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
    public void onConnected(Bundle bundle) {
        a("[DEBUG] onConnected", 3);
        if (this.g) {
            e();
            this.g = false;
            return;
        }
        try {
            this.c = Plus.AccountApi.getAccountName(this.d);
        } catch (SecurityException e2) {
            a(e2.toString(), 3);
        }
        this.f159a.setSelectedAccountName(this.c);
        a("[DEBUG] AccountName: " + this.c, 3);
        a("[DEBUG] Scope: " + this.f159a.getScope(), 3);
        this.f.a(0, this.c);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
    public void onConnectionSuspended(int i) {
        a("[DEBUG] onConnectionSuspended", 3);
    }

    public void a(int i, int i2, Intent intent) {
        a("[DEBUG] handleAvtivityResult requestCode" + i, 3);
        switch (i) {
            case 200:
                a("[DEBUG] REQUEST_GOOGLE_PLAY_SERVICES", 3);
                if (i2 == -1) {
                    g();
                    return;
                } else {
                    d();
                    return;
                }
            case 201:
            default:
                return;
            case 202:
                a("[DEBUG] REQUEST_ACCOUNT_PICKER", 3);
                if (i2 != -1 || intent == null || intent.getExtras() == null) {
                    this.f.a(-1, null);
                    return;
                }
                String string = intent.getExtras().getString("authAccount");
                if (string != null) {
                    this.c = string;
                    this.f159a.setSelectedAccountName(string);
                    a("[DEBUG] REQUEST_ACCOUNT_PICKER:" + this.c, 3);
                    this.f.a(0, this.c);
                    return;
                }
                a("[DEBUG] accountName NULL", 3);
                this.f.a(-1, null);
                return;
            case 203:
                a("[DEBUG] REQUEST_AUTHORIZATION", 3);
                if (i2 != -1) {
                    f();
                    return;
                }
                return;
        }
    }

    private void f() {
        a("[DEBUG] chooseAccount", 3);
        this.e.c.b.startActivityForResult(this.f159a.newChooseAccountIntent(), 202);
    }

    private void g() {
        a("[DEBUG] haveGooglePlayServices", 3);
        if (this.f159a.getSelectedAccountName() == null) {
            f();
        }
    }

    public boolean c() {
        if (this.f159a == null) {
            return false;
        }
        a("Google getSelectedAccountName: " + this.f159a.getSelectedAccountName(), 0);
        if (this.f159a.getSelectedAccountName() != null) {
            return true;
        }
        return false;
    }

    public boolean d() {
        a("[DEBUG] checkGooglePlayServicesAvailable", 3);
        if (GooglePlayServicesUtil.isUserRecoverableError(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.e))) {
            return false;
        }
        return true;
    }

    public void a(AbstractC0008a aVar) {
        this.f = aVar;
    }
}
