package ThirdParty.b;

import android.app.Activity;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.InputDeviceCompat;
import android.util.Log;
import com.google.android.exoplayer.hls.HlsChunkSource;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.CdnSettings;
import com.google.api.services.youtube.model.IngestionInfo;
import com.google.api.services.youtube.model.LiveBroadcast;
import com.google.api.services.youtube.model.LiveBroadcastContentDetails;
import com.google.api.services.youtube.model.LiveBroadcastListResponse;
import com.google.api.services.youtube.model.LiveBroadcastSnippet;
import com.google.api.services.youtube.model.LiveBroadcastStatus;
import com.google.api.services.youtube.model.LiveStream;
import com.google.api.services.youtube.model.LiveStreamListResponse;
import com.google.api.services.youtube.model.LiveStreamSnippet;
import com.google.api.services.youtube.model.LiveStreamStatus;
import com.google.api.services.youtube.model.MonitorStreamInfo;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class b {
    private static b x = null;

    /* renamed from: a  reason: collision with root package name */
    private HandlerThread f161a;
    private Handler b;
    private Handler c;
    private a d = null;
    private Activity e;
    private YouTube f;
    private HttpTransport g = null;
    private JsonFactory h = null;
    private int i = 0;
    private int j;
    private int k;
    private int l;
    private byte[] m;
    private int n;
    private int o;
    private int p;
    private int q;
    private boolean r = false;
    private boolean s = true;
    private String t = null;
    private String u = null;
    private String v = null;
    private String w = null;

    public interface a {
        void a(int i, int i2);

        void a(String str, String str2);

        void a(boolean z);
    }

    static /* synthetic */ int q(b bVar) {
        int i2 = bVar.i;
        bVar.i = i2 + 1;
        return i2;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        if (i2 <= 5) {
            Log.e("YouTubeManager", str);
        }
    }

    private b() {
    }

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (x == null) {
                x = new b();
            }
            bVar = x;
        }
        return bVar;
    }

    public void a(Activity activity) {
        this.e = activity;
    }

    public void a(GoogleAccountCredential googleAccountCredential) {
        this.g = AndroidHttp.newCompatibleTransport();
        this.h = new GsonFactory();
        this.f = new YouTube.Builder(this.g, this.h, googleAccountCredential).setApplicationName("WatchMe").build();
        b();
        a(4096, 0);
    }

    public void a(Message message) {
        int i2 = message.what;
        if ((i2 & 61440) == 4096) {
            this.b.sendMessage(message);
        } else if ((i2 & 61440) == 8192) {
            this.c.sendMessage(message);
        }
    }

    public void a(int i2) {
        Message message = new Message();
        message.what = i2;
        a(message);
    }

    public void a(int i2, long j2) {
        Message message = new Message();
        message.what = i2;
        this.b.sendMessageDelayed(message, j2);
    }

    private void b() {
        this.f161a = new HandlerThread("youtubeApiServer");
        this.f161a.start();
        this.b = new Handler(this.f161a.getLooper()) {
            /* class ThirdParty.b.b.AnonymousClass1 */

            public void handleMessage(Message message) {
                int i;
                int i2 = -1;
                super.handleMessage(message);
                b.this.a((b) ("[DEBUG] youtubeApiServerHandler: 0x" + Integer.toHexString(message.what)), (String) 1);
                switch (message.what) {
                    case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*{ENCODED_INT: 4097}*/:
                        a.c.a aVar = new a.c.a(message);
                        b.this.a((b) ("[DEBUG] YM MSG_YOUTUBE_CREATE_EVENT " + aVar.d("liveStreamTitle")), (String) 3);
                        try {
                            b.this.a((b) aVar, (a.c.a) false);
                            Thread.sleep(500);
                            List c = b.c(b.this.f);
                            b.this.u = ((a) c.get(c.size() - 1)).a();
                            b.this.t = ((a) c.get(c.size() - 1)).b();
                            b.this.w = "https://www.youtube.com/watch?v=" + b.this.u;
                            b.this.a((b) ("[DEBUG] broadcastId: " + b.this.u), (String) 3);
                            b.this.a((b) ("[DEBUG] rtmpUrl: " + b.this.t), (String) 3);
                            b.this.a((b) ("[DEBUG] streaming link: " + b.this.w), (String) 3);
                            if (b.this.t == null) {
                                i = -1;
                            } else {
                                if (b.this.s) {
                                    ThirdParty.Rtmp.a.a().a(b.this.j, b.this.k, b.this.l, b.this.m, b.this.n, b.this.o, b.this.p, b.this.q, b.this.t);
                                }
                                i = 0;
                            }
                            i2 = i;
                            break;
                        } catch (UserRecoverableAuthIOException e) {
                            b.this.e.startActivityForResult(e.getIntent(), 3);
                            break;
                        } catch (IOException e2) {
                            b.this.a((b) "[DEBUG] Enter YM MSG_YOUTUBE_CREATE_EVENT", (String) 3);
                            b.this.a((b) message.what, -1);
                            e2.printStackTrace();
                            break;
                        } catch (InterruptedException e3) {
                            b.this.a((b) message.what, -1);
                            e3.printStackTrace();
                            break;
                        }
                    case InputDeviceCompat.SOURCE_TOUCHSCREEN /*{ENCODED_INT: 4098}*/:
                        try {
                            List d = b.d(b.this.f);
                            b.this.u = ((a) d.get(d.size() - 1)).a();
                            b.this.t = ((a) d.get(d.size() - 1)).b();
                            b.this.w = "https://www.youtube.com/watch?v=" + b.this.u;
                            b.this.a((b) ("[DEBUG] broadcastId: " + b.this.u), (String) 3);
                            b.this.a((b) ("[DEBUG] rtmpUrl: " + b.this.t), (String) 3);
                            b.this.a((b) ("[DEBUG] streaming link: " + b.this.w), (String) 3);
                            if (b.this.t != null) {
                                if (b.this.s) {
                                    ThirdParty.Rtmp.a.a().a(b.this.j, b.this.k, b.this.l, b.this.m, b.this.n, b.this.o, b.this.p, b.this.q, b.this.t);
                                }
                                i2 = 0;
                                break;
                            }
                        } catch (UserRecoverableAuthIOException e4) {
                            b.this.e.startActivityForResult(e4.getIntent(), 3);
                            break;
                        } catch (IOException e5) {
                            e5.printStackTrace();
                            break;
                        }
                        break;
                    case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*{ENCODED_INT: 4099}*/:
                        a.c.a aVar2 = new a.c.a(message);
                        b.this.a((b) ("[DEBUG] MSG_YOUTUBE_CREATE_360_EVENT " + aVar2.d("liveStreamTitle")), (String) 3);
                        i2 = b.this.a((b) aVar2, (a.c.a) true);
                        b.this.a((b) ("[DEBUG] broadcastId: " + b.this.u), (String) 3);
                        b.this.a((b) ("[DEBUG] rtmpUrl: " + b.this.t), (String) 3);
                        b.this.a((b) ("[DEBUG] streaming link: " + b.this.w), (String) 3);
                        if (i2 != 0) {
                            b.this.a((b) ("createLiveEvent res: " + i2), (String) 0);
                            break;
                        } else {
                            if (b.this.s) {
                                ThirdParty.Rtmp.a.a().a(b.this.j, b.this.k, b.this.l, b.this.m, b.this.n, b.this.o, b.this.p, b.this.q, b.this.t);
                            }
                            i2 = 0;
                            break;
                        }
                    case 4100:
                        if (b.this.i <= 20) {
                            if (!b.this.d()) {
                                b.q(b.this);
                                b.this.a(4100, 3000L);
                                i2 = 1;
                                break;
                            } else {
                                try {
                                    b.this.i = 0;
                                    b.d(b.this.f, b.this.u);
                                    i2 = 0;
                                    break;
                                } catch (UserRecoverableAuthIOException e6) {
                                    b.this.e.startActivityForResult(e6.getIntent(), 3);
                                    break;
                                } catch (IOException e7) {
                                    e7.printStackTrace();
                                    break;
                                }
                            }
                        } else {
                            b.this.i = 0;
                            break;
                        }
                    case 4101:
                        try {
                            b.e(b.this.f, b.this.u);
                            i2 = 0;
                            break;
                        } catch (UserRecoverableAuthIOException e8) {
                            b.this.e.startActivityForResult(e8.getIntent(), 3);
                            break;
                        } catch (IOException e9) {
                            e9.printStackTrace();
                            break;
                        }
                    case 4102:
                        try {
                            b.d(b.this.f);
                            b.this.d.a(true);
                            break;
                        } catch (UserRecoverableAuthIOException e10) {
                            b.this.d.a(false);
                            b.this.e.startActivityForResult(e10.getIntent(), 3);
                            break;
                        } catch (IOException e11) {
                            b.this.d.a(true);
                            e11.printStackTrace();
                            break;
                        }
                    case 4103:
                        b.this.a((b) b.this.f, (YouTube) b.this.u, (String) true);
                        break;
                    case 4104:
                        b.this.a((b) b.this.f, (YouTube) b.this.u, (String) false);
                        break;
                    case 4105:
                        if (b.this.i <= 20) {
                            if (!b.this.c()) {
                                b.q(b.this);
                                b.this.a(4105, 3000L);
                                i2 = 1;
                                break;
                            } else {
                                b.this.i = 0;
                                i2 = 0;
                                break;
                            }
                        } else {
                            b.this.i = 0;
                            break;
                        }
                }
                if (message.what != 4102) {
                    if (i2 != 1) {
                        b.this.a((b) message.what, i2);
                    }
                    if (i2 != 0 || message.what == 4101 || message.what == 4100) {
                        b.this.d.a((String) null, (String) null);
                    } else {
                        b.this.d.a(b.this.t, b.this.w);
                    }
                }
            }
        };
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, int i3) {
        if (this.d != null) {
            this.d.a(i2, i3);
        }
    }

    /* access modifiers changed from: private */
    public static List<a> c(YouTube youTube) {
        String boundStreamId;
        YouTube.LiveBroadcasts.List list = youTube.liveBroadcasts().list("id,snippet,contentDetails");
        list.setBroadcastStatus("upcoming");
        List<LiveBroadcast> items = ((LiveBroadcastListResponse) list.execute()).getItems();
        ArrayList arrayList = new ArrayList(items.size());
        for (LiveBroadcast liveBroadcast : items) {
            a aVar = new a();
            aVar.a(liveBroadcast);
            if (!(liveBroadcast.getContentDetails() == null || (boundStreamId = liveBroadcast.getContentDetails().getBoundStreamId()) == null)) {
                aVar.a(c(youTube, boundStreamId));
            }
            arrayList.add(aVar);
        }
        return arrayList;
    }

    private static String c(YouTube youTube, String str) {
        YouTube.LiveStreams.List list = youTube.liveStreams().list("cdn");
        list.setId(str);
        List<LiveStream> items = ((LiveStreamListResponse) list.execute()).getItems();
        if (items.isEmpty()) {
            return "";
        }
        IngestionInfo ingestionInfo = items.get(0).getCdn().getIngestionInfo();
        return ingestionInfo.getIngestionAddress() + "/" + ingestionInfo.getStreamName();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int a(a.c.a aVar, boolean z) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        long currentTimeMillis = System.currentTimeMillis() + HlsChunkSource.DEFAULT_MIN_BUFFER_TO_SWITCH_UP_MS;
        Date date = new Date();
        date.setTime(currentTimeMillis);
        simpleDateFormat.format(date);
        String date2 = new Date().toString();
        try {
            LiveBroadcastSnippet liveBroadcastSnippet = new LiveBroadcastSnippet();
            if (aVar.d("liveStreamTitle") == null) {
                liveBroadcastSnippet.setTitle("360 Live Stream - " + date2);
            } else {
                liveBroadcastSnippet.setTitle(aVar.d("liveStreamTitle"));
            }
            liveBroadcastSnippet.setDescription(aVar.d("liveStreamDescription"));
            liveBroadcastSnippet.setScheduledStartTime(new DateTime(date));
            LiveBroadcastContentDetails liveBroadcastContentDetails = new LiveBroadcastContentDetails();
            MonitorStreamInfo monitorStreamInfo = new MonitorStreamInfo();
            monitorStreamInfo.setEnableMonitorStream(false);
            liveBroadcastContentDetails.setMonitorStream(monitorStreamInfo);
            if (z) {
                liveBroadcastContentDetails.setProjection("360");
            }
            LiveBroadcastStatus liveBroadcastStatus = new LiveBroadcastStatus();
            liveBroadcastStatus.setPrivacyStatus("public");
            LiveBroadcast liveBroadcast = new LiveBroadcast();
            liveBroadcast.setKind("youtube#liveBroadcast");
            liveBroadcast.setSnippet(liveBroadcastSnippet);
            liveBroadcast.setStatus(liveBroadcastStatus);
            liveBroadcast.setContentDetails(liveBroadcastContentDetails);
            LiveBroadcast liveBroadcast2 = (LiveBroadcast) this.f.liveBroadcasts().insert("snippet,status,contentDetails", liveBroadcast).execute();
            this.u = liveBroadcast2.getId();
            this.w = "https://www.youtube.com/watch?v=" + this.u;
            a("================== Returned Broadcast ==================\n", 3);
            a("Id: " + liveBroadcast2.getId(), 3);
            a("Title: " + liveBroadcast2.getSnippet().getTitle(), 3);
            a("Description: " + liveBroadcast2.getSnippet().getDescription(), 3);
            a("Published At: " + liveBroadcast2.getSnippet().getPublishedAt(), 3);
            a("Scheduled Start Time: " + liveBroadcast2.getSnippet().getScheduledStartTime(), 3);
            a("Scheduled End Time: " + liveBroadcast2.getSnippet().getScheduledEndTime(), 3);
            a("Streaming Link: " + this.w, 3);
            LiveStreamSnippet liveStreamSnippet = new LiveStreamSnippet();
            if (aVar.d("liveStreamTitle") == null) {
                liveStreamSnippet.setTitle("360 Live Stream - " + date2);
            } else {
                liveStreamSnippet.setTitle(aVar.d("liveStreamTitle"));
            }
            liveStreamSnippet.setDescription(aVar.d("liveStreamDescription"));
            CdnSettings cdnSettings = new CdnSettings();
            cdnSettings.setFormat(aVar.d("streamFormat"));
            cdnSettings.setIngestionType("rtmp");
            LiveStream liveStream = new LiveStream();
            liveStream.setKind("youtube#liveStream");
            liveStream.setSnippet(liveStreamSnippet);
            liveStream.setCdn(cdnSettings);
            LiveStream liveStream2 = (LiveStream) this.f.liveStreams().insert("snippet,cdn", liveStream).execute();
            this.t = liveStream2.getCdn().getIngestionInfo().getIngestionAddress() + "/" + liveStream2.getCdn().getIngestionInfo().getStreamName();
            this.v = liveStream2.getId();
            a("================== Returned Stream ==================\n", 3);
            a("Id: " + liveStream2.getId(), 3);
            a("Title: " + liveStream2.getSnippet().getTitle(), 3);
            a("Description: " + liveStream2.getSnippet().getDescription(), 3);
            a("Published At: " + liveStream2.getSnippet().getPublishedAt(), 3);
            a("Rtmp URL: " + this.t, 3);
            YouTube.LiveBroadcasts.Bind bind = this.f.liveBroadcasts().bind(liveBroadcast2.getId(), "id,contentDetails");
            bind.setStreamId(liveStream2.getId());
            LiveBroadcast liveBroadcast3 = (LiveBroadcast) bind.execute();
            a("================== Returned Bind Broadcast ==================\n", 3);
            a("Id: " + liveBroadcast3.getId(), 3);
            a("Streaming Link: https://www.youtube.com/watch?v=" + liveBroadcast3.getId(), 3);
            a("=============================================================\n", 3);
            return 0;
        } catch (GoogleJsonResponseException e2) {
            System.err.println("GoogleJsonResponseException code: " + e2.getDetails().getCode() + " : " + e2.getDetails().getMessage() + " : " + e2.getDetails().getMessage().equals("The user is not enabled for live streaming."));
            e2.printStackTrace();
            if (e2.getDetails().getMessage().equals("The user is not enabled for live streaming.")) {
                return -5;
            }
            if (e2.getDetails().getMessage().equals("Title must be between 1 and 128 characters long") || e2.getDetails().getMessage().equals("Title is invalid")) {
                return -6;
            }
            if (e2.getDetails().getCode() >= 500) {
                return -3;
            }
            if (e2.getDetails().getCode() >= 400) {
                return -2;
            }
            return 0;
        } catch (IOException e3) {
            System.err.println("IOException: " + e3.getMessage());
            e3.printStackTrace();
            return -1;
        } catch (Throwable th) {
            System.err.println("Throwable: " + th.getStackTrace());
            th.printStackTrace();
            return -4;
        }
    }

    /* access modifiers changed from: private */
    public static List<a> d(YouTube youTube) {
        String boundStreamId;
        YouTube.LiveBroadcasts.List list = youTube.liveBroadcasts().list("id, snippet, contentDetails, status");
        list.setBroadcastType("persistent");
        list.setMine(true);
        List<LiveBroadcast> items = ((LiveBroadcastListResponse) list.execute()).getItems();
        ArrayList arrayList = new ArrayList(items.size());
        for (LiveBroadcast liveBroadcast : items) {
            a aVar = new a();
            aVar.a(liveBroadcast);
            if (!(liveBroadcast.getContentDetails() == null || (boundStreamId = liveBroadcast.getContentDetails().getBoundStreamId()) == null)) {
                aVar.a(c(youTube, boundStreamId));
            }
            arrayList.add(aVar);
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public static void d(YouTube youTube, String str) {
        youTube.liveBroadcasts().transition("live", str, NotificationCompat.CATEGORY_STATUS).execute();
    }

    /* access modifiers changed from: private */
    public static void e(YouTube youTube, String str) {
        youTube.liveBroadcasts().transition("complete", str, NotificationCompat.CATEGORY_STATUS).execute();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(YouTube youTube, String str, boolean z) {
        try {
            YouTube.LiveBroadcasts.Control control = youTube.liveBroadcasts().control(str, "id, snippet, contentDetails, status");
            if (z) {
                control.setDisplaySlate(true);
            } else {
                control.setDisplaySlate(false);
            }
            LiveBroadcast liveBroadcast = (LiveBroadcast) control.execute();
            a("LifeCycleStatus: " + liveBroadcast.getStatus().getLifeCycleStatus(), 3);
            a("Id: " + liveBroadcast.getId(), 3);
            a("getStartWithSlate: " + liveBroadcast.getContentDetails().getStartWithSlate(), 3);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean c() {
        try {
            LiveBroadcastListResponse liveBroadcastListResponse = (LiveBroadcastListResponse) this.f.liveBroadcasts().list(NotificationCompat.CATEGORY_STATUS).setId(this.u).execute();
            a("Broadcast status: " + liveBroadcastListResponse.getItems().get(0).getStatus().getLifeCycleStatus(), 3);
            if (liveBroadcastListResponse.getItems().get(0).getStatus().getLifeCycleStatus().equals("live")) {
                return true;
            }
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean d() {
        try {
            YouTube.LiveStreams.List list = this.f.liveStreams().list("id,snippet,status");
            list.setMine(true);
            LiveStreamStatus status = ((LiveStreamListResponse) list.execute()).getItems().get(0).getStatus();
            if (status.getStreamStatus().equals("active")) {
                a("stream status: " + status.getStreamStatus(), 3);
                return true;
            }
            a("stream status: " + status.getStreamStatus(), 3);
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public void a(a aVar) {
        this.d = aVar;
    }
}
