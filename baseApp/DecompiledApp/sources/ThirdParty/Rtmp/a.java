package ThirdParty.Rtmp;

import GeneralFunction.d;
import GeneralFunction.e;
import GeneralFunction.j;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import com.google.android.exoplayer.text.eia608.ClosedCaptionCtrl;
import java.util.Timer;
import java.util.TimerTask;

public class a {
    private static a b = null;
    private byte[] A = {1, 24, ClosedCaptionCtrl.RESUME_CAPTION_LOADING, 7};

    /* renamed from: a  reason: collision with root package name */
    public AbstractC0007a f156a = null;
    private HandlerThread c;
    private Handler d;
    private String e = null;
    private boolean f = false;
    private int g;
    private int h;
    private int i;
    private byte[] j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o = 0;
    private int p = 0;
    private int q = 0;
    private int r = 0;
    private long s = 0;
    private int t = 0;
    private int u = 0;
    private Timer v = null;
    private final j w = new j();
    private boolean x = false;
    private boolean y = false;
    private byte[] z = {-34, 2, 0, 76, 97, 118, 99, 53, 54, ClosedCaptionCtrl.ERASE_NON_DISPLAYED_MEMORY, 54, 48, ClosedCaptionCtrl.ERASE_NON_DISPLAYED_MEMORY, 49, 48, 48, 0, 2, 48, 64, 14};

    /* renamed from: ThirdParty.Rtmp.a$a  reason: collision with other inner class name */
    public interface AbstractC0007a {
        void a(int i, int i2);
    }

    static /* synthetic */ int n(a aVar) {
        int i2 = aVar.t;
        aVar.t = i2 - 1;
        return i2;
    }

    static /* synthetic */ int q(a aVar) {
        int i2 = aVar.p;
        aVar.p = i2 + 1;
        return i2;
    }

    static /* synthetic */ int r(a aVar) {
        int i2 = aVar.r;
        aVar.r = i2 + 1;
        return i2;
    }

    static /* synthetic */ int w(a aVar) {
        int i2 = aVar.o;
        aVar.o = i2 + 1;
        return i2;
    }

    /* access modifiers changed from: private */
    public static void b(String str, int i2) {
        d.a("RtmpStreamer", str, i2);
    }

    private a() {
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (b == null) {
                b = new a();
            }
            aVar = b;
        }
        return aVar;
    }

    public void a(int i2) {
        Message message = new Message();
        message.what = i2;
        a(message);
    }

    public void a(Message message) {
        int i2 = message.what;
        if (message.what == 8193) {
            if (this.t > 210) {
                b("[RTMP DEBUG] lPendingVideoMsg > lPendingVideoMsg:" + this.t, 0);
                this.y = true;
            }
            if (this.y) {
                if (e.b(((GeneralFunction.c.d) new a.c.a(message).f("streamVideoData").a()).a()) && this.t <= 150) {
                    b("[RTMP DEBUG] bIsDropFrameMsg = false", 0);
                    this.y = false;
                } else {
                    return;
                }
            }
            if (this.t > this.u) {
                this.u = this.t;
                b("MaxPending:" + this.u, 1);
            }
            this.w.a();
            this.t++;
            this.w.b();
        }
        if (message.what == 8195) {
            this.d.sendMessageAtFrontOfQueue(message);
        } else {
            this.d.sendMessage(message);
        }
    }

    public void a(int i2, int i3, int i4, byte[] bArr, int i5, int i6, int i7, int i8, String str) {
        this.g = i2;
        this.h = i3;
        this.i = i4;
        if (bArr != null) {
            this.j = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.j, 0, bArr.length);
        } else {
            this.j = null;
        }
        this.k = i5;
        this.l = i6;
        this.m = i7;
        this.n = i8;
        this.e = str;
        c();
    }

    private void c() {
        this.c = new HandlerThread("rtmpStreamer");
        this.c.start();
        this.d = new Handler(this.c.getLooper()) {
            /* class ThirdParty.Rtmp.a.AnonymousClass1 */

            public void handleMessage(Message message) {
                int i;
                int i2;
                int i3 = 0;
                super.handleMessage(message);
                if (!(message.what == 8192 || message.what == 8195)) {
                    a.this.v = new Timer();
                    a.this.v.scheduleAtFixedRate(new TimerTask() {
                        /* class ThirdParty.Rtmp.a.AnonymousClass1.AnonymousClass1 */

                        public void run() {
                            a.b("[RTMP DEBUG] Timer timeout...", 0);
                            if (a.this.v != null) {
                                a.this.v.cancel();
                            }
                            a.this.c.interrupt();
                            a.this.c.quit();
                            a.this.d();
                            a.this.d.removeCallbacksAndMessages(null);
                            a.this.a((a) 8193, -999);
                        }
                    }, 30000, 30000);
                }
                if (!(message.what == 8194 || message.what == 8193)) {
                    a.b("rtmpStreamerThread: 0x" + Integer.toHexString(message.what), 1);
                }
                switch (message.what) {
                    case 8192:
                        long currentTimeMillis = System.currentTimeMillis();
                        a.this.p = 0;
                        a.this.o = 0;
                        a.this.q = 0;
                        a.b("[TEST] init:" + a.this.g + " " + a.this.h + " " + a.this.i + " " + a.this.j + " " + a.this.k + " " + a.this.l + " " + a.this.m + " " + a.this.n, 0);
                        if (a.this.b(a.this.g, a.this.h, a.this.i, a.this.j, a.this.k, a.this.l, a.this.m, a.this.n)) {
                            i2 = 0;
                        } else {
                            i2 = -1;
                        }
                        a.b("rtmpInit spend: " + (System.currentTimeMillis() - currentTimeMillis), 0);
                        i3 = i2;
                        break;
                    case 8193:
                        a.this.w.a();
                        a.n(a.this);
                        a.this.w.b();
                        if (!a.this.f) {
                            i3 = -1;
                            break;
                        } else {
                            if (a.this.t >= 150) {
                                a.b("[RTMP DEBUG] lPendingVideoMsg >= 150", 3);
                                a.this.x = true;
                            }
                            a.c.a aVar = new a.c.a(message);
                            if (aVar.f("streamVideoData") == null) {
                                a.b("messageEx.getCustomData(STREAM_VIDEO_DATA_KEY) == null", 3);
                                i3 = -1;
                                break;
                            } else {
                                GeneralFunction.c.d dVar = (GeneralFunction.c.d) aVar.f("streamVideoData").a();
                                int i4 = dVar.f68a;
                                byte[] a2 = dVar.a();
                                int b = aVar.b("streamVideoPts");
                                a.q(a.this);
                                a.r(a.this);
                                if (a.this.x) {
                                    if (e.b(a2) && a.this.t <= 120) {
                                        a.b("[RTMP DEBUG] bIsDropFrame = false", 3);
                                        a.this.x = false;
                                    }
                                }
                                long currentTimeMillis2 = System.currentTimeMillis();
                                int encodeVideoFrame = RtmpFunc.encodeVideoFrame(a2, i4, b);
                                a.b("[TEST] encodeVideoFrame:" + a.this.p + " " + encodeVideoFrame + " " + b + " " + e.b(a2) + " " + i4, 4);
                                a.this.s = (System.currentTimeMillis() - currentTimeMillis2) + a.this.s;
                                if (a.this.r % 30 == 0) {
                                    a.b("Frame cost:" + a.this.r + "," + a.this.s + ",average:" + (a.this.s / ((long) a.this.r)), 0);
                                }
                                if (encodeVideoFrame < 0) {
                                    i3 = encodeVideoFrame;
                                    break;
                                }
                            }
                        }
                        break;
                    case 8194:
                        if (!a.this.f) {
                            i3 = -1;
                            break;
                        } else {
                            a.c.a aVar2 = new a.c.a(message);
                            if (aVar2.f("streamAudioData") != null) {
                                GeneralFunction.c.d dVar2 = (GeneralFunction.c.d) aVar2.f("streamAudioData").a();
                                int i5 = dVar2.f68a;
                                byte[] a3 = dVar2.a();
                                int b2 = aVar2.b("streamAudioPts");
                                a.w(a.this);
                                if (a.this.x) {
                                    i = 0;
                                } else {
                                    i = RtmpFunc.encodeAudioFrame(a3, i5, b2);
                                }
                                a.b("[TEST] encodeAudioFrame:" + a.this.o + " " + i + " " + b2, 4);
                                if (i >= 0) {
                                    i = 0;
                                }
                            } else {
                                a.b("messageEx.getCustomData(STREAM_AAC_DATA_KEY) == null", 3);
                                i = -1;
                            }
                            i3 = i;
                            break;
                        }
                    case 8195:
                        a.b("BARRY MSG_LIVE_STREAMING_RTMP_STREAMER_DESTORY", 0);
                        if (a.this.f) {
                            a.this.d();
                            break;
                        }
                        break;
                    default:
                        i3 = -1;
                        break;
                }
                a.this.a((a) message.what, i3);
                if (a.this.v != null) {
                    a.this.v.cancel();
                    a.this.v = null;
                }
            }
        };
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean b(int i2, int i3, int i4, byte[] bArr, int i5, int i6, int i7, int i8) {
        b("[TEST] rtmpInit:" + this.e, 0);
        b("RtmpFunc JNI ver. " + RtmpFunc.getVersion(), 3);
        this.f = RtmpFunc.init(this.e);
        return this.f;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void d() {
        b("BARRY rtmpDestory()", 3);
        RtmpFunc.shutdown();
        this.f = false;
        this.r = 0;
        this.s = 0;
        this.x = false;
        this.t = 0;
        if (this.v != null) {
            this.v.cancel();
            this.v = null;
        }
    }

    public void a(int i2, int i3, int i4, byte[] bArr, int i5, int i6, int i7, int i8) {
        this.g = i2;
        this.h = i3;
        this.i = i4;
        if (bArr != null) {
            this.j = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.j, 0, bArr.length);
        } else {
            this.j = null;
        }
        this.k = i5;
        this.l = i6;
        this.m = i7;
        this.n = i8;
    }

    public boolean b() {
        return this.f;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, int i3) {
        if (this.f156a != null) {
            this.f156a.a(i2, i3);
        } else {
            b("No register rtmpStreamerOnResultHandler!", 0);
        }
    }

    public void a(AbstractC0007a aVar) {
        this.f156a = aVar;
    }
}
