package androidx.versionedparcelable;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.RestrictTo;
import android.util.SparseIntArray;

@RestrictTo({RestrictTo.Scope.LIBRARY})
class b extends a {

    /* renamed from: a  reason: collision with root package name */
    private final SparseIntArray f735a;
    private final Parcel b;
    private final int c;
    private final int d;
    private final String e;
    private int f;
    private int g;

    b(Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "");
    }

    b(Parcel parcel, int i, int i2, String str) {
        this.f735a = new SparseIntArray();
        this.f = -1;
        this.g = 0;
        this.b = parcel;
        this.c = i;
        this.d = i2;
        this.g = this.c;
        this.e = str;
    }

    private int d(int i) {
        while (this.g < this.d) {
            this.b.setDataPosition(this.g);
            int readInt = this.b.readInt();
            int readInt2 = this.b.readInt();
            this.g = readInt + this.g;
            if (readInt2 == i) {
                return this.b.dataPosition();
            }
        }
        return -1;
    }

    @Override // androidx.versionedparcelable.a
    public boolean b(int i) {
        int d2 = d(i);
        if (d2 == -1) {
            return false;
        }
        this.b.setDataPosition(d2);
        return true;
    }

    @Override // androidx.versionedparcelable.a
    public void c(int i) {
        b();
        this.f = i;
        this.f735a.put(i, this.b.dataPosition());
        a(0);
        a(i);
    }

    @Override // androidx.versionedparcelable.a
    public void b() {
        if (this.f >= 0) {
            int i = this.f735a.get(this.f);
            int dataPosition = this.b.dataPosition();
            this.b.setDataPosition(i);
            this.b.writeInt(dataPosition - i);
            this.b.setDataPosition(dataPosition);
        }
    }

    /* access modifiers changed from: protected */
    @Override // androidx.versionedparcelable.a
    public a c() {
        return new b(this.b, this.b.dataPosition(), this.g == this.c ? this.d : this.g, this.e + "  ");
    }

    @Override // androidx.versionedparcelable.a
    public void a(byte[] bArr) {
        if (bArr != null) {
            this.b.writeInt(bArr.length);
            this.b.writeByteArray(bArr);
            return;
        }
        this.b.writeInt(-1);
    }

    @Override // androidx.versionedparcelable.a
    public void a(int i) {
        this.b.writeInt(i);
    }

    @Override // androidx.versionedparcelable.a
    public void a(String str) {
        this.b.writeString(str);
    }

    @Override // androidx.versionedparcelable.a
    public void a(Parcelable parcelable) {
        this.b.writeParcelable(parcelable, 0);
    }

    @Override // androidx.versionedparcelable.a
    public int d() {
        return this.b.readInt();
    }

    @Override // androidx.versionedparcelable.a
    public String e() {
        return this.b.readString();
    }

    @Override // androidx.versionedparcelable.a
    public byte[] f() {
        int readInt = this.b.readInt();
        if (readInt < 0) {
            return null;
        }
        byte[] bArr = new byte[readInt];
        this.b.readByteArray(bArr);
        return bArr;
    }

    @Override // androidx.versionedparcelable.a
    public <T extends Parcelable> T g() {
        return (T) this.b.readParcelable(getClass().getClassLoader());
    }
}
