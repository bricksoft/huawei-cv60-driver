package a.a.a;

import a.b.a.b;
import a.b.a.d;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static b f164a = null;
    private static Context b;
    private int c = 0;
    private int d = 0;
    private int e = 0;
    private boolean f = false;
    private boolean g = false;
    private int h = 0;
    private GeneralFunction.c.a i = null;
    private boolean j = false;
    private HandlerThread k = null;
    private Handler l = null;
    private AbstractC0009a m = null;
    private b.a n = new b.a() {
        /* class a.a.a.a.AnonymousClass1 */

        @Override // a.b.a.b.a
        public int a(int i, int i2) {
            switch (i) {
                case 0:
                    a.b("Event: Device Attached", 1);
                    a.this.m.a();
                    break;
                case 1:
                    a.b("Event: Device Detached", 1);
                    a.this.e = 0;
                    a.this.a((a) 0);
                    a.this.g();
                    a.this.m.b();
                    break;
                case 2:
                    a.b("Event: Permission " + i2, 1);
                    if (i2 != 0) {
                        a.this.d = 2;
                        break;
                    } else {
                        a.this.d = 3;
                        break;
                    }
            }
            return 0;
        }
    };

    /* renamed from: a.a.a.a$a  reason: collision with other inner class name */
    public interface AbstractC0009a {
        void a();

        void a(a.c.a aVar);

        void b();
    }

    public a(Context context) {
        b = context;
        f164a = new b(b);
    }

    public int a() {
        return this.c;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2) {
        this.c = i2;
        b("USBCoreStatus change to " + this.c, 1);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void g() {
        this.d = 0;
        this.h = 0;
        this.f = false;
        this.g = false;
    }

    public boolean b() {
        int i2 = f164a.i();
        b("CheckUSBPermission: " + i2, 1);
        if (i2 == 0) {
            return true;
        }
        return false;
    }

    public void c() {
        if (f164a != null) {
            f164a.e();
        }
    }

    public int a(AbstractC0009a aVar) {
        if (a() >= 3) {
            b("already init: " + a(), 1);
            return 0;
        } else if (a() != 0) {
            b("init fail, check status: " + a(), 1);
            return -1;
        } else {
            a(1);
            if (aVar == null) {
                b("Initialize with null callbacks", 0);
                a(0);
                return -1;
            }
            f164a.a(this.n);
            this.m = aVar;
            this.d = 1;
            if (f164a.j() != 0) {
                b("Init USB fail!", 0);
                a(0);
                return -1;
            }
            while (this.d == 1) {
                b(200);
            }
            b("Get Permission: " + this.d, 3);
            if (this.d != 3) {
                a(0);
                return -1;
            }
            a(2);
            if (f164a.h() != 0) {
                b("Find target if&eps fail!", 0);
                return -1;
            } else if (f164a.l() != 0) {
                b("Create connection fail!", 0);
                return -1;
            } else {
                if (this.i != null) {
                    this.i.b();
                    this.i = null;
                }
                if (this.k != null) {
                    h();
                }
                this.k = new HandlerThread("UsbCoreMainThread");
                this.k.start();
                this.l = a(this.k);
                a(3);
                b("init done " + System.currentTimeMillis(), 3);
                return 0;
            }
        }
    }

    public void d() {
        if (a() != 0) {
            f164a.k();
            h();
            g();
            a(0);
        }
    }

    public int e() {
        return f164a.n();
    }

    public int a(int i2, Bundle bundle) {
        if (a() < 3) {
            b("Connect before send command", 0);
            return -1;
        } else if (this.l == null) {
            b("UsbCoreMainHandler is null", 0);
            return -1;
        } else {
            Message message = new Message();
            message.what = i2;
            message.setData(bundle);
            if (this.e != 0) {
                if (this.e == 18443 && message.what != 18480) {
                    b("Send USB cmd failed because the communication is closing: " + message.what, 1);
                    return -1;
                } else if (this.e == i2 && i2 == 18464) {
                    b("Warning: skip duplicated get frame", 1);
                    return 5;
                }
            }
            this.e = i2;
            this.l.sendMessage(message);
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public static void b(String str, int i2) {
        d.a("USBCore", str, i2);
    }

    private void b(int i2) {
        try {
            Thread.sleep((long) i2);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private byte[] c(int i2) {
        return new byte[]{(byte) i2, (byte) (i2 >> 8), (byte) (i2 >> 16), (byte) (i2 >> 24)};
    }

    private void h() {
        if (this.k != null) {
            this.j = true;
            this.l = null;
            this.k.quit();
            while (this.k.isAlive()) {
                b(10);
            }
            this.k = null;
            this.j = false;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean a(b bVar) {
        if (bVar.b < 1 || bVar.b > 12) {
            b("[UsbCoreMsg] Invalid month setting: " + ((int) bVar.b), 0);
            return false;
        } else if (bVar.c < 1 || bVar.c > 31) {
            b("[UsbCoreMsg] Invalid day setting: " + ((int) bVar.c), 0);
            return false;
        } else if (bVar.d < 0 || bVar.d > 23) {
            b("[UsbCoreMsg] Invalid hour setting: " + ((int) bVar.d), 0);
            return false;
        } else if (bVar.e < 0 || bVar.e > 60) {
            b("[UsbCoreMsg] Invalid minute setting: " + ((int) bVar.e), 0);
            return false;
        } else if (bVar.f < 0 || bVar.f > 60) {
            b("[UsbCoreMsg] Invalid second setting: " + ((int) bVar.f), 0);
            return false;
        } else if (bVar.h >= 0 && bVar.h <= 999) {
            return true;
        } else {
            b("[UsbCoreMsg] Invalid millisecond setting: " + bVar.h, 0);
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int d(int i2) {
        if (i2 == -1) {
            return 0;
        }
        if (135 <= i2 && i2 < 225) {
            return 0;
        }
        if (225 <= i2 && i2 < 315) {
            return 1;
        }
        if (315 <= i2 || i2 < 45) {
            return 2;
        }
        if (45 <= i2 && i2 < 135) {
            return 3;
        }
        b("[UsbCoreMsg] Invalid orientation: " + i2, 0);
        return -1;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(d dVar, byte b2) {
        switch (b2) {
            case 9:
                dVar.c = 1920;
                dVar.d = 960;
                return;
            case 10:
                dVar.c = 1280;
                dVar.d = 640;
                return;
            case 11:
                dVar.c = 3840;
                dVar.d = 1920;
                return;
            default:
                b("[UsbCoreMsg] Unknown resolution: " + ((int) b2) + ", use default setting", 0);
                dVar.c = 1280;
                dVar.d = 640;
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int i() {
        return a(false);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int a(boolean z) {
        int i2;
        byte[] bArr = new byte[16];
        f164a.g();
        bArr[0] = 122;
        bArr[1] = 0;
        bArr[2] = 1;
        if (z) {
            i2 = 1;
        } else {
            if (f164a.n() != 0) {
                b("[UsbCoreMsg] Send reset fail, try restart connection", 0);
                if (f164a.m() != 0) {
                    b("[UsbCoreMsg] restart connection fail", 0);
                    return -1;
                }
            }
            bArr[8] = 0;
            i2 = 1;
        }
        while (true) {
            if (i2 == 0) {
                break;
            }
            int a2 = f164a.a(bArr);
            if (a2 >= 0) {
                byte a3 = f164a.a(0);
                if (a3 == 1) {
                    i2++;
                } else if (a3 == 0) {
                    f164a.c();
                    a(4);
                } else {
                    b("[UsbCoreMsg] open communication fail: " + ((int) a3), 0);
                    return -1;
                }
            } else if (a() == 3) {
                b("[UsbCoreMsg] Send open communication fail: " + a2 + " . Try no." + i2, 0);
                b("[UsbCoreMsg] Send reset: " + f164a.n(), 0);
                i2++;
            } else {
                b("[UsbCoreMsg] usb not connected, CoreStatus: " + a(), 0);
                return -1;
            }
            if (i2 >= 200) {
                return -1;
            }
            b(100);
        }
        return 0;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean a(byte b2) {
        if (b2 != -1) {
            return false;
        }
        b("Camera go into power save mode. Send open communication first", 0);
        if (i() != 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int b(byte b2) {
        switch (b2) {
            case -1:
                b("Camera go into power save mode. Send open communication first", 0);
                if (i() != 0) {
                    return -1;
                }
                return 3;
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return -1;
            default:
                return -1;
        }
    }

    private Handler a(HandlerThread handlerThread) {
        return new Handler(handlerThread.getLooper()) {
            /* class a.a.a.a.AnonymousClass2 */

            /* JADX WARNING: Code restructure failed: missing block: B:391:0x1164, code lost:
                if (a.a.a.a.f164a.a(r6) != -1) goto L_0x1166;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:398:0x11bd, code lost:
                if (a.a.a.a.f164a.a(r6) != -1) goto L_0x11bf;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:405:0x1215, code lost:
                if (a.a.a.a.f164a.a(r6) != -1) goto L_0x1217;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:412:0x126e, code lost:
                if (a.a.a.a.f164a.a(r6) != -1) goto L_0x1270;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:419:0x12c8, code lost:
                if (a.a.a.a.f164a.a(r6) != -1) goto L_0x12ca;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:426:0x1322, code lost:
                if (a.a.a.a.f164a.a(r6) != -1) goto L_0x1324;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:446:0x1470, code lost:
                if (a.a.a.a.f164a.a(r6) != -1) goto L_0x1472;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:453:0x14ca, code lost:
                if (a.a.a.a.f164a.a(r6) != -1) goto L_0x14cc;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:460:0x1524, code lost:
                if (a.a.a.a.f164a.a(r6) != -1) goto L_0x1526;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void handleMessage(android.os.Message r15) {
                /*
                // Method dump skipped, instructions count: 8636
                */
                throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.AnonymousClass2.handleMessage(android.os.Message):void");
            }
        };
    }
}
