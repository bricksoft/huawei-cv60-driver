package a.c;

import android.os.Bundle;
import android.os.Message;
import android.os.Parcelable;
import java.util.ArrayList;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private int f182a = -1;
    private Message b = null;
    private Bundle c = null;

    public a(int i) {
        this.f182a = i;
        this.b = new Message();
        this.b.what = i;
        this.c = new Bundle();
    }

    public a(Message message) {
        this.f182a = message.what;
        this.b = new Message();
        this.b.what = message.what;
        this.b.obj = message.obj;
        this.c = message.getData();
    }

    public int a() {
        return this.f182a;
    }

    public Message b() {
        this.b.setData(this.c);
        return this.b;
    }

    public void a(Object obj) {
        this.b.obj = obj;
    }

    public Object c() {
        return this.b.obj;
    }

    public void a(String str, boolean z) {
        this.c.putBoolean(str, z);
    }

    public boolean a(String str) {
        return this.c.getBoolean(str);
    }

    public void a(String str, int i) {
        this.c.putInt(str, i);
    }

    public int b(String str) {
        return this.c.getInt(str);
    }

    public void a(String str, long j) {
        this.c.putLong(str, j);
    }

    public long c(String str) {
        return this.c.getLong(str);
    }

    public void a(String str, String str2) {
        this.c.putString(str, str2);
    }

    public String d(String str) {
        return this.c.getString(str);
    }

    public void a(String str, byte[] bArr) {
        this.c.putByteArray(str, bArr);
    }

    public byte[] e(String str) {
        return this.c.getByteArray(str);
    }

    public void a(String str, C0010a aVar) {
        ArrayList<? extends Parcelable> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(aVar);
        arrayList.add(arrayList2);
        this.c.putParcelableArrayList(str, arrayList);
    }

    public C0010a f(String str) {
        ArrayList parcelableArrayList = this.c.getParcelableArrayList(str);
        if (parcelableArrayList == null || parcelableArrayList.get(0) == null) {
            return null;
        }
        return (C0010a) ((ArrayList) parcelableArrayList.get(0)).get(0);
    }

    /* renamed from: a.c.a$a  reason: collision with other inner class name */
    public static class C0010a<T> {

        /* renamed from: a  reason: collision with root package name */
        private T f183a;

        public C0010a(T t) {
            this.f183a = t;
        }

        public T a() {
            return this.f183a;
        }
    }
}
