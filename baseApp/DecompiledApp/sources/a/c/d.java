package a.c;

import a.a.a.a;
import a.c.a;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import java.util.concurrent.ArrayBlockingQueue;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private Context f185a = null;
    private HandlerThread b = null;
    private Handler c = null;
    private HandlerThread d = null;
    private Handler e = null;
    private c f = null;
    private a g = null;
    private a.a.a.a h = null;
    private int i = 0;
    private final int j = 1000;
    private a.AbstractC0009a k = new a.AbstractC0009a() {
        /* class a.c.d.AnonymousClass3 */

        @Override // a.a.a.a.AbstractC0009a
        public void a(a aVar) {
            int i;
            switch (aVar.a()) {
                case 18464:
                    if (d.this.h.a() == 6 || d.this.h.a() == 8) {
                        if (aVar.b("result") == 0) {
                            a aVar2 = new a(18176);
                            a.a.a.d dVar = (a.a.a.d) aVar.f("LiveViewFrame").a();
                            int i2 = dVar.f169a.f68a;
                            byte[] bArr = new byte[i2];
                            System.arraycopy(dVar.f169a.a(), 0, bArr, 0, i2);
                            dVar.f169a.d();
                            GeneralFunction.c.d dVar2 = new GeneralFunction.c.d(bArr);
                            dVar2.b = dVar.f169a.b;
                            dVar2.c = dVar.f169a.c;
                            dVar.f169a = dVar2;
                            aVar2.a("LiveViewFrame", new a.C0010a(dVar));
                            d.this.b((d) aVar2);
                            d.this.i = 0;
                            i = 0;
                        } else if (aVar.b("result") == 1) {
                            i = 5;
                            d.c(d.this);
                            if (d.this.i >= 1000) {
                                d.this.a((d) "Get live view frame busy over 1000 times", (String) 0);
                                d.this.i = 0;
                            }
                        } else {
                            i = 0;
                        }
                        d.this.a(new a(18464), (long) i);
                        break;
                    } else {
                        d.this.a((d) "Enable live view first", (String) 0);
                        return;
                    }
            }
            d.this.b((d) aVar);
        }

        @Override // a.a.a.a.AbstractC0009a
        public void a() {
            d.this.a((d) "Notify: USB device Attached", (String) 1);
            d.this.b((d) new a(18436));
        }

        @Override // a.a.a.a.AbstractC0009a
        public void b() {
            d.this.a((d) "Notify: USB device Detached", (String) 1);
            d.this.f.c = 0;
            d.this.b((d) new a(18435));
        }
    };

    public interface a {
        void a(a aVar);
    }

    static /* synthetic */ int c(d dVar) {
        int i2 = dVar.i;
        dVar.i = i2 + 1;
        return i2;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        a.a.a("UI_RemoteControl", str, i2);
    }

    public d(Context context, a aVar) {
        this.f185a = context;
        this.g = aVar;
    }

    public void a() {
        this.f = new c();
        this.f.d = 0;
        this.f.e = new ArrayBlockingQueue(32);
        this.f.f = new ArrayBlockingQueue(32);
        this.f.f184a = 0;
        this.f.b = 0;
        this.f.c = 0;
        this.f.g = false;
        this.b = new HandlerThread("RemoteControlServer");
        this.b.start();
        this.c = b(this.b);
        this.d = new HandlerThread("RemoteInterruptServer");
        this.d.start();
        this.e = a(this.d);
        a("Library Version : 0.0D", 2);
        a("android.permission.ACCESS_COARSE_LOCATION");
        a("android.permission.ACCESS_FINE_LOCATION");
        a("android.permission.WRITE_EXTERNAL_STORAGE");
    }

    public void a(a aVar) {
        a(aVar, 0);
    }

    public void a(a aVar, long j2) {
        a(aVar.b(), j2);
    }

    public void a(Message message, long j2) {
        if (b(message)) {
            this.e.sendMessageDelayed(message, j2);
            return;
        }
        this.f.d++;
        this.c.sendMessageDelayed(message, j2);
    }

    private Handler a(HandlerThread handlerThread) {
        return new Handler(handlerThread.getLooper()) {
            /* class a.c.d.AnonymousClass1 */

            public void handleMessage(Message message) {
                super.handleMessage(message);
                d.this.a((d) ("Remote Interrupt handleMessage: 0x" + Integer.toHexString(message.what)), (String) 3);
                d.this.a((d) message);
            }
        };
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean a(Message message) {
        return true;
    }

    private Handler b(HandlerThread handlerThread) {
        return new Handler(handlerThread.getLooper()) {
            /* class a.c.d.AnonymousClass2 */

            public void handleMessage(Message message) {
                super.handleMessage(message);
                if (message.what != 18464) {
                    d.this.a((d) ("Remote handleMessage: 0x" + Integer.toHexString(message.what)), (String) 3);
                }
                switch (message.what & MotionEventCompat.ACTION_POINTER_INDEX_MASK) {
                    case 18432:
                        d.this.c((d) message);
                        break;
                    default:
                        d.this.a((d) ("Remote not handle message!! ID:" + message.what), (String) 0);
                        break;
                }
                c cVar = d.this.f;
                cVar.d--;
            }
        };
    }

    private boolean b(Message message) {
        switch (message.what) {
            case 16644:
            case 17414:
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void b(a aVar) {
        this.g.a(aVar);
    }

    private boolean a(String str) {
        if (ContextCompat.checkSelfPermission(this.f185a, str) != 0) {
            a("Permission Not Granted : " + str, 2);
            return false;
        }
        a("Permission Granted : " + str, 4);
        return true;
    }

    public boolean b() {
        if (this.f.c >= 1) {
            return true;
        }
        return false;
    }

    public boolean c() {
        if (this.h.a() >= 4) {
            return true;
        }
        return false;
    }

    public boolean d() {
        if (this.h != null && this.h.a() >= 6) {
            return true;
        }
        return false;
    }

    public void e() {
        if (this.h != null) {
            this.h.c();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean c(Message message) {
        a aVar = new a(message);
        switch (message.what) {
            case 18432:
                if (this.h == null) {
                    this.h = new a.a.a.a(this.f185a);
                }
                int a2 = this.h.a(this.k);
                if (a2 == 0) {
                    this.f.c = 1;
                    a(new a(18442));
                }
                aVar.a("result", a2);
                b(aVar);
                break;
            case 18433:
                this.f.c = 0;
                this.h.d();
                aVar.a("result", 0);
                b(aVar);
                break;
            case 18434:
                aVar.a("result", this.h.e());
                b(aVar);
                break;
            case 18437:
                if (this.h == null) {
                    this.h = new a.a.a.a(this.f185a);
                }
                aVar.a("usb_permission", this.h.b());
                b(aVar);
                break;
            case 18442:
            case 18443:
            case 18444:
            case 18445:
            case 18448:
            case 18449:
            case 18450:
            case 18451:
            case 18452:
            case 18453:
            case 18455:
            case 18456:
            case 18457:
            case 18458:
            case 18459:
            case 18464:
            case 18465:
            case 18466:
            case 18480:
            case 18481:
            case 18482:
            case 18483:
            case 18484:
            case 18496:
            case 18497:
            case 18498:
            case 18499:
            case 18500:
            case 18501:
            case 18503:
            case 18504:
            case 18505:
            case 18506:
            case 18512:
            case 18513:
            case 18514:
            case 18515:
            case 18516:
            case 18517:
            case 18518:
            case 18519:
            case 18520:
            case 18521:
            case 18522:
            case 18523:
            case 18528:
            case 18544:
            case 18545:
            case 18546:
            case 18547:
            case 18548:
            case 18549:
            case 18551:
            case 18552:
            case 18553:
            case 18672:
            case 18673:
            case 18674:
            case 18675:
                if (this.h != null) {
                    if (this.h.a() >= 3) {
                        if (message.what != 18449 || this.h.a() < 5) {
                            if (message.what != 18464 || this.h.a() == 6 || this.h.a() == 8) {
                                if (message.what != 18453 || this.h.a() < 7) {
                                    if (message.what == 18451 && this.h.a() >= 7) {
                                        a("Ignore this record cmd. Status: " + this.h.a(), 0);
                                        break;
                                    } else {
                                        int a3 = this.h.a(message.what, message.getData());
                                        if (a3 != 1) {
                                            if (a3 == 5) {
                                                a("Skip USB Msg: " + message.what, 0);
                                                break;
                                            }
                                        } else if (message.what != 18444) {
                                            Message message2 = new Message();
                                            message2.what = message.what;
                                            message2.setData(message.getData());
                                            a(message2, 5);
                                            break;
                                        }
                                    }
                                } else {
                                    a("Ignore this capture cmd. CoreStatus: " + this.h.a(), 0);
                                    break;
                                }
                            } else {
                                a("Enable live view first. CoreStatus: " + this.h.a(), 0);
                                break;
                            }
                        } else {
                            a("Live view already start, send get frame. CoreStatus: " + this.h.a(), 0);
                            a(new a(18464));
                            break;
                        }
                    } else {
                        a("USB core status is not connected. send cmd fail", 0);
                        break;
                    }
                } else {
                    a("Receive msg but objUsbCore is null!!" + message.what, 0);
                    break;
                }
                break;
            default:
                a("Unknown Remote USB Msg: " + message.what, 0);
                return false;
        }
        return true;
    }
}
