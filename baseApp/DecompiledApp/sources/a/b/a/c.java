// Started to reverse engine file 04/15/21

package a.b.a;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import com.google.android.exoplayer.text.eia608.ClosedCaptionCtrl;
import java.util.Timer;
import java.util.TimerTask;

// c --> SCSI notify --> serial debug info ??
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f174a = {122, 3, -1};
    private b b = null;
    private Timer c = null;
    private Handler d = null;
    private HandlerThread e = null;
    private boolean f = false;
    private int g = 0;
    private int h = 0;
    private int i = 0;
    private Timer j = null;
    private Handler k = null;
    private HandlerThread l = null;
    private boolean m = false;

    static /* synthetic */ int h(c cVar) {
        int i2 = cVar.h;
        cVar.h = i2 + 1;
        return i2;
    }

    public c(b bVar) {
        this.b = bVar;
    }

    public void a(int i2, int i3) {
        if (i2 == 0) {
            b("Disable keep alive mechanism", 3);
            return;
        }
        if (this.e != null) {
            b("Warning! keep alive thread already exist. Would restart it", 0);
            h();
        }
        if (this.c != null) {
            i();
        }
        this.e = new HandlerThread("SCSIKeepAliveThread");
        this.e.start();
        this.d = a(this.e);
        this.c = new Timer();
        AnonymousClass1 r1 = new TimerTask() {
            /* class a.b.a.c.AnonymousClass1 */

            public void run() {
                Message message = new Message();
                message.what = 0;
                if (c.this.h > c.this.g) {
                    message.what = 2;
                }
                if (c.this.f) {
                    message.what = 1;
                    c.this.d.sendMessage(message);
                }
                c.this.f = true;
            }
        };
        this.i = i2;
        this.g = i3 / this.i;
        this.f = true;
        this.c.scheduleAtFixedRate(r1, (long) this.i, (long) this.i);
    }

    public void a() {
        i();
        h();
        this.f = false;
        this.g = 0;
        this.h = 0;
    }

    public void b() {
        this.f = false;
    }

    public void c() {
        if (this.c != null && this.i != 0) {
            i();
            this.c = new Timer();
            this.c.scheduleAtFixedRate(new TimerTask() {
                /* class a.b.a.c.AnonymousClass2 */

                public void run() {
                    Message message = new Message();
                    message.what = 0;
                    if (c.this.h > c.this.g) {
                        message.what = 2;
                    }
                    if (c.this.f) {
                        message.what = 1;
                        c.this.d.sendMessage(message);
                    }
                    c.this.f = true;
                }
            }, (long) this.i, (long) this.i);
        }
    }

    public void d() {
        if (this.l != null) {
            b("Warning! notify connected thread already exist. Would restart it", 0);
            j();
        }
        if (this.j != null) {
            k();
        }
        this.m = true;
        this.l = new HandlerThread("SCSINotifyAPPConnectedThread");
        this.l.start();
        this.k = b(this.l);
        this.j = new Timer();
        this.j.scheduleAtFixedRate(new TimerTask() {
            /* class a.b.a.c.AnonymousClass3 */

            public void run() {
                if (c.this.m) {
                    Message message = new Message();
                    message.what = 0;
                    c.this.k.sendMessage(message);
                }
            }
        }, 150000, 150000);
    }

    public void e() {
        this.m = false;
        k();
        j();
    }

    public void f() {
        if (this.j != null) {
            k();
            this.j = new Timer();
            this.j.scheduleAtFixedRate(new TimerTask() {
                /* class a.b.a.c.AnonymousClass4 */

                public void run() {
                    Message message = new Message();
                    message.what = 0;
                    c.this.k.sendMessage(message);
                }
            }, 150000, 150000);
        }
    }

    /* access modifiers changed from: private */
    public static void b(String str, int i2) {
        d.a("SCSIAlive", str, i2);
    }

    private Handler a(HandlerThread handlerThread) {
        return new Handler(handlerThread.getLooper()) {
            /* class a.b.a.c.AnonymousClass5 */

            public void handleMessage(Message message) {
                super.handleMessage(message);
                switch (message.what) {
                    case 1:
                        int a2 = c.this.b.a(c.f174a, true);
                        if (a2 == -1 || a2 == -3) {
                            c.h(c.this);
                            c.b("Send keep alive fail: " + c.this.h, 0);
                            return;
                        }
                        c.this.h = 0;
                        return;
                    case 2:
                        c.b("Keep alive timeout!!", 0);
                        c.this.i();
                        return;
                    default:
                        c.b("Unknown keep alive status: " + message.what, 0);
                        return;
                }
            }
        };
    }

    private void h() {
        if (this.e != null) {
            this.d = null;
            this.e.quit();
            while (this.e.isAlive()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            this.e = null;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void i() {
        if (this.c != null) {
            this.c.cancel();
            this.c = null;
        }
    }

    private Handler b(HandlerThread handlerThread) {
        if (handlerThread == null) {
            return null;
        }
        return new Handler(handlerThread.getLooper()) {
            /* class a.b.a.c.AnonymousClass6 */

            public void handleMessage(Message message) {
                super.handleMessage(message);
                switch (message.what) {
                    case 0:
                        byte[] bArr = new byte[16];
                        bArr[0] = 122;
                        bArr[1] = 0;
                        bArr[2] = 2;
                        bArr[8] = 1;
                        bArr[9] = 1;
                        bArr[12] = ClosedCaptionCtrl.ERASE_DISPLAYED_MEMORY;
                        bArr[13] = 1;
                        bArr[14] = 0;
                        bArr[15] = 0;
                        c.b("Send APP connected notify", 1);
                        int a2 = c.this.b.a(bArr, true);
                        if (a2 == -1 || a2 == -3) {
                            c.b("Send notify APP connected fail", 0);
                            return;
                        }
                        return;
                    default:
                        c.b("Unknown notify status: " + message.what, 0);
                        return;
                }
            }
        };
    }

    private void j() {
        if (this.l != null) {
            this.k = null;
            this.l.quit();
            while (this.l.isAlive()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            this.l = null;
        }
    }

    private void k() {
        if (this.j != null) {
            this.j.cancel();
            this.j = null;
        }
    }
}
