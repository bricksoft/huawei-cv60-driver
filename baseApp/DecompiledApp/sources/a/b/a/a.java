// Started to reverse engine file 04/15/21

package a.b.a;

import java.nio.ByteBuffer;
import java.util.Random;

// Seems to be a Bytebuffer
public class a {

    /* renamed from: a  reason: collision with root package name */
    private int f171a;
    private int b;
    private int c;
    private byte d;
    private byte e;
    private byte f;
    private byte[] g;

    public a(int i, byte b2) {
        this.f171a = 1431519811;
        this.b = b();
        this.c = i;
        this.d = b2;
        this.e = 0;
        this.f = 16;
    }

    public a(int i, byte b2, int i2) {
        this.f171a = 1431519811;
        this.b = i2;
        this.c = i;
        this.d = b2;
        this.e = 0;
        this.f = 16;
    }

    public void a(byte[] bArr) {
        this.g = bArr;
    }

    public byte[] a() {
        ByteBuffer allocate = ByteBuffer.allocate(31);
        allocate.putInt(this.f171a);
        allocate.putInt(this.b);
        allocate.putInt(this.c);
        allocate.put(this.d);
        allocate.put(this.e);
        allocate.put(this.f);
        allocate.put(this.g);
        return allocate.array();
    }

    private int b() {
        return new Random().nextInt();
    }
}
