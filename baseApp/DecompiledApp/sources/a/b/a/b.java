// Started to reverse engine file 04/14/21
package a.b.a;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import com.google.android.exoplayer.ExoPlayer;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class b {
    private static Context t;

    // c --> SCSI notify --> serial debug info ??
    private static c u = null;

    /* renamed from: a  reason: collision with root package name */
    private int f172a = 0;
    // Camera USB productID
    private int b = 4251;
    // Camera USB vendorID
    private int c = 4817;
    private int d = 65536;
    private int e = 500;
    private int f = ExoPlayer.Factory.DEFAULT_MIN_REBUFFER_MS;
    private UsbManager g = null;

    // The camera as USB device
    private UsbDevice h = null;
    private UsbInterface i = null;
    private UsbEndpoint j = null;
    private int k = 0;
    private UsbEndpoint l = null;
    private int m = 0;
    private UsbDeviceConnection n = null;
    private byte[] o = null;
    private byte[] p = null;
    private PendingIntent q = null;
    private a r = null;
    private int s = -1;
    private Semaphore v = new Semaphore(1, true);
    private final BroadcastReceiver w = new BroadcastReceiver() {
        /* class a.b.a.b.AnonymousClass1 */

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void onReceive(Context context, Intent intent) {
            // z --> can use camera ? 
            boolean z;
            String action = intent.getAction();
            switch (action.hashCode()) {
                case -2114103349:
                    if (action.equals("android.hardware.usb.action.USB_DEVICE_ATTACHED")) {
                        z = false;
                        break;
                    }
                    z = true;
                    break;
                case -1608292967:
                    if (action.equals("android.hardware.usb.action.USB_DEVICE_DETACHED")) {
                        z = true;
                        break;
                    }
                    z = true;
                    break;
                case 1609010426:
                    if (action.equals("com.android.example.USB_PERMISSION")) {
                        z = true;
                        break;
                    }
                    z = true;
                    break;
                default:
                    z = true;
                    break;
            }
            switch (z) {
                case false:
                    b.b("Device Attached", 1);
                    b.this.r.a(0, -2);
                    return;
                case true:
                    b.b("Device Detached", 1);
                    b.this.r.a(1, -2);
                    b.this.d();
                    b.this.g();
                    b.this.q();
                    b.this.f172a = 0;
                    return;
                case true:
                    if (intent.getBooleanExtra("permission", false)) {
                        b.b("Permission accept!", 3);
                        if (b.this.f172a < 2) {
                            b.this.f172a = 2;
                        }
                        b.this.r.a(2, 0);
                        return;
                    }
                    b.b("Permission denied!", 0);
                    if (b.this.f172a < 2) {
                        b.this.f172a = 0;
                    }
                    b.this.r.a(2, -1);
                    return;
                default:
                    b.b("Unknown action " + action, 1);
                    return;
            }
        }
    };

    public interface a {
        int a(int i, int i2);
    }

    // Set Context
    public b(Context context) {
        t = context;
    }

    // Get f172a
    public int a() {
        return this.f172a;
    }

    // Get d
    public int b() {
        return this.d;
    }


    public void c() {
        if (u != null) {
            u.a(this.e, this.f);
        }
    }

    public void d() {
        if (u != null) {
            u.a();
        }
    }

    public void e() {
        if (u != null) {
            u.c();
            u.f();
        }
    }

    public void f() {
        if (u != null) {
            u.d();
        }
    }

    public void g() {
        if (u != null) {
            u.e();
        }
    }

    // Sensors of the camera are interfaces ??? 
    // TODO: need to check android.hardware.usb.UsbInterface;
    public int h() {
        if (this.h == null) {
            b("No target device", 0);
            return -1;
        }
        if (this.i != null) {
            b("Replace original interface", 1);
        }
        for (int i2 = 0; i2 < this.h.getInterfaceCount(); i2++) {
            int i3 = 0;
            boolean z = false;
            boolean z2 = false;
            while (true) {
                if (i3 >= this.h.getInterface(i2).getEndpointCount()) {
                    break;
                }
                UsbEndpoint endpoint = this.h.getInterface(i2).getEndpoint(i3);
                if (endpoint.getType() == 2) {
                    if (endpoint.getDirection() == 128) {
                        this.j = endpoint;
                        this.k = 16384;
                        z2 = true;
                    } else if (endpoint.getDirection() == 0) {
                        this.l = endpoint;
                        this.m = 16384;
                        z = true;
                    }
                }
                if (z2 && z) {
                    this.i = this.h.getInterface(i2);
                    break;
                }
                i3++;
            }
        }
        if (this.i != null) {
            return 0;
        }
        b("Could not find appropriate interface. Device: " + this.h.getDeviceName() + " PID: " + this.h.getProductId() + " VID: " + this.h.getVendorId(), 0);
        return -1;
    }

    public void a(a aVar) {
        this.r = aVar;
    }

    // Check if the camera is plugged in (return 0), 
    //else return -4 if permission denied for using usb, 
    //else return -1 if usb is not present or unavailable
    public int i() {
        // looking for USB controller/manager
        if (this.g == null) {
            this.g = (UsbManager) t.getSystemService("usb");
            if (this.g == null) {
                b("Unable to initialize USB manager", 0);
                return -1;
            }
        }

        // Loop through the device list to find the camera 
        Iterator<UsbDevice> it = this.g.getDeviceList().values().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            UsbDevice next = it.next();
            a(next);
            if (next.getVendorId() == this.c && next.getProductId() == this.b) {
                this.h = next;
                break;
            }
        }

        // If the camera is finded, try to get permission
        if (this.h == null) {
            b("No appropriate USB device", 0);
            this.g = null;
            return -1;
        } else if (this.g.hasPermission(this.h)) {
            return 0;
        } else {
            b("No permission", 1);
            return -4;
        }
    }

    // Init the USB device (camera) (needed before connection ???)
    public int j() {
        o();
        if (a() != 0) {
            b("Init USB fail, check status: " + a(), 1);
            p();
            return -1;
        }
        r();
        // Create a logger ??
        u = new c(this);
        int i2 = i();
        if (i2 == -1) {
            p();
            return -1;
        }
        this.o = new byte[this.d];
        this.p = new byte[64];
        if (i2 == -4) {
            b("Need to ask for permission", 1);
            this.f172a = 1;
            this.g.requestPermission(this.h, this.q);
        } else {
            b("Already has permission", 1);
            this.f172a = 2;
            this.r.a(2, 0);
        }
        p();
        return 0;
    }

    // Release the usb device (camera)
    public int k() {
        o();
        if (a() >= 3) {
            if (!this.n.releaseInterface(this.i)) {
                b("deinit fail: could not release interface", 0);
                p();
                return -1;
            }
            this.n.close();
        }
        d();
        g();
        this.f172a = 0;
        p();
        q();
        return 0;
    }

    // Open the usb device (camera)
    public int l() {
        o();
        if (a() < 2) {
            b("Should do initialize before create connection", 0);
            p();
            return -1;
        } else if (this.h == null || this.i == null) {
            b("No target device or interface", 0);
            p();
            return -1;
        } else {
            this.n = this.g.openDevice(this.h);
            if (this.n == null) {
                b("Open device fail", 0);
                p();
                return -1;
            } else if (!this.n.claimInterface(this.i, true)) {
                b("Claim interface fail", 0);
                p();
                return -1;
            } else {
                this.f172a = 3;
                p();
                return 0;
            }
        }
    }

    // try to restart USB connection multiple time
    public int m() {
        o();
        b("Restart USB connection!", 1);
        if (this.n == null || this.h == null || this.i == null) {
            b("targetConnection or targetDevice or targetInterface is null, restart fail", 0);
            p();
            return -1;
        }
        this.n.close();
        try {
            Thread.sleep(20);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        this.n = this.g.openDevice(this.h);
        if (this.n == null) {
            b("Open device fail", 0);
            p();
            return -1;
        } else if (!this.n.claimInterface(this.i, true)) {
            b("Claim interface fail", 0);
            p();
            return -1;
        } else {
            p();
            return 0;
        }
    }

    public int a(byte[] bArr) {
        return a(bArr, false, 2000);
    }

    public int a(byte[] bArr, int i2) {
        return a(bArr, false, i2);
    }

    public int a(byte[] bArr, boolean z) {
        return a(bArr, z, 2000);
    }

    public int a(byte[] bArr, boolean z, int i2) {
        byte[] bArr2;
        int i3;
        int i4;
        byte[] bArr3 = new byte[16384];
        int i5 = 0;
        if (z) {
            bArr2 = this.p;
            i3 = 64;
        } else {
            bArr2 = this.o;
            i3 = this.d;
        }
        o();
        if (a() != 3) {
            b("Connected first", 0);
            p();
            return -1;
        }
        int s2 = s();
        a aVar = new a(Integer.reverseBytes(i3), Byte.MIN_VALUE, s2);
        aVar.a(bArr);
        byte[] a2 = aVar.a();
        if (this.s != -1) {
            b("[DBG] Read Cmd:\n" + b(a2, 31), this.s);
        }
        int a3 = a(this.l, a2, 31, i2);
        if (a3 < 0) {
            b("Read cmd send Fail: " + a3 + ". Cmd: " + b(bArr, 3), 0);
            p();
            return -3;
        }
        while (true) {
            int a4 = a(this.j, bArr3, 16384, i2);
            if (a4 < 0) {
                b("Recv read data fail: " + a4 + ". Cmd: " + b(bArr, 3), 0);
                if (i5 >= 10) {
                    b("Dump first 10 byte: " + b(bArr2, 10), 1);
                }
                if (i5 >= 23) {
                    byte[] bArr4 = new byte[13];
                    System.arraycopy(bArr2, i5 - 13, bArr4, 0, 13);
                    b("Dump last 13 byte:" + b(bArr4, 13), 1);
                }
                p();
                return -3;
            } else if (a4 == 0) {
                b("Recv empty read data", 0);
                i4 = -1;
                break;
            } else if (a(bArr3, a4, s2)) {
                if (this.s != -1) {
                    b("[DBG] Recv read CSW:\n" + b(bArr3, a4), this.s);
                }
                if (a4 > 13) {
                    System.arraycopy(bArr3, 0, bArr2, i5, a4 - 13);
                    i5 += a4 - 13;
                }
                i4 = i5;
            } else {
                if (this.s != -1) {
                    b("[DBG] Recv read data (" + a4 + "):\n" + b(bArr3, a4), this.s);
                }
                if (i5 + a4 > i3) {
                    b("Over data buffer! Already recv size: " + i5 + ", recv data size: " + a4 + ", data:", 0);
                    b(b(bArr3, a4), 3);
                    i4 = -1;
                    break;
                }
                System.arraycopy(bArr3, 0, bArr2, i5, a4);
                i5 += a4;
            }
        }
        if (!z) {
            u.b();
        }
        p();
        return i4;
    }

    public int a(byte[] bArr, byte b2) {
        return a(bArr, new byte[]{b2}, 1);
    }

    public int a(byte[] bArr, byte[] bArr2, int i2) {
        int i3;
        int i4;
        byte[] bArr3 = new byte[this.k];
        o();
        if (a() != 3) {
            b("Connected first", 0);
            p();
            return -1;
        } else if (i2 > 65536) {
            b("Over buffer. Size: " + i2, 0);
            p();
            return -1;
        } else {
            a aVar = new a(Integer.reverseBytes(i2), (byte) 0);
            aVar.a(bArr);
            byte[] a2 = aVar.a();
            if (this.s != -1) {
                b("[DBG] Write Cmd:\n" + b(a2, 31), this.s);
            }
            int a3 = a(this.l, a2, 31, 2000);
            if (a3 < 0) {
                b("Write cmd send Fail: " + a3 + ". Cmd: " + b(bArr, 3), 0);
                p();
                return -3;
            }
            int i5 = i2;
            int i6 = 0;
            while (i5 > 0) {
                if (i5 > this.m) {
                    System.arraycopy(bArr2, i6, bArr3, 0, this.m);
                    i4 = this.m;
                } else {
                    System.arraycopy(bArr2, i6, bArr3, 0, i5);
                    i4 = i5;
                }
                int a4 = a(this.l, bArr3, i4, 2000);
                if (a4 < 0) {
                    b("Send write data fail: " + a4 + " TargetSize: " + i4 + ". Cmd: " + b(bArr, 3), 0);
                    p();
                    return -3;
                } else if (a4 != i4) {
                    b("Send write data fail, recvDataSize: " + a4 + " TargetSize: " + i4 + ". Cmd: " + b(bArr, 3), 0);
                    p();
                    return -1;
                } else {
                    if (this.s != -1) {
                        b("[DBG] Send write data (" + i4 + "):\n" + b(bArr3, i4), this.s);
                    }
                    i6 += i4;
                    i5 -= i4;
                }
            }
            if (this.s != -1) {
                b("[DBG] Write data done\n", this.s);
            }
            int a5 = a(this.j, bArr3, this.k, 2000);
            if (d(bArr3, a5)) {
                if (this.s != -1) {
                    b("[DBG] Recv write CSW:\n" + b(bArr3, a5), this.s);
                }
                i3 = 0;
            } else {
                b("Unknown recv data, recvDataSize: " + a5, 0);
                i3 = -1;
            }
            u.b();
            p();
            return i3;
        }
    }

    public byte[] a(int i2, int i3) {
        byte[] bArr = new byte[i3];
        o();
        System.arraycopy(this.o, i2, bArr, 0, i3);
        p();
        return bArr;
    }

    public byte a(int i2) {
        return this.o[i2];
    }

    public int b(int i2, int i3) {
        return c(a(i2, i3), i3);
    }

    public int n() {
        int i2 = 0;
        o();
        if (a() != 3) {
            b("Connected first. No need to reset", 0);
            p();
            return 0;
        } else if (this.n == null) {
            b("targetConnection = null, cancel reset", 0);
            return -1;
        } else {
            b("Send Reset", 1);
            if (this.n.controlTransfer(33, 255, 0, this.i.getId(), null, 0, 2000) < 0) {
                i2 = -1;
            }
            p();
            return i2;
        }
    }

    /* access modifiers changed from: private */
    public static void b(String str, int i2) {
        d.a("SCSIHost", str, i2);
    }

    private void o() {
        try {
            this.v.acquire();
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    private void p() {
        this.v.release();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void q() {
        o();
        this.h = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.n = null;
        p();
    }

    private void r() {
        this.q = PendingIntent.getBroadcast(t, 0, new Intent("com.android.example.USB_PERMISSION"), 0);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.android.example.USB_PERMISSION");
        intentFilter.addAction("android.hardware.usb.action.USB_DEVICE_ATTACHED");
        intentFilter.addAction("android.hardware.usb.action.USB_DEVICE_DETACHED");
        t.registerReceiver(this.w, intentFilter);
    }

    private void a(UsbDevice usbDevice) {
        b(((((((("DeviceID: " + usbDevice.getDeviceId()) + "\n  DeviceName: " + usbDevice.getDeviceName()) + "\n  ProductID: " + usbDevice.getProductId()) + "\n  VendorID: " + usbDevice.getVendorId()) + "\n  DeviceClass: " + usbDevice.getDeviceClass()) + "\n  DeviceSubClass: " + usbDevice.getDeviceSubclass()) + "\n  DeviceProtocol: " + usbDevice.getDeviceProtocol()) + "\n  Interface Count: " + usbDevice.getInterfaceCount(), 3);
    }


    // Seems to be something like a formatter --> (convert to hexadecimal with 16 digit width)
    private String b(byte[] bArr, int tabSize) {
        String str = "";
        int index = 0;
        while (index < tabSize) {
            String tmpString = (str + Integer.toString((bArr[index] & 255) + 256, 16).substring(1)) + " ";
            if (index % 16 == 15) {
                tmpString = tmpString + "\r\n";
            }
            index++;
            str = tmpString;
        }
        return str;
    }


    private int c(byte[] bArr, int i2) {
        switch (i2) {
            case 2:
                return (bArr[0] & 255) | ((bArr[1] & 255) << 8);
            case 3:
            default:
                b("Invalid byecount " + i2, 0);
                return 0;
            case 4:
                return (bArr[0] & 255) | ((bArr[1] & 255) << 8) | ((bArr[2] & 255) << 16) | ((bArr[3] & 255) << 24);
        }
    }

    // Get random INT
    private int s() {
        return new Random().nextInt();
    }

    // check if byte Array is valid ???
    private boolean a(byte[] bArr, int i2, int i3) {
        if (i2 >= 13) {
            return i3 == 0 ? bArr[i2 + -13] == 85 && bArr[i2 + -12] == 83 && bArr[i2 + -11] == 66 && bArr[i2 + -10] == 83 : bArr[i2 + -13] == 85 && bArr[i2 + -12] == 83 && bArr[i2 + -11] == 66 && bArr[i2 + -10] == 83 && bArr[i2 + -9] == ((byte) (i3 >> 24)) && bArr[i2 + -8] == ((byte) (i3 >> 16)) && bArr[i2 + -7] == ((byte) (i3 >> 8)) && bArr[i2 + -6] == ((byte) i3);
        }
        return false;
    }

    // Check array using bool a() with fixed i3 parameter
    private boolean d(byte[] bArr, int i2) {
        return a(bArr, i2, 0);
    }

    
    private int a(UsbEndpoint usbEndpoint, byte[] bArr, int i2, int i3) {
        if (a() == 0) {
            b("SCSI_HOST_STATUS_NOT_INITIALIZED, cancel send bulk data", 1);
            return -3;
        } else if (this.n != null) {
            return this.n.bulkTransfer(usbEndpoint, bArr, i2, i3);
        } else {
            b("targetConnection = null, cancel send bulk data", 0);
            return -3;
        }
    }
}
