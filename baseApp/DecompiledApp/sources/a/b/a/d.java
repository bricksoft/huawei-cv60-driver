package a.b.a;

import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static int f181a = 3;

    public static void a(String str, String str2, int i) {
        if (i <= f181a) {
            String format = new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());
            if (i == 0) {
                Log.e(str, format + "[ERROR]" + "[SCSI]" + str2);
            } else if (i == 1) {
                Log.w(str, format + "[WARNING]" + "[SCSI]" + str2);
            } else if (i == 2) {
                Log.i(str, format + "[INFO]" + "[SCSI]" + str2);
            } else if (i == 3) {
                Log.d(str, format + "[DEBUG]" + "[SCSI]" + str2);
            } else if (i == 4) {
                Log.v(str, format + "[MASS_DEBUG]" + "[SCSI]" + str2);
            }
        }
    }
}
