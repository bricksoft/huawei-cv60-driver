package a;

import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static int f163a = 3;

    public static void a(String str, String str2, int i) {
        if (i <= f163a) {
            String format = new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());
            if (i == 0) {
                Log.e(str, format + "[ERROR]" + "[Remote]" + str2);
            } else if (i == 1) {
                Log.w(str, format + "[WARNING]" + "[Remote]" + str2);
            } else if (i == 2) {
                Log.i(str, format + "[INFO]" + "[Remote]" + str2);
            } else if (i == 3) {
                Log.i(str, format + "[DEBUG]" + "[Remote]" + str2);
            } else if (i == 4) {
                Log.i(str, format + "[MASS_DEBUG]" + "[Remote]" + str2);
            }
        }
    }
}
