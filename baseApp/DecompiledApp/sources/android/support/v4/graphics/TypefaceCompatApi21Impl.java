package android.support.v4.graphics;

import android.os.ParcelFileDescriptor;
import android.support.annotation.RequiresApi;
import android.support.annotation.RestrictTo;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import java.io.File;

@RequiresApi(21)
@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
class TypefaceCompatApi21Impl extends TypefaceCompatBaseImpl {
    private static final String TAG = "TypefaceCompatApi21Impl";

    TypefaceCompatApi21Impl() {
    }

    private File getFile(ParcelFileDescriptor parcelFileDescriptor) {
        try {
            String readlink = Os.readlink("/proc/self/fd/" + parcelFileDescriptor.getFd());
            if (OsConstants.S_ISREG(Os.stat(readlink).st_mode)) {
                return new File(readlink);
            }
            return null;
        } catch (ErrnoException e) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004a, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004b, code lost:
        r3 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x005a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x005b, code lost:
        r2 = r1;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0069, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x009b, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x009c, code lost:
        r2 = null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x005a A[ExcHandler: all (r1v4 'th' java.lang.Throwable A[CUSTOM_DECLARE])] */
    @Override // android.support.v4.graphics.TypefaceCompatBaseImpl
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Typeface createFromFontInfo(android.content.Context r7, android.os.CancellationSignal r8, @android.support.annotation.NonNull android.support.v4.provider.FontsContractCompat.FontInfo[] r9, int r10) {
        /*
        // Method dump skipped, instructions count: 158
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.graphics.TypefaceCompatApi21Impl.createFromFontInfo(android.content.Context, android.os.CancellationSignal, android.support.v4.provider.FontsContractCompat$FontInfo[], int):android.graphics.Typeface");
    }
}
