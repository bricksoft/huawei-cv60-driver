package android.support.v4.graphics;

import android.graphics.Path;
import android.support.annotation.RestrictTo;
import android.util.Log;
import java.util.ArrayList;
import tv.danmaku.ijk.media.player.IjkMediaMeta;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class PathParser {
    private static final String LOGTAG = "PathParser";

    static float[] copyOfRange(float[] fArr, int i, int i2) {
        if (i > i2) {
            throw new IllegalArgumentException();
        }
        int length = fArr.length;
        if (i < 0 || i > length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int i3 = i2 - i;
        int min = Math.min(i3, length - i);
        float[] fArr2 = new float[i3];
        System.arraycopy(fArr, i, fArr2, 0, min);
        return fArr2;
    }

    public static Path createPathFromPathData(String str) {
        Path path = new Path();
        PathDataNode[] createNodesFromPathData = createNodesFromPathData(str);
        if (createNodesFromPathData == null) {
            return null;
        }
        try {
            PathDataNode.nodesToPath(createNodesFromPathData, path);
            return path;
        } catch (RuntimeException e) {
            throw new RuntimeException("Error in parsing " + str, e);
        }
    }

    public static PathDataNode[] createNodesFromPathData(String str) {
        if (str == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int i = 1;
        int i2 = 0;
        while (i < str.length()) {
            int nextStart = nextStart(str, i);
            String trim = str.substring(i2, nextStart).trim();
            if (trim.length() > 0) {
                addNode(arrayList, trim.charAt(0), getFloats(trim));
            }
            i = nextStart + 1;
            i2 = nextStart;
        }
        if (i - i2 == 1 && i2 < str.length()) {
            addNode(arrayList, str.charAt(i2), new float[0]);
        }
        return (PathDataNode[]) arrayList.toArray(new PathDataNode[arrayList.size()]);
    }

    public static PathDataNode[] deepCopyNodes(PathDataNode[] pathDataNodeArr) {
        if (pathDataNodeArr == null) {
            return null;
        }
        PathDataNode[] pathDataNodeArr2 = new PathDataNode[pathDataNodeArr.length];
        for (int i = 0; i < pathDataNodeArr.length; i++) {
            pathDataNodeArr2[i] = new PathDataNode(pathDataNodeArr[i]);
        }
        return pathDataNodeArr2;
    }

    public static boolean canMorph(PathDataNode[] pathDataNodeArr, PathDataNode[] pathDataNodeArr2) {
        if (pathDataNodeArr == null || pathDataNodeArr2 == null || pathDataNodeArr.length != pathDataNodeArr2.length) {
            return false;
        }
        for (int i = 0; i < pathDataNodeArr.length; i++) {
            if (!(pathDataNodeArr[i].mType == pathDataNodeArr2[i].mType && pathDataNodeArr[i].mParams.length == pathDataNodeArr2[i].mParams.length)) {
                return false;
            }
        }
        return true;
    }

    public static void updateNodes(PathDataNode[] pathDataNodeArr, PathDataNode[] pathDataNodeArr2) {
        for (int i = 0; i < pathDataNodeArr2.length; i++) {
            pathDataNodeArr[i].mType = pathDataNodeArr2[i].mType;
            for (int i2 = 0; i2 < pathDataNodeArr2[i].mParams.length; i2++) {
                pathDataNodeArr[i].mParams[i2] = pathDataNodeArr2[i].mParams[i2];
            }
        }
    }

    private static int nextStart(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (((charAt - 'A') * (charAt - 'Z') <= 0 || (charAt - 'a') * (charAt - 'z') <= 0) && charAt != 'e' && charAt != 'E') {
                break;
            }
            i++;
        }
        return i;
    }

    private static void addNode(ArrayList<PathDataNode> arrayList, char c, float[] fArr) {
        arrayList.add(new PathDataNode(c, fArr));
    }

    /* access modifiers changed from: private */
    public static class ExtractFloatResult {
        int mEndPosition;
        boolean mEndWithNegOrDot;

        ExtractFloatResult() {
        }
    }

    private static float[] getFloats(String str) {
        int i;
        int i2 = 0;
        if (str.charAt(0) == 'z' || str.charAt(0) == 'Z') {
            return new float[0];
        }
        try {
            float[] fArr = new float[str.length()];
            ExtractFloatResult extractFloatResult = new ExtractFloatResult();
            int length = str.length();
            int i3 = 1;
            while (i3 < length) {
                extract(str, i3, extractFloatResult);
                int i4 = extractFloatResult.mEndPosition;
                if (i3 < i4) {
                    i = i2 + 1;
                    fArr[i2] = Float.parseFloat(str.substring(i3, i4));
                } else {
                    i = i2;
                }
                if (extractFloatResult.mEndWithNegOrDot) {
                    i3 = i4;
                    i2 = i;
                } else {
                    i3 = i4 + 1;
                    i2 = i;
                }
            }
            return copyOfRange(fArr, 0, i2);
        } catch (NumberFormatException e) {
            throw new RuntimeException("error in parsing \"" + str + "\"", e);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static void extract(String str, int i, ExtractFloatResult extractFloatResult) {
        extractFloatResult.mEndWithNegOrDot = false;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        for (int i2 = i; i2 < str.length(); i2++) {
            switch (str.charAt(i2)) {
                case ' ':
                case ',':
                    z = false;
                    z3 = true;
                    break;
                case '-':
                    if (i2 != i && !z) {
                        extractFloatResult.mEndWithNegOrDot = true;
                        z = false;
                        z3 = true;
                        break;
                    }
                    z = false;
                    break;
                case '.':
                    if (!z2) {
                        z = false;
                        z2 = true;
                        break;
                    } else {
                        extractFloatResult.mEndWithNegOrDot = true;
                        z = false;
                        z3 = true;
                        break;
                    }
                case 'E':
                case 'e':
                    z = true;
                    break;
                default:
                    z = false;
                    break;
            }
            if (z3) {
                extractFloatResult.mEndPosition = i2;
            }
        }
        extractFloatResult.mEndPosition = i2;
    }

    public static class PathDataNode {
        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        public float[] mParams;
        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        public char mType;

        PathDataNode(char c, float[] fArr) {
            this.mType = c;
            this.mParams = fArr;
        }

        PathDataNode(PathDataNode pathDataNode) {
            this.mType = pathDataNode.mType;
            this.mParams = PathParser.copyOfRange(pathDataNode.mParams, 0, pathDataNode.mParams.length);
        }

        public static void nodesToPath(PathDataNode[] pathDataNodeArr, Path path) {
            float[] fArr = new float[6];
            char c = 'm';
            for (int i = 0; i < pathDataNodeArr.length; i++) {
                addCommand(path, fArr, c, pathDataNodeArr[i].mType, pathDataNodeArr[i].mParams);
                c = pathDataNodeArr[i].mType;
            }
        }

        public void interpolatePathDataNode(PathDataNode pathDataNode, PathDataNode pathDataNode2, float f) {
            for (int i = 0; i < pathDataNode.mParams.length; i++) {
                this.mParams[i] = (pathDataNode.mParams[i] * (1.0f - f)) + (pathDataNode2.mParams[i] * f);
            }
        }

        private static void addCommand(Path path, float[] fArr, char c, char c2, float[] fArr2) {
            int i;
            boolean z;
            float f;
            float f2;
            float f3;
            float f4;
            float f5;
            float f6;
            boolean z2;
            float f7;
            float f8;
            float f9 = fArr[0];
            float f10 = fArr[1];
            float f11 = fArr[2];
            float f12 = fArr[3];
            float f13 = fArr[4];
            float f14 = fArr[5];
            switch (c2) {
                case 'A':
                case 'a':
                    i = 7;
                    break;
                case 'C':
                case 'c':
                    i = 6;
                    break;
                case 'H':
                case 'V':
                case 'h':
                case 'v':
                    i = 1;
                    break;
                case 'L':
                case 'M':
                case 'T':
                case 'l':
                case 'm':
                case 't':
                    i = 2;
                    break;
                case 'Q':
                case 'S':
                case 'q':
                case 's':
                    i = 4;
                    break;
                case 'Z':
                case IjkMediaMeta.FF_PROFILE_H264_HIGH_422 /*{ENCODED_INT: 122}*/:
                    path.close();
                    path.moveTo(f13, f14);
                    f12 = f14;
                    f11 = f13;
                    f10 = f14;
                    f9 = f13;
                    i = 2;
                    break;
                default:
                    i = 2;
                    break;
            }
            int i2 = 0;
            float f15 = f14;
            float f16 = f13;
            float f17 = f12;
            float f18 = f11;
            float f19 = f10;
            float f20 = f9;
            while (i2 < fArr2.length) {
                switch (c2) {
                    case 'A':
                        float f21 = fArr2[i2 + 5];
                        float f22 = fArr2[i2 + 6];
                        float f23 = fArr2[i2 + 0];
                        float f24 = fArr2[i2 + 1];
                        float f25 = fArr2[i2 + 2];
                        boolean z3 = fArr2[i2 + 3] != 0.0f;
                        if (fArr2[i2 + 4] != 0.0f) {
                            z = true;
                        } else {
                            z = false;
                        }
                        drawArc(path, f20, f19, f21, f22, f23, f24, f25, z3, z);
                        float f26 = fArr2[i2 + 5];
                        float f27 = fArr2[i2 + 6];
                        f = f15;
                        f2 = f16;
                        f3 = f27;
                        f4 = f26;
                        f5 = f27;
                        f6 = f26;
                        break;
                    case 'C':
                        path.cubicTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3], fArr2[i2 + 4], fArr2[i2 + 5]);
                        float f28 = fArr2[i2 + 4];
                        float f29 = fArr2[i2 + 5];
                        float f30 = fArr2[i2 + 2];
                        f = f15;
                        f2 = f16;
                        f3 = fArr2[i2 + 3];
                        f4 = f30;
                        f5 = f29;
                        f6 = f28;
                        break;
                    case 'H':
                        path.lineTo(fArr2[i2 + 0], f19);
                        f = f15;
                        f2 = f16;
                        f3 = f17;
                        f4 = f18;
                        f5 = f19;
                        f6 = fArr2[i2 + 0];
                        break;
                    case 'L':
                        path.lineTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                        float f31 = fArr2[i2 + 0];
                        f = f15;
                        f2 = f16;
                        f3 = f17;
                        f4 = f18;
                        f5 = fArr2[i2 + 1];
                        f6 = f31;
                        break;
                    case 'M':
                        float f32 = fArr2[i2 + 0];
                        float f33 = fArr2[i2 + 1];
                        if (i2 <= 0) {
                            path.moveTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                            f = f33;
                            f2 = f32;
                            f3 = f17;
                            f4 = f18;
                            f5 = f33;
                            f6 = f32;
                            break;
                        } else {
                            path.lineTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                            f = f15;
                            f2 = f16;
                            f3 = f17;
                            f4 = f18;
                            f5 = f33;
                            f6 = f32;
                            break;
                        }
                    case 'Q':
                        path.quadTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3]);
                        float f34 = fArr2[i2 + 0];
                        float f35 = fArr2[i2 + 1];
                        float f36 = fArr2[i2 + 2];
                        f = f15;
                        f2 = f16;
                        f3 = f35;
                        f4 = f34;
                        f5 = fArr2[i2 + 3];
                        f6 = f36;
                        break;
                    case 'S':
                        if (c == 'c' || c == 's' || c == 'C' || c == 'S') {
                            f8 = (2.0f * f20) - f18;
                            f7 = (2.0f * f19) - f17;
                        } else {
                            f7 = f19;
                            f8 = f20;
                        }
                        path.cubicTo(f8, f7, fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3]);
                        float f37 = fArr2[i2 + 0];
                        float f38 = fArr2[i2 + 1];
                        float f39 = fArr2[i2 + 2];
                        f = f15;
                        f2 = f16;
                        f3 = f38;
                        f4 = f37;
                        f5 = fArr2[i2 + 3];
                        f6 = f39;
                        break;
                    case 'T':
                        if (c == 'q' || c == 't' || c == 'Q' || c == 'T') {
                            f20 = (2.0f * f20) - f18;
                            f19 = (2.0f * f19) - f17;
                        }
                        path.quadTo(f20, f19, fArr2[i2 + 0], fArr2[i2 + 1]);
                        float f40 = fArr2[i2 + 0];
                        f = f15;
                        f2 = f16;
                        f3 = f19;
                        f4 = f20;
                        f5 = fArr2[i2 + 1];
                        f6 = f40;
                        break;
                    case 'V':
                        path.lineTo(f20, fArr2[i2 + 0]);
                        f = f15;
                        f2 = f16;
                        f3 = f17;
                        f4 = f18;
                        f5 = fArr2[i2 + 0];
                        f6 = f20;
                        break;
                    case 'a':
                        float f41 = fArr2[i2 + 5] + f20;
                        float f42 = fArr2[i2 + 6] + f19;
                        float f43 = fArr2[i2 + 0];
                        float f44 = fArr2[i2 + 1];
                        float f45 = fArr2[i2 + 2];
                        boolean z4 = fArr2[i2 + 3] != 0.0f;
                        if (fArr2[i2 + 4] != 0.0f) {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        drawArc(path, f20, f19, f41, f42, f43, f44, f45, z4, z2);
                        float f46 = f20 + fArr2[i2 + 5];
                        float f47 = f19 + fArr2[i2 + 6];
                        f = f15;
                        f2 = f16;
                        f3 = f47;
                        f4 = f46;
                        f5 = f47;
                        f6 = f46;
                        break;
                    case 'c':
                        path.rCubicTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3], fArr2[i2 + 4], fArr2[i2 + 5]);
                        float f48 = f20 + fArr2[i2 + 2];
                        float f49 = f19 + fArr2[i2 + 3];
                        float f50 = f20 + fArr2[i2 + 4];
                        f = f15;
                        f2 = f16;
                        f3 = f49;
                        f4 = f48;
                        f5 = fArr2[i2 + 5] + f19;
                        f6 = f50;
                        break;
                    case 'h':
                        path.rLineTo(fArr2[i2 + 0], 0.0f);
                        f = f15;
                        f2 = f16;
                        f3 = f17;
                        f4 = f18;
                        f5 = f19;
                        f6 = f20 + fArr2[i2 + 0];
                        break;
                    case 'l':
                        path.rLineTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                        float f51 = f20 + fArr2[i2 + 0];
                        f = f15;
                        f2 = f16;
                        f3 = f17;
                        f4 = f18;
                        f5 = fArr2[i2 + 1] + f19;
                        f6 = f51;
                        break;
                    case 'm':
                        float f52 = f20 + fArr2[i2 + 0];
                        float f53 = fArr2[i2 + 1] + f19;
                        if (i2 <= 0) {
                            path.rMoveTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                            f = f53;
                            f2 = f52;
                            f3 = f17;
                            f4 = f18;
                            f5 = f53;
                            f6 = f52;
                            break;
                        } else {
                            path.rLineTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                            f = f15;
                            f2 = f16;
                            f3 = f17;
                            f4 = f18;
                            f5 = f53;
                            f6 = f52;
                            break;
                        }
                    case 'q':
                        path.rQuadTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3]);
                        float f54 = f20 + fArr2[i2 + 0];
                        float f55 = f19 + fArr2[i2 + 1];
                        float f56 = f20 + fArr2[i2 + 2];
                        f = f15;
                        f2 = f16;
                        f3 = f55;
                        f4 = f54;
                        f5 = fArr2[i2 + 3] + f19;
                        f6 = f56;
                        break;
                    case 's':
                        float f57 = 0.0f;
                        float f58 = 0.0f;
                        if (c == 'c' || c == 's' || c == 'C' || c == 'S') {
                            f57 = f20 - f18;
                            f58 = f19 - f17;
                        }
                        path.rCubicTo(f57, f58, fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3]);
                        float f59 = f20 + fArr2[i2 + 0];
                        float f60 = f19 + fArr2[i2 + 1];
                        float f61 = f20 + fArr2[i2 + 2];
                        f = f15;
                        f2 = f16;
                        f3 = f60;
                        f4 = f59;
                        f5 = fArr2[i2 + 3] + f19;
                        f6 = f61;
                        break;
                    case 't':
                        float f62 = 0.0f;
                        float f63 = 0.0f;
                        if (c == 'q' || c == 't' || c == 'Q' || c == 'T') {
                            f62 = f20 - f18;
                            f63 = f19 - f17;
                        }
                        path.rQuadTo(f62, f63, fArr2[i2 + 0], fArr2[i2 + 1]);
                        float f64 = f20 + f62;
                        float f65 = f20 + fArr2[i2 + 0];
                        f = f15;
                        f2 = f16;
                        f3 = f19 + f63;
                        f4 = f64;
                        f5 = fArr2[i2 + 1] + f19;
                        f6 = f65;
                        break;
                    case 'v':
                        path.rLineTo(0.0f, fArr2[i2 + 0]);
                        f = f15;
                        f2 = f16;
                        f3 = f17;
                        f4 = f18;
                        f5 = fArr2[i2 + 0] + f19;
                        f6 = f20;
                        break;
                    default:
                        f = f15;
                        f2 = f16;
                        f3 = f17;
                        f4 = f18;
                        f5 = f19;
                        f6 = f20;
                        break;
                }
                i2 += i;
                f15 = f;
                f16 = f2;
                f17 = f3;
                f18 = f4;
                f19 = f5;
                f20 = f6;
                c = c2;
            }
            fArr[0] = f20;
            fArr[1] = f19;
            fArr[2] = f18;
            fArr[3] = f17;
            fArr[4] = f16;
            fArr[5] = f15;
        }

        private static void drawArc(Path path, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z, boolean z2) {
            double d;
            double d2;
            double radians = Math.toRadians((double) f7);
            double cos = Math.cos(radians);
            double sin = Math.sin(radians);
            double d3 = ((((double) f) * cos) + (((double) f2) * sin)) / ((double) f5);
            double d4 = ((((double) (-f)) * sin) + (((double) f2) * cos)) / ((double) f6);
            double d5 = ((((double) f3) * cos) + (((double) f4) * sin)) / ((double) f5);
            double d6 = ((((double) (-f3)) * sin) + (((double) f4) * cos)) / ((double) f6);
            double d7 = d3 - d5;
            double d8 = d4 - d6;
            double d9 = (d3 + d5) / 2.0d;
            double d10 = (d4 + d6) / 2.0d;
            double d11 = (d7 * d7) + (d8 * d8);
            if (d11 == 0.0d) {
                Log.w(PathParser.LOGTAG, " Points are coincident");
                return;
            }
            double d12 = (1.0d / d11) - 0.25d;
            if (d12 < 0.0d) {
                Log.w(PathParser.LOGTAG, "Points are too far apart " + d11);
                float sqrt = (float) (Math.sqrt(d11) / 1.99999d);
                drawArc(path, f, f2, f3, f4, f5 * sqrt, f6 * sqrt, f7, z, z2);
                return;
            }
            double sqrt2 = Math.sqrt(d12);
            double d13 = d7 * sqrt2;
            double d14 = d8 * sqrt2;
            if (z == z2) {
                d = d9 - d14;
                d2 = d13 + d10;
            } else {
                d = d14 + d9;
                d2 = d10 - d13;
            }
            double atan2 = Math.atan2(d4 - d2, d3 - d);
            double atan22 = Math.atan2(d6 - d2, d5 - d) - atan2;
            if (z2 != (atan22 >= 0.0d)) {
                if (atan22 > 0.0d) {
                    atan22 -= 6.283185307179586d;
                } else {
                    atan22 += 6.283185307179586d;
                }
            }
            double d15 = ((double) f5) * d;
            double d16 = d2 * ((double) f6);
            arcToBezier(path, (d15 * cos) - (d16 * sin), (d15 * sin) + (d16 * cos), (double) f5, (double) f6, (double) f, (double) f2, radians, atan2, atan22);
        }

        private static void arcToBezier(Path path, double d, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9) {
            int ceil = (int) Math.ceil(Math.abs((4.0d * d9) / 3.141592653589793d));
            double cos = Math.cos(d7);
            double sin = Math.sin(d7);
            double cos2 = Math.cos(d8);
            double sin2 = Math.sin(d8);
            double d10 = (((-d3) * cos) * sin2) - ((d4 * sin) * cos2);
            double d11 = (sin2 * (-d3) * sin) + (cos2 * d4 * cos);
            double d12 = d9 / ((double) ceil);
            int i = 0;
            while (i < ceil) {
                double d13 = d8 + d12;
                double sin3 = Math.sin(d13);
                double cos3 = Math.cos(d13);
                double d14 = (((d3 * cos) * cos3) + d) - ((d4 * sin) * sin3);
                double d15 = (d4 * cos * sin3) + (d3 * sin * cos3) + d2;
                double d16 = (((-d3) * cos) * sin3) - ((d4 * sin) * cos3);
                double d17 = (cos3 * d4 * cos) + (sin3 * (-d3) * sin);
                double tan = Math.tan((d13 - d8) / 2.0d);
                double sqrt = ((Math.sqrt((tan * (3.0d * tan)) + 4.0d) - 1.0d) * Math.sin(d13 - d8)) / 3.0d;
                path.rLineTo(0.0f, 0.0f);
                path.cubicTo((float) ((d10 * sqrt) + d5), (float) ((d11 * sqrt) + d6), (float) (d14 - (sqrt * d16)), (float) (d15 - (sqrt * d17)), (float) d14, (float) d15);
                i++;
                d11 = d17;
                d10 = d16;
                d8 = d13;
                d6 = d15;
                d5 = d14;
            }
        }
    }

    private PathParser() {
    }
}
