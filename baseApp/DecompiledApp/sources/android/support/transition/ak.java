package android.support.transition;

import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.WindowId;

/* access modifiers changed from: package-private */
@RequiresApi(18)
public class ak implements al {

    /* renamed from: a  reason: collision with root package name */
    private final WindowId f395a;

    ak(@NonNull View view) {
        this.f395a = view.getWindowId();
    }

    public boolean equals(Object obj) {
        return (obj instanceof ak) && ((ak) obj).f395a.equals(this.f395a);
    }

    public int hashCode() {
        return this.f395a.hashCode();
    }
}
