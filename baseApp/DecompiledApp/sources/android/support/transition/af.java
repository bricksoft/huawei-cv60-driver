package android.support.transition;

import android.graphics.Matrix;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@RequiresApi(21)
class af extends ae {

    /* renamed from: a  reason: collision with root package name */
    private static Method f389a;
    private static boolean b;
    private static Method c;
    private static boolean d;

    af() {
    }

    @Override // android.support.transition.ah
    public void a(@NonNull View view, @NonNull Matrix matrix) {
        a();
        if (f389a != null) {
            try {
                f389a.invoke(view, matrix);
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    @Override // android.support.transition.ah
    public void b(@NonNull View view, @NonNull Matrix matrix) {
        b();
        if (c != null) {
            try {
                c.invoke(view, matrix);
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    private void a() {
        if (!b) {
            try {
                f389a = View.class.getDeclaredMethod("transformMatrixToGlobal", Matrix.class);
                f389a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToGlobal method", e);
            }
            b = true;
        }
    }

    private void b() {
        if (!d) {
            try {
                c = View.class.getDeclaredMethod("transformMatrixToLocal", Matrix.class);
                c.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToLocal method", e);
            }
            d = true;
        }
    }
}
