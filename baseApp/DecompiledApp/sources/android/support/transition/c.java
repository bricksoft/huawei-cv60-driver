package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import java.util.Map;

public class c extends l {
    private static final String[] h = {"android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY"};
    private static final Property<Drawable, PointF> i = new Property<Drawable, PointF>(PointF.class, "boundsOrigin") {
        /* class android.support.transition.c.AnonymousClass1 */

        /* renamed from: a  reason: collision with root package name */
        private Rect f396a = new Rect();

        /* renamed from: a */
        public void set(Drawable drawable, PointF pointF) {
            drawable.copyBounds(this.f396a);
            this.f396a.offsetTo(Math.round(pointF.x), Math.round(pointF.y));
            drawable.setBounds(this.f396a);
        }

        /* renamed from: a */
        public PointF get(Drawable drawable) {
            drawable.copyBounds(this.f396a);
            return new PointF((float) this.f396a.left, (float) this.f396a.top);
        }
    };
    private static final Property<a, PointF> j = new Property<a, PointF>(PointF.class, "topLeft") {
        /* class android.support.transition.c.AnonymousClass3 */

        /* renamed from: a */
        public void set(a aVar, PointF pointF) {
            aVar.a(pointF);
        }

        /* renamed from: a */
        public PointF get(a aVar) {
            return null;
        }
    };
    private static final Property<a, PointF> k = new Property<a, PointF>(PointF.class, "bottomRight") {
        /* class android.support.transition.c.AnonymousClass4 */

        /* renamed from: a */
        public void set(a aVar, PointF pointF) {
            aVar.b(pointF);
        }

        /* renamed from: a */
        public PointF get(a aVar) {
            return null;
        }
    };
    private static final Property<View, PointF> l = new Property<View, PointF>(PointF.class, "bottomRight") {
        /* class android.support.transition.c.AnonymousClass5 */

        /* renamed from: a */
        public void set(View view, PointF pointF) {
            ad.a(view, view.getLeft(), view.getTop(), Math.round(pointF.x), Math.round(pointF.y));
        }

        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }
    };
    private static final Property<View, PointF> m = new Property<View, PointF>(PointF.class, "topLeft") {
        /* class android.support.transition.c.AnonymousClass6 */

        /* renamed from: a */
        public void set(View view, PointF pointF) {
            ad.a(view, Math.round(pointF.x), Math.round(pointF.y), view.getRight(), view.getBottom());
        }

        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }
    };
    private static final Property<View, PointF> n = new Property<View, PointF>(PointF.class, "position") {
        /* class android.support.transition.c.AnonymousClass7 */

        /* renamed from: a */
        public void set(View view, PointF pointF) {
            int round = Math.round(pointF.x);
            int round2 = Math.round(pointF.y);
            ad.a(view, round, round2, view.getWidth() + round, view.getHeight() + round2);
        }

        /* renamed from: a */
        public PointF get(View view) {
            return null;
        }
    };
    private static j r = new j();
    private int[] o = new int[2];
    private boolean p = false;
    private boolean q = false;

    @Override // android.support.transition.l
    @Nullable
    public String[] a() {
        return h;
    }

    private void d(s sVar) {
        View view = sVar.b;
        if (ViewCompat.isLaidOut(view) || view.getWidth() != 0 || view.getHeight() != 0) {
            sVar.f421a.put("android:changeBounds:bounds", new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()));
            sVar.f421a.put("android:changeBounds:parent", sVar.b.getParent());
            if (this.q) {
                sVar.b.getLocationInWindow(this.o);
                sVar.f421a.put("android:changeBounds:windowX", Integer.valueOf(this.o[0]));
                sVar.f421a.put("android:changeBounds:windowY", Integer.valueOf(this.o[1]));
            }
            if (this.p) {
                sVar.f421a.put("android:changeBounds:clip", ViewCompat.getClipBounds(view));
            }
        }
    }

    @Override // android.support.transition.l
    public void a(@NonNull s sVar) {
        d(sVar);
    }

    @Override // android.support.transition.l
    public void b(@NonNull s sVar) {
        d(sVar);
    }

    private boolean a(View view, View view2) {
        if (!this.q) {
            return true;
        }
        s b = b(view, true);
        return b == null ? view == view2 : view2 == b.b;
    }

    @Override // android.support.transition.l
    @Nullable
    public Animator a(@NonNull final ViewGroup viewGroup, @Nullable s sVar, @Nullable s sVar2) {
        ObjectAnimator objectAnimator;
        Rect rect;
        Rect rect2;
        ObjectAnimator objectAnimator2;
        ObjectAnimator a2;
        if (sVar == null || sVar2 == null) {
            return null;
        }
        Map<String, Object> map = sVar.f421a;
        Map<String, Object> map2 = sVar2.f421a;
        ViewGroup viewGroup2 = (ViewGroup) map.get("android:changeBounds:parent");
        ViewGroup viewGroup3 = (ViewGroup) map2.get("android:changeBounds:parent");
        if (viewGroup2 == null || viewGroup3 == null) {
            return null;
        }
        final View view = sVar2.b;
        if (a(viewGroup2, viewGroup3)) {
            Rect rect3 = (Rect) sVar.f421a.get("android:changeBounds:bounds");
            Rect rect4 = (Rect) sVar2.f421a.get("android:changeBounds:bounds");
            int i2 = rect3.left;
            final int i3 = rect4.left;
            int i4 = rect3.top;
            final int i5 = rect4.top;
            int i6 = rect3.right;
            final int i7 = rect4.right;
            int i8 = rect3.bottom;
            final int i9 = rect4.bottom;
            int i10 = i6 - i2;
            int i11 = i8 - i4;
            int i12 = i7 - i3;
            int i13 = i9 - i5;
            Rect rect5 = (Rect) sVar.f421a.get("android:changeBounds:clip");
            final Rect rect6 = (Rect) sVar2.f421a.get("android:changeBounds:clip");
            int i14 = 0;
            if (!((i10 == 0 || i11 == 0) && (i12 == 0 || i13 == 0))) {
                if (!(i2 == i3 && i4 == i5)) {
                    i14 = 1;
                }
                if (!(i6 == i7 && i8 == i9)) {
                    i14++;
                }
            }
            if ((rect5 != null && !rect5.equals(rect6)) || (rect5 == null && rect6 != null)) {
                i14++;
            }
            if (i14 > 0) {
                if (!this.p) {
                    ad.a(view, i2, i4, i6, i8);
                    if (i14 == 2) {
                        if (i10 == i12 && i11 == i13) {
                            a2 = f.a(view, n, l().a((float) i2, (float) i4, (float) i3, (float) i5));
                        } else {
                            final a aVar = new a(view);
                            ObjectAnimator a3 = f.a(aVar, j, l().a((float) i2, (float) i4, (float) i3, (float) i5));
                            ObjectAnimator a4 = f.a(aVar, k, l().a((float) i6, (float) i8, (float) i7, (float) i9));
                            AnimatorSet animatorSet = new AnimatorSet();
                            animatorSet.playTogether(a3, a4);
                            animatorSet.addListener(new AnimatorListenerAdapter() {
                                /* class android.support.transition.c.AnonymousClass8 */
                                private a mViewBounds = aVar;
                            });
                            a2 = animatorSet;
                        }
                    } else if (i2 == i3 && i4 == i5) {
                        a2 = f.a(view, l, l().a((float) i6, (float) i8, (float) i7, (float) i9));
                    } else {
                        a2 = f.a(view, m, l().a((float) i2, (float) i4, (float) i3, (float) i5));
                    }
                } else {
                    ad.a(view, i2, i4, Math.max(i10, i12) + i2, Math.max(i11, i13) + i4);
                    if (i2 == i3 && i4 == i5) {
                        objectAnimator = null;
                    } else {
                        objectAnimator = f.a(view, n, l().a((float) i2, (float) i4, (float) i3, (float) i5));
                    }
                    if (rect5 == null) {
                        rect = new Rect(0, 0, i10, i11);
                    } else {
                        rect = rect5;
                    }
                    if (rect6 == null) {
                        rect2 = new Rect(0, 0, i12, i13);
                    } else {
                        rect2 = rect6;
                    }
                    if (!rect.equals(rect2)) {
                        ViewCompat.setClipBounds(view, rect);
                        ObjectAnimator ofObject = ObjectAnimator.ofObject(view, "clipBounds", r, rect, rect2);
                        ofObject.addListener(new AnimatorListenerAdapter() {
                            /* class android.support.transition.c.AnonymousClass9 */
                            private boolean h;

                            public void onAnimationCancel(Animator animator) {
                                this.h = true;
                            }

                            public void onAnimationEnd(Animator animator) {
                                if (!this.h) {
                                    ViewCompat.setClipBounds(view, rect6);
                                    ad.a(view, i3, i5, i7, i9);
                                }
                            }
                        });
                        objectAnimator2 = ofObject;
                    } else {
                        objectAnimator2 = null;
                    }
                    a2 = q.a(objectAnimator, objectAnimator2);
                }
                if (!(view.getParent() instanceof ViewGroup)) {
                    return a2;
                }
                final ViewGroup viewGroup4 = (ViewGroup) view.getParent();
                x.a(viewGroup4, true);
                a(new m() {
                    /* class android.support.transition.c.AnonymousClass10 */

                    /* renamed from: a  reason: collision with root package name */
                    boolean f397a = false;

                    @Override // android.support.transition.l.c, android.support.transition.m
                    public void a(@NonNull l lVar) {
                        if (!this.f397a) {
                            x.a(viewGroup4, false);
                        }
                        lVar.b(this);
                    }

                    @Override // android.support.transition.l.c, android.support.transition.m
                    public void b(@NonNull l lVar) {
                        x.a(viewGroup4, false);
                    }

                    @Override // android.support.transition.l.c, android.support.transition.m
                    public void c(@NonNull l lVar) {
                        x.a(viewGroup4, true);
                    }
                });
                return a2;
            }
        } else {
            int intValue = ((Integer) sVar.f421a.get("android:changeBounds:windowX")).intValue();
            int intValue2 = ((Integer) sVar.f421a.get("android:changeBounds:windowY")).intValue();
            int intValue3 = ((Integer) sVar2.f421a.get("android:changeBounds:windowX")).intValue();
            int intValue4 = ((Integer) sVar2.f421a.get("android:changeBounds:windowY")).intValue();
            if (!(intValue == intValue3 && intValue2 == intValue4)) {
                viewGroup.getLocationInWindow(this.o);
                Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                view.draw(new Canvas(createBitmap));
                final BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
                final float c = ad.c(view);
                ad.a(view, 0.0f);
                ad.a(viewGroup).a(bitmapDrawable);
                ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(bitmapDrawable, i.a(i, l().a((float) (intValue - this.o[0]), (float) (intValue2 - this.o[1]), (float) (intValue3 - this.o[0]), (float) (intValue4 - this.o[1]))));
                ofPropertyValuesHolder.addListener(new AnimatorListenerAdapter() {
                    /* class android.support.transition.c.AnonymousClass2 */

                    public void onAnimationEnd(Animator animator) {
                        ad.a(viewGroup).b(bitmapDrawable);
                        ad.a(view, c);
                    }
                });
                return ofPropertyValuesHolder;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private int f401a;
        private int b;
        private int c;
        private int d;
        private View e;
        private int f;
        private int g;

        a(View view) {
            this.e = view;
        }

        /* access modifiers changed from: package-private */
        public void a(PointF pointF) {
            this.f401a = Math.round(pointF.x);
            this.b = Math.round(pointF.y);
            this.f++;
            if (this.f == this.g) {
                a();
            }
        }

        /* access modifiers changed from: package-private */
        public void b(PointF pointF) {
            this.c = Math.round(pointF.x);
            this.d = Math.round(pointF.y);
            this.g++;
            if (this.f == this.g) {
                a();
            }
        }

        private void a() {
            ad.a(this.e, this.f401a, this.b, this.c, this.d);
            this.f = 0;
            this.g = 0;
        }
    }
}
