package android.support.transition;

import android.view.View;
import android.view.ViewGroup;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private ViewGroup f410a;
    private Runnable b;

    public void a() {
        if (a(this.f410a) == this && this.b != null) {
            this.b.run();
        }
    }

    static void a(View view, k kVar) {
        view.setTag(R.id.transition_current_scene, kVar);
    }

    static k a(View view) {
        return (k) view.getTag(R.id.transition_current_scene);
    }
}
