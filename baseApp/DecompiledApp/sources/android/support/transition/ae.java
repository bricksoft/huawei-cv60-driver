package android.support.transition;

import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@RequiresApi(19)
class ae extends ah {

    /* renamed from: a  reason: collision with root package name */
    private static Method f388a;
    private static boolean b;
    private static Method c;
    private static boolean d;

    ae() {
    }

    @Override // android.support.transition.ah
    public void a(@NonNull View view, float f) {
        a();
        if (f388a != null) {
            try {
                f388a.invoke(view, Float.valueOf(f));
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        } else {
            view.setAlpha(f);
        }
    }

    @Override // android.support.transition.ah
    public float a(@NonNull View view) {
        b();
        if (c != null) {
            try {
                return ((Float) c.invoke(view, new Object[0])).floatValue();
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
        return super.a(view);
    }

    @Override // android.support.transition.ah
    public void b(@NonNull View view) {
    }

    @Override // android.support.transition.ah
    public void c(@NonNull View view) {
    }

    private void a() {
        if (!b) {
            try {
                f388a = View.class.getDeclaredMethod("setTransitionAlpha", Float.TYPE);
                f388a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi19", "Failed to retrieve setTransitionAlpha method", e);
            }
            b = true;
        }
    }

    private void b() {
        if (!d) {
            try {
                c = View.class.getDeclaredMethod("getTransitionAlpha", new Class[0]);
                c.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi19", "Failed to retrieve getTransitionAlpha method", e);
            }
            d = true;
        }
    }
}
