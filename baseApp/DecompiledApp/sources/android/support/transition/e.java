package android.support.transition;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.transition.l;
import android.support.v4.app.FragmentTransitionImpl;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class e extends FragmentTransitionImpl {
    @Override // android.support.v4.app.FragmentTransitionImpl
    public boolean canHandle(Object obj) {
        return obj instanceof l;
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public Object cloneTransition(Object obj) {
        if (obj != null) {
            return ((l) obj).clone();
        }
        return null;
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public Object wrapTransitionInSet(Object obj) {
        if (obj == null) {
            return null;
        }
        p pVar = new p();
        pVar.a((l) obj);
        return pVar;
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void setSharedElementTargets(Object obj, View view, ArrayList<View> arrayList) {
        p pVar = (p) obj;
        List<View> g = pVar.g();
        g.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            bfsAddViewChildren(g, arrayList.get(i));
        }
        g.add(view);
        arrayList.add(view);
        addTargets(pVar, arrayList);
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void setEpicenter(Object obj, View view) {
        if (view != null) {
            final Rect rect = new Rect();
            getBoundsOnScreen(view, rect);
            ((l) obj).a(new l.b() {
                /* class android.support.transition.e.AnonymousClass1 */
            });
        }
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void addTargets(Object obj, ArrayList<View> arrayList) {
        l lVar = (l) obj;
        if (lVar != null) {
            if (lVar instanceof p) {
                p pVar = (p) lVar;
                int q = pVar.q();
                for (int i = 0; i < q; i++) {
                    addTargets(pVar.b(i), arrayList);
                }
            } else if (!a(lVar) && isNullOrEmpty(lVar.g())) {
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    lVar.b(arrayList.get(i2));
                }
            }
        }
    }

    private static boolean a(l lVar) {
        return !isNullOrEmpty(lVar.f()) || !isNullOrEmpty(lVar.h()) || !isNullOrEmpty(lVar.i());
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public Object mergeTransitionsTogether(Object obj, Object obj2, Object obj3) {
        p pVar = new p();
        if (obj != null) {
            pVar.a((l) obj);
        }
        if (obj2 != null) {
            pVar.a((l) obj2);
        }
        if (obj3 != null) {
            pVar.a((l) obj3);
        }
        return pVar;
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void scheduleHideFragmentView(Object obj, final View view, final ArrayList<View> arrayList) {
        ((l) obj).a(new l.c() {
            /* class android.support.transition.e.AnonymousClass2 */

            @Override // android.support.transition.l.c
            public void d(@NonNull l lVar) {
            }

            @Override // android.support.transition.l.c
            public void a(@NonNull l lVar) {
                lVar.b(this);
                view.setVisibility(8);
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    ((View) arrayList.get(i)).setVisibility(0);
                }
            }

            @Override // android.support.transition.l.c
            public void b(@NonNull l lVar) {
            }

            @Override // android.support.transition.l.c
            public void c(@NonNull l lVar) {
            }
        });
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public Object mergeTransitionsInSequence(Object obj, Object obj2, Object obj3) {
        l lVar = null;
        l lVar2 = (l) obj;
        l lVar3 = (l) obj2;
        l lVar4 = (l) obj3;
        if (lVar2 != null && lVar3 != null) {
            lVar = new p().a(lVar2).a(lVar3).a(1);
        } else if (lVar2 != null) {
            lVar = lVar2;
        } else if (lVar3 != null) {
            lVar = lVar3;
        }
        if (lVar4 == null) {
            return lVar;
        }
        p pVar = new p();
        if (lVar != null) {
            pVar.a(lVar);
        }
        pVar.a(lVar4);
        return pVar;
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void beginDelayedTransition(ViewGroup viewGroup, Object obj) {
        n.a(viewGroup, (l) obj);
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void scheduleRemoveTargets(Object obj, final Object obj2, final ArrayList<View> arrayList, final Object obj3, final ArrayList<View> arrayList2, final Object obj4, final ArrayList<View> arrayList3) {
        ((l) obj).a(new l.c() {
            /* class android.support.transition.e.AnonymousClass3 */

            @Override // android.support.transition.l.c
            public void d(@NonNull l lVar) {
                if (obj2 != null) {
                    e.this.replaceTargets(obj2, arrayList, null);
                }
                if (obj3 != null) {
                    e.this.replaceTargets(obj3, arrayList2, null);
                }
                if (obj4 != null) {
                    e.this.replaceTargets(obj4, arrayList3, null);
                }
            }

            @Override // android.support.transition.l.c
            public void a(@NonNull l lVar) {
            }

            @Override // android.support.transition.l.c
            public void b(@NonNull l lVar) {
            }

            @Override // android.support.transition.l.c
            public void c(@NonNull l lVar) {
            }
        });
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void swapSharedElementTargets(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        p pVar = (p) obj;
        if (pVar != null) {
            pVar.g().clear();
            pVar.g().addAll(arrayList2);
            replaceTargets(pVar, arrayList, arrayList2);
        }
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void replaceTargets(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        l lVar = (l) obj;
        if (lVar instanceof p) {
            p pVar = (p) lVar;
            int q = pVar.q();
            for (int i = 0; i < q; i++) {
                replaceTargets(pVar.b(i), arrayList, arrayList2);
            }
        } else if (!a(lVar)) {
            List<View> g = lVar.g();
            if (g.size() == arrayList.size() && g.containsAll(arrayList)) {
                int size = arrayList2 == null ? 0 : arrayList2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    lVar.b(arrayList2.get(i2));
                }
                for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
                    lVar.c(arrayList.get(size2));
                }
            }
        }
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void addTarget(Object obj, View view) {
        if (obj != null) {
            ((l) obj).b(view);
        }
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void removeTarget(Object obj, View view) {
        if (obj != null) {
            ((l) obj).c(view);
        }
    }

    @Override // android.support.v4.app.FragmentTransitionImpl
    public void setEpicenter(Object obj, final Rect rect) {
        if (obj != null) {
            ((l) obj).a(new l.b() {
                /* class android.support.transition.e.AnonymousClass4 */
            });
        }
    }
}
