package android.support.transition;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;

/* access modifiers changed from: package-private */
@RequiresApi(18)
public class v implements w {

    /* renamed from: a  reason: collision with root package name */
    private final ViewGroupOverlay f423a;

    v(@NonNull ViewGroup viewGroup) {
        this.f423a = viewGroup.getOverlay();
    }

    @Override // android.support.transition.ac
    public void a(@NonNull Drawable drawable) {
        this.f423a.add(drawable);
    }

    @Override // android.support.transition.ac
    public void b(@NonNull Drawable drawable) {
        this.f423a.remove(drawable);
    }

    @Override // android.support.transition.w
    public void a(@NonNull View view) {
        this.f423a.add(view);
    }

    @Override // android.support.transition.w
    public void b(@NonNull View view) {
        this.f423a.remove(view);
    }
}
