package android.support.transition;

import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class s {

    /* renamed from: a  reason: collision with root package name */
    public final Map<String, Object> f421a = new HashMap();
    public View b;
    final ArrayList<l> c = new ArrayList<>();

    public boolean equals(Object obj) {
        if (!(obj instanceof s) || this.b != ((s) obj).b || !this.f421a.equals(((s) obj).f421a)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.b.hashCode() * 31) + this.f421a.hashCode();
    }

    public String toString() {
        String str = (("TransitionValues@" + Integer.toHexString(hashCode()) + ":\n") + "    view = " + this.b + "\n") + "    values:";
        for (String str2 : this.f421a.keySet()) {
            str = str + "    " + str2 + ": " + this.f421a.get(str2) + "\n";
        }
        return str;
    }
}
