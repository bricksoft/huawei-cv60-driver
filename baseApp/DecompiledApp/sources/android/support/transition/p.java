package android.support.transition;

import android.animation.TimeInterpolator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.transition.l;
import android.util.AndroidRuntimeException;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

public class p extends l {
    int h;
    boolean i = false;
    private ArrayList<l> j = new ArrayList<>();
    private boolean k = true;
    private int l = 0;

    @NonNull
    public p a(int i2) {
        switch (i2) {
            case 0:
                this.k = true;
                break;
            case 1:
                this.k = false;
                break;
            default:
                throw new AndroidRuntimeException("Invalid parameter for TransitionSet ordering: " + i2);
        }
        return this;
    }

    @NonNull
    public p a(@NonNull l lVar) {
        this.j.add(lVar);
        lVar.d = this;
        if (this.f411a >= 0) {
            lVar.a(this.f411a);
        }
        if ((this.l & 1) != 0) {
            lVar.a(d());
        }
        if ((this.l & 2) != 0) {
            lVar.a(n());
        }
        if ((this.l & 4) != 0) {
            lVar.a(l());
        }
        if ((this.l & 8) != 0) {
            lVar.a(m());
        }
        return this;
    }

    public int q() {
        return this.j.size();
    }

    public l b(int i2) {
        if (i2 < 0 || i2 >= this.j.size()) {
            return null;
        }
        return this.j.get(i2);
    }

    @NonNull
    /* renamed from: c */
    public p a(long j2) {
        super.a(j2);
        if (this.f411a >= 0) {
            int size = this.j.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.j.get(i2).a(j2);
            }
        }
        return this;
    }

    @NonNull
    /* renamed from: d */
    public p b(long j2) {
        return (p) super.b(j2);
    }

    @NonNull
    /* renamed from: b */
    public p a(@Nullable TimeInterpolator timeInterpolator) {
        this.l |= 1;
        if (this.j != null) {
            int size = this.j.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.j.get(i2).a(timeInterpolator);
            }
        }
        return (p) super.a(timeInterpolator);
    }

    @NonNull
    /* renamed from: f */
    public p b(@NonNull View view) {
        for (int i2 = 0; i2 < this.j.size(); i2++) {
            this.j.get(i2).b(view);
        }
        return (p) super.b(view);
    }

    @NonNull
    /* renamed from: c */
    public p a(@NonNull l.c cVar) {
        return (p) super.a(cVar);
    }

    @NonNull
    /* renamed from: g */
    public p c(@NonNull View view) {
        for (int i2 = 0; i2 < this.j.size(); i2++) {
            this.j.get(i2).c(view);
        }
        return (p) super.c(view);
    }

    @NonNull
    /* renamed from: d */
    public p b(@NonNull l.c cVar) {
        return (p) super.b(cVar);
    }

    @Override // android.support.transition.l
    public void a(g gVar) {
        super.a(gVar);
        this.l |= 4;
        for (int i2 = 0; i2 < this.j.size(); i2++) {
            this.j.get(i2).a(gVar);
        }
    }

    private void r() {
        a aVar = new a(this);
        Iterator<l> it = this.j.iterator();
        while (it.hasNext()) {
            it.next().a(aVar);
        }
        this.h = this.j.size();
    }

    /* access modifiers changed from: package-private */
    public static class a extends m {

        /* renamed from: a  reason: collision with root package name */
        p f419a;

        a(p pVar) {
            this.f419a = pVar;
        }

        @Override // android.support.transition.l.c, android.support.transition.m
        public void d(@NonNull l lVar) {
            if (!this.f419a.i) {
                this.f419a.j();
                this.f419a.i = true;
            }
        }

        @Override // android.support.transition.l.c, android.support.transition.m
        public void a(@NonNull l lVar) {
            p pVar = this.f419a;
            pVar.h--;
            if (this.f419a.h == 0) {
                this.f419a.i = false;
                this.f419a.k();
            }
            lVar.b(this);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.transition.l
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void a(ViewGroup viewGroup, t tVar, t tVar2, ArrayList<s> arrayList, ArrayList<s> arrayList2) {
        long c = c();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            l lVar = this.j.get(i2);
            if (c > 0 && (this.k || i2 == 0)) {
                long c2 = lVar.c();
                if (c2 > 0) {
                    lVar.b(c2 + c);
                } else {
                    lVar.b(c);
                }
            }
            lVar.a(viewGroup, tVar, tVar2, arrayList, arrayList2);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.transition.l
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void e() {
        if (this.j.isEmpty()) {
            j();
            k();
            return;
        }
        r();
        if (!this.k) {
            for (int i2 = 1; i2 < this.j.size(); i2++) {
                final l lVar = this.j.get(i2);
                this.j.get(i2 - 1).a(new m() {
                    /* class android.support.transition.p.AnonymousClass1 */

                    @Override // android.support.transition.l.c, android.support.transition.m
                    public void a(@NonNull l lVar) {
                        lVar.e();
                        lVar.b(this);
                    }
                });
            }
            l lVar2 = this.j.get(0);
            if (lVar2 != null) {
                lVar2.e();
                return;
            }
            return;
        }
        Iterator<l> it = this.j.iterator();
        while (it.hasNext()) {
            it.next().e();
        }
    }

    @Override // android.support.transition.l
    public void a(@NonNull s sVar) {
        if (a(sVar.b)) {
            Iterator<l> it = this.j.iterator();
            while (it.hasNext()) {
                l next = it.next();
                if (next.a(sVar.b)) {
                    next.a(sVar);
                    sVar.c.add(next);
                }
            }
        }
    }

    @Override // android.support.transition.l
    public void b(@NonNull s sVar) {
        if (a(sVar.b)) {
            Iterator<l> it = this.j.iterator();
            while (it.hasNext()) {
                l next = it.next();
                if (next.a(sVar.b)) {
                    next.b(sVar);
                    sVar.c.add(next);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.transition.l
    public void c(s sVar) {
        super.c(sVar);
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.j.get(i2).c(sVar);
        }
    }

    @Override // android.support.transition.l
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void d(View view) {
        super.d(view);
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.j.get(i2).d(view);
        }
    }

    @Override // android.support.transition.l
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void e(View view) {
        super.e(view);
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.j.get(i2).e(view);
        }
    }

    @Override // android.support.transition.l
    public void a(o oVar) {
        super.a(oVar);
        this.l |= 2;
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.j.get(i2).a(oVar);
        }
    }

    @Override // android.support.transition.l
    public void a(l.b bVar) {
        super.a(bVar);
        this.l |= 8;
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.j.get(i2).a(bVar);
        }
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.transition.l
    public String a(String str) {
        String a2 = super.a(str);
        for (int i2 = 0; i2 < this.j.size(); i2++) {
            a2 = a2 + "\n" + this.j.get(i2).a(str + "  ");
        }
        return a2;
    }

    @Override // android.support.transition.l
    /* renamed from: o */
    public l clone() {
        p pVar = (p) super.clone();
        pVar.j = new ArrayList<>();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            pVar.a(this.j.get(i2).clone());
        }
        return pVar;
    }
}
