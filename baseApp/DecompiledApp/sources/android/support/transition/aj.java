package android.support.transition;

import android.os.IBinder;

/* access modifiers changed from: package-private */
public class aj implements al {

    /* renamed from: a  reason: collision with root package name */
    private final IBinder f394a;

    aj(IBinder iBinder) {
        this.f394a = iBinder;
    }

    public boolean equals(Object obj) {
        return (obj instanceof aj) && ((aj) obj).f394a.equals(this.f394a);
    }

    public int hashCode() {
        return this.f394a.hashCode();
    }
}
