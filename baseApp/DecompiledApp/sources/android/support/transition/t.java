package android.support.transition;

import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.util.SparseArray;
import android.view.View;

/* access modifiers changed from: package-private */
public class t {

    /* renamed from: a  reason: collision with root package name */
    final ArrayMap<View, s> f422a = new ArrayMap<>();
    final SparseArray<View> b = new SparseArray<>();
    final LongSparseArray<View> c = new LongSparseArray<>();
    final ArrayMap<String, View> d = new ArrayMap<>();

    t() {
    }
}
