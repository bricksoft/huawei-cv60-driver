package android.support.transition;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

/* access modifiers changed from: package-private */
public class u extends aa implements w {
    u(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    static u a(ViewGroup viewGroup) {
        return (u) aa.d(viewGroup);
    }

    @Override // android.support.transition.w
    public void a(@NonNull View view) {
        this.f384a.a(view);
    }

    @Override // android.support.transition.w
    public void b(@NonNull View view) {
        this.f384a.b(view);
    }
}
