package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;

public class d extends ai {
    public d(int i) {
        a(i);
    }

    public d() {
    }

    @Override // android.support.transition.ai, android.support.transition.l
    public void a(@NonNull s sVar) {
        super.a(sVar);
        sVar.f421a.put("android:fade:transitionAlpha", Float.valueOf(ad.c(sVar.b)));
    }

    private Animator a(final View view, float f, float f2) {
        if (f == f2) {
            return null;
        }
        ad.a(view, f);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, ad.f387a, f2);
        ofFloat.addListener(new a(view));
        a(new m() {
            /* class android.support.transition.d.AnonymousClass1 */

            @Override // android.support.transition.l.c, android.support.transition.m
            public void a(@NonNull l lVar) {
                ad.a(view, 1.0f);
                ad.e(view);
                lVar.b(this);
            }
        });
        return ofFloat;
    }

    @Override // android.support.transition.ai
    public Animator a(ViewGroup viewGroup, View view, s sVar, s sVar2) {
        float f = 0.0f;
        float a2 = a(sVar, 0.0f);
        if (a2 != 1.0f) {
            f = a2;
        }
        return a(view, f, 1.0f);
    }

    @Override // android.support.transition.ai
    public Animator b(ViewGroup viewGroup, View view, s sVar, s sVar2) {
        ad.d(view);
        return a(view, a(sVar, 1.0f), 0.0f);
    }

    private static float a(s sVar, float f) {
        Float f2;
        if (sVar == null || (f2 = (Float) sVar.f421a.get("android:fade:transitionAlpha")) == null) {
            return f;
        }
        return f2.floatValue();
    }

    /* access modifiers changed from: private */
    public static class a extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        private final View f403a;
        private boolean b = false;

        a(View view) {
            this.f403a = view;
        }

        public void onAnimationStart(Animator animator) {
            if (ViewCompat.hasOverlappingRendering(this.f403a) && this.f403a.getLayerType() == 0) {
                this.b = true;
                this.f403a.setLayerType(2, null);
            }
        }

        public void onAnimationEnd(Animator animator) {
            ad.a(this.f403a, 1.0f);
            if (this.b) {
                this.f403a.setLayerType(0, null);
            }
        }
    }
}
