package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.graphics.Path;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.support.v4.view.ViewCompat;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class l implements Cloneable {
    private static ThreadLocal<ArrayMap<Animator, a>> A = new ThreadLocal<>();
    private static final int[] h = {2, 1, 3, 4};
    private static final g i = new g() {
        /* class android.support.transition.l.AnonymousClass1 */

        @Override // android.support.transition.g
        public Path a(float f, float f2, float f3, float f4) {
            Path path = new Path();
            path.moveTo(f, f2);
            path.lineTo(f3, f4);
            return path;
        }
    };
    private ViewGroup B = null;
    private int C = 0;
    private boolean D = false;
    private boolean E = false;
    private ArrayList<c> F = null;
    private ArrayList<Animator> G = new ArrayList<>();
    private b H;
    private ArrayMap<String, String> I;
    private g J = i;

    /* renamed from: a  reason: collision with root package name */
    long f411a = -1;
    ArrayList<Integer> b = new ArrayList<>();
    ArrayList<View> c = new ArrayList<>();
    p d = null;
    boolean e = false;
    ArrayList<Animator> f = new ArrayList<>();
    o g;
    private String j = getClass().getName();
    private long k = -1;
    private TimeInterpolator l = null;
    private ArrayList<String> m = null;
    private ArrayList<Class> n = null;
    private ArrayList<Integer> o = null;
    private ArrayList<View> p = null;
    private ArrayList<Class> q = null;
    private ArrayList<String> r = null;
    private ArrayList<Integer> s = null;
    private ArrayList<View> t = null;
    private ArrayList<Class> u = null;
    private t v = new t();
    private t w = new t();
    private int[] x = h;
    private ArrayList<s> y;
    private ArrayList<s> z;

    public static abstract class b {
    }

    public interface c {
        void a(@NonNull l lVar);

        void b(@NonNull l lVar);

        void c(@NonNull l lVar);

        void d(@NonNull l lVar);
    }

    public abstract void a(@NonNull s sVar);

    public abstract void b(@NonNull s sVar);

    @NonNull
    public l a(long j2) {
        this.f411a = j2;
        return this;
    }

    public long b() {
        return this.f411a;
    }

    @NonNull
    public l b(long j2) {
        this.k = j2;
        return this;
    }

    public long c() {
        return this.k;
    }

    @NonNull
    public l a(@Nullable TimeInterpolator timeInterpolator) {
        this.l = timeInterpolator;
        return this;
    }

    @Nullable
    public TimeInterpolator d() {
        return this.l;
    }

    @Nullable
    public String[] a() {
        return null;
    }

    @Nullable
    public Animator a(@NonNull ViewGroup viewGroup, @Nullable s sVar, @Nullable s sVar2) {
        return null;
    }

    private void a(ArrayMap<View, s> arrayMap, ArrayMap<View, s> arrayMap2) {
        s remove;
        for (int size = arrayMap.size() - 1; size >= 0; size--) {
            View keyAt = arrayMap.keyAt(size);
            if (!(keyAt == null || !a(keyAt) || (remove = arrayMap2.remove(keyAt)) == null || remove.b == null || !a(remove.b))) {
                this.y.add(arrayMap.removeAt(size));
                this.z.add(remove);
            }
        }
    }

    private void a(ArrayMap<View, s> arrayMap, ArrayMap<View, s> arrayMap2, LongSparseArray<View> longSparseArray, LongSparseArray<View> longSparseArray2) {
        View view;
        int size = longSparseArray.size();
        for (int i2 = 0; i2 < size; i2++) {
            View valueAt = longSparseArray.valueAt(i2);
            if (valueAt != null && a(valueAt) && (view = longSparseArray2.get(longSparseArray.keyAt(i2))) != null && a(view)) {
                s sVar = arrayMap.get(valueAt);
                s sVar2 = arrayMap2.get(view);
                if (!(sVar == null || sVar2 == null)) {
                    this.y.add(sVar);
                    this.z.add(sVar2);
                    arrayMap.remove(valueAt);
                    arrayMap2.remove(view);
                }
            }
        }
    }

    private void a(ArrayMap<View, s> arrayMap, ArrayMap<View, s> arrayMap2, SparseArray<View> sparseArray, SparseArray<View> sparseArray2) {
        View view;
        int size = sparseArray.size();
        for (int i2 = 0; i2 < size; i2++) {
            View valueAt = sparseArray.valueAt(i2);
            if (valueAt != null && a(valueAt) && (view = sparseArray2.get(sparseArray.keyAt(i2))) != null && a(view)) {
                s sVar = arrayMap.get(valueAt);
                s sVar2 = arrayMap2.get(view);
                if (!(sVar == null || sVar2 == null)) {
                    this.y.add(sVar);
                    this.z.add(sVar2);
                    arrayMap.remove(valueAt);
                    arrayMap2.remove(view);
                }
            }
        }
    }

    private void a(ArrayMap<View, s> arrayMap, ArrayMap<View, s> arrayMap2, ArrayMap<String, View> arrayMap3, ArrayMap<String, View> arrayMap4) {
        View view;
        int size = arrayMap3.size();
        for (int i2 = 0; i2 < size; i2++) {
            View valueAt = arrayMap3.valueAt(i2);
            if (valueAt != null && a(valueAt) && (view = arrayMap4.get(arrayMap3.keyAt(i2))) != null && a(view)) {
                s sVar = arrayMap.get(valueAt);
                s sVar2 = arrayMap2.get(view);
                if (!(sVar == null || sVar2 == null)) {
                    this.y.add(sVar);
                    this.z.add(sVar2);
                    arrayMap.remove(valueAt);
                    arrayMap2.remove(view);
                }
            }
        }
    }

    private void b(ArrayMap<View, s> arrayMap, ArrayMap<View, s> arrayMap2) {
        for (int i2 = 0; i2 < arrayMap.size(); i2++) {
            s valueAt = arrayMap.valueAt(i2);
            if (a(valueAt.b)) {
                this.y.add(valueAt);
                this.z.add(null);
            }
        }
        for (int i3 = 0; i3 < arrayMap2.size(); i3++) {
            s valueAt2 = arrayMap2.valueAt(i3);
            if (a(valueAt2.b)) {
                this.z.add(valueAt2);
                this.y.add(null);
            }
        }
    }

    private void a(t tVar, t tVar2) {
        ArrayMap<View, s> arrayMap = new ArrayMap<>(tVar.f422a);
        ArrayMap<View, s> arrayMap2 = new ArrayMap<>(tVar2.f422a);
        for (int i2 = 0; i2 < this.x.length; i2++) {
            switch (this.x[i2]) {
                case 1:
                    a(arrayMap, arrayMap2);
                    break;
                case 2:
                    a(arrayMap, arrayMap2, tVar.d, tVar2.d);
                    break;
                case 3:
                    a(arrayMap, arrayMap2, tVar.b, tVar2.b);
                    break;
                case 4:
                    a(arrayMap, arrayMap2, tVar.c, tVar2.c);
                    break;
            }
        }
        b(arrayMap, arrayMap2);
    }

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void a(ViewGroup viewGroup, t tVar, t tVar2, ArrayList<s> arrayList, ArrayList<s> arrayList2) {
        Animator a2;
        View view;
        s sVar;
        Animator animator;
        ArrayMap<Animator, a> q2 = q();
        long j2 = Long.MAX_VALUE;
        SparseIntArray sparseIntArray = new SparseIntArray();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            s sVar2 = arrayList.get(i2);
            s sVar3 = arrayList2.get(i2);
            s sVar4 = (sVar2 == null || sVar2.c.contains(this)) ? sVar2 : null;
            s sVar5 = (sVar3 == null || sVar3.c.contains(this)) ? sVar3 : null;
            if (sVar4 != null || sVar5 != null) {
                if ((sVar4 == null || sVar5 == null || a(sVar4, sVar5)) && (a2 = a(viewGroup, sVar4, sVar5)) != null) {
                    s sVar6 = null;
                    if (sVar5 != null) {
                        View view2 = sVar5.b;
                        String[] a3 = a();
                        if (view2 != null && a3 != null && a3.length > 0) {
                            s sVar7 = new s();
                            sVar7.b = view2;
                            s sVar8 = tVar2.f422a.get(view2);
                            if (sVar8 != null) {
                                for (int i3 = 0; i3 < a3.length; i3++) {
                                    sVar7.f421a.put(a3[i3], sVar8.f421a.get(a3[i3]));
                                }
                            }
                            int size2 = q2.size();
                            int i4 = 0;
                            while (true) {
                                if (i4 >= size2) {
                                    sVar6 = sVar7;
                                    break;
                                }
                                a aVar = q2.get(q2.keyAt(i4));
                                if (aVar.c != null && aVar.f414a == view2 && aVar.b.equals(p()) && aVar.c.equals(sVar7)) {
                                    sVar6 = sVar7;
                                    a2 = null;
                                    break;
                                }
                                i4++;
                            }
                        }
                        sVar = sVar6;
                        view = view2;
                        animator = a2;
                    } else {
                        view = sVar4.b;
                        sVar = null;
                        animator = a2;
                    }
                    if (animator != null) {
                        if (this.g != null) {
                            long a4 = this.g.a(viewGroup, this, sVar4, sVar5);
                            sparseIntArray.put(this.G.size(), (int) a4);
                            j2 = Math.min(a4, j2);
                        }
                        q2.put(animator, new a(view, p(), this, ad.b(viewGroup), sVar));
                        this.G.add(animator);
                    }
                }
            }
        }
        if (j2 != 0) {
            for (int i5 = 0; i5 < sparseIntArray.size(); i5++) {
                Animator animator2 = this.G.get(sparseIntArray.keyAt(i5));
                animator2.setStartDelay((((long) sparseIntArray.valueAt(i5)) - j2) + animator2.getStartDelay());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view) {
        int id = view.getId();
        if (this.o != null && this.o.contains(Integer.valueOf(id))) {
            return false;
        }
        if (this.p != null && this.p.contains(view)) {
            return false;
        }
        if (this.q != null) {
            int size = this.q.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.q.get(i2).isInstance(view)) {
                    return false;
                }
            }
        }
        if (!(this.r == null || ViewCompat.getTransitionName(view) == null || !this.r.contains(ViewCompat.getTransitionName(view)))) {
            return false;
        }
        if (this.b.size() == 0 && this.c.size() == 0 && ((this.n == null || this.n.isEmpty()) && (this.m == null || this.m.isEmpty()))) {
            return true;
        }
        if (this.b.contains(Integer.valueOf(id)) || this.c.contains(view)) {
            return true;
        }
        if (this.m != null && this.m.contains(ViewCompat.getTransitionName(view))) {
            return true;
        }
        if (this.n == null) {
            return false;
        }
        for (int i3 = 0; i3 < this.n.size(); i3++) {
            if (this.n.get(i3).isInstance(view)) {
                return true;
            }
        }
        return false;
    }

    private static ArrayMap<Animator, a> q() {
        ArrayMap<Animator, a> arrayMap = A.get();
        if (arrayMap != null) {
            return arrayMap;
        }
        ArrayMap<Animator, a> arrayMap2 = new ArrayMap<>();
        A.set(arrayMap2);
        return arrayMap2;
    }

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void e() {
        j();
        ArrayMap<Animator, a> q2 = q();
        Iterator<Animator> it = this.G.iterator();
        while (it.hasNext()) {
            Animator next = it.next();
            if (q2.containsKey(next)) {
                j();
                a(next, q2);
            }
        }
        this.G.clear();
        k();
    }

    private void a(Animator animator, final ArrayMap<Animator, a> arrayMap) {
        if (animator != null) {
            animator.addListener(new AnimatorListenerAdapter() {
                /* class android.support.transition.l.AnonymousClass2 */

                public void onAnimationStart(Animator animator) {
                    l.this.f.add(animator);
                }

                public void onAnimationEnd(Animator animator) {
                    arrayMap.remove(animator);
                    l.this.f.remove(animator);
                }
            });
            a(animator);
        }
    }

    @NonNull
    public l b(@NonNull View view) {
        this.c.add(view);
        return this;
    }

    @NonNull
    public l c(@NonNull View view) {
        this.c.remove(view);
        return this;
    }

    @NonNull
    public List<Integer> f() {
        return this.b;
    }

    @NonNull
    public List<View> g() {
        return this.c;
    }

    @Nullable
    public List<String> h() {
        return this.m;
    }

    @Nullable
    public List<Class> i() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup, boolean z2) {
        a(z2);
        if ((this.b.size() > 0 || this.c.size() > 0) && ((this.m == null || this.m.isEmpty()) && (this.n == null || this.n.isEmpty()))) {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                View findViewById = viewGroup.findViewById(this.b.get(i2).intValue());
                if (findViewById != null) {
                    s sVar = new s();
                    sVar.b = findViewById;
                    if (z2) {
                        a(sVar);
                    } else {
                        b(sVar);
                    }
                    sVar.c.add(this);
                    c(sVar);
                    if (z2) {
                        a(this.v, findViewById, sVar);
                    } else {
                        a(this.w, findViewById, sVar);
                    }
                }
            }
            for (int i3 = 0; i3 < this.c.size(); i3++) {
                View view = this.c.get(i3);
                s sVar2 = new s();
                sVar2.b = view;
                if (z2) {
                    a(sVar2);
                } else {
                    b(sVar2);
                }
                sVar2.c.add(this);
                c(sVar2);
                if (z2) {
                    a(this.v, view, sVar2);
                } else {
                    a(this.w, view, sVar2);
                }
            }
        } else {
            c(viewGroup, z2);
        }
        if (!(z2 || this.I == null)) {
            int size = this.I.size();
            ArrayList arrayList = new ArrayList(size);
            for (int i4 = 0; i4 < size; i4++) {
                arrayList.add(this.v.d.remove(this.I.keyAt(i4)));
            }
            for (int i5 = 0; i5 < size; i5++) {
                View view2 = (View) arrayList.get(i5);
                if (view2 != null) {
                    this.v.d.put(this.I.valueAt(i5), view2);
                }
            }
        }
    }

    private static void a(t tVar, View view, s sVar) {
        tVar.f422a.put(view, sVar);
        int id = view.getId();
        if (id >= 0) {
            if (tVar.b.indexOfKey(id) >= 0) {
                tVar.b.put(id, null);
            } else {
                tVar.b.put(id, view);
            }
        }
        String transitionName = ViewCompat.getTransitionName(view);
        if (transitionName != null) {
            if (tVar.d.containsKey(transitionName)) {
                tVar.d.put(transitionName, null);
            } else {
                tVar.d.put(transitionName, view);
            }
        }
        if (view.getParent() instanceof ListView) {
            ListView listView = (ListView) view.getParent();
            if (listView.getAdapter().hasStableIds()) {
                long itemIdAtPosition = listView.getItemIdAtPosition(listView.getPositionForView(view));
                if (tVar.c.indexOfKey(itemIdAtPosition) >= 0) {
                    View view2 = tVar.c.get(itemIdAtPosition);
                    if (view2 != null) {
                        ViewCompat.setHasTransientState(view2, false);
                        tVar.c.put(itemIdAtPosition, null);
                        return;
                    }
                    return;
                }
                ViewCompat.setHasTransientState(view, true);
                tVar.c.put(itemIdAtPosition, view);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        if (z2) {
            this.v.f422a.clear();
            this.v.b.clear();
            this.v.c.clear();
            return;
        }
        this.w.f422a.clear();
        this.w.b.clear();
        this.w.c.clear();
    }

    private void c(View view, boolean z2) {
        if (view != null) {
            int id = view.getId();
            if (this.o != null && this.o.contains(Integer.valueOf(id))) {
                return;
            }
            if (this.p == null || !this.p.contains(view)) {
                if (this.q != null) {
                    int size = this.q.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        if (this.q.get(i2).isInstance(view)) {
                            return;
                        }
                    }
                }
                if (view.getParent() instanceof ViewGroup) {
                    s sVar = new s();
                    sVar.b = view;
                    if (z2) {
                        a(sVar);
                    } else {
                        b(sVar);
                    }
                    sVar.c.add(this);
                    c(sVar);
                    if (z2) {
                        a(this.v, view, sVar);
                    } else {
                        a(this.w, view, sVar);
                    }
                }
                if (!(view instanceof ViewGroup)) {
                    return;
                }
                if (this.s != null && this.s.contains(Integer.valueOf(id))) {
                    return;
                }
                if (this.t == null || !this.t.contains(view)) {
                    if (this.u != null) {
                        int size2 = this.u.size();
                        for (int i3 = 0; i3 < size2; i3++) {
                            if (this.u.get(i3).isInstance(view)) {
                                return;
                            }
                        }
                    }
                    ViewGroup viewGroup = (ViewGroup) view;
                    for (int i4 = 0; i4 < viewGroup.getChildCount(); i4++) {
                        c(viewGroup.getChildAt(i4), z2);
                    }
                }
            }
        }
    }

    @Nullable
    public s a(@NonNull View view, boolean z2) {
        if (this.d != null) {
            return this.d.a(view, z2);
        }
        return (z2 ? this.v : this.w).f422a.get(view);
    }

    /* access modifiers changed from: package-private */
    public s b(View view, boolean z2) {
        s sVar;
        if (this.d != null) {
            return this.d.b(view, z2);
        }
        ArrayList<s> arrayList = z2 ? this.y : this.z;
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                i2 = -1;
                break;
            }
            s sVar2 = arrayList.get(i2);
            if (sVar2 == null) {
                return null;
            }
            if (sVar2.b == view) {
                break;
            }
            i2++;
        }
        if (i2 >= 0) {
            sVar = (z2 ? this.z : this.y).get(i2);
        } else {
            sVar = null;
        }
        return sVar;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void d(View view) {
        if (!this.E) {
            ArrayMap<Animator, a> q2 = q();
            int size = q2.size();
            al b2 = ad.b(view);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                a valueAt = q2.valueAt(i2);
                if (valueAt.f414a != null && b2.equals(valueAt.d)) {
                    a.a(q2.keyAt(i2));
                }
            }
            if (this.F != null && this.F.size() > 0) {
                ArrayList arrayList = (ArrayList) this.F.clone();
                int size2 = arrayList.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    ((c) arrayList.get(i3)).b(this);
                }
            }
            this.D = true;
        }
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void e(View view) {
        if (this.D) {
            if (!this.E) {
                ArrayMap<Animator, a> q2 = q();
                int size = q2.size();
                al b2 = ad.b(view);
                for (int i2 = size - 1; i2 >= 0; i2--) {
                    a valueAt = q2.valueAt(i2);
                    if (valueAt.f414a != null && b2.equals(valueAt.d)) {
                        a.b(q2.keyAt(i2));
                    }
                }
                if (this.F != null && this.F.size() > 0) {
                    ArrayList arrayList = (ArrayList) this.F.clone();
                    int size2 = arrayList.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        ((c) arrayList.get(i3)).c(this);
                    }
                }
            }
            this.D = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup) {
        a aVar;
        this.y = new ArrayList<>();
        this.z = new ArrayList<>();
        a(this.v, this.w);
        ArrayMap<Animator, a> q2 = q();
        int size = q2.size();
        al b2 = ad.b(viewGroup);
        for (int i2 = size - 1; i2 >= 0; i2--) {
            Animator keyAt = q2.keyAt(i2);
            if (!(keyAt == null || (aVar = q2.get(keyAt)) == null || aVar.f414a == null || !b2.equals(aVar.d))) {
                s sVar = aVar.c;
                View view = aVar.f414a;
                s a2 = a(view, true);
                s b3 = b(view, true);
                if (!(a2 == null && b3 == null) && aVar.e.a(sVar, b3)) {
                    if (keyAt.isRunning() || keyAt.isStarted()) {
                        keyAt.cancel();
                    } else {
                        q2.remove(keyAt);
                    }
                }
            }
        }
        a(viewGroup, this.v, this.w, this.y, this.z);
        e();
    }

    public boolean a(@Nullable s sVar, @Nullable s sVar2) {
        boolean z2;
        if (sVar == null || sVar2 == null) {
            return false;
        }
        String[] a2 = a();
        if (a2 != null) {
            int length = a2.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z2 = false;
                    break;
                } else if (a(sVar, sVar2, a2[i2])) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            return z2;
        }
        for (String str : sVar.f421a.keySet()) {
            if (a(sVar, sVar2, str)) {
                return true;
            }
        }
        return false;
    }

    private static boolean a(s sVar, s sVar2, String str) {
        Object obj = sVar.f421a.get(str);
        Object obj2 = sVar2.f421a.get(str);
        if (obj == null && obj2 == null) {
            return false;
        }
        return obj == null || obj2 == null || !obj.equals(obj2);
    }

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void a(Animator animator) {
        if (animator == null) {
            k();
            return;
        }
        if (b() >= 0) {
            animator.setDuration(b());
        }
        if (c() >= 0) {
            animator.setStartDelay(c());
        }
        if (d() != null) {
            animator.setInterpolator(d());
        }
        animator.addListener(new AnimatorListenerAdapter() {
            /* class android.support.transition.l.AnonymousClass3 */

            public void onAnimationEnd(Animator animator) {
                l.this.k();
                animator.removeListener(this);
            }
        });
        animator.start();
    }

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void j() {
        if (this.C == 0) {
            if (this.F != null && this.F.size() > 0) {
                ArrayList arrayList = (ArrayList) this.F.clone();
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((c) arrayList.get(i2)).d(this);
                }
            }
            this.E = false;
        }
        this.C++;
    }

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void k() {
        this.C--;
        if (this.C == 0) {
            if (this.F != null && this.F.size() > 0) {
                ArrayList arrayList = (ArrayList) this.F.clone();
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((c) arrayList.get(i2)).a(this);
                }
            }
            for (int i3 = 0; i3 < this.v.c.size(); i3++) {
                View valueAt = this.v.c.valueAt(i3);
                if (valueAt != null) {
                    ViewCompat.setHasTransientState(valueAt, false);
                }
            }
            for (int i4 = 0; i4 < this.w.c.size(); i4++) {
                View valueAt2 = this.w.c.valueAt(i4);
                if (valueAt2 != null) {
                    ViewCompat.setHasTransientState(valueAt2, false);
                }
            }
            this.E = true;
        }
    }

    @NonNull
    public l a(@NonNull c cVar) {
        if (this.F == null) {
            this.F = new ArrayList<>();
        }
        this.F.add(cVar);
        return this;
    }

    @NonNull
    public l b(@NonNull c cVar) {
        if (this.F != null) {
            this.F.remove(cVar);
            if (this.F.size() == 0) {
                this.F = null;
            }
        }
        return this;
    }

    public void a(@Nullable g gVar) {
        if (gVar == null) {
            this.J = i;
        } else {
            this.J = gVar;
        }
    }

    @NonNull
    public g l() {
        return this.J;
    }

    public void a(@Nullable b bVar) {
        this.H = bVar;
    }

    @Nullable
    public b m() {
        return this.H;
    }

    public void a(@Nullable o oVar) {
        this.g = oVar;
    }

    @Nullable
    public o n() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public void c(s sVar) {
        String[] a2;
        boolean z2 = false;
        if (this.g != null && !sVar.f421a.isEmpty() && (a2 = this.g.a()) != null) {
            int i2 = 0;
            while (true) {
                if (i2 >= a2.length) {
                    z2 = true;
                    break;
                } else if (!sVar.f421a.containsKey(a2[i2])) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z2) {
                this.g.a(sVar);
            }
        }
    }

    public String toString() {
        return a("");
    }

    /* renamed from: o */
    public l clone() {
        try {
            l lVar = (l) super.clone();
            lVar.G = new ArrayList<>();
            lVar.v = new t();
            lVar.w = new t();
            lVar.y = null;
            lVar.z = null;
            return lVar;
        } catch (CloneNotSupportedException e2) {
            return null;
        }
    }

    @NonNull
    public String p() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        String str2 = str + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ": ";
        if (this.f411a != -1) {
            str2 = str2 + "dur(" + this.f411a + ") ";
        }
        if (this.k != -1) {
            str2 = str2 + "dly(" + this.k + ") ";
        }
        if (this.l != null) {
            str2 = str2 + "interp(" + this.l + ") ";
        }
        if (this.b.size() <= 0 && this.c.size() <= 0) {
            return str2;
        }
        String str3 = str2 + "tgts(";
        if (this.b.size() > 0) {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                if (i2 > 0) {
                    str3 = str3 + ", ";
                }
                str3 = str3 + this.b.get(i2);
            }
        }
        if (this.c.size() > 0) {
            for (int i3 = 0; i3 < this.c.size(); i3++) {
                if (i3 > 0) {
                    str3 = str3 + ", ";
                }
                str3 = str3 + this.c.get(i3);
            }
        }
        return str3 + ")";
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        View f414a;
        String b;
        s c;
        al d;
        l e;

        a(View view, String str, l lVar, al alVar, s sVar) {
            this.f414a = view;
            this.b = str;
            this.c = sVar;
            this.d = alVar;
            this.e = lVar;
        }
    }
}
