package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.a;
import android.support.transition.l;
import android.view.View;
import android.view.ViewGroup;

public abstract class ai extends l {
    private static final String[] h = {"android:visibility:visibility", "android:visibility:parent"};
    private int i = 3;

    /* access modifiers changed from: private */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        boolean f393a;
        boolean b;
        int c;
        int d;
        ViewGroup e;
        ViewGroup f;

        b() {
        }
    }

    public void a(int i2) {
        if ((i2 & -4) != 0) {
            throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
        }
        this.i = i2;
    }

    @Override // android.support.transition.l
    @Nullable
    public String[] a() {
        return h;
    }

    private void d(s sVar) {
        sVar.f421a.put("android:visibility:visibility", Integer.valueOf(sVar.b.getVisibility()));
        sVar.f421a.put("android:visibility:parent", sVar.b.getParent());
        int[] iArr = new int[2];
        sVar.b.getLocationOnScreen(iArr);
        sVar.f421a.put("android:visibility:screenLocation", iArr);
    }

    @Override // android.support.transition.l
    public void a(@NonNull s sVar) {
        d(sVar);
    }

    @Override // android.support.transition.l
    public void b(@NonNull s sVar) {
        d(sVar);
    }

    private b b(s sVar, s sVar2) {
        b bVar = new b();
        bVar.f393a = false;
        bVar.b = false;
        if (sVar == null || !sVar.f421a.containsKey("android:visibility:visibility")) {
            bVar.c = -1;
            bVar.e = null;
        } else {
            bVar.c = ((Integer) sVar.f421a.get("android:visibility:visibility")).intValue();
            bVar.e = (ViewGroup) sVar.f421a.get("android:visibility:parent");
        }
        if (sVar2 == null || !sVar2.f421a.containsKey("android:visibility:visibility")) {
            bVar.d = -1;
            bVar.f = null;
        } else {
            bVar.d = ((Integer) sVar2.f421a.get("android:visibility:visibility")).intValue();
            bVar.f = (ViewGroup) sVar2.f421a.get("android:visibility:parent");
        }
        if (sVar == null || sVar2 == null) {
            if (sVar == null && bVar.d == 0) {
                bVar.b = true;
                bVar.f393a = true;
            } else if (sVar2 == null && bVar.c == 0) {
                bVar.b = false;
                bVar.f393a = true;
            }
        } else if (bVar.c == bVar.d && bVar.e == bVar.f) {
            return bVar;
        } else {
            if (bVar.c != bVar.d) {
                if (bVar.c == 0) {
                    bVar.b = false;
                    bVar.f393a = true;
                } else if (bVar.d == 0) {
                    bVar.b = true;
                    bVar.f393a = true;
                }
            } else if (bVar.f == null) {
                bVar.b = false;
                bVar.f393a = true;
            } else if (bVar.e == null) {
                bVar.b = true;
                bVar.f393a = true;
            }
        }
        return bVar;
    }

    @Override // android.support.transition.l
    @Nullable
    public Animator a(@NonNull ViewGroup viewGroup, @Nullable s sVar, @Nullable s sVar2) {
        b b2 = b(sVar, sVar2);
        if (!b2.f393a || (b2.e == null && b2.f == null)) {
            return null;
        }
        if (b2.b) {
            return a(viewGroup, sVar, b2.c, sVar2, b2.d);
        }
        return b(viewGroup, sVar, b2.c, sVar2, b2.d);
    }

    public Animator a(ViewGroup viewGroup, s sVar, int i2, s sVar2, int i3) {
        if ((this.i & 1) != 1 || sVar2 == null) {
            return null;
        }
        if (sVar == null) {
            View view = (View) sVar2.b.getParent();
            if (b(b(view, false), a(view, false)).f393a) {
                return null;
            }
        }
        return a(viewGroup, sVar2.b, sVar, sVar2);
    }

    public Animator a(ViewGroup viewGroup, View view, s sVar, s sVar2) {
        return null;
    }

    public Animator b(ViewGroup viewGroup, s sVar, int i2, s sVar2, int i3) {
        View view;
        View view2;
        View view3;
        int id;
        Animator animator = null;
        if ((this.i & 2) == 2) {
            final View view4 = sVar != null ? sVar.b : null;
            if (sVar2 != null) {
                view = sVar2.b;
            } else {
                view = null;
            }
            if (view == null || view.getParent() == null) {
                if (view != null) {
                    view2 = null;
                    view4 = view;
                } else {
                    if (view4 != null) {
                        if (view4.getParent() == null) {
                            view2 = null;
                        } else if (view4.getParent() instanceof View) {
                            View view5 = (View) view4.getParent();
                            if (!b(a(view5, true), b(view5, true)).f393a) {
                                view3 = q.a(viewGroup, view4, view5);
                            } else if (view5.getParent() != null || (id = view5.getId()) == -1 || viewGroup.findViewById(id) == null || !this.e) {
                                view3 = null;
                            } else {
                                view3 = view4;
                            }
                            view2 = null;
                            view4 = view3;
                        }
                    }
                    view2 = null;
                    view4 = null;
                }
            } else if (i3 == 4) {
                view2 = view;
                view4 = null;
            } else if (view4 == view) {
                view2 = view;
                view4 = null;
            } else if (this.e) {
                view2 = null;
            } else {
                view4 = q.a(viewGroup, view4, (View) view4.getParent());
                view2 = null;
            }
            if (view4 != null && sVar != null) {
                int[] iArr = (int[]) sVar.f421a.get("android:visibility:screenLocation");
                int i4 = iArr[0];
                int i5 = iArr[1];
                int[] iArr2 = new int[2];
                viewGroup.getLocationOnScreen(iArr2);
                view4.offsetLeftAndRight((i4 - iArr2[0]) - view4.getLeft());
                view4.offsetTopAndBottom((i5 - iArr2[1]) - view4.getTop());
                final w a2 = x.a(viewGroup);
                a2.a(view4);
                animator = b(viewGroup, view4, sVar, sVar2);
                if (animator == null) {
                    a2.b(view4);
                } else {
                    animator.addListener(new AnimatorListenerAdapter() {
                        /* class android.support.transition.ai.AnonymousClass1 */

                        public void onAnimationEnd(Animator animator) {
                            a2.b(view4);
                        }
                    });
                }
            } else if (view2 != null) {
                int visibility = view2.getVisibility();
                ad.a(view2, 0);
                animator = b(viewGroup, view2, sVar, sVar2);
                if (animator != null) {
                    a aVar = new a(view2, i3, true);
                    animator.addListener(aVar);
                    a.a(animator, aVar);
                    a(aVar);
                } else {
                    ad.a(view2, visibility);
                }
            }
        }
        return animator;
    }

    public Animator b(ViewGroup viewGroup, View view, s sVar, s sVar2) {
        return null;
    }

    @Override // android.support.transition.l
    public boolean a(s sVar, s sVar2) {
        if (sVar == null && sVar2 == null) {
            return false;
        }
        if (sVar != null && sVar2 != null && sVar2.f421a.containsKey("android:visibility:visibility") != sVar.f421a.containsKey("android:visibility:visibility")) {
            return false;
        }
        b b2 = b(sVar, sVar2);
        if (!b2.f393a) {
            return false;
        }
        if (b2.c == 0 || b2.d == 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static class a extends AnimatorListenerAdapter implements a.AbstractC0018a, l.c {

        /* renamed from: a  reason: collision with root package name */
        boolean f392a = false;
        private final View b;
        private final int c;
        private final ViewGroup d;
        private final boolean e;
        private boolean f;

        a(View view, int i, boolean z) {
            this.b = view;
            this.c = i;
            this.d = (ViewGroup) view.getParent();
            this.e = z;
            a(true);
        }

        @Override // android.support.transition.a.AbstractC0018a
        public void onAnimationPause(Animator animator) {
            if (!this.f392a) {
                ad.a(this.b, this.c);
            }
        }

        @Override // android.support.transition.a.AbstractC0018a
        public void onAnimationResume(Animator animator) {
            if (!this.f392a) {
                ad.a(this.b, 0);
            }
        }

        public void onAnimationCancel(Animator animator) {
            this.f392a = true;
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationStart(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            a();
        }

        @Override // android.support.transition.l.c
        public void d(@NonNull l lVar) {
        }

        @Override // android.support.transition.l.c
        public void a(@NonNull l lVar) {
            a();
            lVar.b(this);
        }

        @Override // android.support.transition.l.c
        public void b(@NonNull l lVar) {
            a(false);
        }

        @Override // android.support.transition.l.c
        public void c(@NonNull l lVar) {
            a(true);
        }

        private void a() {
            if (!this.f392a) {
                ad.a(this.b, this.c);
                if (this.d != null) {
                    this.d.invalidate();
                }
            }
            a(false);
        }

        private void a(boolean z) {
            if (this.e && this.f != z && this.d != null) {
                this.f = z;
                x.a(this.d, z);
            }
        }
    }
}
