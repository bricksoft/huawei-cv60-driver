package android.support.transition;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class n {

    /* renamed from: a  reason: collision with root package name */
    static ArrayList<ViewGroup> f415a = new ArrayList<>();
    private static l b = new b();
    private static ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<l>>>> c = new ThreadLocal<>();

    static ArrayMap<ViewGroup, ArrayList<l>> a() {
        ArrayMap<ViewGroup, ArrayList<l>> arrayMap;
        WeakReference<ArrayMap<ViewGroup, ArrayList<l>>> weakReference = c.get();
        if (weakReference != null && (arrayMap = weakReference.get()) != null) {
            return arrayMap;
        }
        ArrayMap<ViewGroup, ArrayList<l>> arrayMap2 = new ArrayMap<>();
        c.set(new WeakReference<>(arrayMap2));
        return arrayMap2;
    }

    private static void b(ViewGroup viewGroup, l lVar) {
        if (lVar != null && viewGroup != null) {
            a aVar = new a(lVar, viewGroup);
            viewGroup.addOnAttachStateChangeListener(aVar);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(aVar);
        }
    }

    /* access modifiers changed from: private */
    public static class a implements View.OnAttachStateChangeListener, ViewTreeObserver.OnPreDrawListener {

        /* renamed from: a  reason: collision with root package name */
        l f416a;
        ViewGroup b;

        a(l lVar, ViewGroup viewGroup) {
            this.f416a = lVar;
            this.b = viewGroup;
        }

        private void a() {
            this.b.getViewTreeObserver().removeOnPreDrawListener(this);
            this.b.removeOnAttachStateChangeListener(this);
        }

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            a();
            n.f415a.remove(this.b);
            ArrayList<l> arrayList = n.a().get(this.b);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator<l> it = arrayList.iterator();
                while (it.hasNext()) {
                    it.next().e(this.b);
                }
            }
            this.f416a.a(true);
        }

        public boolean onPreDraw() {
            a();
            if (n.f415a.remove(this.b)) {
                final ArrayMap<ViewGroup, ArrayList<l>> a2 = n.a();
                ArrayList<l> arrayList = a2.get(this.b);
                ArrayList arrayList2 = null;
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                    a2.put(this.b, arrayList);
                } else if (arrayList.size() > 0) {
                    arrayList2 = new ArrayList(arrayList);
                }
                arrayList.add(this.f416a);
                this.f416a.a(new m() {
                    /* class android.support.transition.n.a.AnonymousClass1 */

                    @Override // android.support.transition.l.c, android.support.transition.m
                    public void a(@NonNull l lVar) {
                        ((ArrayList) a2.get(a.this.b)).remove(lVar);
                    }
                });
                this.f416a.a(this.b, false);
                if (arrayList2 != null) {
                    Iterator it = arrayList2.iterator();
                    while (it.hasNext()) {
                        ((l) it.next()).e(this.b);
                    }
                }
                this.f416a.a(this.b);
            }
            return true;
        }
    }

    private static void c(ViewGroup viewGroup, l lVar) {
        ArrayList<l> arrayList = a().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator<l> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().d(viewGroup);
            }
        }
        if (lVar != null) {
            lVar.a(viewGroup, true);
        }
        k a2 = k.a(viewGroup);
        if (a2 != null) {
            a2.a();
        }
    }

    public static void a(@NonNull ViewGroup viewGroup, @Nullable l lVar) {
        if (!f415a.contains(viewGroup) && ViewCompat.isLaidOut(viewGroup)) {
            f415a.add(viewGroup);
            if (lVar == null) {
                lVar = b;
            }
            l o = lVar.clone();
            c(viewGroup, o);
            k.a(viewGroup, null);
            b(viewGroup, o);
        }
    }
}
