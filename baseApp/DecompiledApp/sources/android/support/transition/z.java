package android.support.transition;

import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* access modifiers changed from: package-private */
@RequiresApi(18)
public class z {

    /* renamed from: a  reason: collision with root package name */
    private static Method f425a;
    private static boolean b;

    static void a(@NonNull ViewGroup viewGroup, boolean z) {
        a();
        if (f425a != null) {
            try {
                f425a.invoke(viewGroup, Boolean.valueOf(z));
            } catch (IllegalAccessException e) {
                Log.i("ViewUtilsApi18", "Failed to invoke suppressLayout method", e);
            } catch (InvocationTargetException e2) {
                Log.i("ViewUtilsApi18", "Error invoking suppressLayout method", e2);
            }
        }
    }

    private static void a() {
        if (!b) {
            try {
                f425a = ViewGroup.class.getDeclaredMethod("suppressLayout", Boolean.TYPE);
                f425a.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi18", "Failed to retrieve suppressLayout method", e);
            }
            b = true;
        }
    }
}
