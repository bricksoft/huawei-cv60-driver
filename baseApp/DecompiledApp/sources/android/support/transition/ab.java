package android.support.transition;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.ViewOverlay;

@RequiresApi(18)
class ab implements ac {

    /* renamed from: a  reason: collision with root package name */
    private final ViewOverlay f386a;

    ab(@NonNull View view) {
        this.f386a = view.getOverlay();
    }

    @Override // android.support.transition.ac
    public void a(@NonNull Drawable drawable) {
        this.f386a.add(drawable);
    }

    @Override // android.support.transition.ac
    public void b(@NonNull Drawable drawable) {
        this.f386a.remove(drawable);
    }
}
