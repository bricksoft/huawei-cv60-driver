package android.support.design.c;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.design.c.d;
import android.widget.FrameLayout;

public class b extends FrameLayout implements d {

    /* renamed from: a  reason: collision with root package name */
    private final c f242a;

    @Override // android.support.design.c.d
    public void a() {
        this.f242a.a();
    }

    @Override // android.support.design.c.d
    public void b() {
        this.f242a.b();
    }

    @Override // android.support.design.c.d
    @Nullable
    public d.C0014d getRevealInfo() {
        return this.f242a.c();
    }

    @Override // android.support.design.c.d
    public void setRevealInfo(@Nullable d.C0014d dVar) {
        this.f242a.a(dVar);
    }

    @Override // android.support.design.c.d
    public int getCircularRevealScrimColor() {
        return this.f242a.d();
    }

    @Override // android.support.design.c.d
    public void setCircularRevealScrimColor(@ColorInt int i) {
        this.f242a.a(i);
    }

    @Nullable
    public Drawable getCircularRevealOverlayDrawable() {
        return this.f242a.e();
    }

    @Override // android.support.design.c.d
    public void setCircularRevealOverlayDrawable(@Nullable Drawable drawable) {
        this.f242a.a(drawable);
    }

    @SuppressLint({"MissingSuperCall"})
    public void draw(Canvas canvas) {
        if (this.f242a != null) {
            this.f242a.a(canvas);
        } else {
            super.draw(canvas);
        }
    }

    @Override // android.support.design.c.c.a
    public void a(Canvas canvas) {
        super.draw(canvas);
    }

    public boolean isOpaque() {
        if (this.f242a != null) {
            return this.f242a.f();
        }
        return super.isOpaque();
    }

    @Override // android.support.design.c.c.a
    public boolean c() {
        return super.isOpaque();
    }
}
