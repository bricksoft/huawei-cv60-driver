package android.support.design.c;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Build;
import android.support.design.c.d;
import android.view.View;
import android.view.ViewAnimationUtils;

public final class a {
    public static Animator a(d dVar, float f, float f2, float f3) {
        ObjectAnimator ofObject = ObjectAnimator.ofObject(dVar, d.b.f245a, d.a.f244a, new d.C0014d(f, f2, f3));
        if (Build.VERSION.SDK_INT < 21) {
            return ofObject;
        }
        d.C0014d revealInfo = dVar.getRevealInfo();
        if (revealInfo == null) {
            throw new IllegalStateException("Caller must set a non-null RevealInfo before calling this.");
        }
        Animator createCircularReveal = ViewAnimationUtils.createCircularReveal((View) dVar, (int) f, (int) f2, revealInfo.c, f3);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(ofObject, createCircularReveal);
        return animatorSet;
    }

    public static Animator.AnimatorListener a(final d dVar) {
        return new AnimatorListenerAdapter() {
            /* class android.support.design.c.a.AnonymousClass1 */

            public void onAnimationStart(Animator animator) {
                dVar.a();
            }

            public void onAnimationEnd(Animator animator) {
                dVar.b();
            }
        };
    }
}
