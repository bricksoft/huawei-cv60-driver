package android.support.design.c;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.design.c.d;
import android.support.design.widget.k;
import android.view.View;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public static final int f243a;
    private final a b;
    private final View c;
    private final Path d;
    private final Paint e;
    private final Paint f;
    @Nullable
    private d.C0014d g;
    @Nullable
    private Drawable h;
    private boolean i;
    private boolean j;

    interface a {
        void a(Canvas canvas);

        boolean c();
    }

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f243a = 2;
        } else if (Build.VERSION.SDK_INT >= 18) {
            f243a = 1;
        } else {
            f243a = 0;
        }
    }

    public void a() {
        if (f243a == 0) {
            this.i = true;
            this.j = false;
            this.c.buildDrawingCache();
            Bitmap drawingCache = this.c.getDrawingCache();
            if (!(drawingCache != null || this.c.getWidth() == 0 || this.c.getHeight() == 0)) {
                drawingCache = Bitmap.createBitmap(this.c.getWidth(), this.c.getHeight(), Bitmap.Config.ARGB_8888);
                this.c.draw(new Canvas(drawingCache));
            }
            if (drawingCache != null) {
                this.e.setShader(new BitmapShader(drawingCache, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
            }
            this.i = false;
            this.j = true;
        }
    }

    public void b() {
        if (f243a == 0) {
            this.j = false;
            this.c.destroyDrawingCache();
            this.e.setShader(null);
            this.c.invalidate();
        }
    }

    public void a(@Nullable d.C0014d dVar) {
        if (dVar == null) {
            this.g = null;
        } else {
            if (this.g == null) {
                this.g = new d.C0014d(dVar);
            } else {
                this.g.a(dVar);
            }
            if (k.b(dVar.c, b(dVar), 1.0E-4f)) {
                this.g.c = Float.MAX_VALUE;
            }
        }
        g();
    }

    @Nullable
    public d.C0014d c() {
        if (this.g == null) {
            return null;
        }
        d.C0014d dVar = new d.C0014d(this.g);
        if (!dVar.a()) {
            return dVar;
        }
        dVar.c = b(dVar);
        return dVar;
    }

    public void a(@ColorInt int i2) {
        this.f.setColor(i2);
        this.c.invalidate();
    }

    @ColorInt
    public int d() {
        return this.f.getColor();
    }

    @Nullable
    public Drawable e() {
        return this.h;
    }

    public void a(@Nullable Drawable drawable) {
        this.h = drawable;
        this.c.invalidate();
    }

    private void g() {
        if (f243a == 1) {
            this.d.rewind();
            if (this.g != null) {
                this.d.addCircle(this.g.f247a, this.g.b, this.g.c, Path.Direction.CW);
            }
        }
        this.c.invalidate();
    }

    private float b(d.C0014d dVar) {
        return k.a(dVar.f247a, dVar.b, 0.0f, 0.0f, (float) this.c.getWidth(), (float) this.c.getHeight());
    }

    public void a(Canvas canvas) {
        if (h()) {
            switch (f243a) {
                case 0:
                    canvas.drawCircle(this.g.f247a, this.g.b, this.g.c, this.e);
                    if (i()) {
                        canvas.drawCircle(this.g.f247a, this.g.b, this.g.c, this.f);
                        break;
                    }
                    break;
                case 1:
                    int save = canvas.save();
                    canvas.clipPath(this.d);
                    this.b.a(canvas);
                    if (i()) {
                        canvas.drawRect(0.0f, 0.0f, (float) this.c.getWidth(), (float) this.c.getHeight(), this.f);
                    }
                    canvas.restoreToCount(save);
                    break;
                case 2:
                    this.b.a(canvas);
                    if (i()) {
                        canvas.drawRect(0.0f, 0.0f, (float) this.c.getWidth(), (float) this.c.getHeight(), this.f);
                        break;
                    }
                    break;
                default:
                    throw new IllegalStateException("Unsupported strategy " + f243a);
            }
        } else {
            this.b.a(canvas);
            if (i()) {
                canvas.drawRect(0.0f, 0.0f, (float) this.c.getWidth(), (float) this.c.getHeight(), this.f);
            }
        }
        b(canvas);
    }

    private void b(Canvas canvas) {
        if (j()) {
            Rect bounds = this.h.getBounds();
            float width = this.g.f247a - (((float) bounds.width()) / 2.0f);
            float height = this.g.b - (((float) bounds.height()) / 2.0f);
            canvas.translate(width, height);
            this.h.draw(canvas);
            canvas.translate(-width, -height);
        }
    }

    public boolean f() {
        return this.b.c() && !h();
    }

    private boolean h() {
        boolean z;
        if (this.g == null || this.g.a()) {
            z = true;
        } else {
            z = false;
        }
        return f243a == 0 ? !z && this.j : !z;
    }

    private boolean i() {
        return !this.i && Color.alpha(this.f.getColor()) != 0;
    }

    private boolean j() {
        return (this.i || this.h == null || this.g == null) ? false : true;
    }
}
