package android.support.design.c.a;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.design.c.c;
import android.support.design.c.d;
import android.support.v7.widget.CardView;

public class a extends CardView implements d {
    private final c e;

    @Override // android.support.design.c.d
    public void a() {
        this.e.a();
    }

    @Override // android.support.design.c.d
    public void b() {
        this.e.b();
    }

    @Override // android.support.design.c.d
    public void setRevealInfo(@Nullable d.C0014d dVar) {
        this.e.a(dVar);
    }

    @Override // android.support.design.c.d
    @Nullable
    public d.C0014d getRevealInfo() {
        return this.e.c();
    }

    @Override // android.support.design.c.d
    public void setCircularRevealScrimColor(@ColorInt int i) {
        this.e.a(i);
    }

    @Override // android.support.design.c.d
    public int getCircularRevealScrimColor() {
        return this.e.d();
    }

    @Nullable
    public Drawable getCircularRevealOverlayDrawable() {
        return this.e.e();
    }

    @Override // android.support.design.c.d
    public void setCircularRevealOverlayDrawable(@Nullable Drawable drawable) {
        this.e.a(drawable);
    }

    public void draw(Canvas canvas) {
        if (this.e != null) {
            this.e.a(canvas);
        } else {
            super.draw(canvas);
        }
    }

    @Override // android.support.design.c.c.a
    public void a(Canvas canvas) {
        super.draw(canvas);
    }

    public boolean isOpaque() {
        if (this.e != null) {
            return this.e.f();
        }
        return super.isOpaque();
    }

    @Override // android.support.design.c.c.a
    public boolean c() {
        return super.isOpaque();
    }
}
