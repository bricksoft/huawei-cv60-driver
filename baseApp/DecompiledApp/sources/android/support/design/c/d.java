package android.support.design.c;

import android.animation.TypeEvaluator;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.design.c.c;
import android.support.design.widget.k;
import android.util.Property;

public interface d extends c.a {
    void a();

    void b();

    @ColorInt
    int getCircularRevealScrimColor();

    @Nullable
    C0014d getRevealInfo();

    void setCircularRevealOverlayDrawable(@Nullable Drawable drawable);

    void setCircularRevealScrimColor(@ColorInt int i);

    void setRevealInfo(@Nullable C0014d dVar);

    /* renamed from: android.support.design.c.d$d  reason: collision with other inner class name */
    public static class C0014d {

        /* renamed from: a  reason: collision with root package name */
        public float f247a;
        public float b;
        public float c;

        private C0014d() {
        }

        public C0014d(float f, float f2, float f3) {
            this.f247a = f;
            this.b = f2;
            this.c = f3;
        }

        public C0014d(C0014d dVar) {
            this(dVar.f247a, dVar.b, dVar.c);
        }

        public void a(float f, float f2, float f3) {
            this.f247a = f;
            this.b = f2;
            this.c = f3;
        }

        public void a(C0014d dVar) {
            a(dVar.f247a, dVar.b, dVar.c);
        }

        public boolean a() {
            return this.c == Float.MAX_VALUE;
        }
    }

    public static class b extends Property<d, C0014d> {

        /* renamed from: a  reason: collision with root package name */
        public static final Property<d, C0014d> f245a = new b("circularReveal");

        private b(String str) {
            super(C0014d.class, str);
        }

        /* renamed from: a */
        public C0014d get(d dVar) {
            return dVar.getRevealInfo();
        }

        /* renamed from: a */
        public void set(d dVar, C0014d dVar2) {
            dVar.setRevealInfo(dVar2);
        }
    }

    public static class a implements TypeEvaluator<C0014d> {

        /* renamed from: a  reason: collision with root package name */
        public static final TypeEvaluator<C0014d> f244a = new a();
        private final C0014d b = new C0014d();

        /* renamed from: a */
        public C0014d evaluate(float f, C0014d dVar, C0014d dVar2) {
            this.b.a(k.a(dVar.f247a, dVar2.f247a, f), k.a(dVar.b, dVar2.b, f), k.a(dVar.c, dVar2.c, f));
            return this.b;
        }
    }

    public static class c extends Property<d, Integer> {

        /* renamed from: a  reason: collision with root package name */
        public static final Property<d, Integer> f246a = new c("circularRevealScrimColor");

        private c(String str) {
            super(Integer.class, str);
        }

        /* renamed from: a */
        public Integer get(d dVar) {
            return Integer.valueOf(dVar.getCircularRevealScrimColor());
        }

        /* renamed from: a */
        public void set(d dVar, Integer num) {
            dVar.setCircularRevealScrimColor(num.intValue());
        }
    }
}
