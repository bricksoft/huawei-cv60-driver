package android.support.design.a;

import android.animation.TypeEvaluator;
import android.graphics.Matrix;

public class g implements TypeEvaluator<Matrix> {

    /* renamed from: a  reason: collision with root package name */
    private final float[] f225a = new float[9];
    private final float[] b = new float[9];
    private final Matrix c = new Matrix();

    /* renamed from: a */
    public Matrix evaluate(float f, Matrix matrix, Matrix matrix2) {
        matrix.getValues(this.f225a);
        matrix2.getValues(this.b);
        for (int i = 0; i < 9; i++) {
            this.b[i] = ((this.b[i] - this.f225a[i]) * f) + this.f225a[i];
        }
        this.c.setValues(this.b);
        return this.c;
    }
}
