package android.support.design.internal;

import android.support.annotation.RestrictTo;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.view.SubMenu;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class d extends h {
    @Override // android.support.v7.view.menu.h, android.view.Menu
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        j jVar = (j) a(i, i2, i3, charSequence);
        f fVar = new f(f(), this, jVar);
        jVar.a(fVar);
        return fVar;
    }
}
