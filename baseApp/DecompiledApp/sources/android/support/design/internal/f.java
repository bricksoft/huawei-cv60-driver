package android.support.design.internal;

import android.content.Context;
import android.support.annotation.RestrictTo;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.u;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class f extends u {
    public f(Context context, d dVar, j jVar) {
        super(context, dVar, jVar);
    }

    @Override // android.support.v7.view.menu.h
    public void a(boolean z) {
        super.a(z);
        ((h) t()).a(z);
    }
}
