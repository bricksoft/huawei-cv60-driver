package android.support.design.internal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.u;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class BottomNavigationPresenter implements o {

    /* renamed from: a  reason: collision with root package name */
    private h f256a;
    private b b;
    private boolean c;
    private int d;

    @Override // android.support.v7.view.menu.o
    public void a(Context context, h hVar) {
        this.f256a = hVar;
        this.b.a(this.f256a);
    }

    @Override // android.support.v7.view.menu.o
    public void a(boolean z) {
        if (!this.c) {
            if (z) {
                this.b.b();
            } else {
                this.b.c();
            }
        }
    }

    @Override // android.support.v7.view.menu.o
    public void a(o.a aVar) {
    }

    @Override // android.support.v7.view.menu.o
    public boolean a(u uVar) {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public void a(h hVar, boolean z) {
    }

    @Override // android.support.v7.view.menu.o
    public boolean a() {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public boolean a(h hVar, j jVar) {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public boolean b(h hVar, j jVar) {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public int b() {
        return this.d;
    }

    @Override // android.support.v7.view.menu.o
    public Parcelable c() {
        SavedState savedState = new SavedState();
        savedState.f257a = this.b.getSelectedItemId();
        return savedState;
    }

    @Override // android.support.v7.view.menu.o
    public void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.b.a(((SavedState) parcelable).f257a);
        }
    }

    public void b(boolean z) {
        this.c = z;
    }

    /* access modifiers changed from: package-private */
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* class android.support.design.internal.BottomNavigationPresenter.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f257a;

        SavedState() {
        }

        SavedState(Parcel parcel) {
            this.f257a = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(@NonNull Parcel parcel, int i) {
            parcel.writeInt(this.f257a);
        }
    }
}
