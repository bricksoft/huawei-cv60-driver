package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.annotation.StyleRes;
import android.support.design.R;
import android.support.v4.view.ViewCompat;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.u;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class e implements o {

    /* renamed from: a  reason: collision with root package name */
    LinearLayout f262a;
    android.support.v7.view.menu.h b;
    b c;
    LayoutInflater d;
    int e;
    boolean f;
    ColorStateList g;
    ColorStateList h;
    Drawable i;
    int j;
    int k;
    int l;
    final View.OnClickListener m;
    private NavigationMenuView n;
    private o.a o;
    private int p;

    /* access modifiers changed from: private */
    public interface d {
    }

    @Override // android.support.v7.view.menu.o
    public void a(Context context, android.support.v7.view.menu.h hVar) {
        this.d = LayoutInflater.from(context);
        this.b = hVar;
        this.l = context.getResources().getDimensionPixelOffset(R.dimen.design_navigation_separator_vertical_padding);
    }

    @Override // android.support.v7.view.menu.o
    public void a(boolean z) {
        if (this.c != null) {
            this.c.b();
        }
    }

    @Override // android.support.v7.view.menu.o
    public void a(o.a aVar) {
        this.o = aVar;
    }

    @Override // android.support.v7.view.menu.o
    public boolean a(u uVar) {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public void a(android.support.v7.view.menu.h hVar, boolean z) {
        if (this.o != null) {
            this.o.a(hVar, z);
        }
    }

    @Override // android.support.v7.view.menu.o
    public boolean a() {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public boolean a(android.support.v7.view.menu.h hVar, android.support.v7.view.menu.j jVar) {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public boolean b(android.support.v7.view.menu.h hVar, android.support.v7.view.menu.j jVar) {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public int b() {
        return this.p;
    }

    @Override // android.support.v7.view.menu.o
    public Parcelable c() {
        Bundle bundle = new Bundle();
        if (this.n != null) {
            SparseArray<? extends Parcelable> sparseArray = new SparseArray<>();
            this.n.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        if (this.c != null) {
            bundle.putBundle("android:menu:adapter", this.c.d());
        }
        if (this.f262a != null) {
            SparseArray<? extends Parcelable> sparseArray2 = new SparseArray<>();
            this.f262a.saveHierarchyState(sparseArray2);
            bundle.putSparseParcelableArray("android:menu:header", sparseArray2);
        }
        return bundle;
    }

    @Override // android.support.v7.view.menu.o
    public void a(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
            if (sparseParcelableArray != null) {
                this.n.restoreHierarchyState(sparseParcelableArray);
            }
            Bundle bundle2 = bundle.getBundle("android:menu:adapter");
            if (bundle2 != null) {
                this.c.a(bundle2);
            }
            SparseArray sparseParcelableArray2 = bundle.getSparseParcelableArray("android:menu:header");
            if (sparseParcelableArray2 != null) {
                this.f262a.restoreHierarchyState(sparseParcelableArray2);
            }
        }
    }

    public void a(@NonNull android.support.v7.view.menu.j jVar) {
        this.c.a(jVar);
    }

    @Nullable
    public android.support.v7.view.menu.j d() {
        return this.c.c();
    }

    public int e() {
        return this.f262a.getChildCount();
    }

    @Nullable
    public ColorStateList f() {
        return this.h;
    }

    public void a(@Nullable ColorStateList colorStateList) {
        this.h = colorStateList;
        a(false);
    }

    @Nullable
    public ColorStateList g() {
        return this.g;
    }

    public void b(@Nullable ColorStateList colorStateList) {
        this.g = colorStateList;
        a(false);
    }

    public void a(@StyleRes int i2) {
        this.e = i2;
        this.f = true;
        a(false);
    }

    @Nullable
    public Drawable h() {
        return this.i;
    }

    public void a(@Nullable Drawable drawable) {
        this.i = drawable;
        a(false);
    }

    public int i() {
        return this.j;
    }

    public void b(int i2) {
        this.j = i2;
        a(false);
    }

    public int j() {
        return this.k;
    }

    public void c(int i2) {
        this.k = i2;
        a(false);
    }

    private static abstract class j extends RecyclerView.w {
        public j(View view) {
            super(view);
        }
    }

    /* access modifiers changed from: private */
    public static class g extends j {
        public g(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(R.layout.design_navigation_item, viewGroup, false));
            this.f590a.setOnClickListener(onClickListener);
        }
    }

    /* access modifiers changed from: private */
    public static class i extends j {
        public i(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(R.layout.design_navigation_item_subheader, viewGroup, false));
        }
    }

    /* access modifiers changed from: private */
    public static class h extends j {
        public h(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(R.layout.design_navigation_item_separator, viewGroup, false));
        }
    }

    /* access modifiers changed from: private */
    public static class a extends j {
        public a(View view) {
            super(view);
        }
    }

    /* access modifiers changed from: private */
    public class b extends RecyclerView.a<j> {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ e f263a;
        private final ArrayList<d> b;
        private android.support.v7.view.menu.j c;
        private boolean d;

        @Override // android.support.v7.widget.RecyclerView.a
        public long a(int i) {
            return (long) i;
        }

        @Override // android.support.v7.widget.RecyclerView.a
        public int a() {
            return this.b.size();
        }

        @Override // android.support.v7.widget.RecyclerView.a
        public int b(int i) {
            d dVar = this.b.get(i);
            if (dVar instanceof C0015e) {
                return 2;
            }
            if (dVar instanceof c) {
                return 3;
            }
            if (!(dVar instanceof f)) {
                throw new RuntimeException("Unknown item type.");
            } else if (((f) dVar).a().hasSubMenu()) {
                return 1;
            } else {
                return 0;
            }
        }

        /* renamed from: a */
        public j b(ViewGroup viewGroup, int i) {
            switch (i) {
                case 0:
                    return new g(this.f263a.d, viewGroup, this.f263a.m);
                case 1:
                    return new i(this.f263a.d, viewGroup);
                case 2:
                    return new h(this.f263a.d, viewGroup);
                case 3:
                    return new a(this.f263a.f262a);
                default:
                    return null;
            }
        }

        public void a(j jVar, int i) {
            switch (b(i)) {
                case 0:
                    NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) jVar.f590a;
                    navigationMenuItemView.setIconTintList(this.f263a.h);
                    if (this.f263a.f) {
                        navigationMenuItemView.setTextAppearance(this.f263a.e);
                    }
                    if (this.f263a.g != null) {
                        navigationMenuItemView.setTextColor(this.f263a.g);
                    }
                    ViewCompat.setBackground(navigationMenuItemView, this.f263a.i != null ? this.f263a.i.getConstantState().newDrawable() : null);
                    f fVar = (f) this.b.get(i);
                    navigationMenuItemView.setNeedsEmptyIcon(fVar.f265a);
                    navigationMenuItemView.setHorizontalPadding(this.f263a.j);
                    navigationMenuItemView.setIconPadding(this.f263a.k);
                    navigationMenuItemView.a(fVar.a(), 0);
                    return;
                case 1:
                    ((TextView) jVar.f590a).setText(((f) this.b.get(i)).a().getTitle());
                    return;
                case 2:
                    C0015e eVar = (C0015e) this.b.get(i);
                    jVar.f590a.setPadding(0, eVar.a(), 0, eVar.b());
                    return;
                default:
                    return;
            }
        }

        public void a(j jVar) {
            if (jVar instanceof g) {
                ((NavigationMenuItemView) jVar.f590a).b();
            }
        }

        public void b() {
            g();
            f();
        }

        private void g() {
            boolean z;
            int i;
            int i2;
            if (!this.d) {
                this.d = true;
                this.b.clear();
                this.b.add(new c());
                int i3 = -1;
                int i4 = 0;
                boolean z2 = false;
                int size = this.f263a.b.j().size();
                int i5 = 0;
                while (i5 < size) {
                    android.support.v7.view.menu.j jVar = this.f263a.b.j().get(i5);
                    if (jVar.isChecked()) {
                        a(jVar);
                    }
                    if (jVar.isCheckable()) {
                        jVar.a(false);
                    }
                    if (jVar.hasSubMenu()) {
                        SubMenu subMenu = jVar.getSubMenu();
                        if (subMenu.hasVisibleItems()) {
                            if (i5 != 0) {
                                this.b.add(new C0015e(this.f263a.l, 0));
                            }
                            this.b.add(new f(jVar));
                            boolean z3 = false;
                            int size2 = this.b.size();
                            int size3 = subMenu.size();
                            for (int i6 = 0; i6 < size3; i6++) {
                                android.support.v7.view.menu.j jVar2 = (android.support.v7.view.menu.j) subMenu.getItem(i6);
                                if (jVar2.isVisible()) {
                                    if (!z3 && jVar2.getIcon() != null) {
                                        z3 = true;
                                    }
                                    if (jVar2.isCheckable()) {
                                        jVar2.a(false);
                                    }
                                    if (jVar.isChecked()) {
                                        a(jVar);
                                    }
                                    this.b.add(new f(jVar2));
                                }
                            }
                            if (z3) {
                                a(size2, this.b.size());
                            }
                        }
                        i2 = i3;
                    } else {
                        int groupId = jVar.getGroupId();
                        if (groupId != i3) {
                            i = this.b.size();
                            z = jVar.getIcon() != null;
                            if (i5 != 0) {
                                i++;
                                this.b.add(new C0015e(this.f263a.l, this.f263a.l));
                            }
                        } else if (z2 || jVar.getIcon() == null) {
                            z = z2;
                            i = i4;
                        } else {
                            z = true;
                            a(i4, this.b.size());
                            i = i4;
                        }
                        f fVar = new f(jVar);
                        fVar.f265a = z;
                        this.b.add(fVar);
                        z2 = z;
                        i4 = i;
                        i2 = groupId;
                    }
                    i5++;
                    i3 = i2;
                }
                this.d = false;
            }
        }

        private void a(int i, int i2) {
            while (i < i2) {
                ((f) this.b.get(i)).f265a = true;
                i++;
            }
        }

        public void a(android.support.v7.view.menu.j jVar) {
            if (this.c != jVar && jVar.isCheckable()) {
                if (this.c != null) {
                    this.c.setChecked(false);
                }
                this.c = jVar;
                jVar.setChecked(true);
            }
        }

        public android.support.v7.view.menu.j c() {
            return this.c;
        }

        public Bundle d() {
            Bundle bundle = new Bundle();
            if (this.c != null) {
                bundle.putInt("android:menu:checked", this.c.getItemId());
            }
            SparseArray<? extends Parcelable> sparseArray = new SparseArray<>();
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                d dVar = this.b.get(i);
                if (dVar instanceof f) {
                    android.support.v7.view.menu.j a2 = ((f) dVar).a();
                    View actionView = a2 != null ? a2.getActionView() : null;
                    if (actionView != null) {
                        ParcelableSparseArray parcelableSparseArray = new ParcelableSparseArray();
                        actionView.saveHierarchyState(parcelableSparseArray);
                        sparseArray.put(a2.getItemId(), parcelableSparseArray);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:action_views", sparseArray);
            return bundle;
        }

        public void a(Bundle bundle) {
            android.support.v7.view.menu.j a2;
            View actionView;
            ParcelableSparseArray parcelableSparseArray;
            android.support.v7.view.menu.j a3;
            int i = bundle.getInt("android:menu:checked", 0);
            if (i != 0) {
                this.d = true;
                int size = this.b.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    d dVar = this.b.get(i2);
                    if ((dVar instanceof f) && (a3 = ((f) dVar).a()) != null && a3.getItemId() == i) {
                        a(a3);
                        break;
                    }
                    i2++;
                }
                this.d = false;
                g();
            }
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:action_views");
            if (sparseParcelableArray != null) {
                int size2 = this.b.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    d dVar2 = this.b.get(i3);
                    if (!(!(dVar2 instanceof f) || (a2 = ((f) dVar2).a()) == null || (actionView = a2.getActionView()) == null || (parcelableSparseArray = (ParcelableSparseArray) sparseParcelableArray.get(a2.getItemId())) == null)) {
                        actionView.restoreHierarchyState(parcelableSparseArray);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static class f implements d {

        /* renamed from: a  reason: collision with root package name */
        boolean f265a;
        private final android.support.v7.view.menu.j b;

        f(android.support.v7.view.menu.j jVar) {
            this.b = jVar;
        }

        public android.support.v7.view.menu.j a() {
            return this.b;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: android.support.design.internal.e$e  reason: collision with other inner class name */
    public static class C0015e implements d {

        /* renamed from: a  reason: collision with root package name */
        private final int f264a;
        private final int b;

        public C0015e(int i, int i2) {
            this.f264a = i;
            this.b = i2;
        }

        public int a() {
            return this.f264a;
        }

        public int b() {
            return this.b;
        }
    }

    /* access modifiers changed from: private */
    public static class c implements d {
        c() {
        }
    }
}
