package android.support.design.internal;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.annotation.Dimension;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.annotation.StyleRes;
import android.support.transition.n;
import android.support.v4.util.Pools;
import android.support.v4.view.ViewCompat;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.p;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.exoplayer.C;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class b extends ViewGroup implements p {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f260a = {16842912};
    private static final int[] b = {-16842910};
    private final android.support.transition.p c;
    private final int d;
    private final int e;
    private final int f;
    private final int g;
    private final int h;
    private final View.OnClickListener i;
    private final Pools.Pool<a> j;
    private boolean k;
    private int l;
    private a[] m;
    private int n;
    private int o;
    private ColorStateList p;
    @Dimension
    private int q;
    private ColorStateList r;
    private final ColorStateList s;
    @StyleRes
    private int t;
    @StyleRes
    private int u;
    private Drawable v;
    private int w;
    private int[] x;
    private BottomNavigationPresenter y;
    private h z;

    @Override // android.support.v7.view.menu.p
    public void a(h hVar) {
        this.z = hVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6 = 1;
        int size = View.MeasureSpec.getSize(i2);
        int size2 = this.z.j().size();
        int childCount = getChildCount();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.h, C.ENCODING_PCM_32BIT);
        if (!a(this.l, size2) || !this.k) {
            if (size2 != 0) {
                i6 = size2;
            }
            int min = Math.min(size / i6, this.f);
            int i7 = size - (min * size2);
            for (int i8 = 0; i8 < childCount; i8++) {
                if (getChildAt(i8).getVisibility() != 8) {
                    this.x[i8] = min;
                    if (i7 > 0) {
                        int[] iArr = this.x;
                        iArr[i8] = iArr[i8] + 1;
                        i7--;
                    }
                } else {
                    this.x[i8] = 0;
                }
            }
        } else {
            View childAt = getChildAt(this.o);
            int i9 = this.g;
            if (childAt.getVisibility() != 8) {
                childAt.measure(View.MeasureSpec.makeMeasureSpec(this.f, Integer.MIN_VALUE), makeMeasureSpec);
                i9 = Math.max(i9, childAt.getMeasuredWidth());
            }
            int i10 = size2 - (childAt.getVisibility() != 8 ? 1 : 0);
            int min2 = Math.min(size - (this.e * i10), Math.min(i9, this.f));
            int i11 = size - min2;
            if (i10 != 0) {
                i6 = i10;
            }
            int min3 = Math.min(i11 / i6, this.d);
            int i12 = (size - min2) - (i10 * min3);
            int i13 = 0;
            while (i13 < childCount) {
                if (getChildAt(i13).getVisibility() != 8) {
                    int[] iArr2 = this.x;
                    if (i13 == this.o) {
                        i5 = min2;
                    } else {
                        i5 = min3;
                    }
                    iArr2[i13] = i5;
                    if (i12 > 0) {
                        int[] iArr3 = this.x;
                        iArr3[i13] = iArr3[i13] + 1;
                        i4 = i12 - 1;
                    }
                    i4 = i12;
                } else {
                    this.x[i13] = 0;
                    i4 = i12;
                }
                i13++;
                i12 = i4;
            }
        }
        int i14 = 0;
        for (int i15 = 0; i15 < childCount; i15++) {
            View childAt2 = getChildAt(i15);
            if (childAt2.getVisibility() != 8) {
                childAt2.measure(View.MeasureSpec.makeMeasureSpec(this.x[i15], C.ENCODING_PCM_32BIT), makeMeasureSpec);
                childAt2.getLayoutParams().width = childAt2.getMeasuredWidth();
                i14 += childAt2.getMeasuredWidth();
            }
        }
        setMeasuredDimension(View.resolveSizeAndState(i14, View.MeasureSpec.makeMeasureSpec(i14, C.ENCODING_PCM_32BIT), 0), View.resolveSizeAndState(this.h, makeMeasureSpec, 0));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int i6 = i4 - i2;
        int i7 = i5 - i3;
        int i8 = 0;
        for (int i9 = 0; i9 < childCount; i9++) {
            View childAt = getChildAt(i9);
            if (childAt.getVisibility() != 8) {
                if (ViewCompat.getLayoutDirection(this) == 1) {
                    childAt.layout((i6 - i8) - childAt.getMeasuredWidth(), 0, i6 - i8, i7);
                } else {
                    childAt.layout(i8, 0, childAt.getMeasuredWidth() + i8, i7);
                }
                i8 += childAt.getMeasuredWidth();
            }
        }
    }

    public int getWindowAnimations() {
        return 0;
    }

    public void setIconTintList(ColorStateList colorStateList) {
        this.p = colorStateList;
        if (this.m != null) {
            for (a aVar : this.m) {
                aVar.setIconTintList(colorStateList);
            }
        }
    }

    @Nullable
    public ColorStateList getIconTintList() {
        return this.p;
    }

    public void setItemIconSize(@Dimension int i2) {
        this.q = i2;
        if (this.m != null) {
            for (a aVar : this.m) {
                aVar.setIconSize(i2);
            }
        }
    }

    @Dimension
    public int getItemIconSize() {
        return this.q;
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.r = colorStateList;
        if (this.m != null) {
            for (a aVar : this.m) {
                aVar.setTextColor(colorStateList);
            }
        }
    }

    public ColorStateList getItemTextColor() {
        return this.r;
    }

    public void setItemTextAppearanceInactive(@StyleRes int i2) {
        this.t = i2;
        if (this.m != null) {
            a[] aVarArr = this.m;
            for (a aVar : aVarArr) {
                aVar.setTextAppearanceInactive(i2);
                if (this.r != null) {
                    aVar.setTextColor(this.r);
                }
            }
        }
    }

    @StyleRes
    public int getItemTextAppearanceInactive() {
        return this.t;
    }

    public void setItemTextAppearanceActive(@StyleRes int i2) {
        this.u = i2;
        if (this.m != null) {
            a[] aVarArr = this.m;
            for (a aVar : aVarArr) {
                aVar.setTextAppearanceActive(i2);
                if (this.r != null) {
                    aVar.setTextColor(this.r);
                }
            }
        }
    }

    @StyleRes
    public int getItemTextAppearanceActive() {
        return this.u;
    }

    public void setItemBackgroundRes(int i2) {
        this.w = i2;
        if (this.m != null) {
            for (a aVar : this.m) {
                aVar.setItemBackground(i2);
            }
        }
    }

    @Deprecated
    public int getItemBackgroundRes() {
        return this.w;
    }

    public void setItemBackground(@Nullable Drawable drawable) {
        this.v = drawable;
        if (this.m != null) {
            for (a aVar : this.m) {
                aVar.setItemBackground(drawable);
            }
        }
    }

    @Nullable
    public Drawable getItemBackground() {
        if (this.m == null || this.m.length <= 0) {
            return this.v;
        }
        return this.m[0].getBackground();
    }

    public void setLabelVisibilityMode(int i2) {
        this.l = i2;
    }

    public int getLabelVisibilityMode() {
        return this.l;
    }

    public void setItemHorizontalTranslationEnabled(boolean z2) {
        this.k = z2;
    }

    public boolean a() {
        return this.k;
    }

    public void setPresenter(BottomNavigationPresenter bottomNavigationPresenter) {
        this.y = bottomNavigationPresenter;
    }

    public void b() {
        removeAllViews();
        if (this.m != null) {
            a[] aVarArr = this.m;
            for (a aVar : aVarArr) {
                if (aVar != null) {
                    this.j.release(aVar);
                }
            }
        }
        if (this.z.size() == 0) {
            this.n = 0;
            this.o = 0;
            this.m = null;
            return;
        }
        this.m = new a[this.z.size()];
        boolean a2 = a(this.l, this.z.j().size());
        for (int i2 = 0; i2 < this.z.size(); i2++) {
            this.y.b(true);
            this.z.getItem(i2).setCheckable(true);
            this.y.b(false);
            a newItem = getNewItem();
            this.m[i2] = newItem;
            newItem.setIconTintList(this.p);
            newItem.setIconSize(this.q);
            newItem.setTextColor(this.s);
            newItem.setTextAppearanceInactive(this.t);
            newItem.setTextAppearanceActive(this.u);
            newItem.setTextColor(this.r);
            if (this.v != null) {
                newItem.setItemBackground(this.v);
            } else {
                newItem.setItemBackground(this.w);
            }
            newItem.setShifting(a2);
            newItem.setLabelVisibilityMode(this.l);
            newItem.a((j) this.z.getItem(i2), 0);
            newItem.setItemPosition(i2);
            newItem.setOnClickListener(this.i);
            addView(newItem);
        }
        this.o = Math.min(this.z.size() - 1, this.o);
        this.z.getItem(this.o).setChecked(true);
    }

    public void c() {
        if (!(this.z == null || this.m == null)) {
            int size = this.z.size();
            if (size != this.m.length) {
                b();
                return;
            }
            int i2 = this.n;
            for (int i3 = 0; i3 < size; i3++) {
                MenuItem item = this.z.getItem(i3);
                if (item.isChecked()) {
                    this.n = item.getItemId();
                    this.o = i3;
                }
            }
            if (i2 != this.n) {
                n.a(this, this.c);
            }
            boolean a2 = a(this.l, this.z.j().size());
            for (int i4 = 0; i4 < size; i4++) {
                this.y.b(true);
                this.m[i4].setLabelVisibilityMode(this.l);
                this.m[i4].setShifting(a2);
                this.m[i4].a((j) this.z.getItem(i4), 0);
                this.y.b(false);
            }
        }
    }

    private a getNewItem() {
        a acquire = this.j.acquire();
        if (acquire == null) {
            return new a(getContext());
        }
        return acquire;
    }

    public int getSelectedItemId() {
        return this.n;
    }

    private boolean a(int i2, int i3) {
        return i2 == -1 ? i3 > 3 : i2 == 0;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        int size = this.z.size();
        for (int i3 = 0; i3 < size; i3++) {
            MenuItem item = this.z.getItem(i3);
            if (i2 == item.getItemId()) {
                this.n = i2;
                this.o = i3;
                item.setChecked(true);
                return;
            }
        }
    }
}
