package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.R;
import android.support.design.a.g;
import android.support.design.a.h;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Iterator;

/* access modifiers changed from: package-private */
public class f {

    /* renamed from: a  reason: collision with root package name */
    static final TimeInterpolator f344a = android.support.design.a.a.c;
    static final int[] p = {16842919, 16842910};
    static final int[] q = {16843623, 16842908, 16842910};
    static final int[] r = {16842908, 16842910};
    static final int[] s = {16843623, 16842910};
    static final int[] t = {16842910};
    static final int[] u = new int[0];
    private float A;
    private ArrayList<Animator.AnimatorListener> B;
    private ArrayList<Animator.AnimatorListener> C;
    private final Rect D = new Rect();
    private final RectF E = new RectF();
    private final RectF F = new RectF();
    private final Matrix G = new Matrix();
    private ViewTreeObserver.OnPreDrawListener H;
    int b = 0;
    @Nullable
    Animator c;
    @Nullable
    h d;
    @Nullable
    h e;
    l f;
    Drawable g;
    Drawable h;
    a i;
    Drawable j;
    float k;
    float l;
    float m;
    int n;
    float o = 1.0f;
    final u v;
    final m w;
    @Nullable
    private h x;
    @Nullable
    private h y;
    private final o z;

    /* access modifiers changed from: package-private */
    public interface d {
        void a();

        void b();
    }

    f(u uVar, m mVar) {
        this.v = uVar;
        this.w = mVar;
        this.z = new o();
        this.z.a(p, a((AbstractC0017f) new c()));
        this.z.a(q, a((AbstractC0017f) new b()));
        this.z.a(r, a((AbstractC0017f) new b()));
        this.z.a(s, a((AbstractC0017f) new b()));
        this.z.a(t, a((AbstractC0017f) new e()));
        this.z.a(u, a((AbstractC0017f) new a()));
        this.A = this.v.getRotation();
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (this.g != null) {
            DrawableCompat.setTintList(this.g, colorStateList);
        }
        if (this.i != null) {
            this.i.a(colorStateList);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        if (this.g != null) {
            DrawableCompat.setTintMode(this.g, mode);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        if (this.h != null) {
            DrawableCompat.setTintList(this.h, android.support.design.f.a.a(colorStateList));
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(float f2) {
        if (this.k != f2) {
            this.k = f2;
            a(this.k, this.l, this.m);
        }
    }

    /* access modifiers changed from: package-private */
    public float a() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public float b() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public float c() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final void b(float f2) {
        if (this.l != f2) {
            this.l = f2;
            a(this.k, this.l, this.m);
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(float f2) {
        if (this.m != f2) {
            this.m = f2;
            a(this.k, this.l, this.m);
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        d(this.o);
    }

    /* access modifiers changed from: package-private */
    public final void d(float f2) {
        this.o = f2;
        Matrix matrix = this.G;
        a(f2, matrix);
        this.v.setImageMatrix(matrix);
    }

    private void a(float f2, Matrix matrix) {
        matrix.reset();
        Drawable drawable = this.v.getDrawable();
        if (drawable != null && this.n != 0) {
            RectF rectF = this.E;
            RectF rectF2 = this.F;
            rectF.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
            rectF2.set(0.0f, 0.0f, (float) this.n, (float) this.n);
            matrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
            matrix.postScale(f2, f2, ((float) this.n) / 2.0f, ((float) this.n) / 2.0f);
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public final h e() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final void a(@Nullable h hVar) {
        this.d = hVar;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public final h f() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final void b(@Nullable h hVar) {
        this.e = hVar;
    }

    /* access modifiers changed from: package-private */
    public void a(float f2, float f3, float f4) {
        if (this.f != null) {
            this.f.a(f2, this.m + f2);
            j();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int[] iArr) {
        this.z.a(iArr);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        this.z.a();
    }

    /* access modifiers changed from: package-private */
    public void a(@NonNull Animator.AnimatorListener animatorListener) {
        if (this.B == null) {
            this.B = new ArrayList<>();
        }
        this.B.add(animatorListener);
    }

    /* access modifiers changed from: package-private */
    public void b(@NonNull Animator.AnimatorListener animatorListener) {
        if (this.B != null) {
            this.B.remove(animatorListener);
        }
    }

    public void c(@NonNull Animator.AnimatorListener animatorListener) {
        if (this.C == null) {
            this.C = new ArrayList<>();
        }
        this.C.add(animatorListener);
    }

    public void d(@NonNull Animator.AnimatorListener animatorListener) {
        if (this.C != null) {
            this.C.remove(animatorListener);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable final d dVar, final boolean z2) {
        h r2;
        if (!p()) {
            if (this.c != null) {
                this.c.cancel();
            }
            if (t()) {
                if (this.e != null) {
                    r2 = this.e;
                } else {
                    r2 = r();
                }
                AnimatorSet a2 = a(r2, 0.0f, 0.0f, 0.0f);
                a2.addListener(new AnimatorListenerAdapter() {
                    /* class android.support.design.widget.f.AnonymousClass1 */
                    private boolean d;

                    public void onAnimationStart(Animator animator) {
                        f.this.v.a(0, z2);
                        f.this.b = 1;
                        f.this.c = animator;
                        this.d = false;
                    }

                    public void onAnimationCancel(Animator animator) {
                        this.d = true;
                    }

                    public void onAnimationEnd(Animator animator) {
                        f.this.b = 0;
                        f.this.c = null;
                        if (!this.d) {
                            f.this.v.a(z2 ? 8 : 4, z2);
                            if (dVar != null) {
                                dVar.b();
                            }
                        }
                    }
                });
                if (this.C != null) {
                    Iterator<Animator.AnimatorListener> it = this.C.iterator();
                    while (it.hasNext()) {
                        a2.addListener(it.next());
                    }
                }
                a2.start();
                return;
            }
            this.v.a(z2 ? 8 : 4, z2);
            if (dVar != null) {
                dVar.b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(@Nullable final d dVar, final boolean z2) {
        h q2;
        if (!o()) {
            if (this.c != null) {
                this.c.cancel();
            }
            if (t()) {
                if (this.v.getVisibility() != 0) {
                    this.v.setAlpha(0.0f);
                    this.v.setScaleY(0.0f);
                    this.v.setScaleX(0.0f);
                    d(0.0f);
                }
                if (this.d != null) {
                    q2 = this.d;
                } else {
                    q2 = q();
                }
                AnimatorSet a2 = a(q2, 1.0f, 1.0f, 1.0f);
                a2.addListener(new AnimatorListenerAdapter() {
                    /* class android.support.design.widget.f.AnonymousClass2 */

                    public void onAnimationStart(Animator animator) {
                        f.this.v.a(0, z2);
                        f.this.b = 2;
                        f.this.c = animator;
                    }

                    public void onAnimationEnd(Animator animator) {
                        f.this.b = 0;
                        f.this.c = null;
                        if (dVar != null) {
                            dVar.a();
                        }
                    }
                });
                if (this.B != null) {
                    Iterator<Animator.AnimatorListener> it = this.B.iterator();
                    while (it.hasNext()) {
                        a2.addListener(it.next());
                    }
                }
                a2.start();
                return;
            }
            this.v.a(0, z2);
            this.v.setAlpha(1.0f);
            this.v.setScaleY(1.0f);
            this.v.setScaleX(1.0f);
            d(1.0f);
            if (dVar != null) {
                dVar.a();
            }
        }
    }

    private h q() {
        if (this.x == null) {
            this.x = h.a(this.v.getContext(), R.animator.design_fab_show_motion_spec);
        }
        return this.x;
    }

    private h r() {
        if (this.y == null) {
            this.y = h.a(this.v.getContext(), R.animator.design_fab_hide_motion_spec);
        }
        return this.y;
    }

    @NonNull
    private AnimatorSet a(@NonNull h hVar, float f2, float f3, float f4) {
        ArrayList arrayList = new ArrayList();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.v, View.ALPHA, f2);
        hVar.b("opacity").a((Animator) ofFloat);
        arrayList.add(ofFloat);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.v, View.SCALE_X, f3);
        hVar.b("scale").a((Animator) ofFloat2);
        arrayList.add(ofFloat2);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.v, View.SCALE_Y, f3);
        hVar.b("scale").a((Animator) ofFloat3);
        arrayList.add(ofFloat3);
        a(f4, this.G);
        ObjectAnimator ofObject = ObjectAnimator.ofObject(this.v, new android.support.design.a.f(), new g(), new Matrix(this.G));
        hVar.b("iconScale").a((Animator) ofObject);
        arrayList.add(ofObject);
        AnimatorSet animatorSet = new AnimatorSet();
        android.support.design.a.b.a(animatorSet, arrayList);
        return animatorSet;
    }

    /* access modifiers changed from: package-private */
    public final Drawable h() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void i() {
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        Rect rect = this.D;
        a(rect);
        b(rect);
        this.w.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    /* access modifiers changed from: package-private */
    public void a(Rect rect) {
        this.f.getPadding(rect);
    }

    /* access modifiers changed from: package-private */
    public void b(Rect rect) {
    }

    /* access modifiers changed from: package-private */
    public void k() {
        if (m()) {
            s();
            this.v.getViewTreeObserver().addOnPreDrawListener(this.H);
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        if (this.H != null) {
            this.v.getViewTreeObserver().removeOnPreDrawListener(this.H);
            this.H = null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean m() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public void n() {
        float rotation = this.v.getRotation();
        if (this.A != rotation) {
            this.A = rotation;
            u();
        }
    }

    private void s() {
        if (this.H == null) {
            this.H = new ViewTreeObserver.OnPreDrawListener() {
                /* class android.support.design.widget.f.AnonymousClass3 */

                public boolean onPreDraw() {
                    f.this.n();
                    return true;
                }
            };
        }
    }

    /* access modifiers changed from: package-private */
    public boolean o() {
        return this.v.getVisibility() != 0 ? this.b == 2 : this.b != 1;
    }

    /* access modifiers changed from: package-private */
    public boolean p() {
        return this.v.getVisibility() == 0 ? this.b == 1 : this.b != 2;
    }

    private ValueAnimator a(@NonNull AbstractC0017f fVar) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(f344a);
        valueAnimator.setDuration(100L);
        valueAnimator.addListener(fVar);
        valueAnimator.addUpdateListener(fVar);
        valueAnimator.setFloatValues(0.0f, 1.0f);
        return valueAnimator;
    }

    /* renamed from: android.support.design.widget.f$f  reason: collision with other inner class name */
    private abstract class AbstractC0017f extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: a  reason: collision with root package name */
        private boolean f352a;
        private float c;
        private float d;

        /* access modifiers changed from: protected */
        public abstract float a();

        private AbstractC0017f() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (!this.f352a) {
                this.c = f.this.f.a();
                this.d = a();
                this.f352a = true;
            }
            f.this.f.a(this.c + ((this.d - this.c) * valueAnimator.getAnimatedFraction()));
        }

        public void onAnimationEnd(Animator animator) {
            f.this.f.a(this.d);
            this.f352a = false;
        }
    }

    private class e extends AbstractC0017f {
        e() {
            super();
        }

        /* access modifiers changed from: protected */
        @Override // android.support.design.widget.f.AbstractC0017f
        public float a() {
            return f.this.k;
        }
    }

    private class b extends AbstractC0017f {
        b() {
            super();
        }

        /* access modifiers changed from: protected */
        @Override // android.support.design.widget.f.AbstractC0017f
        public float a() {
            return f.this.k + f.this.l;
        }
    }

    private class c extends AbstractC0017f {
        c() {
            super();
        }

        /* access modifiers changed from: protected */
        @Override // android.support.design.widget.f.AbstractC0017f
        public float a() {
            return f.this.k + f.this.m;
        }
    }

    private class a extends AbstractC0017f {
        a() {
            super();
        }

        /* access modifiers changed from: protected */
        @Override // android.support.design.widget.f.AbstractC0017f
        public float a() {
            return 0.0f;
        }
    }

    private boolean t() {
        return ViewCompat.isLaidOut(this.v) && !this.v.isInEditMode();
    }

    private void u() {
        if (Build.VERSION.SDK_INT == 19) {
            if (this.A % 90.0f != 0.0f) {
                if (this.v.getLayerType() != 1) {
                    this.v.setLayerType(1, null);
                }
            } else if (this.v.getLayerType() != 0) {
                this.v.setLayerType(0, null);
            }
        }
        if (this.f != null) {
            this.f.b(-this.A);
        }
        if (this.i != null) {
            this.i.a(-this.A);
        }
    }
}
