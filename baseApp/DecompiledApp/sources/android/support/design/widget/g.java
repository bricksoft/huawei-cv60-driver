package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.f.a;
import android.view.View;
import java.util.ArrayList;

/* access modifiers changed from: package-private */
@RequiresApi(21)
public class g extends f {
    private InsetDrawable x;

    g(u uVar, m mVar) {
        super(uVar, mVar);
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.design.widget.f
    public void b(ColorStateList colorStateList) {
        if (this.h instanceof RippleDrawable) {
            ((RippleDrawable) this.h).setColor(a.a(colorStateList));
        } else {
            super.b(colorStateList);
        }
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.design.widget.f
    public void a(float f, float f2, float f3) {
        if (Build.VERSION.SDK_INT == 21) {
            this.v.refreshDrawableState();
        } else {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(p, a(f, f3));
            stateListAnimator.addState(q, a(f, f2));
            stateListAnimator.addState(r, a(f, f2));
            stateListAnimator.addState(s, a(f, f2));
            AnimatorSet animatorSet = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            arrayList.add(ObjectAnimator.ofFloat(this.v, "elevation", f).setDuration(0L));
            if (Build.VERSION.SDK_INT >= 22 && Build.VERSION.SDK_INT <= 24) {
                arrayList.add(ObjectAnimator.ofFloat(this.v, View.TRANSLATION_Z, this.v.getTranslationZ()).setDuration(100L));
            }
            arrayList.add(ObjectAnimator.ofFloat(this.v, View.TRANSLATION_Z, 0.0f).setDuration(100L));
            animatorSet.playSequentially((Animator[]) arrayList.toArray(new Animator[0]));
            animatorSet.setInterpolator(f344a);
            stateListAnimator.addState(t, animatorSet);
            stateListAnimator.addState(u, a(0.0f, 0.0f));
            this.v.setStateListAnimator(stateListAnimator);
        }
        if (this.w.b()) {
            j();
        }
    }

    @NonNull
    private Animator a(float f, float f2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ObjectAnimator.ofFloat(this.v, "elevation", f).setDuration(0L)).with(ObjectAnimator.ofFloat(this.v, View.TRANSLATION_Z, f2).setDuration(100L));
        animatorSet.setInterpolator(f344a);
        return animatorSet;
    }

    @Override // android.support.design.widget.f
    public float a() {
        return this.v.getElevation();
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.design.widget.f
    public void i() {
        j();
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.design.widget.f
    public void b(Rect rect) {
        if (this.w.b()) {
            this.x = new InsetDrawable(this.h, rect.left, rect.top, rect.right, rect.bottom);
            this.w.a(this.x);
            return;
        }
        this.w.a(this.h);
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.design.widget.f
    public void a(int[] iArr) {
        if (Build.VERSION.SDK_INT != 21) {
            return;
        }
        if (this.v.isEnabled()) {
            this.v.setElevation(this.k);
            if (this.v.isPressed()) {
                this.v.setTranslationZ(this.m);
            } else if (this.v.isFocused() || this.v.isHovered()) {
                this.v.setTranslationZ(this.l);
            } else {
                this.v.setTranslationZ(0.0f);
            }
        } else {
            this.v.setElevation(0.0f);
            this.v.setTranslationZ(0.0f);
        }
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.design.widget.f
    public void g() {
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.design.widget.f
    public boolean m() {
        return false;
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.design.widget.f
    public void a(Rect rect) {
        if (this.w.b()) {
            float a2 = this.w.a();
            float a3 = a() + this.m;
            int ceil = (int) Math.ceil((double) l.b(a3, a2, false));
            int ceil2 = (int) Math.ceil((double) l.a(a3, a2, false));
            rect.set(ceil, ceil2, ceil, ceil2);
            return;
        }
        rect.set(0, 0, 0, 0);
    }
}
