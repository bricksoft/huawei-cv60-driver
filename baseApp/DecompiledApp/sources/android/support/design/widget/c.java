package android.support.design.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;

class c extends GradientDrawable {

    /* renamed from: a  reason: collision with root package name */
    private final Paint f341a = new Paint(1);
    private final RectF b;
    private int c;

    c() {
        c();
        this.b = new RectF();
    }

    private void c() {
        this.f341a.setStyle(Paint.Style.FILL_AND_STROKE);
        this.f341a.setColor(-1);
        this.f341a.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return !this.b.isEmpty();
    }

    /* access modifiers changed from: package-private */
    public void a(float f, float f2, float f3, float f4) {
        if (f != this.b.left || f2 != this.b.top || f3 != this.b.right || f4 != this.b.bottom) {
            this.b.set(f, f2, f3, f4);
            invalidateSelf();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(RectF rectF) {
        a(rectF.left, rectF.top, rectF.right, rectF.bottom);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a(0.0f, 0.0f, 0.0f, 0.0f);
    }

    public void draw(@NonNull Canvas canvas) {
        a(canvas);
        super.draw(canvas);
        canvas.drawRect(this.b, this.f341a);
        c(canvas);
    }

    private void a(@NonNull Canvas canvas) {
        Drawable.Callback callback = getCallback();
        if (a(callback)) {
            ((View) callback).setLayerType(2, null);
        } else {
            b(canvas);
        }
    }

    private void b(@NonNull Canvas canvas) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.c = canvas.saveLayer(0.0f, 0.0f, (float) canvas.getWidth(), (float) canvas.getHeight(), null);
        } else {
            this.c = canvas.saveLayer(0.0f, 0.0f, (float) canvas.getWidth(), (float) canvas.getHeight(), null, 31);
        }
    }

    private void c(@NonNull Canvas canvas) {
        if (!a(getCallback())) {
            canvas.restoreToCount(this.c);
        }
    }

    private boolean a(Drawable.Callback callback) {
        return callback instanceof View;
    }
}
