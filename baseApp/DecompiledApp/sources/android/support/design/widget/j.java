package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.R;
import android.support.design.a.a;
import android.support.design.a.b;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.Space;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.y;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/* access modifiers changed from: package-private */
public final class j {

    /* renamed from: a  reason: collision with root package name */
    private final Context f356a;
    private final TextInputLayout b;
    private LinearLayout c;
    private int d;
    private FrameLayout e;
    private int f;
    @Nullable
    private Animator g;
    private final float h = ((float) this.f356a.getResources().getDimensionPixelSize(R.dimen.design_textinput_caption_translate_y));
    private int i;
    private int j;
    private CharSequence k;
    private boolean l;
    private TextView m;
    private int n;
    private CharSequence o;
    private boolean p;
    private TextView q;
    private int r;
    private Typeface s;

    public j(TextInputLayout textInputLayout) {
        this.f356a = textInputLayout.getContext();
        this.b = textInputLayout;
    }

    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        c();
        this.o = charSequence;
        this.q.setText(charSequence);
        if (this.i != 2) {
            this.j = 2;
        }
        a(this.i, this.j, a(this.q, charSequence));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        c();
        if (this.i == 2) {
            this.j = 0;
        }
        a(this.i, this.j, a(this.q, (CharSequence) null));
    }

    /* access modifiers changed from: package-private */
    public void b(CharSequence charSequence) {
        c();
        this.k = charSequence;
        this.m.setText(charSequence);
        if (this.i != 1) {
            this.j = 1;
        }
        a(this.i, this.j, a(this.m, charSequence));
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.k = null;
        c();
        if (this.i == 1) {
            if (!this.p || TextUtils.isEmpty(this.o)) {
                this.j = 0;
            } else {
                this.j = 2;
            }
        }
        a(this.i, this.j, a(this.m, (CharSequence) null));
    }

    private boolean a(TextView textView, @Nullable CharSequence charSequence) {
        return ViewCompat.isLaidOut(this.b) && this.b.isEnabled() && (this.j != this.i || textView == null || !TextUtils.equals(textView.getText(), charSequence));
    }

    private void a(final int i2, final int i3, boolean z) {
        if (z) {
            AnimatorSet animatorSet = new AnimatorSet();
            this.g = animatorSet;
            ArrayList arrayList = new ArrayList();
            a(arrayList, this.p, this.q, 2, i2, i3);
            a(arrayList, this.l, this.m, 1, i2, i3);
            b.a(animatorSet, arrayList);
            final TextView d2 = d(i2);
            final TextView d3 = d(i3);
            animatorSet.addListener(new AnimatorListenerAdapter() {
                /* class android.support.design.widget.j.AnonymousClass1 */

                public void onAnimationEnd(Animator animator) {
                    j.this.i = i3;
                    j.this.g = null;
                    if (d2 != null) {
                        d2.setVisibility(4);
                        if (i2 == 1 && j.this.m != null) {
                            j.this.m.setText((CharSequence) null);
                        }
                    }
                }

                public void onAnimationStart(Animator animator) {
                    if (d3 != null) {
                        d3.setVisibility(0);
                    }
                }
            });
            animatorSet.start();
        } else {
            a(i2, i3);
        }
        this.b.c();
        this.b.a(z);
        this.b.d();
    }

    private void a(int i2, int i3) {
        TextView d2;
        TextView d3;
        if (i2 != i3) {
            if (!(i3 == 0 || (d3 = d(i3)) == null)) {
                d3.setVisibility(0);
                d3.setAlpha(1.0f);
            }
            if (!(i2 == 0 || (d2 = d(i2)) == null)) {
                d2.setVisibility(4);
                if (i2 == 1) {
                    d2.setText((CharSequence) null);
                }
            }
            this.i = i3;
        }
    }

    private void a(List<Animator> list, boolean z, TextView textView, int i2, int i3, int i4) {
        if (textView != null && z) {
            if (i2 == i4 || i2 == i3) {
                list.add(a(textView, i4 == i2));
                if (i4 == i2) {
                    list.add(a(textView));
                }
            }
        }
    }

    private ObjectAnimator a(TextView textView, boolean z) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, View.ALPHA, z ? 1.0f : 0.0f);
        ofFloat.setDuration(167L);
        ofFloat.setInterpolator(a.f220a);
        return ofFloat;
    }

    private ObjectAnimator a(TextView textView) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, View.TRANSLATION_Y, -this.h, 0.0f);
        ofFloat.setDuration(217L);
        ofFloat.setInterpolator(a.d);
        return ofFloat;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.g != null) {
            this.g.cancel();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        return i2 == 0 || i2 == 1;
    }

    @Nullable
    private TextView d(int i2) {
        switch (i2) {
            case 1:
                return this.m;
            case 2:
                return this.q;
            default:
                return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (m()) {
            ViewCompat.setPaddingRelative(this.c, ViewCompat.getPaddingStart(this.b.getEditText()), 0, ViewCompat.getPaddingEnd(this.b.getEditText()), 0);
        }
    }

    private boolean m() {
        return (this.c == null || this.b.getEditText() == null) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public void a(TextView textView, int i2) {
        if (this.c == null && this.e == null) {
            this.c = new LinearLayout(this.f356a);
            this.c.setOrientation(0);
            this.b.addView(this.c, -1, -2);
            this.e = new FrameLayout(this.f356a);
            this.c.addView(this.e, -1, new FrameLayout.LayoutParams(-2, -2));
            this.c.addView(new Space(this.f356a), new LinearLayout.LayoutParams(0, 0, 1.0f));
            if (this.b.getEditText() != null) {
                d();
            }
        }
        if (a(i2)) {
            this.e.setVisibility(0);
            this.e.addView(textView);
            this.f++;
        } else {
            this.c.addView(textView, i2);
        }
        this.c.setVisibility(0);
        this.d++;
    }

    /* access modifiers changed from: package-private */
    public void b(TextView textView, int i2) {
        if (this.c != null) {
            if (!a(i2) || this.e == null) {
                this.c.removeView(textView);
            } else {
                this.f--;
                a(this.e, this.f);
                this.e.removeView(textView);
            }
            this.d--;
            a(this.c, this.d);
        }
    }

    private void a(ViewGroup viewGroup, int i2) {
        if (i2 == 0) {
            viewGroup.setVisibility(8);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (this.l != z) {
            c();
            if (z) {
                this.m = new y(this.f356a);
                this.m.setId(R.id.textinput_error);
                if (this.s != null) {
                    this.m.setTypeface(this.s);
                }
                b(this.n);
                this.m.setVisibility(4);
                ViewCompat.setAccessibilityLiveRegion(this.m, 1);
                a(this.m, 0);
            } else {
                b();
                b(this.m, 0);
                this.m = null;
                this.b.c();
                this.b.d();
            }
            this.l = z;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        if (this.p != z) {
            c();
            if (z) {
                this.q = new y(this.f356a);
                this.q.setId(R.id.textinput_helper_text);
                if (this.s != null) {
                    this.q.setTypeface(this.s);
                }
                this.q.setVisibility(4);
                ViewCompat.setAccessibilityLiveRegion(this.q, 1);
                c(this.r);
                a(this.q, 1);
            } else {
                a();
                b(this.q, 1);
                this.q = null;
                this.b.c();
                this.b.d();
            }
            this.p = z;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return e(this.j);
    }

    private boolean e(int i2) {
        if (i2 != 1 || this.m == null || TextUtils.isEmpty(this.k)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public CharSequence h() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public CharSequence i() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public void a(Typeface typeface) {
        if (typeface != this.s) {
            this.s = typeface;
            a(this.m, typeface);
            a(this.q, typeface);
        }
    }

    private void a(@Nullable TextView textView, Typeface typeface) {
        if (textView != null) {
            textView.setTypeface(typeface);
        }
    }

    /* access modifiers changed from: package-private */
    @ColorInt
    public int j() {
        if (this.m != null) {
            return this.m.getCurrentTextColor();
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public ColorStateList k() {
        if (this.m != null) {
            return this.m.getTextColors();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable ColorStateList colorStateList) {
        if (this.m != null) {
            this.m.setTextColor(colorStateList);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(@StyleRes int i2) {
        this.n = i2;
        if (this.m != null) {
            this.b.a(this.m, i2);
        }
    }

    /* access modifiers changed from: package-private */
    @ColorInt
    public int l() {
        if (this.q != null) {
            return this.q.getCurrentTextColor();
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public void b(@Nullable ColorStateList colorStateList) {
        if (this.q != null) {
            this.q.setTextColor(colorStateList);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(@StyleRes int i2) {
        this.r = i2;
        if (this.q != null) {
            TextViewCompat.setTextAppearance(this.q, i2);
        }
    }
}
