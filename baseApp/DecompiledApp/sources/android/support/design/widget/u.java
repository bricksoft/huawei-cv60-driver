package android.support.design.widget;

import android.support.annotation.RestrictTo;
import android.widget.ImageButton;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class u extends ImageButton {

    /* renamed from: a  reason: collision with root package name */
    private int f369a;

    public void setVisibility(int i) {
        a(i, true);
    }

    public final void a(int i, boolean z) {
        super.setVisibility(i);
        if (z) {
            this.f369a = i;
        }
    }

    public final int getUserSetVisibility() {
        return this.f369a;
    }
}
