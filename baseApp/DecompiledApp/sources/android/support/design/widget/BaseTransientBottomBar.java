package android.support.design.widget;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.RestrictTo;
import android.support.design.R;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.design.widget.n;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.accessibility.AccessibilityManagerCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import java.util.List;

public abstract class BaseTransientBottomBar<B extends BaseTransientBottomBar<B>> {

    /* renamed from: a  reason: collision with root package name */
    static final Handler f286a = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        /* class android.support.design.widget.BaseTransientBottomBar.AnonymousClass1 */

        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ((BaseTransientBottomBar) message.obj).c();
                    return true;
                case 1:
                    ((BaseTransientBottomBar) message.obj).b(message.arg1);
                    return true;
                default:
                    return false;
            }
        }
    });
    private static final boolean d = (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT <= 19);
    private static final int[] e = {R.attr.snackbarStyle};
    protected final e b;
    final n.a c;
    private final ViewGroup f;
    private final android.support.design.h.a g;
    private List<a<B>> h;
    private Behavior i;
    private final AccessibilityManager j;

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public interface c {
        void a(View view);

        void b(View view);
    }

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public interface d {
        void a(View view, int i, int i2, int i3, int i4);
    }

    public static abstract class a<B> {
        public void a(B b, int i) {
        }

        public void a(B b) {
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        n.a().a(this.c, i2);
    }

    public boolean a() {
        return n.a().e(this.c);
    }

    /* access modifiers changed from: protected */
    public SwipeDismissBehavior<? extends View> b() {
        return new Behavior();
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (this.b.getParent() == null) {
            ViewGroup.LayoutParams layoutParams = this.b.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.e) {
                CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                SwipeDismissBehavior<? extends View> b2 = this.i == null ? b() : this.i;
                if (b2 instanceof Behavior) {
                    ((Behavior) b2).a((Behavior) this);
                }
                b2.a(new SwipeDismissBehavior.a() {
                    /* class android.support.design.widget.BaseTransientBottomBar.AnonymousClass4 */

                    @Override // android.support.design.widget.SwipeDismissBehavior.a
                    public void a(View view) {
                        view.setVisibility(8);
                        BaseTransientBottomBar.this.a(0);
                    }

                    @Override // android.support.design.widget.SwipeDismissBehavior.a
                    public void a(int i) {
                        switch (i) {
                            case 0:
                                n.a().d(BaseTransientBottomBar.this.c);
                                return;
                            case 1:
                            case 2:
                                n.a().c(BaseTransientBottomBar.this.c);
                                return;
                            default:
                                return;
                        }
                    }
                });
                eVar.a(b2);
                eVar.g = 80;
            }
            this.f.addView(this.b);
        }
        this.b.setOnAttachStateChangeListener(new c() {
            /* class android.support.design.widget.BaseTransientBottomBar.AnonymousClass5 */

            @Override // android.support.design.widget.BaseTransientBottomBar.c
            public void a(View view) {
            }

            @Override // android.support.design.widget.BaseTransientBottomBar.c
            public void b(View view) {
                if (BaseTransientBottomBar.this.a()) {
                    BaseTransientBottomBar.f286a.post(new Runnable() {
                        /* class android.support.design.widget.BaseTransientBottomBar.AnonymousClass5.AnonymousClass1 */

                        public void run() {
                            BaseTransientBottomBar.this.c(3);
                        }
                    });
                }
            }
        });
        if (!ViewCompat.isLaidOut(this.b)) {
            this.b.setOnLayoutChangeListener(new d() {
                /* class android.support.design.widget.BaseTransientBottomBar.AnonymousClass6 */

                @Override // android.support.design.widget.BaseTransientBottomBar.d
                public void a(View view, int i, int i2, int i3, int i4) {
                    BaseTransientBottomBar.this.b.setOnLayoutChangeListener(null);
                    if (BaseTransientBottomBar.this.f()) {
                        BaseTransientBottomBar.this.d();
                    } else {
                        BaseTransientBottomBar.this.e();
                    }
                }
            });
        } else if (f()) {
            d();
        } else {
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        final int h2 = h();
        if (d) {
            ViewCompat.offsetTopAndBottom(this.b, h2);
        } else {
            this.b.setTranslationY((float) h2);
        }
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(h2, 0);
        valueAnimator.setInterpolator(android.support.design.a.a.b);
        valueAnimator.setDuration(250L);
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            /* class android.support.design.widget.BaseTransientBottomBar.AnonymousClass7 */

            public void onAnimationStart(Animator animator) {
                BaseTransientBottomBar.this.g.a(70, 180);
            }

            public void onAnimationEnd(Animator animator) {
                BaseTransientBottomBar.this.e();
            }
        });
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            /* class android.support.design.widget.BaseTransientBottomBar.AnonymousClass8 */
            private int c = h2;

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                if (BaseTransientBottomBar.d) {
                    ViewCompat.offsetTopAndBottom(BaseTransientBottomBar.this.b, intValue - this.c);
                } else {
                    BaseTransientBottomBar.this.b.setTranslationY((float) intValue);
                }
                this.c = intValue;
            }
        });
        valueAnimator.start();
    }

    private void d(final int i2) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(0, h());
        valueAnimator.setInterpolator(android.support.design.a.a.b);
        valueAnimator.setDuration(250L);
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            /* class android.support.design.widget.BaseTransientBottomBar.AnonymousClass2 */

            public void onAnimationStart(Animator animator) {
                BaseTransientBottomBar.this.g.b(0, 180);
            }

            public void onAnimationEnd(Animator animator) {
                BaseTransientBottomBar.this.c(i2);
            }
        });
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            /* class android.support.design.widget.BaseTransientBottomBar.AnonymousClass3 */
            private int b = 0;

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                if (BaseTransientBottomBar.d) {
                    ViewCompat.offsetTopAndBottom(BaseTransientBottomBar.this.b, intValue - this.b);
                } else {
                    BaseTransientBottomBar.this.b.setTranslationY((float) intValue);
                }
                this.b = intValue;
            }
        });
        valueAnimator.start();
    }

    private int h() {
        int height = this.b.getHeight();
        ViewGroup.LayoutParams layoutParams = this.b.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin + height;
        }
        return height;
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) {
        if (!f() || this.b.getVisibility() != 0) {
            c(i2);
        } else {
            d(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        n.a().b(this.c);
        if (this.h != null) {
            for (int size = this.h.size() - 1; size >= 0; size--) {
                this.h.get(size).a(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        n.a().a(this.c);
        if (this.h != null) {
            for (int size = this.h.size() - 1; size >= 0; size--) {
                this.h.get(size).a(this, i2);
            }
        }
        ViewParent parent = this.b.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.b);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        List<AccessibilityServiceInfo> enabledAccessibilityServiceList = this.j.getEnabledAccessibilityServiceList(1);
        if (enabledAccessibilityServiceList == null || !enabledAccessibilityServiceList.isEmpty()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public static class e extends FrameLayout {

        /* renamed from: a  reason: collision with root package name */
        private final AccessibilityManager f296a;
        private final AccessibilityManagerCompat.TouchExplorationStateChangeListener b;
        private d c;
        private c d;

        protected e(Context context) {
            this(context, null);
        }

        protected e(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SnackbarLayout);
            if (obtainStyledAttributes.hasValue(R.styleable.SnackbarLayout_elevation)) {
                ViewCompat.setElevation(this, (float) obtainStyledAttributes.getDimensionPixelSize(R.styleable.SnackbarLayout_elevation, 0));
            }
            obtainStyledAttributes.recycle();
            this.f296a = (AccessibilityManager) context.getSystemService("accessibility");
            this.b = new AccessibilityManagerCompat.TouchExplorationStateChangeListener() {
                /* class android.support.design.widget.BaseTransientBottomBar.e.AnonymousClass1 */

                @Override // android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener
                public void onTouchExplorationStateChanged(boolean z) {
                    e.this.setClickableOrFocusableBasedOnAccessibility(z);
                }
            };
            AccessibilityManagerCompat.addTouchExplorationStateChangeListener(this.f296a, this.b);
            setClickableOrFocusableBasedOnAccessibility(this.f296a.isTouchExplorationEnabled());
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void setClickableOrFocusableBasedOnAccessibility(boolean z) {
            setClickable(!z);
            setFocusable(z);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            if (this.c != null) {
                this.c.a(this, i, i2, i3, i4);
            }
        }

        /* access modifiers changed from: protected */
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            if (this.d != null) {
                this.d.a(this);
            }
            ViewCompat.requestApplyInsets(this);
        }

        /* access modifiers changed from: protected */
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            if (this.d != null) {
                this.d.b(this);
            }
            AccessibilityManagerCompat.removeTouchExplorationStateChangeListener(this.f296a, this.b);
        }

        /* access modifiers changed from: package-private */
        public void setOnLayoutChangeListener(d dVar) {
            this.c = dVar;
        }

        /* access modifiers changed from: package-private */
        public void setOnAttachStateChangeListener(c cVar) {
            this.d = cVar;
        }
    }

    public static class Behavior extends SwipeDismissBehavior<View> {
        private final b g = new b(this);

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.g.a(baseTransientBottomBar);
        }

        @Override // android.support.design.widget.SwipeDismissBehavior
        public boolean a(View view) {
            return this.g.a(view);
        }

        @Override // android.support.design.widget.SwipeDismissBehavior, android.support.design.widget.CoordinatorLayout.b
        public boolean b(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
            this.g.a(coordinatorLayout, view, motionEvent);
            return super.b(coordinatorLayout, view, motionEvent);
        }
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private n.a f295a;

        public b(SwipeDismissBehavior<?> swipeDismissBehavior) {
            swipeDismissBehavior.a(0.1f);
            swipeDismissBehavior.b(0.6f);
            swipeDismissBehavior.a(0);
        }

        public void a(BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.f295a = baseTransientBottomBar.c;
        }

        public boolean a(View view) {
            return view instanceof e;
        }

        public void a(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
            switch (motionEvent.getActionMasked()) {
                case 0:
                    if (coordinatorLayout.a(view, (int) motionEvent.getX(), (int) motionEvent.getY())) {
                        n.a().c(this.f295a);
                        return;
                    }
                    return;
                case 1:
                case 3:
                    n.a().d(this.f295a);
                    return;
                case 2:
                default:
                    return;
            }
        }
    }
}
