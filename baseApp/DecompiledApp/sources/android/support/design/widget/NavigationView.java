package android.support.design.widget;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DimenRes;
import android.support.annotation.Dimension;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.internal.d;
import android.support.design.internal.e;
import android.support.design.internal.g;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.AbsSavedState;
import android.support.v7.view.menu.j;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import com.google.android.exoplayer.C;

public class NavigationView extends g {
    private static final int[] d = {16842912};
    private static final int[] e = {-16842910};
    a c;
    private final d f;
    private final e g;
    private final int h;
    private MenuInflater i;

    public interface a {
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f316a = new Bundle();
        this.f.a(savedState.f316a);
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.f.b(savedState.f316a);
    }

    public void setNavigationItemSelectedListener(@Nullable a aVar) {
        this.c = aVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        switch (View.MeasureSpec.getMode(i2)) {
            case Integer.MIN_VALUE:
                i2 = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(i2), this.h), C.ENCODING_PCM_32BIT);
                break;
            case 0:
                i2 = View.MeasureSpec.makeMeasureSpec(this.h, C.ENCODING_PCM_32BIT);
                break;
        }
        super.onMeasure(i2, i3);
    }

    public Menu getMenu() {
        return this.f;
    }

    public int getHeaderCount() {
        return this.g.e();
    }

    @Nullable
    public ColorStateList getItemIconTintList() {
        return this.g.f();
    }

    public void setItemIconTintList(@Nullable ColorStateList colorStateList) {
        this.g.a(colorStateList);
    }

    @Nullable
    public ColorStateList getItemTextColor() {
        return this.g.g();
    }

    public void setItemTextColor(@Nullable ColorStateList colorStateList) {
        this.g.b(colorStateList);
    }

    @Nullable
    public Drawable getItemBackground() {
        return this.g.h();
    }

    public void setItemBackgroundResource(@DrawableRes int i2) {
        setItemBackground(ContextCompat.getDrawable(getContext(), i2));
    }

    public void setItemBackground(@Nullable Drawable drawable) {
        this.g.a(drawable);
    }

    @Dimension
    public int getItemHorizontalPadding() {
        return this.g.i();
    }

    public void setItemHorizontalPadding(@Dimension int i2) {
        this.g.b(i2);
    }

    public void setItemHorizontalPaddingResource(@DimenRes int i2) {
        this.g.b(getResources().getDimensionPixelSize(i2));
    }

    @Dimension
    public int getItemIconPadding() {
        return this.g.j();
    }

    public void setItemIconPadding(@Dimension int i2) {
        this.g.c(i2);
    }

    public void setItemIconPaddingResource(int i2) {
        this.g.c(getResources().getDimensionPixelSize(i2));
    }

    public void setCheckedItem(@IdRes int i2) {
        MenuItem findItem = this.f.findItem(i2);
        if (findItem != null) {
            this.g.a((j) findItem);
        }
    }

    public void setCheckedItem(@NonNull MenuItem menuItem) {
        MenuItem findItem = this.f.findItem(menuItem.getItemId());
        if (findItem != null) {
            this.g.a((j) findItem);
            return;
        }
        throw new IllegalArgumentException("Called setCheckedItem(MenuItem) with an item that is not in the current menu.");
    }

    @Nullable
    public MenuItem getCheckedItem() {
        return this.g.d();
    }

    public void setItemTextAppearance(@StyleRes int i2) {
        this.g.a(i2);
    }

    private MenuInflater getMenuInflater() {
        if (this.i == null) {
            this.i = new android.support.v7.view.g(getContext());
        }
        return this.i;
    }

    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            /* class android.support.design.widget.NavigationView.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        public Bundle f316a;

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f316a = parcel.readBundle(classLoader);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        @Override // android.support.v4.view.AbsSavedState
        public void writeToParcel(@NonNull Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeBundle(this.f316a);
        }
    }
}
