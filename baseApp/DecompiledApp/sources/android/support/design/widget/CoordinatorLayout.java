package android.support.design.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.FloatRange;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.annotation.VisibleForTesting;
import android.support.coordinatorlayout.R;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ObjectsCompat;
import android.support.v4.util.Pools;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.NestedScrollingParent2;
import android.support.v4.view.NestedScrollingParentHelper;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v4.widget.DirectedAcyclicGraph;
import android.support.v4.widget.ViewGroupUtils;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoordinatorLayout extends ViewGroup implements NestedScrollingParent2 {

    /* renamed from: a  reason: collision with root package name */
    static final String f306a;
    static final Class<?>[] b = {Context.class, AttributeSet.class};
    static final ThreadLocal<Map<String, Constructor<b>>> c = new ThreadLocal<>();
    static final Comparator<View> d;
    private static final Pools.Pool<Rect> f = new Pools.SynchronizedPool(12);
    ViewGroup.OnHierarchyChangeListener e;
    private final List<View> g;
    private final DirectedAcyclicGraph<View> h;
    private final List<View> i;
    private final List<View> j;
    private final int[] k;
    private Paint l;
    private boolean m;
    private boolean n;
    private int[] o;
    private View p;
    private View q;
    private f r;
    private boolean s;
    private WindowInsetsCompat t;
    private boolean u;
    private Drawable v;
    private OnApplyWindowInsetsListener w;
    private final NestedScrollingParentHelper x;

    public interface a {
        @NonNull
        b getBehavior();
    }

    @Deprecated
    @Retention(RetentionPolicy.RUNTIME)
    public @interface c {
        Class<? extends b> a();
    }

    static {
        Package r0 = CoordinatorLayout.class.getPackage();
        f306a = r0 != null ? r0.getName() : null;
        if (Build.VERSION.SDK_INT >= 21) {
            d = new g();
        } else {
            d = null;
        }
    }

    @NonNull
    private static Rect e() {
        Rect acquire = f.acquire();
        if (acquire == null) {
            return new Rect();
        }
        return acquire;
    }

    private static void a(@NonNull Rect rect) {
        rect.setEmpty();
        f.release(rect);
    }

    public CoordinatorLayout(@NonNull Context context) {
        this(context, null);
    }

    public CoordinatorLayout(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.coordinatorLayoutStyle);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CoordinatorLayout(@NonNull Context context, @Nullable AttributeSet attributeSet, @AttrRes int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes;
        this.g = new ArrayList();
        this.h = new DirectedAcyclicGraph<>();
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.k = new int[2];
        this.x = new NestedScrollingParentHelper(this);
        if (i2 == 0) {
            obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CoordinatorLayout, 0, R.style.Widget_Support_CoordinatorLayout);
        } else {
            obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CoordinatorLayout, i2, 0);
        }
        int resourceId = obtainStyledAttributes.getResourceId(R.styleable.CoordinatorLayout_keylines, 0);
        if (resourceId != 0) {
            Resources resources = context.getResources();
            this.o = resources.getIntArray(resourceId);
            float f2 = resources.getDisplayMetrics().density;
            int length = this.o.length;
            for (int i3 = 0; i3 < length; i3++) {
                this.o[i3] = (int) (((float) this.o[i3]) * f2);
            }
        }
        this.v = obtainStyledAttributes.getDrawable(R.styleable.CoordinatorLayout_statusBarBackground);
        obtainStyledAttributes.recycle();
        g();
        super.setOnHierarchyChangeListener(new d());
    }

    public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener) {
        this.e = onHierarchyChangeListener;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a(false);
        if (this.s) {
            if (this.r == null) {
                this.r = new f();
            }
            getViewTreeObserver().addOnPreDrawListener(this.r);
        }
        if (this.t == null && ViewCompat.getFitsSystemWindows(this)) {
            ViewCompat.requestApplyInsets(this);
        }
        this.n = true;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        a(false);
        if (this.s && this.r != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.r);
        }
        if (this.q != null) {
            onStopNestedScroll(this.q);
        }
        this.n = false;
    }

    public void setStatusBarBackground(@Nullable Drawable drawable) {
        Drawable drawable2 = null;
        if (this.v != drawable) {
            if (this.v != null) {
                this.v.setCallback(null);
            }
            if (drawable != null) {
                drawable2 = drawable.mutate();
            }
            this.v = drawable2;
            if (this.v != null) {
                if (this.v.isStateful()) {
                    this.v.setState(getDrawableState());
                }
                DrawableCompat.setLayoutDirection(this.v, ViewCompat.getLayoutDirection(this));
                this.v.setVisible(getVisibility() == 0, false);
                this.v.setCallback(this);
            }
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Nullable
    public Drawable getStatusBarBackground() {
        return this.v;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        boolean z = false;
        Drawable drawable = this.v;
        if (drawable != null && drawable.isStateful()) {
            z = false | drawable.setState(drawableState);
        }
        if (z) {
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.v;
    }

    public void setVisibility(int i2) {
        boolean z;
        super.setVisibility(i2);
        if (i2 == 0) {
            z = true;
        } else {
            z = false;
        }
        if (this.v != null && this.v.isVisible() != z) {
            this.v.setVisible(z, false);
        }
    }

    public void setStatusBarBackgroundResource(@DrawableRes int i2) {
        setStatusBarBackground(i2 != 0 ? ContextCompat.getDrawable(getContext(), i2) : null);
    }

    public void setStatusBarBackgroundColor(@ColorInt int i2) {
        setStatusBarBackground(new ColorDrawable(i2));
    }

    /* access modifiers changed from: package-private */
    public final WindowInsetsCompat a(WindowInsetsCompat windowInsetsCompat) {
        boolean z = true;
        if (ObjectsCompat.equals(this.t, windowInsetsCompat)) {
            return windowInsetsCompat;
        }
        this.t = windowInsetsCompat;
        this.u = windowInsetsCompat != null && windowInsetsCompat.getSystemWindowInsetTop() > 0;
        if (this.u || getBackground() != null) {
            z = false;
        }
        setWillNotDraw(z);
        WindowInsetsCompat b2 = b(windowInsetsCompat);
        requestLayout();
        return b2;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public final WindowInsetsCompat getLastWindowInsets() {
        return this.t;
    }

    private void a(boolean z) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            b b2 = ((e) childAt.getLayoutParams()).b();
            if (b2 != null) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                if (z) {
                    b2.b(this, childAt, obtain);
                } else {
                    b2.a(this, childAt, obtain);
                }
                obtain.recycle();
            }
        }
        for (int i3 = 0; i3 < childCount; i3++) {
            ((e) getChildAt(i3).getLayoutParams()).f();
        }
        this.p = null;
        this.m = false;
    }

    private void a(List<View> list) {
        int i2;
        list.clear();
        boolean isChildrenDrawingOrderEnabled = isChildrenDrawingOrderEnabled();
        int childCount = getChildCount();
        for (int i3 = childCount - 1; i3 >= 0; i3--) {
            if (isChildrenDrawingOrderEnabled) {
                i2 = getChildDrawingOrder(childCount, i3);
            } else {
                i2 = i3;
            }
            list.add(getChildAt(i2));
        }
        if (d != null) {
            Collections.sort(list, d);
        }
    }

    private boolean a(MotionEvent motionEvent, int i2) {
        boolean z;
        boolean z2;
        MotionEvent motionEvent2;
        boolean z3 = false;
        boolean z4 = false;
        MotionEvent motionEvent3 = null;
        int actionMasked = motionEvent.getActionMasked();
        List<View> list = this.i;
        a(list);
        int size = list.size();
        int i3 = 0;
        while (true) {
            if (i3 < size) {
                View view = list.get(i3);
                e eVar = (e) view.getLayoutParams();
                b b2 = eVar.b();
                if ((!z3 && !z4) || actionMasked == 0) {
                    if (!z3 && b2 != null) {
                        switch (i2) {
                            case 0:
                                z3 = b2.b(this, view, motionEvent);
                                break;
                            case 1:
                                z3 = b2.a(this, view, motionEvent);
                                break;
                        }
                        if (z3) {
                            this.p = view;
                        }
                    }
                    z = z3;
                    boolean e2 = eVar.e();
                    boolean a2 = eVar.a(this, view);
                    z2 = a2 && !e2;
                    if (!a2 || z2) {
                        motionEvent2 = motionEvent3;
                    }
                } else if (b2 != null) {
                    if (motionEvent3 == null) {
                        long uptimeMillis = SystemClock.uptimeMillis();
                        motionEvent2 = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                    } else {
                        motionEvent2 = motionEvent3;
                    }
                    switch (i2) {
                        case 0:
                            b2.b(this, view, motionEvent2);
                            break;
                        case 1:
                            b2.a(this, view, motionEvent2);
                            break;
                    }
                    z2 = z4;
                    z = z3;
                } else {
                    motionEvent2 = motionEvent3;
                    z2 = z4;
                    z = z3;
                }
                i3++;
                motionEvent3 = motionEvent2;
                z4 = z2;
                z3 = z;
            } else {
                z = z3;
            }
        }
        list.clear();
        return z;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            a(true);
        }
        boolean a2 = a(motionEvent, 0);
        if (actionMasked == 1 || actionMasked == 3) {
            a(true);
        }
        return a2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r12) {
        /*
            r11 = this;
            r4 = 3
            r10 = 1
            r5 = 0
            r7 = 0
            r2 = 0
            int r9 = r12.getActionMasked()
            android.view.View r0 = r11.p
            if (r0 != 0) goto L_0x005d
            boolean r0 = r11.a(r12, r10)
            if (r0 == 0) goto L_0x005a
            r1 = r0
        L_0x0014:
            android.view.View r0 = r11.p
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.support.design.widget.CoordinatorLayout$e r0 = (android.support.design.widget.CoordinatorLayout.e) r0
            android.support.design.widget.CoordinatorLayout$b r0 = r0.b()
            if (r0 == 0) goto L_0x0058
            android.view.View r3 = r11.p
            boolean r0 = r0.a(r11, r3, r12)
            r8 = r0
        L_0x0029:
            android.view.View r0 = r11.p
            if (r0 != 0) goto L_0x0043
            boolean r0 = super.onTouchEvent(r12)
            r8 = r8 | r0
        L_0x0032:
            if (r8 != 0) goto L_0x0036
            if (r9 != 0) goto L_0x0036
        L_0x0036:
            if (r2 == 0) goto L_0x003b
            r2.recycle()
        L_0x003b:
            if (r9 == r10) goto L_0x003f
            if (r9 != r4) goto L_0x0042
        L_0x003f:
            r11.a(r7)
        L_0x0042:
            return r8
        L_0x0043:
            if (r1 == 0) goto L_0x0032
            if (r2 != 0) goto L_0x0056
            long r0 = android.os.SystemClock.uptimeMillis()
            r2 = r0
            r6 = r5
            android.view.MotionEvent r0 = android.view.MotionEvent.obtain(r0, r2, r4, r5, r6, r7)
        L_0x0051:
            super.onTouchEvent(r0)
            r2 = r0
            goto L_0x0032
        L_0x0056:
            r0 = r2
            goto L_0x0051
        L_0x0058:
            r8 = r7
            goto L_0x0029
        L_0x005a:
            r1 = r0
            r8 = r7
            goto L_0x0029
        L_0x005d:
            r1 = r7
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (z && !this.m) {
            a(false);
            this.m = true;
        }
    }

    private int b(int i2) {
        if (this.o == null) {
            Log.e("CoordinatorLayout", "No keylines defined for " + this + " - attempted index lookup " + i2);
            return 0;
        } else if (i2 >= 0 && i2 < this.o.length) {
            return this.o[i2];
        } else {
            Log.e("CoordinatorLayout", "Keyline index " + i2 + " out of range for " + this);
            return 0;
        }
    }

    static b a(Context context, AttributeSet attributeSet, String str) {
        HashMap hashMap;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith(".")) {
            str = context.getPackageName() + str;
        } else if (str.indexOf(46) < 0 && !TextUtils.isEmpty(f306a)) {
            str = f306a + '.' + str;
        }
        try {
            Map<String, Constructor<b>> map = c.get();
            if (map == null) {
                HashMap hashMap2 = new HashMap();
                c.set(hashMap2);
                hashMap = hashMap2;
            } else {
                hashMap = map;
            }
            Constructor<b> constructor = hashMap.get(str);
            if (constructor == null) {
                constructor = context.getClassLoader().loadClass(str).getConstructor(b);
                constructor.setAccessible(true);
                hashMap.put(str, constructor);
            }
            return (b) constructor.newInstance(context, attributeSet);
        } catch (Exception e2) {
            throw new RuntimeException("Could not inflate Behavior subclass " + str, e2);
        }
    }

    /* access modifiers changed from: package-private */
    public e a(View view) {
        e eVar = (e) view.getLayoutParams();
        if (!eVar.b) {
            if (view instanceof a) {
                b behavior = ((a) view).getBehavior();
                if (behavior == null) {
                    Log.e("CoordinatorLayout", "Attached behavior class is null");
                }
                eVar.a(behavior);
                eVar.b = true;
            } else {
                c cVar = null;
                for (Class<?> cls = view.getClass(); cls != null; cls = cls.getSuperclass()) {
                    cVar = (c) cls.getAnnotation(c.class);
                    if (cVar != null) {
                        break;
                    }
                }
                if (cVar != null) {
                    try {
                        eVar.a((b) cVar.a().getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                    } catch (Exception e2) {
                        Log.e("CoordinatorLayout", "Default behavior class " + cVar.a().getName() + " could not be instantiated. Did you forget" + " a default constructor?", e2);
                    }
                }
                eVar.b = true;
            }
        }
        return eVar;
    }

    private void f() {
        this.g.clear();
        this.h.clear();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            e a2 = a(childAt);
            a2.b(this, childAt);
            this.h.addNode(childAt);
            for (int i3 = 0; i3 < childCount; i3++) {
                if (i3 != i2) {
                    View childAt2 = getChildAt(i3);
                    if (a2.a(this, childAt, childAt2)) {
                        if (!this.h.contains(childAt2)) {
                            this.h.addNode(childAt2);
                        }
                        this.h.addEdge(childAt2, childAt);
                    }
                }
            }
        }
        this.g.addAll(this.h.getSortedList());
        Collections.reverse(this.g);
    }

    /* access modifiers changed from: package-private */
    public void a(View view, Rect rect) {
        ViewGroupUtils.getDescendantRect(this, view, rect);
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumWidth() {
        return Math.max(super.getSuggestedMinimumWidth(), getPaddingLeft() + getPaddingRight());
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumHeight() {
        return Math.max(super.getSuggestedMinimumHeight(), getPaddingTop() + getPaddingBottom());
    }

    public void a(View view, int i2, int i3, int i4, int i5) {
        measureChildWithMargins(view, i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean z;
        int i4;
        int i5;
        int max;
        int combineMeasuredStates;
        int i6;
        f();
        a();
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        if (layoutDirection == 1) {
            z = true;
        } else {
            z = false;
        }
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        int i7 = paddingLeft + paddingRight;
        int i8 = paddingTop + paddingBottom;
        int suggestedMinimumWidth = getSuggestedMinimumWidth();
        int suggestedMinimumHeight = getSuggestedMinimumHeight();
        int i9 = 0;
        boolean z2 = this.t != null && ViewCompat.getFitsSystemWindows(this);
        int size3 = this.g.size();
        int i10 = 0;
        while (i10 < size3) {
            View view = this.g.get(i10);
            if (view.getVisibility() == 8) {
                combineMeasuredStates = i9;
                max = suggestedMinimumHeight;
                i6 = suggestedMinimumWidth;
            } else {
                e eVar = (e) view.getLayoutParams();
                int i11 = 0;
                if (eVar.e >= 0 && mode != 0) {
                    int b2 = b(eVar.e);
                    int absoluteGravity = GravityCompat.getAbsoluteGravity(d(eVar.c), layoutDirection) & 7;
                    if ((absoluteGravity == 3 && !z) || (absoluteGravity == 5 && z)) {
                        i11 = Math.max(0, (size - paddingRight) - b2);
                    } else if ((absoluteGravity == 5 && !z) || (absoluteGravity == 3 && z)) {
                        i11 = Math.max(0, b2 - paddingLeft);
                    }
                }
                if (!z2 || ViewCompat.getFitsSystemWindows(view)) {
                    i4 = i3;
                    i5 = i2;
                } else {
                    int systemWindowInsetLeft = this.t.getSystemWindowInsetLeft() + this.t.getSystemWindowInsetRight();
                    int systemWindowInsetTop = this.t.getSystemWindowInsetTop() + this.t.getSystemWindowInsetBottom();
                    i5 = View.MeasureSpec.makeMeasureSpec(size - systemWindowInsetLeft, mode);
                    i4 = View.MeasureSpec.makeMeasureSpec(size2 - systemWindowInsetTop, mode2);
                }
                b b3 = eVar.b();
                if (b3 == null || !b3.a(this, view, i5, i11, i4, 0)) {
                    a(view, i5, i11, i4, 0);
                }
                int max2 = Math.max(suggestedMinimumWidth, view.getMeasuredWidth() + i7 + eVar.leftMargin + eVar.rightMargin);
                max = Math.max(suggestedMinimumHeight, view.getMeasuredHeight() + i8 + eVar.topMargin + eVar.bottomMargin);
                combineMeasuredStates = View.combineMeasuredStates(i9, view.getMeasuredState());
                i6 = max2;
            }
            i10++;
            i9 = combineMeasuredStates;
            suggestedMinimumHeight = max;
            suggestedMinimumWidth = i6;
        }
        setMeasuredDimension(View.resolveSizeAndState(suggestedMinimumWidth, i2, -16777216 & i9), View.resolveSizeAndState(suggestedMinimumHeight, i3, i9 << 16));
    }

    private WindowInsetsCompat b(WindowInsetsCompat windowInsetsCompat) {
        WindowInsetsCompat windowInsetsCompat2;
        b b2;
        if (windowInsetsCompat.isConsumed()) {
            return windowInsetsCompat;
        }
        int childCount = getChildCount();
        int i2 = 0;
        WindowInsetsCompat windowInsetsCompat3 = windowInsetsCompat;
        while (true) {
            if (i2 >= childCount) {
                windowInsetsCompat2 = windowInsetsCompat3;
                break;
            }
            View childAt = getChildAt(i2);
            if (!ViewCompat.getFitsSystemWindows(childAt) || (b2 = ((e) childAt.getLayoutParams()).b()) == null) {
                windowInsetsCompat2 = windowInsetsCompat3;
            } else {
                windowInsetsCompat2 = b2.a(this, childAt, windowInsetsCompat3);
                if (windowInsetsCompat2.isConsumed()) {
                    break;
                }
            }
            i2++;
            windowInsetsCompat3 = windowInsetsCompat2;
        }
        return windowInsetsCompat2;
    }

    public void a(@NonNull View view, int i2) {
        e eVar = (e) view.getLayoutParams();
        if (eVar.d()) {
            throw new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
        } else if (eVar.k != null) {
            a(view, eVar.k, i2);
        } else if (eVar.e >= 0) {
            b(view, eVar.e, i2);
        } else {
            c(view, i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        b b2;
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        int size = this.g.size();
        for (int i6 = 0; i6 < size; i6++) {
            View view = this.g.get(i6);
            if (view.getVisibility() != 8 && ((b2 = ((e) view.getLayoutParams()).b()) == null || !b2.a(this, view, layoutDirection))) {
                a(view, layoutDirection);
            }
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.u && this.v != null) {
            int systemWindowInsetTop = this.t != null ? this.t.getSystemWindowInsetTop() : 0;
            if (systemWindowInsetTop > 0) {
                this.v.setBounds(0, 0, getWidth(), systemWindowInsetTop);
                this.v.draw(canvas);
            }
        }
    }

    public void setFitsSystemWindows(boolean z) {
        super.setFitsSystemWindows(z);
        g();
    }

    /* access modifiers changed from: package-private */
    public void b(View view, Rect rect) {
        ((e) view.getLayoutParams()).a(rect);
    }

    /* access modifiers changed from: package-private */
    public void c(View view, Rect rect) {
        rect.set(((e) view.getLayoutParams()).c());
    }

    /* access modifiers changed from: package-private */
    public void a(View view, boolean z, Rect rect) {
        if (view.isLayoutRequested() || view.getVisibility() == 8) {
            rect.setEmpty();
        } else if (z) {
            a(view, rect);
        } else {
            rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        }
    }

    private void a(View view, int i2, Rect rect, Rect rect2, e eVar, int i3, int i4) {
        int width;
        int height;
        int absoluteGravity = GravityCompat.getAbsoluteGravity(e(eVar.c), i2);
        int absoluteGravity2 = GravityCompat.getAbsoluteGravity(c(eVar.d), i2);
        int i5 = absoluteGravity & 7;
        int i6 = absoluteGravity & 112;
        int i7 = absoluteGravity2 & 112;
        switch (absoluteGravity2 & 7) {
            case 1:
                width = (rect.width() / 2) + rect.left;
                break;
            case 5:
                width = rect.right;
                break;
            default:
                width = rect.left;
                break;
        }
        switch (i7) {
            case 16:
                height = rect.top + (rect.height() / 2);
                break;
            case 80:
                height = rect.bottom;
                break;
            default:
                height = rect.top;
                break;
        }
        switch (i5) {
            case 1:
                width -= i3 / 2;
                break;
            case 5:
                break;
            default:
                width -= i3;
                break;
        }
        switch (i6) {
            case 16:
                height -= i4 / 2;
                break;
            case 80:
                break;
            default:
                height -= i4;
                break;
        }
        rect2.set(width, height, width + i3, height + i4);
    }

    private void a(e eVar, Rect rect, int i2, int i3) {
        int width = getWidth();
        int height = getHeight();
        int max = Math.max(getPaddingLeft() + eVar.leftMargin, Math.min(rect.left, ((width - getPaddingRight()) - i2) - eVar.rightMargin));
        int max2 = Math.max(getPaddingTop() + eVar.topMargin, Math.min(rect.top, ((height - getPaddingBottom()) - i3) - eVar.bottomMargin));
        rect.set(max, max2, max + i2, max2 + i3);
    }

    /* access modifiers changed from: package-private */
    public void a(View view, int i2, Rect rect, Rect rect2) {
        e eVar = (e) view.getLayoutParams();
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        a(view, i2, rect, rect2, eVar, measuredWidth, measuredHeight);
        a(eVar, rect2, measuredWidth, measuredHeight);
    }

    private void a(View view, View view2, int i2) {
        Rect e2 = e();
        Rect e3 = e();
        try {
            a(view2, e2);
            a(view, i2, e2, e3);
            view.layout(e3.left, e3.top, e3.right, e3.bottom);
        } finally {
            a(e2);
            a(e3);
        }
    }

    private void b(View view, int i2, int i3) {
        e eVar = (e) view.getLayoutParams();
        int absoluteGravity = GravityCompat.getAbsoluteGravity(d(eVar.c), i3);
        int i4 = absoluteGravity & 7;
        int i5 = absoluteGravity & 112;
        int width = getWidth();
        int height = getHeight();
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        if (i3 == 1) {
            i2 = width - i2;
        }
        int b2 = b(i2) - measuredWidth;
        int i6 = 0;
        switch (i4) {
            case 1:
                b2 += measuredWidth / 2;
                break;
            case 5:
                b2 += measuredWidth;
                break;
        }
        switch (i5) {
            case 16:
                i6 = 0 + (measuredHeight / 2);
                break;
            case 80:
                i6 = 0 + measuredHeight;
                break;
        }
        int max = Math.max(getPaddingLeft() + eVar.leftMargin, Math.min(b2, ((width - getPaddingRight()) - measuredWidth) - eVar.rightMargin));
        int max2 = Math.max(getPaddingTop() + eVar.topMargin, Math.min(i6, ((height - getPaddingBottom()) - measuredHeight) - eVar.bottomMargin));
        view.layout(max, max2, max + measuredWidth, max2 + measuredHeight);
    }

    private void c(View view, int i2) {
        e eVar = (e) view.getLayoutParams();
        Rect e2 = e();
        e2.set(getPaddingLeft() + eVar.leftMargin, getPaddingTop() + eVar.topMargin, (getWidth() - getPaddingRight()) - eVar.rightMargin, (getHeight() - getPaddingBottom()) - eVar.bottomMargin);
        if (this.t != null && ViewCompat.getFitsSystemWindows(this) && !ViewCompat.getFitsSystemWindows(view)) {
            e2.left += this.t.getSystemWindowInsetLeft();
            e2.top += this.t.getSystemWindowInsetTop();
            e2.right -= this.t.getSystemWindowInsetRight();
            e2.bottom -= this.t.getSystemWindowInsetBottom();
        }
        Rect e3 = e();
        GravityCompat.apply(c(eVar.c), view.getMeasuredWidth(), view.getMeasuredHeight(), e2, e3, i2);
        view.layout(e3.left, e3.top, e3.right, e3.bottom);
        a(e2);
        a(e3);
    }

    private static int c(int i2) {
        int i3;
        if ((i2 & 7) == 0) {
            i3 = 8388611 | i2;
        } else {
            i3 = i2;
        }
        if ((i3 & 112) == 0) {
            return i3 | 48;
        }
        return i3;
    }

    private static int d(int i2) {
        if (i2 == 0) {
            return 8388661;
        }
        return i2;
    }

    private static int e(int i2) {
        if (i2 == 0) {
            return 17;
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        e eVar = (e) view.getLayoutParams();
        if (eVar.f310a != null) {
            float d2 = eVar.f310a.d(this, view);
            if (d2 > 0.0f) {
                if (this.l == null) {
                    this.l = new Paint();
                }
                this.l.setColor(eVar.f310a.c(this, view));
                this.l.setAlpha(a(Math.round(d2 * 255.0f), 0, 255));
                int save = canvas.save();
                if (view.isOpaque()) {
                    canvas.clipRect((float) view.getLeft(), (float) view.getTop(), (float) view.getRight(), (float) view.getBottom(), Region.Op.DIFFERENCE);
                }
                canvas.drawRect((float) getPaddingLeft(), (float) getPaddingTop(), (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()), this.l);
                canvas.restoreToCount(save);
            }
        }
        return super.drawChild(canvas, view, j2);
    }

    private static int a(int i2, int i3, int i4) {
        if (i2 < i3) {
            return i3;
        }
        return i2 > i4 ? i4 : i2;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        boolean z;
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        int size = this.g.size();
        Rect e2 = e();
        Rect e3 = e();
        Rect e4 = e();
        for (int i3 = 0; i3 < size; i3++) {
            View view = this.g.get(i3);
            e eVar = (e) view.getLayoutParams();
            if (i2 != 0 || view.getVisibility() != 8) {
                for (int i4 = 0; i4 < i3; i4++) {
                    if (eVar.l == this.g.get(i4)) {
                        b(view, layoutDirection);
                    }
                }
                a(view, true, e3);
                if (eVar.g != 0 && !e3.isEmpty()) {
                    int absoluteGravity = GravityCompat.getAbsoluteGravity(eVar.g, layoutDirection);
                    switch (absoluteGravity & 112) {
                        case 48:
                            e2.top = Math.max(e2.top, e3.bottom);
                            break;
                        case 80:
                            e2.bottom = Math.max(e2.bottom, getHeight() - e3.top);
                            break;
                    }
                    switch (absoluteGravity & 7) {
                        case 3:
                            e2.left = Math.max(e2.left, e3.right);
                            break;
                        case 5:
                            e2.right = Math.max(e2.right, getWidth() - e3.left);
                            break;
                    }
                }
                if (eVar.h != 0 && view.getVisibility() == 0) {
                    a(view, e2, layoutDirection);
                }
                if (i2 != 2) {
                    c(view, e4);
                    if (!e4.equals(e3)) {
                        b(view, e3);
                    }
                }
                for (int i5 = i3 + 1; i5 < size; i5++) {
                    View view2 = this.g.get(i5);
                    e eVar2 = (e) view2.getLayoutParams();
                    b b2 = eVar2.b();
                    if (b2 != null && b2.a(this, view2, view)) {
                        if (i2 != 0 || !eVar2.g()) {
                            switch (i2) {
                                case 2:
                                    b2.c(this, view2, view);
                                    z = true;
                                    break;
                                default:
                                    z = b2.b(this, view2, view);
                                    break;
                            }
                            if (i2 == 1) {
                                eVar2.a(z);
                            }
                        } else {
                            eVar2.h();
                        }
                    }
                }
            }
        }
        a(e2);
        a(e3);
        a(e4);
    }

    private void a(View view, Rect rect, int i2) {
        boolean z;
        boolean z2;
        boolean z3;
        int width;
        int i3;
        int height;
        int i4;
        if (ViewCompat.isLaidOut(view) && view.getWidth() > 0 && view.getHeight() > 0) {
            e eVar = (e) view.getLayoutParams();
            b b2 = eVar.b();
            Rect e2 = e();
            Rect e3 = e();
            e3.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            if (b2 == null || !b2.a(this, view, e2)) {
                e2.set(e3);
            } else if (!e3.contains(e2)) {
                throw new IllegalArgumentException("Rect should be within the child's bounds. Rect:" + e2.toShortString() + " | Bounds:" + e3.toShortString());
            }
            a(e3);
            if (e2.isEmpty()) {
                a(e2);
                return;
            }
            int absoluteGravity = GravityCompat.getAbsoluteGravity(eVar.h, i2);
            if ((absoluteGravity & 48) != 48 || (i4 = (e2.top - eVar.topMargin) - eVar.j) >= rect.top) {
                z = false;
            } else {
                e(view, rect.top - i4);
                z = true;
            }
            if ((absoluteGravity & 80) == 80 && (height = ((getHeight() - e2.bottom) - eVar.bottomMargin) + eVar.j) < rect.bottom) {
                e(view, height - rect.bottom);
                z = true;
            }
            if (!z) {
                e(view, 0);
            }
            if ((absoluteGravity & 3) != 3 || (i3 = (e2.left - eVar.leftMargin) - eVar.i) >= rect.left) {
                z2 = false;
            } else {
                d(view, rect.left - i3);
                z2 = true;
            }
            if ((absoluteGravity & 5) != 5 || (width = eVar.i + ((getWidth() - e2.right) - eVar.rightMargin)) >= rect.right) {
                z3 = z2;
            } else {
                d(view, width - rect.right);
                z3 = true;
            }
            if (!z3) {
                d(view, 0);
            }
            a(e2);
        }
    }

    private void d(View view, int i2) {
        e eVar = (e) view.getLayoutParams();
        if (eVar.i != i2) {
            ViewCompat.offsetLeftAndRight(view, i2 - eVar.i);
            eVar.i = i2;
        }
    }

    private void e(View view, int i2) {
        e eVar = (e) view.getLayoutParams();
        if (eVar.j != i2) {
            ViewCompat.offsetTopAndBottom(view, i2 - eVar.j);
            eVar.j = i2;
        }
    }

    public void b(@NonNull View view) {
        List incomingEdges = this.h.getIncomingEdges(view);
        if (!(incomingEdges == null || incomingEdges.isEmpty())) {
            for (int i2 = 0; i2 < incomingEdges.size(); i2++) {
                View view2 = (View) incomingEdges.get(i2);
                b b2 = ((e) view2.getLayoutParams()).b();
                if (b2 != null) {
                    b2.b(this, view2, view);
                }
            }
        }
    }

    @NonNull
    public List<View> c(@NonNull View view) {
        List<View> outgoingEdges = this.h.getOutgoingEdges(view);
        this.j.clear();
        if (outgoingEdges != null) {
            this.j.addAll(outgoingEdges);
        }
        return this.j;
    }

    @NonNull
    public List<View> d(@NonNull View view) {
        List incomingEdges = this.h.getIncomingEdges(view);
        this.j.clear();
        if (incomingEdges != null) {
            this.j.addAll(incomingEdges);
        }
        return this.j;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final List<View> getDependencySortedChildren() {
        f();
        return Collections.unmodifiableList(this.g);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        boolean z = false;
        int childCount = getChildCount();
        int i2 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            } else if (e(getChildAt(i2))) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        if (z == this.s) {
            return;
        }
        if (z) {
            b();
        } else {
            c();
        }
    }

    private boolean e(View view) {
        return this.h.hasOutgoingEdges(view);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.n) {
            if (this.r == null) {
                this.r = new f();
            }
            getViewTreeObserver().addOnPreDrawListener(this.r);
        }
        this.s = true;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.n && this.r != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.r);
        }
        this.s = false;
    }

    /* access modifiers changed from: package-private */
    public void b(View view, int i2) {
        b b2;
        e eVar = (e) view.getLayoutParams();
        if (eVar.k != null) {
            Rect e2 = e();
            Rect e3 = e();
            Rect e4 = e();
            a(eVar.k, e2);
            a(view, false, e3);
            int measuredWidth = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();
            a(view, i2, e2, e4, eVar, measuredWidth, measuredHeight);
            boolean z = (e4.left == e3.left && e4.top == e3.top) ? false : true;
            a(eVar, e4, measuredWidth, measuredHeight);
            int i3 = e4.left - e3.left;
            int i4 = e4.top - e3.top;
            if (i3 != 0) {
                ViewCompat.offsetLeftAndRight(view, i3);
            }
            if (i4 != 0) {
                ViewCompat.offsetTopAndBottom(view, i4);
            }
            if (z && (b2 = eVar.b()) != null) {
                b2.b(this, view, eVar.k);
            }
            a(e2);
            a(e3);
            a(e4);
        }
    }

    public boolean a(@NonNull View view, int i2, int i3) {
        Rect e2 = e();
        a(view, e2);
        try {
            return e2.contains(i2, i3);
        } finally {
            a(e2);
        }
    }

    /* renamed from: a */
    public e generateLayoutParams(AttributeSet attributeSet) {
        return new e(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public e generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof e) {
            return new e((e) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new e((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new e(layoutParams);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public e generateDefaultLayoutParams() {
        return new e(-2, -2);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof e) && super.checkLayoutParams(layoutParams);
    }

    @Override // android.support.v4.view.NestedScrollingParent
    public boolean onStartNestedScroll(View view, View view2, int i2) {
        return onStartNestedScroll(view, view2, i2, 0);
    }

    @Override // android.support.v4.view.NestedScrollingParent2
    public boolean onStartNestedScroll(View view, View view2, int i2, int i3) {
        boolean z;
        boolean z2 = false;
        int childCount = getChildCount();
        int i4 = 0;
        while (i4 < childCount) {
            View childAt = getChildAt(i4);
            if (childAt.getVisibility() == 8) {
                z = z2;
            } else {
                e eVar = (e) childAt.getLayoutParams();
                b b2 = eVar.b();
                if (b2 != null) {
                    boolean a2 = b2.a(this, childAt, view, view2, i2, i3);
                    z = z2 | a2;
                    eVar.a(i3, a2);
                } else {
                    eVar.a(i3, false);
                    z = z2;
                }
            }
            i4++;
            z2 = z;
        }
        return z2;
    }

    @Override // android.support.v4.view.NestedScrollingParent
    public void onNestedScrollAccepted(View view, View view2, int i2) {
        onNestedScrollAccepted(view, view2, i2, 0);
    }

    @Override // android.support.v4.view.NestedScrollingParent2
    public void onNestedScrollAccepted(View view, View view2, int i2, int i3) {
        b b2;
        this.x.onNestedScrollAccepted(view, view2, i2, i3);
        this.q = view2;
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt = getChildAt(i4);
            e eVar = (e) childAt.getLayoutParams();
            if (eVar.b(i3) && (b2 = eVar.b()) != null) {
                b2.b(this, childAt, view, view2, i2, i3);
            }
        }
    }

    @Override // android.support.v4.view.NestedScrollingParent
    public void onStopNestedScroll(View view) {
        onStopNestedScroll(view, 0);
    }

    @Override // android.support.v4.view.NestedScrollingParent2
    public void onStopNestedScroll(View view, int i2) {
        this.x.onStopNestedScroll(view, i2);
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            e eVar = (e) childAt.getLayoutParams();
            if (eVar.b(i2)) {
                b b2 = eVar.b();
                if (b2 != null) {
                    b2.a(this, childAt, view, i2);
                }
                eVar.a(i2);
                eVar.h();
            }
        }
        this.q = null;
    }

    @Override // android.support.v4.view.NestedScrollingParent
    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        onNestedScroll(view, i2, i3, i4, i5, 0);
    }

    @Override // android.support.v4.view.NestedScrollingParent2
    public void onNestedScroll(View view, int i2, int i3, int i4, int i5, int i6) {
        boolean z;
        int childCount = getChildCount();
        boolean z2 = false;
        int i7 = 0;
        while (i7 < childCount) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() == 8) {
                z = z2;
            } else {
                e eVar = (e) childAt.getLayoutParams();
                if (!eVar.b(i6)) {
                    z = z2;
                } else {
                    b b2 = eVar.b();
                    if (b2 != null) {
                        b2.a(this, childAt, view, i2, i3, i4, i5, i6);
                        z = true;
                    } else {
                        z = z2;
                    }
                }
            }
            i7++;
            z2 = z;
        }
        if (z2) {
            a(1);
        }
    }

    @Override // android.support.v4.view.NestedScrollingParent
    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        onNestedPreScroll(view, i2, i3, iArr, 0);
    }

    @Override // android.support.v4.view.NestedScrollingParent2
    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr, int i4) {
        int i5;
        int i6;
        int i7 = 0;
        int i8 = 0;
        boolean z = false;
        int childCount = getChildCount();
        int i9 = 0;
        while (i9 < childCount) {
            View childAt = getChildAt(i9);
            if (childAt.getVisibility() == 8) {
                i5 = i8;
                i6 = i7;
            } else {
                e eVar = (e) childAt.getLayoutParams();
                if (!eVar.b(i4)) {
                    i5 = i8;
                    i6 = i7;
                } else {
                    b b2 = eVar.b();
                    if (b2 != null) {
                        int[] iArr2 = this.k;
                        this.k[1] = 0;
                        iArr2[0] = 0;
                        b2.a(this, childAt, view, i2, i3, this.k, i4);
                        if (i2 > 0) {
                            i6 = Math.max(i7, this.k[0]);
                        } else {
                            i6 = Math.min(i7, this.k[0]);
                        }
                        if (i3 > 0) {
                            i5 = Math.max(i8, this.k[1]);
                        } else {
                            i5 = Math.min(i8, this.k[1]);
                        }
                        z = true;
                    } else {
                        i5 = i8;
                        i6 = i7;
                    }
                }
            }
            i9++;
            i8 = i5;
            i7 = i6;
        }
        iArr[0] = i7;
        iArr[1] = i8;
        if (z) {
            a(1);
        }
    }

    @Override // android.support.v4.view.NestedScrollingParent
    public boolean onNestedFling(View view, float f2, float f3, boolean z) {
        boolean z2;
        int childCount = getChildCount();
        int i2 = 0;
        boolean z3 = false;
        while (i2 < childCount) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 8) {
                z2 = z3;
            } else {
                e eVar = (e) childAt.getLayoutParams();
                if (!eVar.b(0)) {
                    z2 = z3;
                } else {
                    b b2 = eVar.b();
                    if (b2 != null) {
                        z2 = b2.a(this, childAt, view, f2, f3, z) | z3;
                    } else {
                        z2 = z3;
                    }
                }
            }
            i2++;
            z3 = z2;
        }
        if (z3) {
            a(1);
        }
        return z3;
    }

    @Override // android.support.v4.view.NestedScrollingParent
    public boolean onNestedPreFling(View view, float f2, float f3) {
        boolean z;
        int childCount = getChildCount();
        int i2 = 0;
        boolean z2 = false;
        while (i2 < childCount) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 8) {
                z = z2;
            } else {
                e eVar = (e) childAt.getLayoutParams();
                if (!eVar.b(0)) {
                    z = z2;
                } else {
                    b b2 = eVar.b();
                    if (b2 != null) {
                        z = b2.a(this, childAt, view, f2, f3) | z2;
                    } else {
                        z = z2;
                    }
                }
            }
            i2++;
            z2 = z;
        }
        return z2;
    }

    @Override // android.support.v4.view.NestedScrollingParent
    public int getNestedScrollAxes() {
        return this.x.getNestedScrollAxes();
    }

    /* access modifiers changed from: package-private */
    public class f implements ViewTreeObserver.OnPreDrawListener {
        f() {
        }

        public boolean onPreDraw() {
            CoordinatorLayout.this.a(0);
            return true;
        }
    }

    static class g implements Comparator<View> {
        g() {
        }

        /* renamed from: a */
        public int compare(View view, View view2) {
            float z = ViewCompat.getZ(view);
            float z2 = ViewCompat.getZ(view2);
            if (z > z2) {
                return -1;
            }
            if (z < z2) {
                return 1;
            }
            return 0;
        }
    }

    public static abstract class b<V extends View> {
        public b() {
        }

        public b(Context context, AttributeSet attributeSet) {
        }

        public void a(@NonNull e eVar) {
        }

        public void c() {
        }

        public boolean b(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull MotionEvent motionEvent) {
            return false;
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull MotionEvent motionEvent) {
            return false;
        }

        @ColorInt
        public int c(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v) {
            return ViewCompat.MEASURED_STATE_MASK;
        }

        @FloatRange(from = 0.0d, to = 1.0d)
        public float d(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v) {
            return 0.0f;
        }

        public boolean e(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v) {
            return d(coordinatorLayout, v) > 0.0f;
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view) {
            return false;
        }

        public boolean b(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view) {
            return false;
        }

        public void c(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view) {
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, int i, int i2, int i3, int i4) {
            return false;
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, int i) {
            return false;
        }

        @Deprecated
        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, @NonNull View view2, int i) {
            return false;
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, @NonNull View view2, int i, int i2) {
            if (i2 == 0) {
                return a(coordinatorLayout, v, view, view2, i);
            }
            return false;
        }

        @Deprecated
        public void b(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, @NonNull View view2, int i) {
        }

        public void b(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, @NonNull View view2, int i, int i2) {
            if (i2 == 0) {
                b(coordinatorLayout, v, view, view2, i);
            }
        }

        @Deprecated
        public void d(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view) {
        }

        public void a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, int i) {
            if (i == 0) {
                d(coordinatorLayout, v, view);
            }
        }

        @Deprecated
        public void a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, int i, int i2, int i3, int i4) {
        }

        public void a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, int i, int i2, int i3, int i4, int i5) {
            if (i5 == 0) {
                a(coordinatorLayout, v, view, i, i2, i3, i4);
            }
        }

        @Deprecated
        public void a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, int i, int i2, @NonNull int[] iArr) {
        }

        public void a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, int i, int i2, @NonNull int[] iArr, int i3) {
            if (i3 == 0) {
                a(coordinatorLayout, v, view, i, i2, iArr);
            }
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, float f, float f2, boolean z) {
            return false;
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull View view, float f, float f2) {
            return false;
        }

        @NonNull
        public WindowInsetsCompat a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull WindowInsetsCompat windowInsetsCompat) {
            return windowInsetsCompat;
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull Rect rect, boolean z) {
            return false;
        }

        public void a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull Parcelable parcelable) {
        }

        @Nullable
        public Parcelable b(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v) {
            return View.BaseSavedState.EMPTY_STATE;
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull V v, @NonNull Rect rect) {
            return false;
        }
    }

    public static class e extends ViewGroup.MarginLayoutParams {

        /* renamed from: a  reason: collision with root package name */
        b f310a;
        boolean b = false;
        public int c = 0;
        public int d = 0;
        public int e = -1;
        int f = -1;
        public int g = 0;
        public int h = 0;
        int i;
        int j;
        View k;
        View l;
        final Rect m = new Rect();
        Object n;
        private boolean o;
        private boolean p;
        private boolean q;
        private boolean r;

        public e(int i2, int i3) {
            super(i2, i3);
        }

        e(@NonNull Context context, @Nullable AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CoordinatorLayout_Layout);
            this.c = obtainStyledAttributes.getInteger(R.styleable.CoordinatorLayout_Layout_android_layout_gravity, 0);
            this.f = obtainStyledAttributes.getResourceId(R.styleable.CoordinatorLayout_Layout_layout_anchor, -1);
            this.d = obtainStyledAttributes.getInteger(R.styleable.CoordinatorLayout_Layout_layout_anchorGravity, 0);
            this.e = obtainStyledAttributes.getInteger(R.styleable.CoordinatorLayout_Layout_layout_keyline, -1);
            this.g = obtainStyledAttributes.getInt(R.styleable.CoordinatorLayout_Layout_layout_insetEdge, 0);
            this.h = obtainStyledAttributes.getInt(R.styleable.CoordinatorLayout_Layout_layout_dodgeInsetEdges, 0);
            this.b = obtainStyledAttributes.hasValue(R.styleable.CoordinatorLayout_Layout_layout_behavior);
            if (this.b) {
                this.f310a = CoordinatorLayout.a(context, attributeSet, obtainStyledAttributes.getString(R.styleable.CoordinatorLayout_Layout_layout_behavior));
            }
            obtainStyledAttributes.recycle();
            if (this.f310a != null) {
                this.f310a.a(this);
            }
        }

        public e(e eVar) {
            super((ViewGroup.MarginLayoutParams) eVar);
        }

        public e(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public e(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        @IdRes
        public int a() {
            return this.f;
        }

        @Nullable
        public b b() {
            return this.f310a;
        }

        public void a(@Nullable b bVar) {
            if (this.f310a != bVar) {
                if (this.f310a != null) {
                    this.f310a.c();
                }
                this.f310a = bVar;
                this.n = null;
                this.b = true;
                if (bVar != null) {
                    bVar.a(this);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(Rect rect) {
            this.m.set(rect);
        }

        /* access modifiers changed from: package-private */
        public Rect c() {
            return this.m;
        }

        /* access modifiers changed from: package-private */
        public boolean d() {
            return this.k == null && this.f != -1;
        }

        /* access modifiers changed from: package-private */
        public boolean e() {
            if (this.f310a == null) {
                this.o = false;
            }
            return this.o;
        }

        /* access modifiers changed from: package-private */
        public boolean a(CoordinatorLayout coordinatorLayout, View view) {
            if (this.o) {
                return true;
            }
            boolean e2 = (this.f310a != null ? this.f310a.e(coordinatorLayout, view) : false) | this.o;
            this.o = e2;
            return e2;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.o = false;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            a(i2, false);
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, boolean z) {
            switch (i2) {
                case 0:
                    this.p = z;
                    return;
                case 1:
                    this.q = z;
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean b(int i2) {
            switch (i2) {
                case 0:
                    return this.p;
                case 1:
                    return this.q;
                default:
                    return false;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean g() {
            return this.r;
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            this.r = z;
        }

        /* access modifiers changed from: package-private */
        public void h() {
            this.r = false;
        }

        /* access modifiers changed from: package-private */
        public boolean a(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2 == this.l || a(view2, ViewCompat.getLayoutDirection(coordinatorLayout)) || (this.f310a != null && this.f310a.a(coordinatorLayout, view, view2));
        }

        /* access modifiers changed from: package-private */
        public View b(CoordinatorLayout coordinatorLayout, View view) {
            if (this.f == -1) {
                this.l = null;
                this.k = null;
                return null;
            }
            if (this.k == null || !b(view, coordinatorLayout)) {
                a(view, coordinatorLayout);
            }
            return this.k;
        }

        private void a(View view, CoordinatorLayout coordinatorLayout) {
            this.k = coordinatorLayout.findViewById(this.f);
            if (this.k != null) {
                if (this.k != coordinatorLayout) {
                    View view2 = this.k;
                    ViewParent parent = this.k.getParent();
                    while (parent != coordinatorLayout && parent != null) {
                        if (parent != view) {
                            if (parent instanceof View) {
                                view2 = (View) parent;
                            }
                            parent = parent.getParent();
                        } else if (coordinatorLayout.isInEditMode()) {
                            this.l = null;
                            this.k = null;
                            return;
                        } else {
                            throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                        }
                    }
                    this.l = view2;
                } else if (coordinatorLayout.isInEditMode()) {
                    this.l = null;
                    this.k = null;
                } else {
                    throw new IllegalStateException("View can not be anchored to the the parent CoordinatorLayout");
                }
            } else if (coordinatorLayout.isInEditMode()) {
                this.l = null;
                this.k = null;
            } else {
                throw new IllegalStateException("Could not find CoordinatorLayout descendant view with id " + coordinatorLayout.getResources().getResourceName(this.f) + " to anchor view " + view);
            }
        }

        private boolean b(View view, CoordinatorLayout coordinatorLayout) {
            if (this.k.getId() != this.f) {
                return false;
            }
            View view2 = this.k;
            for (ViewParent parent = this.k.getParent(); parent != coordinatorLayout; parent = parent.getParent()) {
                if (parent == null || parent == view) {
                    this.l = null;
                    this.k = null;
                    return false;
                }
                if (parent instanceof View) {
                    view2 = (View) parent;
                }
            }
            this.l = view2;
            return true;
        }

        private boolean a(View view, int i2) {
            int absoluteGravity = GravityCompat.getAbsoluteGravity(((e) view.getLayoutParams()).g, i2);
            return absoluteGravity != 0 && (GravityCompat.getAbsoluteGravity(this.h, i2) & absoluteGravity) == absoluteGravity;
        }
    }

    private class d implements ViewGroup.OnHierarchyChangeListener {
        d() {
        }

        public void onChildViewAdded(View view, View view2) {
            if (CoordinatorLayout.this.e != null) {
                CoordinatorLayout.this.e.onChildViewAdded(view, view2);
            }
        }

        public void onChildViewRemoved(View view, View view2) {
            CoordinatorLayout.this.a(2);
            if (CoordinatorLayout.this.e != null) {
                CoordinatorLayout.this.e.onChildViewRemoved(view, view2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        SparseArray<Parcelable> sparseArray = savedState.f308a;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            int id = childAt.getId();
            b b2 = a(childAt).b();
            if (!(id == -1 || b2 == null || (parcelable2 = sparseArray.get(id)) == null)) {
                b2.a(this, childAt, parcelable2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable b2;
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        SparseArray<Parcelable> sparseArray = new SparseArray<>();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            int id = childAt.getId();
            b b3 = ((e) childAt.getLayoutParams()).b();
            if (!(id == -1 || b3 == null || (b2 = b3.b(this, childAt)) == null)) {
                sparseArray.append(id, b2);
            }
        }
        savedState.f308a = sparseArray;
        return savedState;
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        b b2 = ((e) view.getLayoutParams()).b();
        if (b2 == null || !b2.a(this, view, rect, z)) {
            return super.requestChildRectangleOnScreen(view, rect, z);
        }
        return true;
    }

    private void g() {
        if (Build.VERSION.SDK_INT >= 21) {
            if (ViewCompat.getFitsSystemWindows(this)) {
                if (this.w == null) {
                    this.w = new OnApplyWindowInsetsListener() {
                        /* class android.support.design.widget.CoordinatorLayout.AnonymousClass1 */

                        @Override // android.support.v4.view.OnApplyWindowInsetsListener
                        public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                            return CoordinatorLayout.this.a(windowInsetsCompat);
                        }
                    };
                }
                ViewCompat.setOnApplyWindowInsetsListener(this, this.w);
                setSystemUiVisibility(1280);
                return;
            }
            ViewCompat.setOnApplyWindowInsetsListener(this, null);
        }
    }

    /* access modifiers changed from: protected */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            /* class android.support.design.widget.CoordinatorLayout.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        SparseArray<Parcelable> f308a;

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            int readInt = parcel.readInt();
            int[] iArr = new int[readInt];
            parcel.readIntArray(iArr);
            Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
            this.f308a = new SparseArray<>(readInt);
            for (int i = 0; i < readInt; i++) {
                this.f308a.append(iArr[i], readParcelableArray[i]);
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        @Override // android.support.v4.view.AbsSavedState
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            int size = this.f308a != null ? this.f308a.size() : 0;
            parcel.writeInt(size);
            int[] iArr = new int[size];
            Parcelable[] parcelableArr = new Parcelable[size];
            for (int i2 = 0; i2 < size; i2++) {
                iArr[i2] = this.f308a.keyAt(i2);
                parcelableArr[i2] = this.f308a.valueAt(i2);
            }
            parcel.writeIntArray(iArr);
            parcel.writeParcelableArray(parcelableArr, i);
        }
    }
}
