package android.support.design.widget;

import android.support.v4.view.ViewCompat;
import android.view.View;

class s {

    /* renamed from: a  reason: collision with root package name */
    private final View f367a;
    private int b;
    private int c;
    private int d;
    private int e;

    public s(View view) {
        this.f367a = view;
    }

    public void a() {
        this.b = this.f367a.getTop();
        this.c = this.f367a.getLeft();
        c();
    }

    private void c() {
        ViewCompat.offsetTopAndBottom(this.f367a, this.d - (this.f367a.getTop() - this.b));
        ViewCompat.offsetLeftAndRight(this.f367a, this.e - (this.f367a.getLeft() - this.c));
    }

    public boolean a(int i) {
        if (this.d == i) {
            return false;
        }
        this.d = i;
        c();
        return true;
    }

    public boolean b(int i) {
        if (this.e == i) {
            return false;
        }
        this.e = i;
        c();
        return true;
    }

    public int b() {
        return this.d;
    }
}
