package android.support.design.widget;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public class SwipeDismissBehavior<V extends View> extends CoordinatorLayout.b<V> {

    /* renamed from: a  reason: collision with root package name */
    ViewDragHelper f318a;
    a b;
    int c = 2;
    float d = 0.5f;
    float e = 0.0f;
    float f = 0.5f;
    private boolean g;
    private float h = 0.0f;
    private boolean i;
    private final ViewDragHelper.Callback j = new ViewDragHelper.Callback() {
        /* class android.support.design.widget.SwipeDismissBehavior.AnonymousClass1 */
        private int b;
        private int c = -1;

        @Override // android.support.v4.widget.ViewDragHelper.Callback
        public boolean tryCaptureView(View view, int i) {
            return this.c == -1 && SwipeDismissBehavior.this.a(view);
        }

        @Override // android.support.v4.widget.ViewDragHelper.Callback
        public void onViewCaptured(View view, int i) {
            this.c = i;
            this.b = view.getLeft();
            ViewParent parent = view.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }

        @Override // android.support.v4.widget.ViewDragHelper.Callback
        public void onViewDragStateChanged(int i) {
            if (SwipeDismissBehavior.this.b != null) {
                SwipeDismissBehavior.this.b.a(i);
            }
        }

        @Override // android.support.v4.widget.ViewDragHelper.Callback
        public void onViewReleased(View view, float f, float f2) {
            int i;
            boolean z;
            this.c = -1;
            int width = view.getWidth();
            if (a(view, f)) {
                int i2 = view.getLeft() < this.b ? this.b - width : this.b + width;
                z = true;
                i = i2;
            } else {
                i = this.b;
                z = false;
            }
            if (SwipeDismissBehavior.this.f318a.settleCapturedViewAt(i, view.getTop())) {
                ViewCompat.postOnAnimation(view, new b(view, z));
            } else if (z && SwipeDismissBehavior.this.b != null) {
                SwipeDismissBehavior.this.b.a(view);
            }
        }

        private boolean a(View view, float f) {
            if (f != 0.0f) {
                boolean z = ViewCompat.getLayoutDirection(view) == 1;
                if (SwipeDismissBehavior.this.c == 2) {
                    return true;
                }
                if (SwipeDismissBehavior.this.c == 0) {
                    return z ? f < 0.0f : f > 0.0f;
                }
                if (SwipeDismissBehavior.this.c == 1) {
                    return z ? f > 0.0f : f < 0.0f;
                }
                return false;
            }
            return Math.abs(view.getLeft() - this.b) >= Math.round(((float) view.getWidth()) * SwipeDismissBehavior.this.d);
        }

        @Override // android.support.v4.widget.ViewDragHelper.Callback
        public int getViewHorizontalDragRange(View view) {
            return view.getWidth();
        }

        @Override // android.support.v4.widget.ViewDragHelper.Callback
        public int clampViewPositionHorizontal(View view, int i, int i2) {
            int width;
            int width2;
            boolean z = ViewCompat.getLayoutDirection(view) == 1;
            if (SwipeDismissBehavior.this.c == 0) {
                if (z) {
                    width = this.b - view.getWidth();
                    width2 = this.b;
                } else {
                    width = this.b;
                    width2 = this.b + view.getWidth();
                }
            } else if (SwipeDismissBehavior.this.c != 1) {
                width = this.b - view.getWidth();
                width2 = this.b + view.getWidth();
            } else if (z) {
                width = this.b;
                width2 = this.b + view.getWidth();
            } else {
                width = this.b - view.getWidth();
                width2 = this.b;
            }
            return SwipeDismissBehavior.a(width, i, width2);
        }

        @Override // android.support.v4.widget.ViewDragHelper.Callback
        public int clampViewPositionVertical(View view, int i, int i2) {
            return view.getTop();
        }

        @Override // android.support.v4.widget.ViewDragHelper.Callback
        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
            float width = ((float) this.b) + (((float) view.getWidth()) * SwipeDismissBehavior.this.e);
            float width2 = ((float) this.b) + (((float) view.getWidth()) * SwipeDismissBehavior.this.f);
            if (((float) i) <= width) {
                view.setAlpha(1.0f);
            } else if (((float) i) >= width2) {
                view.setAlpha(0.0f);
            } else {
                view.setAlpha(SwipeDismissBehavior.a(0.0f, 1.0f - SwipeDismissBehavior.b(width, width2, (float) i), 1.0f));
            }
        }
    };

    public interface a {
        void a(int i);

        void a(View view);
    }

    public void a(a aVar) {
        this.b = aVar;
    }

    public void a(int i2) {
        this.c = i2;
    }

    public void a(float f2) {
        this.e = a(0.0f, f2, 1.0f);
    }

    public void b(float f2) {
        this.f = a(0.0f, f2, 1.0f);
    }

    @Override // android.support.design.widget.CoordinatorLayout.b
    public boolean b(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        boolean z = this.g;
        switch (motionEvent.getActionMasked()) {
            case 0:
                this.g = coordinatorLayout.a(v, (int) motionEvent.getX(), (int) motionEvent.getY());
                z = this.g;
                break;
            case 1:
            case 3:
                this.g = false;
                break;
        }
        if (!z) {
            return false;
        }
        a((ViewGroup) coordinatorLayout);
        return this.f318a.shouldInterceptTouchEvent(motionEvent);
    }

    @Override // android.support.design.widget.CoordinatorLayout.b
    public boolean a(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (this.f318a == null) {
            return false;
        }
        this.f318a.processTouchEvent(motionEvent);
        return true;
    }

    public boolean a(@NonNull View view) {
        return true;
    }

    private void a(ViewGroup viewGroup) {
        ViewDragHelper create;
        if (this.f318a == null) {
            if (this.i) {
                create = ViewDragHelper.create(viewGroup, this.h, this.j);
            } else {
                create = ViewDragHelper.create(viewGroup, this.j);
            }
            this.f318a = create;
        }
    }

    private class b implements Runnable {
        private final View b;
        private final boolean c;

        b(View view, boolean z) {
            this.b = view;
            this.c = z;
        }

        public void run() {
            if (SwipeDismissBehavior.this.f318a != null && SwipeDismissBehavior.this.f318a.continueSettling(true)) {
                ViewCompat.postOnAnimation(this.b, this);
            } else if (this.c && SwipeDismissBehavior.this.b != null) {
                SwipeDismissBehavior.this.b.a(this.b);
            }
        }
    }

    static float a(float f2, float f3, float f4) {
        return Math.min(Math.max(f2, f3), f4);
    }

    static int a(int i2, int i3, int i4) {
        return Math.min(Math.max(i2, i3), i4);
    }

    static float b(float f2, float f3, float f4) {
        return (f4 - f2) / (f3 - f2);
    }
}
