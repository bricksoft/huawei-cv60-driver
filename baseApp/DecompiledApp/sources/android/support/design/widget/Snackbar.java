package android.support.design.widget;

import android.content.Context;
import android.support.annotation.RestrictTo;
import android.support.design.R;
import android.support.design.widget.BaseTransientBottomBar;
import android.util.AttributeSet;
import android.view.View;
import com.google.android.exoplayer.C;

public final class Snackbar extends BaseTransientBottomBar<Snackbar> {
    private static final int[] d = {R.attr.snackbarButtonStyle};

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public static final class SnackbarLayout extends BaseTransientBottomBar.e {
        public SnackbarLayout(Context context) {
            super(context);
        }

        public SnackbarLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            int childCount = getChildCount();
            int measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = getChildAt(i3);
                if (childAt.getLayoutParams().width == -1) {
                    childAt.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth, C.ENCODING_PCM_32BIT), View.MeasureSpec.makeMeasureSpec(childAt.getMeasuredHeight(), C.ENCODING_PCM_32BIT));
                }
            }
        }
    }
}
