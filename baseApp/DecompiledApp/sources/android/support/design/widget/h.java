package android.support.design.widget;

import android.content.Context;
import android.support.v4.math.MathUtils;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.OverScroller;

abstract class h<V extends View> extends r<V> {

    /* renamed from: a  reason: collision with root package name */
    OverScroller f353a;
    private Runnable b;
    private boolean c;
    private int d = -1;
    private int e;
    private int f = -1;
    private VelocityTracker g;

    public h() {
    }

    public h(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.support.design.widget.CoordinatorLayout.b
    public boolean b(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        int findPointerIndex;
        if (this.f < 0) {
            this.f = ViewConfiguration.get(coordinatorLayout.getContext()).getScaledTouchSlop();
        }
        if (motionEvent.getAction() == 2 && this.c) {
            return true;
        }
        switch (motionEvent.getActionMasked()) {
            case 0:
                this.c = false;
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();
                if (c(v) && coordinatorLayout.a(v, x, y)) {
                    this.e = y;
                    this.d = motionEvent.getPointerId(0);
                    d();
                    break;
                }
            case 1:
            case 3:
                this.c = false;
                this.d = -1;
                if (this.g != null) {
                    this.g.recycle();
                    this.g = null;
                    break;
                }
                break;
            case 2:
                int i = this.d;
                if (!(i == -1 || (findPointerIndex = motionEvent.findPointerIndex(i)) == -1)) {
                    int y2 = (int) motionEvent.getY(findPointerIndex);
                    if (Math.abs(y2 - this.e) > this.f) {
                        this.c = true;
                        this.e = y2;
                        break;
                    }
                }
                break;
        }
        if (this.g != null) {
            this.g.addMovement(motionEvent);
        }
        return this.c;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ad  */
    @Override // android.support.design.widget.CoordinatorLayout.b
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.support.design.widget.CoordinatorLayout r10, V r11, android.view.MotionEvent r12) {
        /*
        // Method dump skipped, instructions count: 196
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.h.a(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: package-private */
    public int a_(CoordinatorLayout coordinatorLayout, V v, int i) {
        return a(coordinatorLayout, (View) v, i, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    /* access modifiers changed from: package-private */
    public int a(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3) {
        int clamp;
        int b2 = b();
        if (i2 == 0 || b2 < i2 || b2 > i3 || b2 == (clamp = MathUtils.clamp(i, i2, i3))) {
            return 0;
        }
        a(clamp);
        return b2 - clamp;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return b();
    }

    /* access modifiers changed from: package-private */
    public final int b(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3) {
        return a(coordinatorLayout, (View) v, a() - i, i2, i3);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(CoordinatorLayout coordinatorLayout, V v, int i, int i2, float f2) {
        if (this.b != null) {
            v.removeCallbacks(this.b);
            this.b = null;
        }
        if (this.f353a == null) {
            this.f353a = new OverScroller(v.getContext());
        }
        this.f353a.fling(0, b(), 0, Math.round(f2), 0, 0, i, i2);
        if (this.f353a.computeScrollOffset()) {
            this.b = new a(coordinatorLayout, v);
            ViewCompat.postOnAnimation(v, this.b);
            return true;
        }
        a(coordinatorLayout, v);
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(CoordinatorLayout coordinatorLayout, V v) {
    }

    /* access modifiers changed from: package-private */
    public boolean c(V v) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public int b(V v) {
        return -v.getHeight();
    }

    /* access modifiers changed from: package-private */
    public int a(V v) {
        return v.getHeight();
    }

    private void d() {
        if (this.g == null) {
            this.g = VelocityTracker.obtain();
        }
    }

    /* access modifiers changed from: private */
    public class a implements Runnable {
        private final CoordinatorLayout b;
        private final V c;

        a(CoordinatorLayout coordinatorLayout, V v) {
            this.b = coordinatorLayout;
            this.c = v;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v6, resolved type: android.support.design.widget.h */
        /* JADX DEBUG: Multi-variable search result rejected for r0v7, resolved type: android.support.design.widget.h */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            if (this.c != null && h.this.f353a != null) {
                if (h.this.f353a.computeScrollOffset()) {
                    h.this.a_(this.b, this.c, h.this.f353a.getCurrY());
                    ViewCompat.postOnAnimation(this.c, this);
                    return;
                }
                h.this.a(this.b, this.c);
            }
        }
    }
}
