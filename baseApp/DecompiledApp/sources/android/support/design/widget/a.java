package android.support.design.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.Dimension;
import android.support.annotation.FloatRange;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.v4.graphics.ColorUtils;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class a extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    final Paint f338a;
    final Rect b;
    final RectF c;
    final C0016a d;
    @Dimension
    float e;
    @ColorInt
    private int f;
    @ColorInt
    private int g;
    @ColorInt
    private int h;
    @ColorInt
    private int i;
    private ColorStateList j;
    @ColorInt
    private int k;
    private boolean l;
    @FloatRange(from = 0.0d, to = 360.0d)
    private float m;

    @Nullable
    public Drawable.ConstantState getConstantState() {
        return this.d;
    }

    public void draw(Canvas canvas) {
        if (this.l) {
            this.f338a.setShader(a());
            this.l = false;
        }
        float strokeWidth = this.f338a.getStrokeWidth() / 2.0f;
        RectF rectF = this.c;
        copyBounds(this.b);
        rectF.set(this.b);
        rectF.left += strokeWidth;
        rectF.top += strokeWidth;
        rectF.right -= strokeWidth;
        rectF.bottom -= strokeWidth;
        canvas.save();
        canvas.rotate(this.m, rectF.centerX(), rectF.centerY());
        canvas.drawOval(rectF, this.f338a);
        canvas.restore();
    }

    public boolean getPadding(Rect rect) {
        int round = Math.round(this.e);
        rect.set(round, round, round, round);
        return true;
    }

    public void setAlpha(@IntRange(from = 0, to = 255) int i2) {
        this.f338a.setAlpha(i2);
        invalidateSelf();
    }

    public void a(ColorStateList colorStateList) {
        if (colorStateList != null) {
            this.k = colorStateList.getColorForState(getState(), this.k);
        }
        this.j = colorStateList;
        this.l = true;
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f338a.setColorFilter(colorFilter);
        invalidateSelf();
    }

    public int getOpacity() {
        return this.e > 0.0f ? -3 : -2;
    }

    public final void a(float f2) {
        if (f2 != this.m) {
            this.m = f2;
            invalidateSelf();
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.l = true;
    }

    public boolean isStateful() {
        return (this.j != null && this.j.isStateful()) || super.isStateful();
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        int colorForState;
        if (!(this.j == null || (colorForState = this.j.getColorForState(iArr, this.k)) == this.k)) {
            this.l = true;
            this.k = colorForState;
        }
        if (this.l) {
            invalidateSelf();
        }
        return this.l;
    }

    private Shader a() {
        Rect rect = this.b;
        copyBounds(rect);
        float height = this.e / ((float) rect.height());
        return new LinearGradient(0.0f, (float) rect.top, 0.0f, (float) rect.bottom, new int[]{ColorUtils.compositeColors(this.f, this.k), ColorUtils.compositeColors(this.g, this.k), ColorUtils.compositeColors(ColorUtils.setAlphaComponent(this.g, 0), this.k), ColorUtils.compositeColors(ColorUtils.setAlphaComponent(this.i, 0), this.k), ColorUtils.compositeColors(this.i, this.k), ColorUtils.compositeColors(this.h, this.k)}, new float[]{0.0f, height, 0.5f, 0.5f, 1.0f - height, 1.0f}, Shader.TileMode.CLAMP);
    }

    /* renamed from: android.support.design.widget.a$a  reason: collision with other inner class name */
    private class C0016a extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ a f339a;

        @NonNull
        public Drawable newDrawable() {
            return this.f339a;
        }

        public int getChangingConfigurations() {
            return 0;
        }
    }
}
