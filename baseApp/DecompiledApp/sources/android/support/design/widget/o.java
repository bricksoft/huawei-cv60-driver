package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.support.annotation.RestrictTo;
import android.util.StateSet;
import java.util.ArrayList;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public final class o {

    /* renamed from: a  reason: collision with root package name */
    ValueAnimator f362a = null;
    private final ArrayList<a> b = new ArrayList<>();
    private a c = null;
    private final Animator.AnimatorListener d = new AnimatorListenerAdapter() {
        /* class android.support.design.widget.o.AnonymousClass1 */

        public void onAnimationEnd(Animator animator) {
            if (o.this.f362a == animator) {
                o.this.f362a = null;
            }
        }
    };

    public void a(int[] iArr, ValueAnimator valueAnimator) {
        a aVar = new a(iArr, valueAnimator);
        valueAnimator.addListener(this.d);
        this.b.add(aVar);
    }

    public void a(int[] iArr) {
        a aVar;
        int size = this.b.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                aVar = null;
                break;
            }
            aVar = this.b.get(i);
            if (StateSet.stateSetMatches(aVar.f364a, iArr)) {
                break;
            }
            i++;
        }
        if (aVar != this.c) {
            if (this.c != null) {
                b();
            }
            this.c = aVar;
            if (aVar != null) {
                a(aVar);
            }
        }
    }

    private void a(a aVar) {
        this.f362a = aVar.b;
        this.f362a.start();
    }

    private void b() {
        if (this.f362a != null) {
            this.f362a.cancel();
            this.f362a = null;
        }
    }

    public void a() {
        if (this.f362a != null) {
            this.f362a.end();
            this.f362a = null;
        }
    }

    /* access modifiers changed from: package-private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final int[] f364a;
        final ValueAnimator b;

        a(int[] iArr, ValueAnimator valueAnimator) {
            this.f364a = iArr;
            this.b = valueAnimator;
        }
    }
}
