package android.support.design.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.VisibleForTesting;
import android.support.design.R;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.math.MathUtils;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.NestedScrollingChild;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import java.lang.ref.WeakReference;
import java.util.List;
import tv.danmaku.ijk.media.player.IjkMediaCodecInfo;

@CoordinatorLayout.c(a = Behavior.class)
public class AppBarLayout extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private int f282a;
    private int b;
    private int c;
    private boolean d;
    private int e;
    private WindowInsetsCompat f;
    private List<a> g;
    private boolean h;
    private boolean i;
    private boolean j;
    private boolean k;
    private int[] l;

    public interface a<T extends AppBarLayout> {
        void a(T t, int i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        g();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        g();
        this.d = false;
        int childCount = getChildCount();
        int i6 = 0;
        while (true) {
            if (i6 >= childCount) {
                break;
            } else if (((b) getChildAt(i6).getLayoutParams()).b() != null) {
                this.d = true;
                break;
            } else {
                i6++;
            }
        }
        if (!this.h) {
            b(this.k || f());
        }
    }

    private boolean f() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (((b) getChildAt(i2).getLayoutParams()).c()) {
                return true;
            }
        }
        return false;
    }

    private void g() {
        this.f282a = -1;
        this.b = -1;
        this.c = -1;
    }

    public void setOrientation(int i2) {
        if (i2 != 1) {
            throw new IllegalArgumentException("AppBarLayout is always vertical and does not support horizontal orientation");
        }
        super.setOrientation(i2);
    }

    public void setExpanded(boolean z) {
        a(z, ViewCompat.isLaidOut(this));
    }

    public void a(boolean z, boolean z2) {
        a(z, z2, true);
    }

    private void a(boolean z, boolean z2, boolean z3) {
        int i2 = 0;
        int i3 = (z2 ? 4 : 0) | (z ? 1 : 2);
        if (z3) {
            i2 = 8;
        }
        this.e = i2 | i3;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public b generateDefaultLayoutParams() {
        return new b(-1, -2);
    }

    /* renamed from: a */
    public b generateLayoutParams(AttributeSet attributeSet) {
        return new b(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public b generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (Build.VERSION.SDK_INT >= 19 && (layoutParams instanceof LinearLayout.LayoutParams)) {
            return new b((LinearLayout.LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new b((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new b(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.d;
    }

    public final int getTotalScrollRange() {
        int i2;
        if (this.f282a != -1) {
            return this.f282a;
        }
        int childCount = getChildCount();
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= childCount) {
                break;
            }
            View childAt = getChildAt(i3);
            b bVar = (b) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = bVar.f285a;
            if ((i5 & 1) == 0) {
                break;
            }
            i4 += bVar.bottomMargin + measuredHeight + bVar.topMargin;
            if ((i5 & 2) != 0) {
                i2 = i4 - ViewCompat.getMinimumHeight(childAt);
                break;
            }
            i3++;
        }
        i2 = i4;
        int max = Math.max(0, i2 - getTopInset());
        this.f282a = max;
        return max;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return getTotalScrollRange() != 0;
    }

    /* access modifiers changed from: package-private */
    public int getUpNestedPreScrollRange() {
        return getTotalScrollRange();
    }

    /* access modifiers changed from: package-private */
    public int getDownNestedPreScrollRange() {
        int i2;
        if (this.b != -1) {
            return this.b;
        }
        int childCount = getChildCount() - 1;
        int i3 = 0;
        while (childCount >= 0) {
            View childAt = getChildAt(childCount);
            b bVar = (b) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i4 = bVar.f285a;
            if ((i4 & 5) == 5) {
                int i5 = bVar.bottomMargin + bVar.topMargin + i3;
                if ((i4 & 8) != 0) {
                    i2 = i5 + ViewCompat.getMinimumHeight(childAt);
                } else if ((i4 & 2) != 0) {
                    i2 = i5 + (measuredHeight - ViewCompat.getMinimumHeight(childAt));
                } else {
                    i2 = i5 + (measuredHeight - getTopInset());
                }
            } else if (i3 > 0) {
                break;
            } else {
                i2 = i3;
            }
            childCount--;
            i3 = i2;
        }
        int max = Math.max(0, i3);
        this.b = max;
        return max;
    }

    /* access modifiers changed from: package-private */
    public int getDownNestedScrollRange() {
        int i2;
        if (this.c != -1) {
            return this.c;
        }
        int childCount = getChildCount();
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= childCount) {
                break;
            }
            View childAt = getChildAt(i3);
            b bVar = (b) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight() + bVar.topMargin + bVar.bottomMargin;
            int i5 = bVar.f285a;
            if ((i5 & 1) == 0) {
                break;
            }
            i4 += measuredHeight;
            if ((i5 & 2) != 0) {
                i2 = i4 - (ViewCompat.getMinimumHeight(childAt) + getTopInset());
                break;
            }
            i3++;
        }
        i2 = i4;
        int max = Math.max(0, i2);
        this.c = max;
        return max;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (this.g != null) {
            int size = this.g.size();
            for (int i3 = 0; i3 < size; i3++) {
                a aVar = this.g.get(i3);
                if (aVar != null) {
                    aVar.a(this, i2);
                }
            }
        }
    }

    public final int getMinimumHeightForVisibleOverlappingContent() {
        int topInset = getTopInset();
        int minimumHeight = ViewCompat.getMinimumHeight(this);
        if (minimumHeight != 0) {
            return (minimumHeight * 2) + topInset;
        }
        int childCount = getChildCount();
        int minimumHeight2 = childCount >= 1 ? ViewCompat.getMinimumHeight(getChildAt(childCount - 1)) : 0;
        if (minimumHeight2 != 0) {
            return (minimumHeight2 * 2) + topInset;
        }
        return getHeight() / 3;
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i2) {
        if (this.l == null) {
            this.l = new int[4];
        }
        int[] iArr = this.l;
        int[] onCreateDrawableState = super.onCreateDrawableState(iArr.length + i2);
        iArr[0] = this.i ? R.attr.state_liftable : -R.attr.state_liftable;
        iArr[1] = (!this.i || !this.j) ? -R.attr.state_lifted : R.attr.state_lifted;
        iArr[2] = this.i ? R.attr.state_collapsible : -R.attr.state_collapsible;
        iArr[3] = (!this.i || !this.j) ? -R.attr.state_collapsed : R.attr.state_collapsed;
        return mergeDrawableStates(onCreateDrawableState, iArr);
    }

    private boolean b(boolean z) {
        if (this.i == z) {
            return false;
        }
        this.i = z;
        refreshDrawableState();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(boolean z) {
        if (this.j == z) {
            return false;
        }
        this.j = z;
        refreshDrawableState();
        return true;
    }

    public void setLiftOnScroll(boolean z) {
        this.k = z;
    }

    public boolean d() {
        return this.k;
    }

    @Deprecated
    public void setTargetElevation(float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            t.a(this, f2);
        }
    }

    @Deprecated
    public float getTargetElevation() {
        return 0.0f;
    }

    /* access modifiers changed from: package-private */
    public int getPendingAction() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.e = 0;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final int getTopInset() {
        if (this.f != null) {
            return this.f.getSystemWindowInsetTop();
        }
        return 0;
    }

    public static class b extends LinearLayout.LayoutParams {

        /* renamed from: a  reason: collision with root package name */
        int f285a = 1;
        Interpolator b;

        public b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.AppBarLayout_Layout);
            this.f285a = obtainStyledAttributes.getInt(R.styleable.AppBarLayout_Layout_layout_scrollFlags, 0);
            if (obtainStyledAttributes.hasValue(R.styleable.AppBarLayout_Layout_layout_scrollInterpolator)) {
                this.b = AnimationUtils.loadInterpolator(context, obtainStyledAttributes.getResourceId(R.styleable.AppBarLayout_Layout_layout_scrollInterpolator, 0));
            }
            obtainStyledAttributes.recycle();
        }

        public b(int i, int i2) {
            super(i, i2);
        }

        public b(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public b(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        @RequiresApi(19)
        public b(LinearLayout.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public int a() {
            return this.f285a;
        }

        public Interpolator b() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public boolean c() {
            return (this.f285a & 1) == 1 && (this.f285a & 10) != 0;
        }
    }

    public static class Behavior extends BaseBehavior<AppBarLayout> {
        @Override // android.support.design.widget.AppBarLayout.BaseBehavior
        public /* bridge */ /* synthetic */ void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, Parcelable parcelable) {
            super.a(coordinatorLayout, appBarLayout, parcelable);
        }

        @Override // android.support.design.widget.AppBarLayout.BaseBehavior
        public /* bridge */ /* synthetic */ void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i) {
            super.a(coordinatorLayout, appBarLayout, view, i);
        }

        @Override // android.support.design.widget.AppBarLayout.BaseBehavior
        public /* bridge */ /* synthetic */ void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int i3, int i4, int i5) {
            super.a(coordinatorLayout, appBarLayout, view, i, i2, i3, i4, i5);
        }

        @Override // android.support.design.widget.AppBarLayout.BaseBehavior
        public /* bridge */ /* synthetic */ void a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int[] iArr, int i3) {
            super.a(coordinatorLayout, appBarLayout, view, i, i2, iArr, i3);
        }

        @Override // android.support.design.widget.r
        public /* bridge */ /* synthetic */ boolean a(int i) {
            return super.a(i);
        }

        @Override // android.support.design.widget.AppBarLayout.BaseBehavior
        public /* bridge */ /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i) {
            return super.a(coordinatorLayout, appBarLayout, i);
        }

        @Override // android.support.design.widget.AppBarLayout.BaseBehavior
        public /* bridge */ /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3, int i4) {
            return super.a(coordinatorLayout, appBarLayout, i, i2, i3, i4);
        }

        @Override // android.support.design.widget.AppBarLayout.BaseBehavior
        public /* bridge */ /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, View view2, int i, int i2) {
            return super.a(coordinatorLayout, appBarLayout, view, view2, i, i2);
        }

        @Override // android.support.design.widget.r
        public /* bridge */ /* synthetic */ int b() {
            return super.b();
        }

        @Override // android.support.design.widget.AppBarLayout.BaseBehavior
        public /* bridge */ /* synthetic */ Parcelable b(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            return super.b(coordinatorLayout, appBarLayout);
        }

        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }
    }

    /* access modifiers changed from: protected */
    public static class BaseBehavior<T extends AppBarLayout> extends h<T> {
        private int b;
        private int c;
        private ValueAnimator d;
        private int e = -1;
        private boolean f;
        private float g;
        private WeakReference<View> h;
        private a i;

        public static abstract class a<T extends AppBarLayout> {
            public abstract boolean a(@NonNull T t);
        }

        public BaseBehavior() {
        }

        public BaseBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public boolean a(CoordinatorLayout coordinatorLayout, T t, View view, View view2, int i2, int i3) {
            boolean z = (i2 & 2) != 0 && (t.d() || a(coordinatorLayout, t, view));
            if (z && this.d != null) {
                this.d.cancel();
            }
            this.h = null;
            this.c = i3;
            return z;
        }

        private boolean a(CoordinatorLayout coordinatorLayout, T t, View view) {
            return t.c() && coordinatorLayout.getHeight() - view.getHeight() <= t.getHeight();
        }

        public void a(CoordinatorLayout coordinatorLayout, T t, View view, int i2, int i3, int[] iArr, int i4) {
            int i5;
            int i6;
            if (i3 != 0) {
                if (i3 < 0) {
                    i5 = -t.getTotalScrollRange();
                    i6 = i5 + t.getDownNestedPreScrollRange();
                } else {
                    i5 = -t.getUpNestedPreScrollRange();
                    i6 = 0;
                }
                if (i5 != i6) {
                    iArr[1] = b(coordinatorLayout, t, i3, i5, i6);
                    a(i3, t, view, i4);
                }
            }
        }

        public void a(CoordinatorLayout coordinatorLayout, T t, View view, int i2, int i3, int i4, int i5, int i6) {
            boolean z = false;
            if (i5 < 0) {
                b(coordinatorLayout, t, i5, -t.getDownNestedScrollRange(), 0);
                a(i5, t, view, i6);
            }
            if (t.d()) {
                if (view.getScrollY() > 0) {
                    z = true;
                }
                t.a(z);
            }
        }

        private void a(int i2, T t, View view, int i3) {
            if (i3 == 1) {
                int a2 = a();
                if ((i2 < 0 && a2 == 0) || (i2 > 0 && a2 == (-t.getDownNestedScrollRange()))) {
                    ViewCompat.stopNestedScroll(view, 1);
                }
            }
        }

        public void a(CoordinatorLayout coordinatorLayout, T t, View view, int i2) {
            if (this.c == 0 || i2 == 1) {
                c(coordinatorLayout, (AppBarLayout) t);
            }
            this.h = new WeakReference<>(view);
        }

        private void a(CoordinatorLayout coordinatorLayout, T t, int i2, float f2) {
            int height;
            int abs = Math.abs(a() - i2);
            float abs2 = Math.abs(f2);
            if (abs2 > 0.0f) {
                height = Math.round((((float) abs) / abs2) * 1000.0f) * 3;
            } else {
                height = (int) (((((float) abs) / ((float) t.getHeight())) + 1.0f) * 150.0f);
            }
            a(coordinatorLayout, (AppBarLayout) t, i2, height);
        }

        private void a(final CoordinatorLayout coordinatorLayout, final T t, int i2, int i3) {
            int a2 = a();
            if (a2 != i2) {
                if (this.d == null) {
                    this.d = new ValueAnimator();
                    this.d.setInterpolator(android.support.design.a.a.e);
                    this.d.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        /* class android.support.design.widget.AppBarLayout.BaseBehavior.AnonymousClass1 */

                        public void onAnimationUpdate(ValueAnimator valueAnimator) {
                            BaseBehavior.this.a_(coordinatorLayout, t, ((Integer) valueAnimator.getAnimatedValue()).intValue());
                        }
                    });
                } else {
                    this.d.cancel();
                }
                this.d.setDuration((long) Math.min(i3, (int) IjkMediaCodecInfo.RANK_LAST_CHANCE));
                this.d.setIntValues(a2, i2);
                this.d.start();
            } else if (this.d != null && this.d.isRunning()) {
                this.d.cancel();
            }
        }

        private int a(T t, int i2) {
            int i3;
            int childCount = t.getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = t.getChildAt(i4);
                int top = childAt.getTop();
                int bottom = childAt.getBottom();
                b bVar = (b) childAt.getLayoutParams();
                if (a(bVar.a(), 32)) {
                    top -= bVar.topMargin;
                    i3 = bVar.bottomMargin + bottom;
                } else {
                    i3 = bottom;
                }
                if (top <= (-i2) && i3 >= (-i2)) {
                    return i4;
                }
            }
            return -1;
        }

        private void c(CoordinatorLayout coordinatorLayout, T t) {
            int a2 = a();
            int a3 = a(t, a2);
            if (a3 >= 0) {
                View childAt = t.getChildAt(a3);
                b bVar = (b) childAt.getLayoutParams();
                int a4 = bVar.a();
                if ((a4 & 17) == 17) {
                    int i2 = -childAt.getTop();
                    int i3 = -childAt.getBottom();
                    if (a3 == t.getChildCount() - 1) {
                        i3 += t.getTopInset();
                    }
                    if (a(a4, 2)) {
                        i3 += ViewCompat.getMinimumHeight(childAt);
                    } else if (a(a4, 5)) {
                        int minimumHeight = ViewCompat.getMinimumHeight(childAt) + i3;
                        if (a2 < minimumHeight) {
                            i2 = minimumHeight;
                        } else {
                            i3 = minimumHeight;
                        }
                    }
                    if (a(a4, 32)) {
                        i2 += bVar.topMargin;
                        i3 -= bVar.bottomMargin;
                    }
                    if (a2 >= (i3 + i2) / 2) {
                        i3 = i2;
                    }
                    a(coordinatorLayout, (AppBarLayout) t, MathUtils.clamp(i3, -t.getTotalScrollRange(), 0), 0.0f);
                }
            }
        }

        private static boolean a(int i2, int i3) {
            return (i2 & i3) == i3;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, T t, int i2, int i3, int i4, int i5) {
            if (((CoordinatorLayout.e) t.getLayoutParams()).height != -2) {
                return super.a(coordinatorLayout, (View) t, i2, i3, i4, i5);
            }
            coordinatorLayout.a(t, i2, i3, View.MeasureSpec.makeMeasureSpec(0, 0), i5);
            return true;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, T t, int i2) {
            boolean z;
            int round;
            boolean a2 = super.a(coordinatorLayout, (View) t, i2);
            int pendingAction = t.getPendingAction();
            if (this.e >= 0 && (pendingAction & 8) == 0) {
                View childAt = t.getChildAt(this.e);
                int i3 = -childAt.getBottom();
                if (this.f) {
                    round = ViewCompat.getMinimumHeight(childAt) + t.getTopInset() + i3;
                } else {
                    round = Math.round(((float) childAt.getHeight()) * this.g) + i3;
                }
                a_(coordinatorLayout, t, round);
            } else if (pendingAction != 0) {
                if ((pendingAction & 4) != 0) {
                    z = true;
                } else {
                    z = false;
                }
                if ((pendingAction & 2) != 0) {
                    int i4 = -t.getUpNestedPreScrollRange();
                    if (z) {
                        a(coordinatorLayout, (AppBarLayout) t, i4, 0.0f);
                    } else {
                        a_(coordinatorLayout, t, i4);
                    }
                } else if ((pendingAction & 1) != 0) {
                    if (z) {
                        a(coordinatorLayout, (AppBarLayout) t, 0, 0.0f);
                    } else {
                        a_(coordinatorLayout, t, 0);
                    }
                }
            }
            t.e();
            this.e = -1;
            a(MathUtils.clamp(b(), -t.getTotalScrollRange(), 0));
            a(coordinatorLayout, (AppBarLayout) t, b(), 0, true);
            t.a(b());
            return a2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public boolean c(T t) {
            if (this.i != null) {
                return this.i.a(t);
            }
            if (this.h == null) {
                return true;
            }
            View view = this.h.get();
            if (view == null || !view.isShown() || view.canScrollVertically(-1)) {
                return false;
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public void a(CoordinatorLayout coordinatorLayout, T t) {
            c(coordinatorLayout, (AppBarLayout) t);
        }

        /* access modifiers changed from: package-private */
        public int b(T t) {
            return -t.getDownNestedScrollRange();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public int a(T t) {
            return t.getTotalScrollRange();
        }

        /* access modifiers changed from: package-private */
        public int a(CoordinatorLayout coordinatorLayout, T t, int i2, int i3, int i4) {
            int i5;
            int a2 = a();
            if (i3 == 0 || a2 < i3 || a2 > i4) {
                this.b = 0;
                return 0;
            }
            int clamp = MathUtils.clamp(i2, i3, i4);
            if (a2 == clamp) {
                return 0;
            }
            int b2 = t.b() ? b(t, clamp) : clamp;
            boolean a3 = a(b2);
            int i6 = a2 - clamp;
            this.b = clamp - b2;
            if (!a3 && t.b()) {
                coordinatorLayout.b(t);
            }
            t.a(b());
            if (clamp < a2) {
                i5 = -1;
            } else {
                i5 = 1;
            }
            a(coordinatorLayout, (AppBarLayout) t, clamp, i5, false);
            return i6;
        }

        private int b(T t, int i2) {
            int i3;
            int abs = Math.abs(i2);
            int childCount = t.getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = t.getChildAt(i4);
                b bVar = (b) childAt.getLayoutParams();
                Interpolator b2 = bVar.b();
                if (abs >= childAt.getTop() && abs <= childAt.getBottom()) {
                    if (b2 == null) {
                        return i2;
                    } else {
                        int a2 = bVar.a();
                        if ((a2 & 1) != 0) {
                            i3 = bVar.bottomMargin + childAt.getHeight() + bVar.topMargin + 0;
                            if ((a2 & 2) != 0) {
                                i3 -= ViewCompat.getMinimumHeight(childAt);
                            }
                        } else {
                            i3 = 0;
                        }
                        if (ViewCompat.getFitsSystemWindows(childAt)) {
                            i3 -= t.getTopInset();
                        }
                        if (i3 <= 0) {
                            return i2;
                        }
                        return Integer.signum(i2) * (Math.round(b2.getInterpolation(((float) (abs - childAt.getTop())) / ((float) i3)) * ((float) i3)) + childAt.getTop());
                    }
                }
            }
            return i2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:20:0x004b  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0070  */
        /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void a(android.support.design.widget.CoordinatorLayout r7, T r8, int r9, int r10, boolean r11) {
            /*
            // Method dump skipped, instructions count: 116
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.AppBarLayout.BaseBehavior.a(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, boolean):void");
        }

        private boolean d(CoordinatorLayout coordinatorLayout, T t) {
            List<View> d2 = coordinatorLayout.d(t);
            int size = d2.size();
            for (int i2 = 0; i2 < size; i2++) {
                CoordinatorLayout.b b2 = ((CoordinatorLayout.e) d2.get(i2).getLayoutParams()).b();
                if (b2 instanceof ScrollingViewBehavior) {
                    if (((ScrollingViewBehavior) b2).d() != 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
            return false;
        }

        private static View c(AppBarLayout appBarLayout, int i2) {
            int abs = Math.abs(i2);
            int childCount = appBarLayout.getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = appBarLayout.getChildAt(i3);
                if (abs >= childAt.getTop() && abs <= childAt.getBottom()) {
                    return childAt;
                }
            }
            return null;
        }

        @Nullable
        private View a(CoordinatorLayout coordinatorLayout) {
            int childCount = coordinatorLayout.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = coordinatorLayout.getChildAt(i2);
                if (childAt instanceof NestedScrollingChild) {
                    return childAt;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        @Override // android.support.design.widget.h
        public int a() {
            return b() + this.b;
        }

        public Parcelable b(CoordinatorLayout coordinatorLayout, T t) {
            boolean z = false;
            Parcelable b2 = super.b(coordinatorLayout, (View) t);
            int b3 = b();
            int childCount = t.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = t.getChildAt(i2);
                int bottom = childAt.getBottom() + b3;
                if (childAt.getTop() + b3 <= 0 && bottom >= 0) {
                    SavedState savedState = new SavedState(b2);
                    savedState.f284a = i2;
                    if (bottom == ViewCompat.getMinimumHeight(childAt) + t.getTopInset()) {
                        z = true;
                    }
                    savedState.c = z;
                    savedState.b = ((float) bottom) / ((float) childAt.getHeight());
                    return savedState;
                }
            }
            return b2;
        }

        public void a(CoordinatorLayout coordinatorLayout, T t, Parcelable parcelable) {
            if (parcelable instanceof SavedState) {
                SavedState savedState = (SavedState) parcelable;
                super.a(coordinatorLayout, (View) t, savedState.getSuperState());
                this.e = savedState.f284a;
                this.g = savedState.b;
                this.f = savedState.c;
                return;
            }
            super.a(coordinatorLayout, (View) t, parcelable);
            this.e = -1;
        }

        /* access modifiers changed from: protected */
        public static class SavedState extends AbsSavedState {
            public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
                /* class android.support.design.widget.AppBarLayout.BaseBehavior.SavedState.AnonymousClass1 */

                /* renamed from: a */
                public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return new SavedState(parcel, classLoader);
                }

                /* renamed from: a */
                public SavedState createFromParcel(Parcel parcel) {
                    return new SavedState(parcel, null);
                }

                /* renamed from: a */
                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };

            /* renamed from: a  reason: collision with root package name */
            int f284a;
            float b;
            boolean c;

            public SavedState(Parcel parcel, ClassLoader classLoader) {
                super(parcel, classLoader);
                this.f284a = parcel.readInt();
                this.b = parcel.readFloat();
                this.c = parcel.readByte() != 0;
            }

            public SavedState(Parcelable parcelable) {
                super(parcelable);
            }

            @Override // android.support.v4.view.AbsSavedState
            public void writeToParcel(Parcel parcel, int i) {
                super.writeToParcel(parcel, i);
                parcel.writeInt(this.f284a);
                parcel.writeFloat(this.b);
                parcel.writeByte((byte) (this.c ? 1 : 0));
            }
        }
    }

    public static class ScrollingViewBehavior extends i {
        @Override // android.support.design.widget.r
        public /* bridge */ /* synthetic */ boolean a(int i) {
            return super.a(i);
        }

        @Override // android.support.design.widget.r, android.support.design.widget.CoordinatorLayout.b
        public /* bridge */ /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, int i) {
            return super.a(coordinatorLayout, view, i);
        }

        @Override // android.support.design.widget.CoordinatorLayout.b, android.support.design.widget.i
        public /* bridge */ /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
            return super.a(coordinatorLayout, view, i, i2, i3, i4);
        }

        @Override // android.support.design.widget.r
        public /* bridge */ /* synthetic */ int b() {
            return super.b();
        }

        public ScrollingViewBehavior() {
        }

        public ScrollingViewBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ScrollingViewBehavior_Layout);
            b(obtainStyledAttributes.getDimensionPixelSize(R.styleable.ScrollingViewBehavior_Layout_behavior_overlapTop, 0));
            obtainStyledAttributes.recycle();
        }

        @Override // android.support.design.widget.CoordinatorLayout.b
        public boolean a(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2 instanceof AppBarLayout;
        }

        @Override // android.support.design.widget.CoordinatorLayout.b
        public boolean b(CoordinatorLayout coordinatorLayout, View view, View view2) {
            a(view, view2);
            b(view, view2);
            return false;
        }

        @Override // android.support.design.widget.CoordinatorLayout.b
        public boolean a(CoordinatorLayout coordinatorLayout, View view, Rect rect, boolean z) {
            boolean z2;
            AppBarLayout a2 = b(coordinatorLayout.c(view));
            if (a2 != null) {
                rect.offset(view.getLeft(), view.getTop());
                Rect rect2 = this.f355a;
                rect2.set(0, 0, coordinatorLayout.getWidth(), coordinatorLayout.getHeight());
                if (!rect2.contains(rect)) {
                    if (!z) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    a2.a(false, z2);
                    return true;
                }
            }
            return false;
        }

        private void a(View view, View view2) {
            CoordinatorLayout.b b = ((CoordinatorLayout.e) view2.getLayoutParams()).b();
            if (b instanceof BaseBehavior) {
                ViewCompat.offsetTopAndBottom(view, ((((BaseBehavior) b).b + (view2.getBottom() - view.getTop())) + a()) - c(view2));
            }
        }

        /* access modifiers changed from: package-private */
        @Override // android.support.design.widget.i
        public float a(View view) {
            int i;
            if (!(view instanceof AppBarLayout)) {
                return 0.0f;
            }
            AppBarLayout appBarLayout = (AppBarLayout) view;
            int totalScrollRange = appBarLayout.getTotalScrollRange();
            int downNestedPreScrollRange = appBarLayout.getDownNestedPreScrollRange();
            int a2 = a(appBarLayout);
            if ((downNestedPreScrollRange == 0 || totalScrollRange + a2 > downNestedPreScrollRange) && (i = totalScrollRange - downNestedPreScrollRange) != 0) {
                return 1.0f + (((float) a2) / ((float) i));
            }
            return 0.0f;
        }

        private static int a(AppBarLayout appBarLayout) {
            CoordinatorLayout.b b = ((CoordinatorLayout.e) appBarLayout.getLayoutParams()).b();
            if (b instanceof BaseBehavior) {
                return ((BaseBehavior) b).a();
            }
            return 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public AppBarLayout b(List<View> list) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                View view = list.get(i);
                if (view instanceof AppBarLayout) {
                    return (AppBarLayout) view;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        @Override // android.support.design.widget.i
        public int b(View view) {
            if (view instanceof AppBarLayout) {
                return ((AppBarLayout) view).getTotalScrollRange();
            }
            return super.b(view);
        }

        private void b(View view, View view2) {
            if (view2 instanceof AppBarLayout) {
                AppBarLayout appBarLayout = (AppBarLayout) view2;
                if (appBarLayout.d()) {
                    appBarLayout.a(view.getScrollY() > 0);
                }
            }
        }
    }
}
