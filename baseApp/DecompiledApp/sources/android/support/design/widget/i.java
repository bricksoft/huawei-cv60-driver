package android.support.design.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.math.MathUtils;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.util.AttributeSet;
import android.view.View;
import com.google.android.exoplayer.C;
import java.util.List;

abstract class i extends r<View> {

    /* renamed from: a  reason: collision with root package name */
    final Rect f355a = new Rect();
    final Rect b = new Rect();
    private int c = 0;
    private int d;

    /* access modifiers changed from: package-private */
    public abstract View b(List<View> list);

    public i() {
    }

    public i(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.support.design.widget.CoordinatorLayout.b
    public boolean a(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
        View b2;
        int i5 = view.getLayoutParams().height;
        if ((i5 != -1 && i5 != -2) || (b2 = b(coordinatorLayout.c(view))) == null) {
            return false;
        }
        if (ViewCompat.getFitsSystemWindows(b2) && !ViewCompat.getFitsSystemWindows(view)) {
            ViewCompat.setFitsSystemWindows(view, true);
            if (ViewCompat.getFitsSystemWindows(view)) {
                view.requestLayout();
                return true;
            }
        }
        int size = View.MeasureSpec.getSize(i3);
        if (size == 0) {
            size = coordinatorLayout.getHeight();
        }
        coordinatorLayout.a(view, i, i2, View.MeasureSpec.makeMeasureSpec(b(b2) + (size - b2.getMeasuredHeight()), i5 == -1 ? C.ENCODING_PCM_32BIT : Integer.MIN_VALUE), i4);
        return true;
    }

    /* access modifiers changed from: protected */
    @Override // android.support.design.widget.r
    public void b(CoordinatorLayout coordinatorLayout, View view, int i) {
        View b2 = b(coordinatorLayout.c(view));
        if (b2 != null) {
            CoordinatorLayout.e eVar = (CoordinatorLayout.e) view.getLayoutParams();
            Rect rect = this.f355a;
            rect.set(coordinatorLayout.getPaddingLeft() + eVar.leftMargin, b2.getBottom() + eVar.topMargin, (coordinatorLayout.getWidth() - coordinatorLayout.getPaddingRight()) - eVar.rightMargin, ((coordinatorLayout.getHeight() + b2.getBottom()) - coordinatorLayout.getPaddingBottom()) - eVar.bottomMargin);
            WindowInsetsCompat lastWindowInsets = coordinatorLayout.getLastWindowInsets();
            if (lastWindowInsets != null && ViewCompat.getFitsSystemWindows(coordinatorLayout) && !ViewCompat.getFitsSystemWindows(view)) {
                rect.left += lastWindowInsets.getSystemWindowInsetLeft();
                rect.right -= lastWindowInsets.getSystemWindowInsetRight();
            }
            Rect rect2 = this.b;
            GravityCompat.apply(c(eVar.c), view.getMeasuredWidth(), view.getMeasuredHeight(), rect, rect2, i);
            int c2 = c(b2);
            view.layout(rect2.left, rect2.top - c2, rect2.right, rect2.bottom - c2);
            this.c = rect2.top - b2.getBottom();
            return;
        }
        super.b(coordinatorLayout, view, i);
        this.c = 0;
    }

    /* access modifiers changed from: package-private */
    public float a(View view) {
        return 1.0f;
    }

    /* access modifiers changed from: package-private */
    public final int c(View view) {
        if (this.d == 0) {
            return 0;
        }
        return MathUtils.clamp((int) (a(view) * ((float) this.d)), 0, this.d);
    }

    private static int c(int i) {
        if (i == 0) {
            return 8388659;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public int b(View view) {
        return view.getMeasuredHeight();
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        return this.c;
    }

    public final void b(int i) {
        this.d = i;
    }

    public final int d() {
        return this.d;
    }
}
