package android.support.design.widget;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

/* access modifiers changed from: package-private */
public class r<V extends View> extends CoordinatorLayout.b<V> {

    /* renamed from: a  reason: collision with root package name */
    private s f366a;
    private int b = 0;
    private int c = 0;

    public r() {
    }

    public r(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.support.design.widget.CoordinatorLayout.b
    public boolean a(CoordinatorLayout coordinatorLayout, V v, int i) {
        b(coordinatorLayout, v, i);
        if (this.f366a == null) {
            this.f366a = new s(v);
        }
        this.f366a.a();
        if (this.b != 0) {
            this.f366a.a(this.b);
            this.b = 0;
        }
        if (this.c == 0) {
            return true;
        }
        this.f366a.b(this.c);
        this.c = 0;
        return true;
    }

    /* access modifiers changed from: protected */
    public void b(CoordinatorLayout coordinatorLayout, V v, int i) {
        coordinatorLayout.a(v, i);
    }

    public boolean a(int i) {
        if (this.f366a != null) {
            return this.f366a.a(i);
        }
        this.b = i;
        return false;
    }

    public int b() {
        if (this.f366a != null) {
            return this.f366a.b();
        }
        return 0;
    }
}
