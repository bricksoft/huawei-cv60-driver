package android.support.design.e;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.annotation.StyleableRes;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class a {
    @Nullable
    public static ColorStateList a(Context context, TypedArray typedArray, @StyleableRes int i) {
        int resourceId;
        ColorStateList a2;
        return (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0 || (a2 = android.support.v7.a.a.a.a(context, resourceId)) == null) ? typedArray.getColorStateList(i) : a2;
    }

    @Nullable
    public static Drawable b(Context context, TypedArray typedArray, @StyleableRes int i) {
        int resourceId;
        Drawable b;
        return (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0 || (b = android.support.v7.a.a.a.b(context, resourceId)) == null) ? typedArray.getDrawable(i) : b;
    }
}
