package android.support.design.transformation;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.design.d.b;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import java.util.List;

public abstract class ExpandableBehavior extends CoordinatorLayout.b<View> {

    /* renamed from: a  reason: collision with root package name */
    private int f269a = 0;

    @Override // android.support.design.widget.CoordinatorLayout.b
    public abstract boolean a(CoordinatorLayout coordinatorLayout, View view, View view2);

    /* access modifiers changed from: protected */
    public abstract boolean a(View view, View view2, boolean z, boolean z2);

    public ExpandableBehavior() {
    }

    public ExpandableBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r1 = a(r5, r6);
     */
    @Override // android.support.design.widget.CoordinatorLayout.b
    @android.support.annotation.CallSuper
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.support.design.widget.CoordinatorLayout r5, final android.view.View r6, int r7) {
        /*
            r4 = this;
            boolean r0 = android.support.v4.view.ViewCompat.isLaidOut(r6)
            if (r0 != 0) goto L_0x002d
            android.support.design.d.b r1 = r4.a(r5, r6)
            if (r1 == 0) goto L_0x002d
            boolean r0 = r1.a()
            boolean r0 = r4.a(r0)
            if (r0 == 0) goto L_0x002d
            boolean r0 = r1.a()
            if (r0 == 0) goto L_0x002f
            r0 = 1
        L_0x001d:
            r4.f269a = r0
            int r0 = r4.f269a
            android.view.ViewTreeObserver r2 = r6.getViewTreeObserver()
            android.support.design.transformation.ExpandableBehavior$1 r3 = new android.support.design.transformation.ExpandableBehavior$1
            r3.<init>(r6, r0, r1)
            r2.addOnPreDrawListener(r3)
        L_0x002d:
            r0 = 0
            return r0
        L_0x002f:
            r0 = 2
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.transformation.ExpandableBehavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean");
    }

    @Override // android.support.design.widget.CoordinatorLayout.b
    @CallSuper
    public boolean b(CoordinatorLayout coordinatorLayout, View view, View view2) {
        b bVar = (b) view2;
        if (!a(bVar.a())) {
            return false;
        }
        this.f269a = bVar.a() ? 1 : 2;
        return a((View) bVar, view, bVar.a(), true);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public b a(CoordinatorLayout coordinatorLayout, View view) {
        List<View> c = coordinatorLayout.c(view);
        int size = c.size();
        for (int i = 0; i < size; i++) {
            View view2 = c.get(i);
            if (a(coordinatorLayout, view, view2)) {
                return (b) view2;
            }
        }
        return null;
    }

    private boolean a(boolean z) {
        boolean z2 = true;
        if (z) {
            return this.f269a == 0 || this.f269a == 2;
        }
        if (this.f269a != 1) {
            z2 = false;
        }
        return z2;
    }
}
