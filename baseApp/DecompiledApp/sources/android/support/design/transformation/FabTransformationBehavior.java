package android.support.design.transformation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.R;
import android.support.design.a.b;
import android.support.design.a.c;
import android.support.design.a.e;
import android.support.design.a.h;
import android.support.design.a.i;
import android.support.design.a.j;
import android.support.design.c.d;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.k;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import java.util.ArrayList;
import java.util.List;

public abstract class FabTransformationBehavior extends ExpandableTransformationBehavior {

    /* renamed from: a  reason: collision with root package name */
    private final Rect f273a;
    private final RectF b;
    private final RectF c;
    private final int[] d;

    /* access modifiers changed from: protected */
    public abstract a a(Context context, boolean z);

    public FabTransformationBehavior() {
        this.f273a = new Rect();
        this.b = new RectF();
        this.c = new RectF();
        this.d = new int[2];
    }

    public FabTransformationBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f273a = new Rect();
        this.b = new RectF();
        this.c = new RectF();
        this.d = new int[2];
    }

    @Override // android.support.design.widget.CoordinatorLayout.b, android.support.design.transformation.ExpandableBehavior
    @CallSuper
    public boolean a(CoordinatorLayout coordinatorLayout, View view, View view2) {
        if (view.getVisibility() == 8) {
            throw new IllegalStateException("This behavior cannot be attached to a GONE view. Set the view to INVISIBLE instead.");
        } else if (!(view2 instanceof FloatingActionButton)) {
            return false;
        } else {
            int expandedComponentIdHint = ((FloatingActionButton) view2).getExpandedComponentIdHint();
            if (expandedComponentIdHint == 0 || expandedComponentIdHint == view.getId()) {
                return true;
            }
            return false;
        }
    }

    @Override // android.support.design.widget.CoordinatorLayout.b
    @CallSuper
    public void a(@NonNull CoordinatorLayout.e eVar) {
        if (eVar.h == 0) {
            eVar.h = 80;
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.design.transformation.ExpandableTransformationBehavior
    @NonNull
    public AnimatorSet b(final View view, final View view2, final boolean z, boolean z2) {
        a a2 = a(view2.getContext(), z);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (Build.VERSION.SDK_INT >= 21) {
            a(view, view2, z, z2, a2, arrayList, arrayList2);
        }
        RectF rectF = this.b;
        a(view, view2, z, z2, a2, arrayList, arrayList2, rectF);
        float width = rectF.width();
        float height = rectF.height();
        b(view, view2, z, z2, a2, arrayList, arrayList2);
        a(view, view2, z, z2, a2, width, height, arrayList, arrayList2);
        c(view, view2, z, z2, a2, arrayList, arrayList2);
        d(view, view2, z, z2, a2, arrayList, arrayList2);
        AnimatorSet animatorSet = new AnimatorSet();
        b.a(animatorSet, arrayList);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            /* class android.support.design.transformation.FabTransformationBehavior.AnonymousClass1 */

            public void onAnimationStart(Animator animator) {
                if (z) {
                    view2.setVisibility(0);
                    view.setAlpha(0.0f);
                    view.setVisibility(4);
                }
            }

            public void onAnimationEnd(Animator animator) {
                if (!z) {
                    view2.setVisibility(4);
                    view.setAlpha(1.0f);
                    view.setVisibility(0);
                }
            }
        });
        int size = arrayList2.size();
        for (int i = 0; i < size; i++) {
            animatorSet.addListener(arrayList2.get(i));
        }
        return animatorSet;
    }

    @TargetApi(21)
    private void a(View view, View view2, boolean z, boolean z2, a aVar, List<Animator> list, List<Animator.AnimatorListener> list2) {
        ObjectAnimator ofFloat;
        float elevation = ViewCompat.getElevation(view2) - ViewCompat.getElevation(view);
        if (z) {
            if (!z2) {
                view2.setTranslationZ(-elevation);
            }
            ofFloat = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Z, 0.0f);
        } else {
            ofFloat = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Z, -elevation);
        }
        aVar.f278a.b("elevation").a((Animator) ofFloat);
        list.add(ofFloat);
    }

    private void a(View view, View view2, boolean z, boolean z2, a aVar, List<Animator> list, List<Animator.AnimatorListener> list2, RectF rectF) {
        i iVar;
        i iVar2;
        ObjectAnimator ofFloat;
        ObjectAnimator ofFloat2;
        float a2 = a(view, view2, aVar.b);
        float b2 = b(view, view2, aVar.b);
        if (a2 == 0.0f || b2 == 0.0f) {
            iVar2 = aVar.f278a.b("translationXLinear");
            iVar = aVar.f278a.b("translationYLinear");
        } else if ((!z || b2 >= 0.0f) && (z || b2 <= 0.0f)) {
            iVar2 = aVar.f278a.b("translationXCurveDownwards");
            iVar = aVar.f278a.b("translationYCurveDownwards");
        } else {
            iVar2 = aVar.f278a.b("translationXCurveUpwards");
            iVar = aVar.f278a.b("translationYCurveUpwards");
        }
        if (z) {
            if (!z2) {
                view2.setTranslationX(-a2);
                view2.setTranslationY(-b2);
            }
            ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(view2, View.TRANSLATION_X, 0.0f);
            ObjectAnimator ofFloat4 = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Y, 0.0f);
            a(view2, aVar, iVar2, iVar, -a2, -b2, 0.0f, 0.0f, rectF);
            ofFloat2 = ofFloat4;
            ofFloat = ofFloat3;
        } else {
            ofFloat = ObjectAnimator.ofFloat(view2, View.TRANSLATION_X, -a2);
            ofFloat2 = ObjectAnimator.ofFloat(view2, View.TRANSLATION_Y, -b2);
        }
        iVar2.a((Animator) ofFloat);
        iVar.a((Animator) ofFloat2);
        list.add(ofFloat);
        list.add(ofFloat2);
    }

    private void b(View view, final View view2, boolean z, boolean z2, a aVar, List<Animator> list, List<Animator.AnimatorListener> list2) {
        ObjectAnimator ofInt;
        if ((view2 instanceof d) && (view instanceof ImageView)) {
            final d dVar = (d) view2;
            final Drawable drawable = ((ImageView) view).getDrawable();
            if (drawable != null) {
                drawable.mutate();
                if (z) {
                    if (!z2) {
                        drawable.setAlpha(255);
                    }
                    ofInt = ObjectAnimator.ofInt(drawable, e.f223a, 0);
                } else {
                    ofInt = ObjectAnimator.ofInt(drawable, e.f223a, 255);
                }
                ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    /* class android.support.design.transformation.FabTransformationBehavior.AnonymousClass2 */

                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        view2.invalidate();
                    }
                });
                aVar.f278a.b("iconFade").a((Animator) ofInt);
                list.add(ofInt);
                list2.add(new AnimatorListenerAdapter() {
                    /* class android.support.design.transformation.FabTransformationBehavior.AnonymousClass3 */

                    public void onAnimationStart(Animator animator) {
                        dVar.setCircularRevealOverlayDrawable(drawable);
                    }

                    public void onAnimationEnd(Animator animator) {
                        dVar.setCircularRevealOverlayDrawable(null);
                    }
                });
            }
        }
    }

    private void a(View view, View view2, boolean z, boolean z2, a aVar, float f, float f2, List<Animator> list, List<Animator.AnimatorListener> list2) {
        Animator animator;
        if (view2 instanceof d) {
            final d dVar = (d) view2;
            float c2 = c(view, view2, aVar.b);
            float d2 = d(view, view2, aVar.b);
            ((FloatingActionButton) view).a(this.f273a);
            float width = ((float) this.f273a.width()) / 2.0f;
            i b2 = aVar.f278a.b("expansion");
            if (z) {
                if (!z2) {
                    dVar.setRevealInfo(new d.C0014d(c2, d2, width));
                }
                float f3 = z2 ? dVar.getRevealInfo().c : width;
                Animator a2 = android.support.design.c.a.a(dVar, c2, d2, k.a(c2, d2, 0.0f, 0.0f, f, f2));
                a2.addListener(new AnimatorListenerAdapter() {
                    /* class android.support.design.transformation.FabTransformationBehavior.AnonymousClass4 */

                    public void onAnimationEnd(Animator animator) {
                        d.C0014d revealInfo = dVar.getRevealInfo();
                        revealInfo.c = Float.MAX_VALUE;
                        dVar.setRevealInfo(revealInfo);
                    }
                });
                a(view2, b2.a(), (int) c2, (int) d2, f3, list);
                animator = a2;
            } else {
                float f4 = dVar.getRevealInfo().c;
                Animator a3 = android.support.design.c.a.a(dVar, c2, d2, width);
                a(view2, b2.a(), (int) c2, (int) d2, f4, list);
                a(view2, b2.a(), b2.b(), aVar.f278a.a(), (int) c2, (int) d2, width, list);
                animator = a3;
            }
            b2.a(animator);
            list.add(animator);
            list2.add(android.support.design.c.a.a(dVar));
        }
    }

    private void c(View view, View view2, boolean z, boolean z2, a aVar, List<Animator> list, List<Animator.AnimatorListener> list2) {
        ObjectAnimator ofInt;
        if (view2 instanceof d) {
            d dVar = (d) view2;
            int c2 = c(view);
            int i = 16777215 & c2;
            if (z) {
                if (!z2) {
                    dVar.setCircularRevealScrimColor(c2);
                }
                ofInt = ObjectAnimator.ofInt(dVar, d.c.f246a, i);
            } else {
                ofInt = ObjectAnimator.ofInt(dVar, d.c.f246a, c2);
            }
            ofInt.setEvaluator(c.a());
            aVar.f278a.b(TtmlNode.ATTR_TTS_COLOR).a((Animator) ofInt);
            list.add(ofInt);
        }
    }

    private void d(View view, View view2, boolean z, boolean z2, a aVar, List<Animator> list, List<Animator.AnimatorListener> list2) {
        ViewGroup a2;
        ObjectAnimator ofFloat;
        if (view2 instanceof ViewGroup) {
            if ((!(view2 instanceof d) || android.support.design.c.c.f243a != 0) && (a2 = a(view2)) != null) {
                if (z) {
                    if (!z2) {
                        android.support.design.a.d.f222a.set(a2, Float.valueOf(0.0f));
                    }
                    ofFloat = ObjectAnimator.ofFloat(a2, android.support.design.a.d.f222a, 1.0f);
                } else {
                    ofFloat = ObjectAnimator.ofFloat(a2, android.support.design.a.d.f222a, 0.0f);
                }
                aVar.f278a.b("contentFade").a((Animator) ofFloat);
                list.add(ofFloat);
            }
        }
    }

    private float a(View view, View view2, j jVar) {
        RectF rectF = this.b;
        RectF rectF2 = this.c;
        a(view, rectF);
        a(view2, rectF2);
        float f = 0.0f;
        switch (jVar.f228a & 7) {
            case 1:
                f = rectF2.centerX() - rectF.centerX();
                break;
            case 3:
                f = rectF2.left - rectF.left;
                break;
            case 5:
                f = rectF2.right - rectF.right;
                break;
        }
        return f + jVar.b;
    }

    private float b(View view, View view2, j jVar) {
        RectF rectF = this.b;
        RectF rectF2 = this.c;
        a(view, rectF);
        a(view2, rectF2);
        float f = 0.0f;
        switch (jVar.f228a & 112) {
            case 16:
                f = rectF2.centerY() - rectF.centerY();
                break;
            case 48:
                f = rectF2.top - rectF.top;
                break;
            case 80:
                f = rectF2.bottom - rectF.bottom;
                break;
        }
        return f + jVar.c;
    }

    private void a(View view, RectF rectF) {
        rectF.set(0.0f, 0.0f, (float) view.getWidth(), (float) view.getHeight());
        int[] iArr = this.d;
        view.getLocationInWindow(iArr);
        rectF.offsetTo((float) iArr[0], (float) iArr[1]);
        rectF.offset((float) ((int) (-view.getTranslationX())), (float) ((int) (-view.getTranslationY())));
    }

    private float c(View view, View view2, j jVar) {
        RectF rectF = this.b;
        RectF rectF2 = this.c;
        a(view, rectF);
        a(view2, rectF2);
        rectF2.offset(-a(view, view2, jVar), 0.0f);
        return rectF.centerX() - rectF2.left;
    }

    private float d(View view, View view2, j jVar) {
        RectF rectF = this.b;
        RectF rectF2 = this.c;
        a(view, rectF);
        a(view2, rectF2);
        rectF2.offset(0.0f, -b(view, view2, jVar));
        return rectF.centerY() - rectF2.top;
    }

    private void a(View view, a aVar, i iVar, i iVar2, float f, float f2, float f3, float f4, RectF rectF) {
        float a2 = a(aVar, iVar, f, f3);
        float a3 = a(aVar, iVar2, f2, f4);
        Rect rect = this.f273a;
        view.getWindowVisibleDisplayFrame(rect);
        RectF rectF2 = this.b;
        rectF2.set(rect);
        RectF rectF3 = this.c;
        a(view, rectF3);
        rectF3.offset(a2, a3);
        rectF3.intersect(rectF2);
        rectF.set(rectF3);
    }

    private float a(a aVar, i iVar, float f, float f2) {
        long a2 = iVar.a();
        long b2 = iVar.b();
        i b3 = aVar.f278a.b("expansion");
        return android.support.design.a.a.a(f, f2, iVar.c().getInterpolation(((float) (((b3.b() + b3.a()) + 17) - a2)) / ((float) b2)));
    }

    @Nullable
    private ViewGroup a(View view) {
        View findViewById = view.findViewById(R.id.mtrl_child_content_container);
        if (findViewById != null) {
            return b(findViewById);
        }
        if ((view instanceof b) || (view instanceof a)) {
            return b(((ViewGroup) view).getChildAt(0));
        }
        return b(view);
    }

    @Nullable
    private ViewGroup b(View view) {
        if (view instanceof ViewGroup) {
            return (ViewGroup) view;
        }
        return null;
    }

    private int c(View view) {
        ColorStateList backgroundTintList = ViewCompat.getBackgroundTintList(view);
        if (backgroundTintList != null) {
            return backgroundTintList.getColorForState(view.getDrawableState(), backgroundTintList.getDefaultColor());
        }
        return 0;
    }

    private void a(View view, long j, int i, int i2, float f, List<Animator> list) {
        if (Build.VERSION.SDK_INT >= 21 && j > 0) {
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, i, i2, f, f);
            createCircularReveal.setStartDelay(0);
            createCircularReveal.setDuration(j);
            list.add(createCircularReveal);
        }
    }

    private void a(View view, long j, long j2, long j3, int i, int i2, float f, List<Animator> list) {
        if (Build.VERSION.SDK_INT >= 21 && j + j2 < j3) {
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, i, i2, f, f);
            createCircularReveal.setStartDelay(j + j2);
            createCircularReveal.setDuration(j3 - (j + j2));
            list.add(createCircularReveal);
        }
    }

    /* access modifiers changed from: protected */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public h f278a;
        public j b;

        protected a() {
        }
    }
}
