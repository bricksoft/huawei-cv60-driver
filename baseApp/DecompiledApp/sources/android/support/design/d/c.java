package android.support.design.d;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.view.ViewParent;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final View f248a;
    private boolean b;
    @IdRes
    private int c;

    public boolean a() {
        return this.b;
    }

    public Bundle b() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("expanded", this.b);
        bundle.putInt("expandedComponentIdHint", this.c);
        return bundle;
    }

    public void a(Bundle bundle) {
        this.b = bundle.getBoolean("expanded", false);
        this.c = bundle.getInt("expandedComponentIdHint", 0);
        if (this.b) {
            d();
        }
    }

    public void a(@IdRes int i) {
        this.c = i;
    }

    @IdRes
    public int c() {
        return this.c;
    }

    private void d() {
        ViewParent parent = this.f248a.getParent();
        if (parent instanceof CoordinatorLayout) {
            ((CoordinatorLayout) parent).b(this.f248a);
        }
    }
}
