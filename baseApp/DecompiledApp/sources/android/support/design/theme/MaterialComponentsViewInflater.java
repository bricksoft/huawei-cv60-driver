package android.support.design.theme;

import android.content.Context;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.design.b.a;
import android.support.v7.app.AppCompatViewInflater;
import android.support.v7.widget.g;
import android.util.AttributeSet;

@Keep
public class MaterialComponentsViewInflater extends AppCompatViewInflater {
    /* access modifiers changed from: protected */
    @Override // android.support.v7.app.AppCompatViewInflater
    @NonNull
    public g createButton(Context context, AttributeSet attributeSet) {
        return new a(context, attributeSet);
    }
}
