package android.support.design.g;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.List;

public class d {

    /* renamed from: a  reason: collision with root package name */
    public float f251a;
    public float b;
    public float c;
    public float d;
    private final List<c> e;

    public static abstract class c {
        protected final Matrix g = new Matrix();

        public abstract void a(Matrix matrix, Path path);
    }

    public void a(float f, float f2) {
        this.f251a = f;
        this.b = f2;
        this.c = f;
        this.d = f2;
        this.e.clear();
    }

    public void b(float f, float f2) {
        b bVar = new b();
        bVar.f253a = f;
        bVar.b = f2;
        this.e.add(bVar);
        this.c = f;
        this.d = f2;
    }

    public void a(float f, float f2, float f3, float f4, float f5, float f6) {
        a aVar = new a(f, f2, f3, f4);
        aVar.e = f5;
        aVar.f = f6;
        this.e.add(aVar);
        this.c = ((f + f3) * 0.5f) + (((f3 - f) / 2.0f) * ((float) Math.cos(Math.toRadians((double) (f5 + f6)))));
        this.d = ((f2 + f4) * 0.5f) + (((f4 - f2) / 2.0f) * ((float) Math.sin(Math.toRadians((double) (f5 + f6)))));
    }

    public void a(Matrix matrix, Path path) {
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).a(matrix, path);
        }
    }

    public static class b extends c {

        /* renamed from: a  reason: collision with root package name */
        private float f253a;
        private float b;

        @Override // android.support.design.g.d.c
        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.g;
            matrix.invert(matrix2);
            path.transform(matrix2);
            path.lineTo(this.f253a, this.b);
            path.transform(matrix);
        }
    }

    public static class a extends c {
        private static final RectF h = new RectF();

        /* renamed from: a  reason: collision with root package name */
        public float f252a;
        public float b;
        public float c;
        public float d;
        public float e;
        public float f;

        public a(float f2, float f3, float f4, float f5) {
            this.f252a = f2;
            this.b = f3;
            this.c = f4;
            this.d = f5;
        }

        @Override // android.support.design.g.d.c
        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.g;
            matrix.invert(matrix2);
            path.transform(matrix2);
            h.set(this.f252a, this.b, this.c, this.d);
            path.arcTo(h, this.e, this.f, false);
            path.transform(matrix);
        }
    }
}
