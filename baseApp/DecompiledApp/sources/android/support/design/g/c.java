package android.support.design.g;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.IntRange;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.TintAwareDrawable;

public class c extends Drawable implements TintAwareDrawable {

    /* renamed from: a  reason: collision with root package name */
    private final Paint f250a;
    private final Matrix[] b;
    private final Matrix[] c;
    private final d[] d;
    private final Matrix e;
    private final Path f;
    private final PointF g;
    private final d h;
    private final Region i;
    private final Region j;
    private final float[] k;
    private final float[] l;
    @Nullable
    private e m;
    private boolean n;
    private boolean o;
    private float p;
    private int q;
    private int r;
    private int s;
    private int t;
    private float u;
    private float v;
    private Paint.Style w;
    @Nullable
    private PorterDuffColorFilter x;
    private PorterDuff.Mode y;
    private ColorStateList z;

    private static int a(int i2, int i3) {
        return (((i3 >>> 7) + i3) * i2) >>> 8;
    }

    public ColorStateList a() {
        return this.z;
    }

    @Override // android.support.v4.graphics.drawable.TintAwareDrawable
    public void setTintList(ColorStateList colorStateList) {
        this.z = colorStateList;
        b();
        invalidateSelf();
    }

    @Override // android.support.v4.graphics.drawable.TintAwareDrawable
    public void setTintMode(PorterDuff.Mode mode) {
        this.y = mode;
        b();
        invalidateSelf();
    }

    @Override // android.support.v4.graphics.drawable.TintAwareDrawable
    public void setTint(@ColorInt int i2) {
        setTintList(ColorStateList.valueOf(i2));
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(@IntRange(from = 0, to = 255) int i2) {
        this.t = i2;
        invalidateSelf();
    }

    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        this.f250a.setColorFilter(colorFilter);
        invalidateSelf();
    }

    public Region getTransparentRegion() {
        Rect bounds = getBounds();
        this.i.set(bounds);
        b(bounds.width(), bounds.height(), this.f);
        this.j.setPath(this.f, this.i);
        this.i.op(this.j, Region.Op.DIFFERENCE);
        return this.i;
    }

    public void a(float f2) {
        this.p = f2;
        invalidateSelf();
    }

    public void draw(Canvas canvas) {
        this.f250a.setColorFilter(this.x);
        int alpha = this.f250a.getAlpha();
        this.f250a.setAlpha(a(alpha, this.t));
        this.f250a.setStrokeWidth(this.v);
        this.f250a.setStyle(this.w);
        if (this.r > 0 && this.n) {
            this.f250a.setShadowLayer((float) this.s, 0.0f, (float) this.r, this.q);
        }
        if (this.m != null) {
            b(canvas.getWidth(), canvas.getHeight(), this.f);
            canvas.drawPath(this.f, this.f250a);
        } else {
            canvas.drawRect(0.0f, 0.0f, (float) canvas.getWidth(), (float) canvas.getHeight(), this.f250a);
        }
        this.f250a.setAlpha(alpha);
    }

    public void a(int i2, int i3, Path path) {
        path.rewind();
        if (this.m != null) {
            for (int i4 = 0; i4 < 4; i4++) {
                a(i4, i2, i3);
                b(i4, i2, i3);
            }
            for (int i5 = 0; i5 < 4; i5++) {
                a(i5, path);
                b(i5, path);
            }
            path.close();
        }
    }

    private void a(int i2, int i3, int i4) {
        a(i2, i3, i4, this.g);
        a(i2).a(c(i2, i3, i4), this.p, this.d[i2]);
        this.b[i2].reset();
        this.b[i2].setTranslate(this.g.x, this.g.y);
        this.b[i2].preRotate((float) Math.toDegrees((double) (d(((i2 - 1) + 4) % 4, i3, i4) + 1.5707964f)));
    }

    private void b(int i2, int i3, int i4) {
        this.k[0] = this.d[i2].c;
        this.k[1] = this.d[i2].d;
        this.b[i2].mapPoints(this.k);
        float d2 = d(i2, i3, i4);
        this.c[i2].reset();
        this.c[i2].setTranslate(this.k[0], this.k[1]);
        this.c[i2].preRotate((float) Math.toDegrees((double) d2));
    }

    private void a(int i2, Path path) {
        this.k[0] = this.d[i2].f251a;
        this.k[1] = this.d[i2].b;
        this.b[i2].mapPoints(this.k);
        if (i2 == 0) {
            path.moveTo(this.k[0], this.k[1]);
        } else {
            path.lineTo(this.k[0], this.k[1]);
        }
        this.d[i2].a(this.b[i2], path);
    }

    private void b(int i2, Path path) {
        int i3 = (i2 + 1) % 4;
        this.k[0] = this.d[i2].c;
        this.k[1] = this.d[i2].d;
        this.b[i2].mapPoints(this.k);
        this.l[0] = this.d[i3].f251a;
        this.l[1] = this.d[i3].b;
        this.b[i3].mapPoints(this.l);
        this.h.a(0.0f, 0.0f);
        b(i2).a((float) Math.hypot((double) (this.k[0] - this.l[0]), (double) (this.k[1] - this.l[1])), this.p, this.h);
        this.h.a(this.c[i2], path);
    }

    private a a(int i2) {
        switch (i2) {
            case 1:
                return this.m.b();
            case 2:
                return this.m.c();
            case 3:
                return this.m.d();
            default:
                return this.m.a();
        }
    }

    private b b(int i2) {
        switch (i2) {
            case 1:
                return this.m.f();
            case 2:
                return this.m.g();
            case 3:
                return this.m.h();
            default:
                return this.m.e();
        }
    }

    private void a(int i2, int i3, int i4, PointF pointF) {
        switch (i2) {
            case 1:
                pointF.set((float) i3, 0.0f);
                return;
            case 2:
                pointF.set((float) i3, (float) i4);
                return;
            case 3:
                pointF.set(0.0f, (float) i4);
                return;
            default:
                pointF.set(0.0f, 0.0f);
                return;
        }
    }

    private float c(int i2, int i3, int i4) {
        a(((i2 - 1) + 4) % 4, i3, i4, this.g);
        float f2 = this.g.x;
        float f3 = this.g.y;
        a((i2 + 1) % 4, i3, i4, this.g);
        float f4 = this.g.x;
        float f5 = this.g.y;
        a(i2, i3, i4, this.g);
        float f6 = this.g.x;
        float f7 = this.g.y;
        float atan2 = ((float) Math.atan2((double) (f3 - f7), (double) (f2 - f6))) - ((float) Math.atan2((double) (f5 - f7), (double) (f4 - f6)));
        if (atan2 < 0.0f) {
            return (float) (((double) atan2) + 6.283185307179586d);
        }
        return atan2;
    }

    private float d(int i2, int i3, int i4) {
        a(i2, i3, i4, this.g);
        float f2 = this.g.x;
        float f3 = this.g.y;
        a((i2 + 1) % 4, i3, i4, this.g);
        return (float) Math.atan2((double) (this.g.y - f3), (double) (this.g.x - f2));
    }

    private void b(int i2, int i3, Path path) {
        a(i2, i3, path);
        if (this.u != 1.0f) {
            this.e.reset();
            this.e.setScale(this.u, this.u, (float) (i2 / 2), (float) (i3 / 2));
            path.transform(this.e);
        }
    }

    private void b() {
        if (this.z == null || this.y == null) {
            this.x = null;
            return;
        }
        int colorForState = this.z.getColorForState(getState(), 0);
        this.x = new PorterDuffColorFilter(colorForState, this.y);
        if (this.o) {
            this.q = colorForState;
        }
    }
}
