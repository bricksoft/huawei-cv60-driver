package android.support.design.b;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.Px;
import android.support.annotation.RestrictTo;
import android.support.design.R;
import android.support.design.internal.h;
import android.support.design.internal.i;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.g;
import android.util.AttributeSet;
import android.util.Log;

public class a extends g {
    @Nullable

    /* renamed from: a  reason: collision with root package name */
    private final c f229a;
    @Px
    private int b;
    private PorterDuff.Mode c;
    private ColorStateList d;
    private Drawable e;
    @Px
    private int f;
    @Px
    private int g;
    private int h;

    public a(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.materialButtonStyle);
    }

    public a(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray a2 = h.a(context, attributeSet, R.styleable.MaterialButton, i, R.style.Widget_MaterialComponents_Button, new int[0]);
        this.b = a2.getDimensionPixelSize(R.styleable.MaterialButton_iconPadding, 0);
        this.c = i.a(a2.getInt(R.styleable.MaterialButton_iconTintMode, -1), PorterDuff.Mode.SRC_IN);
        this.d = android.support.design.e.a.a(getContext(), a2, R.styleable.MaterialButton_iconTint);
        this.e = android.support.design.e.a.b(getContext(), a2, R.styleable.MaterialButton_icon);
        this.h = a2.getInteger(R.styleable.MaterialButton_iconGravity, 1);
        this.f = a2.getDimensionPixelSize(R.styleable.MaterialButton_iconSize, 0);
        this.f229a = new c(this);
        this.f229a.a(a2);
        a2.recycle();
        setCompoundDrawablePadding(this.b);
        b();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (Build.VERSION.SDK_INT < 21 && c()) {
            this.f229a.a(canvas);
        }
    }

    @Override // android.support.v7.widget.g, android.support.v4.view.TintableBackgroundView
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void setSupportBackgroundTintList(@Nullable ColorStateList colorStateList) {
        if (c()) {
            this.f229a.a(colorStateList);
        } else if (this.f229a != null) {
            super.setSupportBackgroundTintList(colorStateList);
        }
    }

    @Override // android.support.v7.widget.g, android.support.v4.view.TintableBackgroundView
    @Nullable
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public ColorStateList getSupportBackgroundTintList() {
        if (c()) {
            return this.f229a.c();
        }
        return super.getSupportBackgroundTintList();
    }

    @Override // android.support.v7.widget.g, android.support.v4.view.TintableBackgroundView
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void setSupportBackgroundTintMode(@Nullable PorterDuff.Mode mode) {
        if (c()) {
            this.f229a.a(mode);
        } else if (this.f229a != null) {
            super.setSupportBackgroundTintMode(mode);
        }
    }

    @Override // android.support.v7.widget.g, android.support.v4.view.TintableBackgroundView
    @Nullable
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (c()) {
            return this.f229a.d();
        }
        return super.getSupportBackgroundTintMode();
    }

    public void setBackgroundTintList(@Nullable ColorStateList colorStateList) {
        setSupportBackgroundTintList(colorStateList);
    }

    @Nullable
    public ColorStateList getBackgroundTintList() {
        return getSupportBackgroundTintList();
    }

    public void setBackgroundTintMode(@Nullable PorterDuff.Mode mode) {
        setSupportBackgroundTintMode(mode);
    }

    @Nullable
    public PorterDuff.Mode getBackgroundTintMode() {
        return getSupportBackgroundTintMode();
    }

    public void setBackgroundColor(@ColorInt int i) {
        if (c()) {
            this.f229a.a(i);
        } else {
            super.setBackgroundColor(i);
        }
    }

    public void setBackground(Drawable drawable) {
        setBackgroundDrawable(drawable);
    }

    @Override // android.support.v7.widget.g
    public void setBackgroundResource(@DrawableRes int i) {
        Drawable drawable = null;
        if (i != 0) {
            drawable = android.support.v7.a.a.a.b(getContext(), i);
        }
        setBackgroundDrawable(drawable);
    }

    @Override // android.support.v7.widget.g
    public void setBackgroundDrawable(Drawable drawable) {
        if (!c()) {
            super.setBackgroundDrawable(drawable);
        } else if (drawable != getBackground()) {
            Log.i("MaterialButton", "Setting a custom background is not supported.");
            this.f229a.a();
            super.setBackgroundDrawable(drawable);
        } else {
            getBackground().setState(drawable.getState());
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.widget.g
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (Build.VERSION.SDK_INT == 21 && this.f229a != null) {
            this.f229a.a(i4 - i2, i3 - i);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.e != null && this.h == 2) {
            int measuredWidth = (((((getMeasuredWidth() - ((int) getPaint().measureText(getText().toString()))) - ViewCompat.getPaddingEnd(this)) - (this.f == 0 ? this.e.getIntrinsicWidth() : this.f)) - this.b) - ViewCompat.getPaddingStart(this)) / 2;
            if (a()) {
                measuredWidth = -measuredWidth;
            }
            if (this.g != measuredWidth) {
                this.g = measuredWidth;
                b();
            }
        }
    }

    private boolean a() {
        return ViewCompat.getLayoutDirection(this) == 1;
    }

    /* access modifiers changed from: package-private */
    public void setInternalBackground(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
    }

    public void setIconPadding(@Px int i) {
        if (this.b != i) {
            this.b = i;
            setCompoundDrawablePadding(i);
        }
    }

    @Px
    public int getIconPadding() {
        return this.b;
    }

    public void setIconSize(@Px int i) {
        if (i < 0) {
            throw new IllegalArgumentException("iconSize cannot be less than 0");
        } else if (this.f != i) {
            this.f = i;
            b();
        }
    }

    @Px
    public int getIconSize() {
        return this.f;
    }

    public void setIcon(Drawable drawable) {
        if (this.e != drawable) {
            this.e = drawable;
            b();
        }
    }

    public void setIconResource(@DrawableRes int i) {
        Drawable drawable = null;
        if (i != 0) {
            drawable = android.support.v7.a.a.a.b(getContext(), i);
        }
        setIcon(drawable);
    }

    public Drawable getIcon() {
        return this.e;
    }

    public void setIconTint(@Nullable ColorStateList colorStateList) {
        if (this.d != colorStateList) {
            this.d = colorStateList;
            b();
        }
    }

    public void setIconTintResource(@ColorRes int i) {
        setIconTint(android.support.v7.a.a.a.a(getContext(), i));
    }

    public ColorStateList getIconTint() {
        return this.d;
    }

    public void setIconTintMode(PorterDuff.Mode mode) {
        if (this.c != mode) {
            this.c = mode;
            b();
        }
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.c;
    }

    private void b() {
        if (this.e != null) {
            this.e = this.e.mutate();
            DrawableCompat.setTintList(this.e, this.d);
            if (this.c != null) {
                DrawableCompat.setTintMode(this.e, this.c);
            }
            this.e.setBounds(this.g, 0, (this.f != 0 ? this.f : this.e.getIntrinsicWidth()) + this.g, this.f != 0 ? this.f : this.e.getIntrinsicHeight());
        }
        TextViewCompat.setCompoundDrawablesRelative(this, this.e, null, null, null);
    }

    public void setRippleColor(@Nullable ColorStateList colorStateList) {
        if (c()) {
            this.f229a.b(colorStateList);
        }
    }

    public void setRippleColorResource(@ColorRes int i) {
        if (c()) {
            setRippleColor(android.support.v7.a.a.a.a(getContext(), i));
        }
    }

    public ColorStateList getRippleColor() {
        if (c()) {
            return this.f229a.e();
        }
        return null;
    }

    public void setStrokeColor(@Nullable ColorStateList colorStateList) {
        if (c()) {
            this.f229a.c(colorStateList);
        }
    }

    public void setStrokeColorResource(@ColorRes int i) {
        if (c()) {
            setStrokeColor(android.support.v7.a.a.a.a(getContext(), i));
        }
    }

    public ColorStateList getStrokeColor() {
        if (c()) {
            return this.f229a.f();
        }
        return null;
    }

    public void setStrokeWidth(@Px int i) {
        if (c()) {
            this.f229a.b(i);
        }
    }

    public void setStrokeWidthResource(@DimenRes int i) {
        if (c()) {
            setStrokeWidth(getResources().getDimensionPixelSize(i));
        }
    }

    @Px
    public int getStrokeWidth() {
        if (c()) {
            return this.f229a.g();
        }
        return 0;
    }

    public void setCornerRadius(@Px int i) {
        if (c()) {
            this.f229a.c(i);
        }
    }

    public void setCornerRadiusResource(@DimenRes int i) {
        if (c()) {
            setCornerRadius(getResources().getDimensionPixelSize(i));
        }
    }

    @Px
    public int getCornerRadius() {
        if (c()) {
            return this.f229a.h();
        }
        return 0;
    }

    public int getIconGravity() {
        return this.h;
    }

    public void setIconGravity(int i) {
        this.h = i;
    }

    private boolean c() {
        return this.f229a != null && !this.f229a.b();
    }
}
