package android.support.design.b;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.design.R;
import android.support.design.e.a;
import android.support.design.internal.i;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;

/* access modifiers changed from: package-private */
@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final boolean f230a = (Build.VERSION.SDK_INT >= 21);
    private final a b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    @Nullable
    private PorterDuff.Mode i;
    @Nullable
    private ColorStateList j;
    @Nullable
    private ColorStateList k;
    @Nullable
    private ColorStateList l;
    private final Paint m = new Paint(1);
    private final Rect n = new Rect();
    private final RectF o = new RectF();
    @Nullable
    private GradientDrawable p;
    @Nullable
    private Drawable q;
    @Nullable
    private GradientDrawable r;
    @Nullable
    private Drawable s;
    @Nullable
    private GradientDrawable t;
    @Nullable
    private GradientDrawable u;
    @Nullable
    private GradientDrawable v;
    private boolean w = false;

    public c(a aVar) {
        this.b = aVar;
    }

    public void a(TypedArray typedArray) {
        int i2 = 0;
        this.c = typedArray.getDimensionPixelOffset(R.styleable.MaterialButton_android_insetLeft, 0);
        this.d = typedArray.getDimensionPixelOffset(R.styleable.MaterialButton_android_insetRight, 0);
        this.e = typedArray.getDimensionPixelOffset(R.styleable.MaterialButton_android_insetTop, 0);
        this.f = typedArray.getDimensionPixelOffset(R.styleable.MaterialButton_android_insetBottom, 0);
        this.g = typedArray.getDimensionPixelSize(R.styleable.MaterialButton_cornerRadius, 0);
        this.h = typedArray.getDimensionPixelSize(R.styleable.MaterialButton_strokeWidth, 0);
        this.i = i.a(typedArray.getInt(R.styleable.MaterialButton_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN);
        this.j = a.a(this.b.getContext(), typedArray, R.styleable.MaterialButton_backgroundTint);
        this.k = a.a(this.b.getContext(), typedArray, R.styleable.MaterialButton_strokeColor);
        this.l = a.a(this.b.getContext(), typedArray, R.styleable.MaterialButton_rippleColor);
        this.m.setStyle(Paint.Style.STROKE);
        this.m.setStrokeWidth((float) this.h);
        Paint paint = this.m;
        if (this.k != null) {
            i2 = this.k.getColorForState(this.b.getDrawableState(), 0);
        }
        paint.setColor(i2);
        int paddingStart = ViewCompat.getPaddingStart(this.b);
        int paddingTop = this.b.getPaddingTop();
        int paddingEnd = ViewCompat.getPaddingEnd(this.b);
        int paddingBottom = this.b.getPaddingBottom();
        this.b.setInternalBackground(f230a ? k() : i());
        ViewCompat.setPaddingRelative(this.b, paddingStart + this.c, paddingTop + this.e, paddingEnd + this.d, paddingBottom + this.f);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.w = true;
        this.b.setSupportBackgroundTintList(this.j);
        this.b.setSupportBackgroundTintMode(this.i);
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable Canvas canvas) {
        if (canvas != null && this.k != null && this.h > 0) {
            this.n.set(this.b.getBackground().getBounds());
            this.o.set(((float) this.n.left) + (((float) this.h) / 2.0f) + ((float) this.c), ((float) this.n.top) + (((float) this.h) / 2.0f) + ((float) this.e), (((float) this.n.right) - (((float) this.h) / 2.0f)) - ((float) this.d), (((float) this.n.bottom) - (((float) this.h) / 2.0f)) - ((float) this.f));
            float f2 = ((float) this.g) - (((float) this.h) / 2.0f);
            canvas.drawRoundRect(this.o, f2, f2, this.m);
        }
    }

    private Drawable i() {
        this.p = new GradientDrawable();
        this.p.setCornerRadius(((float) this.g) + 1.0E-5f);
        this.p.setColor(-1);
        this.q = DrawableCompat.wrap(this.p);
        DrawableCompat.setTintList(this.q, this.j);
        if (this.i != null) {
            DrawableCompat.setTintMode(this.q, this.i);
        }
        this.r = new GradientDrawable();
        this.r.setCornerRadius(((float) this.g) + 1.0E-5f);
        this.r.setColor(-1);
        this.s = DrawableCompat.wrap(this.r);
        DrawableCompat.setTintList(this.s, this.l);
        return a(new LayerDrawable(new Drawable[]{this.q, this.s}));
    }

    private InsetDrawable a(Drawable drawable) {
        return new InsetDrawable(drawable, this.c, this.e, this.d, this.f);
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            if (f230a) {
                j();
            } else if (this.q != null) {
                DrawableCompat.setTintList(this.q, this.j);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ColorStateList c() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable PorterDuff.Mode mode) {
        if (this.i != mode) {
            this.i = mode;
            if (f230a) {
                j();
            } else if (this.q != null && this.i != null) {
                DrawableCompat.setTintMode(this.q, this.i);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode d() {
        return this.i;
    }

    private void j() {
        if (this.t != null) {
            DrawableCompat.setTintList(this.t, this.j);
            if (this.i != null) {
                DrawableCompat.setTintMode(this.t, this.i);
            }
        }
    }

    @TargetApi(21)
    private Drawable k() {
        this.t = new GradientDrawable();
        this.t.setCornerRadius(((float) this.g) + 1.0E-5f);
        this.t.setColor(-1);
        j();
        this.u = new GradientDrawable();
        this.u.setCornerRadius(((float) this.g) + 1.0E-5f);
        this.u.setColor(0);
        this.u.setStroke(this.h, this.k);
        InsetDrawable a2 = a(new LayerDrawable(new Drawable[]{this.t, this.u}));
        this.v = new GradientDrawable();
        this.v.setCornerRadius(((float) this.g) + 1.0E-5f);
        this.v.setColor(-1);
        return new b(android.support.design.f.a.a(this.l), a2, this.v);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        if (this.v != null) {
            this.v.setBounds(this.c, this.e, i3 - this.d, i2 - this.f);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (f230a && this.t != null) {
            this.t.setColor(i2);
        } else if (!f230a && this.p != null) {
            this.p.setColor(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(@Nullable ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            if (f230a && (this.b.getBackground() instanceof RippleDrawable)) {
                ((RippleDrawable) this.b.getBackground()).setColor(colorStateList);
            } else if (!f230a && this.s != null) {
                DrawableCompat.setTintList(this.s, colorStateList);
            }
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public ColorStateList e() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public void c(@Nullable ColorStateList colorStateList) {
        int i2 = 0;
        if (this.k != colorStateList) {
            this.k = colorStateList;
            Paint paint = this.m;
            if (colorStateList != null) {
                i2 = colorStateList.getColorForState(this.b.getDrawableState(), 0);
            }
            paint.setColor(i2);
            l();
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public ColorStateList f() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        if (this.h != i2) {
            this.h = i2;
            this.m.setStrokeWidth((float) i2);
            l();
        }
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.h;
    }

    private void l() {
        if (f230a && this.u != null) {
            this.b.setInternalBackground(k());
        } else if (!f230a) {
            this.b.invalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        if (this.g != i2) {
            this.g = i2;
            if (f230a && this.t != null && this.u != null && this.v != null) {
                if (Build.VERSION.SDK_INT == 21) {
                    n().setCornerRadius(((float) i2) + 1.0E-5f);
                    m().setCornerRadius(((float) i2) + 1.0E-5f);
                }
                this.t.setCornerRadius(((float) i2) + 1.0E-5f);
                this.u.setCornerRadius(((float) i2) + 1.0E-5f);
                this.v.setCornerRadius(((float) i2) + 1.0E-5f);
            } else if (!f230a && this.p != null && this.r != null) {
                this.p.setCornerRadius(((float) i2) + 1.0E-5f);
                this.r.setCornerRadius(((float) i2) + 1.0E-5f);
                this.b.invalidate();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int h() {
        return this.g;
    }

    @Nullable
    private GradientDrawable m() {
        if (!f230a || this.b.getBackground() == null) {
            return null;
        }
        return (GradientDrawable) ((LayerDrawable) ((InsetDrawable) ((RippleDrawable) this.b.getBackground()).getDrawable(0)).getDrawable()).getDrawable(1);
    }

    @Nullable
    private GradientDrawable n() {
        if (!f230a || this.b.getBackground() == null) {
            return null;
        }
        return (GradientDrawable) ((LayerDrawable) ((InsetDrawable) ((RippleDrawable) this.b.getBackground()).getDrawable(0)).getDrawable()).getDrawable(0);
    }
}
