package android.support.design.bottomappbar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Dimension;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Px;
import android.support.design.a.a;
import android.support.design.behavior.HideBottomViewOnScrollBehavior;
import android.support.design.g.c;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public class BottomAppBar extends Toolbar implements CoordinatorLayout.a {

    /* renamed from: a  reason: collision with root package name */
    AnimatorListenerAdapter f233a;
    private final int f;
    private final c g;
    private final a h;
    @Nullable
    private Animator i;
    @Nullable
    private Animator j;
    @Nullable
    private Animator k;
    private int l;
    private boolean m;
    private boolean n;

    public int getFabAlignmentMode() {
        return this.l;
    }

    public void setFabAlignmentMode(int i2) {
        a(i2);
        a(i2, this.n);
        this.l = i2;
    }

    public void setBackgroundTint(@Nullable ColorStateList colorStateList) {
        DrawableCompat.setTintList(this.g, colorStateList);
    }

    @Nullable
    public ColorStateList getBackgroundTint() {
        return this.g.a();
    }

    public float getFabCradleMargin() {
        return this.h.d();
    }

    public void setFabCradleMargin(@Dimension float f2) {
        if (f2 != getFabCradleMargin()) {
            this.h.d(f2);
            this.g.invalidateSelf();
        }
    }

    @Dimension
    public float getFabCradleRoundedCornerRadius() {
        return this.h.e();
    }

    public void setFabCradleRoundedCornerRadius(@Dimension float f2) {
        if (f2 != getFabCradleRoundedCornerRadius()) {
            this.h.e(f2);
            this.g.invalidateSelf();
        }
    }

    @Dimension
    public float getCradleVerticalOffset() {
        return this.h.b();
    }

    public void setCradleVerticalOffset(@Dimension float f2) {
        if (f2 != getCradleVerticalOffset()) {
            this.h.b(f2);
            this.g.invalidateSelf();
        }
    }

    public boolean getHideOnScroll() {
        return this.m;
    }

    public void setHideOnScroll(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: package-private */
    public void setFabDiameter(@Px int i2) {
        if (((float) i2) != this.h.c()) {
            this.h.c((float) i2);
            this.g.invalidateSelf();
        }
    }

    private void a(int i2) {
        if (this.l != i2 && ViewCompat.isLaidOut(this)) {
            if (this.j != null) {
                this.j.cancel();
            }
            ArrayList arrayList = new ArrayList();
            a(i2, arrayList);
            b(i2, arrayList);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(arrayList);
            this.j = animatorSet;
            this.j.addListener(new AnimatorListenerAdapter() {
                /* class android.support.design.bottomappbar.BottomAppBar.AnonymousClass1 */

                public void onAnimationEnd(Animator animator) {
                    BottomAppBar.this.j = null;
                }
            });
            this.j.start();
        }
    }

    private void a(int i2, List<Animator> list) {
        if (this.n) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(this.h.a(), (float) b(i2));
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                /* class android.support.design.bottomappbar.BottomAppBar.AnonymousClass2 */

                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    BottomAppBar.this.h.a(((Float) valueAnimator.getAnimatedValue()).floatValue());
                    BottomAppBar.this.g.invalidateSelf();
                }
            });
            ofFloat.setDuration(300L);
            list.add(ofFloat);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    @Nullable
    private FloatingActionButton m() {
        if (!(getParent() instanceof CoordinatorLayout)) {
            return null;
        }
        for (View view : ((CoordinatorLayout) getParent()).d(this)) {
            if (view instanceof FloatingActionButton) {
                return (FloatingActionButton) view;
            }
        }
        return null;
    }

    private boolean n() {
        FloatingActionButton m2 = m();
        return m2 != null && m2.b();
    }

    private void b(int i2, List<Animator> list) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(m(), "translationX", (float) b(i2));
        ofFloat.setDuration(300L);
        list.add(ofFloat);
    }

    private void a(int i2, boolean z) {
        if (ViewCompat.isLaidOut(this)) {
            if (this.k != null) {
                this.k.cancel();
            }
            ArrayList arrayList = new ArrayList();
            if (!n()) {
                z = false;
                i2 = 0;
            }
            a(i2, z, arrayList);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(arrayList);
            this.k = animatorSet;
            this.k.addListener(new AnimatorListenerAdapter() {
                /* class android.support.design.bottomappbar.BottomAppBar.AnonymousClass3 */

                public void onAnimationEnd(Animator animator) {
                    BottomAppBar.this.k = null;
                }
            });
            this.k.start();
        }
    }

    private void a(final int i2, final boolean z, List<Animator> list) {
        final ActionMenuView actionMenuView = getActionMenuView();
        if (actionMenuView != null) {
            Animator ofFloat = ObjectAnimator.ofFloat(actionMenuView, "alpha", 1.0f);
            if ((this.n || (z && n())) && (this.l == 1 || i2 == 1)) {
                ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(actionMenuView, "alpha", 0.0f);
                ofFloat2.addListener(new AnimatorListenerAdapter() {
                    /* class android.support.design.bottomappbar.BottomAppBar.AnonymousClass4 */

                    /* renamed from: a  reason: collision with root package name */
                    public boolean f237a;

                    public void onAnimationCancel(Animator animator) {
                        this.f237a = true;
                    }

                    public void onAnimationEnd(Animator animator) {
                        if (!this.f237a) {
                            BottomAppBar.this.a((BottomAppBar) actionMenuView, (ActionMenuView) i2, (int) z);
                        }
                    }
                });
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.setDuration(150L);
                animatorSet.playSequentially(ofFloat2, ofFloat);
                list.add(animatorSet);
            } else if (actionMenuView.getAlpha() < 1.0f) {
                list.add(ofFloat);
            }
        }
    }

    private float a(boolean z) {
        FloatingActionButton m2 = m();
        if (m2 == null) {
            return 0.0f;
        }
        Rect rect = new Rect();
        m2.a(rect);
        float height = (float) rect.height();
        if (height == 0.0f) {
            height = (float) m2.getMeasuredHeight();
        }
        float height2 = (height / 2.0f) + (-getCradleVerticalOffset()) + ((float) (m2.getHeight() - rect.bottom));
        float height3 = ((float) (m2.getHeight() - rect.height())) - ((float) m2.getPaddingBottom());
        float f2 = (float) (-getMeasuredHeight());
        if (!z) {
            height2 = height3;
        }
        return height2 + f2;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float getFabTranslationY() {
        return a(this.n);
    }

    private int b(int i2) {
        boolean z;
        int i3 = 1;
        if (ViewCompat.getLayoutDirection(this) == 1) {
            z = true;
        } else {
            z = false;
        }
        if (i2 != 1) {
            return 0;
        }
        int measuredWidth = (getMeasuredWidth() / 2) - this.f;
        if (z) {
            i3 = -1;
        }
        return i3 * measuredWidth;
    }

    private float getFabTranslationX() {
        return (float) b(this.l);
    }

    @Nullable
    private ActionMenuView getActionMenuView() {
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt instanceof ActionMenuView) {
                return (ActionMenuView) childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(ActionMenuView actionMenuView, int i2, boolean z) {
        boolean z2 = ViewCompat.getLayoutDirection(this) == 1;
        int i3 = 0;
        for (int i4 = 0; i4 < getChildCount(); i4++) {
            View childAt = getChildAt(i4);
            if ((childAt.getLayoutParams() instanceof Toolbar.b) && (((Toolbar.b) childAt.getLayoutParams()).f461a & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK) == 8388611) {
                i3 = Math.max(i3, z2 ? childAt.getLeft() : childAt.getRight());
            }
        }
        actionMenuView.setTranslationX((i2 != 1 || !z) ? 0.0f : (float) (i3 - (z2 ? actionMenuView.getRight() : actionMenuView.getLeft())));
    }

    private void o() {
        if (this.i != null) {
            this.i.cancel();
        }
        if (this.k != null) {
            this.k.cancel();
        }
        if (this.j != null) {
            this.j.cancel();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean p() {
        return (this.i != null && this.i.isRunning()) || (this.k != null && this.k.isRunning()) || (this.j != null && this.j.isRunning());
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.widget.Toolbar
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        o();
        q();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void q() {
        this.h.a(getFabTranslationX());
        FloatingActionButton m2 = m();
        this.g.a((!this.n || !n()) ? 0.0f : 1.0f);
        if (m2 != null) {
            m2.setTranslationY(getFabTranslationY());
            m2.setTranslationX(getFabTranslationX());
        }
        ActionMenuView actionMenuView = getActionMenuView();
        if (actionMenuView != null) {
            actionMenuView.setAlpha(1.0f);
            if (!n()) {
                a(actionMenuView, 0, false);
            } else {
                a(actionMenuView, this.l, this.n);
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(@NonNull FloatingActionButton floatingActionButton) {
        b(floatingActionButton);
        floatingActionButton.c(this.f233a);
        floatingActionButton.a(this.f233a);
    }

    private void b(@NonNull FloatingActionButton floatingActionButton) {
        floatingActionButton.d(this.f233a);
        floatingActionButton.b(this.f233a);
    }

    @Override // android.support.v7.widget.Toolbar
    public void setTitle(CharSequence charSequence) {
    }

    @Override // android.support.v7.widget.Toolbar
    public void setSubtitle(CharSequence charSequence) {
    }

    @Override // android.support.design.widget.CoordinatorLayout.a
    @NonNull
    public CoordinatorLayout.b<BottomAppBar> getBehavior() {
        return new Behavior();
    }

    public static class Behavior extends HideBottomViewOnScrollBehavior<BottomAppBar> {

        /* renamed from: a  reason: collision with root package name */
        private final Rect f238a = new Rect();

        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        private boolean a(FloatingActionButton floatingActionButton, BottomAppBar bottomAppBar) {
            ((CoordinatorLayout.e) floatingActionButton.getLayoutParams()).d = 17;
            bottomAppBar.a((BottomAppBar) floatingActionButton);
            return true;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, BottomAppBar bottomAppBar, int i) {
            FloatingActionButton m = bottomAppBar.m();
            if (m != null) {
                a(m, bottomAppBar);
                m.b(this.f238a);
                bottomAppBar.setFabDiameter(this.f238a.height());
            }
            if (!bottomAppBar.p()) {
                bottomAppBar.q();
            }
            coordinatorLayout.a(bottomAppBar, i);
            return super.a(coordinatorLayout, (View) bottomAppBar, i);
        }

        public boolean a(@NonNull CoordinatorLayout coordinatorLayout, @NonNull BottomAppBar bottomAppBar, @NonNull View view, @NonNull View view2, int i, int i2) {
            return bottomAppBar.getHideOnScroll() && super.a(coordinatorLayout, bottomAppBar, view, view2, i, i2);
        }

        /* access modifiers changed from: protected */
        public void a(BottomAppBar bottomAppBar) {
            super.a((View) bottomAppBar);
            FloatingActionButton m = bottomAppBar.m();
            if (m != null) {
                m.clearAnimation();
                m.animate().translationY(bottomAppBar.getFabTranslationY()).setInterpolator(a.d).setDuration(225);
            }
        }

        /* access modifiers changed from: protected */
        public void b(BottomAppBar bottomAppBar) {
            super.b((View) bottomAppBar);
            FloatingActionButton m = bottomAppBar.m();
            if (m != null) {
                m.a(this.f238a);
                m.clearAnimation();
                m.animate().translationY(((float) (-m.getPaddingBottom())) + ((float) (m.getMeasuredHeight() - this.f238a.height()))).setInterpolator(a.c).setDuration(175);
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.widget.Toolbar
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f239a = this.l;
        savedState.b = this.n;
        return savedState;
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.widget.Toolbar
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.l = savedState.f239a;
        this.n = savedState.b;
    }

    /* access modifiers changed from: package-private */
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            /* class android.support.design.bottomappbar.BottomAppBar.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f239a;
        boolean b;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f239a = parcel.readInt();
            this.b = parcel.readInt() != 0;
        }

        @Override // android.support.v4.view.AbsSavedState
        public void writeToParcel(@NonNull Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f239a);
            parcel.writeInt(this.b ? 1 : 0);
        }
    }
}
