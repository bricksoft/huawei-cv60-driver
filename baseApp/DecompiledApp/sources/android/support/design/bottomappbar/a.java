package android.support.design.bottomappbar;

import android.support.design.g.b;
import android.support.design.g.d;

public class a extends b {

    /* renamed from: a  reason: collision with root package name */
    private float f240a;
    private float b;
    private float c;
    private float d;
    private float e;

    @Override // android.support.design.g.b
    public void a(float f, float f2, d dVar) {
        if (this.c == 0.0f) {
            dVar.b(f, 0.0f);
            return;
        }
        float f3 = ((this.b * 2.0f) + this.c) / 2.0f;
        float f4 = f2 * this.f240a;
        float f5 = (f / 2.0f) + this.e;
        float f6 = (this.d * f2) + ((1.0f - f2) * f3);
        if (f6 / f3 >= 1.0f) {
            dVar.b(f, 0.0f);
            return;
        }
        float f7 = f3 + f4;
        float f8 = f6 + f4;
        float sqrt = (float) Math.sqrt((double) ((f7 * f7) - (f8 * f8)));
        float f9 = f5 - sqrt;
        float f10 = f5 + sqrt;
        float degrees = (float) Math.toDegrees(Math.atan((double) (sqrt / f8)));
        float f11 = 90.0f - degrees;
        dVar.b(f9 - f4, 0.0f);
        dVar.a(f9 - f4, 0.0f, f9 + f4, f4 * 2.0f, 270.0f, degrees);
        dVar.a(f5 - f3, (-f3) - f6, f5 + f3, f3 - f6, 180.0f - f11, (2.0f * f11) - 180.0f);
        dVar.a(f10 - f4, 0.0f, f10 + f4, f4 * 2.0f, 270.0f - degrees, degrees);
        dVar.b(f, 0.0f);
    }

    /* access modifiers changed from: package-private */
    public void a(float f) {
        this.e = f;
    }

    /* access modifiers changed from: package-private */
    public float a() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public float b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void b(float f) {
        this.d = f;
    }

    /* access modifiers changed from: package-private */
    public float c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void c(float f) {
        this.c = f;
    }

    /* access modifiers changed from: package-private */
    public float d() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void d(float f) {
        this.b = f;
    }

    /* access modifiers changed from: package-private */
    public float e() {
        return this.f240a;
    }

    /* access modifiers changed from: package-private */
    public void e(float f) {
        this.f240a = f;
    }
}
