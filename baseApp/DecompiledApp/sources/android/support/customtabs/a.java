package android.support.customtabs;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.BundleCompat;
import android.support.v4.content.ContextCompat;
import java.util.ArrayList;

public final class a {
    @NonNull

    /* renamed from: a  reason: collision with root package name */
    public final Intent f217a;
    @Nullable
    public final Bundle b;

    public void a(Context context, Uri uri) {
        this.f217a.setData(uri);
        ContextCompat.startActivity(context, this.f217a, this.b);
    }

    private a(Intent intent, Bundle bundle) {
        this.f217a = intent;
        this.b = bundle;
    }

    /* renamed from: android.support.customtabs.a$a  reason: collision with other inner class name */
    public static final class C0013a {

        /* renamed from: a  reason: collision with root package name */
        private final Intent f218a;
        private ArrayList<Bundle> b;
        private Bundle c;
        private ArrayList<Bundle> d;
        private boolean e;

        public C0013a() {
            this(null);
        }

        public C0013a(@Nullable b bVar) {
            IBinder iBinder = null;
            this.f218a = new Intent("android.intent.action.VIEW");
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = true;
            if (bVar != null) {
                this.f218a.setPackage(bVar.b().getPackageName());
            }
            Bundle bundle = new Bundle();
            BundleCompat.putBinder(bundle, "android.support.customtabs.extra.SESSION", bVar != null ? bVar.a() : iBinder);
            this.f218a.putExtras(bundle);
        }

        public a a() {
            if (this.b != null) {
                this.f218a.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", this.b);
            }
            if (this.d != null) {
                this.f218a.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", this.d);
            }
            this.f218a.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.e);
            return new a(this.f218a, this.c);
        }
    }
}
