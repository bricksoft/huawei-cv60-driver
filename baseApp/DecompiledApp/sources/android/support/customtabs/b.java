package android.support.customtabs;

import android.content.ComponentName;
import android.os.IBinder;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final c f219a;
    private final ComponentName b;

    /* access modifiers changed from: package-private */
    public IBinder a() {
        return this.f219a.asBinder();
    }

    /* access modifiers changed from: package-private */
    public ComponentName b() {
        return this.b;
    }
}
