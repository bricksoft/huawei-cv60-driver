package android.support.v7.b.a;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.graphics.drawable.i;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v4.util.LongSparseArray;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.appcompat.R;
import android.support.v7.b.a.b;
import android.support.v7.b.a.d;
import android.util.AttributeSet;
import android.util.StateSet;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import tv.danmaku.ijk.media.player.IjkMediaMeta;

public class a extends d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f479a = a.class.getSimpleName();
    private b b;
    private f c;
    private int d;
    private int e;
    private boolean f;

    @Override // android.support.v7.b.a.d, android.support.v7.b.a.b
    @RequiresApi(21)
    public /* bridge */ /* synthetic */ void applyTheme(@NonNull Resources.Theme theme) {
        super.applyTheme(theme);
    }

    @Override // android.support.v7.b.a.b
    @RequiresApi(21)
    public /* bridge */ /* synthetic */ boolean canApplyTheme() {
        return super.canApplyTheme();
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void draw(@NonNull Canvas canvas) {
        super.draw(canvas);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ int getAlpha() {
        return super.getAlpha();
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ int getChangingConfigurations() {
        return super.getChangingConfigurations();
    }

    @Override // android.support.v7.b.a.b
    @NonNull
    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void getHotspotBounds(@NonNull Rect rect) {
        super.getHotspotBounds(rect);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ int getIntrinsicHeight() {
        return super.getIntrinsicHeight();
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ int getIntrinsicWidth() {
        return super.getIntrinsicWidth();
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ int getOpacity() {
        return super.getOpacity();
    }

    @Override // android.support.v7.b.a.b
    @RequiresApi(21)
    public /* bridge */ /* synthetic */ void getOutline(@NonNull Outline outline) {
        super.getOutline(outline);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ boolean getPadding(@NonNull Rect rect) {
        return super.getPadding(rect);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void invalidateDrawable(@NonNull Drawable drawable) {
        super.invalidateDrawable(drawable);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ boolean isAutoMirrored() {
        return super.isAutoMirrored();
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ boolean onLayoutDirectionChanged(int i) {
        return super.onLayoutDirectionChanged(i);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void scheduleDrawable(@NonNull Drawable drawable, @NonNull Runnable runnable, long j) {
        super.scheduleDrawable(drawable, runnable, j);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void setAlpha(int i) {
        super.setAlpha(i);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void setAutoMirrored(boolean z) {
        super.setAutoMirrored(z);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void setColorFilter(ColorFilter colorFilter) {
        super.setColorFilter(colorFilter);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void setDither(boolean z) {
        super.setDither(z);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void setHotspot(float f2, float f3) {
        super.setHotspot(f2, f3);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void setTintList(ColorStateList colorStateList) {
        super.setTintList(colorStateList);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void setTintMode(@NonNull PorterDuff.Mode mode) {
        super.setTintMode(mode);
    }

    @Override // android.support.v7.b.a.b
    public /* bridge */ /* synthetic */ void unscheduleDrawable(@NonNull Drawable drawable, @NonNull Runnable runnable) {
        super.unscheduleDrawable(drawable, runnable);
    }

    public a() {
        this(null, null);
    }

    a(@Nullable b bVar, @Nullable Resources resources) {
        super(null);
        this.d = -1;
        this.e = -1;
        a(new b(bVar, this, resources));
        onStateChange(getState());
        jumpToCurrentState();
    }

    public static a a(@NonNull Context context, @NonNull Resources resources, @NonNull XmlPullParser xmlPullParser, @NonNull AttributeSet attributeSet, @Nullable Resources.Theme theme) {
        String name = xmlPullParser.getName();
        if (!name.equals("animated-selector")) {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid animated-selector tag " + name);
        }
        a aVar = new a();
        aVar.b(context, resources, xmlPullParser, attributeSet, theme);
        return aVar;
    }

    @Override // android.support.v7.b.a.d
    public void b(@NonNull Context context, @NonNull Resources resources, @NonNull XmlPullParser xmlPullParser, @NonNull AttributeSet attributeSet, @Nullable Resources.Theme theme) {
        TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, R.styleable.AnimatedStateListDrawableCompat);
        setVisible(obtainAttributes.getBoolean(R.styleable.AnimatedStateListDrawableCompat_android_visible, true), true);
        a(obtainAttributes);
        a(resources);
        obtainAttributes.recycle();
        c(context, resources, xmlPullParser, attributeSet, theme);
        e();
    }

    @Override // android.support.v7.b.a.b
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (this.c != null && (visible || z2)) {
            if (z) {
                this.c.a();
            } else {
                jumpToCurrentState();
            }
        }
        return visible;
    }

    @Override // android.support.v7.b.a.d, android.support.v7.b.a.b
    public boolean isStateful() {
        return true;
    }

    @Override // android.support.v7.b.a.b
    public void jumpToCurrentState() {
        super.jumpToCurrentState();
        if (this.c != null) {
            this.c.b();
            this.c = null;
            a(this.d);
            this.d = -1;
            this.e = -1;
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.b.a.d, android.support.v7.b.a.b
    public boolean onStateChange(int[] iArr) {
        int a2 = this.b.a(iArr);
        boolean z = a2 != d() && (b(a2) || a(a2));
        Drawable current = getCurrent();
        if (current != null) {
            return z | current.setState(iArr);
        }
        return z;
    }

    private boolean b(int i) {
        int d2;
        f aVar;
        f fVar = this.c;
        if (fVar == null) {
            d2 = d();
        } else if (i == this.d) {
            return true;
        } else {
            if (i != this.e || !fVar.c()) {
                int i2 = this.d;
                fVar.b();
                d2 = i2;
            } else {
                fVar.d();
                this.d = this.e;
                this.e = i;
                return true;
            }
        }
        this.c = null;
        this.e = -1;
        this.d = -1;
        b bVar = this.b;
        int a2 = bVar.a(d2);
        int a3 = bVar.a(i);
        if (a3 == 0 || a2 == 0) {
            return false;
        }
        int a4 = bVar.a(a2, a3);
        if (a4 < 0) {
            return false;
        }
        boolean c2 = bVar.c(a2, a3);
        a(a4);
        Drawable current = getCurrent();
        if (current instanceof AnimationDrawable) {
            aVar = new d((AnimationDrawable) current, bVar.b(a2, a3), c2);
        } else if (current instanceof android.support.graphics.drawable.c) {
            aVar = new c((android.support.graphics.drawable.c) current);
        } else if (!(current instanceof Animatable)) {
            return false;
        } else {
            aVar = new C0022a((Animatable) current);
        }
        aVar.a();
        this.c = aVar;
        this.e = d2;
        this.d = i;
        return true;
    }

    /* access modifiers changed from: private */
    public static abstract class f {
        public abstract void a();

        public abstract void b();

        private f() {
        }

        public void d() {
        }

        public boolean c() {
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: android.support.v7.b.a.a$a  reason: collision with other inner class name */
    public static class C0022a extends f {

        /* renamed from: a  reason: collision with root package name */
        private final Animatable f480a;

        C0022a(Animatable animatable) {
            super();
            this.f480a = animatable;
        }

        @Override // android.support.v7.b.a.a.f
        public void a() {
            this.f480a.start();
        }

        @Override // android.support.v7.b.a.a.f
        public void b() {
            this.f480a.stop();
        }
    }

    /* access modifiers changed from: private */
    public static class d extends f {

        /* renamed from: a  reason: collision with root package name */
        private final ObjectAnimator f483a;
        private final boolean b;

        d(AnimationDrawable animationDrawable, boolean z, boolean z2) {
            super();
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            int i = z ? numberOfFrames - 1 : 0;
            int i2 = z ? 0 : numberOfFrames - 1;
            e eVar = new e(animationDrawable, z);
            ObjectAnimator ofInt = ObjectAnimator.ofInt(animationDrawable, "currentIndex", i, i2);
            if (Build.VERSION.SDK_INT >= 18) {
                ofInt.setAutoCancel(true);
            }
            ofInt.setDuration((long) eVar.a());
            ofInt.setInterpolator(eVar);
            this.b = z2;
            this.f483a = ofInt;
        }

        @Override // android.support.v7.b.a.a.f
        public boolean c() {
            return this.b;
        }

        @Override // android.support.v7.b.a.a.f
        public void a() {
            this.f483a.start();
        }

        @Override // android.support.v7.b.a.a.f
        public void d() {
            this.f483a.reverse();
        }

        @Override // android.support.v7.b.a.a.f
        public void b() {
            this.f483a.cancel();
        }
    }

    /* access modifiers changed from: private */
    public static class c extends f {

        /* renamed from: a  reason: collision with root package name */
        private final android.support.graphics.drawable.c f482a;

        c(android.support.graphics.drawable.c cVar) {
            super();
            this.f482a = cVar;
        }

        @Override // android.support.v7.b.a.a.f
        public void a() {
            this.f482a.start();
        }

        @Override // android.support.v7.b.a.a.f
        public void b() {
            this.f482a.stop();
        }
    }

    private void a(TypedArray typedArray) {
        b bVar = this.b;
        if (Build.VERSION.SDK_INT >= 21) {
            bVar.f |= typedArray.getChangingConfigurations();
        }
        bVar.a(typedArray.getBoolean(R.styleable.AnimatedStateListDrawableCompat_android_variablePadding, bVar.k));
        bVar.b(typedArray.getBoolean(R.styleable.AnimatedStateListDrawableCompat_android_constantSize, bVar.n));
        bVar.c(typedArray.getInt(R.styleable.AnimatedStateListDrawableCompat_android_enterFadeDuration, bVar.C));
        bVar.d(typedArray.getInt(R.styleable.AnimatedStateListDrawableCompat_android_exitFadeDuration, bVar.D));
        setDither(typedArray.getBoolean(R.styleable.AnimatedStateListDrawableCompat_android_dither, bVar.z));
    }

    private void e() {
        onStateChange(getState());
    }

    private void c(@NonNull Context context, @NonNull Resources resources, @NonNull XmlPullParser xmlPullParser, @NonNull AttributeSet attributeSet, @Nullable Resources.Theme theme) {
        int depth = xmlPullParser.getDepth() + 1;
        while (true) {
            int next = xmlPullParser.next();
            if (next != 1) {
                int depth2 = xmlPullParser.getDepth();
                if (depth2 < depth && next == 3) {
                    return;
                }
                if (next == 2 && depth2 <= depth) {
                    if (xmlPullParser.getName().equals("item")) {
                        e(context, resources, xmlPullParser, attributeSet, theme);
                    } else if (xmlPullParser.getName().equals("transition")) {
                        d(context, resources, xmlPullParser, attributeSet, theme);
                    }
                }
            } else {
                return;
            }
        }
    }

    private int d(@NonNull Context context, @NonNull Resources resources, @NonNull XmlPullParser xmlPullParser, @NonNull AttributeSet attributeSet, @Nullable Resources.Theme theme) {
        int next;
        TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, R.styleable.AnimatedStateListDrawableTransition);
        int resourceId = obtainAttributes.getResourceId(R.styleable.AnimatedStateListDrawableTransition_android_fromId, -1);
        int resourceId2 = obtainAttributes.getResourceId(R.styleable.AnimatedStateListDrawableTransition_android_toId, -1);
        Drawable drawable = null;
        int resourceId3 = obtainAttributes.getResourceId(R.styleable.AnimatedStateListDrawableTransition_android_drawable, -1);
        if (resourceId3 > 0) {
            drawable = android.support.v7.a.a.a.b(context, resourceId3);
        }
        boolean z = obtainAttributes.getBoolean(R.styleable.AnimatedStateListDrawableTransition_android_reversible, false);
        obtainAttributes.recycle();
        if (drawable == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next != 2) {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
            } else if (xmlPullParser.getName().equals("animated-vector")) {
                drawable = android.support.graphics.drawable.c.a(context, resources, xmlPullParser, attributeSet, theme);
            } else if (Build.VERSION.SDK_INT >= 21) {
                drawable = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
            } else {
                drawable = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            }
        }
        if (drawable == null) {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
        } else if (resourceId != -1 && resourceId2 != -1) {
            return this.b.a(resourceId, resourceId2, drawable, z);
        } else {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires 'fromId' & 'toId' attributes");
        }
    }

    private int e(@NonNull Context context, @NonNull Resources resources, @NonNull XmlPullParser xmlPullParser, @NonNull AttributeSet attributeSet, @Nullable Resources.Theme theme) {
        int next;
        TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, theme, attributeSet, R.styleable.AnimatedStateListDrawableItem);
        int resourceId = obtainAttributes.getResourceId(R.styleable.AnimatedStateListDrawableItem_android_id, 0);
        Drawable drawable = null;
        int resourceId2 = obtainAttributes.getResourceId(R.styleable.AnimatedStateListDrawableItem_android_drawable, -1);
        if (resourceId2 > 0) {
            drawable = android.support.v7.a.a.a.b(context, resourceId2);
        }
        obtainAttributes.recycle();
        int[] a2 = a(attributeSet);
        if (drawable == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next != 2) {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
            } else if (xmlPullParser.getName().equals("vector")) {
                drawable = i.a(resources, xmlPullParser, attributeSet, theme);
            } else if (Build.VERSION.SDK_INT >= 21) {
                drawable = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
            } else {
                drawable = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            }
        }
        if (drawable != null) {
            return this.b.a(a2, drawable, resourceId);
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
    }

    @Override // android.support.v7.b.a.d, android.support.v7.b.a.b
    public Drawable mutate() {
        if (!this.f && super.mutate() == this) {
            this.b.a();
            this.f = true;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public b c() {
        return new b(this.b, this, null);
    }

    /* access modifiers changed from: package-private */
    public static class b extends d.a {

        /* renamed from: a  reason: collision with root package name */
        LongSparseArray<Long> f481a;
        SparseArrayCompat<Integer> b;

        b(@Nullable b bVar, @NonNull a aVar, @Nullable Resources resources) {
            super(bVar, aVar, resources);
            if (bVar != null) {
                this.f481a = bVar.f481a;
                this.b = bVar.b;
                return;
            }
            this.f481a = new LongSparseArray<>();
            this.b = new SparseArrayCompat<>();
        }

        /* access modifiers changed from: package-private */
        @Override // android.support.v7.b.a.d.a, android.support.v7.b.a.b.AbstractC0023b
        public void a() {
            this.f481a = this.f481a.clone();
            this.b = this.b.clone();
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2, @NonNull Drawable drawable, boolean z) {
            int a2 = super.a(drawable);
            long f = f(i, i2);
            long j = 0;
            if (z) {
                j = IjkMediaMeta.AV_CH_SURROUND_DIRECT_LEFT;
            }
            this.f481a.append(f, Long.valueOf(((long) a2) | j));
            if (z) {
                this.f481a.append(f(i2, i), Long.valueOf(j | ((long) a2) | IjkMediaMeta.AV_CH_WIDE_RIGHT));
            }
            return a2;
        }

        /* access modifiers changed from: package-private */
        public int a(@NonNull int[] iArr, @NonNull Drawable drawable, int i) {
            int a2 = super.a(iArr, drawable);
            this.b.put(a2, Integer.valueOf(i));
            return a2;
        }

        /* access modifiers changed from: package-private */
        public int a(@NonNull int[] iArr) {
            int b2 = super.b(iArr);
            return b2 >= 0 ? b2 : super.b(StateSet.WILD_CARD);
        }

        /* access modifiers changed from: package-private */
        public int a(int i) {
            if (i < 0) {
                return 0;
            }
            return this.b.get(i, 0).intValue();
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2) {
            return (int) this.f481a.get(f(i, i2), -1L).longValue();
        }

        /* access modifiers changed from: package-private */
        public boolean b(int i, int i2) {
            return (this.f481a.get(f(i, i2), -1L).longValue() & IjkMediaMeta.AV_CH_WIDE_RIGHT) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean c(int i, int i2) {
            return (this.f481a.get(f(i, i2), -1L).longValue() & IjkMediaMeta.AV_CH_SURROUND_DIRECT_LEFT) != 0;
        }

        @Override // android.support.v7.b.a.d.a
        @NonNull
        public Drawable newDrawable() {
            return new a(this, null);
        }

        @Override // android.support.v7.b.a.d.a
        @NonNull
        public Drawable newDrawable(Resources resources) {
            return new a(this, resources);
        }

        private static long f(int i, int i2) {
            return (((long) i) << 32) | ((long) i2);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.b.a.d, android.support.v7.b.a.b
    public void a(@NonNull b.AbstractC0023b bVar) {
        super.a(bVar);
        if (bVar instanceof b) {
            this.b = (b) bVar;
        }
    }

    private static class e implements TimeInterpolator {

        /* renamed from: a  reason: collision with root package name */
        private int[] f484a;
        private int b;
        private int c;

        e(AnimationDrawable animationDrawable, boolean z) {
            a(animationDrawable, z);
        }

        /* access modifiers changed from: package-private */
        public int a(AnimationDrawable animationDrawable, boolean z) {
            int i;
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            this.b = numberOfFrames;
            if (this.f484a == null || this.f484a.length < numberOfFrames) {
                this.f484a = new int[numberOfFrames];
            }
            int[] iArr = this.f484a;
            int i2 = 0;
            int i3 = 0;
            while (i2 < numberOfFrames) {
                if (z) {
                    i = (numberOfFrames - i2) - 1;
                } else {
                    i = i2;
                }
                int duration = animationDrawable.getDuration(i);
                iArr[i2] = duration;
                i2++;
                i3 = duration + i3;
            }
            this.c = i3;
            return i3;
        }

        /* access modifiers changed from: package-private */
        public int a() {
            return this.c;
        }

        public float getInterpolation(float f) {
            float f2;
            int i = (int) ((((float) this.c) * f) + 0.5f);
            int i2 = this.b;
            int[] iArr = this.f484a;
            int i3 = 0;
            while (i3 < i2 && i >= iArr[i3]) {
                i -= iArr[i3];
                i3++;
            }
            if (i3 < i2) {
                f2 = ((float) i) / ((float) this.c);
            } else {
                f2 = 0.0f;
            }
            return f2 + (((float) i3) / ((float) i2));
        }
    }
}
