package android.support.v7.b.a;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.graphics.drawable.DrawableCompat;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class c extends Drawable implements Drawable.Callback {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f488a;

    public c(Drawable drawable) {
        a(drawable);
    }

    public void draw(Canvas canvas) {
        this.f488a.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.f488a.setBounds(rect);
    }

    public void setChangingConfigurations(int i) {
        this.f488a.setChangingConfigurations(i);
    }

    public int getChangingConfigurations() {
        return this.f488a.getChangingConfigurations();
    }

    public void setDither(boolean z) {
        this.f488a.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f488a.setFilterBitmap(z);
    }

    public void setAlpha(int i) {
        this.f488a.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f488a.setColorFilter(colorFilter);
    }

    public boolean isStateful() {
        return this.f488a.isStateful();
    }

    public boolean setState(int[] iArr) {
        return this.f488a.setState(iArr);
    }

    public int[] getState() {
        return this.f488a.getState();
    }

    public void jumpToCurrentState() {
        DrawableCompat.jumpToCurrentState(this.f488a);
    }

    public Drawable getCurrent() {
        return this.f488a.getCurrent();
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f488a.setVisible(z, z2);
    }

    public int getOpacity() {
        return this.f488a.getOpacity();
    }

    public Region getTransparentRegion() {
        return this.f488a.getTransparentRegion();
    }

    public int getIntrinsicWidth() {
        return this.f488a.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        return this.f488a.getIntrinsicHeight();
    }

    public int getMinimumWidth() {
        return this.f488a.getMinimumWidth();
    }

    public int getMinimumHeight() {
        return this.f488a.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        return this.f488a.getPadding(rect);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f488a.setLevel(i);
    }

    public void setAutoMirrored(boolean z) {
        DrawableCompat.setAutoMirrored(this.f488a, z);
    }

    public boolean isAutoMirrored() {
        return DrawableCompat.isAutoMirrored(this.f488a);
    }

    public void setTint(int i) {
        DrawableCompat.setTint(this.f488a, i);
    }

    public void setTintList(ColorStateList colorStateList) {
        DrawableCompat.setTintList(this.f488a, colorStateList);
    }

    public void setTintMode(PorterDuff.Mode mode) {
        DrawableCompat.setTintMode(this.f488a, mode);
    }

    public void setHotspot(float f, float f2) {
        DrawableCompat.setHotspot(this.f488a, f, f2);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        DrawableCompat.setHotspotBounds(this.f488a, i, i2, i3, i4);
    }

    public Drawable b() {
        return this.f488a;
    }

    public void a(Drawable drawable) {
        if (this.f488a != null) {
            this.f488a.setCallback(null);
        }
        this.f488a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
