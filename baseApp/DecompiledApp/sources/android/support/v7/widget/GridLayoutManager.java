package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.exoplayer.C;
import java.util.Arrays;

public class GridLayoutManager extends LinearLayoutManager {

    /* renamed from: a  reason: collision with root package name */
    boolean f559a = false;
    int b = -1;
    int[] c;
    View[] d;
    final SparseIntArray e = new SparseIntArray();
    final SparseIntArray f = new SparseIntArray();
    c g = new a();
    final Rect h = new Rect();

    public GridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        a(a(context, attributeSet, i, i2).b);
    }

    @Override // android.support.v7.widget.LinearLayoutManager
    public void a(boolean z) {
        if (z) {
            throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
        }
        super.a(false);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int a(RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.i == 0) {
            return this.b;
        }
        if (tVar.e() < 1) {
            return 0;
        }
        return a(pVar, tVar, tVar.e() - 1) + 1;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int b(RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.i == 1) {
            return this.b;
        }
        if (tVar.e() < 1) {
            return 0;
        }
        return a(pVar, tVar, tVar.e() - 1) + 1;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView.p pVar, RecyclerView.t tVar, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof b)) {
            super.a(view, accessibilityNodeInfoCompat);
            return;
        }
        b bVar = (b) layoutParams;
        int a2 = a(pVar, tVar, bVar.f());
        if (this.i == 0) {
            accessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(bVar.a(), bVar.b(), a2, 1, this.b > 1 && bVar.b() == this.b, false));
        } else {
            accessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(a2, 1, bVar.a(), bVar.b(), this.b > 1 && bVar.b() == this.b, false));
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i, android.support.v7.widget.LinearLayoutManager
    public void c(RecyclerView.p pVar, RecyclerView.t tVar) {
        if (tVar.a()) {
            L();
        }
        super.c(pVar, tVar);
        K();
    }

    @Override // android.support.v7.widget.RecyclerView.i, android.support.v7.widget.LinearLayoutManager
    public void a(RecyclerView.t tVar) {
        super.a(tVar);
        this.f559a = false;
    }

    private void K() {
        this.e.clear();
        this.f.clear();
    }

    private void L() {
        int v = v();
        for (int i = 0; i < v; i++) {
            b bVar = (b) i(i).getLayoutParams();
            int f2 = bVar.f();
            this.e.put(f2, bVar.b());
            this.f.put(f2, bVar.a());
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView, int i, int i2) {
        this.g.a();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView) {
        this.g.a();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void b(RecyclerView recyclerView, int i, int i2) {
        this.g.a();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView, int i, int i2, Object obj) {
        this.g.a();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView, int i, int i2, int i3) {
        this.g.a();
    }

    @Override // android.support.v7.widget.RecyclerView.i, android.support.v7.widget.LinearLayoutManager
    public RecyclerView.j a() {
        if (this.i == 0) {
            return new b(-2, -1);
        }
        return new b(-1, -2);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public RecyclerView.j a(Context context, AttributeSet attributeSet) {
        return new b(context, attributeSet);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public RecyclerView.j a(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new b((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new b(layoutParams);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean a(RecyclerView.j jVar) {
        return jVar instanceof b;
    }

    private void M() {
        int z;
        if (g() == 1) {
            z = (y() - C()) - A();
        } else {
            z = (z() - D()) - B();
        }
        m(z);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(Rect rect, int i, int i2) {
        int a2;
        int a3;
        if (this.c == null) {
            super.a(rect, i, i2);
        }
        int C = C() + A();
        int B = B() + D();
        if (this.i == 1) {
            a3 = a(i2, B + rect.height(), G());
            a2 = a(i, C + this.c[this.c.length - 1], F());
        } else {
            a2 = a(i, C + rect.width(), F());
            a3 = a(i2, B + this.c[this.c.length - 1], G());
        }
        f(a2, a3);
    }

    private void m(int i) {
        this.c = a(this.c, this.b, i);
    }

    static int[] a(int[] iArr, int i, int i2) {
        int i3;
        if (!(iArr != null && iArr.length == i + 1 && iArr[iArr.length - 1] == i2)) {
            iArr = new int[(i + 1)];
        }
        iArr[0] = 0;
        int i4 = i2 / i;
        int i5 = i2 % i;
        int i6 = 1;
        int i7 = 0;
        int i8 = 0;
        while (i6 <= i) {
            i7 += i5;
            if (i7 <= 0 || i - i7 >= i5) {
                i3 = i4;
            } else {
                i3 = i4 + 1;
                i7 -= i;
            }
            int i9 = i8 + i3;
            iArr[i6] = i9;
            i6++;
            i8 = i9;
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public int a(int i, int i2) {
        if (this.i != 1 || !h()) {
            return this.c[i + i2] - this.c[i];
        }
        return this.c[this.b - i] - this.c[(this.b - i) - i2];
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v7.widget.LinearLayoutManager
    public void a(RecyclerView.p pVar, RecyclerView.t tVar, LinearLayoutManager.a aVar, int i) {
        super.a(pVar, tVar, aVar, i);
        M();
        if (tVar.e() > 0 && !tVar.a()) {
            b(pVar, tVar, aVar, i);
        }
        N();
    }

    private void N() {
        if (this.d == null || this.d.length != this.b) {
            this.d = new View[this.b];
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i, android.support.v7.widget.LinearLayoutManager
    public int a(int i, RecyclerView.p pVar, RecyclerView.t tVar) {
        M();
        N();
        return super.a(i, pVar, tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i, android.support.v7.widget.LinearLayoutManager
    public int b(int i, RecyclerView.p pVar, RecyclerView.t tVar) {
        M();
        N();
        return super.b(i, pVar, tVar);
    }

    private void b(RecyclerView.p pVar, RecyclerView.t tVar, LinearLayoutManager.a aVar, int i) {
        boolean z = true;
        if (i != 1) {
            z = false;
        }
        int b2 = b(pVar, tVar, aVar.b);
        if (z) {
            while (b2 > 0 && aVar.b > 0) {
                aVar.b--;
                b2 = b(pVar, tVar, aVar.b);
            }
            return;
        }
        int e2 = tVar.e() - 1;
        int i2 = aVar.b;
        int i3 = b2;
        while (i2 < e2) {
            int b3 = b(pVar, tVar, i2 + 1);
            if (b3 <= i3) {
                break;
            }
            i2++;
            i3 = b3;
        }
        aVar.b = i2;
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v7.widget.LinearLayoutManager
    public View a(RecyclerView.p pVar, RecyclerView.t tVar, int i, int i2, int i3) {
        View view;
        i();
        int c2 = this.j.c();
        int d2 = this.j.d();
        int i4 = i2 > i ? 1 : -1;
        View view2 = null;
        View view3 = null;
        while (i != i2) {
            View i5 = i(i);
            int d3 = d(i5);
            if (d3 >= 0 && d3 < i3) {
                if (b(pVar, tVar, d3) != 0) {
                    view = view2;
                } else if (((RecyclerView.j) i5.getLayoutParams()).d()) {
                    if (view3 == null) {
                        view = view2;
                        view3 = i5;
                    }
                } else if (this.j.a(i5) < d2 && this.j.b(i5) >= c2) {
                    return i5;
                } else {
                    if (view2 == null) {
                        view = i5;
                    }
                }
                i += i4;
                view2 = view;
            }
            view = view2;
            i += i4;
            view2 = view;
        }
        if (view2 == null) {
            view2 = view3;
        }
        return view2;
    }

    private int a(RecyclerView.p pVar, RecyclerView.t tVar, int i) {
        if (!tVar.a()) {
            return this.g.c(i, this.b);
        }
        int b2 = pVar.b(i);
        if (b2 != -1) {
            return this.g.c(b2, this.b);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. " + i);
        return 0;
    }

    private int b(RecyclerView.p pVar, RecyclerView.t tVar, int i) {
        if (!tVar.a()) {
            return this.g.b(i, this.b);
        }
        int i2 = this.f.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int b2 = pVar.b(i);
        if (b2 != -1) {
            return this.g.b(b2, this.b);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i);
        return 0;
    }

    private int c(RecyclerView.p pVar, RecyclerView.t tVar, int i) {
        if (!tVar.a()) {
            return this.g.a(i);
        }
        int i2 = this.e.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int b2 = pVar.b(i);
        if (b2 != -1) {
            return this.g.a(b2);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i);
        return 1;
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v7.widget.LinearLayoutManager
    public void a(RecyclerView.t tVar, LinearLayoutManager.c cVar, RecyclerView.i.a aVar) {
        int i = this.b;
        for (int i2 = 0; i2 < this.b && cVar.a(tVar) && i > 0; i2++) {
            int i3 = cVar.d;
            aVar.b(i3, Math.max(0, cVar.g));
            i -= this.g.a(i3);
            cVar.d += cVar.e;
        }
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v7.widget.LinearLayoutManager
    public void a(RecyclerView.p pVar, RecyclerView.t tVar, LinearLayoutManager.c cVar, LinearLayoutManager.b bVar) {
        int i;
        int makeMeasureSpec;
        int a2;
        View a3;
        int i2 = this.j.i();
        boolean z = i2 != 1073741824;
        int i3 = v() > 0 ? this.c[this.b] : 0;
        if (z) {
            M();
        }
        boolean z2 = cVar.e == 1;
        int i4 = 0;
        int i5 = 0;
        int i6 = this.b;
        if (!z2) {
            i6 = b(pVar, tVar, cVar.d) + c(pVar, tVar, cVar.d);
        }
        while (i4 < this.b && cVar.a(tVar) && i6 > 0) {
            int i7 = cVar.d;
            int c2 = c(pVar, tVar, i7);
            if (c2 <= this.b) {
                i6 -= c2;
                if (i6 < 0 || (a3 = cVar.a(pVar)) == null) {
                    break;
                }
                i5 += c2;
                this.d[i4] = a3;
                i4++;
            } else {
                throw new IllegalArgumentException("Item at position " + i7 + " requires " + c2 + " spans but GridLayoutManager has only " + this.b + " spans.");
            }
        }
        if (i4 == 0) {
            bVar.b = true;
            return;
        }
        a(pVar, tVar, i4, i5, z2);
        int i8 = 0;
        float f2 = 0.0f;
        int i9 = 0;
        while (i8 < i4) {
            View view = this.d[i8];
            if (cVar.k == null) {
                if (z2) {
                    b(view);
                } else {
                    b(view, 0);
                }
            } else if (z2) {
                a(view);
            } else {
                a(view, 0);
            }
            b(view, this.h);
            a(view, i2, false);
            int e2 = this.j.e(view);
            if (e2 > i9) {
                i9 = e2;
            }
            float f3 = (((float) this.j.f(view)) * 1.0f) / ((float) ((b) view.getLayoutParams()).b);
            if (f3 <= f2) {
                f3 = f2;
            }
            i8++;
            f2 = f3;
        }
        if (z) {
            a(f2, i3);
            i9 = 0;
            int i10 = 0;
            while (i10 < i4) {
                View view2 = this.d[i10];
                a(view2, C.ENCODING_PCM_32BIT, true);
                int e3 = this.j.e(view2);
                if (e3 <= i9) {
                    e3 = i9;
                }
                i10++;
                i9 = e3;
            }
        }
        for (int i11 = 0; i11 < i4; i11++) {
            View view3 = this.d[i11];
            if (this.j.e(view3) != i9) {
                b bVar2 = (b) view3.getLayoutParams();
                Rect rect = bVar2.d;
                int i12 = rect.top + rect.bottom + bVar2.topMargin + bVar2.bottomMargin;
                int i13 = rect.right + rect.left + bVar2.leftMargin + bVar2.rightMargin;
                int a4 = a(bVar2.f560a, bVar2.b);
                if (this.i == 1) {
                    makeMeasureSpec = a(a4, (int) C.ENCODING_PCM_32BIT, i13, bVar2.width, false);
                    a2 = View.MeasureSpec.makeMeasureSpec(i9 - i12, C.ENCODING_PCM_32BIT);
                } else {
                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i9 - i13, C.ENCODING_PCM_32BIT);
                    a2 = a(a4, (int) C.ENCODING_PCM_32BIT, i12, bVar2.height, false);
                }
                a(view3, makeMeasureSpec, a2, true);
            }
        }
        bVar.f565a = i9;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        if (this.i == 1) {
            if (cVar.f == -1) {
                i16 = cVar.b;
                i = i16 - i9;
            } else {
                int i17 = cVar.b;
                i16 = i17 + i9;
                i = i17;
            }
        } else if (cVar.f == -1) {
            i15 = cVar.b;
            i14 = i15 - i9;
            i = 0;
        } else {
            i14 = cVar.b;
            i15 = i14 + i9;
            i = 0;
        }
        int i18 = i16;
        int i19 = i;
        int i20 = i15;
        int i21 = i14;
        for (int i22 = 0; i22 < i4; i22++) {
            View view4 = this.d[i22];
            b bVar3 = (b) view4.getLayoutParams();
            if (this.i != 1) {
                i19 = B() + this.c[bVar3.f560a];
                i18 = i19 + this.j.f(view4);
            } else if (h()) {
                i20 = A() + this.c[this.b - bVar3.f560a];
                i21 = i20 - this.j.f(view4);
            } else {
                i21 = A() + this.c[bVar3.f560a];
                i20 = i21 + this.j.f(view4);
            }
            a(view4, i21, i19, i20, i18);
            if (bVar3.d() || bVar3.e()) {
                bVar.c = true;
            }
            bVar.d |= view4.hasFocusable();
        }
        Arrays.fill(this.d, (Object) null);
    }

    private void a(View view, int i, boolean z) {
        int a2;
        int i2;
        b bVar = (b) view.getLayoutParams();
        Rect rect = bVar.d;
        int i3 = rect.top + rect.bottom + bVar.topMargin + bVar.bottomMargin;
        int i4 = bVar.rightMargin + rect.right + rect.left + bVar.leftMargin;
        int a3 = a(bVar.f560a, bVar.b);
        if (this.i == 1) {
            int a4 = a(a3, i, i4, bVar.width, false);
            i2 = a(this.j.f(), x(), i3, bVar.height, true);
            a2 = a4;
        } else {
            int a5 = a(a3, i, i3, bVar.height, false);
            a2 = a(this.j.f(), w(), i4, bVar.width, true);
            i2 = a5;
        }
        a(view, a2, i2, z);
    }

    private void a(float f2, int i) {
        m(Math.max(Math.round(((float) this.b) * f2), i));
    }

    private void a(View view, int i, int i2, boolean z) {
        boolean b2;
        RecyclerView.j jVar = (RecyclerView.j) view.getLayoutParams();
        if (z) {
            b2 = a(view, i, i2, jVar);
        } else {
            b2 = b(view, i, i2, jVar);
        }
        if (b2) {
            view.measure(i, i2);
        }
    }

    private void a(RecyclerView.p pVar, RecyclerView.t tVar, int i, int i2, boolean z) {
        int i3;
        int i4;
        int i5 = 0;
        if (z) {
            i4 = 1;
            i3 = 0;
        } else {
            i3 = i - 1;
            i4 = -1;
            i = -1;
        }
        while (i3 != i) {
            View view = this.d[i3];
            b bVar = (b) view.getLayoutParams();
            bVar.b = c(pVar, tVar, d(view));
            bVar.f560a = i5;
            i5 += bVar.b;
            i3 += i4;
        }
    }

    public void a(int i) {
        if (i != this.b) {
            this.f559a = true;
            if (i < 1) {
                throw new IllegalArgumentException("Span count should be at least 1. Provided " + i);
            }
            this.b = i;
            this.g.a();
            o();
        }
    }

    public static abstract class c {

        /* renamed from: a  reason: collision with root package name */
        final SparseIntArray f561a = new SparseIntArray();
        private boolean b = false;

        public abstract int a(int i);

        public void a() {
            this.f561a.clear();
        }

        /* access modifiers changed from: package-private */
        public int b(int i, int i2) {
            if (!this.b) {
                return a(i, i2);
            }
            int i3 = this.f561a.get(i, -1);
            if (i3 != -1) {
                return i3;
            }
            int a2 = a(i, i2);
            this.f561a.put(i, a2);
            return a2;
        }

        public int a(int i, int i2) {
            int i3;
            int i4;
            int b2;
            int a2 = a(i);
            if (a2 == i2) {
                return 0;
            }
            if (!this.b || this.f561a.size() <= 0 || (b2 = b(i)) < 0) {
                i3 = 0;
                i4 = 0;
            } else {
                i4 = this.f561a.get(b2) + a(b2);
                i3 = b2 + 1;
            }
            int i5 = i3;
            while (i5 < i) {
                int a3 = a(i5);
                int i6 = i4 + a3;
                if (i6 == i2) {
                    a3 = 0;
                } else if (i6 <= i2) {
                    a3 = i6;
                }
                i5++;
                i4 = a3;
            }
            if (i4 + a2 <= i2) {
                return i4;
            }
            return 0;
        }

        /* access modifiers changed from: package-private */
        public int b(int i) {
            int i2 = 0;
            int size = this.f561a.size() - 1;
            while (i2 <= size) {
                int i3 = (i2 + size) >>> 1;
                if (this.f561a.keyAt(i3) < i) {
                    i2 = i3 + 1;
                } else {
                    size = i3 - 1;
                }
            }
            int i4 = i2 - 1;
            if (i4 < 0 || i4 >= this.f561a.size()) {
                return -1;
            }
            return this.f561a.keyAt(i4);
        }

        public int c(int i, int i2) {
            int a2 = a(i);
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            while (i3 < i) {
                int a3 = a(i3);
                int i6 = i5 + a3;
                if (i6 == i2) {
                    i4++;
                    a3 = 0;
                } else if (i6 > i2) {
                    i4++;
                } else {
                    a3 = i6;
                }
                i3++;
                i5 = a3;
            }
            if (i5 + a2 > i2) {
                return i4 + 1;
            }
            return i4;
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i, android.support.v7.widget.LinearLayoutManager
    public View a(View view, int i, RecyclerView.p pVar, RecyclerView.t tVar) {
        int i2;
        int i3;
        int v;
        boolean z;
        int i4;
        int min;
        View e2 = e(view);
        if (e2 == null) {
            return null;
        }
        b bVar = (b) e2.getLayoutParams();
        int i5 = bVar.f560a;
        int i6 = bVar.f560a + bVar.b;
        if (super.a(view, i, pVar, tVar) == null) {
            return null;
        }
        if ((f(i) == 1) != this.k) {
            i2 = v() - 1;
            i3 = -1;
            v = -1;
        } else {
            i2 = 0;
            i3 = 1;
            v = v();
        }
        if (this.i != 1 || !h()) {
            z = false;
        } else {
            z = true;
        }
        View view2 = null;
        int i7 = -1;
        int i8 = 0;
        View view3 = null;
        int i9 = -1;
        int i10 = 0;
        int a2 = a(pVar, tVar, i2);
        int i11 = i2;
        while (i11 != v) {
            int a3 = a(pVar, tVar, i11);
            View i12 = i(i11);
            if (i12 == e2) {
                break;
            }
            if (i12.hasFocusable() && a3 != a2) {
                if (view2 != null) {
                    break;
                }
            } else {
                b bVar2 = (b) i12.getLayoutParams();
                int i13 = bVar2.f560a;
                int i14 = bVar2.f560a + bVar2.b;
                if (i12.hasFocusable() && i13 == i5 && i14 == i6) {
                    return i12;
                }
                boolean z2 = false;
                if ((!i12.hasFocusable() || view2 != null) && (i12.hasFocusable() || view3 != null)) {
                    int min2 = Math.min(i14, i6) - Math.max(i13, i5);
                    if (i12.hasFocusable()) {
                        if (min2 > i8) {
                            z2 = true;
                        } else if (min2 == i8) {
                            if (z == (i13 > i7)) {
                                z2 = true;
                            }
                        }
                    } else if (view2 == null && a(i12, false, true)) {
                        if (min2 > i10) {
                            z2 = true;
                        } else if (min2 == i10) {
                            if (z == (i13 > i9)) {
                                z2 = true;
                            }
                        }
                    }
                } else {
                    z2 = true;
                }
                if (z2) {
                    if (i12.hasFocusable()) {
                        i7 = bVar2.f560a;
                        i8 = Math.min(i14, i6) - Math.max(i13, i5);
                        min = i10;
                        i4 = i9;
                        view2 = i12;
                    } else {
                        i4 = bVar2.f560a;
                        min = Math.min(i14, i6) - Math.max(i13, i5);
                        view3 = i12;
                    }
                    i11 += i3;
                    i10 = min;
                    i9 = i4;
                }
            }
            min = i10;
            i4 = i9;
            i11 += i3;
            i10 = min;
            i9 = i4;
        }
        if (view2 == null) {
            view2 = view3;
        }
        return view2;
    }

    @Override // android.support.v7.widget.RecyclerView.i, android.support.v7.widget.LinearLayoutManager
    public boolean b() {
        return this.n == null && !this.f559a;
    }

    public static final class a extends c {
        @Override // android.support.v7.widget.GridLayoutManager.c
        public int a(int i) {
            return 1;
        }

        @Override // android.support.v7.widget.GridLayoutManager.c
        public int a(int i, int i2) {
            return i % i2;
        }
    }

    public static class b extends RecyclerView.j {

        /* renamed from: a  reason: collision with root package name */
        int f560a = -1;
        int b = 0;

        public b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public b(int i, int i2) {
            super(i, i2);
        }

        public b(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public b(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public int a() {
            return this.f560a;
        }

        public int b() {
            return this.b;
        }
    }
}
