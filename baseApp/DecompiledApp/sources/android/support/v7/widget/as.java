package android.support.v7.widget;

import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.v7.view.menu.h;
import android.view.MenuItem;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public interface as {
    void a(@NonNull h hVar, @NonNull MenuItem menuItem);

    void b(@NonNull h hVar, @NonNull MenuItem menuItem);
}
