package android.support.v7.widget;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class bd extends RecyclerView.f {
    boolean h = true;

    public abstract boolean a(RecyclerView.w wVar);

    public abstract boolean a(RecyclerView.w wVar, int i, int i2, int i3, int i4);

    public abstract boolean a(RecyclerView.w wVar, RecyclerView.w wVar2, int i, int i2, int i3, int i4);

    public abstract boolean b(RecyclerView.w wVar);

    @Override // android.support.v7.widget.RecyclerView.f
    public boolean h(@NonNull RecyclerView.w wVar) {
        return !this.h || wVar.n();
    }

    @Override // android.support.v7.widget.RecyclerView.f
    public boolean a(@NonNull RecyclerView.w wVar, @NonNull RecyclerView.f.c cVar, @Nullable RecyclerView.f.c cVar2) {
        int i = cVar.f576a;
        int i2 = cVar.b;
        View view = wVar.f590a;
        int left = cVar2 == null ? view.getLeft() : cVar2.f576a;
        int top = cVar2 == null ? view.getTop() : cVar2.b;
        if (wVar.q() || (i == left && i2 == top)) {
            return a(wVar);
        }
        view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
        return a(wVar, i, i2, left, top);
    }

    @Override // android.support.v7.widget.RecyclerView.f
    public boolean b(@NonNull RecyclerView.w wVar, @Nullable RecyclerView.f.c cVar, @NonNull RecyclerView.f.c cVar2) {
        if (cVar == null || (cVar.f576a == cVar2.f576a && cVar.b == cVar2.b)) {
            return b(wVar);
        }
        return a(wVar, cVar.f576a, cVar.b, cVar2.f576a, cVar2.b);
    }

    @Override // android.support.v7.widget.RecyclerView.f
    public boolean c(@NonNull RecyclerView.w wVar, @NonNull RecyclerView.f.c cVar, @NonNull RecyclerView.f.c cVar2) {
        if (cVar.f576a != cVar2.f576a || cVar.b != cVar2.b) {
            return a(wVar, cVar.f576a, cVar.b, cVar2.f576a, cVar2.b);
        }
        j(wVar);
        return false;
    }

    @Override // android.support.v7.widget.RecyclerView.f
    public boolean a(@NonNull RecyclerView.w wVar, @NonNull RecyclerView.w wVar2, @NonNull RecyclerView.f.c cVar, @NonNull RecyclerView.f.c cVar2) {
        int i;
        int i2;
        int i3 = cVar.f576a;
        int i4 = cVar.b;
        if (wVar2.c()) {
            i = cVar.f576a;
            i2 = cVar.b;
        } else {
            i = cVar2.f576a;
            i2 = cVar2.b;
        }
        return a(wVar, wVar2, i3, i4, i, i2);
    }

    public final void i(RecyclerView.w wVar) {
        p(wVar);
        f(wVar);
    }

    public final void j(RecyclerView.w wVar) {
        t(wVar);
        f(wVar);
    }

    public final void k(RecyclerView.w wVar) {
        r(wVar);
        f(wVar);
    }

    public final void a(RecyclerView.w wVar, boolean z) {
        d(wVar, z);
        f(wVar);
    }

    public final void l(RecyclerView.w wVar) {
        o(wVar);
    }

    public final void m(RecyclerView.w wVar) {
        s(wVar);
    }

    public final void n(RecyclerView.w wVar) {
        q(wVar);
    }

    public final void b(RecyclerView.w wVar, boolean z) {
        c(wVar, z);
    }

    public void o(RecyclerView.w wVar) {
    }

    public void p(RecyclerView.w wVar) {
    }

    public void q(RecyclerView.w wVar) {
    }

    public void r(RecyclerView.w wVar) {
    }

    public void s(RecyclerView.w wVar) {
    }

    public void t(RecyclerView.w wVar) {
    }

    public void c(RecyclerView.w wVar, boolean z) {
    }

    public void d(RecyclerView.w wVar, boolean z) {
    }
}
