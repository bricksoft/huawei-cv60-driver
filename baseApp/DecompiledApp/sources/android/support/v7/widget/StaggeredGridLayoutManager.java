package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class StaggeredGridLayoutManager extends RecyclerView.i implements RecyclerView.s.b {
    private SavedState A;
    private int B;
    private final Rect C = new Rect();
    private final a D = new a();
    private boolean E = false;
    private boolean F = true;
    private int[] G;
    private final Runnable H = new Runnable() {
        /* class android.support.v7.widget.StaggeredGridLayoutManager.AnonymousClass1 */

        public void run() {
            StaggeredGridLayoutManager.this.g();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    c[] f607a;
    @NonNull
    av b;
    @NonNull
    av c;
    boolean d = false;
    boolean e = false;
    int f = -1;
    int g = Integer.MIN_VALUE;
    LazySpanLookup h = new LazySpanLookup();
    private int i = -1;
    private int j;
    private int k;
    @NonNull
    private final ap l;
    private BitSet m;
    private int n = 2;
    private boolean o;
    private boolean z;

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        RecyclerView.i.b a2 = a(context, attributeSet, i2, i3);
        b(a2.f581a);
        a(a2.b);
        a(a2.c);
        this.l = new ap();
        M();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean c() {
        return this.n != 0;
    }

    private void M() {
        this.b = av.a(this, this.j);
        this.c = av.a(this, 1 - this.j);
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        int L;
        int K;
        if (v() == 0 || this.n == 0 || !q()) {
            return false;
        }
        if (this.e) {
            L = K();
            K = L();
        } else {
            L = L();
            K = K();
        }
        if (L == 0 && h() != null) {
            this.h.a();
            I();
            o();
            return true;
        } else if (!this.E) {
            return false;
        } else {
            int i2 = this.e ? -1 : 1;
            LazySpanLookup.FullSpanItem a2 = this.h.a(L, K + 1, i2, true);
            if (a2 == null) {
                this.E = false;
                this.h.a(K + 1);
                return false;
            }
            LazySpanLookup.FullSpanItem a3 = this.h.a(L, a2.f610a, i2 * -1, true);
            if (a3 == null) {
                this.h.a(a2.f610a);
            } else {
                this.h.a(a3.f610a + 1);
            }
            I();
            o();
            return true;
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void l(int i2) {
        if (i2 == 0) {
            g();
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView, RecyclerView.p pVar) {
        super.a(recyclerView, pVar);
        a(this.H);
        for (int i2 = 0; i2 < this.i; i2++) {
            this.f607a[i2].e();
        }
        recyclerView.requestLayout();
    }

    /* access modifiers changed from: package-private */
    public View h() {
        int i2;
        boolean z2;
        int v = v() - 1;
        BitSet bitSet = new BitSet(this.i);
        bitSet.set(0, this.i, true);
        char c2 = (this.j != 1 || !j()) ? (char) 65535 : 1;
        if (this.e) {
            i2 = -1;
        } else {
            i2 = v + 1;
            v = 0;
        }
        int i3 = v < i2 ? 1 : -1;
        for (int i4 = v; i4 != i2; i4 += i3) {
            View i5 = i(i4);
            b bVar = (b) i5.getLayoutParams();
            if (bitSet.get(bVar.f613a.e)) {
                if (a(bVar.f613a)) {
                    return i5;
                }
                bitSet.clear(bVar.f613a.e);
            }
            if (!bVar.b && i4 + i3 != i2) {
                View i6 = i(i4 + i3);
                if (this.e) {
                    int b2 = this.b.b(i5);
                    int b3 = this.b.b(i6);
                    if (b2 < b3) {
                        return i5;
                    }
                    if (b2 == b3) {
                        z2 = true;
                    }
                    z2 = false;
                } else {
                    int a2 = this.b.a(i5);
                    int a3 = this.b.a(i6);
                    if (a2 > a3) {
                        return i5;
                    }
                    if (a2 == a3) {
                        z2 = true;
                    }
                    z2 = false;
                }
                if (!z2) {
                    continue;
                } else {
                    if ((bVar.f613a.e - ((b) i6.getLayoutParams()).f613a.e < 0) != (c2 < 0)) {
                        return i5;
                    }
                }
            }
        }
        return null;
    }

    private boolean a(c cVar) {
        boolean z2 = true;
        if (this.e) {
            if (cVar.d() < this.b.d()) {
                return !cVar.c(cVar.f614a.get(cVar.f614a.size() + -1)).b;
            }
        } else if (cVar.b() > this.b.c()) {
            if (cVar.c(cVar.f614a.get(0)).b) {
                z2 = false;
            }
            return z2;
        }
        return false;
    }

    public void a(int i2) {
        a((String) null);
        if (i2 != this.i) {
            i();
            this.i = i2;
            this.m = new BitSet(this.i);
            this.f607a = new c[this.i];
            for (int i3 = 0; i3 < this.i; i3++) {
                this.f607a[i3] = new c(i3);
            }
            o();
        }
    }

    public void b(int i2) {
        if (i2 == 0 || i2 == 1) {
            a((String) null);
            if (i2 != this.j) {
                this.j = i2;
                av avVar = this.b;
                this.b = this.c;
                this.c = avVar;
                o();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation.");
    }

    public void a(boolean z2) {
        a((String) null);
        if (!(this.A == null || this.A.h == z2)) {
            this.A.h = z2;
        }
        this.d = z2;
        o();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(String str) {
        if (this.A == null) {
            super.a(str);
        }
    }

    public void i() {
        this.h.a();
        o();
    }

    private void N() {
        boolean z2 = true;
        if (this.j == 1 || !j()) {
            this.e = this.d;
            return;
        }
        if (this.d) {
            z2 = false;
        }
        this.e = z2;
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        return t() == 1;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(Rect rect, int i2, int i3) {
        int a2;
        int a3;
        int C2 = C() + A();
        int B2 = B() + D();
        if (this.j == 1) {
            a3 = a(i3, B2 + rect.height(), G());
            a2 = a(i2, C2 + (this.k * this.i), F());
        } else {
            a2 = a(i2, C2 + rect.width(), F());
            a3 = a(i3, B2 + (this.k * this.i), G());
        }
        f(a2, a3);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void c(RecyclerView.p pVar, RecyclerView.t tVar) {
        a(pVar, tVar, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:79:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:97:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.support.v7.widget.RecyclerView.p r9, android.support.v7.widget.RecyclerView.t r10, boolean r11) {
        /*
        // Method dump skipped, instructions count: 393
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$p, android.support.v7.widget.RecyclerView$t, boolean):void");
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView.t tVar) {
        super.a(tVar);
        this.f = -1;
        this.g = Integer.MIN_VALUE;
        this.A = null;
        this.D.a();
    }

    private void O() {
        float f2;
        float max;
        if (this.c.h() != 1073741824) {
            float f3 = 0.0f;
            int v = v();
            int i2 = 0;
            while (i2 < v) {
                View i3 = i(i2);
                float e2 = (float) this.c.e(i3);
                if (e2 < f3) {
                    max = f3;
                } else {
                    if (((b) i3.getLayoutParams()).a()) {
                        f2 = (1.0f * e2) / ((float) this.i);
                    } else {
                        f2 = e2;
                    }
                    max = Math.max(f3, f2);
                }
                i2++;
                f3 = max;
            }
            int i4 = this.k;
            int round = Math.round(((float) this.i) * f3);
            if (this.c.h() == Integer.MIN_VALUE) {
                round = Math.min(round, this.c.f());
            }
            f(round);
            if (this.k != i4) {
                for (int i5 = 0; i5 < v; i5++) {
                    View i6 = i(i5);
                    b bVar = (b) i6.getLayoutParams();
                    if (!bVar.b) {
                        if (!j() || this.j != 1) {
                            int i7 = bVar.f613a.e * this.k;
                            int i8 = bVar.f613a.e * i4;
                            if (this.j == 1) {
                                i6.offsetLeftAndRight(i7 - i8);
                            } else {
                                i6.offsetTopAndBottom(i7 - i8);
                            }
                        } else {
                            i6.offsetLeftAndRight(((-((this.i - 1) - bVar.f613a.e)) * this.k) - ((-((this.i - 1) - bVar.f613a.e)) * i4));
                        }
                    }
                }
            }
        }
    }

    private void a(a aVar) {
        if (this.A.c > 0) {
            if (this.A.c == this.i) {
                for (int i2 = 0; i2 < this.i; i2++) {
                    this.f607a[i2].e();
                    int i3 = this.A.d[i2];
                    if (i3 != Integer.MIN_VALUE) {
                        if (this.A.i) {
                            i3 += this.b.d();
                        } else {
                            i3 += this.b.c();
                        }
                    }
                    this.f607a[i2].c(i3);
                }
            } else {
                this.A.a();
                this.A.f611a = this.A.b;
            }
        }
        this.z = this.A.j;
        a(this.A.h);
        N();
        if (this.A.f611a != -1) {
            this.f = this.A.f611a;
            aVar.c = this.A.i;
        } else {
            aVar.c = this.e;
        }
        if (this.A.e > 1) {
            this.h.f609a = this.A.f;
            this.h.b = this.A.g;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.t tVar, a aVar) {
        if (!b(tVar, aVar) && !c(tVar, aVar)) {
            aVar.b();
            aVar.f612a = 0;
        }
    }

    private boolean c(RecyclerView.t tVar, a aVar) {
        int v;
        if (this.o) {
            v = w(tVar.e());
        } else {
            v = v(tVar.e());
        }
        aVar.f612a = v;
        aVar.b = Integer.MIN_VALUE;
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b(RecyclerView.t tVar, a aVar) {
        int L;
        int c2;
        boolean z2 = false;
        if (tVar.a() || this.f == -1) {
            return false;
        }
        if (this.f < 0 || this.f >= tVar.e()) {
            this.f = -1;
            this.g = Integer.MIN_VALUE;
            return false;
        } else if (this.A == null || this.A.f611a == -1 || this.A.c < 1) {
            View c3 = c(this.f);
            if (c3 != null) {
                if (this.e) {
                    L = K();
                } else {
                    L = L();
                }
                aVar.f612a = L;
                if (this.g != Integer.MIN_VALUE) {
                    if (aVar.c) {
                        aVar.b = (this.b.d() - this.g) - this.b.b(c3);
                        return true;
                    }
                    aVar.b = (this.b.c() + this.g) - this.b.a(c3);
                    return true;
                } else if (this.b.e(c3) > this.b.f()) {
                    if (aVar.c) {
                        c2 = this.b.d();
                    } else {
                        c2 = this.b.c();
                    }
                    aVar.b = c2;
                    return true;
                } else {
                    int a2 = this.b.a(c3) - this.b.c();
                    if (a2 < 0) {
                        aVar.b = -a2;
                        return true;
                    }
                    int d2 = this.b.d() - this.b.b(c3);
                    if (d2 < 0) {
                        aVar.b = d2;
                        return true;
                    }
                    aVar.b = Integer.MIN_VALUE;
                    return true;
                }
            } else {
                aVar.f612a = this.f;
                if (this.g == Integer.MIN_VALUE) {
                    if (u(aVar.f612a) == 1) {
                        z2 = true;
                    }
                    aVar.c = z2;
                    aVar.b();
                } else {
                    aVar.a(this.g);
                }
                aVar.d = true;
                return true;
            }
        } else {
            aVar.b = Integer.MIN_VALUE;
            aVar.f612a = this.f;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void f(int i2) {
        this.k = i2 / this.i;
        this.B = View.MeasureSpec.makeMeasureSpec(i2, this.c.h());
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean b() {
        return this.A == null;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int c(RecyclerView.t tVar) {
        return b(tVar);
    }

    private int b(RecyclerView.t tVar) {
        boolean z2 = true;
        if (v() == 0) {
            return 0;
        }
        av avVar = this.b;
        View b2 = b(!this.F);
        if (this.F) {
            z2 = false;
        }
        return bb.a(tVar, avVar, b2, c(z2), this, this.F, this.e);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int d(RecyclerView.t tVar) {
        return b(tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int e(RecyclerView.t tVar) {
        return i(tVar);
    }

    private int i(RecyclerView.t tVar) {
        boolean z2 = true;
        if (v() == 0) {
            return 0;
        }
        av avVar = this.b;
        View b2 = b(!this.F);
        if (this.F) {
            z2 = false;
        }
        return bb.a(tVar, avVar, b2, c(z2), this, this.F);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int f(RecyclerView.t tVar) {
        return i(tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int g(RecyclerView.t tVar) {
        return j(tVar);
    }

    private int j(RecyclerView.t tVar) {
        boolean z2 = true;
        if (v() == 0) {
            return 0;
        }
        av avVar = this.b;
        View b2 = b(!this.F);
        if (this.F) {
            z2 = false;
        }
        return bb.b(tVar, avVar, b2, c(z2), this, this.F);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int h(RecyclerView.t tVar) {
        return j(tVar);
    }

    private void a(View view, b bVar, boolean z2) {
        if (bVar.b) {
            if (this.j == 1) {
                a(view, this.B, a(z(), x(), B() + D(), bVar.height, true), z2);
            } else {
                a(view, a(y(), w(), A() + C(), bVar.width, true), this.B, z2);
            }
        } else if (this.j == 1) {
            a(view, a(this.k, w(), 0, bVar.width, false), a(z(), x(), B() + D(), bVar.height, true), z2);
        } else {
            a(view, a(y(), w(), A() + C(), bVar.width, true), a(this.k, x(), 0, bVar.height, false), z2);
        }
    }

    private void a(View view, int i2, int i3, boolean z2) {
        boolean b2;
        b(view, this.C);
        b bVar = (b) view.getLayoutParams();
        int b3 = b(i2, bVar.leftMargin + this.C.left, bVar.rightMargin + this.C.right);
        int b4 = b(i3, bVar.topMargin + this.C.top, bVar.bottomMargin + this.C.bottom);
        if (z2) {
            b2 = a(view, b3, b4, bVar);
        } else {
            b2 = b(view, b3, b4, bVar);
        }
        if (b2) {
            view.measure(b3, b4);
        }
    }

    private int b(int i2, int i3, int i4) {
        if (i3 == 0 && i4 == 0) {
            return i2;
        }
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            return View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i2) - i3) - i4), mode);
        }
        return i2;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.A = (SavedState) parcelable;
            o();
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public Parcelable d() {
        int L;
        int a2;
        if (this.A != null) {
            return new SavedState(this.A);
        }
        SavedState savedState = new SavedState();
        savedState.h = this.d;
        savedState.i = this.o;
        savedState.j = this.z;
        if (this.h == null || this.h.f609a == null) {
            savedState.e = 0;
        } else {
            savedState.f = this.h.f609a;
            savedState.e = savedState.f.length;
            savedState.g = this.h.b;
        }
        if (v() > 0) {
            if (this.o) {
                L = K();
            } else {
                L = L();
            }
            savedState.f611a = L;
            savedState.b = k();
            savedState.c = this.i;
            savedState.d = new int[this.i];
            for (int i2 = 0; i2 < this.i; i2++) {
                if (this.o) {
                    a2 = this.f607a[i2].b(Integer.MIN_VALUE);
                    if (a2 != Integer.MIN_VALUE) {
                        a2 -= this.b.d();
                    }
                } else {
                    a2 = this.f607a[i2].a(Integer.MIN_VALUE);
                    if (a2 != Integer.MIN_VALUE) {
                        a2 -= this.b.c();
                    }
                }
                savedState.d[i2] = a2;
            }
        } else {
            savedState.f611a = -1;
            savedState.b = -1;
            savedState.c = 0;
        }
        return savedState;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView.p pVar, RecyclerView.t tVar, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof b)) {
            super.a(view, accessibilityNodeInfoCompat);
            return;
        }
        b bVar = (b) layoutParams;
        if (this.j == 0) {
            accessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(bVar.b(), bVar.b ? this.i : 1, -1, -1, bVar.b, false));
        } else {
            accessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(-1, -1, bVar.b(), bVar.b ? this.i : 1, bVar.b, false));
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(AccessibilityEvent accessibilityEvent) {
        super.a(accessibilityEvent);
        if (v() > 0) {
            View b2 = b(false);
            View c2 = c(false);
            if (b2 != null && c2 != null) {
                int d2 = d(b2);
                int d3 = d(c2);
                if (d2 < d3) {
                    accessibilityEvent.setFromIndex(d2);
                    accessibilityEvent.setToIndex(d3);
                    return;
                }
                accessibilityEvent.setFromIndex(d3);
                accessibilityEvent.setToIndex(d2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int k() {
        View b2;
        if (this.e) {
            b2 = c(true);
        } else {
            b2 = b(true);
        }
        if (b2 == null) {
            return -1;
        }
        return d(b2);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int a(RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.j == 0) {
            return this.i;
        }
        return super.a(pVar, tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int b(RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.j == 1) {
            return this.i;
        }
        return super.b(pVar, tVar);
    }

    /* access modifiers changed from: package-private */
    public View b(boolean z2) {
        int c2 = this.b.c();
        int d2 = this.b.d();
        int v = v();
        View view = null;
        for (int i2 = 0; i2 < v; i2++) {
            View i3 = i(i2);
            int a2 = this.b.a(i3);
            if (this.b.b(i3) > c2 && a2 < d2) {
                if (a2 >= c2 || !z2) {
                    return i3;
                }
                if (view == null) {
                    view = i3;
                }
            }
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    public View c(boolean z2) {
        int c2 = this.b.c();
        int d2 = this.b.d();
        View view = null;
        for (int v = v() - 1; v >= 0; v--) {
            View i2 = i(v);
            int a2 = this.b.a(i2);
            int b2 = this.b.b(i2);
            if (b2 > c2 && a2 < d2) {
                if (b2 <= d2 || !z2) {
                    return i2;
                }
                if (view == null) {
                    view = i2;
                }
            }
        }
        return view;
    }

    private void b(RecyclerView.p pVar, RecyclerView.t tVar, boolean z2) {
        int d2;
        int r = r(Integer.MIN_VALUE);
        if (r != Integer.MIN_VALUE && (d2 = this.b.d() - r) > 0) {
            int i2 = d2 - (-c(-d2, pVar, tVar));
            if (z2 && i2 > 0) {
                this.b.a(i2);
            }
        }
    }

    private void c(RecyclerView.p pVar, RecyclerView.t tVar, boolean z2) {
        int c2;
        int q = q(Integer.MAX_VALUE);
        if (q != Integer.MAX_VALUE && (c2 = q - this.b.c()) > 0) {
            int c3 = c2 - c(c2, pVar, tVar);
            if (z2 && c3 > 0) {
                this.b.a(-c3);
            }
        }
    }

    private void b(int i2, RecyclerView.t tVar) {
        int i3;
        int i4;
        int c2;
        boolean z2 = false;
        this.l.b = 0;
        this.l.c = i2;
        if (!s() || (c2 = tVar.c()) == -1) {
            i3 = 0;
            i4 = 0;
        } else {
            if (this.e == (c2 < i2)) {
                i3 = this.b.f();
                i4 = 0;
            } else {
                i4 = this.b.f();
                i3 = 0;
            }
        }
        if (r()) {
            this.l.f = this.b.c() - i4;
            this.l.g = i3 + this.b.d();
        } else {
            this.l.g = i3 + this.b.e();
            this.l.f = -i4;
        }
        this.l.h = false;
        this.l.f655a = true;
        ap apVar = this.l;
        if (this.b.h() == 0 && this.b.e() == 0) {
            z2 = true;
        }
        apVar.i = z2;
    }

    private void m(int i2) {
        int i3 = 1;
        this.l.e = i2;
        ap apVar = this.l;
        if (this.e != (i2 == -1)) {
            i3 = -1;
        }
        apVar.d = i3;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void j(int i2) {
        super.j(i2);
        for (int i3 = 0; i3 < this.i; i3++) {
            this.f607a[i3].d(i2);
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void k(int i2) {
        super.k(i2);
        for (int i3 = 0; i3 < this.i; i3++) {
            this.f607a[i3].d(i2);
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void b(RecyclerView recyclerView, int i2, int i3) {
        c(i2, i3, 2);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView, int i2, int i3) {
        c(i2, i3, 1);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView) {
        this.h.a();
        o();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView, int i2, int i3, int i4) {
        c(i2, i3, 8);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView, int i2, int i3, Object obj) {
        c(i2, i3, 4);
    }

    private void c(int i2, int i3, int i4) {
        int i5;
        int i6;
        int K = this.e ? K() : L();
        if (i4 != 8) {
            i5 = i2 + i3;
            i6 = i2;
        } else if (i2 < i3) {
            i5 = i3 + 1;
            i6 = i2;
        } else {
            i5 = i2 + 1;
            i6 = i3;
        }
        this.h.b(i6);
        switch (i4) {
            case 1:
                this.h.b(i2, i3);
                break;
            case 2:
                this.h.a(i2, i3);
                break;
            case 8:
                this.h.a(i2, 1);
                this.h.b(i3, 1);
                break;
        }
        if (i5 > K) {
            if (i6 <= (this.e ? L() : K())) {
                o();
            }
        }
    }

    private int a(RecyclerView.p pVar, ap apVar, RecyclerView.t tVar) {
        int i2;
        int c2;
        int r;
        c cVar;
        int a2;
        int e2;
        int i3;
        int c3;
        int e3;
        int i4;
        int d2;
        boolean z2;
        int b2;
        this.m.set(0, this.i, true);
        if (this.l.i) {
            if (apVar.e == 1) {
                i2 = Integer.MAX_VALUE;
            } else {
                i2 = Integer.MIN_VALUE;
            }
        } else if (apVar.e == 1) {
            i2 = apVar.g + apVar.b;
        } else {
            i2 = apVar.f - apVar.b;
        }
        a(apVar.e, i2);
        if (this.e) {
            c2 = this.b.d();
        } else {
            c2 = this.b.c();
        }
        boolean z3 = false;
        while (apVar.a(tVar) && (this.l.i || !this.m.isEmpty())) {
            View a3 = apVar.a(pVar);
            b bVar = (b) a3.getLayoutParams();
            int f2 = bVar.f();
            int c4 = this.h.c(f2);
            boolean z4 = c4 == -1;
            if (z4) {
                c a4 = bVar.b ? this.f607a[0] : a(apVar);
                this.h.a(f2, a4);
                cVar = a4;
            } else {
                cVar = this.f607a[c4];
            }
            bVar.f613a = cVar;
            if (apVar.e == 1) {
                b(a3);
            } else {
                b(a3, 0);
            }
            a(a3, bVar, false);
            if (apVar.e == 1) {
                if (bVar.b) {
                    b2 = r(c2);
                } else {
                    b2 = cVar.b(c2);
                }
                i3 = b2 + this.b.e(a3);
                if (!z4 || !bVar.b) {
                    e2 = b2;
                } else {
                    LazySpanLookup.FullSpanItem n2 = n(b2);
                    n2.b = -1;
                    n2.f610a = f2;
                    this.h.a(n2);
                    e2 = b2;
                }
            } else {
                if (bVar.b) {
                    a2 = q(c2);
                } else {
                    a2 = cVar.a(c2);
                }
                e2 = a2 - this.b.e(a3);
                if (z4 && bVar.b) {
                    LazySpanLookup.FullSpanItem o2 = o(a2);
                    o2.b = 1;
                    o2.f610a = f2;
                    this.h.a(o2);
                }
                i3 = a2;
            }
            if (bVar.b && apVar.d == -1) {
                if (z4) {
                    this.E = true;
                } else {
                    if (apVar.e == 1) {
                        z2 = !m();
                    } else {
                        z2 = !n();
                    }
                    if (z2) {
                        LazySpanLookup.FullSpanItem f3 = this.h.f(f2);
                        if (f3 != null) {
                            f3.d = true;
                        }
                        this.E = true;
                    }
                }
            }
            a(a3, bVar, apVar);
            if (!j() || this.j != 1) {
                if (bVar.b) {
                    c3 = this.c.c();
                } else {
                    c3 = (cVar.e * this.k) + this.c.c();
                }
                e3 = c3 + this.c.e(a3);
                i4 = c3;
            } else {
                if (bVar.b) {
                    d2 = this.c.d();
                } else {
                    d2 = this.c.d() - (((this.i - 1) - cVar.e) * this.k);
                }
                i4 = d2 - this.c.e(a3);
                e3 = d2;
            }
            if (this.j == 1) {
                a(a3, i4, e2, e3, i3);
            } else {
                a(a3, e2, i4, i3, e3);
            }
            if (bVar.b) {
                a(this.l.e, i2);
            } else {
                a(cVar, this.l.e, i2);
            }
            a(pVar, this.l);
            if (this.l.h && a3.hasFocusable()) {
                if (bVar.b) {
                    this.m.clear();
                } else {
                    this.m.set(cVar.e, false);
                }
            }
            z3 = true;
        }
        if (!z3) {
            a(pVar, this.l);
        }
        if (this.l.e == -1) {
            r = this.b.c() - q(this.b.c());
        } else {
            r = r(this.b.d()) - this.b.d();
        }
        if (r > 0) {
            return Math.min(apVar.b, r);
        }
        return 0;
    }

    private LazySpanLookup.FullSpanItem n(int i2) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.c = new int[this.i];
        for (int i3 = 0; i3 < this.i; i3++) {
            fullSpanItem.c[i3] = i2 - this.f607a[i3].b(i2);
        }
        return fullSpanItem;
    }

    private LazySpanLookup.FullSpanItem o(int i2) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.c = new int[this.i];
        for (int i3 = 0; i3 < this.i; i3++) {
            fullSpanItem.c[i3] = this.f607a[i3].a(i2) - i2;
        }
        return fullSpanItem;
    }

    private void a(View view, b bVar, ap apVar) {
        if (apVar.e == 1) {
            if (bVar.b) {
                p(view);
            } else {
                bVar.f613a.b(view);
            }
        } else if (bVar.b) {
            q(view);
        } else {
            bVar.f613a.a(view);
        }
    }

    private void a(RecyclerView.p pVar, ap apVar) {
        int min;
        int min2;
        if (apVar.f655a && !apVar.i) {
            if (apVar.b == 0) {
                if (apVar.e == -1) {
                    b(pVar, apVar.g);
                } else {
                    a(pVar, apVar.f);
                }
            } else if (apVar.e == -1) {
                int p = apVar.f - p(apVar.f);
                if (p < 0) {
                    min2 = apVar.g;
                } else {
                    min2 = apVar.g - Math.min(p, apVar.b);
                }
                b(pVar, min2);
            } else {
                int s = s(apVar.g) - apVar.g;
                if (s < 0) {
                    min = apVar.f;
                } else {
                    min = Math.min(s, apVar.b) + apVar.f;
                }
                a(pVar, min);
            }
        }
    }

    private void p(View view) {
        for (int i2 = this.i - 1; i2 >= 0; i2--) {
            this.f607a[i2].b(view);
        }
    }

    private void q(View view) {
        for (int i2 = this.i - 1; i2 >= 0; i2--) {
            this.f607a[i2].a(view);
        }
    }

    private void a(int i2, int i3) {
        for (int i4 = 0; i4 < this.i; i4++) {
            if (!this.f607a[i4].f614a.isEmpty()) {
                a(this.f607a[i4], i2, i3);
            }
        }
    }

    private void a(c cVar, int i2, int i3) {
        int i4 = cVar.i();
        if (i2 == -1) {
            if (i4 + cVar.b() <= i3) {
                this.m.set(cVar.e, false);
            }
        } else if (cVar.d() - i4 >= i3) {
            this.m.set(cVar.e, false);
        }
    }

    private int p(int i2) {
        int a2 = this.f607a[0].a(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int a3 = this.f607a[i3].a(i2);
            if (a3 > a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    private int q(int i2) {
        int a2 = this.f607a[0].a(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int a3 = this.f607a[i3].a(i2);
            if (a3 < a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public boolean m() {
        int b2 = this.f607a[0].b(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.i; i2++) {
            if (this.f607a[i2].b(Integer.MIN_VALUE) != b2) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean n() {
        int a2 = this.f607a[0].a(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.i; i2++) {
            if (this.f607a[i2].a(Integer.MIN_VALUE) != a2) {
                return false;
            }
        }
        return true;
    }

    private int r(int i2) {
        int b2 = this.f607a[0].b(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int b3 = this.f607a[i3].b(i2);
            if (b3 > b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private int s(int i2) {
        int b2 = this.f607a[0].b(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int b3 = this.f607a[i3].b(i2);
            if (b3 < b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private void a(RecyclerView.p pVar, int i2) {
        while (v() > 0) {
            View i3 = i(0);
            if (this.b.b(i3) <= i2 && this.b.c(i3) <= i2) {
                b bVar = (b) i3.getLayoutParams();
                if (bVar.b) {
                    for (int i4 = 0; i4 < this.i; i4++) {
                        if (this.f607a[i4].f614a.size() == 1) {
                            return;
                        }
                    }
                    for (int i5 = 0; i5 < this.i; i5++) {
                        this.f607a[i5].h();
                    }
                } else if (bVar.f613a.f614a.size() != 1) {
                    bVar.f613a.h();
                } else {
                    return;
                }
                a(i3, pVar);
            } else {
                return;
            }
        }
    }

    private void b(RecyclerView.p pVar, int i2) {
        for (int v = v() - 1; v >= 0; v--) {
            View i3 = i(v);
            if (this.b.a(i3) >= i2 && this.b.d(i3) >= i2) {
                b bVar = (b) i3.getLayoutParams();
                if (bVar.b) {
                    for (int i4 = 0; i4 < this.i; i4++) {
                        if (this.f607a[i4].f614a.size() == 1) {
                            return;
                        }
                    }
                    for (int i5 = 0; i5 < this.i; i5++) {
                        this.f607a[i5].g();
                    }
                } else if (bVar.f613a.f614a.size() != 1) {
                    bVar.f613a.g();
                } else {
                    return;
                }
                a(i3, pVar);
            } else {
                return;
            }
        }
    }

    private boolean t(int i2) {
        boolean z2;
        if (this.j == 0) {
            if (i2 == -1) {
                z2 = true;
            } else {
                z2 = false;
            }
            return z2 != this.e;
        }
        return ((i2 == -1) == this.e) == j();
    }

    private c a(ap apVar) {
        int i2;
        int i3;
        int i4;
        c cVar = null;
        if (t(apVar.e)) {
            i2 = this.i - 1;
            i3 = -1;
            i4 = -1;
        } else {
            i2 = 0;
            i3 = 1;
            i4 = this.i;
        }
        if (apVar.e == 1) {
            int i5 = Integer.MAX_VALUE;
            int c2 = this.b.c();
            int i6 = i2;
            while (i6 != i4) {
                c cVar2 = this.f607a[i6];
                int b2 = cVar2.b(c2);
                if (b2 >= i5) {
                    b2 = i5;
                    cVar2 = cVar;
                }
                i6 += i3;
                i5 = b2;
                cVar = cVar2;
            }
        } else {
            int i7 = Integer.MIN_VALUE;
            int d2 = this.b.d();
            int i8 = i2;
            while (i8 != i4) {
                c cVar3 = this.f607a[i8];
                int a2 = cVar3.a(d2);
                if (a2 <= i7) {
                    a2 = i7;
                    cVar3 = cVar;
                }
                i8 += i3;
                i7 = a2;
                cVar = cVar3;
            }
        }
        return cVar;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean f() {
        return this.j == 1;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean e() {
        return this.j == 0;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int a(int i2, RecyclerView.p pVar, RecyclerView.t tVar) {
        return c(i2, pVar, tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int b(int i2, RecyclerView.p pVar, RecyclerView.t tVar) {
        return c(i2, pVar, tVar);
    }

    private int u(int i2) {
        int i3 = -1;
        if (v() == 0) {
            return this.e ? 1 : -1;
        }
        if ((i2 < L()) == this.e) {
            i3 = 1;
        }
        return i3;
    }

    @Override // android.support.v7.widget.RecyclerView.s.b
    public PointF d(int i2) {
        int u = u(i2);
        PointF pointF = new PointF();
        if (u == 0) {
            return null;
        }
        if (this.j == 0) {
            pointF.x = (float) u;
            pointF.y = 0.0f;
            return pointF;
        }
        pointF.x = 0.0f;
        pointF.y = (float) u;
        return pointF;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void e(int i2) {
        if (!(this.A == null || this.A.f611a == i2)) {
            this.A.b();
        }
        this.f = i2;
        this.g = Integer.MIN_VALUE;
        o();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public void a(int i2, int i3, RecyclerView.t tVar, RecyclerView.i.a aVar) {
        int b2;
        if (this.j != 0) {
            i2 = i3;
        }
        if (!(v() == 0 || i2 == 0)) {
            a(i2, tVar);
            if (this.G == null || this.G.length < this.i) {
                this.G = new int[this.i];
            }
            int i4 = 0;
            for (int i5 = 0; i5 < this.i; i5++) {
                if (this.l.d == -1) {
                    b2 = this.l.f - this.f607a[i5].a(this.l.f);
                } else {
                    b2 = this.f607a[i5].b(this.l.g) - this.l.g;
                }
                if (b2 >= 0) {
                    this.G[i4] = b2;
                    i4++;
                }
            }
            Arrays.sort(this.G, 0, i4);
            for (int i6 = 0; i6 < i4 && this.l.a(tVar); i6++) {
                aVar.b(this.l.c, this.G[i6]);
                this.l.c += this.l.d;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, RecyclerView.t tVar) {
        int i3;
        int L;
        if (i2 > 0) {
            L = K();
            i3 = 1;
        } else {
            i3 = -1;
            L = L();
        }
        this.l.f655a = true;
        b(L, tVar);
        m(i3);
        this.l.c = this.l.d + L;
        this.l.b = Math.abs(i2);
    }

    /* access modifiers changed from: package-private */
    public int c(int i2, RecyclerView.p pVar, RecyclerView.t tVar) {
        if (v() == 0 || i2 == 0) {
            return 0;
        }
        a(i2, tVar);
        int a2 = a(pVar, this.l, tVar);
        if (this.l.b >= a2) {
            i2 = i2 < 0 ? -a2 : a2;
        }
        this.b.a(-i2);
        this.o = this.e;
        this.l.b = 0;
        a(pVar, this.l);
        return i2;
    }

    /* access modifiers changed from: package-private */
    public int K() {
        int v = v();
        if (v == 0) {
            return 0;
        }
        return d(i(v - 1));
    }

    /* access modifiers changed from: package-private */
    public int L() {
        if (v() == 0) {
            return 0;
        }
        return d(i(0));
    }

    private int v(int i2) {
        int v = v();
        for (int i3 = 0; i3 < v; i3++) {
            int d2 = d(i(i3));
            if (d2 >= 0 && d2 < i2) {
                return d2;
            }
        }
        return 0;
    }

    private int w(int i2) {
        for (int v = v() - 1; v >= 0; v--) {
            int d2 = d(i(v));
            if (d2 >= 0 && d2 < i2) {
                return d2;
            }
        }
        return 0;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public RecyclerView.j a() {
        if (this.j == 0) {
            return new b(-2, -1);
        }
        return new b(-1, -2);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public RecyclerView.j a(Context context, AttributeSet attributeSet) {
        return new b(context, attributeSet);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public RecyclerView.j a(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new b((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new b(layoutParams);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean a(RecyclerView.j jVar) {
        return jVar instanceof b;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    @Nullable
    public View a(View view, int i2, RecyclerView.p pVar, RecyclerView.t tVar) {
        int L;
        int k2;
        int k3;
        int k4;
        View a2;
        if (v() == 0) {
            return null;
        }
        View e2 = e(view);
        if (e2 == null) {
            return null;
        }
        N();
        int x = x(i2);
        if (x == Integer.MIN_VALUE) {
            return null;
        }
        b bVar = (b) e2.getLayoutParams();
        boolean z2 = bVar.b;
        c cVar = bVar.f613a;
        if (x == 1) {
            L = K();
        } else {
            L = L();
        }
        b(L, tVar);
        m(x);
        this.l.c = this.l.d + L;
        this.l.b = (int) (0.33333334f * ((float) this.b.f()));
        this.l.h = true;
        this.l.f655a = false;
        a(pVar, this.l, tVar);
        this.o = this.e;
        if (!(z2 || (a2 = cVar.a(L, x)) == null || a2 == e2)) {
            return a2;
        }
        if (t(x)) {
            for (int i3 = this.i - 1; i3 >= 0; i3--) {
                View a3 = this.f607a[i3].a(L, x);
                if (!(a3 == null || a3 == e2)) {
                    return a3;
                }
            }
        } else {
            for (int i4 = 0; i4 < this.i; i4++) {
                View a4 = this.f607a[i4].a(L, x);
                if (!(a4 == null || a4 == e2)) {
                    return a4;
                }
            }
        }
        boolean z3 = (!this.d) == (x == -1);
        if (!z2) {
            if (z3) {
                k4 = cVar.j();
            } else {
                k4 = cVar.k();
            }
            View c2 = c(k4);
            if (!(c2 == null || c2 == e2)) {
                return c2;
            }
        }
        if (t(x)) {
            for (int i5 = this.i - 1; i5 >= 0; i5--) {
                if (i5 != cVar.e) {
                    if (z3) {
                        k3 = this.f607a[i5].j();
                    } else {
                        k3 = this.f607a[i5].k();
                    }
                    View c3 = c(k3);
                    if (!(c3 == null || c3 == e2)) {
                        return c3;
                    }
                }
            }
        } else {
            for (int i6 = 0; i6 < this.i; i6++) {
                if (z3) {
                    k2 = this.f607a[i6].j();
                } else {
                    k2 = this.f607a[i6].k();
                }
                View c4 = c(k2);
                if (!(c4 == null || c4 == e2)) {
                    return c4;
                }
            }
        }
        return null;
    }

    private int x(int i2) {
        int i3 = Integer.MIN_VALUE;
        int i4 = 1;
        switch (i2) {
            case 1:
                return (this.j == 1 || !j()) ? -1 : 1;
            case 2:
                if (this.j == 1) {
                    return 1;
                }
                return !j() ? 1 : -1;
            case 17:
                return this.j != 0 ? Integer.MIN_VALUE : -1;
            case 33:
                return this.j != 1 ? Integer.MIN_VALUE : -1;
            case 66:
                if (this.j != 0) {
                    i4 = Integer.MIN_VALUE;
                }
                return i4;
            case 130:
                if (this.j == 1) {
                    i3 = 1;
                }
                return i3;
            default:
                return Integer.MIN_VALUE;
        }
    }

    public static class b extends RecyclerView.j {

        /* renamed from: a  reason: collision with root package name */
        c f613a;
        boolean b;

        public b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public b(int i, int i2) {
            super(i, i2);
        }

        public b(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public b(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public boolean a() {
            return this.b;
        }

        public final int b() {
            if (this.f613a == null) {
                return -1;
            }
            return this.f613a.e;
        }
    }

    /* access modifiers changed from: package-private */
    public class c {

        /* renamed from: a  reason: collision with root package name */
        ArrayList<View> f614a = new ArrayList<>();
        int b = Integer.MIN_VALUE;
        int c = Integer.MIN_VALUE;
        int d = 0;
        final int e;

        c(int i) {
            this.e = i;
        }

        /* access modifiers changed from: package-private */
        public int a(int i) {
            if (this.b != Integer.MIN_VALUE) {
                return this.b;
            }
            if (this.f614a.size() == 0) {
                return i;
            }
            a();
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            LazySpanLookup.FullSpanItem f2;
            View view = this.f614a.get(0);
            b c2 = c(view);
            this.b = StaggeredGridLayoutManager.this.b.a(view);
            if (c2.b && (f2 = StaggeredGridLayoutManager.this.h.f(c2.f())) != null && f2.b == -1) {
                this.b -= f2.a(this.e);
            }
        }

        /* access modifiers changed from: package-private */
        public int b() {
            if (this.b != Integer.MIN_VALUE) {
                return this.b;
            }
            a();
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public int b(int i) {
            if (this.c != Integer.MIN_VALUE) {
                return this.c;
            }
            if (this.f614a.size() == 0) {
                return i;
            }
            c();
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public void c() {
            LazySpanLookup.FullSpanItem f2;
            View view = this.f614a.get(this.f614a.size() - 1);
            b c2 = c(view);
            this.c = StaggeredGridLayoutManager.this.b.b(view);
            if (c2.b && (f2 = StaggeredGridLayoutManager.this.h.f(c2.f())) != null && f2.b == 1) {
                this.c = f2.a(this.e) + this.c;
            }
        }

        /* access modifiers changed from: package-private */
        public int d() {
            if (this.c != Integer.MIN_VALUE) {
                return this.c;
            }
            c();
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public void a(View view) {
            b c2 = c(view);
            c2.f613a = this;
            this.f614a.add(0, view);
            this.b = Integer.MIN_VALUE;
            if (this.f614a.size() == 1) {
                this.c = Integer.MIN_VALUE;
            }
            if (c2.d() || c2.e()) {
                this.d += StaggeredGridLayoutManager.this.b.e(view);
            }
        }

        /* access modifiers changed from: package-private */
        public void b(View view) {
            b c2 = c(view);
            c2.f613a = this;
            this.f614a.add(view);
            this.c = Integer.MIN_VALUE;
            if (this.f614a.size() == 1) {
                this.b = Integer.MIN_VALUE;
            }
            if (c2.d() || c2.e()) {
                this.d += StaggeredGridLayoutManager.this.b.e(view);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z, int i) {
            int a2;
            if (z) {
                a2 = b(Integer.MIN_VALUE);
            } else {
                a2 = a(Integer.MIN_VALUE);
            }
            e();
            if (a2 != Integer.MIN_VALUE) {
                if (z && a2 < StaggeredGridLayoutManager.this.b.d()) {
                    return;
                }
                if (z || a2 <= StaggeredGridLayoutManager.this.b.c()) {
                    if (i != Integer.MIN_VALUE) {
                        a2 += i;
                    }
                    this.c = a2;
                    this.b = a2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void e() {
            this.f614a.clear();
            f();
            this.d = 0;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.b = Integer.MIN_VALUE;
            this.c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void c(int i) {
            this.b = i;
            this.c = i;
        }

        /* access modifiers changed from: package-private */
        public void g() {
            int size = this.f614a.size();
            View remove = this.f614a.remove(size - 1);
            b c2 = c(remove);
            c2.f613a = null;
            if (c2.d() || c2.e()) {
                this.d -= StaggeredGridLayoutManager.this.b.e(remove);
            }
            if (size == 1) {
                this.b = Integer.MIN_VALUE;
            }
            this.c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void h() {
            View remove = this.f614a.remove(0);
            b c2 = c(remove);
            c2.f613a = null;
            if (this.f614a.size() == 0) {
                this.c = Integer.MIN_VALUE;
            }
            if (c2.d() || c2.e()) {
                this.d -= StaggeredGridLayoutManager.this.b.e(remove);
            }
            this.b = Integer.MIN_VALUE;
        }

        public int i() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public b c(View view) {
            return (b) view.getLayoutParams();
        }

        /* access modifiers changed from: package-private */
        public void d(int i) {
            if (this.b != Integer.MIN_VALUE) {
                this.b += i;
            }
            if (this.c != Integer.MIN_VALUE) {
                this.c += i;
            }
        }

        public int j() {
            if (StaggeredGridLayoutManager.this.d) {
                return a(this.f614a.size() - 1, -1, true);
            }
            return a(0, this.f614a.size(), true);
        }

        public int k() {
            if (StaggeredGridLayoutManager.this.d) {
                return a(0, this.f614a.size(), true);
            }
            return a(this.f614a.size() - 1, -1, true);
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2, boolean z, boolean z2, boolean z3) {
            int c2 = StaggeredGridLayoutManager.this.b.c();
            int d2 = StaggeredGridLayoutManager.this.b.d();
            int i3 = i2 > i ? 1 : -1;
            while (i != i2) {
                View view = this.f614a.get(i);
                int a2 = StaggeredGridLayoutManager.this.b.a(view);
                int b2 = StaggeredGridLayoutManager.this.b.b(view);
                boolean z4 = z3 ? a2 <= d2 : a2 < d2;
                boolean z5 = z3 ? b2 >= c2 : b2 > c2;
                if (z4 && z5) {
                    if (!z || !z2) {
                        if (z2) {
                            return StaggeredGridLayoutManager.this.d(view);
                        }
                        if (a2 < c2 || b2 > d2) {
                            return StaggeredGridLayoutManager.this.d(view);
                        }
                    } else if (a2 >= c2 && b2 <= d2) {
                        return StaggeredGridLayoutManager.this.d(view);
                    }
                }
                i += i3;
            }
            return -1;
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2, boolean z) {
            return a(i, i2, false, false, z);
        }

        public View a(int i, int i2) {
            View view = null;
            if (i2 == -1) {
                int size = this.f614a.size();
                int i3 = 0;
                while (i3 < size) {
                    View view2 = this.f614a.get(i3);
                    if ((StaggeredGridLayoutManager.this.d && StaggeredGridLayoutManager.this.d(view2) <= i) || ((!StaggeredGridLayoutManager.this.d && StaggeredGridLayoutManager.this.d(view2) >= i) || !view2.hasFocusable())) {
                        break;
                    }
                    i3++;
                    view = view2;
                }
                return view;
            }
            int size2 = this.f614a.size() - 1;
            while (size2 >= 0) {
                View view3 = this.f614a.get(size2);
                if (StaggeredGridLayoutManager.this.d && StaggeredGridLayoutManager.this.d(view3) >= i) {
                    break;
                } else if (StaggeredGridLayoutManager.this.d || StaggeredGridLayoutManager.this.d(view3) > i) {
                    if (!view3.hasFocusable()) {
                        break;
                    }
                    size2--;
                    view = view3;
                } else {
                    return view;
                }
            }
            return view;
        }
    }

    /* access modifiers changed from: package-private */
    public static class LazySpanLookup {

        /* renamed from: a  reason: collision with root package name */
        int[] f609a;
        List<FullSpanItem> b;

        LazySpanLookup() {
        }

        /* access modifiers changed from: package-private */
        public int a(int i) {
            if (this.b != null) {
                for (int size = this.b.size() - 1; size >= 0; size--) {
                    if (this.b.get(size).f610a >= i) {
                        this.b.remove(size);
                    }
                }
            }
            return b(i);
        }

        /* access modifiers changed from: package-private */
        public int b(int i) {
            if (this.f609a == null || i >= this.f609a.length) {
                return -1;
            }
            int g = g(i);
            if (g == -1) {
                Arrays.fill(this.f609a, i, this.f609a.length, -1);
                return this.f609a.length;
            }
            Arrays.fill(this.f609a, i, g + 1, -1);
            return g + 1;
        }

        /* access modifiers changed from: package-private */
        public int c(int i) {
            if (this.f609a == null || i >= this.f609a.length) {
                return -1;
            }
            return this.f609a[i];
        }

        /* access modifiers changed from: package-private */
        public void a(int i, c cVar) {
            e(i);
            this.f609a[i] = cVar.e;
        }

        /* access modifiers changed from: package-private */
        public int d(int i) {
            int length = this.f609a.length;
            while (length <= i) {
                length *= 2;
            }
            return length;
        }

        /* access modifiers changed from: package-private */
        public void e(int i) {
            if (this.f609a == null) {
                this.f609a = new int[(Math.max(i, 10) + 1)];
                Arrays.fill(this.f609a, -1);
            } else if (i >= this.f609a.length) {
                int[] iArr = this.f609a;
                this.f609a = new int[d(i)];
                System.arraycopy(iArr, 0, this.f609a, 0, iArr.length);
                Arrays.fill(this.f609a, iArr.length, this.f609a.length, -1);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (this.f609a != null) {
                Arrays.fill(this.f609a, -1);
            }
            this.b = null;
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2) {
            if (this.f609a != null && i < this.f609a.length) {
                e(i + i2);
                System.arraycopy(this.f609a, i + i2, this.f609a, i, (this.f609a.length - i) - i2);
                Arrays.fill(this.f609a, this.f609a.length - i2, this.f609a.length, -1);
                c(i, i2);
            }
        }

        private void c(int i, int i2) {
            if (this.b != null) {
                int i3 = i + i2;
                for (int size = this.b.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.b.get(size);
                    if (fullSpanItem.f610a >= i) {
                        if (fullSpanItem.f610a < i3) {
                            this.b.remove(size);
                        } else {
                            fullSpanItem.f610a -= i2;
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i, int i2) {
            if (this.f609a != null && i < this.f609a.length) {
                e(i + i2);
                System.arraycopy(this.f609a, i, this.f609a, i + i2, (this.f609a.length - i) - i2);
                Arrays.fill(this.f609a, i, i + i2, -1);
                d(i, i2);
            }
        }

        private void d(int i, int i2) {
            if (this.b != null) {
                for (int size = this.b.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.b.get(size);
                    if (fullSpanItem.f610a >= i) {
                        fullSpanItem.f610a += i2;
                    }
                }
            }
        }

        private int g(int i) {
            if (this.b == null) {
                return -1;
            }
            FullSpanItem f = f(i);
            if (f != null) {
                this.b.remove(f);
            }
            int size = this.b.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i2 = -1;
                    break;
                } else if (this.b.get(i2).f610a >= i) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 == -1) {
                return -1;
            }
            this.b.remove(i2);
            return this.b.get(i2).f610a;
        }

        public void a(FullSpanItem fullSpanItem) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                FullSpanItem fullSpanItem2 = this.b.get(i);
                if (fullSpanItem2.f610a == fullSpanItem.f610a) {
                    this.b.remove(i);
                }
                if (fullSpanItem2.f610a >= fullSpanItem.f610a) {
                    this.b.add(i, fullSpanItem);
                    return;
                }
            }
            this.b.add(fullSpanItem);
        }

        public FullSpanItem f(int i) {
            if (this.b == null) {
                return null;
            }
            for (int size = this.b.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = this.b.get(size);
                if (fullSpanItem.f610a == i) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        public FullSpanItem a(int i, int i2, int i3, boolean z) {
            if (this.b == null) {
                return null;
            }
            int size = this.b.size();
            for (int i4 = 0; i4 < size; i4++) {
                FullSpanItem fullSpanItem = this.b.get(i4);
                if (fullSpanItem.f610a >= i2) {
                    return null;
                }
                if (fullSpanItem.f610a >= i) {
                    if (i3 == 0 || fullSpanItem.b == i3) {
                        return fullSpanItem;
                    }
                    if (z && fullSpanItem.d) {
                        return fullSpanItem;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public static class FullSpanItem implements Parcelable {
            public static final Parcelable.Creator<FullSpanItem> CREATOR = new Parcelable.Creator<FullSpanItem>() {
                /* class android.support.v7.widget.StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem.AnonymousClass1 */

                /* renamed from: a */
                public FullSpanItem createFromParcel(Parcel parcel) {
                    return new FullSpanItem(parcel);
                }

                /* renamed from: a */
                public FullSpanItem[] newArray(int i) {
                    return new FullSpanItem[i];
                }
            };

            /* renamed from: a  reason: collision with root package name */
            int f610a;
            int b;
            int[] c;
            boolean d;

            FullSpanItem(Parcel parcel) {
                boolean z = true;
                this.f610a = parcel.readInt();
                this.b = parcel.readInt();
                this.d = parcel.readInt() != 1 ? false : z;
                int readInt = parcel.readInt();
                if (readInt > 0) {
                    this.c = new int[readInt];
                    parcel.readIntArray(this.c);
                }
            }

            FullSpanItem() {
            }

            /* access modifiers changed from: package-private */
            public int a(int i) {
                if (this.c == null) {
                    return 0;
                }
                return this.c[i];
            }

            public int describeContents() {
                return 0;
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f610a);
                parcel.writeInt(this.b);
                parcel.writeInt(this.d ? 1 : 0);
                if (this.c == null || this.c.length <= 0) {
                    parcel.writeInt(0);
                    return;
                }
                parcel.writeInt(this.c.length);
                parcel.writeIntArray(this.c);
            }

            public String toString() {
                return "FullSpanItem{mPosition=" + this.f610a + ", mGapDir=" + this.b + ", mHasUnwantedGapAfter=" + this.d + ", mGapPerSpan=" + Arrays.toString(this.c) + '}';
            }
        }
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* class android.support.v7.widget.StaggeredGridLayoutManager.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f611a;
        int b;
        int c;
        int[] d;
        int e;
        int[] f;
        List<LazySpanLookup.FullSpanItem> g;
        boolean h;
        boolean i;
        boolean j;

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            boolean z;
            boolean z2 = true;
            this.f611a = parcel.readInt();
            this.b = parcel.readInt();
            this.c = parcel.readInt();
            if (this.c > 0) {
                this.d = new int[this.c];
                parcel.readIntArray(this.d);
            }
            this.e = parcel.readInt();
            if (this.e > 0) {
                this.f = new int[this.e];
                parcel.readIntArray(this.f);
            }
            this.h = parcel.readInt() == 1;
            if (parcel.readInt() == 1) {
                z = true;
            } else {
                z = false;
            }
            this.i = z;
            this.j = parcel.readInt() != 1 ? false : z2;
            this.g = parcel.readArrayList(LazySpanLookup.FullSpanItem.class.getClassLoader());
        }

        public SavedState(SavedState savedState) {
            this.c = savedState.c;
            this.f611a = savedState.f611a;
            this.b = savedState.b;
            this.d = savedState.d;
            this.e = savedState.e;
            this.f = savedState.f;
            this.h = savedState.h;
            this.i = savedState.i;
            this.j = savedState.j;
            this.g = savedState.g;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.d = null;
            this.c = 0;
            this.e = 0;
            this.f = null;
            this.g = null;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.d = null;
            this.c = 0;
            this.f611a = -1;
            this.b = -1;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            int i3;
            int i4;
            int i5 = 1;
            parcel.writeInt(this.f611a);
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
            if (this.c > 0) {
                parcel.writeIntArray(this.d);
            }
            parcel.writeInt(this.e);
            if (this.e > 0) {
                parcel.writeIntArray(this.f);
            }
            if (this.h) {
                i3 = 1;
            } else {
                i3 = 0;
            }
            parcel.writeInt(i3);
            if (this.i) {
                i4 = 1;
            } else {
                i4 = 0;
            }
            parcel.writeInt(i4);
            if (!this.j) {
                i5 = 0;
            }
            parcel.writeInt(i5);
            parcel.writeList(this.g);
        }
    }

    /* access modifiers changed from: package-private */
    public class a {

        /* renamed from: a  reason: collision with root package name */
        int f612a;
        int b;
        boolean c;
        boolean d;
        boolean e;
        int[] f;

        a() {
            a();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f612a = -1;
            this.b = Integer.MIN_VALUE;
            this.c = false;
            this.d = false;
            this.e = false;
            if (this.f != null) {
                Arrays.fill(this.f, -1);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(c[] cVarArr) {
            int length = cVarArr.length;
            if (this.f == null || this.f.length < length) {
                this.f = new int[StaggeredGridLayoutManager.this.f607a.length];
            }
            for (int i = 0; i < length; i++) {
                this.f[i] = cVarArr[i].a(Integer.MIN_VALUE);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            int c2;
            if (this.c) {
                c2 = StaggeredGridLayoutManager.this.b.d();
            } else {
                c2 = StaggeredGridLayoutManager.this.b.c();
            }
            this.b = c2;
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            if (this.c) {
                this.b = StaggeredGridLayoutManager.this.b.d() - i;
            } else {
                this.b = StaggeredGridLayoutManager.this.b.c() + i;
            }
        }
    }
}
