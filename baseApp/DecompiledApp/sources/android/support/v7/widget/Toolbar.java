package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.annotation.StringRes;
import android.support.annotation.StyleRes;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.a;
import android.support.v7.appcompat.R;
import android.support.v7.view.g;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.u;
import android.support.v7.widget.ActionMenuView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.exoplayer.C;
import java.util.ArrayList;
import java.util.List;

public class Toolbar extends ViewGroup {
    private int A;
    private int B;
    private boolean C;
    private boolean D;
    private final ArrayList<View> E;
    private final ArrayList<View> F;
    private final int[] G;
    private final ActionMenuView.e H;
    private bl I;
    private ActionMenuPresenter J;
    private a K;
    private o.a L;
    private h.a M;
    private boolean N;
    private final Runnable O;

    /* renamed from: a  reason: collision with root package name */
    private ActionMenuView f615a;
    ImageButton b;
    View c;
    int d;
    c e;
    private TextView f;
    private TextView g;
    private ImageButton h;
    private ImageView i;
    private Drawable j;
    private CharSequence k;
    private Context l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private ba u;
    private int v;
    private int w;
    private int x;
    private CharSequence y;
    private CharSequence z;

    public interface c {
        boolean a(MenuItem menuItem);
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.toolbarStyle);
    }

    public Toolbar(Context context, @Nullable AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.x = 8388627;
        this.E = new ArrayList<>();
        this.F = new ArrayList<>();
        this.G = new int[2];
        this.H = new ActionMenuView.e() {
            /* class android.support.v7.widget.Toolbar.AnonymousClass1 */

            @Override // android.support.v7.widget.ActionMenuView.e
            public boolean a(MenuItem menuItem) {
                if (Toolbar.this.e != null) {
                    return Toolbar.this.e.a(menuItem);
                }
                return false;
            }
        };
        this.O = new Runnable() {
            /* class android.support.v7.widget.Toolbar.AnonymousClass2 */

            public void run() {
                Toolbar.this.d();
            }
        };
        bk a2 = bk.a(getContext(), attributeSet, R.styleable.Toolbar, i2, 0);
        this.n = a2.g(R.styleable.Toolbar_titleTextAppearance, 0);
        this.o = a2.g(R.styleable.Toolbar_subtitleTextAppearance, 0);
        this.x = a2.c(R.styleable.Toolbar_android_gravity, this.x);
        this.d = a2.c(R.styleable.Toolbar_buttonGravity, 48);
        int d2 = a2.d(R.styleable.Toolbar_titleMargin, 0);
        d2 = a2.g(R.styleable.Toolbar_titleMargins) ? a2.d(R.styleable.Toolbar_titleMargins, d2) : d2;
        this.t = d2;
        this.s = d2;
        this.r = d2;
        this.q = d2;
        int d3 = a2.d(R.styleable.Toolbar_titleMarginStart, -1);
        if (d3 >= 0) {
            this.q = d3;
        }
        int d4 = a2.d(R.styleable.Toolbar_titleMarginEnd, -1);
        if (d4 >= 0) {
            this.r = d4;
        }
        int d5 = a2.d(R.styleable.Toolbar_titleMarginTop, -1);
        if (d5 >= 0) {
            this.s = d5;
        }
        int d6 = a2.d(R.styleable.Toolbar_titleMarginBottom, -1);
        if (d6 >= 0) {
            this.t = d6;
        }
        this.p = a2.e(R.styleable.Toolbar_maxButtonHeight, -1);
        int d7 = a2.d(R.styleable.Toolbar_contentInsetStart, Integer.MIN_VALUE);
        int d8 = a2.d(R.styleable.Toolbar_contentInsetEnd, Integer.MIN_VALUE);
        int e2 = a2.e(R.styleable.Toolbar_contentInsetLeft, 0);
        int e3 = a2.e(R.styleable.Toolbar_contentInsetRight, 0);
        s();
        this.u.b(e2, e3);
        if (!(d7 == Integer.MIN_VALUE && d8 == Integer.MIN_VALUE)) {
            this.u.a(d7, d8);
        }
        this.v = a2.d(R.styleable.Toolbar_contentInsetStartWithNavigation, Integer.MIN_VALUE);
        this.w = a2.d(R.styleable.Toolbar_contentInsetEndWithActions, Integer.MIN_VALUE);
        this.j = a2.a(R.styleable.Toolbar_collapseIcon);
        this.k = a2.c(R.styleable.Toolbar_collapseContentDescription);
        CharSequence c2 = a2.c(R.styleable.Toolbar_title);
        if (!TextUtils.isEmpty(c2)) {
            setTitle(c2);
        }
        CharSequence c3 = a2.c(R.styleable.Toolbar_subtitle);
        if (!TextUtils.isEmpty(c3)) {
            setSubtitle(c3);
        }
        this.l = getContext();
        setPopupTheme(a2.g(R.styleable.Toolbar_popupTheme, 0));
        Drawable a3 = a2.a(R.styleable.Toolbar_navigationIcon);
        if (a3 != null) {
            setNavigationIcon(a3);
        }
        CharSequence c4 = a2.c(R.styleable.Toolbar_navigationContentDescription);
        if (!TextUtils.isEmpty(c4)) {
            setNavigationContentDescription(c4);
        }
        Drawable a4 = a2.a(R.styleable.Toolbar_logo);
        if (a4 != null) {
            setLogo(a4);
        }
        CharSequence c5 = a2.c(R.styleable.Toolbar_logoDescription);
        if (!TextUtils.isEmpty(c5)) {
            setLogoDescription(c5);
        }
        if (a2.g(R.styleable.Toolbar_titleTextColor)) {
            setTitleTextColor(a2.b(R.styleable.Toolbar_titleTextColor, -1));
        }
        if (a2.g(R.styleable.Toolbar_subtitleTextColor)) {
            setSubtitleTextColor(a2.b(R.styleable.Toolbar_subtitleTextColor, -1));
        }
        a2.a();
    }

    public void setPopupTheme(@StyleRes int i2) {
        if (this.m != i2) {
            this.m = i2;
            if (i2 == 0) {
                this.l = getContext();
            } else {
                this.l = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public int getPopupTheme() {
        return this.m;
    }

    public int getTitleMarginStart() {
        return this.q;
    }

    public void setTitleMarginStart(int i2) {
        this.q = i2;
        requestLayout();
    }

    public int getTitleMarginTop() {
        return this.s;
    }

    public void setTitleMarginTop(int i2) {
        this.s = i2;
        requestLayout();
    }

    public int getTitleMarginEnd() {
        return this.r;
    }

    public void setTitleMarginEnd(int i2) {
        this.r = i2;
        requestLayout();
    }

    public int getTitleMarginBottom() {
        return this.t;
    }

    public void setTitleMarginBottom(int i2) {
        this.t = i2;
        requestLayout();
    }

    public void onRtlPropertiesChanged(int i2) {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(i2);
        }
        s();
        ba baVar = this.u;
        if (i2 != 1) {
            z2 = false;
        }
        baVar.a(z2);
    }

    public void setLogo(@DrawableRes int i2) {
        setLogo(android.support.v7.a.a.a.b(getContext(), i2));
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public boolean a() {
        return getVisibility() == 0 && this.f615a != null && this.f615a.a();
    }

    public boolean b() {
        return this.f615a != null && this.f615a.g();
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public boolean c() {
        return this.f615a != null && this.f615a.h();
    }

    public boolean d() {
        return this.f615a != null && this.f615a.e();
    }

    public boolean e() {
        return this.f615a != null && this.f615a.f();
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void a(h hVar, ActionMenuPresenter actionMenuPresenter) {
        if (hVar != null || this.f615a != null) {
            o();
            h d2 = this.f615a.d();
            if (d2 != hVar) {
                if (d2 != null) {
                    d2.b(this.J);
                    d2.b(this.K);
                }
                if (this.K == null) {
                    this.K = new a();
                }
                actionMenuPresenter.c(true);
                if (hVar != null) {
                    hVar.a(actionMenuPresenter, this.l);
                    hVar.a(this.K, this.l);
                } else {
                    actionMenuPresenter.a(this.l, (h) null);
                    this.K.a(this.l, (h) null);
                    actionMenuPresenter.a(true);
                    this.K.a(true);
                }
                this.f615a.setPopupTheme(this.m);
                this.f615a.setPresenter(actionMenuPresenter);
                this.J = actionMenuPresenter;
            }
        }
    }

    public void f() {
        if (this.f615a != null) {
            this.f615a.i();
        }
    }

    public void setLogo(Drawable drawable) {
        if (drawable != null) {
            m();
            if (!d(this.i)) {
                a((View) this.i, true);
            }
        } else if (this.i != null && d(this.i)) {
            removeView(this.i);
            this.F.remove(this.i);
        }
        if (this.i != null) {
            this.i.setImageDrawable(drawable);
        }
    }

    public Drawable getLogo() {
        if (this.i != null) {
            return this.i.getDrawable();
        }
        return null;
    }

    public void setLogoDescription(@StringRes int i2) {
        setLogoDescription(getContext().getText(i2));
    }

    public void setLogoDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            m();
        }
        if (this.i != null) {
            this.i.setContentDescription(charSequence);
        }
    }

    public CharSequence getLogoDescription() {
        if (this.i != null) {
            return this.i.getContentDescription();
        }
        return null;
    }

    private void m() {
        if (this.i == null) {
            this.i = new AppCompatImageView(getContext());
        }
    }

    public boolean g() {
        return (this.K == null || this.K.b == null) ? false : true;
    }

    public void h() {
        j jVar = this.K == null ? null : this.K.b;
        if (jVar != null) {
            jVar.collapseActionView();
        }
    }

    public CharSequence getTitle() {
        return this.y;
    }

    public void setTitle(@StringRes int i2) {
        setTitle(getContext().getText(i2));
    }

    public void setTitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.f == null) {
                Context context = getContext();
                this.f = new y(context);
                this.f.setSingleLine();
                this.f.setEllipsize(TextUtils.TruncateAt.END);
                if (this.n != 0) {
                    this.f.setTextAppearance(context, this.n);
                }
                if (this.A != 0) {
                    this.f.setTextColor(this.A);
                }
            }
            if (!d(this.f)) {
                a((View) this.f, true);
            }
        } else if (this.f != null && d(this.f)) {
            removeView(this.f);
            this.F.remove(this.f);
        }
        if (this.f != null) {
            this.f.setText(charSequence);
        }
        this.y = charSequence;
    }

    public CharSequence getSubtitle() {
        return this.z;
    }

    public void setSubtitle(@StringRes int i2) {
        setSubtitle(getContext().getText(i2));
    }

    public void setSubtitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.g == null) {
                Context context = getContext();
                this.g = new y(context);
                this.g.setSingleLine();
                this.g.setEllipsize(TextUtils.TruncateAt.END);
                if (this.o != 0) {
                    this.g.setTextAppearance(context, this.o);
                }
                if (this.B != 0) {
                    this.g.setTextColor(this.B);
                }
            }
            if (!d(this.g)) {
                a((View) this.g, true);
            }
        } else if (this.g != null && d(this.g)) {
            removeView(this.g);
            this.F.remove(this.g);
        }
        if (this.g != null) {
            this.g.setText(charSequence);
        }
        this.z = charSequence;
    }

    public void a(Context context, @StyleRes int i2) {
        this.n = i2;
        if (this.f != null) {
            this.f.setTextAppearance(context, i2);
        }
    }

    public void b(Context context, @StyleRes int i2) {
        this.o = i2;
        if (this.g != null) {
            this.g.setTextAppearance(context, i2);
        }
    }

    public void setTitleTextColor(@ColorInt int i2) {
        this.A = i2;
        if (this.f != null) {
            this.f.setTextColor(i2);
        }
    }

    public void setSubtitleTextColor(@ColorInt int i2) {
        this.B = i2;
        if (this.g != null) {
            this.g.setTextColor(i2);
        }
    }

    @Nullable
    public CharSequence getNavigationContentDescription() {
        if (this.h != null) {
            return this.h.getContentDescription();
        }
        return null;
    }

    public void setNavigationContentDescription(@StringRes int i2) {
        setNavigationContentDescription(i2 != 0 ? getContext().getText(i2) : null);
    }

    public void setNavigationContentDescription(@Nullable CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            p();
        }
        if (this.h != null) {
            this.h.setContentDescription(charSequence);
        }
    }

    public void setNavigationIcon(@DrawableRes int i2) {
        setNavigationIcon(android.support.v7.a.a.a.b(getContext(), i2));
    }

    public void setNavigationIcon(@Nullable Drawable drawable) {
        if (drawable != null) {
            p();
            if (!d(this.h)) {
                a((View) this.h, true);
            }
        } else if (this.h != null && d(this.h)) {
            removeView(this.h);
            this.F.remove(this.h);
        }
        if (this.h != null) {
            this.h.setImageDrawable(drawable);
        }
    }

    @Nullable
    public Drawable getNavigationIcon() {
        if (this.h != null) {
            return this.h.getDrawable();
        }
        return null;
    }

    public void setNavigationOnClickListener(View.OnClickListener onClickListener) {
        p();
        this.h.setOnClickListener(onClickListener);
    }

    public Menu getMenu() {
        n();
        return this.f615a.getMenu();
    }

    public void setOverflowIcon(@Nullable Drawable drawable) {
        n();
        this.f615a.setOverflowIcon(drawable);
    }

    @Nullable
    public Drawable getOverflowIcon() {
        n();
        return this.f615a.getOverflowIcon();
    }

    private void n() {
        o();
        if (this.f615a.d() == null) {
            h hVar = (h) this.f615a.getMenu();
            if (this.K == null) {
                this.K = new a();
            }
            this.f615a.setExpandedActionViewsExclusive(true);
            hVar.a(this.K, this.l);
        }
    }

    private void o() {
        if (this.f615a == null) {
            this.f615a = new ActionMenuView(getContext());
            this.f615a.setPopupTheme(this.m);
            this.f615a.setOnMenuItemClickListener(this.H);
            this.f615a.a(this.L, this.M);
            b j2 = generateDefaultLayoutParams();
            j2.f461a = 8388613 | (this.d & 112);
            this.f615a.setLayoutParams(j2);
            a((View) this.f615a, false);
        }
    }

    private MenuInflater getMenuInflater() {
        return new g(getContext());
    }

    public void setOnMenuItemClickListener(c cVar) {
        this.e = cVar;
    }

    public void a(int i2, int i3) {
        s();
        this.u.a(i2, i3);
    }

    public int getContentInsetStart() {
        if (this.u != null) {
            return this.u.c();
        }
        return 0;
    }

    public int getContentInsetEnd() {
        if (this.u != null) {
            return this.u.d();
        }
        return 0;
    }

    public int getContentInsetLeft() {
        if (this.u != null) {
            return this.u.a();
        }
        return 0;
    }

    public int getContentInsetRight() {
        if (this.u != null) {
            return this.u.b();
        }
        return 0;
    }

    public int getContentInsetStartWithNavigation() {
        if (this.v != Integer.MIN_VALUE) {
            return this.v;
        }
        return getContentInsetStart();
    }

    public void setContentInsetStartWithNavigation(int i2) {
        if (i2 < 0) {
            i2 = Integer.MIN_VALUE;
        }
        if (i2 != this.v) {
            this.v = i2;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public int getContentInsetEndWithActions() {
        if (this.w != Integer.MIN_VALUE) {
            return this.w;
        }
        return getContentInsetEnd();
    }

    public void setContentInsetEndWithActions(int i2) {
        if (i2 < 0) {
            i2 = Integer.MIN_VALUE;
        }
        if (i2 != this.w) {
            this.w = i2;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public int getCurrentContentInsetStart() {
        if (getNavigationIcon() != null) {
            return Math.max(getContentInsetStart(), Math.max(this.v, 0));
        }
        return getContentInsetStart();
    }

    public int getCurrentContentInsetEnd() {
        boolean z2;
        if (this.f615a != null) {
            h d2 = this.f615a.d();
            z2 = d2 != null && d2.hasVisibleItems();
        } else {
            z2 = false;
        }
        if (z2) {
            return Math.max(getContentInsetEnd(), Math.max(this.w, 0));
        }
        return getContentInsetEnd();
    }

    public int getCurrentContentInsetLeft() {
        if (ViewCompat.getLayoutDirection(this) == 1) {
            return getCurrentContentInsetEnd();
        }
        return getCurrentContentInsetStart();
    }

    public int getCurrentContentInsetRight() {
        if (ViewCompat.getLayoutDirection(this) == 1) {
            return getCurrentContentInsetStart();
        }
        return getCurrentContentInsetEnd();
    }

    private void p() {
        if (this.h == null) {
            this.h = new n(getContext(), null, R.attr.toolbarNavigationButtonStyle);
            b j2 = generateDefaultLayoutParams();
            j2.f461a = 8388611 | (this.d & 112);
            this.h.setLayoutParams(j2);
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.b == null) {
            this.b = new n(getContext(), null, R.attr.toolbarNavigationButtonStyle);
            this.b.setImageDrawable(this.j);
            this.b.setContentDescription(this.k);
            b j2 = generateDefaultLayoutParams();
            j2.f461a = 8388611 | (this.d & 112);
            j2.b = 2;
            this.b.setLayoutParams(j2);
            this.b.setOnClickListener(new View.OnClickListener() {
                /* class android.support.v7.widget.Toolbar.AnonymousClass3 */

                public void onClick(View view) {
                    Toolbar.this.h();
                }
            });
        }
    }

    private void a(View view, boolean z2) {
        b bVar;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            bVar = generateDefaultLayoutParams();
        } else if (!checkLayoutParams(layoutParams)) {
            bVar = generateLayoutParams(layoutParams);
        } else {
            bVar = (b) layoutParams;
        }
        bVar.b = 1;
        if (!z2 || this.c == null) {
            addView(view, bVar);
            return;
        }
        view.setLayoutParams(bVar);
        this.F.add(view);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (!(this.K == null || this.K.b == null)) {
            savedState.f619a = this.K.b.getItemId();
        }
        savedState.b = b();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        h d2 = this.f615a != null ? this.f615a.d() : null;
        if (!(savedState.f619a == 0 || this.K == null || d2 == null || (findItem = d2.findItem(savedState.f619a)) == null)) {
            findItem.expandActionView();
        }
        if (savedState.b) {
            q();
        }
    }

    private void q() {
        removeCallbacks(this.O);
        post(this.O);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.O);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.C = false;
        }
        if (!this.C) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.C = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.C = false;
        }
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.D = false;
        }
        if (!this.D) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.D = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.D = false;
        }
        return true;
    }

    private void a(View view, int i2, int i3, int i4, int i5, int i6) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i3, marginLayoutParams.width);
        int childMeasureSpec2 = getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height);
        int mode = View.MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i6 >= 0) {
            if (mode != 0) {
                i6 = Math.min(View.MeasureSpec.getSize(childMeasureSpec2), i6);
            }
            childMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i6, C.ENCODING_PCM_32BIT);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    private int a(View view, int i2, int i3, int i4, int i5, int[] iArr) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i6 = marginLayoutParams.leftMargin - iArr[0];
        int i7 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i6) + Math.max(0, i7);
        iArr[0] = Math.max(0, -i6);
        iArr[1] = Math.max(0, -i7);
        view.measure(getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + max + i3, marginLayoutParams.width), getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    private boolean r() {
        if (!this.N) {
            return false;
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (a(childAt) && childAt.getMeasuredWidth() > 0 && childAt.getMeasuredHeight() > 0) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        char c2;
        char c3;
        int i4;
        int i5;
        int i6 = 0;
        int i7 = 0;
        int[] iArr = this.G;
        if (bs.a(this)) {
            c2 = 0;
            c3 = 1;
        } else {
            c2 = 1;
            c3 = 0;
        }
        int i8 = 0;
        if (a(this.h)) {
            a(this.h, i2, 0, i3, 0, this.p);
            i8 = this.h.getMeasuredWidth() + b(this.h);
            int max = Math.max(0, this.h.getMeasuredHeight() + c(this.h));
            i7 = View.combineMeasuredStates(0, this.h.getMeasuredState());
            i6 = max;
        }
        if (a(this.b)) {
            a(this.b, i2, 0, i3, 0, this.p);
            i8 = this.b.getMeasuredWidth() + b(this.b);
            i6 = Math.max(i6, this.b.getMeasuredHeight() + c(this.b));
            i7 = View.combineMeasuredStates(i7, this.b.getMeasuredState());
        }
        int currentContentInsetStart = getCurrentContentInsetStart();
        int max2 = 0 + Math.max(currentContentInsetStart, i8);
        iArr[c3] = Math.max(0, currentContentInsetStart - i8);
        int i9 = 0;
        if (a(this.f615a)) {
            a(this.f615a, i2, max2, i3, 0, this.p);
            i9 = this.f615a.getMeasuredWidth() + b(this.f615a);
            i6 = Math.max(i6, this.f615a.getMeasuredHeight() + c(this.f615a));
            i7 = View.combineMeasuredStates(i7, this.f615a.getMeasuredState());
        }
        int currentContentInsetEnd = getCurrentContentInsetEnd();
        int max3 = max2 + Math.max(currentContentInsetEnd, i9);
        iArr[c2] = Math.max(0, currentContentInsetEnd - i9);
        if (a(this.c)) {
            max3 += a(this.c, i2, max3, i3, 0, iArr);
            i6 = Math.max(i6, this.c.getMeasuredHeight() + c(this.c));
            i7 = View.combineMeasuredStates(i7, this.c.getMeasuredState());
        }
        if (a(this.i)) {
            max3 += a(this.i, i2, max3, i3, 0, iArr);
            i6 = Math.max(i6, this.i.getMeasuredHeight() + c(this.i));
            i7 = View.combineMeasuredStates(i7, this.i.getMeasuredState());
        }
        int childCount = getChildCount();
        int i10 = 0;
        int i11 = i7;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            if (((b) childAt.getLayoutParams()).b != 0) {
                i4 = i11;
                i5 = i6;
            } else if (!a(childAt)) {
                i4 = i11;
                i5 = i6;
            } else {
                max3 += a(childAt, i2, max3, i3, 0, iArr);
                int max4 = Math.max(i6, childAt.getMeasuredHeight() + c(childAt));
                i4 = View.combineMeasuredStates(i11, childAt.getMeasuredState());
                i5 = max4;
            }
            i10++;
            i11 = i4;
            i6 = i5;
        }
        int i12 = 0;
        int i13 = 0;
        int i14 = this.s + this.t;
        int i15 = this.q + this.r;
        if (a(this.f)) {
            a(this.f, i2, max3 + i15, i3, i14, iArr);
            i12 = b(this.f) + this.f.getMeasuredWidth();
            i13 = this.f.getMeasuredHeight() + c(this.f);
            i11 = View.combineMeasuredStates(i11, this.f.getMeasuredState());
        }
        if (a(this.g)) {
            i12 = Math.max(i12, a(this.g, i2, max3 + i15, i3, i14 + i13, iArr));
            i13 += this.g.getMeasuredHeight() + c(this.g);
            i11 = View.combineMeasuredStates(i11, this.g.getMeasuredState());
        }
        int max5 = Math.max(i6, i13);
        int paddingLeft = i12 + max3 + getPaddingLeft() + getPaddingRight();
        int paddingTop = max5 + getPaddingTop() + getPaddingBottom();
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(paddingLeft, getSuggestedMinimumWidth()), i2, -16777216 & i11);
        int resolveSizeAndState2 = View.resolveSizeAndState(Math.max(paddingTop, getSuggestedMinimumHeight()), i3, i11 << 16);
        if (r()) {
            resolveSizeAndState2 = 0;
        }
        setMeasuredDimension(resolveSizeAndState, resolveSizeAndState2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        boolean z3 = ViewCompat.getLayoutDirection(this) == 1;
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i18 = width - paddingRight;
        int[] iArr = this.G;
        iArr[1] = 0;
        iArr[0] = 0;
        int minimumHeight = ViewCompat.getMinimumHeight(this);
        int min = minimumHeight >= 0 ? Math.min(minimumHeight, i5 - i3) : 0;
        if (!a(this.h)) {
            i6 = i18;
            i7 = paddingLeft;
        } else if (z3) {
            i6 = b(this.h, i18, iArr, min);
            i7 = paddingLeft;
        } else {
            i7 = a(this.h, paddingLeft, iArr, min);
            i6 = i18;
        }
        if (a(this.b)) {
            if (z3) {
                i6 = b(this.b, i6, iArr, min);
            } else {
                i7 = a(this.b, i7, iArr, min);
            }
        }
        if (a(this.f615a)) {
            if (z3) {
                i7 = a(this.f615a, i7, iArr, min);
            } else {
                i6 = b(this.f615a, i6, iArr, min);
            }
        }
        int currentContentInsetLeft = getCurrentContentInsetLeft();
        int currentContentInsetRight = getCurrentContentInsetRight();
        iArr[0] = Math.max(0, currentContentInsetLeft - i7);
        iArr[1] = Math.max(0, currentContentInsetRight - ((width - paddingRight) - i6));
        int max = Math.max(i7, currentContentInsetLeft);
        int min2 = Math.min(i6, (width - paddingRight) - currentContentInsetRight);
        if (a(this.c)) {
            if (z3) {
                min2 = b(this.c, min2, iArr, min);
            } else {
                max = a(this.c, max, iArr, min);
            }
        }
        if (!a(this.i)) {
            i8 = min2;
            i9 = max;
        } else if (z3) {
            i8 = b(this.i, min2, iArr, min);
            i9 = max;
        } else {
            i8 = min2;
            i9 = a(this.i, max, iArr, min);
        }
        boolean a2 = a(this.f);
        boolean a3 = a(this.g);
        int i19 = 0;
        if (a2) {
            b bVar = (b) this.f.getLayoutParams();
            i19 = 0 + bVar.bottomMargin + bVar.topMargin + this.f.getMeasuredHeight();
        }
        if (a3) {
            b bVar2 = (b) this.g.getLayoutParams();
            i10 = bVar2.bottomMargin + bVar2.topMargin + this.g.getMeasuredHeight() + i19;
        } else {
            i10 = i19;
        }
        if (a2 || a3) {
            TextView textView = a2 ? this.f : this.g;
            TextView textView2 = a3 ? this.g : this.f;
            b bVar3 = (b) textView.getLayoutParams();
            b bVar4 = (b) textView2.getLayoutParams();
            boolean z4 = (a2 && this.f.getMeasuredWidth() > 0) || (a3 && this.g.getMeasuredWidth() > 0);
            switch (this.x & 112) {
                case 48:
                    i11 = bVar3.topMargin + getPaddingTop() + this.s;
                    break;
                case 80:
                    i11 = (((height - paddingBottom) - bVar4.bottomMargin) - this.t) - i10;
                    break;
                default:
                    int i20 = (((height - paddingTop) - paddingBottom) - i10) / 2;
                    if (i20 < bVar3.topMargin + this.s) {
                        i17 = bVar3.topMargin + this.s;
                    } else {
                        int i21 = (((height - paddingBottom) - i10) - i20) - paddingTop;
                        if (i21 < bVar3.bottomMargin + this.t) {
                            i17 = Math.max(0, i20 - ((bVar4.bottomMargin + this.t) - i21));
                        } else {
                            i17 = i20;
                        }
                    }
                    i11 = paddingTop + i17;
                    break;
            }
            if (z3) {
                int i22 = (z4 ? this.q : 0) - iArr[1];
                int max2 = i8 - Math.max(0, i22);
                iArr[1] = Math.max(0, -i22);
                if (a2) {
                    int measuredWidth = max2 - this.f.getMeasuredWidth();
                    int measuredHeight = this.f.getMeasuredHeight() + i11;
                    this.f.layout(measuredWidth, i11, max2, measuredHeight);
                    int i23 = measuredWidth - this.r;
                    i11 = measuredHeight + ((b) this.f.getLayoutParams()).bottomMargin;
                    i14 = i23;
                } else {
                    i14 = max2;
                }
                if (a3) {
                    b bVar5 = (b) this.g.getLayoutParams();
                    int i24 = bVar5.topMargin + i11;
                    int measuredHeight2 = this.g.getMeasuredHeight() + i24;
                    this.g.layout(max2 - this.g.getMeasuredWidth(), i24, max2, measuredHeight2);
                    int i25 = bVar5.bottomMargin + measuredHeight2;
                    i15 = max2 - this.r;
                } else {
                    i15 = max2;
                }
                if (z4) {
                    i16 = Math.min(i14, i15);
                } else {
                    i16 = max2;
                }
                i8 = i16;
            } else {
                int i26 = (z4 ? this.q : 0) - iArr[0];
                i9 += Math.max(0, i26);
                iArr[0] = Math.max(0, -i26);
                if (a2) {
                    int measuredWidth2 = this.f.getMeasuredWidth() + i9;
                    int measuredHeight3 = this.f.getMeasuredHeight() + i11;
                    this.f.layout(i9, i11, measuredWidth2, measuredHeight3);
                    int i27 = ((b) this.f.getLayoutParams()).bottomMargin + measuredHeight3;
                    i12 = measuredWidth2 + this.r;
                    i11 = i27;
                } else {
                    i12 = i9;
                }
                if (a3) {
                    b bVar6 = (b) this.g.getLayoutParams();
                    int i28 = bVar6.topMargin + i11;
                    int measuredWidth3 = this.g.getMeasuredWidth() + i9;
                    int measuredHeight4 = this.g.getMeasuredHeight() + i28;
                    this.g.layout(i9, i28, measuredWidth3, measuredHeight4);
                    int i29 = bVar6.bottomMargin + measuredHeight4;
                    i13 = this.r + measuredWidth3;
                } else {
                    i13 = i9;
                }
                if (z4) {
                    i9 = Math.max(i12, i13);
                }
            }
        }
        a(this.E, 3);
        int size = this.E.size();
        int i30 = i9;
        for (int i31 = 0; i31 < size; i31++) {
            i30 = a(this.E.get(i31), i30, iArr, min);
        }
        a(this.E, 5);
        int size2 = this.E.size();
        for (int i32 = 0; i32 < size2; i32++) {
            i8 = b(this.E.get(i32), i8, iArr, min);
        }
        a(this.E, 1);
        int a4 = a(this.E, iArr);
        int i33 = ((((width - paddingLeft) - paddingRight) / 2) + paddingLeft) - (a4 / 2);
        int i34 = a4 + i33;
        if (i33 < i30) {
            i33 = i30;
        } else if (i34 > i8) {
            i33 -= i34 - i8;
        }
        int size3 = this.E.size();
        int i35 = i33;
        for (int i36 = 0; i36 < size3; i36++) {
            i35 = a(this.E.get(i36), i35, iArr, min);
        }
        this.E.clear();
    }

    private int a(List<View> list, int[] iArr) {
        int i2 = iArr[0];
        int i3 = iArr[1];
        int size = list.size();
        int i4 = 0;
        int i5 = 0;
        int i6 = i3;
        int i7 = i2;
        while (i4 < size) {
            View view = list.get(i4);
            b bVar = (b) view.getLayoutParams();
            int i8 = bVar.leftMargin - i7;
            int i9 = bVar.rightMargin - i6;
            int max = Math.max(0, i8);
            int max2 = Math.max(0, i9);
            i7 = Math.max(0, -i8);
            i6 = Math.max(0, -i9);
            i4++;
            i5 += view.getMeasuredWidth() + max + max2;
        }
        return i5;
    }

    private int a(View view, int i2, int[] iArr, int i3) {
        b bVar = (b) view.getLayoutParams();
        int i4 = bVar.leftMargin - iArr[0];
        int max = Math.max(0, i4) + i2;
        iArr[0] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, a2, max + measuredWidth, view.getMeasuredHeight() + a2);
        return bVar.rightMargin + measuredWidth + max;
    }

    private int b(View view, int i2, int[] iArr, int i3) {
        b bVar = (b) view.getLayoutParams();
        int i4 = bVar.rightMargin - iArr[1];
        int max = i2 - Math.max(0, i4);
        iArr[1] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, a2, max, view.getMeasuredHeight() + a2);
        return max - (bVar.leftMargin + measuredWidth);
    }

    private int a(View view, int i2) {
        int max;
        b bVar = (b) view.getLayoutParams();
        int measuredHeight = view.getMeasuredHeight();
        int i3 = i2 > 0 ? (measuredHeight - i2) / 2 : 0;
        switch (a(bVar.f461a)) {
            case 48:
                return getPaddingTop() - i3;
            case 80:
                return (((getHeight() - getPaddingBottom()) - measuredHeight) - bVar.bottomMargin) - i3;
            default:
                int paddingTop = getPaddingTop();
                int paddingBottom = getPaddingBottom();
                int height = getHeight();
                int i4 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
                if (i4 < bVar.topMargin) {
                    max = bVar.topMargin;
                } else {
                    int i5 = (((height - paddingBottom) - measuredHeight) - i4) - paddingTop;
                    max = i5 < bVar.bottomMargin ? Math.max(0, i4 - (bVar.bottomMargin - i5)) : i4;
                }
                return max + paddingTop;
        }
    }

    private int a(int i2) {
        int i3 = i2 & 112;
        switch (i3) {
            case 16:
            case 48:
            case 80:
                return i3;
            default:
                return this.x & 112;
        }
    }

    private void a(List<View> list, int i2) {
        boolean z2 = true;
        if (ViewCompat.getLayoutDirection(this) != 1) {
            z2 = false;
        }
        int childCount = getChildCount();
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i2, ViewCompat.getLayoutDirection(this));
        list.clear();
        if (z2) {
            for (int i3 = childCount - 1; i3 >= 0; i3--) {
                View childAt = getChildAt(i3);
                b bVar = (b) childAt.getLayoutParams();
                if (bVar.b == 0 && a(childAt) && b(bVar.f461a) == absoluteGravity) {
                    list.add(childAt);
                }
            }
            return;
        }
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt2 = getChildAt(i4);
            b bVar2 = (b) childAt2.getLayoutParams();
            if (bVar2.b == 0 && a(childAt2) && b(bVar2.f461a) == absoluteGravity) {
                list.add(childAt2);
            }
        }
    }

    private int b(int i2) {
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i2, layoutDirection) & 7;
        switch (absoluteGravity) {
            case 1:
            case 3:
            case 5:
                return absoluteGravity;
            case 2:
            case 4:
            default:
                return layoutDirection == 1 ? 5 : 3;
        }
    }

    private boolean a(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    private int b(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return MarginLayoutParamsCompat.getMarginEnd(marginLayoutParams) + MarginLayoutParamsCompat.getMarginStart(marginLayoutParams);
    }

    private int c(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.bottomMargin + marginLayoutParams.topMargin;
    }

    /* renamed from: a */
    public b generateLayoutParams(AttributeSet attributeSet) {
        return new b(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public b generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof b) {
            return new b((b) layoutParams);
        }
        if (layoutParams instanceof a.C0021a) {
            return new b((a.C0021a) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new b((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new b(layoutParams);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public b generateDefaultLayoutParams() {
        return new b(-2, -2);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof b);
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public ah getWrapper() {
        if (this.I == null) {
            this.I = new bl(this, true);
        }
        return this.I;
    }

    /* access modifiers changed from: package-private */
    public void k() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            if (!(((b) childAt.getLayoutParams()).b == 2 || childAt == this.f615a)) {
                removeViewAt(childCount);
                this.F.add(childAt);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        for (int size = this.F.size() - 1; size >= 0; size--) {
            addView(this.F.get(size));
        }
        this.F.clear();
    }

    private boolean d(View view) {
        return view.getParent() == this || this.F.contains(view);
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void setCollapsible(boolean z2) {
        this.N = z2;
        requestLayout();
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void a(o.a aVar, h.a aVar2) {
        this.L = aVar;
        this.M = aVar2;
        if (this.f615a != null) {
            this.f615a.a(aVar, aVar2);
        }
    }

    private void s() {
        if (this.u == null) {
            this.u = new ba();
        }
    }

    /* access modifiers changed from: package-private */
    public ActionMenuPresenter getOuterActionMenuPresenter() {
        return this.J;
    }

    /* access modifiers changed from: package-private */
    public Context getPopupContext() {
        return this.l;
    }

    public static class b extends a.C0021a {
        int b = 0;

        public b(@NonNull Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public b(int i, int i2) {
            super(i, i2);
            this.f461a = 8388627;
        }

        public b(b bVar) {
            super((a.C0021a) bVar);
            this.b = bVar.b;
        }

        public b(a.C0021a aVar) {
            super(aVar);
        }

        public b(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            a(marginLayoutParams);
        }

        public b(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* access modifiers changed from: package-private */
        public void a(ViewGroup.MarginLayoutParams marginLayoutParams) {
            this.leftMargin = marginLayoutParams.leftMargin;
            this.topMargin = marginLayoutParams.topMargin;
            this.rightMargin = marginLayoutParams.rightMargin;
            this.bottomMargin = marginLayoutParams.bottomMargin;
        }
    }

    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            /* class android.support.v7.widget.Toolbar.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f619a;
        boolean b;

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f619a = parcel.readInt();
            this.b = parcel.readInt() != 0;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        @Override // android.support.v4.view.AbsSavedState
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f619a);
            parcel.writeInt(this.b ? 1 : 0);
        }
    }

    /* access modifiers changed from: private */
    public class a implements o {

        /* renamed from: a  reason: collision with root package name */
        h f620a;
        j b;

        a() {
        }

        @Override // android.support.v7.view.menu.o
        public void a(Context context, h hVar) {
            if (!(this.f620a == null || this.b == null)) {
                this.f620a.d(this.b);
            }
            this.f620a = hVar;
        }

        @Override // android.support.v7.view.menu.o
        public void a(boolean z) {
            boolean z2 = false;
            if (this.b != null) {
                if (this.f620a != null) {
                    int size = this.f620a.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            break;
                        } else if (this.f620a.getItem(i) == this.b) {
                            z2 = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (!z2) {
                    b(this.f620a, this.b);
                }
            }
        }

        @Override // android.support.v7.view.menu.o
        public void a(o.a aVar) {
        }

        @Override // android.support.v7.view.menu.o
        public boolean a(u uVar) {
            return false;
        }

        @Override // android.support.v7.view.menu.o
        public void a(h hVar, boolean z) {
        }

        @Override // android.support.v7.view.menu.o
        public boolean a() {
            return false;
        }

        @Override // android.support.v7.view.menu.o
        public boolean a(h hVar, j jVar) {
            Toolbar.this.i();
            ViewParent parent = Toolbar.this.b.getParent();
            if (parent != Toolbar.this) {
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(Toolbar.this.b);
                }
                Toolbar.this.addView(Toolbar.this.b);
            }
            Toolbar.this.c = jVar.getActionView();
            this.b = jVar;
            ViewParent parent2 = Toolbar.this.c.getParent();
            if (parent2 != Toolbar.this) {
                if (parent2 instanceof ViewGroup) {
                    ((ViewGroup) parent2).removeView(Toolbar.this.c);
                }
                b j = Toolbar.this.generateDefaultLayoutParams();
                j.f461a = 8388611 | (Toolbar.this.d & 112);
                j.b = 2;
                Toolbar.this.c.setLayoutParams(j);
                Toolbar.this.addView(Toolbar.this.c);
            }
            Toolbar.this.k();
            Toolbar.this.requestLayout();
            jVar.e(true);
            if (Toolbar.this.c instanceof android.support.v7.view.c) {
                ((android.support.v7.view.c) Toolbar.this.c).a();
            }
            return true;
        }

        @Override // android.support.v7.view.menu.o
        public boolean b(h hVar, j jVar) {
            if (Toolbar.this.c instanceof android.support.v7.view.c) {
                ((android.support.v7.view.c) Toolbar.this.c).b();
            }
            Toolbar.this.removeView(Toolbar.this.c);
            Toolbar.this.removeView(Toolbar.this.b);
            Toolbar.this.c = null;
            Toolbar.this.l();
            this.b = null;
            Toolbar.this.requestLayout();
            jVar.e(false);
            return true;
        }

        @Override // android.support.v7.view.menu.o
        public int b() {
            return 0;
        }

        @Override // android.support.v7.view.menu.o
        public Parcelable c() {
            return null;
        }

        @Override // android.support.v7.view.menu.o
        public void a(Parcelable parcelable) {
        }
    }
}
