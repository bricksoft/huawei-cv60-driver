package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.a.a.a;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.widget.ImageView;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class o {

    /* renamed from: a  reason: collision with root package name */
    private final ImageView f715a;
    private bi b;
    private bi c;
    private bi d;

    public o(ImageView imageView) {
        this.f715a = imageView;
    }

    public void a(AttributeSet attributeSet, int i) {
        int g;
        bk a2 = bk.a(this.f715a.getContext(), attributeSet, R.styleable.AppCompatImageView, i, 0);
        try {
            Drawable drawable = this.f715a.getDrawable();
            if (!(drawable != null || (g = a2.g(R.styleable.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = a.b(this.f715a.getContext(), g)) == null)) {
                this.f715a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                aj.a(drawable);
            }
            if (a2.g(R.styleable.AppCompatImageView_tint)) {
                ImageViewCompat.setImageTintList(this.f715a, a2.e(R.styleable.AppCompatImageView_tint));
            }
            if (a2.g(R.styleable.AppCompatImageView_tintMode)) {
                ImageViewCompat.setImageTintMode(this.f715a, aj.a(a2.a(R.styleable.AppCompatImageView_tintMode, -1), null));
            }
        } finally {
            a2.a();
        }
    }

    public void a(int i) {
        if (i != 0) {
            Drawable b2 = a.b(this.f715a.getContext(), i);
            if (b2 != null) {
                aj.a(b2);
            }
            this.f715a.setImageDrawable(b2);
        } else {
            this.f715a.setImageDrawable(null);
        }
        d();
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        Drawable background = this.f715a.getBackground();
        if (Build.VERSION.SDK_INT < 21 || !(background instanceof RippleDrawable)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (this.c == null) {
            this.c = new bi();
        }
        this.c.f684a = colorStateList;
        this.c.d = true;
        d();
    }

    /* access modifiers changed from: package-private */
    public ColorStateList b() {
        if (this.c != null) {
            return this.c.f684a;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        if (this.c == null) {
            this.c = new bi();
        }
        this.c.b = mode;
        this.c.c = true;
        d();
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode c() {
        if (this.c != null) {
            return this.c.b;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        Drawable drawable = this.f715a.getDrawable();
        if (drawable != null) {
            aj.a(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (e() && a(drawable)) {
            return;
        }
        if (this.c != null) {
            k.a(drawable, this.c, this.f715a.getDrawableState());
        } else if (this.b != null) {
            k.a(drawable, this.b, this.f715a.getDrawableState());
        }
    }

    private boolean e() {
        int i = Build.VERSION.SDK_INT;
        return i > 21 ? this.b != null : i == 21;
    }

    private boolean a(@NonNull Drawable drawable) {
        if (this.d == null) {
            this.d = new bi();
        }
        bi biVar = this.d;
        biVar.a();
        ColorStateList imageTintList = ImageViewCompat.getImageTintList(this.f715a);
        if (imageTintList != null) {
            biVar.d = true;
            biVar.f684a = imageTintList;
        }
        PorterDuff.Mode imageTintMode = ImageViewCompat.getImageTintMode(this.f715a);
        if (imageTintMode != null) {
            biVar.c = true;
            biVar.b = imageTintMode;
        }
        if (!biVar.d && !biVar.c) {
            return false;
        }
        k.a(drawable, biVar, this.f715a.getDrawableState());
        return true;
    }
}
