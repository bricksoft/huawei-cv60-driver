package android.support.v7.widget;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.support.v4.util.Pools;
import android.support.v7.widget.RecyclerView;

/* access modifiers changed from: package-private */
public class br {
    @VisibleForTesting

    /* renamed from: a  reason: collision with root package name */
    final ArrayMap<RecyclerView.w, a> f697a = new ArrayMap<>();
    @VisibleForTesting
    final LongSparseArray<RecyclerView.w> b = new LongSparseArray<>();

    /* access modifiers changed from: package-private */
    public interface b {
        void a(RecyclerView.w wVar);

        void a(RecyclerView.w wVar, @NonNull RecyclerView.f.c cVar, @Nullable RecyclerView.f.c cVar2);

        void b(RecyclerView.w wVar, @Nullable RecyclerView.f.c cVar, RecyclerView.f.c cVar2);

        void c(RecyclerView.w wVar, @NonNull RecyclerView.f.c cVar, @NonNull RecyclerView.f.c cVar2);
    }

    br() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f697a.clear();
        this.b.clear();
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.w wVar, RecyclerView.f.c cVar) {
        a aVar = this.f697a.get(wVar);
        if (aVar == null) {
            aVar = a.a();
            this.f697a.put(wVar, aVar);
        }
        aVar.b = cVar;
        aVar.f698a |= 4;
    }

    /* access modifiers changed from: package-private */
    public boolean a(RecyclerView.w wVar) {
        a aVar = this.f697a.get(wVar);
        return (aVar == null || (aVar.f698a & 1) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public RecyclerView.f.c b(RecyclerView.w wVar) {
        return a(wVar, 4);
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public RecyclerView.f.c c(RecyclerView.w wVar) {
        return a(wVar, 8);
    }

    private RecyclerView.f.c a(RecyclerView.w wVar, int i) {
        a valueAt;
        RecyclerView.f.c cVar = null;
        int indexOfKey = this.f697a.indexOfKey(wVar);
        if (!(indexOfKey < 0 || (valueAt = this.f697a.valueAt(indexOfKey)) == null || (valueAt.f698a & i) == 0)) {
            valueAt.f698a &= i ^ -1;
            if (i == 4) {
                cVar = valueAt.b;
            } else if (i == 8) {
                cVar = valueAt.c;
            } else {
                throw new IllegalArgumentException("Must provide flag PRE or POST");
            }
            if ((valueAt.f698a & 12) == 0) {
                this.f697a.removeAt(indexOfKey);
                a.a(valueAt);
            }
        }
        return cVar;
    }

    /* access modifiers changed from: package-private */
    public void a(long j, RecyclerView.w wVar) {
        this.b.put(j, wVar);
    }

    /* access modifiers changed from: package-private */
    public void b(RecyclerView.w wVar, RecyclerView.f.c cVar) {
        a aVar = this.f697a.get(wVar);
        if (aVar == null) {
            aVar = a.a();
            this.f697a.put(wVar, aVar);
        }
        aVar.f698a |= 2;
        aVar.b = cVar;
    }

    /* access modifiers changed from: package-private */
    public boolean d(RecyclerView.w wVar) {
        a aVar = this.f697a.get(wVar);
        return (aVar == null || (aVar.f698a & 4) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.w a(long j) {
        return this.b.get(j);
    }

    /* access modifiers changed from: package-private */
    public void c(RecyclerView.w wVar, RecyclerView.f.c cVar) {
        a aVar = this.f697a.get(wVar);
        if (aVar == null) {
            aVar = a.a();
            this.f697a.put(wVar, aVar);
        }
        aVar.c = cVar;
        aVar.f698a |= 8;
    }

    /* access modifiers changed from: package-private */
    public void e(RecyclerView.w wVar) {
        a aVar = this.f697a.get(wVar);
        if (aVar == null) {
            aVar = a.a();
            this.f697a.put(wVar, aVar);
        }
        aVar.f698a |= 1;
    }

    /* access modifiers changed from: package-private */
    public void f(RecyclerView.w wVar) {
        a aVar = this.f697a.get(wVar);
        if (aVar != null) {
            aVar.f698a &= -2;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar) {
        for (int size = this.f697a.size() - 1; size >= 0; size--) {
            RecyclerView.w keyAt = this.f697a.keyAt(size);
            a removeAt = this.f697a.removeAt(size);
            if ((removeAt.f698a & 3) == 3) {
                bVar.a(keyAt);
            } else if ((removeAt.f698a & 1) != 0) {
                if (removeAt.b == null) {
                    bVar.a(keyAt);
                } else {
                    bVar.a(keyAt, removeAt.b, removeAt.c);
                }
            } else if ((removeAt.f698a & 14) == 14) {
                bVar.b(keyAt, removeAt.b, removeAt.c);
            } else if ((removeAt.f698a & 12) == 12) {
                bVar.c(keyAt, removeAt.b, removeAt.c);
            } else if ((removeAt.f698a & 4) != 0) {
                bVar.a(keyAt, removeAt.b, null);
            } else if ((removeAt.f698a & 8) != 0) {
                bVar.b(keyAt, removeAt.b, removeAt.c);
            } else if ((removeAt.f698a & 2) != 0) {
            }
            a.a(removeAt);
        }
    }

    /* access modifiers changed from: package-private */
    public void g(RecyclerView.w wVar) {
        int size = this.b.size() - 1;
        while (true) {
            if (size < 0) {
                break;
            } else if (wVar == this.b.valueAt(size)) {
                this.b.removeAt(size);
                break;
            } else {
                size--;
            }
        }
        a remove = this.f697a.remove(wVar);
        if (remove != null) {
            a.a(remove);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a.b();
    }

    public void h(RecyclerView.w wVar) {
        f(wVar);
    }

    /* access modifiers changed from: package-private */
    public static class a {
        static Pools.Pool<a> d = new Pools.SimplePool(20);

        /* renamed from: a  reason: collision with root package name */
        int f698a;
        @Nullable
        RecyclerView.f.c b;
        @Nullable
        RecyclerView.f.c c;

        private a() {
        }

        static a a() {
            a acquire = d.acquire();
            return acquire == null ? new a() : acquire;
        }

        static void a(a aVar) {
            aVar.f698a = 0;
            aVar.b = null;
            aVar.c = null;
            d.release(aVar);
        }

        static void b() {
            do {
            } while (d.acquire() != null);
        }
    }
}
