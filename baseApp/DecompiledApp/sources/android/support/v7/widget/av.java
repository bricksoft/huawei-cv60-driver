package android.support.v7.widget;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class av {

    /* renamed from: a  reason: collision with root package name */
    protected final RecyclerView.i f667a;
    final Rect b;
    private int c;

    public abstract int a(View view);

    public abstract void a(int i);

    public abstract int b(View view);

    public abstract int c();

    public abstract int c(View view);

    public abstract int d();

    public abstract int d(View view);

    public abstract int e();

    public abstract int e(View view);

    public abstract int f();

    public abstract int f(View view);

    public abstract int g();

    public abstract int h();

    public abstract int i();

    private av(RecyclerView.i iVar) {
        this.c = Integer.MIN_VALUE;
        this.b = new Rect();
        this.f667a = iVar;
    }

    public void a() {
        this.c = f();
    }

    public int b() {
        if (Integer.MIN_VALUE == this.c) {
            return 0;
        }
        return f() - this.c;
    }

    public static av a(RecyclerView.i iVar, int i) {
        switch (i) {
            case 0:
                return a(iVar);
            case 1:
                return b(iVar);
            default:
                throw new IllegalArgumentException("invalid orientation");
        }
    }

    public static av a(RecyclerView.i iVar) {
        return new av(iVar) {
            /* class android.support.v7.widget.av.AnonymousClass1 */

            @Override // android.support.v7.widget.av
            public int d() {
                return this.f667a.y() - this.f667a.C();
            }

            @Override // android.support.v7.widget.av
            public int e() {
                return this.f667a.y();
            }

            @Override // android.support.v7.widget.av
            public void a(int i) {
                this.f667a.j(i);
            }

            @Override // android.support.v7.widget.av
            public int c() {
                return this.f667a.A();
            }

            @Override // android.support.v7.widget.av
            public int e(View view) {
                RecyclerView.j jVar = (RecyclerView.j) view.getLayoutParams();
                return jVar.rightMargin + this.f667a.f(view) + jVar.leftMargin;
            }

            @Override // android.support.v7.widget.av
            public int f(View view) {
                RecyclerView.j jVar = (RecyclerView.j) view.getLayoutParams();
                return jVar.bottomMargin + this.f667a.g(view) + jVar.topMargin;
            }

            @Override // android.support.v7.widget.av
            public int b(View view) {
                return ((RecyclerView.j) view.getLayoutParams()).rightMargin + this.f667a.j(view);
            }

            @Override // android.support.v7.widget.av
            public int a(View view) {
                return this.f667a.h(view) - ((RecyclerView.j) view.getLayoutParams()).leftMargin;
            }

            @Override // android.support.v7.widget.av
            public int c(View view) {
                this.f667a.a(view, true, this.b);
                return this.b.right;
            }

            @Override // android.support.v7.widget.av
            public int d(View view) {
                this.f667a.a(view, true, this.b);
                return this.b.left;
            }

            @Override // android.support.v7.widget.av
            public int f() {
                return (this.f667a.y() - this.f667a.A()) - this.f667a.C();
            }

            @Override // android.support.v7.widget.av
            public int g() {
                return this.f667a.C();
            }

            @Override // android.support.v7.widget.av
            public int h() {
                return this.f667a.w();
            }

            @Override // android.support.v7.widget.av
            public int i() {
                return this.f667a.x();
            }
        };
    }

    public static av b(RecyclerView.i iVar) {
        return new av(iVar) {
            /* class android.support.v7.widget.av.AnonymousClass2 */

            @Override // android.support.v7.widget.av
            public int d() {
                return this.f667a.z() - this.f667a.D();
            }

            @Override // android.support.v7.widget.av
            public int e() {
                return this.f667a.z();
            }

            @Override // android.support.v7.widget.av
            public void a(int i) {
                this.f667a.k(i);
            }

            @Override // android.support.v7.widget.av
            public int c() {
                return this.f667a.B();
            }

            @Override // android.support.v7.widget.av
            public int e(View view) {
                RecyclerView.j jVar = (RecyclerView.j) view.getLayoutParams();
                return jVar.bottomMargin + this.f667a.g(view) + jVar.topMargin;
            }

            @Override // android.support.v7.widget.av
            public int f(View view) {
                RecyclerView.j jVar = (RecyclerView.j) view.getLayoutParams();
                return jVar.rightMargin + this.f667a.f(view) + jVar.leftMargin;
            }

            @Override // android.support.v7.widget.av
            public int b(View view) {
                return ((RecyclerView.j) view.getLayoutParams()).bottomMargin + this.f667a.k(view);
            }

            @Override // android.support.v7.widget.av
            public int a(View view) {
                return this.f667a.i(view) - ((RecyclerView.j) view.getLayoutParams()).topMargin;
            }

            @Override // android.support.v7.widget.av
            public int c(View view) {
                this.f667a.a(view, true, this.b);
                return this.b.bottom;
            }

            @Override // android.support.v7.widget.av
            public int d(View view) {
                this.f667a.a(view, true, this.b);
                return this.b.top;
            }

            @Override // android.support.v7.widget.av
            public int f() {
                return (this.f667a.z() - this.f667a.B()) - this.f667a.D();
            }

            @Override // android.support.v7.widget.av
            public int g() {
                return this.f667a.D();
            }

            @Override // android.support.v7.widget.av
            public int h() {
                return this.f667a.x();
            }

            @Override // android.support.v7.widget.av
            public int i() {
                return this.f667a.w();
            }
        };
    }
}
