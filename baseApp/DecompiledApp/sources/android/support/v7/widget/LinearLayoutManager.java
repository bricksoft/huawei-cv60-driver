package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.RestrictTo;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

public class LinearLayoutManager extends RecyclerView.i implements RecyclerView.s.b {

    /* renamed from: a  reason: collision with root package name */
    private c f562a;
    private boolean b;
    private boolean c = false;
    private boolean d = false;
    private boolean e = true;
    private boolean f;
    private final b g = new b();
    private int h = 2;
    int i = 1;
    av j;
    boolean k = false;
    int l = -1;
    int m = Integer.MIN_VALUE;
    SavedState n = null;
    final a o = new a();

    public LinearLayoutManager(Context context, int i2, boolean z) {
        b(i2);
        b(z);
    }

    public LinearLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        RecyclerView.i.b a2 = a(context, attributeSet, i2, i3);
        b(a2.f581a);
        b(a2.c);
        a(a2.d);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean c() {
        return true;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public RecyclerView.j a() {
        return new RecyclerView.j(-2, -2);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView recyclerView, RecyclerView.p pVar) {
        super.a(recyclerView, pVar);
        if (this.f) {
            c(pVar);
            pVar.a();
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(AccessibilityEvent accessibilityEvent) {
        super.a(accessibilityEvent);
        if (v() > 0) {
            accessibilityEvent.setFromIndex(m());
            accessibilityEvent.setToIndex(n());
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public Parcelable d() {
        if (this.n != null) {
            return new SavedState(this.n);
        }
        SavedState savedState = new SavedState();
        if (v() > 0) {
            i();
            boolean z = this.b ^ this.k;
            savedState.c = z;
            if (z) {
                View M = M();
                savedState.b = this.j.d() - this.j.b(M);
                savedState.f563a = d(M);
                return savedState;
            }
            View L = L();
            savedState.f563a = d(L);
            savedState.b = this.j.a(L) - this.j.c();
            return savedState;
        }
        savedState.b();
        return savedState;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.n = (SavedState) parcelable;
            o();
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean e() {
        return this.i == 0;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean f() {
        return this.i == 1;
    }

    public void a(boolean z) {
        a((String) null);
        if (this.d != z) {
            this.d = z;
            o();
        }
    }

    public int g() {
        return this.i;
    }

    public void b(int i2) {
        if (i2 == 0 || i2 == 1) {
            a((String) null);
            if (i2 != this.i || this.j == null) {
                this.j = av.a(this, i2);
                this.o.f564a = this.j;
                this.i = i2;
                o();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation:" + i2);
    }

    private void K() {
        boolean z = true;
        if (this.i == 1 || !h()) {
            this.k = this.c;
            return;
        }
        if (this.c) {
            z = false;
        }
        this.k = z;
    }

    public void b(boolean z) {
        a((String) null);
        if (z != this.c) {
            this.c = z;
            o();
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public View c(int i2) {
        int v = v();
        if (v == 0) {
            return null;
        }
        int d2 = i2 - d(i(0));
        if (d2 >= 0 && d2 < v) {
            View i3 = i(d2);
            if (d(i3) == i2) {
                return i3;
            }
        }
        return super.c(i2);
    }

    /* access modifiers changed from: protected */
    public int b(RecyclerView.t tVar) {
        if (tVar.d()) {
            return this.j.f();
        }
        return 0;
    }

    @Override // android.support.v7.widget.RecyclerView.s.b
    public PointF d(int i2) {
        int i3 = 1;
        boolean z = false;
        if (v() == 0) {
            return null;
        }
        if (i2 < d(i(0))) {
            z = true;
        }
        if (z != this.k) {
            i3 = -1;
        }
        if (this.i == 0) {
            return new PointF((float) i3, 0.0f);
        }
        return new PointF(0.0f, (float) i3);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void c(RecyclerView.p pVar, RecyclerView.t tVar) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        View c2;
        int a2;
        int i8 = -1;
        if (!(this.n == null && this.l == -1) && tVar.e() == 0) {
            c(pVar);
            return;
        }
        if (this.n != null && this.n.a()) {
            this.l = this.n.f563a;
        }
        i();
        this.f562a.f566a = false;
        K();
        View E = E();
        if (!this.o.e || this.l != -1 || this.n != null) {
            this.o.a();
            this.o.d = this.k ^ this.d;
            a(pVar, tVar, this.o);
            this.o.e = true;
        } else if (E != null && (this.j.a(E) >= this.j.d() || this.j.b(E) <= this.j.c())) {
            this.o.a(E, d(E));
        }
        int b2 = b(tVar);
        if (this.f562a.j >= 0) {
            i2 = b2;
            i3 = 0;
        } else {
            i2 = 0;
            i3 = b2;
        }
        int c3 = this.j.c() + i3;
        int g2 = i2 + this.j.g();
        if (!(!tVar.a() || this.l == -1 || this.m == Integer.MIN_VALUE || (c2 = c(this.l)) == null)) {
            if (this.k) {
                a2 = (this.j.d() - this.j.b(c2)) - this.m;
            } else {
                a2 = this.m - (this.j.a(c2) - this.j.c());
            }
            if (a2 > 0) {
                c3 += a2;
            } else {
                g2 -= a2;
            }
        }
        if (!this.o.d) {
            if (!this.k) {
                i8 = 1;
            }
            i4 = i8;
        } else if (this.k) {
            i4 = 1;
        } else {
            i4 = -1;
        }
        a(pVar, tVar, this.o, i4);
        a(pVar);
        this.f562a.l = k();
        this.f562a.i = tVar.a();
        if (this.o.d) {
            b(this.o);
            this.f562a.h = c3;
            a(pVar, this.f562a, tVar, false);
            int i9 = this.f562a.b;
            int i10 = this.f562a.d;
            if (this.f562a.c > 0) {
                g2 += this.f562a.c;
            }
            a(this.o);
            this.f562a.h = g2;
            this.f562a.d += this.f562a.e;
            a(pVar, this.f562a, tVar, false);
            int i11 = this.f562a.b;
            if (this.f562a.c > 0) {
                int i12 = this.f562a.c;
                g(i10, i9);
                this.f562a.h = i12;
                a(pVar, this.f562a, tVar, false);
                i7 = this.f562a.b;
            } else {
                i7 = i9;
            }
            i5 = i11;
            i6 = i7;
        } else {
            a(this.o);
            this.f562a.h = g2;
            a(pVar, this.f562a, tVar, false);
            int i13 = this.f562a.b;
            int i14 = this.f562a.d;
            if (this.f562a.c > 0) {
                c3 += this.f562a.c;
            }
            b(this.o);
            this.f562a.h = c3;
            this.f562a.d += this.f562a.e;
            a(pVar, this.f562a, tVar, false);
            int i15 = this.f562a.b;
            if (this.f562a.c > 0) {
                int i16 = this.f562a.c;
                a(i14, i13);
                this.f562a.h = i16;
                a(pVar, this.f562a, tVar, false);
                i5 = this.f562a.b;
                i6 = i15;
            } else {
                i5 = i13;
                i6 = i15;
            }
        }
        if (v() > 0) {
            if (this.k ^ this.d) {
                int a3 = a(i5, pVar, tVar, true);
                int i17 = i6 + a3;
                int i18 = a3 + i5;
                int b3 = b(i17, pVar, tVar, false);
                i6 = i17 + b3;
                i5 = b3 + i18;
            } else {
                int b4 = b(i6, pVar, tVar, true);
                int i19 = i6 + b4;
                int i20 = b4 + i5;
                int a4 = a(i20, pVar, tVar, false);
                i6 = i19 + a4;
                i5 = a4 + i20;
            }
        }
        b(pVar, tVar, i6, i5);
        if (!tVar.a()) {
            this.j.a();
        } else {
            this.o.a();
        }
        this.b = this.d;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(RecyclerView.t tVar) {
        super.a(tVar);
        this.n = null;
        this.l = -1;
        this.m = Integer.MIN_VALUE;
        this.o.a();
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.p pVar, RecyclerView.t tVar, a aVar, int i2) {
    }

    private void b(RecyclerView.p pVar, RecyclerView.t tVar, int i2, int i3) {
        int e2;
        int i4;
        if (tVar.b() && v() != 0 && !tVar.a() && b()) {
            int i5 = 0;
            int i6 = 0;
            List<RecyclerView.w> c2 = pVar.c();
            int size = c2.size();
            int d2 = d(i(0));
            int i7 = 0;
            while (i7 < size) {
                RecyclerView.w wVar = c2.get(i7);
                if (wVar.q()) {
                    e2 = i6;
                    i4 = i5;
                } else {
                    if (((wVar.d() < d2) != this.k ? (char) 65535 : 1) == 65535) {
                        i4 = i5 + this.j.e(wVar.f590a);
                        e2 = i6;
                    } else {
                        e2 = this.j.e(wVar.f590a) + i6;
                        i4 = i5;
                    }
                }
                i7++;
                i6 = e2;
                i5 = i4;
            }
            this.f562a.k = c2;
            if (i5 > 0) {
                g(d(L()), i2);
                this.f562a.h = i5;
                this.f562a.c = 0;
                this.f562a.a();
                a(pVar, this.f562a, tVar, false);
            }
            if (i6 > 0) {
                a(d(M()), i3);
                this.f562a.h = i6;
                this.f562a.c = 0;
                this.f562a.a();
                a(pVar, this.f562a, tVar, false);
            }
            this.f562a.k = null;
        }
    }

    private void a(RecyclerView.p pVar, RecyclerView.t tVar, a aVar) {
        if (!a(tVar, aVar) && !b(pVar, tVar, aVar)) {
            aVar.b();
            aVar.b = this.d ? tVar.e() - 1 : 0;
        }
    }

    private boolean b(RecyclerView.p pVar, RecyclerView.t tVar, a aVar) {
        View g2;
        int c2;
        boolean z = false;
        if (v() == 0) {
            return false;
        }
        View E = E();
        if (E != null && aVar.a(E, tVar)) {
            aVar.a(E, d(E));
            return true;
        } else if (this.b != this.d) {
            return false;
        } else {
            if (aVar.d) {
                g2 = f(pVar, tVar);
            } else {
                g2 = g(pVar, tVar);
            }
            if (g2 == null) {
                return false;
            }
            aVar.b(g2, d(g2));
            if (!tVar.a() && b()) {
                if (this.j.a(g2) >= this.j.d() || this.j.b(g2) < this.j.c()) {
                    z = true;
                }
                if (z) {
                    if (aVar.d) {
                        c2 = this.j.d();
                    } else {
                        c2 = this.j.c();
                    }
                    aVar.c = c2;
                }
            }
            return true;
        }
    }

    private boolean a(RecyclerView.t tVar, a aVar) {
        boolean z;
        int a2;
        boolean z2 = false;
        if (tVar.a() || this.l == -1) {
            return false;
        }
        if (this.l < 0 || this.l >= tVar.e()) {
            this.l = -1;
            this.m = Integer.MIN_VALUE;
            return false;
        }
        aVar.b = this.l;
        if (this.n != null && this.n.a()) {
            aVar.d = this.n.c;
            if (aVar.d) {
                aVar.c = this.j.d() - this.n.b;
                return true;
            }
            aVar.c = this.j.c() + this.n.b;
            return true;
        } else if (this.m == Integer.MIN_VALUE) {
            View c2 = c(this.l);
            if (c2 == null) {
                if (v() > 0) {
                    if (this.l < d(i(0))) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (z == this.k) {
                        z2 = true;
                    }
                    aVar.d = z2;
                }
                aVar.b();
                return true;
            } else if (this.j.e(c2) > this.j.f()) {
                aVar.b();
                return true;
            } else if (this.j.a(c2) - this.j.c() < 0) {
                aVar.c = this.j.c();
                aVar.d = false;
                return true;
            } else if (this.j.d() - this.j.b(c2) < 0) {
                aVar.c = this.j.d();
                aVar.d = true;
                return true;
            } else {
                if (aVar.d) {
                    a2 = this.j.b(c2) + this.j.b();
                } else {
                    a2 = this.j.a(c2);
                }
                aVar.c = a2;
                return true;
            }
        } else {
            aVar.d = this.k;
            if (this.k) {
                aVar.c = this.j.d() - this.m;
                return true;
            }
            aVar.c = this.j.c() + this.m;
            return true;
        }
    }

    private int a(int i2, RecyclerView.p pVar, RecyclerView.t tVar, boolean z) {
        int d2;
        int d3 = this.j.d() - i2;
        if (d3 <= 0) {
            return 0;
        }
        int i3 = -c(-d3, pVar, tVar);
        int i4 = i2 + i3;
        if (!z || (d2 = this.j.d() - i4) <= 0) {
            return i3;
        }
        this.j.a(d2);
        return i3 + d2;
    }

    private int b(int i2, RecyclerView.p pVar, RecyclerView.t tVar, boolean z) {
        int c2;
        int c3 = i2 - this.j.c();
        if (c3 <= 0) {
            return 0;
        }
        int i3 = -c(c3, pVar, tVar);
        int i4 = i2 + i3;
        if (!z || (c2 = i4 - this.j.c()) <= 0) {
            return i3;
        }
        this.j.a(-c2);
        return i3 - c2;
    }

    private void a(a aVar) {
        a(aVar.b, aVar.c);
    }

    private void a(int i2, int i3) {
        this.f562a.c = this.j.d() - i3;
        this.f562a.e = this.k ? -1 : 1;
        this.f562a.d = i2;
        this.f562a.f = 1;
        this.f562a.b = i3;
        this.f562a.g = Integer.MIN_VALUE;
    }

    private void b(a aVar) {
        g(aVar.b, aVar.c);
    }

    private void g(int i2, int i3) {
        this.f562a.c = i3 - this.j.c();
        this.f562a.d = i2;
        this.f562a.e = this.k ? 1 : -1;
        this.f562a.f = -1;
        this.f562a.b = i3;
        this.f562a.g = Integer.MIN_VALUE;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return t() == 1;
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.f562a == null) {
            this.f562a = j();
        }
    }

    /* access modifiers changed from: package-private */
    public c j() {
        return new c();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void e(int i2) {
        this.l = i2;
        this.m = Integer.MIN_VALUE;
        if (this.n != null) {
            this.n.b();
        }
        o();
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int a(int i2, RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.i == 1) {
            return 0;
        }
        return c(i2, pVar, tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int b(int i2, RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.i == 0) {
            return 0;
        }
        return c(i2, pVar, tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int c(RecyclerView.t tVar) {
        return i(tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int d(RecyclerView.t tVar) {
        return i(tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int e(RecyclerView.t tVar) {
        return j(tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int f(RecyclerView.t tVar) {
        return j(tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int g(RecyclerView.t tVar) {
        return k(tVar);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public int h(RecyclerView.t tVar) {
        return k(tVar);
    }

    private int i(RecyclerView.t tVar) {
        boolean z = false;
        if (v() == 0) {
            return 0;
        }
        i();
        av avVar = this.j;
        View a2 = a(!this.e, true);
        if (!this.e) {
            z = true;
        }
        return bb.a(tVar, avVar, a2, b(z, true), this, this.e, this.k);
    }

    private int j(RecyclerView.t tVar) {
        boolean z = false;
        if (v() == 0) {
            return 0;
        }
        i();
        av avVar = this.j;
        View a2 = a(!this.e, true);
        if (!this.e) {
            z = true;
        }
        return bb.a(tVar, avVar, a2, b(z, true), this, this.e);
    }

    private int k(RecyclerView.t tVar) {
        boolean z = false;
        if (v() == 0) {
            return 0;
        }
        i();
        av avVar = this.j;
        View a2 = a(!this.e, true);
        if (!this.e) {
            z = true;
        }
        return bb.b(tVar, avVar, a2, b(z, true), this, this.e);
    }

    private void a(int i2, int i3, boolean z, RecyclerView.t tVar) {
        int c2;
        int i4 = -1;
        int i5 = 1;
        this.f562a.l = k();
        this.f562a.h = b(tVar);
        this.f562a.f = i2;
        if (i2 == 1) {
            this.f562a.h += this.j.g();
            View M = M();
            c cVar = this.f562a;
            if (!this.k) {
                i4 = 1;
            }
            cVar.e = i4;
            this.f562a.d = d(M) + this.f562a.e;
            this.f562a.b = this.j.b(M);
            c2 = this.j.b(M) - this.j.d();
        } else {
            View L = L();
            this.f562a.h += this.j.c();
            c cVar2 = this.f562a;
            if (!this.k) {
                i5 = -1;
            }
            cVar2.e = i5;
            this.f562a.d = d(L) + this.f562a.e;
            this.f562a.b = this.j.a(L);
            c2 = (-this.j.a(L)) + this.j.c();
        }
        this.f562a.c = i3;
        if (z) {
            this.f562a.c -= c2;
        }
        this.f562a.g = c2;
    }

    /* access modifiers changed from: package-private */
    public boolean k() {
        return this.j.h() == 0 && this.j.e() == 0;
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.t tVar, c cVar, RecyclerView.i.a aVar) {
        int i2 = cVar.d;
        if (i2 >= 0 && i2 < tVar.e()) {
            aVar.b(i2, Math.max(0, cVar.g));
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(int i2, RecyclerView.i.a aVar) {
        boolean z;
        int i3;
        if (this.n == null || !this.n.a()) {
            K();
            z = this.k;
            if (this.l == -1) {
                i3 = z ? i2 - 1 : 0;
            } else {
                i3 = this.l;
            }
        } else {
            boolean z2 = this.n.c;
            i3 = this.n.f563a;
            z = z2;
        }
        int i4 = z ? -1 : 1;
        for (int i5 = 0; i5 < this.h && i3 >= 0 && i3 < i2; i5++) {
            aVar.b(i3, 0);
            i3 += i4;
        }
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(int i2, int i3, RecyclerView.t tVar, RecyclerView.i.a aVar) {
        if (this.i != 0) {
            i2 = i3;
        }
        if (v() != 0 && i2 != 0) {
            i();
            a(i2 > 0 ? 1 : -1, Math.abs(i2), true, tVar);
            a(tVar, this.f562a, aVar);
        }
    }

    /* access modifiers changed from: package-private */
    public int c(int i2, RecyclerView.p pVar, RecyclerView.t tVar) {
        if (v() == 0 || i2 == 0) {
            return 0;
        }
        this.f562a.f566a = true;
        i();
        int i3 = i2 > 0 ? 1 : -1;
        int abs = Math.abs(i2);
        a(i3, abs, true, tVar);
        int a2 = this.f562a.g + a(pVar, this.f562a, tVar, false);
        if (a2 < 0) {
            return 0;
        }
        if (abs > a2) {
            i2 = i3 * a2;
        }
        this.j.a(-i2);
        this.f562a.j = i2;
        return i2;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public void a(String str) {
        if (this.n == null) {
            super.a(str);
        }
    }

    private void a(RecyclerView.p pVar, int i2, int i3) {
        if (i2 != i3) {
            if (i3 > i2) {
                for (int i4 = i3 - 1; i4 >= i2; i4--) {
                    a(i4, pVar);
                }
                return;
            }
            while (i2 > i3) {
                a(i2, pVar);
                i2--;
            }
        }
    }

    private void a(RecyclerView.p pVar, int i2) {
        if (i2 >= 0) {
            int v = v();
            if (this.k) {
                for (int i3 = v - 1; i3 >= 0; i3--) {
                    View i4 = i(i3);
                    if (this.j.b(i4) > i2 || this.j.c(i4) > i2) {
                        a(pVar, v - 1, i3);
                        return;
                    }
                }
                return;
            }
            for (int i5 = 0; i5 < v; i5++) {
                View i6 = i(i5);
                if (this.j.b(i6) > i2 || this.j.c(i6) > i2) {
                    a(pVar, 0, i5);
                    return;
                }
            }
        }
    }

    private void b(RecyclerView.p pVar, int i2) {
        int v = v();
        if (i2 >= 0) {
            int e2 = this.j.e() - i2;
            if (this.k) {
                for (int i3 = 0; i3 < v; i3++) {
                    View i4 = i(i3);
                    if (this.j.a(i4) < e2 || this.j.d(i4) < e2) {
                        a(pVar, 0, i3);
                        return;
                    }
                }
                return;
            }
            for (int i5 = v - 1; i5 >= 0; i5--) {
                View i6 = i(i5);
                if (this.j.a(i6) < e2 || this.j.d(i6) < e2) {
                    a(pVar, v - 1, i5);
                    return;
                }
            }
        }
    }

    private void a(RecyclerView.p pVar, c cVar) {
        if (cVar.f566a && !cVar.l) {
            if (cVar.f == -1) {
                b(pVar, cVar.g);
            } else {
                a(pVar, cVar.g);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a(RecyclerView.p pVar, c cVar, RecyclerView.t tVar, boolean z) {
        int i2 = cVar.c;
        if (cVar.g != Integer.MIN_VALUE) {
            if (cVar.c < 0) {
                cVar.g += cVar.c;
            }
            a(pVar, cVar);
        }
        int i3 = cVar.c + cVar.h;
        b bVar = this.g;
        while (true) {
            if ((!cVar.l && i3 <= 0) || !cVar.a(tVar)) {
                break;
            }
            bVar.a();
            a(pVar, tVar, cVar, bVar);
            if (!bVar.b) {
                cVar.b += bVar.f565a * cVar.f;
                if (!bVar.c || this.f562a.k != null || !tVar.a()) {
                    cVar.c -= bVar.f565a;
                    i3 -= bVar.f565a;
                }
                if (cVar.g != Integer.MIN_VALUE) {
                    cVar.g += bVar.f565a;
                    if (cVar.c < 0) {
                        cVar.g += cVar.c;
                    }
                    a(pVar, cVar);
                }
                if (z && bVar.d) {
                    break;
                }
            } else {
                break;
            }
        }
        return i2 - cVar.c;
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.p pVar, RecyclerView.t tVar, c cVar, b bVar) {
        boolean z;
        int B;
        int f2;
        int i2;
        int i3;
        int f3;
        View a2 = cVar.a(pVar);
        if (a2 == null) {
            bVar.b = true;
            return;
        }
        RecyclerView.j jVar = (RecyclerView.j) a2.getLayoutParams();
        if (cVar.k == null) {
            if (this.k == (cVar.f == -1)) {
                b(a2);
            } else {
                b(a2, 0);
            }
        } else {
            boolean z2 = this.k;
            if (cVar.f == -1) {
                z = true;
            } else {
                z = false;
            }
            if (z2 == z) {
                a(a2);
            } else {
                a(a2, 0);
            }
        }
        a(a2, 0, 0);
        bVar.f565a = this.j.e(a2);
        if (this.i == 1) {
            if (h()) {
                f3 = y() - C();
                i2 = f3 - this.j.f(a2);
            } else {
                i2 = A();
                f3 = this.j.f(a2) + i2;
            }
            if (cVar.f == -1) {
                f2 = cVar.b;
                B = cVar.b - bVar.f565a;
                i3 = f3;
            } else {
                B = cVar.b;
                f2 = bVar.f565a + cVar.b;
                i3 = f3;
            }
        } else {
            B = B();
            f2 = B + this.j.f(a2);
            if (cVar.f == -1) {
                int i4 = cVar.b;
                i2 = cVar.b - bVar.f565a;
                i3 = i4;
            } else {
                i2 = cVar.b;
                i3 = cVar.b + bVar.f565a;
            }
        }
        a(a2, i2, B, i3, f2);
        if (jVar.d() || jVar.e()) {
            bVar.c = true;
        }
        bVar.d = a2.hasFocusable();
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v7.widget.RecyclerView.i
    public boolean l() {
        return (x() == 1073741824 || w() == 1073741824 || !J()) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public int f(int i2) {
        int i3 = Integer.MIN_VALUE;
        int i4 = 1;
        switch (i2) {
            case 1:
                return (this.i == 1 || !h()) ? -1 : 1;
            case 2:
                if (this.i == 1) {
                    return 1;
                }
                return !h() ? 1 : -1;
            case 17:
                return this.i != 0 ? Integer.MIN_VALUE : -1;
            case 33:
                return this.i != 1 ? Integer.MIN_VALUE : -1;
            case 66:
                if (this.i != 0) {
                    i4 = Integer.MIN_VALUE;
                }
                return i4;
            case 130:
                if (this.i == 1) {
                    i3 = 1;
                }
                return i3;
            default:
                return Integer.MIN_VALUE;
        }
    }

    private View L() {
        return i(this.k ? v() - 1 : 0);
    }

    private View M() {
        return i(this.k ? 0 : v() - 1);
    }

    private View a(boolean z, boolean z2) {
        if (this.k) {
            return a(v() - 1, -1, z, z2);
        }
        return a(0, v(), z, z2);
    }

    private View b(boolean z, boolean z2) {
        if (this.k) {
            return a(0, v(), z, z2);
        }
        return a(v() - 1, -1, z, z2);
    }

    private View f(RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.k) {
            return h(pVar, tVar);
        }
        return i(pVar, tVar);
    }

    private View g(RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.k) {
            return i(pVar, tVar);
        }
        return h(pVar, tVar);
    }

    private View h(RecyclerView.p pVar, RecyclerView.t tVar) {
        return a(pVar, tVar, 0, v(), tVar.e());
    }

    private View i(RecyclerView.p pVar, RecyclerView.t tVar) {
        return a(pVar, tVar, v() - 1, -1, tVar.e());
    }

    /* access modifiers changed from: package-private */
    public View a(RecyclerView.p pVar, RecyclerView.t tVar, int i2, int i3, int i4) {
        View view;
        i();
        int c2 = this.j.c();
        int d2 = this.j.d();
        int i5 = i3 > i2 ? 1 : -1;
        View view2 = null;
        View view3 = null;
        while (i2 != i3) {
            View i6 = i(i2);
            int d3 = d(i6);
            if (d3 >= 0 && d3 < i4) {
                if (((RecyclerView.j) i6.getLayoutParams()).d()) {
                    if (view3 == null) {
                        view = view2;
                        view3 = i6;
                        i2 += i5;
                        view2 = view;
                    }
                } else if (this.j.a(i6) < d2 && this.j.b(i6) >= c2) {
                    return i6;
                } else {
                    if (view2 == null) {
                        view = i6;
                        i2 += i5;
                        view2 = view;
                    }
                }
            }
            view = view2;
            i2 += i5;
            view2 = view;
        }
        if (view2 == null) {
            view2 = view3;
        }
        return view2;
    }

    private View j(RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.k) {
            return l(pVar, tVar);
        }
        return m(pVar, tVar);
    }

    private View k(RecyclerView.p pVar, RecyclerView.t tVar) {
        if (this.k) {
            return m(pVar, tVar);
        }
        return l(pVar, tVar);
    }

    private View l(RecyclerView.p pVar, RecyclerView.t tVar) {
        return b(0, v());
    }

    private View m(RecyclerView.p pVar, RecyclerView.t tVar) {
        return b(v() - 1, -1);
    }

    public int m() {
        View a2 = a(0, v(), false, true);
        if (a2 == null) {
            return -1;
        }
        return d(a2);
    }

    public int n() {
        View a2 = a(v() - 1, -1, false, true);
        if (a2 == null) {
            return -1;
        }
        return d(a2);
    }

    /* access modifiers changed from: package-private */
    public View a(int i2, int i3, boolean z, boolean z2) {
        int i4;
        int i5 = 320;
        i();
        if (z) {
            i4 = 24579;
        } else {
            i4 = 320;
        }
        if (!z2) {
            i5 = 0;
        }
        if (this.i == 0) {
            return this.r.a(i2, i3, i4, i5);
        }
        return this.s.a(i2, i3, i4, i5);
    }

    /* access modifiers changed from: package-private */
    public View b(int i2, int i3) {
        int i4;
        int i5;
        i();
        if ((i3 > i2 ? 1 : i3 < i2 ? (char) 65535 : 0) == 0) {
            return i(i2);
        }
        if (this.j.a(i(i2)) < this.j.c()) {
            i4 = 16644;
            i5 = 16388;
        } else {
            i4 = 4161;
            i5 = FragmentTransaction.TRANSIT_FRAGMENT_OPEN;
        }
        if (this.i == 0) {
            return this.r.a(i2, i3, i4, i5);
        }
        return this.s.a(i2, i3, i4, i5);
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public View a(View view, int i2, RecyclerView.p pVar, RecyclerView.t tVar) {
        View j2;
        View M;
        K();
        if (v() == 0) {
            return null;
        }
        int f2 = f(i2);
        if (f2 == Integer.MIN_VALUE) {
            return null;
        }
        i();
        i();
        a(f2, (int) (0.33333334f * ((float) this.j.f())), false, tVar);
        this.f562a.g = Integer.MIN_VALUE;
        this.f562a.f566a = false;
        a(pVar, this.f562a, tVar, true);
        if (f2 == -1) {
            j2 = k(pVar, tVar);
        } else {
            j2 = j(pVar, tVar);
        }
        if (f2 == -1) {
            M = L();
        } else {
            M = M();
        }
        if (!M.hasFocusable()) {
            return j2;
        }
        if (j2 == null) {
            return null;
        }
        return M;
    }

    @Override // android.support.v7.widget.RecyclerView.i
    public boolean b() {
        return this.n == null && this.b == this.d;
    }

    /* access modifiers changed from: package-private */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        boolean f566a = true;
        int b;
        int c;
        int d;
        int e;
        int f;
        int g;
        int h = 0;
        boolean i = false;
        int j;
        List<RecyclerView.w> k = null;
        boolean l;

        c() {
        }

        /* access modifiers changed from: package-private */
        public boolean a(RecyclerView.t tVar) {
            return this.d >= 0 && this.d < tVar.e();
        }

        /* access modifiers changed from: package-private */
        public View a(RecyclerView.p pVar) {
            if (this.k != null) {
                return b();
            }
            View c2 = pVar.c(this.d);
            this.d += this.e;
            return c2;
        }

        private View b() {
            int size = this.k.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = this.k.get(i2).f590a;
                RecyclerView.j jVar = (RecyclerView.j) view.getLayoutParams();
                if (!jVar.d() && this.d == jVar.f()) {
                    a(view);
                    return view;
                }
            }
            return null;
        }

        public void a() {
            a((View) null);
        }

        public void a(View view) {
            View b2 = b(view);
            if (b2 == null) {
                this.d = -1;
            } else {
                this.d = ((RecyclerView.j) b2.getLayoutParams()).f();
            }
        }

        public View b(View view) {
            int i2;
            int size = this.k.size();
            View view2 = null;
            int i3 = Integer.MAX_VALUE;
            int i4 = 0;
            while (i4 < size) {
                View view3 = this.k.get(i4).f590a;
                RecyclerView.j jVar = (RecyclerView.j) view3.getLayoutParams();
                if (view3 != view) {
                    if (jVar.d()) {
                        i2 = i3;
                    } else {
                        i2 = (jVar.f() - this.d) * this.e;
                        if (i2 < 0) {
                            i2 = i3;
                        } else if (i2 < i3) {
                            if (i2 == 0) {
                                return view3;
                            }
                            view2 = view3;
                        }
                    }
                    i4++;
                    i3 = i2;
                }
                i2 = i3;
                i4++;
                i3 = i2;
            }
            return view2;
        }
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* class android.support.v7.widget.LinearLayoutManager.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f563a;
        int b;
        boolean c;

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            boolean z = true;
            this.f563a = parcel.readInt();
            this.b = parcel.readInt();
            this.c = parcel.readInt() != 1 ? false : z;
        }

        public SavedState(SavedState savedState) {
            this.f563a = savedState.f563a;
            this.b = savedState.b;
            this.c = savedState.c;
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.f563a >= 0;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f563a = -1;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f563a);
            parcel.writeInt(this.b);
            parcel.writeInt(this.c ? 1 : 0);
        }
    }

    /* access modifiers changed from: package-private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        av f564a;
        int b;
        int c;
        boolean d;
        boolean e;

        a() {
            a();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.b = -1;
            this.c = Integer.MIN_VALUE;
            this.d = false;
            this.e = false;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            int c2;
            if (this.d) {
                c2 = this.f564a.d();
            } else {
                c2 = this.f564a.c();
            }
            this.c = c2;
        }

        public String toString() {
            return "AnchorInfo{mPosition=" + this.b + ", mCoordinate=" + this.c + ", mLayoutFromEnd=" + this.d + ", mValid=" + this.e + '}';
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view, RecyclerView.t tVar) {
            RecyclerView.j jVar = (RecyclerView.j) view.getLayoutParams();
            return !jVar.d() && jVar.f() >= 0 && jVar.f() < tVar.e();
        }

        public void a(View view, int i) {
            int b2 = this.f564a.b();
            if (b2 >= 0) {
                b(view, i);
                return;
            }
            this.b = i;
            if (this.d) {
                int d2 = (this.f564a.d() - b2) - this.f564a.b(view);
                this.c = this.f564a.d() - d2;
                if (d2 > 0) {
                    int e2 = this.c - this.f564a.e(view);
                    int c2 = this.f564a.c();
                    int min = e2 - (c2 + Math.min(this.f564a.a(view) - c2, 0));
                    if (min < 0) {
                        this.c = Math.min(d2, -min) + this.c;
                        return;
                    }
                    return;
                }
                return;
            }
            int a2 = this.f564a.a(view);
            int c3 = a2 - this.f564a.c();
            this.c = a2;
            if (c3 > 0) {
                int d3 = (this.f564a.d() - Math.min(0, (this.f564a.d() - b2) - this.f564a.b(view))) - (a2 + this.f564a.e(view));
                if (d3 < 0) {
                    this.c -= Math.min(c3, -d3);
                }
            }
        }

        public void b(View view, int i) {
            if (this.d) {
                this.c = this.f564a.b(view) + this.f564a.b();
            } else {
                this.c = this.f564a.a(view);
            }
            this.b = i;
        }
    }

    /* access modifiers changed from: protected */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public int f565a;
        public boolean b;
        public boolean c;
        public boolean d;

        protected b() {
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f565a = 0;
            this.b = false;
            this.c = false;
            this.d = false;
        }
    }
}
