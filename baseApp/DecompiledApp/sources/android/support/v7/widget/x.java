package android.support.v7.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.AutoSizeableTextView;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.appcompat.R;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.widget.TextView;
import java.lang.ref.WeakReference;

/* access modifiers changed from: package-private */
public class x {

    /* renamed from: a  reason: collision with root package name */
    private final TextView f730a;
    private bi b;
    private bi c;
    private bi d;
    private bi e;
    private bi f;
    private bi g;
    @NonNull
    private final z h;
    private int i = 0;
    private Typeface j;
    private boolean k;

    x(TextView textView) {
        this.f730a = textView;
        this.h = new z(this.f730a);
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"NewApi"})
    public void a(AttributeSet attributeSet, int i2) {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        ColorStateList colorStateList3;
        boolean z;
        boolean z2;
        Context context = this.f730a.getContext();
        k a2 = k.a();
        bk a3 = bk.a(context, attributeSet, R.styleable.AppCompatTextHelper, i2, 0);
        int g2 = a3.g(R.styleable.AppCompatTextHelper_android_textAppearance, -1);
        if (a3.g(R.styleable.AppCompatTextHelper_android_drawableLeft)) {
            this.b = a(context, a2, a3.g(R.styleable.AppCompatTextHelper_android_drawableLeft, 0));
        }
        if (a3.g(R.styleable.AppCompatTextHelper_android_drawableTop)) {
            this.c = a(context, a2, a3.g(R.styleable.AppCompatTextHelper_android_drawableTop, 0));
        }
        if (a3.g(R.styleable.AppCompatTextHelper_android_drawableRight)) {
            this.d = a(context, a2, a3.g(R.styleable.AppCompatTextHelper_android_drawableRight, 0));
        }
        if (a3.g(R.styleable.AppCompatTextHelper_android_drawableBottom)) {
            this.e = a(context, a2, a3.g(R.styleable.AppCompatTextHelper_android_drawableBottom, 0));
        }
        if (Build.VERSION.SDK_INT >= 17) {
            if (a3.g(R.styleable.AppCompatTextHelper_android_drawableStart)) {
                this.f = a(context, a2, a3.g(R.styleable.AppCompatTextHelper_android_drawableStart, 0));
            }
            if (a3.g(R.styleable.AppCompatTextHelper_android_drawableEnd)) {
                this.g = a(context, a2, a3.g(R.styleable.AppCompatTextHelper_android_drawableEnd, 0));
            }
        }
        a3.a();
        boolean z3 = this.f730a.getTransformationMethod() instanceof PasswordTransformationMethod;
        if (g2 != -1) {
            bk a4 = bk.a(context, g2, R.styleable.TextAppearance);
            if (z3 || !a4.g(R.styleable.TextAppearance_textAllCaps)) {
                z = false;
                z2 = false;
            } else {
                z2 = a4.a(R.styleable.TextAppearance_textAllCaps, false);
                z = true;
            }
            a(context, a4);
            if (Build.VERSION.SDK_INT < 23) {
                if (a4.g(R.styleable.TextAppearance_android_textColor)) {
                    colorStateList3 = a4.e(R.styleable.TextAppearance_android_textColor);
                } else {
                    colorStateList3 = null;
                }
                if (a4.g(R.styleable.TextAppearance_android_textColorHint)) {
                    colorStateList2 = a4.e(R.styleable.TextAppearance_android_textColorHint);
                } else {
                    colorStateList2 = null;
                }
                colorStateList = a4.g(R.styleable.TextAppearance_android_textColorLink) ? a4.e(R.styleable.TextAppearance_android_textColorLink) : null;
            } else {
                colorStateList = null;
                colorStateList2 = null;
                colorStateList3 = null;
            }
            a4.a();
        } else {
            colorStateList = null;
            colorStateList2 = null;
            colorStateList3 = null;
            z = false;
            z2 = false;
        }
        bk a5 = bk.a(context, attributeSet, R.styleable.TextAppearance, i2, 0);
        if (!z3 && a5.g(R.styleable.TextAppearance_textAllCaps)) {
            z2 = a5.a(R.styleable.TextAppearance_textAllCaps, false);
            z = true;
        }
        if (Build.VERSION.SDK_INT < 23) {
            if (a5.g(R.styleable.TextAppearance_android_textColor)) {
                colorStateList3 = a5.e(R.styleable.TextAppearance_android_textColor);
            }
            if (a5.g(R.styleable.TextAppearance_android_textColorHint)) {
                colorStateList2 = a5.e(R.styleable.TextAppearance_android_textColorHint);
            }
            if (a5.g(R.styleable.TextAppearance_android_textColorLink)) {
                colorStateList = a5.e(R.styleable.TextAppearance_android_textColorLink);
            }
        }
        if (Build.VERSION.SDK_INT >= 28 && a5.g(R.styleable.TextAppearance_android_textSize) && a5.e(R.styleable.TextAppearance_android_textSize, -1) == 0) {
            this.f730a.setTextSize(0, 0.0f);
        }
        a(context, a5);
        a5.a();
        if (colorStateList3 != null) {
            this.f730a.setTextColor(colorStateList3);
        }
        if (colorStateList2 != null) {
            this.f730a.setHintTextColor(colorStateList2);
        }
        if (colorStateList != null) {
            this.f730a.setLinkTextColor(colorStateList);
        }
        if (!z3 && z) {
            a(z2);
        }
        if (this.j != null) {
            this.f730a.setTypeface(this.j, this.i);
        }
        this.h.a(attributeSet, i2);
        if (AutoSizeableTextView.PLATFORM_SUPPORTS_AUTOSIZE && this.h.a() != 0) {
            int[] e2 = this.h.e();
            if (e2.length > 0) {
                if (((float) this.f730a.getAutoSizeStepGranularity()) != -1.0f) {
                    this.f730a.setAutoSizeTextTypeUniformWithConfiguration(this.h.c(), this.h.d(), this.h.b(), 0);
                } else {
                    this.f730a.setAutoSizeTextTypeUniformWithPresetSizes(e2, 0);
                }
            }
        }
        bk a6 = bk.a(context, attributeSet, R.styleable.AppCompatTextView);
        int e3 = a6.e(R.styleable.AppCompatTextView_firstBaselineToTopHeight, -1);
        int e4 = a6.e(R.styleable.AppCompatTextView_lastBaselineToBottomHeight, -1);
        int e5 = a6.e(R.styleable.AppCompatTextView_lineHeight, -1);
        a6.a();
        if (e3 != -1) {
            TextViewCompat.setFirstBaselineToTopHeight(this.f730a, e3);
        }
        if (e4 != -1) {
            TextViewCompat.setLastBaselineToBottomHeight(this.f730a, e4);
        }
        if (e5 != -1) {
            TextViewCompat.setLineHeight(this.f730a, e5);
        }
    }

    private void a(Context context, bk bkVar) {
        String d2;
        boolean z = true;
        this.i = bkVar.a(R.styleable.TextAppearance_android_textStyle, this.i);
        if (bkVar.g(R.styleable.TextAppearance_android_fontFamily) || bkVar.g(R.styleable.TextAppearance_fontFamily)) {
            this.j = null;
            int i2 = bkVar.g(R.styleable.TextAppearance_fontFamily) ? R.styleable.TextAppearance_fontFamily : R.styleable.TextAppearance_android_fontFamily;
            if (!context.isRestricted()) {
                final WeakReference weakReference = new WeakReference(this.f730a);
                try {
                    this.j = bkVar.a(i2, this.i, new ResourcesCompat.FontCallback() {
                        /* class android.support.v7.widget.x.AnonymousClass1 */

                        @Override // android.support.v4.content.res.ResourcesCompat.FontCallback
                        public void onFontRetrieved(@NonNull Typeface typeface) {
                            x.this.a(weakReference, typeface);
                        }

                        @Override // android.support.v4.content.res.ResourcesCompat.FontCallback
                        public void onFontRetrievalFailed(int i) {
                        }
                    });
                    if (this.j != null) {
                        z = false;
                    }
                    this.k = z;
                } catch (Resources.NotFoundException | UnsupportedOperationException e2) {
                }
            }
            if (this.j == null && (d2 = bkVar.d(i2)) != null) {
                this.j = Typeface.create(d2, this.i);
            }
        } else if (bkVar.g(R.styleable.TextAppearance_android_typeface)) {
            this.k = false;
            switch (bkVar.a(R.styleable.TextAppearance_android_typeface, 1)) {
                case 1:
                    this.j = Typeface.SANS_SERIF;
                    return;
                case 2:
                    this.j = Typeface.SERIF;
                    return;
                case 3:
                    this.j = Typeface.MONOSPACE;
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(WeakReference<TextView> weakReference, Typeface typeface) {
        if (this.k) {
            this.j = typeface;
            TextView textView = weakReference.get();
            if (textView != null) {
                textView.setTypeface(typeface, this.i);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, int i2) {
        ColorStateList e2;
        bk a2 = bk.a(context, i2, R.styleable.TextAppearance);
        if (a2.g(R.styleable.TextAppearance_textAllCaps)) {
            a(a2.a(R.styleable.TextAppearance_textAllCaps, false));
        }
        if (Build.VERSION.SDK_INT < 23 && a2.g(R.styleable.TextAppearance_android_textColor) && (e2 = a2.e(R.styleable.TextAppearance_android_textColor)) != null) {
            this.f730a.setTextColor(e2);
        }
        if (a2.g(R.styleable.TextAppearance_android_textSize) && a2.e(R.styleable.TextAppearance_android_textSize, -1) == 0) {
            this.f730a.setTextSize(0, 0.0f);
        }
        a(context, a2);
        a2.a();
        if (this.j != null) {
            this.f730a.setTypeface(this.j, this.i);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f730a.setAllCaps(z);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (!(this.b == null && this.c == null && this.d == null && this.e == null)) {
            Drawable[] compoundDrawables = this.f730a.getCompoundDrawables();
            a(compoundDrawables[0], this.b);
            a(compoundDrawables[1], this.c);
            a(compoundDrawables[2], this.d);
            a(compoundDrawables[3], this.e);
        }
        if (Build.VERSION.SDK_INT < 17) {
            return;
        }
        if (this.f != null || this.g != null) {
            Drawable[] compoundDrawablesRelative = this.f730a.getCompoundDrawablesRelative();
            a(compoundDrawablesRelative[0], this.f);
            a(compoundDrawablesRelative[2], this.g);
        }
    }

    private void a(Drawable drawable, bi biVar) {
        if (drawable != null && biVar != null) {
            k.a(drawable, biVar, this.f730a.getDrawableState());
        }
    }

    private static bi a(Context context, k kVar, int i2) {
        ColorStateList b2 = kVar.b(context, i2);
        if (b2 == null) {
            return null;
        }
        bi biVar = new bi();
        biVar.d = true;
        biVar.f684a = b2;
        return biVar;
    }

    /* access modifiers changed from: package-private */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void a(boolean z, int i2, int i3, int i4, int i5) {
        if (!AutoSizeableTextView.PLATFORM_SUPPORTS_AUTOSIZE) {
            b();
        }
    }

    /* access modifiers changed from: package-private */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void a(int i2, float f2) {
        if (!AutoSizeableTextView.PLATFORM_SUPPORTS_AUTOSIZE && !c()) {
            b(i2, f2);
        }
    }

    /* access modifiers changed from: package-private */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void b() {
        this.h.f();
    }

    /* access modifiers changed from: package-private */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public boolean c() {
        return this.h.g();
    }

    private void b(int i2, float f2) {
        this.h.a(i2, f2);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.h.a(i2);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, int i5) {
        this.h.a(i2, i3, i4, i5);
    }

    /* access modifiers changed from: package-private */
    public void a(@NonNull int[] iArr, int i2) {
        this.h.a(iArr, i2);
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.h.a();
    }

    /* access modifiers changed from: package-private */
    public int e() {
        return this.h.b();
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.h.c();
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.h.d();
    }

    /* access modifiers changed from: package-private */
    public int[] h() {
        return this.h.e();
    }
}
