package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import java.lang.ref.WeakReference;

/* access modifiers changed from: package-private */
public class bj extends ax {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<Context> f685a;

    public bj(@NonNull Context context, @NonNull Resources resources) {
        super(resources);
        this.f685a = new WeakReference<>(context);
    }

    @Override // android.support.v7.widget.ax, android.content.res.Resources
    public Drawable getDrawable(int i) {
        Drawable drawable = super.getDrawable(i);
        Context context = this.f685a.get();
        if (!(drawable == null || context == null)) {
            k.a();
            k.a(context, i, drawable);
        }
        return drawable;
    }
}
