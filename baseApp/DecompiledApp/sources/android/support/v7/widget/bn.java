package android.support.v7.widget;

import android.support.annotation.RestrictTo;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;

/* access modifiers changed from: package-private */
@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class bn implements View.OnAttachStateChangeListener, View.OnHoverListener, View.OnLongClickListener {
    private static bn j;
    private static bn k;

    /* renamed from: a  reason: collision with root package name */
    private final View f690a;
    private final CharSequence b;
    private final int c;
    private final Runnable d = new Runnable() {
        /* class android.support.v7.widget.bn.AnonymousClass1 */

        public void run() {
            bn.this.a(false);
        }
    };
    private final Runnable e = new Runnable() {
        /* class android.support.v7.widget.bn.AnonymousClass2 */

        public void run() {
            bn.this.a();
        }
    };
    private int f;
    private int g;
    private bo h;
    private boolean i;

    public static void a(View view, CharSequence charSequence) {
        if (j != null && j.f690a == view) {
            a((bn) null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            if (k != null && k.f690a == view) {
                k.a();
            }
            view.setOnLongClickListener(null);
            view.setLongClickable(false);
            view.setOnHoverListener(null);
            return;
        }
        new bn(view, charSequence);
    }

    private bn(View view, CharSequence charSequence) {
        this.f690a = view;
        this.b = charSequence;
        this.c = ViewConfigurationCompat.getScaledHoverSlop(ViewConfiguration.get(this.f690a.getContext()));
        d();
        this.f690a.setOnLongClickListener(this);
        this.f690a.setOnHoverListener(this);
    }

    public boolean onLongClick(View view) {
        this.f = view.getWidth() / 2;
        this.g = view.getHeight() / 2;
        a(true);
        return true;
    }

    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.h == null || !this.i) {
            AccessibilityManager accessibilityManager = (AccessibilityManager) this.f690a.getContext().getSystemService("accessibility");
            if (!accessibilityManager.isEnabled() || !accessibilityManager.isTouchExplorationEnabled()) {
                switch (motionEvent.getAction()) {
                    case 7:
                        if (this.f690a.isEnabled() && this.h == null && a(motionEvent)) {
                            a(this);
                            break;
                        }
                    case 10:
                        d();
                        a();
                        break;
                }
            }
        }
        return false;
    }

    public void onViewAttachedToWindow(View view) {
    }

    public void onViewDetachedFromWindow(View view) {
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        long longPressTimeout;
        if (ViewCompat.isAttachedToWindow(this.f690a)) {
            a((bn) null);
            if (k != null) {
                k.a();
            }
            k = this;
            this.i = z;
            this.h = new bo(this.f690a.getContext());
            this.h.a(this.f690a, this.f, this.g, this.i, this.b);
            this.f690a.addOnAttachStateChangeListener(this);
            if (this.i) {
                longPressTimeout = 2500;
            } else if ((ViewCompat.getWindowSystemUiVisibility(this.f690a) & 1) == 1) {
                longPressTimeout = 3000 - ((long) ViewConfiguration.getLongPressTimeout());
            } else {
                longPressTimeout = 15000 - ((long) ViewConfiguration.getLongPressTimeout());
            }
            this.f690a.removeCallbacks(this.e);
            this.f690a.postDelayed(this.e, longPressTimeout);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (k == this) {
            k = null;
            if (this.h != null) {
                this.h.a();
                this.h = null;
                d();
                this.f690a.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (j == this) {
            a((bn) null);
        }
        this.f690a.removeCallbacks(this.e);
    }

    private static void a(bn bnVar) {
        if (j != null) {
            j.c();
        }
        j = bnVar;
        if (j != null) {
            j.b();
        }
    }

    private void b() {
        this.f690a.postDelayed(this.d, (long) ViewConfiguration.getLongPressTimeout());
    }

    private void c() {
        this.f690a.removeCallbacks(this.d);
    }

    private boolean a(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (Math.abs(x - this.f) <= this.c && Math.abs(y - this.g) <= this.c) {
            return false;
        }
        this.f = x;
        this.g = y;
        return true;
    }

    private void d() {
        this.f = Integer.MAX_VALUE;
        this.g = Integer.MAX_VALUE;
    }
}
