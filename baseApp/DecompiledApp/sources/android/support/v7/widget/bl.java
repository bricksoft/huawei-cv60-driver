package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.a;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.o;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class bl implements ah {

    /* renamed from: a  reason: collision with root package name */
    Toolbar f687a;
    CharSequence b;
    Window.Callback c;
    boolean d;
    private int e;
    private View f;
    private View g;
    private Drawable h;
    private Drawable i;
    private Drawable j;
    private boolean k;
    private CharSequence l;
    private CharSequence m;
    private ActionMenuPresenter n;
    private int o;
    private int p;
    private Drawable q;

    public bl(Toolbar toolbar, boolean z) {
        this(toolbar, z, R.string.abc_action_bar_up_description, R.drawable.abc_ic_ab_back_material);
    }

    public bl(Toolbar toolbar, boolean z, int i2, int i3) {
        this.o = 0;
        this.p = 0;
        this.f687a = toolbar;
        this.b = toolbar.getTitle();
        this.l = toolbar.getSubtitle();
        this.k = this.b != null;
        this.j = toolbar.getNavigationIcon();
        bk a2 = bk.a(toolbar.getContext(), null, R.styleable.ActionBar, R.attr.actionBarStyle, 0);
        this.q = a2.a(R.styleable.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence c2 = a2.c(R.styleable.ActionBar_title);
            if (!TextUtils.isEmpty(c2)) {
                b(c2);
            }
            CharSequence c3 = a2.c(R.styleable.ActionBar_subtitle);
            if (!TextUtils.isEmpty(c3)) {
                c(c3);
            }
            Drawable a3 = a2.a(R.styleable.ActionBar_logo);
            if (a3 != null) {
                b(a3);
            }
            Drawable a4 = a2.a(R.styleable.ActionBar_icon);
            if (a4 != null) {
                a(a4);
            }
            if (this.j == null && this.q != null) {
                c(this.q);
            }
            c(a2.a(R.styleable.ActionBar_displayOptions, 0));
            int g2 = a2.g(R.styleable.ActionBar_customNavigationLayout, 0);
            if (g2 != 0) {
                a(LayoutInflater.from(this.f687a.getContext()).inflate(g2, (ViewGroup) this.f687a, false));
                c(this.e | 16);
            }
            int f2 = a2.f(R.styleable.ActionBar_height, 0);
            if (f2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.f687a.getLayoutParams();
                layoutParams.height = f2;
                this.f687a.setLayoutParams(layoutParams);
            }
            int d2 = a2.d(R.styleable.ActionBar_contentInsetStart, -1);
            int d3 = a2.d(R.styleable.ActionBar_contentInsetEnd, -1);
            if (d2 >= 0 || d3 >= 0) {
                this.f687a.a(Math.max(d2, 0), Math.max(d3, 0));
            }
            int g3 = a2.g(R.styleable.ActionBar_titleTextStyle, 0);
            if (g3 != 0) {
                this.f687a.a(this.f687a.getContext(), g3);
            }
            int g4 = a2.g(R.styleable.ActionBar_subtitleTextStyle, 0);
            if (g4 != 0) {
                this.f687a.b(this.f687a.getContext(), g4);
            }
            int g5 = a2.g(R.styleable.ActionBar_popupTheme, 0);
            if (g5 != 0) {
                this.f687a.setPopupTheme(g5);
            }
        } else {
            this.e = r();
        }
        a2.a();
        e(i2);
        this.m = this.f687a.getNavigationContentDescription();
        this.f687a.setNavigationOnClickListener(new View.OnClickListener() {
            /* class android.support.v7.widget.bl.AnonymousClass1 */

            /* renamed from: a  reason: collision with root package name */
            final a f688a = new a(bl.this.f687a.getContext(), 0, 16908332, 0, 0, bl.this.b);

            public void onClick(View view) {
                if (bl.this.c != null && bl.this.d) {
                    bl.this.c.onMenuItemSelected(0, this.f688a);
                }
            }
        });
    }

    public void e(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.f687a.getNavigationContentDescription())) {
                f(this.p);
            }
        }
    }

    private int r() {
        if (this.f687a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.f687a.getNavigationIcon();
        return 15;
    }

    @Override // android.support.v7.widget.ah
    public ViewGroup a() {
        return this.f687a;
    }

    @Override // android.support.v7.widget.ah
    public Context b() {
        return this.f687a.getContext();
    }

    @Override // android.support.v7.widget.ah
    public boolean c() {
        return this.f687a.g();
    }

    @Override // android.support.v7.widget.ah
    public void d() {
        this.f687a.h();
    }

    @Override // android.support.v7.widget.ah
    public void a(Window.Callback callback) {
        this.c = callback;
    }

    @Override // android.support.v7.widget.ah
    public void a(CharSequence charSequence) {
        if (!this.k) {
            e(charSequence);
        }
    }

    @Override // android.support.v7.widget.ah
    public CharSequence e() {
        return this.f687a.getTitle();
    }

    public void b(CharSequence charSequence) {
        this.k = true;
        e(charSequence);
    }

    private void e(CharSequence charSequence) {
        this.b = charSequence;
        if ((this.e & 8) != 0) {
            this.f687a.setTitle(charSequence);
        }
    }

    public void c(CharSequence charSequence) {
        this.l = charSequence;
        if ((this.e & 8) != 0) {
            this.f687a.setSubtitle(charSequence);
        }
    }

    @Override // android.support.v7.widget.ah
    public void f() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @Override // android.support.v7.widget.ah
    public void g() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @Override // android.support.v7.widget.ah
    public void a(int i2) {
        a(i2 != 0 ? android.support.v7.a.a.a.b(b(), i2) : null);
    }

    @Override // android.support.v7.widget.ah
    public void a(Drawable drawable) {
        this.h = drawable;
        s();
    }

    @Override // android.support.v7.widget.ah
    public void b(int i2) {
        b(i2 != 0 ? android.support.v7.a.a.a.b(b(), i2) : null);
    }

    public void b(Drawable drawable) {
        this.i = drawable;
        s();
    }

    private void s() {
        Drawable drawable = null;
        if ((this.e & 2) != 0) {
            if ((this.e & 1) != 0) {
                drawable = this.i != null ? this.i : this.h;
            } else {
                drawable = this.h;
            }
        }
        this.f687a.setLogo(drawable);
    }

    @Override // android.support.v7.widget.ah
    public boolean h() {
        return this.f687a.a();
    }

    @Override // android.support.v7.widget.ah
    public boolean i() {
        return this.f687a.b();
    }

    @Override // android.support.v7.widget.ah
    public boolean j() {
        return this.f687a.c();
    }

    @Override // android.support.v7.widget.ah
    public boolean k() {
        return this.f687a.d();
    }

    @Override // android.support.v7.widget.ah
    public boolean l() {
        return this.f687a.e();
    }

    @Override // android.support.v7.widget.ah
    public void m() {
        this.d = true;
    }

    @Override // android.support.v7.widget.ah
    public void a(Menu menu, o.a aVar) {
        if (this.n == null) {
            this.n = new ActionMenuPresenter(this.f687a.getContext());
            this.n.a(R.id.action_menu_presenter);
        }
        this.n.a(aVar);
        this.f687a.a((h) menu, this.n);
    }

    @Override // android.support.v7.widget.ah
    public void n() {
        this.f687a.f();
    }

    @Override // android.support.v7.widget.ah
    public int o() {
        return this.e;
    }

    @Override // android.support.v7.widget.ah
    public void c(int i2) {
        int i3 = this.e ^ i2;
        this.e = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    u();
                }
                t();
            }
            if ((i3 & 3) != 0) {
                s();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.f687a.setTitle(this.b);
                    this.f687a.setSubtitle(this.l);
                } else {
                    this.f687a.setTitle((CharSequence) null);
                    this.f687a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && this.g != null) {
                if ((i2 & 16) != 0) {
                    this.f687a.addView(this.g);
                } else {
                    this.f687a.removeView(this.g);
                }
            }
        }
    }

    @Override // android.support.v7.widget.ah
    public void a(bc bcVar) {
        if (this.f != null && this.f.getParent() == this.f687a) {
            this.f687a.removeView(this.f);
        }
        this.f = bcVar;
        if (bcVar != null && this.o == 2) {
            this.f687a.addView(this.f, 0);
            Toolbar.b bVar = (Toolbar.b) this.f.getLayoutParams();
            bVar.width = -2;
            bVar.height = -2;
            bVar.f461a = 8388691;
            bcVar.setAllowCollapse(true);
        }
    }

    @Override // android.support.v7.widget.ah
    public void a(boolean z) {
        this.f687a.setCollapsible(z);
    }

    @Override // android.support.v7.widget.ah
    public void b(boolean z) {
    }

    @Override // android.support.v7.widget.ah
    public int p() {
        return this.o;
    }

    public void a(View view) {
        if (!(this.g == null || (this.e & 16) == 0)) {
            this.f687a.removeView(this.g);
        }
        this.g = view;
        if (view != null && (this.e & 16) != 0) {
            this.f687a.addView(this.g);
        }
    }

    @Override // android.support.v7.widget.ah
    public ViewPropertyAnimatorCompat a(final int i2, long j2) {
        return ViewCompat.animate(this.f687a).alpha(i2 == 0 ? 1.0f : 0.0f).setDuration(j2).setListener(new ViewPropertyAnimatorListenerAdapter() {
            /* class android.support.v7.widget.bl.AnonymousClass2 */
            private boolean c = false;

            @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
            public void onAnimationStart(View view) {
                bl.this.f687a.setVisibility(0);
            }

            @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
            public void onAnimationEnd(View view) {
                if (!this.c) {
                    bl.this.f687a.setVisibility(i2);
                }
            }

            @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
            public void onAnimationCancel(View view) {
                this.c = true;
            }
        });
    }

    public void c(Drawable drawable) {
        this.j = drawable;
        t();
    }

    private void t() {
        if ((this.e & 4) != 0) {
            this.f687a.setNavigationIcon(this.j != null ? this.j : this.q);
        } else {
            this.f687a.setNavigationIcon((Drawable) null);
        }
    }

    public void d(CharSequence charSequence) {
        this.m = charSequence;
        u();
    }

    public void f(int i2) {
        d(i2 == 0 ? null : b().getString(i2));
    }

    private void u() {
        if ((this.e & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.m)) {
            this.f687a.setNavigationContentDescription(this.p);
        } else {
            this.f687a.setNavigationContentDescription(this.m);
        }
    }

    @Override // android.support.v7.widget.ah
    public void d(int i2) {
        this.f687a.setVisibility(i2);
    }

    @Override // android.support.v7.widget.ah
    public void a(o.a aVar, h.a aVar2) {
        this.f687a.a(aVar, aVar2);
    }

    @Override // android.support.v7.widget.ah
    public Menu q() {
        return this.f687a.getMenu();
    }
}
