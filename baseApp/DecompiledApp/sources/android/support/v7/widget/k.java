package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.RestrictTo;
import android.support.graphics.drawable.i;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.support.v4.util.LruCache;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public final class k {

    /* renamed from: a  reason: collision with root package name */
    private static final PorterDuff.Mode f712a = PorterDuff.Mode.SRC_IN;
    private static k b;
    private static final c c = new c(6);
    private static final int[] d = {R.drawable.abc_textfield_search_default_mtrl_alpha, R.drawable.abc_textfield_default_mtrl_alpha, R.drawable.abc_ab_share_pack_mtrl_alpha};
    private static final int[] e = {R.drawable.abc_ic_commit_search_api_mtrl_alpha, R.drawable.abc_seekbar_tick_mark_material, R.drawable.abc_ic_menu_share_mtrl_alpha, R.drawable.abc_ic_menu_copy_mtrl_am_alpha, R.drawable.abc_ic_menu_cut_mtrl_alpha, R.drawable.abc_ic_menu_selectall_mtrl_alpha, R.drawable.abc_ic_menu_paste_mtrl_am_alpha};
    private static final int[] f = {R.drawable.abc_textfield_activated_mtrl_alpha, R.drawable.abc_textfield_search_activated_mtrl_alpha, R.drawable.abc_cab_background_top_mtrl_alpha, R.drawable.abc_text_cursor_material, R.drawable.abc_text_select_handle_left_mtrl_dark, R.drawable.abc_text_select_handle_middle_mtrl_dark, R.drawable.abc_text_select_handle_right_mtrl_dark, R.drawable.abc_text_select_handle_left_mtrl_light, R.drawable.abc_text_select_handle_middle_mtrl_light, R.drawable.abc_text_select_handle_right_mtrl_light};
    private static final int[] g = {R.drawable.abc_popup_background_mtrl_mult, R.drawable.abc_cab_background_internal_bg, R.drawable.abc_menu_hardkey_panel_mtrl_mult};
    private static final int[] h = {R.drawable.abc_tab_indicator_material, R.drawable.abc_textfield_search_material};
    private static final int[] i = {R.drawable.abc_btn_check_material, R.drawable.abc_btn_radio_material};
    private WeakHashMap<Context, SparseArrayCompat<ColorStateList>> j;
    private ArrayMap<String, d> k;
    private SparseArrayCompat<String> l;
    private final WeakHashMap<Context, LongSparseArray<WeakReference<Drawable.ConstantState>>> m = new WeakHashMap<>(0);
    private TypedValue n;
    private boolean o;

    /* access modifiers changed from: private */
    public interface d {
        Drawable a(@NonNull Context context, @NonNull XmlPullParser xmlPullParser, @NonNull AttributeSet attributeSet, @Nullable Resources.Theme theme);
    }

    public static synchronized k a() {
        k kVar;
        synchronized (k.class) {
            if (b == null) {
                b = new k();
                a(b);
            }
            kVar = b;
        }
        return kVar;
    }

    private static void a(@NonNull k kVar) {
        if (Build.VERSION.SDK_INT < 24) {
            kVar.a("vector", new e());
            kVar.a("animated-vector", new b());
            kVar.a("animated-selector", new a());
        }
    }

    public synchronized Drawable a(@NonNull Context context, @DrawableRes int i2) {
        return a(context, i2, false);
    }

    /* access modifiers changed from: package-private */
    public synchronized Drawable a(@NonNull Context context, @DrawableRes int i2, boolean z) {
        Drawable d2;
        e(context);
        d2 = d(context, i2);
        if (d2 == null) {
            d2 = c(context, i2);
        }
        if (d2 == null) {
            d2 = ContextCompat.getDrawable(context, i2);
        }
        if (d2 != null) {
            d2 = a(context, i2, z, d2);
        }
        if (d2 != null) {
            aj.a(d2);
        }
        return d2;
    }

    private static long a(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    private Drawable c(@NonNull Context context, @DrawableRes int i2) {
        if (this.n == null) {
            this.n = new TypedValue();
        }
        TypedValue typedValue = this.n;
        context.getResources().getValue(i2, typedValue, true);
        long a2 = a(typedValue);
        Drawable a3 = a(context, a2);
        if (a3 == null) {
            if (i2 == R.drawable.abc_cab_background_top_material) {
                a3 = new LayerDrawable(new Drawable[]{a(context, R.drawable.abc_cab_background_internal_bg), a(context, R.drawable.abc_cab_background_top_mtrl_alpha)});
            }
            if (a3 != null) {
                a3.setChangingConfigurations(typedValue.changingConfigurations);
                a(context, a2, a3);
            }
        }
        return a3;
    }

    private Drawable a(@NonNull Context context, @DrawableRes int i2, boolean z, @NonNull Drawable drawable) {
        ColorStateList b2 = b(context, i2);
        if (b2 != null) {
            if (aj.b(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable wrap = DrawableCompat.wrap(drawable);
            DrawableCompat.setTintList(wrap, b2);
            PorterDuff.Mode a2 = a(i2);
            if (a2 == null) {
                return wrap;
            }
            DrawableCompat.setTintMode(wrap, a2);
            return wrap;
        } else if (i2 == R.drawable.abc_seekbar_track_material) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            a(layerDrawable.findDrawableByLayerId(16908288), bf.a(context, R.attr.colorControlNormal), f712a);
            a(layerDrawable.findDrawableByLayerId(16908303), bf.a(context, R.attr.colorControlNormal), f712a);
            a(layerDrawable.findDrawableByLayerId(16908301), bf.a(context, R.attr.colorControlActivated), f712a);
            return drawable;
        } else if (i2 == R.drawable.abc_ratingbar_material || i2 == R.drawable.abc_ratingbar_indicator_material || i2 == R.drawable.abc_ratingbar_small_material) {
            LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
            a(layerDrawable2.findDrawableByLayerId(16908288), bf.c(context, R.attr.colorControlNormal), f712a);
            a(layerDrawable2.findDrawableByLayerId(16908303), bf.a(context, R.attr.colorControlActivated), f712a);
            a(layerDrawable2.findDrawableByLayerId(16908301), bf.a(context, R.attr.colorControlActivated), f712a);
            return drawable;
        } else if (a(context, i2, drawable) || !z) {
            return drawable;
        } else {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable d(@android.support.annotation.NonNull android.content.Context r10, @android.support.annotation.DrawableRes int r11) {
        /*
        // Method dump skipped, instructions count: 194
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.k.d(android.content.Context, int):android.graphics.drawable.Drawable");
    }

    private synchronized Drawable a(@NonNull Context context, long j2) {
        Drawable drawable;
        LongSparseArray<WeakReference<Drawable.ConstantState>> longSparseArray = this.m.get(context);
        if (longSparseArray == null) {
            drawable = null;
        } else {
            WeakReference<Drawable.ConstantState> weakReference = longSparseArray.get(j2);
            if (weakReference != null) {
                Drawable.ConstantState constantState = weakReference.get();
                if (constantState != null) {
                    drawable = constantState.newDrawable(context.getResources());
                } else {
                    longSparseArray.delete(j2);
                }
            }
            drawable = null;
        }
        return drawable;
    }

    private synchronized boolean a(@NonNull Context context, long j2, @NonNull Drawable drawable) {
        boolean z;
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState != null) {
            LongSparseArray<WeakReference<Drawable.ConstantState>> longSparseArray = this.m.get(context);
            if (longSparseArray == null) {
                longSparseArray = new LongSparseArray<>();
                this.m.put(context, longSparseArray);
            }
            longSparseArray.put(j2, new WeakReference<>(constantState));
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public synchronized Drawable a(@NonNull Context context, @NonNull bp bpVar, @DrawableRes int i2) {
        Drawable drawable;
        Drawable d2 = d(context, i2);
        if (d2 == null) {
            d2 = bpVar.a(i2);
        }
        if (d2 != null) {
            drawable = a(context, i2, false, d2);
        } else {
            drawable = null;
        }
        return drawable;
    }

    static boolean a(@NonNull Context context, @DrawableRes int i2, @NonNull Drawable drawable) {
        int i3;
        boolean z;
        int i4 = 16842801;
        PorterDuff.Mode mode = f712a;
        if (a(d, i2)) {
            i4 = R.attr.colorControlNormal;
            i3 = -1;
            z = true;
        } else if (a(f, i2)) {
            i4 = R.attr.colorControlActivated;
            i3 = -1;
            z = true;
        } else if (a(g, i2)) {
            mode = PorterDuff.Mode.MULTIPLY;
            i3 = -1;
            z = true;
        } else if (i2 == R.drawable.abc_list_divider_mtrl_alpha) {
            i4 = 16842800;
            i3 = Math.round(40.8f);
            z = true;
        } else if (i2 == R.drawable.abc_dialog_material_background) {
            i3 = -1;
            z = true;
        } else {
            i3 = -1;
            i4 = 0;
            z = false;
        }
        if (!z) {
            return false;
        }
        if (aj.b(drawable)) {
            drawable = drawable.mutate();
        }
        drawable.setColorFilter(a(bf.a(context, i4), mode));
        if (i3 == -1) {
            return true;
        }
        drawable.setAlpha(i3);
        return true;
    }

    private void a(@NonNull String str, @NonNull d dVar) {
        if (this.k == null) {
            this.k = new ArrayMap<>();
        }
        this.k.put(str, dVar);
    }

    private static boolean a(int[] iArr, int i2) {
        for (int i3 : iArr) {
            if (i3 == i2) {
                return true;
            }
        }
        return false;
    }

    static PorterDuff.Mode a(int i2) {
        if (i2 == R.drawable.abc_switch_thumb_material) {
            return PorterDuff.Mode.MULTIPLY;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public synchronized ColorStateList b(@NonNull Context context, @DrawableRes int i2) {
        ColorStateList e2;
        e2 = e(context, i2);
        if (e2 == null) {
            if (i2 == R.drawable.abc_edit_text_material) {
                e2 = android.support.v7.a.a.a.a(context, R.color.abc_tint_edittext);
            } else if (i2 == R.drawable.abc_switch_track_mtrl_alpha) {
                e2 = android.support.v7.a.a.a.a(context, R.color.abc_tint_switch_track);
            } else if (i2 == R.drawable.abc_switch_thumb_material) {
                e2 = d(context);
            } else if (i2 == R.drawable.abc_btn_default_mtrl_shape) {
                e2 = a(context);
            } else if (i2 == R.drawable.abc_btn_borderless_material) {
                e2 = b(context);
            } else if (i2 == R.drawable.abc_btn_colored_material) {
                e2 = c(context);
            } else if (i2 == R.drawable.abc_spinner_mtrl_am_alpha || i2 == R.drawable.abc_spinner_textfield_background_material) {
                e2 = android.support.v7.a.a.a.a(context, R.color.abc_tint_spinner);
            } else if (a(e, i2)) {
                e2 = bf.b(context, R.attr.colorControlNormal);
            } else if (a(h, i2)) {
                e2 = android.support.v7.a.a.a.a(context, R.color.abc_tint_default);
            } else if (a(i, i2)) {
                e2 = android.support.v7.a.a.a.a(context, R.color.abc_tint_btn_checkable);
            } else if (i2 == R.drawable.abc_seekbar_thumb_material) {
                e2 = android.support.v7.a.a.a.a(context, R.color.abc_tint_seek_thumb);
            }
            if (e2 != null) {
                a(context, i2, e2);
            }
        }
        return e2;
    }

    private ColorStateList e(@NonNull Context context, @DrawableRes int i2) {
        if (this.j == null) {
            return null;
        }
        SparseArrayCompat<ColorStateList> sparseArrayCompat = this.j.get(context);
        if (sparseArrayCompat != null) {
            return sparseArrayCompat.get(i2);
        }
        return null;
    }

    private void a(@NonNull Context context, @DrawableRes int i2, @NonNull ColorStateList colorStateList) {
        if (this.j == null) {
            this.j = new WeakHashMap<>();
        }
        SparseArrayCompat<ColorStateList> sparseArrayCompat = this.j.get(context);
        if (sparseArrayCompat == null) {
            sparseArrayCompat = new SparseArrayCompat<>();
            this.j.put(context, sparseArrayCompat);
        }
        sparseArrayCompat.append(i2, colorStateList);
    }

    private ColorStateList a(@NonNull Context context) {
        return f(context, bf.a(context, R.attr.colorButtonNormal));
    }

    private ColorStateList b(@NonNull Context context) {
        return f(context, 0);
    }

    private ColorStateList c(@NonNull Context context) {
        return f(context, bf.a(context, R.attr.colorAccent));
    }

    private ColorStateList f(@NonNull Context context, @ColorInt int i2) {
        int a2 = bf.a(context, R.attr.colorControlHighlight);
        int c2 = bf.c(context, R.attr.colorButtonNormal);
        return new ColorStateList(new int[][]{bf.f682a, bf.d, bf.b, bf.h}, new int[]{c2, ColorUtils.compositeColors(a2, i2), ColorUtils.compositeColors(a2, i2), i2});
    }

    private ColorStateList d(Context context) {
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        ColorStateList b2 = bf.b(context, R.attr.colorSwitchThumbNormal);
        if (b2 == null || !b2.isStateful()) {
            iArr[0] = bf.f682a;
            iArr2[0] = bf.c(context, R.attr.colorSwitchThumbNormal);
            iArr[1] = bf.e;
            iArr2[1] = bf.a(context, R.attr.colorControlActivated);
            iArr[2] = bf.h;
            iArr2[2] = bf.a(context, R.attr.colorSwitchThumbNormal);
        } else {
            iArr[0] = bf.f682a;
            iArr2[0] = b2.getColorForState(iArr[0], 0);
            iArr[1] = bf.e;
            iArr2[1] = bf.a(context, R.attr.colorControlActivated);
            iArr[2] = bf.h;
            iArr2[2] = b2.getDefaultColor();
        }
        return new ColorStateList(iArr, iArr2);
    }

    /* access modifiers changed from: private */
    public static class c extends LruCache<Integer, PorterDuffColorFilter> {
        public c(int i) {
            super(i);
        }

        /* access modifiers changed from: package-private */
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
            return (PorterDuffColorFilter) get(Integer.valueOf(b(i, mode)));
        }

        /* access modifiers changed from: package-private */
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) put(Integer.valueOf(b(i, mode)), porterDuffColorFilter);
        }

        private static int b(int i, PorterDuff.Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }
    }

    static void a(Drawable drawable, bi biVar, int[] iArr) {
        if (!aj.b(drawable) || drawable.mutate() == drawable) {
            if (biVar.d || biVar.c) {
                drawable.setColorFilter(a(biVar.d ? biVar.f684a : null, biVar.c ? biVar.b : f712a, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("AppCompatDrawableManag", "Mutated drawable is not the same instance as the input.");
    }

    private static PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return a(colorStateList.getColorForState(iArr, 0), mode);
    }

    public static synchronized PorterDuffColorFilter a(int i2, PorterDuff.Mode mode) {
        PorterDuffColorFilter a2;
        synchronized (k.class) {
            a2 = c.a(i2, mode);
            if (a2 == null) {
                a2 = new PorterDuffColorFilter(i2, mode);
                c.a(i2, mode, a2);
            }
        }
        return a2;
    }

    private static void a(Drawable drawable, int i2, PorterDuff.Mode mode) {
        if (aj.b(drawable)) {
            drawable = drawable.mutate();
        }
        if (mode == null) {
            mode = f712a;
        }
        drawable.setColorFilter(a(i2, mode));
    }

    private void e(@NonNull Context context) {
        if (!this.o) {
            this.o = true;
            Drawable a2 = a(context, R.drawable.abc_vector_test);
            if (a2 == null || !a(a2)) {
                this.o = false;
                throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    private static boolean a(@NonNull Drawable drawable) {
        return (drawable instanceof i) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }

    /* access modifiers changed from: private */
    public static class e implements d {
        e() {
        }

        @Override // android.support.v7.widget.k.d
        public Drawable a(@NonNull Context context, @NonNull XmlPullParser xmlPullParser, @NonNull AttributeSet attributeSet, @Nullable Resources.Theme theme) {
            try {
                return i.a(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    /* access modifiers changed from: private */
    public static class b implements d {
        b() {
        }

        @Override // android.support.v7.widget.k.d
        public Drawable a(@NonNull Context context, @NonNull XmlPullParser xmlPullParser, @NonNull AttributeSet attributeSet, @Nullable Resources.Theme theme) {
            try {
                return android.support.graphics.drawable.c.a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    @RequiresApi(11)
    public static class a implements d {
        a() {
        }

        @Override // android.support.v7.widget.k.d
        public Drawable a(@NonNull Context context, @NonNull XmlPullParser xmlPullParser, @NonNull AttributeSet attributeSet, @Nullable Resources.Theme theme) {
            try {
                return android.support.v7.b.a.a.a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AsldcInflateDelegate", "Exception while inflating <animated-selector>", e);
                return null;
            }
        }
    }
}
