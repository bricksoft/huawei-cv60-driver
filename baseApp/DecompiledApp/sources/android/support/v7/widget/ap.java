package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

class ap {

    /* renamed from: a  reason: collision with root package name */
    boolean f655a = true;
    int b;
    int c;
    int d;
    int e;
    int f = 0;
    int g = 0;
    boolean h;
    boolean i;

    ap() {
    }

    /* access modifiers changed from: package-private */
    public boolean a(RecyclerView.t tVar) {
        return this.c >= 0 && this.c < tVar.e();
    }

    /* access modifiers changed from: package-private */
    public View a(RecyclerView.p pVar) {
        View c2 = pVar.c(this.c);
        this.c += this.d;
        return c2;
    }

    public String toString() {
        return "LayoutState{mAvailable=" + this.b + ", mCurrentPosition=" + this.c + ", mItemDirection=" + this.d + ", mLayoutDirection=" + this.e + ", mStartLine=" + this.f + ", mEndLine=" + this.g + '}';
    }
}
