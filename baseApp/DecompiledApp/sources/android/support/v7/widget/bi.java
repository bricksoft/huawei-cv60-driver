package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* access modifiers changed from: package-private */
public class bi {

    /* renamed from: a  reason: collision with root package name */
    public ColorStateList f684a;
    public PorterDuff.Mode b;
    public boolean c;
    public boolean d;

    bi() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f684a = null;
        this.d = false;
        this.b = null;
        this.c = false;
    }
}
