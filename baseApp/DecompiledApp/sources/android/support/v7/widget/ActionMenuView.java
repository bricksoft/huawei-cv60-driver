package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.annotation.StyleRes;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.p;
import android.support.v7.widget.aq;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import com.google.android.exoplayer.C;

public class ActionMenuView extends aq implements h.b, p {

    /* renamed from: a  reason: collision with root package name */
    h.a f545a;
    e b;
    private h c;
    private Context d;
    private int e;
    private boolean f;
    private ActionMenuPresenter g;
    private o.a h;
    private boolean i;
    private int j;
    private int k;
    private int l;

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public interface a {
        boolean c();

        boolean d();
    }

    public interface e {
        boolean a(MenuItem menuItem);
    }

    public ActionMenuView(Context context) {
        this(context, null);
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBaselineAligned(false);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.k = (int) (56.0f * f2);
        this.l = (int) (f2 * 4.0f);
        this.d = context;
        this.e = 0;
    }

    public void setPopupTheme(@StyleRes int i2) {
        if (this.e != i2) {
            this.e = i2;
            if (i2 == 0) {
                this.d = getContext();
            } else {
                this.d = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public int getPopupTheme() {
        return this.e;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void setPresenter(ActionMenuPresenter actionMenuPresenter) {
        this.g = actionMenuPresenter;
        this.g.a(this);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.g != null) {
            this.g.a(false);
            if (this.g.j()) {
                this.g.g();
                this.g.f();
            }
        }
    }

    public void setOnMenuItemClickListener(e eVar) {
        this.b = eVar;
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.widget.aq
    public void onMeasure(int i2, int i3) {
        boolean z = this.i;
        this.i = View.MeasureSpec.getMode(i2) == 1073741824;
        if (z != this.i) {
            this.j = 0;
        }
        int size = View.MeasureSpec.getSize(i2);
        if (!(!this.i || this.c == null || size == this.j)) {
            this.j = size;
            this.c.a(true);
        }
        int childCount = getChildCount();
        if (!this.i || childCount <= 0) {
            for (int i4 = 0; i4 < childCount; i4++) {
                c cVar = (c) getChildAt(i4).getLayoutParams();
                cVar.rightMargin = 0;
                cVar.leftMargin = 0;
            }
            super.onMeasure(i2, i3);
            return;
        }
        c(i2, i3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x0262  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01c2  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01d1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c(int r33, int r34) {
        /*
        // Method dump skipped, instructions count: 776
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActionMenuView.c(int, int):void");
    }

    static int a(View view, int i2, int i3, int i4, int i5) {
        boolean z;
        int i6;
        boolean z2 = false;
        c cVar = (c) view.getLayoutParams();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i4) - i5, View.MeasureSpec.getMode(i4));
        ActionMenuItemView actionMenuItemView = view instanceof ActionMenuItemView ? (ActionMenuItemView) view : null;
        if (actionMenuItemView == null || !actionMenuItemView.b()) {
            z = false;
        } else {
            z = true;
        }
        if (i3 <= 0 || (z && i3 < 2)) {
            i6 = 0;
        } else {
            view.measure(View.MeasureSpec.makeMeasureSpec(i2 * i3, Integer.MIN_VALUE), makeMeasureSpec);
            int measuredWidth = view.getMeasuredWidth();
            i6 = measuredWidth / i2;
            if (measuredWidth % i2 != 0) {
                i6++;
            }
            if (z && i6 < 2) {
                i6 = 2;
            }
        }
        if (!cVar.f546a && z) {
            z2 = true;
        }
        cVar.d = z2;
        cVar.b = i6;
        view.measure(View.MeasureSpec.makeMeasureSpec(i6 * i2, C.ENCODING_PCM_32BIT), makeMeasureSpec);
        return i6;
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.widget.aq
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int width;
        int i9;
        if (!this.i) {
            super.onLayout(z, i2, i3, i4, i5);
            return;
        }
        int childCount = getChildCount();
        int i10 = (i5 - i3) / 2;
        int dividerWidth = getDividerWidth();
        int i11 = 0;
        int i12 = 0;
        int paddingRight = ((i4 - i2) - getPaddingRight()) - getPaddingLeft();
        boolean z2 = false;
        boolean a2 = bs.a(this);
        int i13 = 0;
        while (i13 < childCount) {
            View childAt = getChildAt(i13);
            if (childAt.getVisibility() == 8) {
                i8 = paddingRight;
            } else {
                c cVar = (c) childAt.getLayoutParams();
                if (cVar.f546a) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (a(i13)) {
                        measuredWidth += dividerWidth;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a2) {
                        i9 = cVar.leftMargin + getPaddingLeft();
                        width = i9 + measuredWidth;
                    } else {
                        width = (getWidth() - getPaddingRight()) - cVar.rightMargin;
                        i9 = width - measuredWidth;
                    }
                    int i14 = i10 - (measuredHeight / 2);
                    childAt.layout(i9, i14, width, measuredHeight + i14);
                    i8 = paddingRight - measuredWidth;
                    z2 = true;
                } else {
                    int measuredWidth2 = childAt.getMeasuredWidth() + cVar.leftMargin + cVar.rightMargin;
                    int i15 = i11 + measuredWidth2;
                    i8 = paddingRight - measuredWidth2;
                    if (a(i13)) {
                        i15 += dividerWidth;
                    }
                    i12++;
                    i11 = i15;
                }
            }
            i13++;
            paddingRight = i8;
        }
        if (childCount != 1 || z2) {
            int i16 = i12 - (z2 ? 0 : 1);
            int max = Math.max(0, i16 > 0 ? paddingRight / i16 : 0);
            if (a2) {
                int width2 = getWidth() - getPaddingRight();
                int i17 = 0;
                while (i17 < childCount) {
                    View childAt2 = getChildAt(i17);
                    c cVar2 = (c) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() == 8) {
                        i7 = width2;
                    } else if (cVar2.f546a) {
                        i7 = width2;
                    } else {
                        int i18 = width2 - cVar2.rightMargin;
                        int measuredWidth3 = childAt2.getMeasuredWidth();
                        int measuredHeight2 = childAt2.getMeasuredHeight();
                        int i19 = i10 - (measuredHeight2 / 2);
                        childAt2.layout(i18 - measuredWidth3, i19, i18, measuredHeight2 + i19);
                        i7 = i18 - ((cVar2.leftMargin + measuredWidth3) + max);
                    }
                    i17++;
                    width2 = i7;
                }
                return;
            }
            int paddingLeft = getPaddingLeft();
            int i20 = 0;
            while (i20 < childCount) {
                View childAt3 = getChildAt(i20);
                c cVar3 = (c) childAt3.getLayoutParams();
                if (childAt3.getVisibility() == 8) {
                    i6 = paddingLeft;
                } else if (cVar3.f546a) {
                    i6 = paddingLeft;
                } else {
                    int i21 = paddingLeft + cVar3.leftMargin;
                    int measuredWidth4 = childAt3.getMeasuredWidth();
                    int measuredHeight3 = childAt3.getMeasuredHeight();
                    int i22 = i10 - (measuredHeight3 / 2);
                    childAt3.layout(i21, i22, i21 + measuredWidth4, measuredHeight3 + i22);
                    i6 = cVar3.rightMargin + measuredWidth4 + max + i21;
                }
                i20++;
                paddingLeft = i6;
            }
            return;
        }
        View childAt4 = getChildAt(0);
        int measuredWidth5 = childAt4.getMeasuredWidth();
        int measuredHeight4 = childAt4.getMeasuredHeight();
        int i23 = ((i4 - i2) / 2) - (measuredWidth5 / 2);
        int i24 = i10 - (measuredHeight4 / 2);
        childAt4.layout(i23, i24, measuredWidth5 + i23, measuredHeight4 + i24);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        i();
    }

    public void setOverflowIcon(@Nullable Drawable drawable) {
        getMenu();
        this.g.a(drawable);
    }

    @Nullable
    public Drawable getOverflowIcon() {
        getMenu();
        return this.g.e();
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public boolean a() {
        return this.f;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void setOverflowReserved(boolean z) {
        this.f = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public c j() {
        c cVar = new c(-2, -2);
        cVar.h = 16;
        return cVar;
    }

    /* renamed from: a */
    public c generateLayoutParams(AttributeSet attributeSet) {
        return new c(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public c generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams == null) {
            return j();
        }
        c cVar = layoutParams instanceof c ? new c((c) layoutParams) : new c(layoutParams);
        if (cVar.h > 0) {
            return cVar;
        }
        cVar.h = 16;
        return cVar;
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.widget.aq
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams != null && (layoutParams instanceof c);
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public c c() {
        c b2 = j();
        b2.f546a = true;
        return b2;
    }

    @Override // android.support.v7.view.menu.h.b
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public boolean a(j jVar) {
        return this.c.a(jVar, 0);
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public int getWindowAnimations() {
        return 0;
    }

    @Override // android.support.v7.view.menu.p
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void a(h hVar) {
        this.c = hVar;
    }

    public Menu getMenu() {
        if (this.c == null) {
            Context context = getContext();
            this.c = new h(context);
            this.c.a(new d());
            this.g = new ActionMenuPresenter(context);
            this.g.b(true);
            this.g.a(this.h != null ? this.h : new b());
            this.c.a(this.g, this.d);
            this.g.a(this);
        }
        return this.c;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void a(o.a aVar, h.a aVar2) {
        this.h = aVar;
        this.f545a = aVar2;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public h d() {
        return this.c;
    }

    public boolean e() {
        return this.g != null && this.g.f();
    }

    public boolean f() {
        return this.g != null && this.g.g();
    }

    public boolean g() {
        return this.g != null && this.g.j();
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public boolean h() {
        return this.g != null && this.g.k();
    }

    public void i() {
        if (this.g != null) {
            this.g.h();
        }
    }

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public boolean a(int i2) {
        boolean z = false;
        if (i2 == 0) {
            return false;
        }
        View childAt = getChildAt(i2 - 1);
        View childAt2 = getChildAt(i2);
        if (i2 < getChildCount() && (childAt instanceof a)) {
            z = false | ((a) childAt).d();
        }
        if (i2 <= 0 || !(childAt2 instanceof a)) {
            return z;
        }
        return ((a) childAt2).c() | z;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void setExpandedActionViewsExclusive(boolean z) {
        this.g.c(z);
    }

    /* access modifiers changed from: private */
    public class d implements h.a {
        d() {
        }

        @Override // android.support.v7.view.menu.h.a
        public boolean a(h hVar, MenuItem menuItem) {
            return ActionMenuView.this.b != null && ActionMenuView.this.b.a(menuItem);
        }

        @Override // android.support.v7.view.menu.h.a
        public void a(h hVar) {
            if (ActionMenuView.this.f545a != null) {
                ActionMenuView.this.f545a.a(hVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public static class b implements o.a {
        b() {
        }

        @Override // android.support.v7.view.menu.o.a
        public void a(h hVar, boolean z) {
        }

        @Override // android.support.v7.view.menu.o.a
        public boolean a(h hVar) {
            return false;
        }
    }

    public static class c extends aq.a {
        @ViewDebug.ExportedProperty

        /* renamed from: a  reason: collision with root package name */
        public boolean f546a;
        @ViewDebug.ExportedProperty
        public int b;
        @ViewDebug.ExportedProperty
        public int c;
        @ViewDebug.ExportedProperty
        public boolean d;
        @ViewDebug.ExportedProperty
        public boolean e;
        boolean f;

        public c(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public c(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public c(c cVar) {
            super(cVar);
            this.f546a = cVar.f546a;
        }

        public c(int i, int i2) {
            super(i, i2);
            this.f546a = false;
        }
    }
}
