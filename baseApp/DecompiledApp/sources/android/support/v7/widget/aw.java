package android.support.v7.widget;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

public class aw extends AccessibilityDelegateCompat {

    /* renamed from: a  reason: collision with root package name */
    final RecyclerView f668a;
    final AccessibilityDelegateCompat b = new a(this);

    public aw(@NonNull RecyclerView recyclerView) {
        this.f668a = recyclerView;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.f668a.v();
    }

    @Override // android.support.v4.view.AccessibilityDelegateCompat
    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        if (super.performAccessibilityAction(view, i, bundle)) {
            return true;
        }
        if (a() || this.f668a.getLayoutManager() == null) {
            return false;
        }
        return this.f668a.getLayoutManager().a(i, bundle);
    }

    @Override // android.support.v4.view.AccessibilityDelegateCompat
    public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
        accessibilityNodeInfoCompat.setClassName(RecyclerView.class.getName());
        if (!a() && this.f668a.getLayoutManager() != null) {
            this.f668a.getLayoutManager().a(accessibilityNodeInfoCompat);
        }
    }

    @Override // android.support.v4.view.AccessibilityDelegateCompat
    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        accessibilityEvent.setClassName(RecyclerView.class.getName());
        if ((view instanceof RecyclerView) && !a()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().a(accessibilityEvent);
            }
        }
    }

    @NonNull
    public AccessibilityDelegateCompat b() {
        return this.b;
    }

    public static class a extends AccessibilityDelegateCompat {

        /* renamed from: a  reason: collision with root package name */
        final aw f669a;

        public a(@NonNull aw awVar) {
            this.f669a = awVar;
        }

        @Override // android.support.v4.view.AccessibilityDelegateCompat
        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
            if (!this.f669a.a() && this.f669a.f668a.getLayoutManager() != null) {
                this.f669a.f668a.getLayoutManager().a(view, accessibilityNodeInfoCompat);
            }
        }

        @Override // android.support.v4.view.AccessibilityDelegateCompat
        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            if (super.performAccessibilityAction(view, i, bundle)) {
                return true;
            }
            if (this.f669a.a() || this.f669a.f668a.getLayoutManager() == null) {
                return false;
            }
            return this.f669a.f668a.getLayoutManager().a(view, i, bundle);
        }
    }
}
