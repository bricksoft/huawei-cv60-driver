package android.support.v7.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewPropertyAnimator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ai extends bd {
    private static TimeInterpolator i;

    /* renamed from: a  reason: collision with root package name */
    ArrayList<ArrayList<RecyclerView.w>> f629a = new ArrayList<>();
    ArrayList<ArrayList<b>> b = new ArrayList<>();
    ArrayList<ArrayList<a>> c = new ArrayList<>();
    ArrayList<RecyclerView.w> d = new ArrayList<>();
    ArrayList<RecyclerView.w> e = new ArrayList<>();
    ArrayList<RecyclerView.w> f = new ArrayList<>();
    ArrayList<RecyclerView.w> g = new ArrayList<>();
    private ArrayList<RecyclerView.w> j = new ArrayList<>();
    private ArrayList<RecyclerView.w> k = new ArrayList<>();
    private ArrayList<b> l = new ArrayList<>();
    private ArrayList<a> m = new ArrayList<>();

    /* access modifiers changed from: private */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public RecyclerView.w f639a;
        public int b;
        public int c;
        public int d;
        public int e;

        b(RecyclerView.w wVar, int i, int i2, int i3, int i4) {
            this.f639a = wVar;
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public RecyclerView.w f638a;
        public RecyclerView.w b;
        public int c;
        public int d;
        public int e;
        public int f;

        private a(RecyclerView.w wVar, RecyclerView.w wVar2) {
            this.f638a = wVar;
            this.b = wVar2;
        }

        a(RecyclerView.w wVar, RecyclerView.w wVar2, int i, int i2, int i3, int i4) {
            this(wVar, wVar2);
            this.c = i;
            this.d = i2;
            this.e = i3;
            this.f = i4;
        }

        public String toString() {
            return "ChangeInfo{oldHolder=" + this.f638a + ", newHolder=" + this.b + ", fromX=" + this.c + ", fromY=" + this.d + ", toX=" + this.e + ", toY=" + this.f + '}';
        }
    }

    @Override // android.support.v7.widget.RecyclerView.f
    public void a() {
        boolean z;
        boolean z2;
        boolean z3;
        long j2;
        long j3;
        boolean z4 = !this.j.isEmpty();
        if (!this.l.isEmpty()) {
            z = true;
        } else {
            z = false;
        }
        if (!this.m.isEmpty()) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!this.k.isEmpty()) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (z4 || z || z3 || z2) {
            Iterator<RecyclerView.w> it = this.j.iterator();
            while (it.hasNext()) {
                u(it.next());
            }
            this.j.clear();
            if (z) {
                final ArrayList<b> arrayList = new ArrayList<>();
                arrayList.addAll(this.l);
                this.b.add(arrayList);
                this.l.clear();
                AnonymousClass1 r8 = new Runnable() {
                    /* class android.support.v7.widget.ai.AnonymousClass1 */

                    public void run() {
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            b bVar = (b) it.next();
                            ai.this.b(bVar.f639a, bVar.b, bVar.c, bVar.d, bVar.e);
                        }
                        arrayList.clear();
                        ai.this.b.remove(arrayList);
                    }
                };
                if (z4) {
                    ViewCompat.postOnAnimationDelayed(arrayList.get(0).f639a.f590a, r8, g());
                } else {
                    r8.run();
                }
            }
            if (z2) {
                final ArrayList<a> arrayList2 = new ArrayList<>();
                arrayList2.addAll(this.m);
                this.c.add(arrayList2);
                this.m.clear();
                AnonymousClass2 r82 = new Runnable() {
                    /* class android.support.v7.widget.ai.AnonymousClass2 */

                    public void run() {
                        Iterator it = arrayList2.iterator();
                        while (it.hasNext()) {
                            ai.this.a((a) it.next());
                        }
                        arrayList2.clear();
                        ai.this.c.remove(arrayList2);
                    }
                };
                if (z4) {
                    ViewCompat.postOnAnimationDelayed(arrayList2.get(0).f638a.f590a, r82, g());
                } else {
                    r82.run();
                }
            }
            if (z3) {
                final ArrayList<RecyclerView.w> arrayList3 = new ArrayList<>();
                arrayList3.addAll(this.k);
                this.f629a.add(arrayList3);
                this.k.clear();
                AnonymousClass3 r12 = new Runnable() {
                    /* class android.support.v7.widget.ai.AnonymousClass3 */

                    public void run() {
                        Iterator it = arrayList3.iterator();
                        while (it.hasNext()) {
                            ai.this.c((RecyclerView.w) it.next());
                        }
                        arrayList3.clear();
                        ai.this.f629a.remove(arrayList3);
                    }
                };
                if (z4 || z || z2) {
                    long g2 = z4 ? g() : 0;
                    if (z) {
                        j2 = e();
                    } else {
                        j2 = 0;
                    }
                    if (z2) {
                        j3 = h();
                    } else {
                        j3 = 0;
                    }
                    ViewCompat.postOnAnimationDelayed(arrayList3.get(0).f590a, r12, g2 + Math.max(j2, j3));
                    return;
                }
                r12.run();
            }
        }
    }

    @Override // android.support.v7.widget.bd
    public boolean a(RecyclerView.w wVar) {
        v(wVar);
        this.j.add(wVar);
        return true;
    }

    private void u(final RecyclerView.w wVar) {
        final View view = wVar.f590a;
        final ViewPropertyAnimator animate = view.animate();
        this.f.add(wVar);
        animate.setDuration(g()).alpha(0.0f).setListener(new AnimatorListenerAdapter() {
            /* class android.support.v7.widget.ai.AnonymousClass4 */

            public void onAnimationStart(Animator animator) {
                ai.this.l(wVar);
            }

            public void onAnimationEnd(Animator animator) {
                animate.setListener(null);
                view.setAlpha(1.0f);
                ai.this.i(wVar);
                ai.this.f.remove(wVar);
                ai.this.c();
            }
        }).start();
    }

    @Override // android.support.v7.widget.bd
    public boolean b(RecyclerView.w wVar) {
        v(wVar);
        wVar.f590a.setAlpha(0.0f);
        this.k.add(wVar);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void c(final RecyclerView.w wVar) {
        final View view = wVar.f590a;
        final ViewPropertyAnimator animate = view.animate();
        this.d.add(wVar);
        animate.alpha(1.0f).setDuration(f()).setListener(new AnimatorListenerAdapter() {
            /* class android.support.v7.widget.ai.AnonymousClass5 */

            public void onAnimationStart(Animator animator) {
                ai.this.n(wVar);
            }

            public void onAnimationCancel(Animator animator) {
                view.setAlpha(1.0f);
            }

            public void onAnimationEnd(Animator animator) {
                animate.setListener(null);
                ai.this.k(wVar);
                ai.this.d.remove(wVar);
                ai.this.c();
            }
        }).start();
    }

    @Override // android.support.v7.widget.bd
    public boolean a(RecyclerView.w wVar, int i2, int i3, int i4, int i5) {
        View view = wVar.f590a;
        int translationX = i2 + ((int) wVar.f590a.getTranslationX());
        int translationY = i3 + ((int) wVar.f590a.getTranslationY());
        v(wVar);
        int i6 = i4 - translationX;
        int i7 = i5 - translationY;
        if (i6 == 0 && i7 == 0) {
            j(wVar);
            return false;
        }
        if (i6 != 0) {
            view.setTranslationX((float) (-i6));
        }
        if (i7 != 0) {
            view.setTranslationY((float) (-i7));
        }
        this.l.add(new b(wVar, translationX, translationY, i4, i5));
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(final RecyclerView.w wVar, int i2, int i3, int i4, int i5) {
        final View view = wVar.f590a;
        final int i6 = i4 - i2;
        final int i7 = i5 - i3;
        if (i6 != 0) {
            view.animate().translationX(0.0f);
        }
        if (i7 != 0) {
            view.animate().translationY(0.0f);
        }
        final ViewPropertyAnimator animate = view.animate();
        this.e.add(wVar);
        animate.setDuration(e()).setListener(new AnimatorListenerAdapter() {
            /* class android.support.v7.widget.ai.AnonymousClass6 */

            public void onAnimationStart(Animator animator) {
                ai.this.m(wVar);
            }

            public void onAnimationCancel(Animator animator) {
                if (i6 != 0) {
                    view.setTranslationX(0.0f);
                }
                if (i7 != 0) {
                    view.setTranslationY(0.0f);
                }
            }

            public void onAnimationEnd(Animator animator) {
                animate.setListener(null);
                ai.this.j(wVar);
                ai.this.e.remove(wVar);
                ai.this.c();
            }
        }).start();
    }

    @Override // android.support.v7.widget.bd
    public boolean a(RecyclerView.w wVar, RecyclerView.w wVar2, int i2, int i3, int i4, int i5) {
        if (wVar == wVar2) {
            return a(wVar, i2, i3, i4, i5);
        }
        float translationX = wVar.f590a.getTranslationX();
        float translationY = wVar.f590a.getTranslationY();
        float alpha = wVar.f590a.getAlpha();
        v(wVar);
        int i6 = (int) (((float) (i4 - i2)) - translationX);
        int i7 = (int) (((float) (i5 - i3)) - translationY);
        wVar.f590a.setTranslationX(translationX);
        wVar.f590a.setTranslationY(translationY);
        wVar.f590a.setAlpha(alpha);
        if (wVar2 != null) {
            v(wVar2);
            wVar2.f590a.setTranslationX((float) (-i6));
            wVar2.f590a.setTranslationY((float) (-i7));
            wVar2.f590a.setAlpha(0.0f);
        }
        this.m.add(new a(wVar, wVar2, i2, i3, i4, i5));
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(final a aVar) {
        final View view = null;
        RecyclerView.w wVar = aVar.f638a;
        final View view2 = wVar == null ? null : wVar.f590a;
        RecyclerView.w wVar2 = aVar.b;
        if (wVar2 != null) {
            view = wVar2.f590a;
        }
        if (view2 != null) {
            final ViewPropertyAnimator duration = view2.animate().setDuration(h());
            this.g.add(aVar.f638a);
            duration.translationX((float) (aVar.e - aVar.c));
            duration.translationY((float) (aVar.f - aVar.d));
            duration.alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                /* class android.support.v7.widget.ai.AnonymousClass7 */

                public void onAnimationStart(Animator animator) {
                    ai.this.b(aVar.f638a, true);
                }

                public void onAnimationEnd(Animator animator) {
                    duration.setListener(null);
                    view2.setAlpha(1.0f);
                    view2.setTranslationX(0.0f);
                    view2.setTranslationY(0.0f);
                    ai.this.a(aVar.f638a, true);
                    ai.this.g.remove(aVar.f638a);
                    ai.this.c();
                }
            }).start();
        }
        if (view != null) {
            final ViewPropertyAnimator animate = view.animate();
            this.g.add(aVar.b);
            animate.translationX(0.0f).translationY(0.0f).setDuration(h()).alpha(1.0f).setListener(new AnimatorListenerAdapter() {
                /* class android.support.v7.widget.ai.AnonymousClass8 */

                public void onAnimationStart(Animator animator) {
                    ai.this.b(aVar.b, false);
                }

                public void onAnimationEnd(Animator animator) {
                    animate.setListener(null);
                    view.setAlpha(1.0f);
                    view.setTranslationX(0.0f);
                    view.setTranslationY(0.0f);
                    ai.this.a(aVar.b, false);
                    ai.this.g.remove(aVar.b);
                    ai.this.c();
                }
            }).start();
        }
    }

    private void a(List<a> list, RecyclerView.w wVar) {
        for (int size = list.size() - 1; size >= 0; size--) {
            a aVar = list.get(size);
            if (a(aVar, wVar) && aVar.f638a == null && aVar.b == null) {
                list.remove(aVar);
            }
        }
    }

    private void b(a aVar) {
        if (aVar.f638a != null) {
            a(aVar, aVar.f638a);
        }
        if (aVar.b != null) {
            a(aVar, aVar.b);
        }
    }

    private boolean a(a aVar, RecyclerView.w wVar) {
        boolean z = false;
        if (aVar.b == wVar) {
            aVar.b = null;
        } else if (aVar.f638a != wVar) {
            return false;
        } else {
            aVar.f638a = null;
            z = true;
        }
        wVar.f590a.setAlpha(1.0f);
        wVar.f590a.setTranslationX(0.0f);
        wVar.f590a.setTranslationY(0.0f);
        a(wVar, z);
        return true;
    }

    @Override // android.support.v7.widget.RecyclerView.f
    public void d(RecyclerView.w wVar) {
        View view = wVar.f590a;
        view.animate().cancel();
        for (int size = this.l.size() - 1; size >= 0; size--) {
            if (this.l.get(size).f639a == wVar) {
                view.setTranslationY(0.0f);
                view.setTranslationX(0.0f);
                j(wVar);
                this.l.remove(size);
            }
        }
        a(this.m, wVar);
        if (this.j.remove(wVar)) {
            view.setAlpha(1.0f);
            i(wVar);
        }
        if (this.k.remove(wVar)) {
            view.setAlpha(1.0f);
            k(wVar);
        }
        for (int size2 = this.c.size() - 1; size2 >= 0; size2--) {
            ArrayList<a> arrayList = this.c.get(size2);
            a(arrayList, wVar);
            if (arrayList.isEmpty()) {
                this.c.remove(size2);
            }
        }
        for (int size3 = this.b.size() - 1; size3 >= 0; size3--) {
            ArrayList<b> arrayList2 = this.b.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (arrayList2.get(size4).f639a == wVar) {
                    view.setTranslationY(0.0f);
                    view.setTranslationX(0.0f);
                    j(wVar);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.b.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.f629a.size() - 1; size5 >= 0; size5--) {
            ArrayList<RecyclerView.w> arrayList3 = this.f629a.get(size5);
            if (arrayList3.remove(wVar)) {
                view.setAlpha(1.0f);
                k(wVar);
                if (arrayList3.isEmpty()) {
                    this.f629a.remove(size5);
                }
            }
        }
        if (this.f.remove(wVar)) {
        }
        if (this.d.remove(wVar)) {
        }
        if (this.g.remove(wVar)) {
        }
        if (this.e.remove(wVar)) {
        }
        c();
    }

    private void v(RecyclerView.w wVar) {
        if (i == null) {
            i = new ValueAnimator().getInterpolator();
        }
        wVar.f590a.animate().setInterpolator(i);
        d(wVar);
    }

    @Override // android.support.v7.widget.RecyclerView.f
    public boolean b() {
        return !this.k.isEmpty() || !this.m.isEmpty() || !this.l.isEmpty() || !this.j.isEmpty() || !this.e.isEmpty() || !this.f.isEmpty() || !this.d.isEmpty() || !this.g.isEmpty() || !this.b.isEmpty() || !this.f629a.isEmpty() || !this.c.isEmpty();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (!b()) {
            i();
        }
    }

    @Override // android.support.v7.widget.RecyclerView.f
    public void d() {
        for (int size = this.l.size() - 1; size >= 0; size--) {
            b bVar = this.l.get(size);
            View view = bVar.f639a.f590a;
            view.setTranslationY(0.0f);
            view.setTranslationX(0.0f);
            j(bVar.f639a);
            this.l.remove(size);
        }
        for (int size2 = this.j.size() - 1; size2 >= 0; size2--) {
            i(this.j.get(size2));
            this.j.remove(size2);
        }
        for (int size3 = this.k.size() - 1; size3 >= 0; size3--) {
            RecyclerView.w wVar = this.k.get(size3);
            wVar.f590a.setAlpha(1.0f);
            k(wVar);
            this.k.remove(size3);
        }
        for (int size4 = this.m.size() - 1; size4 >= 0; size4--) {
            b(this.m.get(size4));
        }
        this.m.clear();
        if (b()) {
            for (int size5 = this.b.size() - 1; size5 >= 0; size5--) {
                ArrayList<b> arrayList = this.b.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    b bVar2 = arrayList.get(size6);
                    View view2 = bVar2.f639a.f590a;
                    view2.setTranslationY(0.0f);
                    view2.setTranslationX(0.0f);
                    j(bVar2.f639a);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.b.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.f629a.size() - 1; size7 >= 0; size7--) {
                ArrayList<RecyclerView.w> arrayList2 = this.f629a.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    RecyclerView.w wVar2 = arrayList2.get(size8);
                    wVar2.f590a.setAlpha(1.0f);
                    k(wVar2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.f629a.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.c.size() - 1; size9 >= 0; size9--) {
                ArrayList<a> arrayList3 = this.c.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    b(arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.c.remove(arrayList3);
                    }
                }
            }
            a(this.f);
            a(this.e);
            a(this.d);
            a(this.g);
            i();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<RecyclerView.w> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            list.get(size).f590a.animate().cancel();
        }
    }

    @Override // android.support.v7.widget.RecyclerView.f
    public boolean a(@NonNull RecyclerView.w wVar, @NonNull List<Object> list) {
        return !list.isEmpty() || super.a(wVar, list);
    }
}
