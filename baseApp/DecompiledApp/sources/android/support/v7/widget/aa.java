package android.support.v7.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.az;

@RequiresApi(17)
class aa extends ac {
    aa() {
    }

    @Override // android.support.v7.widget.ae, android.support.v7.widget.ac
    public void a() {
        az.f672a = new az.a() {
            /* class android.support.v7.widget.aa.AnonymousClass1 */

            @Override // android.support.v7.widget.az.a
            public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
                canvas.drawRoundRect(rectF, f, f, paint);
            }
        };
    }
}
