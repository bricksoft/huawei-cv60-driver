package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/* access modifiers changed from: package-private */
public class bb {
    static int a(RecyclerView.t tVar, av avVar, View view, View view2, RecyclerView.i iVar, boolean z, boolean z2) {
        int max;
        if (iVar.v() == 0 || tVar.e() == 0 || view == null || view2 == null) {
            return 0;
        }
        int min = Math.min(iVar.d(view), iVar.d(view2));
        int max2 = Math.max(iVar.d(view), iVar.d(view2));
        if (z2) {
            max = Math.max(0, (tVar.e() - max2) - 1);
        } else {
            max = Math.max(0, min);
        }
        if (!z) {
            return max;
        }
        return Math.round((((float) max) * (((float) Math.abs(avVar.b(view2) - avVar.a(view))) / ((float) (Math.abs(iVar.d(view) - iVar.d(view2)) + 1)))) + ((float) (avVar.c() - avVar.a(view))));
    }

    static int a(RecyclerView.t tVar, av avVar, View view, View view2, RecyclerView.i iVar, boolean z) {
        if (iVar.v() == 0 || tVar.e() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(iVar.d(view) - iVar.d(view2)) + 1;
        }
        return Math.min(avVar.f(), avVar.b(view2) - avVar.a(view));
    }

    static int b(RecyclerView.t tVar, av avVar, View view, View view2, RecyclerView.i iVar, boolean z) {
        if (iVar.v() == 0 || tVar.e() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return tVar.e();
        }
        return (int) ((((float) (avVar.b(view2) - avVar.a(view))) / ((float) (Math.abs(iVar.d(view) - iVar.d(view2)) + 1))) * ((float) tVar.e()));
    }
}
