package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.Nullable;

/* access modifiers changed from: package-private */
public interface ae {
    float a(ad adVar);

    void a();

    void a(ad adVar, float f);

    void a(ad adVar, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    void a(ad adVar, @Nullable ColorStateList colorStateList);

    float b(ad adVar);

    void b(ad adVar, float f);

    float c(ad adVar);

    void c(ad adVar, float f);

    float d(ad adVar);

    float e(ad adVar);

    void g(ad adVar);

    void h(ad adVar);

    ColorStateList i(ad adVar);
}
