package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.GravityCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.n;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.p;
import android.support.v7.view.menu.s;
import android.support.v7.view.menu.u;
import android.support.v7.widget.ActionMenuView;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* access modifiers changed from: package-private */
public class ActionMenuPresenter extends android.support.v7.view.menu.b implements ActionProvider.SubUiVisibilityListener {
    private b A;
    d g;
    e h;
    a i;
    c j;
    final f k = new f();
    int l;
    private Drawable m;
    private boolean n;
    private boolean o;
    private boolean p;
    private int q;
    private int r;
    private int s;
    private boolean t;
    private boolean u;
    private boolean v;
    private boolean w;
    private int x;
    private final SparseBooleanArray y = new SparseBooleanArray();
    private View z;

    public ActionMenuPresenter(Context context) {
        super(context, R.layout.abc_action_menu_layout, R.layout.abc_action_menu_item_layout);
    }

    @Override // android.support.v7.view.menu.b, android.support.v7.view.menu.o
    public void a(@NonNull Context context, @Nullable h hVar) {
        super.a(context, hVar);
        Resources resources = context.getResources();
        android.support.v7.view.a a2 = android.support.v7.view.a.a(context);
        if (!this.p) {
            this.o = a2.b();
        }
        if (!this.v) {
            this.q = a2.c();
        }
        if (!this.t) {
            this.s = a2.a();
        }
        int i2 = this.q;
        if (this.o) {
            if (this.g == null) {
                this.g = new d(this.f506a);
                if (this.n) {
                    this.g.setImageDrawable(this.m);
                    this.m = null;
                    this.n = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.g.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i2 -= this.g.getMeasuredWidth();
        } else {
            this.g = null;
        }
        this.r = i2;
        this.x = (int) (56.0f * resources.getDisplayMetrics().density);
        this.z = null;
    }

    public void a(Configuration configuration) {
        if (!this.t) {
            this.s = android.support.v7.view.a.a(this.b).a();
        }
        if (this.c != null) {
            this.c.a(true);
        }
    }

    public void b(boolean z2) {
        this.o = z2;
        this.p = true;
    }

    public void c(boolean z2) {
        this.w = z2;
    }

    public void a(Drawable drawable) {
        if (this.g != null) {
            this.g.setImageDrawable(drawable);
            return;
        }
        this.n = true;
        this.m = drawable;
    }

    public Drawable e() {
        if (this.g != null) {
            return this.g.getDrawable();
        }
        if (this.n) {
            return this.m;
        }
        return null;
    }

    @Override // android.support.v7.view.menu.b
    public p a(ViewGroup viewGroup) {
        p pVar = this.f;
        p a2 = super.a(viewGroup);
        if (pVar != a2) {
            ((ActionMenuView) a2).setPresenter(this);
        }
        return a2;
    }

    @Override // android.support.v7.view.menu.b
    public View a(j jVar, View view, ViewGroup viewGroup) {
        View actionView = jVar.getActionView();
        if (actionView == null || jVar.m()) {
            actionView = super.a(jVar, view, viewGroup);
        }
        actionView.setVisibility(jVar.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    @Override // android.support.v7.view.menu.b
    public void a(j jVar, p.a aVar) {
        aVar.a(jVar, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.f);
        if (this.A == null) {
            this.A = new b();
        }
        actionMenuItemView.setPopupCallback(this.A);
    }

    @Override // android.support.v7.view.menu.b
    public boolean a(int i2, j jVar) {
        return jVar.i();
    }

    @Override // android.support.v7.view.menu.b, android.support.v7.view.menu.o
    public void a(boolean z2) {
        boolean z3 = true;
        boolean z4 = false;
        super.a(z2);
        ((View) this.f).requestLayout();
        if (this.c != null) {
            ArrayList<j> l2 = this.c.l();
            int size = l2.size();
            for (int i2 = 0; i2 < size; i2++) {
                ActionProvider supportActionProvider = l2.get(i2).getSupportActionProvider();
                if (supportActionProvider != null) {
                    supportActionProvider.setSubUiVisibilityListener(this);
                }
            }
        }
        ArrayList<j> m2 = this.c != null ? this.c.m() : null;
        if (this.o && m2 != null) {
            int size2 = m2.size();
            if (size2 == 1) {
                z4 = !m2.get(0).isActionViewExpanded();
            } else {
                if (size2 <= 0) {
                    z3 = false;
                }
                z4 = z3;
            }
        }
        if (z4) {
            if (this.g == null) {
                this.g = new d(this.f506a);
            }
            ViewGroup viewGroup = (ViewGroup) this.g.getParent();
            if (viewGroup != this.f) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.g);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.f;
                actionMenuView.addView(this.g, actionMenuView.c());
            }
        } else if (this.g != null && this.g.getParent() == this.f) {
            ((ViewGroup) this.f).removeView(this.g);
        }
        ((ActionMenuView) this.f).setOverflowReserved(this.o);
    }

    @Override // android.support.v7.view.menu.b
    public boolean a(ViewGroup viewGroup, int i2) {
        if (viewGroup.getChildAt(i2) == this.g) {
            return false;
        }
        return super.a(viewGroup, i2);
    }

    @Override // android.support.v7.view.menu.b, android.support.v7.view.menu.o
    public boolean a(u uVar) {
        boolean z2;
        if (!uVar.hasVisibleItems()) {
            return false;
        }
        u uVar2 = uVar;
        while (uVar2.t() != this.c) {
            uVar2 = (u) uVar2.t();
        }
        View a2 = a(uVar2.getItem());
        if (a2 == null) {
            return false;
        }
        this.l = uVar.getItem().getItemId();
        int size = uVar.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                z2 = false;
                break;
            }
            MenuItem item = uVar.getItem(i2);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i2++;
        }
        this.i = new a(this.b, uVar, a2);
        this.i.a(z2);
        this.i.a();
        super.a(uVar);
        return true;
    }

    private View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.f;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if ((childAt instanceof p.a) && ((p.a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public boolean f() {
        if (!this.o || j() || this.c == null || this.f == null || this.j != null || this.c.m().isEmpty()) {
            return false;
        }
        this.j = new c(new e(this.b, this.c, this.g, true));
        ((View) this.f).post(this.j);
        super.a((u) null);
        return true;
    }

    public boolean g() {
        if (this.j == null || this.f == null) {
            e eVar = this.h;
            if (eVar == null) {
                return false;
            }
            eVar.d();
            return true;
        }
        ((View) this.f).removeCallbacks(this.j);
        this.j = null;
        return true;
    }

    public boolean h() {
        return g() | i();
    }

    public boolean i() {
        if (this.i == null) {
            return false;
        }
        this.i.d();
        return true;
    }

    public boolean j() {
        return this.h != null && this.h.f();
    }

    public boolean k() {
        return this.j != null || j();
    }

    @Override // android.support.v7.view.menu.b, android.support.v7.view.menu.o
    public boolean a() {
        int i2;
        ArrayList<j> arrayList;
        int i3;
        int i4;
        int i5;
        boolean z2;
        int i6;
        int i7;
        boolean z3;
        int i8;
        if (this.c != null) {
            ArrayList<j> j2 = this.c.j();
            i2 = j2.size();
            arrayList = j2;
        } else {
            i2 = 0;
            arrayList = null;
        }
        int i9 = this.s;
        int i10 = this.r;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.f;
        int i11 = 0;
        int i12 = 0;
        boolean z4 = false;
        int i13 = 0;
        while (i13 < i2) {
            j jVar = arrayList.get(i13);
            if (jVar.k()) {
                i11++;
            } else if (jVar.j()) {
                i12++;
            } else {
                z4 = true;
            }
            if (!this.w || !jVar.isActionViewExpanded()) {
                i8 = i9;
            } else {
                i8 = 0;
            }
            i13++;
            i9 = i8;
        }
        if (this.o && (z4 || i11 + i12 > i9)) {
            i9--;
        }
        int i14 = i9 - i11;
        SparseBooleanArray sparseBooleanArray = this.y;
        sparseBooleanArray.clear();
        int i15 = 0;
        if (this.u) {
            i15 = i10 / this.x;
            i3 = ((i10 % this.x) / i15) + this.x;
        } else {
            i3 = 0;
        }
        int i16 = 0;
        int i17 = i15;
        int i18 = 0;
        while (i16 < i2) {
            j jVar2 = arrayList.get(i16);
            if (jVar2.k()) {
                View a2 = a(jVar2, this.z, viewGroup);
                if (this.z == null) {
                    this.z = a2;
                }
                if (this.u) {
                    i17 -= ActionMenuView.a(a2, i3, i17, makeMeasureSpec, 0);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                i5 = a2.getMeasuredWidth();
                int i19 = i10 - i5;
                if (i18 != 0) {
                    i5 = i18;
                }
                int groupId = jVar2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                jVar2.d(true);
                i4 = i17;
                i10 = i19;
            } else if (jVar2.j()) {
                int groupId2 = jVar2.getGroupId();
                boolean z5 = sparseBooleanArray.get(groupId2);
                boolean z6 = (i14 > 0 || z5) && i10 > 0 && (!this.u || i17 > 0);
                if (z6) {
                    View a3 = a(jVar2, this.z, viewGroup);
                    if (this.z == null) {
                        this.z = a3;
                    }
                    if (this.u) {
                        int a4 = ActionMenuView.a(a3, i3, i17, makeMeasureSpec, 0);
                        i6 = i17 - a4;
                        if (a4 == 0) {
                            z3 = false;
                        } else {
                            z3 = z6;
                        }
                        z6 = z3;
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                        i6 = i17;
                    }
                    int measuredWidth = a3.getMeasuredWidth();
                    i10 -= measuredWidth;
                    if (i18 == 0) {
                        i18 = measuredWidth;
                    }
                    if (this.u) {
                        z2 = z6 & (i10 >= 0);
                    } else {
                        z2 = z6 & (i10 + i18 > 0);
                    }
                } else {
                    z2 = z6;
                    i6 = i17;
                }
                if (z2 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                    i7 = i14;
                } else if (z5) {
                    sparseBooleanArray.put(groupId2, false);
                    int i20 = i14;
                    for (int i21 = 0; i21 < i16; i21++) {
                        j jVar3 = arrayList.get(i21);
                        if (jVar3.getGroupId() == groupId2) {
                            if (jVar3.i()) {
                                i20++;
                            }
                            jVar3.d(false);
                        }
                    }
                    i7 = i20;
                } else {
                    i7 = i14;
                }
                if (z2) {
                    i7--;
                }
                jVar2.d(z2);
                i4 = i6;
                i5 = i18;
                i14 = i7;
            } else {
                jVar2.d(false);
                i4 = i17;
                i5 = i18;
            }
            i16++;
            i17 = i4;
            i18 = i5;
        }
        return true;
    }

    @Override // android.support.v7.view.menu.b, android.support.v7.view.menu.o
    public void a(h hVar, boolean z2) {
        h();
        super.a(hVar, z2);
    }

    @Override // android.support.v7.view.menu.o
    public Parcelable c() {
        SavedState savedState = new SavedState();
        savedState.f537a = this.l;
        return savedState;
    }

    @Override // android.support.v7.view.menu.o
    public void a(Parcelable parcelable) {
        MenuItem findItem;
        if (parcelable instanceof SavedState) {
            SavedState savedState = (SavedState) parcelable;
            if (savedState.f537a > 0 && (findItem = this.c.findItem(savedState.f537a)) != null) {
                a((u) findItem.getSubMenu());
            }
        }
    }

    @Override // android.support.v4.view.ActionProvider.SubUiVisibilityListener
    public void onSubUiVisibilityChanged(boolean z2) {
        if (z2) {
            super.a((u) null);
        } else if (this.c != null) {
            this.c.b(false);
        }
    }

    public void a(ActionMenuView actionMenuView) {
        this.f = actionMenuView;
        actionMenuView.a(this.c);
    }

    /* access modifiers changed from: private */
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* class android.support.v7.widget.ActionMenuPresenter.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        public int f537a;

        SavedState() {
        }

        SavedState(Parcel parcel) {
            this.f537a = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f537a);
        }
    }

    /* access modifiers changed from: private */
    public class d extends AppCompatImageView implements ActionMenuView.a {
        private final float[] b = new float[2];

        public d(Context context) {
            super(context, null, R.attr.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            bm.a(this, getContentDescription());
            setOnTouchListener(new an(this, ActionMenuPresenter.this) {
                /* class android.support.v7.widget.ActionMenuPresenter.d.AnonymousClass1 */

                @Override // android.support.v7.widget.an
                public s a() {
                    if (ActionMenuPresenter.this.h == null) {
                        return null;
                    }
                    return ActionMenuPresenter.this.h.b();
                }

                @Override // android.support.v7.widget.an
                public boolean b() {
                    ActionMenuPresenter.this.f();
                    return true;
                }

                @Override // android.support.v7.widget.an
                public boolean c() {
                    if (ActionMenuPresenter.this.j != null) {
                        return false;
                    }
                    ActionMenuPresenter.this.g();
                    return true;
                }
            });
        }

        public boolean performClick() {
            if (!super.performClick()) {
                playSoundEffect(0);
                ActionMenuPresenter.this.f();
            }
            return true;
        }

        @Override // android.support.v7.widget.ActionMenuView.a
        public boolean c() {
            return false;
        }

        @Override // android.support.v7.widget.ActionMenuView.a
        public boolean d() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                DrawableCompat.setHotspotBounds(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    /* access modifiers changed from: private */
    public class e extends n {
        public e(Context context, h hVar, View view, boolean z) {
            super(context, hVar, view, z, R.attr.actionOverflowMenuStyle);
            a(GravityCompat.END);
            a(ActionMenuPresenter.this.k);
        }

        /* access modifiers changed from: protected */
        @Override // android.support.v7.view.menu.n
        public void e() {
            if (ActionMenuPresenter.this.c != null) {
                ActionMenuPresenter.this.c.close();
            }
            ActionMenuPresenter.this.h = null;
            super.e();
        }
    }

    /* access modifiers changed from: private */
    public class a extends n {
        public a(Context context, u uVar, View view) {
            super(context, uVar, view, false, R.attr.actionOverflowMenuStyle);
            if (!((j) uVar.getItem()).i()) {
                a(ActionMenuPresenter.this.g == null ? (View) ActionMenuPresenter.this.f : ActionMenuPresenter.this.g);
            }
            a(ActionMenuPresenter.this.k);
        }

        /* access modifiers changed from: protected */
        @Override // android.support.v7.view.menu.n
        public void e() {
            ActionMenuPresenter.this.i = null;
            ActionMenuPresenter.this.l = 0;
            super.e();
        }
    }

    private class f implements o.a {
        f() {
        }

        @Override // android.support.v7.view.menu.o.a
        public boolean a(h hVar) {
            if (hVar == null) {
                return false;
            }
            ActionMenuPresenter.this.l = ((u) hVar).getItem().getItemId();
            o.a d = ActionMenuPresenter.this.d();
            return d != null ? d.a(hVar) : false;
        }

        @Override // android.support.v7.view.menu.o.a
        public void a(h hVar, boolean z) {
            if (hVar instanceof u) {
                hVar.q().b(false);
            }
            o.a d = ActionMenuPresenter.this.d();
            if (d != null) {
                d.a(hVar, z);
            }
        }
    }

    /* access modifiers changed from: private */
    public class c implements Runnable {
        private e b;

        public c(e eVar) {
            this.b = eVar;
        }

        public void run() {
            if (ActionMenuPresenter.this.c != null) {
                ActionMenuPresenter.this.c.g();
            }
            View view = (View) ActionMenuPresenter.this.f;
            if (!(view == null || view.getWindowToken() == null || !this.b.c())) {
                ActionMenuPresenter.this.h = this.b;
            }
            ActionMenuPresenter.this.j = null;
        }
    }

    private class b extends ActionMenuItemView.b {
        b() {
        }

        @Override // android.support.v7.view.menu.ActionMenuItemView.b
        public s a() {
            if (ActionMenuPresenter.this.i != null) {
                return ActionMenuPresenter.this.i.b();
            }
            return null;
        }
    }
}
