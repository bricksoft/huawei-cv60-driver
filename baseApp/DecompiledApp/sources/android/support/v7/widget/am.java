package android.support.v7.widget;

import android.graphics.Rect;
import android.support.annotation.RestrictTo;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public interface am {

    public interface a {
        void a(Rect rect);
    }

    void setOnFitSystemWindowsListener(a aVar);
}
