package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.google.android.exoplayer.C;

public class aq extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    private boolean f656a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private float g;
    private boolean h;
    private int[] i;
    private int[] j;
    private Drawable k;
    private int l;
    private int m;
    private int n;
    private int o;

    public aq(Context context) {
        this(context, null);
    }

    public aq(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public aq(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f656a = true;
        this.b = -1;
        this.c = 0;
        this.e = 8388659;
        bk a2 = bk.a(context, attributeSet, R.styleable.LinearLayoutCompat, i2, 0);
        int a3 = a2.a(R.styleable.LinearLayoutCompat_android_orientation, -1);
        if (a3 >= 0) {
            setOrientation(a3);
        }
        int a4 = a2.a(R.styleable.LinearLayoutCompat_android_gravity, -1);
        if (a4 >= 0) {
            setGravity(a4);
        }
        boolean a5 = a2.a(R.styleable.LinearLayoutCompat_android_baselineAligned, true);
        if (!a5) {
            setBaselineAligned(a5);
        }
        this.g = a2.a(R.styleable.LinearLayoutCompat_android_weightSum, -1.0f);
        this.b = a2.a(R.styleable.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
        this.h = a2.a(R.styleable.LinearLayoutCompat_measureWithLargestChild, false);
        setDividerDrawable(a2.a(R.styleable.LinearLayoutCompat_divider));
        this.n = a2.a(R.styleable.LinearLayoutCompat_showDividers, 0);
        this.o = a2.e(R.styleable.LinearLayoutCompat_dividerPadding, 0);
        a2.a();
    }

    public void setShowDividers(int i2) {
        if (i2 != this.n) {
            requestLayout();
        }
        this.n = i2;
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public int getShowDividers() {
        return this.n;
    }

    public Drawable getDividerDrawable() {
        return this.k;
    }

    public void setDividerDrawable(Drawable drawable) {
        boolean z = false;
        if (drawable != this.k) {
            this.k = drawable;
            if (drawable != null) {
                this.l = drawable.getIntrinsicWidth();
                this.m = drawable.getIntrinsicHeight();
            } else {
                this.l = 0;
                this.m = 0;
            }
            if (drawable == null) {
                z = true;
            }
            setWillNotDraw(z);
            requestLayout();
        }
    }

    public void setDividerPadding(int i2) {
        this.o = i2;
    }

    public int getDividerPadding() {
        return this.o;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public int getDividerWidth() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.k != null) {
            if (this.d == 1) {
                a(canvas);
            } else {
                b(canvas);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas) {
        int bottom;
        int virtualChildCount = getVirtualChildCount();
        for (int i2 = 0; i2 < virtualChildCount; i2++) {
            View b2 = b(i2);
            if (!(b2 == null || b2.getVisibility() == 8 || !c(i2))) {
                a(canvas, (b2.getTop() - ((a) b2.getLayoutParams()).topMargin) - this.m);
            }
        }
        if (c(virtualChildCount)) {
            View b3 = b(virtualChildCount - 1);
            if (b3 == null) {
                bottom = (getHeight() - getPaddingBottom()) - this.m;
            } else {
                bottom = ((a) b3.getLayoutParams()).bottomMargin + b3.getBottom();
            }
            a(canvas, bottom);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Canvas canvas) {
        int right;
        int left;
        int virtualChildCount = getVirtualChildCount();
        boolean a2 = bs.a(this);
        for (int i2 = 0; i2 < virtualChildCount; i2++) {
            View b2 = b(i2);
            if (!(b2 == null || b2.getVisibility() == 8 || !c(i2))) {
                a aVar = (a) b2.getLayoutParams();
                if (a2) {
                    left = aVar.rightMargin + b2.getRight();
                } else {
                    left = (b2.getLeft() - aVar.leftMargin) - this.l;
                }
                b(canvas, left);
            }
        }
        if (c(virtualChildCount)) {
            View b3 = b(virtualChildCount - 1);
            if (b3 != null) {
                a aVar2 = (a) b3.getLayoutParams();
                if (a2) {
                    right = (b3.getLeft() - aVar2.leftMargin) - this.l;
                } else {
                    right = aVar2.rightMargin + b3.getRight();
                }
            } else if (a2) {
                right = getPaddingLeft();
            } else {
                right = (getWidth() - getPaddingRight()) - this.l;
            }
            b(canvas, right);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas, int i2) {
        this.k.setBounds(getPaddingLeft() + this.o, i2, (getWidth() - getPaddingRight()) - this.o, this.m + i2);
        this.k.draw(canvas);
    }

    /* access modifiers changed from: package-private */
    public void b(Canvas canvas, int i2) {
        this.k.setBounds(i2, getPaddingTop() + this.o, this.l + i2, (getHeight() - getPaddingBottom()) - this.o);
        this.k.draw(canvas);
    }

    public void setBaselineAligned(boolean z) {
        this.f656a = z;
    }

    public void setMeasureWithLargestChildEnabled(boolean z) {
        this.h = z;
    }

    public int getBaseline() {
        int i2;
        int i3;
        if (this.b < 0) {
            return super.getBaseline();
        }
        if (getChildCount() <= this.b) {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
        View childAt = getChildAt(this.b);
        int baseline = childAt.getBaseline();
        if (baseline != -1) {
            int i4 = this.c;
            if (this.d == 1 && (i3 = this.e & 112) != 48) {
                switch (i3) {
                    case 16:
                        i2 = i4 + (((((getBottom() - getTop()) - getPaddingTop()) - getPaddingBottom()) - this.f) / 2);
                        break;
                    case 80:
                        i2 = ((getBottom() - getTop()) - getPaddingBottom()) - this.f;
                        break;
                }
                return ((a) childAt.getLayoutParams()).topMargin + i2 + baseline;
            }
            i2 = i4;
            return ((a) childAt.getLayoutParams()).topMargin + i2 + baseline;
        } else if (this.b == 0) {
            return -1;
        } else {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
        }
    }

    public int getBaselineAlignedChildIndex() {
        return this.b;
    }

    public void setBaselineAlignedChildIndex(int i2) {
        if (i2 < 0 || i2 >= getChildCount()) {
            throw new IllegalArgumentException("base aligned child index out of range (0, " + getChildCount() + ")");
        }
        this.b = i2;
    }

    /* access modifiers changed from: package-private */
    public View b(int i2) {
        return getChildAt(i2);
    }

    /* access modifiers changed from: package-private */
    public int getVirtualChildCount() {
        return getChildCount();
    }

    public float getWeightSum() {
        return this.g;
    }

    public void setWeightSum(float f2) {
        this.g = Math.max(0.0f, f2);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.d == 1) {
            a(i2, i3);
        } else {
            b(i2, i3);
        }
    }

    /* access modifiers changed from: protected */
    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public boolean c(int i2) {
        if (i2 == 0) {
            return (this.n & 1) != 0;
        }
        if (i2 == getChildCount()) {
            return (this.n & 4) != 0;
        }
        if ((this.n & 2) == 0) {
            return false;
        }
        for (int i3 = i2 - 1; i3 >= 0; i3--) {
            if (getChildAt(i3).getVisibility() != 8) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        int i4;
        int i5;
        float f2;
        int i6;
        boolean z;
        int i7;
        boolean z2;
        boolean z3;
        int max;
        int i8;
        int i9;
        boolean z4;
        float f3;
        int i10;
        this.f = 0;
        int i11 = 0;
        int i12 = 0;
        int i13 = 0;
        int i14 = 0;
        boolean z5 = true;
        float f4 = 0.0f;
        int virtualChildCount = getVirtualChildCount();
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        boolean z6 = false;
        boolean z7 = false;
        int i15 = this.b;
        boolean z8 = this.h;
        int i16 = 0;
        int i17 = 0;
        while (i17 < virtualChildCount) {
            View b2 = b(i17);
            if (b2 == null) {
                this.f += d(i17);
                i9 = i16;
                z4 = z7;
                f3 = f4;
                z3 = z5;
            } else if (b2.getVisibility() == 8) {
                i17 += a(b2, i17);
                i9 = i16;
                z4 = z7;
                f3 = f4;
                z3 = z5;
            } else {
                if (c(i17)) {
                    this.f += this.m;
                }
                a aVar = (a) b2.getLayoutParams();
                float f5 = f4 + aVar.g;
                if (mode2 == 1073741824 && aVar.height == 0 && aVar.g > 0.0f) {
                    int i18 = this.f;
                    this.f = Math.max(i18, aVar.topMargin + i18 + aVar.bottomMargin);
                    z7 = true;
                } else {
                    int i19 = Integer.MIN_VALUE;
                    if (aVar.height == 0 && aVar.g > 0.0f) {
                        i19 = 0;
                        aVar.height = -2;
                    }
                    a(b2, i17, i2, 0, i3, f5 == 0.0f ? this.f : 0);
                    if (i19 != Integer.MIN_VALUE) {
                        aVar.height = i19;
                    }
                    int measuredHeight = b2.getMeasuredHeight();
                    int i20 = this.f;
                    this.f = Math.max(i20, i20 + measuredHeight + aVar.topMargin + aVar.bottomMargin + b(b2));
                    if (z8) {
                        i16 = Math.max(measuredHeight, i16);
                    }
                }
                if (i15 >= 0 && i15 == i17 + 1) {
                    this.c = this.f;
                }
                if (i17 >= i15 || aVar.g <= 0.0f) {
                    boolean z9 = false;
                    if (mode == 1073741824 || aVar.width != -1) {
                        z2 = z6;
                    } else {
                        z2 = true;
                        z9 = true;
                    }
                    int i21 = aVar.rightMargin + aVar.leftMargin;
                    int measuredWidth = b2.getMeasuredWidth() + i21;
                    i11 = Math.max(i11, measuredWidth);
                    i12 = View.combineMeasuredStates(i12, b2.getMeasuredState());
                    z3 = z5 && aVar.width == -1;
                    if (aVar.g > 0.0f) {
                        if (z9) {
                            i10 = i21;
                        } else {
                            i10 = measuredWidth;
                        }
                        i8 = Math.max(i14, i10);
                        max = i13;
                    } else {
                        if (!z9) {
                            i21 = measuredWidth;
                        }
                        max = Math.max(i13, i21);
                        i8 = i14;
                    }
                    i17 += a(b2, i17);
                    i9 = i16;
                    z4 = z7;
                    z6 = z2;
                    f3 = f5;
                    i14 = i8;
                    i13 = max;
                } else {
                    throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                }
            }
            i17++;
            i16 = i9;
            z7 = z4;
            f4 = f3;
            z5 = z3;
        }
        if (this.f > 0 && c(virtualChildCount)) {
            this.f += this.m;
        }
        if (z8 && (mode2 == Integer.MIN_VALUE || mode2 == 0)) {
            this.f = 0;
            int i22 = 0;
            while (i22 < virtualChildCount) {
                View b3 = b(i22);
                if (b3 == null) {
                    this.f += d(i22);
                    i7 = i22;
                } else if (b3.getVisibility() == 8) {
                    i7 = a(b3, i22) + i22;
                } else {
                    a aVar2 = (a) b3.getLayoutParams();
                    int i23 = this.f;
                    this.f = Math.max(i23, aVar2.bottomMargin + i23 + i16 + aVar2.topMargin + b(b3));
                    i7 = i22;
                }
                i22 = i7 + 1;
            }
        }
        this.f += getPaddingTop() + getPaddingBottom();
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(this.f, getSuggestedMinimumHeight()), i3, 0);
        int i24 = (16777215 & resolveSizeAndState) - this.f;
        if (z7 || (i24 != 0 && f4 > 0.0f)) {
            if (this.g > 0.0f) {
                f4 = this.g;
            }
            this.f = 0;
            int i25 = 0;
            float f6 = f4;
            boolean z10 = z5;
            int i26 = i13;
            int i27 = i12;
            int i28 = i11;
            while (i25 < virtualChildCount) {
                View b4 = b(i25);
                if (b4.getVisibility() == 8) {
                    f2 = f6;
                    z = z10;
                } else {
                    a aVar3 = (a) b4.getLayoutParams();
                    float f7 = aVar3.g;
                    if (f7 > 0.0f) {
                        int i29 = (int) ((((float) i24) * f7) / f6);
                        float f8 = f6 - f7;
                        i6 = i24 - i29;
                        int childMeasureSpec = getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + aVar3.leftMargin + aVar3.rightMargin, aVar3.width);
                        if (aVar3.height == 0 && mode2 == 1073741824) {
                            if (i29 <= 0) {
                                i29 = 0;
                            }
                            b4.measure(childMeasureSpec, View.MeasureSpec.makeMeasureSpec(i29, C.ENCODING_PCM_32BIT));
                        } else {
                            int measuredHeight2 = i29 + b4.getMeasuredHeight();
                            if (measuredHeight2 < 0) {
                                measuredHeight2 = 0;
                            }
                            b4.measure(childMeasureSpec, View.MeasureSpec.makeMeasureSpec(measuredHeight2, C.ENCODING_PCM_32BIT));
                        }
                        f2 = f8;
                        i27 = View.combineMeasuredStates(i27, b4.getMeasuredState() & InputDeviceCompat.SOURCE_ANY);
                    } else {
                        f2 = f6;
                        i6 = i24;
                    }
                    int i30 = aVar3.leftMargin + aVar3.rightMargin;
                    int measuredWidth2 = b4.getMeasuredWidth() + i30;
                    i28 = Math.max(i28, measuredWidth2);
                    if (!(mode != 1073741824 && aVar3.width == -1)) {
                        i30 = measuredWidth2;
                    }
                    i26 = Math.max(i26, i30);
                    boolean z11 = z10 && aVar3.width == -1;
                    int i31 = this.f;
                    this.f = Math.max(i31, aVar3.bottomMargin + b4.getMeasuredHeight() + i31 + aVar3.topMargin + b(b4));
                    i24 = i6;
                    z = z11;
                }
                i25++;
                f6 = f2;
                z10 = z;
            }
            this.f += getPaddingTop() + getPaddingBottom();
            z5 = z10;
            i5 = i26;
            i12 = i27;
            i4 = i28;
        } else {
            int max2 = Math.max(i13, i14);
            if (z8 && mode2 != 1073741824) {
                for (int i32 = 0; i32 < virtualChildCount; i32++) {
                    View b5 = b(i32);
                    if (!(b5 == null || b5.getVisibility() == 8 || ((a) b5.getLayoutParams()).g <= 0.0f)) {
                        b5.measure(View.MeasureSpec.makeMeasureSpec(b5.getMeasuredWidth(), C.ENCODING_PCM_32BIT), View.MeasureSpec.makeMeasureSpec(i16, C.ENCODING_PCM_32BIT));
                    }
                }
            }
            i5 = max2;
            i4 = i11;
        }
        if (z5 || mode == 1073741824) {
            i5 = i4;
        }
        setMeasuredDimension(View.resolveSizeAndState(Math.max(i5 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i2, i12), resolveSizeAndState);
        if (z6) {
            c(virtualChildCount, i3);
        }
    }

    private void c(int i2, int i3) {
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), C.ENCODING_PCM_32BIT);
        for (int i4 = 0; i4 < i2; i4++) {
            View b2 = b(i4);
            if (b2.getVisibility() != 8) {
                a aVar = (a) b2.getLayoutParams();
                if (aVar.width == -1) {
                    int i5 = aVar.height;
                    aVar.height = b2.getMeasuredHeight();
                    measureChildWithMargins(b2, makeMeasureSpec, 0, i3, 0);
                    aVar.height = i5;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        float f2;
        int i7;
        boolean z;
        int i8;
        float f3;
        int i9;
        int baseline;
        int i10;
        boolean z2;
        boolean z3;
        int max;
        int i11;
        int i12;
        boolean z4;
        float f4;
        int i13;
        int baseline2;
        this.f = 0;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        int i17 = 0;
        boolean z5 = true;
        float f5 = 0.0f;
        int virtualChildCount = getVirtualChildCount();
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        boolean z6 = false;
        boolean z7 = false;
        if (this.i == null || this.j == null) {
            this.i = new int[4];
            this.j = new int[4];
        }
        int[] iArr = this.i;
        int[] iArr2 = this.j;
        iArr[3] = -1;
        iArr[2] = -1;
        iArr[1] = -1;
        iArr[0] = -1;
        iArr2[3] = -1;
        iArr2[2] = -1;
        iArr2[1] = -1;
        iArr2[0] = -1;
        boolean z8 = this.f656a;
        boolean z9 = this.h;
        boolean z10 = mode == 1073741824;
        int i18 = 0;
        int i19 = 0;
        while (i19 < virtualChildCount) {
            View b2 = b(i19);
            if (b2 == null) {
                this.f += d(i19);
                i12 = i18;
                z4 = z7;
                f4 = f5;
                z3 = z5;
            } else if (b2.getVisibility() == 8) {
                i19 += a(b2, i19);
                i12 = i18;
                z4 = z7;
                f4 = f5;
                z3 = z5;
            } else {
                if (c(i19)) {
                    this.f += this.l;
                }
                a aVar = (a) b2.getLayoutParams();
                float f6 = f5 + aVar.g;
                if (mode == 1073741824 && aVar.width == 0 && aVar.g > 0.0f) {
                    if (z10) {
                        this.f += aVar.leftMargin + aVar.rightMargin;
                    } else {
                        int i20 = this.f;
                        this.f = Math.max(i20, aVar.leftMargin + i20 + aVar.rightMargin);
                    }
                    if (z8) {
                        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                        b2.measure(makeMeasureSpec, makeMeasureSpec);
                    } else {
                        z7 = true;
                    }
                } else {
                    int i21 = Integer.MIN_VALUE;
                    if (aVar.width == 0 && aVar.g > 0.0f) {
                        i21 = 0;
                        aVar.width = -2;
                    }
                    a(b2, i19, i2, f6 == 0.0f ? this.f : 0, i3, 0);
                    if (i21 != Integer.MIN_VALUE) {
                        aVar.width = i21;
                    }
                    int measuredWidth = b2.getMeasuredWidth();
                    if (z10) {
                        this.f += aVar.leftMargin + measuredWidth + aVar.rightMargin + b(b2);
                    } else {
                        int i22 = this.f;
                        this.f = Math.max(i22, i22 + measuredWidth + aVar.leftMargin + aVar.rightMargin + b(b2));
                    }
                    if (z9) {
                        i18 = Math.max(measuredWidth, i18);
                    }
                }
                boolean z11 = false;
                if (mode2 == 1073741824 || aVar.height != -1) {
                    z2 = z6;
                } else {
                    z2 = true;
                    z11 = true;
                }
                int i23 = aVar.bottomMargin + aVar.topMargin;
                int measuredHeight = b2.getMeasuredHeight() + i23;
                i15 = View.combineMeasuredStates(i15, b2.getMeasuredState());
                if (z8 && (baseline2 = b2.getBaseline()) != -1) {
                    int i24 = ((((aVar.h < 0 ? this.e : aVar.h) & 112) >> 4) & -2) >> 1;
                    iArr[i24] = Math.max(iArr[i24], baseline2);
                    iArr2[i24] = Math.max(iArr2[i24], measuredHeight - baseline2);
                }
                i14 = Math.max(i14, measuredHeight);
                z3 = z5 && aVar.height == -1;
                if (aVar.g > 0.0f) {
                    if (z11) {
                        i13 = i23;
                    } else {
                        i13 = measuredHeight;
                    }
                    i11 = Math.max(i17, i13);
                    max = i16;
                } else {
                    if (!z11) {
                        i23 = measuredHeight;
                    }
                    max = Math.max(i16, i23);
                    i11 = i17;
                }
                i19 += a(b2, i19);
                i12 = i18;
                z4 = z7;
                z6 = z2;
                f4 = f6;
                i17 = i11;
                i16 = max;
            }
            i19++;
            i18 = i12;
            z7 = z4;
            f5 = f4;
            z5 = z3;
        }
        if (this.f > 0 && c(virtualChildCount)) {
            this.f += this.l;
        }
        if (iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1) {
            i4 = i14;
        } else {
            i4 = Math.max(i14, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
        }
        if (z9 && (mode == Integer.MIN_VALUE || mode == 0)) {
            this.f = 0;
            int i25 = 0;
            while (i25 < virtualChildCount) {
                View b3 = b(i25);
                if (b3 == null) {
                    this.f += d(i25);
                    i10 = i25;
                } else if (b3.getVisibility() == 8) {
                    i10 = a(b3, i25) + i25;
                } else {
                    a aVar2 = (a) b3.getLayoutParams();
                    if (z10) {
                        this.f = aVar2.rightMargin + aVar2.leftMargin + i18 + b(b3) + this.f;
                        i10 = i25;
                    } else {
                        int i26 = this.f;
                        this.f = Math.max(i26, aVar2.rightMargin + i26 + i18 + aVar2.leftMargin + b(b3));
                        i10 = i25;
                    }
                }
                i25 = i10 + 1;
            }
        }
        this.f += getPaddingLeft() + getPaddingRight();
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(this.f, getSuggestedMinimumWidth()), i2, 0);
        int i27 = (16777215 & resolveSizeAndState) - this.f;
        if (z7 || (i27 != 0 && f5 > 0.0f)) {
            if (this.g > 0.0f) {
                f5 = this.g;
            }
            iArr[3] = -1;
            iArr[2] = -1;
            iArr[1] = -1;
            iArr[0] = -1;
            iArr2[3] = -1;
            iArr2[2] = -1;
            iArr2[1] = -1;
            iArr2[0] = -1;
            int i28 = -1;
            this.f = 0;
            int i29 = 0;
            float f7 = f5;
            boolean z12 = z5;
            int i30 = i16;
            int i31 = i15;
            while (i29 < virtualChildCount) {
                View b4 = b(i29);
                if (b4 == null) {
                    f2 = f7;
                    i7 = i27;
                    z = z12;
                    i8 = i28;
                } else if (b4.getVisibility() == 8) {
                    f2 = f7;
                    i7 = i27;
                    z = z12;
                    i8 = i28;
                } else {
                    a aVar3 = (a) b4.getLayoutParams();
                    float f8 = aVar3.g;
                    if (f8 > 0.0f) {
                        int i32 = (int) ((((float) i27) * f8) / f7);
                        f3 = f7 - f8;
                        i27 -= i32;
                        int childMeasureSpec = getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + aVar3.topMargin + aVar3.bottomMargin, aVar3.height);
                        if (aVar3.width == 0 && mode == 1073741824) {
                            if (i32 <= 0) {
                                i32 = 0;
                            }
                            b4.measure(View.MeasureSpec.makeMeasureSpec(i32, C.ENCODING_PCM_32BIT), childMeasureSpec);
                        } else {
                            int measuredWidth2 = i32 + b4.getMeasuredWidth();
                            if (measuredWidth2 < 0) {
                                measuredWidth2 = 0;
                            }
                            b4.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth2, C.ENCODING_PCM_32BIT), childMeasureSpec);
                        }
                        i31 = View.combineMeasuredStates(i31, b4.getMeasuredState() & ViewCompat.MEASURED_STATE_MASK);
                    } else {
                        f3 = f7;
                    }
                    if (z10) {
                        this.f += b4.getMeasuredWidth() + aVar3.leftMargin + aVar3.rightMargin + b(b4);
                    } else {
                        int i33 = this.f;
                        this.f = Math.max(i33, b4.getMeasuredWidth() + i33 + aVar3.leftMargin + aVar3.rightMargin + b(b4));
                    }
                    boolean z13 = mode2 != 1073741824 && aVar3.height == -1;
                    int i34 = aVar3.topMargin + aVar3.bottomMargin;
                    int measuredHeight2 = b4.getMeasuredHeight() + i34;
                    i8 = Math.max(i28, measuredHeight2);
                    if (z13) {
                        i9 = i34;
                    } else {
                        i9 = measuredHeight2;
                    }
                    i30 = Math.max(i30, i9);
                    boolean z14 = z12 && aVar3.height == -1;
                    if (z8 && (baseline = b4.getBaseline()) != -1) {
                        int i35 = ((((aVar3.h < 0 ? this.e : aVar3.h) & 112) >> 4) & -2) >> 1;
                        iArr[i35] = Math.max(iArr[i35], baseline);
                        iArr2[i35] = Math.max(iArr2[i35], measuredHeight2 - baseline);
                    }
                    f2 = f3;
                    i7 = i27;
                    z = z14;
                }
                i29++;
                f7 = f2;
                i27 = i7;
                z12 = z;
                i28 = i8;
            }
            this.f += getPaddingLeft() + getPaddingRight();
            if (!(iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1)) {
                i28 = Math.max(i28, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
            }
            z5 = z12;
            i6 = i30;
            i15 = i31;
            i5 = i28;
        } else {
            int max2 = Math.max(i16, i17);
            if (z9 && mode != 1073741824) {
                for (int i36 = 0; i36 < virtualChildCount; i36++) {
                    View b5 = b(i36);
                    if (!(b5 == null || b5.getVisibility() == 8 || ((a) b5.getLayoutParams()).g <= 0.0f)) {
                        b5.measure(View.MeasureSpec.makeMeasureSpec(i18, C.ENCODING_PCM_32BIT), View.MeasureSpec.makeMeasureSpec(b5.getMeasuredHeight(), C.ENCODING_PCM_32BIT));
                    }
                }
            }
            i6 = max2;
            i5 = i4;
        }
        if (z5 || mode2 == 1073741824) {
            i6 = i5;
        }
        setMeasuredDimension((-16777216 & i15) | resolveSizeAndState, View.resolveSizeAndState(Math.max(i6 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i3, i15 << 16));
        if (z6) {
            d(virtualChildCount, i2);
        }
    }

    private void d(int i2, int i3) {
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), C.ENCODING_PCM_32BIT);
        for (int i4 = 0; i4 < i2; i4++) {
            View b2 = b(i4);
            if (b2.getVisibility() != 8) {
                a aVar = (a) b2.getLayoutParams();
                if (aVar.height == -1) {
                    int i5 = aVar.width;
                    aVar.width = b2.getMeasuredWidth();
                    measureChildWithMargins(b2, i3, 0, makeMeasureSpec, 0);
                    aVar.width = i5;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a(View view, int i2) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int d(int i2) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void a(View view, int i2, int i3, int i4, int i5, int i6) {
        measureChildWithMargins(view, i3, i4, i5, i6);
    }

    /* access modifiers changed from: package-private */
    public int a(View view) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int b(View view) {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        if (this.d == 1) {
            a(i2, i3, i4, i5);
        } else {
            b(i2, i3, i4, i5);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, int i5) {
        int paddingTop;
        int i6;
        int i7;
        int i8;
        int paddingLeft = getPaddingLeft();
        int i9 = i4 - i2;
        int paddingRight = i9 - getPaddingRight();
        int paddingRight2 = (i9 - paddingLeft) - getPaddingRight();
        int virtualChildCount = getVirtualChildCount();
        int i10 = this.e & 112;
        int i11 = this.e & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        switch (i10) {
            case 16:
                paddingTop = getPaddingTop() + (((i5 - i3) - this.f) / 2);
                break;
            case 80:
                paddingTop = ((getPaddingTop() + i5) - i3) - this.f;
                break;
            default:
                paddingTop = getPaddingTop();
                break;
        }
        int i12 = 0;
        int i13 = paddingTop;
        while (i12 < virtualChildCount) {
            View b2 = b(i12);
            if (b2 == null) {
                i13 += d(i12);
                i6 = i12;
            } else if (b2.getVisibility() != 8) {
                int measuredWidth = b2.getMeasuredWidth();
                int measuredHeight = b2.getMeasuredHeight();
                a aVar = (a) b2.getLayoutParams();
                int i14 = aVar.h;
                if (i14 < 0) {
                    i14 = i11;
                }
                switch (GravityCompat.getAbsoluteGravity(i14, ViewCompat.getLayoutDirection(this)) & 7) {
                    case 1:
                        i7 = ((((paddingRight2 - measuredWidth) / 2) + paddingLeft) + aVar.leftMargin) - aVar.rightMargin;
                        break;
                    case 5:
                        i7 = (paddingRight - measuredWidth) - aVar.rightMargin;
                        break;
                    default:
                        i7 = paddingLeft + aVar.leftMargin;
                        break;
                }
                if (c(i12)) {
                    i8 = this.m + i13;
                } else {
                    i8 = i13;
                }
                int i15 = i8 + aVar.topMargin;
                a(b2, i7, i15 + a(b2), measuredWidth, measuredHeight);
                i13 = i15 + aVar.bottomMargin + measuredHeight + b(b2);
                i6 = a(b2, i12) + i12;
            } else {
                i6 = i12;
            }
            i12 = i6 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3, int i4, int i5) {
        int paddingLeft;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        boolean a2 = bs.a(this);
        int paddingTop = getPaddingTop();
        int i12 = i5 - i3;
        int paddingBottom = i12 - getPaddingBottom();
        int paddingBottom2 = (i12 - paddingTop) - getPaddingBottom();
        int virtualChildCount = getVirtualChildCount();
        int i13 = this.e & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        int i14 = this.e & 112;
        boolean z = this.f656a;
        int[] iArr = this.i;
        int[] iArr2 = this.j;
        switch (GravityCompat.getAbsoluteGravity(i13, ViewCompat.getLayoutDirection(this))) {
            case 1:
                paddingLeft = getPaddingLeft() + (((i4 - i2) - this.f) / 2);
                break;
            case 5:
                paddingLeft = ((getPaddingLeft() + i4) - i2) - this.f;
                break;
            default:
                paddingLeft = getPaddingLeft();
                break;
        }
        if (a2) {
            i6 = -1;
            i7 = virtualChildCount - 1;
        } else {
            i6 = 1;
            i7 = 0;
        }
        int i15 = 0;
        while (i15 < virtualChildCount) {
            int i16 = i7 + (i6 * i15);
            View b2 = b(i16);
            if (b2 == null) {
                paddingLeft += d(i16);
                i8 = i15;
            } else if (b2.getVisibility() != 8) {
                int measuredWidth = b2.getMeasuredWidth();
                int measuredHeight = b2.getMeasuredHeight();
                a aVar = (a) b2.getLayoutParams();
                if (!z || aVar.height == -1) {
                    i9 = -1;
                } else {
                    i9 = b2.getBaseline();
                }
                int i17 = aVar.h;
                if (i17 < 0) {
                    i17 = i14;
                }
                switch (i17 & 112) {
                    case 16:
                        i10 = ((((paddingBottom2 - measuredHeight) / 2) + paddingTop) + aVar.topMargin) - aVar.bottomMargin;
                        break;
                    case 48:
                        i10 = paddingTop + aVar.topMargin;
                        if (i9 != -1) {
                            i10 += iArr[1] - i9;
                            break;
                        }
                        break;
                    case 80:
                        i10 = (paddingBottom - measuredHeight) - aVar.bottomMargin;
                        if (i9 != -1) {
                            i10 -= iArr2[2] - (b2.getMeasuredHeight() - i9);
                            break;
                        }
                        break;
                    default:
                        i10 = paddingTop;
                        break;
                }
                if (c(i16)) {
                    i11 = this.l + paddingLeft;
                } else {
                    i11 = paddingLeft;
                }
                int i18 = i11 + aVar.leftMargin;
                a(b2, i18 + a(b2), i10, measuredWidth, measuredHeight);
                paddingLeft = i18 + aVar.rightMargin + measuredWidth + b(b2);
                i8 = a(b2, i16) + i15;
            } else {
                i8 = i15;
            }
            i15 = i8 + 1;
        }
    }

    private void a(View view, int i2, int i3, int i4, int i5) {
        view.layout(i2, i3, i2 + i4, i3 + i5);
    }

    public void setOrientation(int i2) {
        if (this.d != i2) {
            this.d = i2;
            requestLayout();
        }
    }

    public int getOrientation() {
        return this.d;
    }

    public void setGravity(int i2) {
        int i3;
        if (this.e != i2) {
            if ((8388615 & i2) == 0) {
                i3 = 8388611 | i2;
            } else {
                i3 = i2;
            }
            if ((i3 & 112) == 0) {
                i3 |= 48;
            }
            this.e = i3;
            requestLayout();
        }
    }

    public int getGravity() {
        return this.e;
    }

    public void setHorizontalGravity(int i2) {
        int i3 = i2 & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        if ((this.e & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK) != i3) {
            this.e = i3 | (this.e & -8388616);
            requestLayout();
        }
    }

    public void setVerticalGravity(int i2) {
        int i3 = i2 & 112;
        if ((this.e & 112) != i3) {
            this.e = i3 | (this.e & -113);
            requestLayout();
        }
    }

    /* renamed from: b */
    public a generateLayoutParams(AttributeSet attributeSet) {
        return new a(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public a generateDefaultLayoutParams() {
        if (this.d == 0) {
            return new a(-2, -2);
        }
        if (this.d == 1) {
            return new a(-1, -2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public a generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new a(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof a;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(aq.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(aq.class.getName());
    }

    public static class a extends ViewGroup.MarginLayoutParams {
        public float g;
        public int h;

        public a(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.h = -1;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.LinearLayoutCompat_Layout);
            this.g = obtainStyledAttributes.getFloat(R.styleable.LinearLayoutCompat_Layout_android_layout_weight, 0.0f);
            this.h = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_Layout_android_layout_gravity, -1);
            obtainStyledAttributes.recycle();
        }

        public a(int i, int i2) {
            super(i, i2);
            this.h = -1;
            this.g = 0.0f;
        }

        public a(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.h = -1;
        }
    }
}
