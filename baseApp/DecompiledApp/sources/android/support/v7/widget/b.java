package android.support.v7.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

/* access modifiers changed from: package-private */
public class b extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    final ActionBarContainer f673a;

    public b(ActionBarContainer actionBarContainer) {
        this.f673a = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        if (!this.f673a.d) {
            if (this.f673a.f531a != null) {
                this.f673a.f531a.draw(canvas);
            }
            if (this.f673a.b != null && this.f673a.e) {
                this.f673a.b.draw(canvas);
            }
        } else if (this.f673a.c != null) {
            this.f673a.c.draw(canvas);
        }
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public int getOpacity() {
        return 0;
    }

    @RequiresApi(21)
    public void getOutline(@NonNull Outline outline) {
        if (this.f673a.d) {
            if (this.f673a.c != null) {
                this.f673a.c.getOutline(outline);
            }
        } else if (this.f673a.f531a != null) {
            this.f673a.f531a.getOutline(outline);
        }
    }
}
