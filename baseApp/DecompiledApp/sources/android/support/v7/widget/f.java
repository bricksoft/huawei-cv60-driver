package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.view.View;

/* access modifiers changed from: package-private */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private final View f707a;
    private final k b;
    private int c = -1;
    private bi d;
    private bi e;
    private bi f;

    f(View view) {
        this.f707a = view;
        this.b = k.a();
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        bk a2 = bk.a(this.f707a.getContext(), attributeSet, R.styleable.ViewBackgroundHelper, i, 0);
        try {
            if (a2.g(R.styleable.ViewBackgroundHelper_android_background)) {
                this.c = a2.g(R.styleable.ViewBackgroundHelper_android_background, -1);
                ColorStateList b2 = this.b.b(this.f707a.getContext(), this.c);
                if (b2 != null) {
                    b(b2);
                }
            }
            if (a2.g(R.styleable.ViewBackgroundHelper_backgroundTint)) {
                ViewCompat.setBackgroundTintList(this.f707a, a2.e(R.styleable.ViewBackgroundHelper_backgroundTint));
            }
            if (a2.g(R.styleable.ViewBackgroundHelper_backgroundTintMode)) {
                ViewCompat.setBackgroundTintMode(this.f707a, aj.a(a2.a(R.styleable.ViewBackgroundHelper_backgroundTintMode, -1), null));
            }
        } finally {
            a2.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.c = i;
        b(this.b != null ? this.b.b(this.f707a.getContext(), i) : null);
        c();
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable) {
        this.c = -1;
        b((ColorStateList) null);
        c();
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (this.e == null) {
            this.e = new bi();
        }
        this.e.f684a = colorStateList;
        this.e.d = true;
        c();
    }

    /* access modifiers changed from: package-private */
    public ColorStateList a() {
        if (this.e != null) {
            return this.e.f684a;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        if (this.e == null) {
            this.e = new bi();
        }
        this.e.b = mode;
        this.e.c = true;
        c();
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode b() {
        if (this.e != null) {
            return this.e.b;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Drawable background = this.f707a.getBackground();
        if (background == null) {
            return;
        }
        if (d() && b(background)) {
            return;
        }
        if (this.e != null) {
            k.a(background, this.e, this.f707a.getDrawableState());
        } else if (this.d != null) {
            k.a(background, this.d, this.f707a.getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.d == null) {
                this.d = new bi();
            }
            this.d.f684a = colorStateList;
            this.d.d = true;
        } else {
            this.d = null;
        }
        c();
    }

    private boolean d() {
        int i = Build.VERSION.SDK_INT;
        return i > 21 ? this.d != null : i == 21;
    }

    private boolean b(@NonNull Drawable drawable) {
        if (this.f == null) {
            this.f = new bi();
        }
        bi biVar = this.f;
        biVar.a();
        ColorStateList backgroundTintList = ViewCompat.getBackgroundTintList(this.f707a);
        if (backgroundTintList != null) {
            biVar.d = true;
            biVar.f684a = backgroundTintList;
        }
        PorterDuff.Mode backgroundTintMode = ViewCompat.getBackgroundTintMode(this.f707a);
        if (backgroundTintMode != null) {
            biVar.c = true;
            biVar.b = backgroundTintMode;
        }
        if (!biVar.d && !biVar.c) {
            return false;
        }
        k.a(drawable, biVar, this.f707a.getDrawableState());
        return true;
    }
}
