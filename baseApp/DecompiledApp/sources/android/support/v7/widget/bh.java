package android.support.v7.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class bh extends ContextWrapper {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f683a = new Object();
    private static ArrayList<WeakReference<bh>> b;
    private final Resources c;
    private final Resources.Theme d;

    public static Context a(@NonNull Context context) {
        if (!b(context)) {
            return context;
        }
        synchronized (f683a) {
            if (b == null) {
                b = new ArrayList<>();
            } else {
                for (int size = b.size() - 1; size >= 0; size--) {
                    WeakReference<bh> weakReference = b.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        b.remove(size);
                    }
                }
                for (int size2 = b.size() - 1; size2 >= 0; size2--) {
                    WeakReference<bh> weakReference2 = b.get(size2);
                    bh bhVar = weakReference2 != null ? weakReference2.get() : null;
                    if (bhVar != null && bhVar.getBaseContext() == context) {
                        return bhVar;
                    }
                }
            }
            bh bhVar2 = new bh(context);
            b.add(new WeakReference<>(bhVar2));
            return bhVar2;
        }
    }

    private static boolean b(@NonNull Context context) {
        if ((context instanceof bh) || (context.getResources() instanceof bj) || (context.getResources() instanceof bp)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 21 || bp.a()) {
            return true;
        }
        return false;
    }

    private bh(@NonNull Context context) {
        super(context);
        if (bp.a()) {
            this.c = new bp(this, context.getResources());
            this.d = this.c.newTheme();
            this.d.setTo(context.getTheme());
            return;
        }
        this.c = new bj(this, context.getResources());
        this.d = null;
    }

    public Resources.Theme getTheme() {
        return this.d == null ? super.getTheme() : this.d;
    }

    public void setTheme(int i) {
        if (this.d == null) {
            super.setTheme(i);
        } else {
            this.d.applyStyle(i, true);
        }
    }

    public Resources getResources() {
        return this.c;
    }

    public AssetManager getAssets() {
        return this.c.getAssets();
    }
}
