package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.o;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public interface ah {
    ViewPropertyAnimatorCompat a(int i, long j);

    ViewGroup a();

    void a(int i);

    void a(Drawable drawable);

    void a(o.a aVar, h.a aVar2);

    void a(bc bcVar);

    void a(Menu menu, o.a aVar);

    void a(Window.Callback callback);

    void a(CharSequence charSequence);

    void a(boolean z);

    Context b();

    void b(int i);

    void b(boolean z);

    void c(int i);

    boolean c();

    void d();

    void d(int i);

    CharSequence e();

    void f();

    void g();

    boolean h();

    boolean i();

    boolean j();

    boolean k();

    boolean l();

    void m();

    void n();

    int o();

    int p();

    Menu q();
}
