package android.support.v7.widget;

import android.view.View;

/* access modifiers changed from: package-private */
public class bq {

    /* renamed from: a  reason: collision with root package name */
    final b f695a;
    a b = new a();

    /* access modifiers changed from: package-private */
    public interface b {
        int a();

        int a(View view);

        View a(int i);

        int b();

        int b(View view);
    }

    bq(b bVar) {
        this.f695a = bVar;
    }

    /* access modifiers changed from: package-private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        int f696a = 0;
        int b;
        int c;
        int d;
        int e;

        a() {
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2, int i3, int i4) {
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            this.f696a |= i;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f696a = 0;
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2) {
            if (i > i2) {
                return 1;
            }
            if (i == i2) {
                return 2;
            }
            return 4;
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            if ((this.f696a & 7) != 0 && (this.f696a & (a(this.d, this.b) << 0)) == 0) {
                return false;
            }
            if ((this.f696a & 112) != 0 && (this.f696a & (a(this.d, this.c) << 4)) == 0) {
                return false;
            }
            if ((this.f696a & 1792) != 0 && (this.f696a & (a(this.e, this.b) << 8)) == 0) {
                return false;
            }
            if ((this.f696a & 28672) == 0 || (this.f696a & (a(this.e, this.c) << 12)) != 0) {
                return true;
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public View a(int i, int i2, int i3, int i4) {
        int a2 = this.f695a.a();
        int b2 = this.f695a.b();
        int i5 = i2 > i ? 1 : -1;
        View view = null;
        while (i != i2) {
            View a3 = this.f695a.a(i);
            this.b.a(a2, b2, this.f695a.a(a3), this.f695a.b(a3));
            if (i3 != 0) {
                this.b.a();
                this.b.a(i3);
                if (this.b.b()) {
                    return a3;
                }
            }
            if (i4 != 0) {
                this.b.a();
                this.b.a(i4);
                if (this.b.b()) {
                    i += i5;
                    view = a3;
                }
            }
            a3 = view;
            i += i5;
            view = a3;
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i) {
        this.b.a(this.f695a.a(), this.f695a.b(), this.f695a.a(view), this.f695a.b(view));
        if (i == 0) {
            return false;
        }
        this.b.a();
        this.b.a(i);
        return this.b.b();
    }
}
