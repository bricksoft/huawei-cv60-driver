package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.support.v7.widget.az;

/* access modifiers changed from: package-private */
public class ac implements ae {

    /* renamed from: a  reason: collision with root package name */
    final RectF f625a = new RectF();

    ac() {
    }

    @Override // android.support.v7.widget.ae
    public void a() {
        az.f672a = new az.a() {
            /* class android.support.v7.widget.ac.AnonymousClass1 */

            @Override // android.support.v7.widget.az.a
            public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
                float f2 = 2.0f * f;
                float width = (rectF.width() - f2) - 1.0f;
                float height = (rectF.height() - f2) - 1.0f;
                if (f >= 1.0f) {
                    float f3 = f + 0.5f;
                    ac.this.f625a.set(-f3, -f3, f3, f3);
                    int save = canvas.save();
                    canvas.translate(rectF.left + f3, rectF.top + f3);
                    canvas.drawArc(ac.this.f625a, 180.0f, 90.0f, true, paint);
                    canvas.translate(width, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(ac.this.f625a, 180.0f, 90.0f, true, paint);
                    canvas.translate(height, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(ac.this.f625a, 180.0f, 90.0f, true, paint);
                    canvas.translate(width, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(ac.this.f625a, 180.0f, 90.0f, true, paint);
                    canvas.restoreToCount(save);
                    canvas.drawRect((rectF.left + f3) - 1.0f, rectF.top, 1.0f + (rectF.right - f3), rectF.top + f3, paint);
                    canvas.drawRect((rectF.left + f3) - 1.0f, rectF.bottom - f3, 1.0f + (rectF.right - f3), rectF.bottom, paint);
                }
                canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom - f, paint);
            }
        };
    }

    @Override // android.support.v7.widget.ae
    public void a(ad adVar, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        az a2 = a(context, colorStateList, f, f2, f3);
        a2.a(adVar.b());
        adVar.a(a2);
        f(adVar);
    }

    private az a(Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        return new az(context.getResources(), colorStateList, f, f2, f3);
    }

    public void f(ad adVar) {
        Rect rect = new Rect();
        j(adVar).a(rect);
        adVar.a((int) Math.ceil((double) b(adVar)), (int) Math.ceil((double) c(adVar)));
        adVar.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @Override // android.support.v7.widget.ae
    public void g(ad adVar) {
    }

    @Override // android.support.v7.widget.ae
    public void h(ad adVar) {
        j(adVar).a(adVar.b());
        f(adVar);
    }

    @Override // android.support.v7.widget.ae
    public void a(ad adVar, @Nullable ColorStateList colorStateList) {
        j(adVar).a(colorStateList);
    }

    @Override // android.support.v7.widget.ae
    public ColorStateList i(ad adVar) {
        return j(adVar).f();
    }

    @Override // android.support.v7.widget.ae
    public void a(ad adVar, float f) {
        j(adVar).a(f);
        f(adVar);
    }

    @Override // android.support.v7.widget.ae
    public float d(ad adVar) {
        return j(adVar).a();
    }

    @Override // android.support.v7.widget.ae
    public void c(ad adVar, float f) {
        j(adVar).b(f);
    }

    @Override // android.support.v7.widget.ae
    public float e(ad adVar) {
        return j(adVar).b();
    }

    @Override // android.support.v7.widget.ae
    public void b(ad adVar, float f) {
        j(adVar).c(f);
        f(adVar);
    }

    @Override // android.support.v7.widget.ae
    public float a(ad adVar) {
        return j(adVar).c();
    }

    @Override // android.support.v7.widget.ae
    public float b(ad adVar) {
        return j(adVar).d();
    }

    @Override // android.support.v7.widget.ae
    public float c(ad adVar) {
        return j(adVar).e();
    }

    private az j(ad adVar) {
        return (az) adVar.c();
    }
}
