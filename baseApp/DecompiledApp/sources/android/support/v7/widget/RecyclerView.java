package android.support.v7.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Px;
import android.support.annotation.RestrictTo;
import android.support.annotation.VisibleForTesting;
import android.support.v4.os.TraceCompat;
import android.support.v4.util.Preconditions;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.NestedScrollingChild2;
import android.support.v4.view.NestedScrollingChildHelper;
import android.support.v4.view.ScrollingView;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.widget.EdgeEffectCompat;
import android.support.v7.recyclerview.R;
import android.support.v7.widget.af;
import android.support.v7.widget.ao;
import android.support.v7.widget.bq;
import android.support.v7.widget.br;
import android.support.v7.widget.d;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import com.google.android.exoplayer.C;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerView extends ViewGroup implements NestedScrollingChild2, ScrollingView {
    static final Interpolator L = new Interpolator() {
        /* class android.support.v7.widget.RecyclerView.AnonymousClass3 */

        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };
    private static final int[] M = {16843830};
    private static final int[] N = {16842987};
    private static final boolean O;
    private static final boolean P;
    private static final Class<?>[] Q = {Context.class, AttributeSet.class, Integer.TYPE, Integer.TYPE};

    /* renamed from: a  reason: collision with root package name */
    static final boolean f567a = (Build.VERSION.SDK_INT == 18 || Build.VERSION.SDK_INT == 19 || Build.VERSION.SDK_INT == 20);
    static final boolean b;
    static final boolean c;
    static final boolean d;
    final v A;
    ao B;
    ao.a C;
    final t D;
    boolean E;
    boolean F;
    boolean G;
    aw H;
    final int[] I;
    final int[] J;
    @VisibleForTesting
    final List<w> K;
    private final r R;
    private SavedState S;
    private final Rect T;
    private final ArrayList<m> U;
    private m V;
    private int W;
    private List<n> aA;
    private f.b aB;
    private d aC;
    private final int[] aD;
    private NestedScrollingChildHelper aE;
    private final int[] aF;
    private final int[] aG;
    private Runnable aH;
    private final br.b aI;
    private boolean aa;
    private int ab;
    private final AccessibilityManager ac;
    private List<k> ad;
    private int ae;
    private int af;
    @NonNull
    private e ag;
    private EdgeEffect ah;
    private EdgeEffect ai;
    private EdgeEffect aj;
    private EdgeEffect ak;
    private int al;
    private int am;
    private VelocityTracker an;
    private int ao;
    private int ap;
    private int aq;
    private int ar;
    private int as;
    private l at;
    private final int au;
    private final int av;
    private float aw;
    private float ax;
    private boolean ay;
    private n az;
    final p e;
    d f;
    af g;
    final br h;
    boolean i;
    final Runnable j;
    final Rect k;
    final RectF l;
    a m;
    @VisibleForTesting
    i n;
    q o;
    final ArrayList<h> p;
    boolean q;
    boolean r;
    boolean s;
    @VisibleForTesting
    boolean t;
    boolean u;
    boolean v;
    boolean w;
    boolean x;
    boolean y;
    f z;

    public interface d {
        int a(int i, int i2);
    }

    public interface k {
        void a(@NonNull View view);

        void b(@NonNull View view);
    }

    public static abstract class l {
        public abstract boolean a(int i, int i2);
    }

    public interface m {
        void a(boolean z);

        boolean a(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent);

        void b(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent);
    }

    public interface q {
        void a(@NonNull w wVar);
    }

    public static abstract class u {
        @Nullable
        public abstract View a(@NonNull p pVar, int i, int i2);
    }

    static {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        if (Build.VERSION.SDK_INT >= 23) {
            z2 = true;
        } else {
            z2 = false;
        }
        b = z2;
        if (Build.VERSION.SDK_INT >= 16) {
            z3 = true;
        } else {
            z3 = false;
        }
        c = z3;
        if (Build.VERSION.SDK_INT >= 21) {
            z4 = true;
        } else {
            z4 = false;
        }
        d = z4;
        if (Build.VERSION.SDK_INT <= 15) {
            z5 = true;
        } else {
            z5 = false;
        }
        O = z5;
        if (Build.VERSION.SDK_INT <= 15) {
            z6 = true;
        } else {
            z6 = false;
        }
        P = z6;
    }

    public RecyclerView(@NonNull Context context) {
        this(context, null);
    }

    public RecyclerView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        boolean z2 = true;
        this.R = new r();
        this.e = new p();
        this.h = new br();
        this.j = new Runnable() {
            /* class android.support.v7.widget.RecyclerView.AnonymousClass1 */

            public void run() {
                if (RecyclerView.this.t && !RecyclerView.this.isLayoutRequested()) {
                    if (!RecyclerView.this.q) {
                        RecyclerView.this.requestLayout();
                    } else if (RecyclerView.this.v) {
                        RecyclerView.this.u = true;
                    } else {
                        RecyclerView.this.d();
                    }
                }
            }
        };
        this.k = new Rect();
        this.T = new Rect();
        this.l = new RectF();
        this.p = new ArrayList<>();
        this.U = new ArrayList<>();
        this.W = 0;
        this.x = false;
        this.y = false;
        this.ae = 0;
        this.af = 0;
        this.ag = new e();
        this.z = new ai();
        this.al = 0;
        this.am = -1;
        this.aw = Float.MIN_VALUE;
        this.ax = Float.MIN_VALUE;
        this.ay = true;
        this.A = new v();
        this.C = d ? new ao.a() : null;
        this.D = new t();
        this.E = false;
        this.F = false;
        this.aB = new g();
        this.G = false;
        this.aD = new int[2];
        this.aF = new int[2];
        this.I = new int[2];
        this.aG = new int[2];
        this.J = new int[2];
        this.K = new ArrayList();
        this.aH = new Runnable() {
            /* class android.support.v7.widget.RecyclerView.AnonymousClass2 */

            public void run() {
                if (RecyclerView.this.z != null) {
                    RecyclerView.this.z.a();
                }
                RecyclerView.this.G = false;
            }
        };
        this.aI = new br.b() {
            /* class android.support.v7.widget.RecyclerView.AnonymousClass4 */

            @Override // android.support.v7.widget.br.b
            public void a(w wVar, @NonNull f.c cVar, @Nullable f.c cVar2) {
                RecyclerView.this.e.c(wVar);
                RecyclerView.this.b(wVar, cVar, cVar2);
            }

            @Override // android.support.v7.widget.br.b
            public void b(w wVar, f.c cVar, f.c cVar2) {
                RecyclerView.this.a(wVar, cVar, cVar2);
            }

            @Override // android.support.v7.widget.br.b
            public void c(w wVar, @NonNull f.c cVar, @NonNull f.c cVar2) {
                wVar.a(false);
                if (RecyclerView.this.x) {
                    if (RecyclerView.this.z.a(wVar, wVar, cVar, cVar2)) {
                        RecyclerView.this.p();
                    }
                } else if (RecyclerView.this.z.c(wVar, cVar, cVar2)) {
                    RecyclerView.this.p();
                }
            }

            @Override // android.support.v7.widget.br.b
            public void a(w wVar) {
                RecyclerView.this.n.a(wVar.f590a, RecyclerView.this.e);
            }
        };
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, N, i2, 0);
            this.i = obtainStyledAttributes.getBoolean(0, true);
            obtainStyledAttributes.recycle();
        } else {
            this.i = true;
        }
        setScrollContainer(true);
        setFocusableInTouchMode(true);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.as = viewConfiguration.getScaledTouchSlop();
        this.aw = ViewConfigurationCompat.getScaledHorizontalScrollFactor(viewConfiguration, context);
        this.ax = ViewConfigurationCompat.getScaledVerticalScrollFactor(viewConfiguration, context);
        this.au = viewConfiguration.getScaledMinimumFlingVelocity();
        this.av = viewConfiguration.getScaledMaximumFlingVelocity();
        setWillNotDraw(getOverScrollMode() == 2);
        this.z.a(this.aB);
        b();
        z();
        y();
        if (ViewCompat.getImportantForAccessibility(this) == 0) {
            ViewCompat.setImportantForAccessibility(this, 1);
        }
        this.ac = (AccessibilityManager) getContext().getSystemService("accessibility");
        setAccessibilityDelegateCompat(new aw(this));
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, R.styleable.RecyclerView, i2, 0);
            String string = obtainStyledAttributes2.getString(R.styleable.RecyclerView_layoutManager);
            if (obtainStyledAttributes2.getInt(R.styleable.RecyclerView_android_descendantFocusability, -1) == -1) {
                setDescendantFocusability(262144);
            }
            this.s = obtainStyledAttributes2.getBoolean(R.styleable.RecyclerView_fastScrollEnabled, false);
            if (this.s) {
                a((StateListDrawable) obtainStyledAttributes2.getDrawable(R.styleable.RecyclerView_fastScrollVerticalThumbDrawable), obtainStyledAttributes2.getDrawable(R.styleable.RecyclerView_fastScrollVerticalTrackDrawable), (StateListDrawable) obtainStyledAttributes2.getDrawable(R.styleable.RecyclerView_fastScrollHorizontalThumbDrawable), obtainStyledAttributes2.getDrawable(R.styleable.RecyclerView_fastScrollHorizontalTrackDrawable));
            }
            obtainStyledAttributes2.recycle();
            a(context, string, attributeSet, i2, 0);
            if (Build.VERSION.SDK_INT >= 21) {
                TypedArray obtainStyledAttributes3 = context.obtainStyledAttributes(attributeSet, M, i2, 0);
                z2 = obtainStyledAttributes3.getBoolean(0, true);
                obtainStyledAttributes3.recycle();
            }
        } else {
            setDescendantFocusability(262144);
        }
        setNestedScrollingEnabled(z2);
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return " " + super.toString() + ", adapter:" + this.m + ", layout:" + this.n + ", context:" + getContext();
    }

    @SuppressLint({"InlinedApi"})
    private void y() {
        if (ViewCompat.getImportantForAutofill(this) == 0) {
            ViewCompat.setImportantForAutofill(this, 8);
        }
    }

    @Nullable
    public aw getCompatAccessibilityDelegate() {
        return this.H;
    }

    public void setAccessibilityDelegateCompat(@Nullable aw awVar) {
        this.H = awVar;
        ViewCompat.setAccessibilityDelegate(this, this.H);
    }

    private void a(Context context, String str, AttributeSet attributeSet, int i2, int i3) {
        ClassLoader classLoader;
        Constructor<? extends U> constructor;
        Object[] objArr;
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                String a2 = a(context, trim);
                try {
                    if (isInEditMode()) {
                        classLoader = getClass().getClassLoader();
                    } else {
                        classLoader = context.getClassLoader();
                    }
                    Class<? extends U> asSubclass = classLoader.loadClass(a2).asSubclass(i.class);
                    try {
                        constructor = asSubclass.getConstructor(Q);
                        objArr = new Object[]{context, attributeSet, Integer.valueOf(i2), Integer.valueOf(i3)};
                    } catch (NoSuchMethodException e2) {
                        try {
                            constructor = asSubclass.getConstructor(new Class[0]);
                            objArr = null;
                        } catch (NoSuchMethodException e3) {
                            e3.initCause(e2);
                            throw new IllegalStateException(attributeSet.getPositionDescription() + ": Error creating LayoutManager " + a2, e3);
                        }
                    }
                    constructor.setAccessible(true);
                    setLayoutManager((i) constructor.newInstance(objArr));
                } catch (ClassNotFoundException e4) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Unable to find LayoutManager " + a2, e4);
                } catch (InvocationTargetException e5) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + a2, e5);
                } catch (InstantiationException e6) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + a2, e6);
                } catch (IllegalAccessException e7) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Cannot access non-public constructor " + a2, e7);
                } catch (ClassCastException e8) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Class is not a LayoutManager " + a2, e8);
                }
            }
        }
    }

    private String a(Context context, String str) {
        if (str.charAt(0) == '.') {
            return context.getPackageName() + str;
        }
        return !str.contains(".") ? RecyclerView.class.getPackage().getName() + '.' + str : str;
    }

    private void z() {
        this.g = new af(new af.b() {
            /* class android.support.v7.widget.RecyclerView.AnonymousClass5 */

            @Override // android.support.v7.widget.af.b
            public int a() {
                return RecyclerView.this.getChildCount();
            }

            @Override // android.support.v7.widget.af.b
            public void a(View view, int i) {
                RecyclerView.this.addView(view, i);
                RecyclerView.this.l(view);
            }

            @Override // android.support.v7.widget.af.b
            public int a(View view) {
                return RecyclerView.this.indexOfChild(view);
            }

            @Override // android.support.v7.widget.af.b
            public void a(int i) {
                View childAt = RecyclerView.this.getChildAt(i);
                if (childAt != null) {
                    RecyclerView.this.k(childAt);
                    childAt.clearAnimation();
                }
                RecyclerView.this.removeViewAt(i);
            }

            @Override // android.support.v7.widget.af.b
            public View b(int i) {
                return RecyclerView.this.getChildAt(i);
            }

            @Override // android.support.v7.widget.af.b
            public void b() {
                int a2 = a();
                for (int i = 0; i < a2; i++) {
                    View b = b(i);
                    RecyclerView.this.k(b);
                    b.clearAnimation();
                }
                RecyclerView.this.removeAllViews();
            }

            @Override // android.support.v7.widget.af.b
            public w b(View view) {
                return RecyclerView.e(view);
            }

            @Override // android.support.v7.widget.af.b
            public void a(View view, int i, ViewGroup.LayoutParams layoutParams) {
                w e = RecyclerView.e(view);
                if (e != null) {
                    if (e.r() || e.c()) {
                        e.m();
                    } else {
                        throw new IllegalArgumentException("Called attach on a child which is not detached: " + e + RecyclerView.this.a());
                    }
                }
                RecyclerView.this.attachViewToParent(view, i, layoutParams);
            }

            @Override // android.support.v7.widget.af.b
            public void c(int i) {
                w e;
                View b = b(i);
                if (!(b == null || (e = RecyclerView.e(b)) == null)) {
                    if (!e.r() || e.c()) {
                        e.b(256);
                    } else {
                        throw new IllegalArgumentException("called detach on an already detached child " + e + RecyclerView.this.a());
                    }
                }
                RecyclerView.this.detachViewFromParent((RecyclerView) i);
            }

            @Override // android.support.v7.widget.af.b
            public void c(View view) {
                w e = RecyclerView.e(view);
                if (e != null) {
                    e.a(RecyclerView.this);
                }
            }

            @Override // android.support.v7.widget.af.b
            public void d(View view) {
                w e = RecyclerView.e(view);
                if (e != null) {
                    e.b(RecyclerView.this);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f = new d(new d.a() {
            /* class android.support.v7.widget.RecyclerView.AnonymousClass6 */

            @Override // android.support.v7.widget.d.a
            public w a(int i) {
                w a2 = RecyclerView.this.a(i, true);
                if (a2 != null && !RecyclerView.this.g.c(a2.f590a)) {
                    return a2;
                }
                return null;
            }

            @Override // android.support.v7.widget.d.a
            public void a(int i, int i2) {
                RecyclerView.this.a(i, i2, true);
                RecyclerView.this.E = true;
                RecyclerView.this.D.c += i2;
            }

            @Override // android.support.v7.widget.d.a
            public void b(int i, int i2) {
                RecyclerView.this.a(i, i2, false);
                RecyclerView.this.E = true;
            }

            @Override // android.support.v7.widget.d.a
            public void a(int i, int i2, Object obj) {
                RecyclerView.this.a(i, i2, obj);
                RecyclerView.this.F = true;
            }

            @Override // android.support.v7.widget.d.a
            public void a(d.b bVar) {
                c(bVar);
            }

            /* access modifiers changed from: package-private */
            public void c(d.b bVar) {
                switch (bVar.f705a) {
                    case 1:
                        RecyclerView.this.n.a(RecyclerView.this, bVar.b, bVar.d);
                        return;
                    case 2:
                        RecyclerView.this.n.b(RecyclerView.this, bVar.b, bVar.d);
                        return;
                    case 3:
                    case 5:
                    case 6:
                    case 7:
                    default:
                        return;
                    case 4:
                        RecyclerView.this.n.a(RecyclerView.this, bVar.b, bVar.d, bVar.c);
                        return;
                    case 8:
                        RecyclerView.this.n.a(RecyclerView.this, bVar.b, bVar.d, 1);
                        return;
                }
            }

            @Override // android.support.v7.widget.d.a
            public void b(d.b bVar) {
                c(bVar);
            }

            @Override // android.support.v7.widget.d.a
            public void c(int i, int i2) {
                RecyclerView.this.g(i, i2);
                RecyclerView.this.E = true;
            }

            @Override // android.support.v7.widget.d.a
            public void d(int i, int i2) {
                RecyclerView.this.f(i, i2);
                RecyclerView.this.E = true;
            }
        });
    }

    public void setHasFixedSize(boolean z2) {
        this.r = z2;
    }

    public void setClipToPadding(boolean z2) {
        if (z2 != this.i) {
            k();
        }
        this.i = z2;
        super.setClipToPadding(z2);
        if (this.t) {
            requestLayout();
        }
    }

    public boolean getClipToPadding() {
        return this.i;
    }

    public void setScrollingTouchSlop(int i2) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        switch (i2) {
            case 0:
                break;
            case 1:
                this.as = viewConfiguration.getScaledPagingTouchSlop();
                return;
            default:
                Log.w("RecyclerView", "setScrollingTouchSlop(): bad argument constant " + i2 + "; using default value");
                break;
        }
        this.as = viewConfiguration.getScaledTouchSlop();
    }

    public void a(@Nullable a aVar, boolean z2) {
        setLayoutFrozen(false);
        a(aVar, true, z2);
        c(true);
        requestLayout();
    }

    public void setAdapter(@Nullable a aVar) {
        setLayoutFrozen(false);
        a(aVar, false, true);
        c(false);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.z != null) {
            this.z.d();
        }
        if (this.n != null) {
            this.n.c(this.e);
            this.n.b(this.e);
        }
        this.e.a();
    }

    private void a(@Nullable a aVar, boolean z2, boolean z3) {
        if (this.m != null) {
            this.m.b(this.R);
            this.m.b(this);
        }
        if (!z2 || z3) {
            c();
        }
        this.f.a();
        a aVar2 = this.m;
        this.m = aVar;
        if (aVar != null) {
            aVar.a(this.R);
            aVar.a(this);
        }
        if (this.n != null) {
            this.n.a(aVar2, this.m);
        }
        this.e.a(aVar2, this.m, z2);
        this.D.f = true;
    }

    @Nullable
    public a getAdapter() {
        return this.m;
    }

    public void setRecyclerListener(@Nullable q qVar) {
        this.o = qVar;
    }

    public int getBaseline() {
        if (this.n != null) {
            return this.n.u();
        }
        return super.getBaseline();
    }

    public void setLayoutManager(@Nullable i iVar) {
        if (iVar != this.n) {
            f();
            if (this.n != null) {
                if (this.z != null) {
                    this.z.d();
                }
                this.n.c(this.e);
                this.n.b(this.e);
                this.e.a();
                if (this.q) {
                    this.n.b(this, this.e);
                }
                this.n.b((RecyclerView) null);
                this.n = null;
            } else {
                this.e.a();
            }
            this.g.a();
            this.n = iVar;
            if (iVar != null) {
                if (iVar.q != null) {
                    throw new IllegalArgumentException("LayoutManager " + iVar + " is already attached to a RecyclerView:" + iVar.q.a());
                }
                this.n.b(this);
                if (this.q) {
                    this.n.c(this);
                }
            }
            this.e.b();
            requestLayout();
        }
    }

    public void setOnFlingListener(@Nullable l lVar) {
        this.at = lVar;
    }

    @Nullable
    public l getOnFlingListener() {
        return this.at;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (this.S != null) {
            savedState.a(this.S);
        } else if (this.n != null) {
            savedState.f573a = this.n.d();
        } else {
            savedState.f573a = null;
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        this.S = (SavedState) parcelable;
        super.onRestoreInstanceState(this.S.getSuperState());
        if (this.n != null && this.S.f573a != null) {
            this.n.a(this.S.f573a);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.view.View, android.view.ViewGroup
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    @Override // android.view.View, android.view.ViewGroup
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    private void e(w wVar) {
        View view = wVar.f590a;
        boolean z2 = view.getParent() == this;
        this.e.c(b(view));
        if (wVar.r()) {
            this.g.a(view, -1, view.getLayoutParams(), true);
        } else if (!z2) {
            this.g.a(view, true);
        } else {
            this.g.d(view);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view) {
        e();
        boolean f2 = this.g.f(view);
        if (f2) {
            w e2 = e(view);
            this.e.c(e2);
            this.e.b(e2);
        }
        a(!f2);
        return f2;
    }

    @Nullable
    public i getLayoutManager() {
        return this.n;
    }

    @NonNull
    public o getRecycledViewPool() {
        return this.e.g();
    }

    public void setRecycledViewPool(@Nullable o oVar) {
        this.e.a(oVar);
    }

    public void setViewCacheExtension(@Nullable u uVar) {
        this.e.a(uVar);
    }

    public void setItemViewCacheSize(int i2) {
        this.e.a(i2);
    }

    public int getScrollState() {
        return this.al;
    }

    /* access modifiers changed from: package-private */
    public void setScrollState(int i2) {
        if (i2 != this.al) {
            this.al = i2;
            if (i2 != 2) {
                B();
            }
            g(i2);
        }
    }

    public void a(@NonNull h hVar, int i2) {
        if (this.n != null) {
            this.n.a("Cannot add item decoration during a scroll  or layout");
        }
        if (this.p.isEmpty()) {
            setWillNotDraw(false);
        }
        if (i2 < 0) {
            this.p.add(hVar);
        } else {
            this.p.add(i2, hVar);
        }
        r();
        requestLayout();
    }

    public void a(@NonNull h hVar) {
        a(hVar, -1);
    }

    public int getItemDecorationCount() {
        return this.p.size();
    }

    public void b(@NonNull h hVar) {
        if (this.n != null) {
            this.n.a("Cannot remove item decoration during a scroll  or layout");
        }
        this.p.remove(hVar);
        if (this.p.isEmpty()) {
            setWillNotDraw(getOverScrollMode() == 2);
        }
        r();
        requestLayout();
    }

    public void setChildDrawingOrderCallback(@Nullable d dVar) {
        if (dVar != this.aC) {
            this.aC = dVar;
            setChildrenDrawingOrderEnabled(this.aC != null);
        }
    }

    @Deprecated
    public void setOnScrollListener(@Nullable n nVar) {
        this.az = nVar;
    }

    public void a(@NonNull n nVar) {
        if (this.aA == null) {
            this.aA = new ArrayList();
        }
        this.aA.add(nVar);
    }

    public void b(@NonNull n nVar) {
        if (this.aA != null) {
            this.aA.remove(nVar);
        }
    }

    public void a(int i2) {
        if (!this.v) {
            f();
            if (this.n == null) {
                Log.e("RecyclerView", "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
                return;
            }
            this.n.e(i2);
            awakenScrollBars();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        if (this.n != null) {
            this.n.e(i2);
            awakenScrollBars();
        }
    }

    public void scrollTo(int i2, int i3) {
        Log.w("RecyclerView", "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    public void scrollBy(int i2, int i3) {
        if (this.n == null) {
            Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.v) {
            boolean e2 = this.n.e();
            boolean f2 = this.n.f();
            if (e2 || f2) {
                if (!e2) {
                    i2 = 0;
                }
                if (!f2) {
                    i3 = 0;
                }
                a(i2, i3, (MotionEvent) null);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, @Nullable int[] iArr) {
        int i4;
        int i5;
        e();
        l();
        TraceCompat.beginSection("RV Scroll");
        a(this.D);
        if (i2 != 0) {
            i4 = this.n.a(i2, this.e, this.D);
        } else {
            i4 = 0;
        }
        if (i3 != 0) {
            i5 = this.n.b(i3, this.e, this.D);
        } else {
            i5 = 0;
        }
        TraceCompat.endSection();
        w();
        m();
        a(false);
        if (iArr != null) {
            iArr[0] = i4;
            iArr[1] = i5;
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (!this.t || this.x) {
            TraceCompat.beginSection("RV FullInvalidate");
            q();
            TraceCompat.endSection();
        } else if (!this.f.d()) {
        } else {
            if (this.f.a(4) && !this.f.a(11)) {
                TraceCompat.beginSection("RV PartialInvalidate");
                e();
                l();
                this.f.b();
                if (!this.u) {
                    if (A()) {
                        q();
                    } else {
                        this.f.c();
                    }
                }
                a(true);
                m();
                TraceCompat.endSection();
            } else if (this.f.d()) {
                TraceCompat.beginSection("RV FullInvalidate");
                q();
                TraceCompat.endSection();
            }
        }
    }

    private boolean A() {
        int b2 = this.g.b();
        for (int i2 = 0; i2 < b2; i2++) {
            w e2 = e(this.g.b(i2));
            if (!(e2 == null || e2.c() || !e2.z())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, MotionEvent motionEvent) {
        int i4;
        int i5;
        int i6;
        int i7;
        d();
        if (this.m != null) {
            a(i2, i3, this.J);
            i5 = this.J[0];
            i4 = this.J[1];
            i7 = i2 - i5;
            i6 = i3 - i4;
        } else {
            i4 = 0;
            i5 = 0;
            i6 = 0;
            i7 = 0;
        }
        if (!this.p.isEmpty()) {
            invalidate();
        }
        if (dispatchNestedScroll(i5, i4, i7, i6, this.aF, 0)) {
            this.aq -= this.aF[0];
            this.ar -= this.aF[1];
            if (motionEvent != null) {
                motionEvent.offsetLocation((float) this.aF[0], (float) this.aF[1]);
            }
            int[] iArr = this.aG;
            iArr[0] = iArr[0] + this.aF[0];
            int[] iArr2 = this.aG;
            iArr2[1] = iArr2[1] + this.aF[1];
        } else if (getOverScrollMode() != 2) {
            if (motionEvent != null && !MotionEventCompat.isFromSource(motionEvent, 8194)) {
                a(motionEvent.getX(), (float) i7, motionEvent.getY(), (float) i6);
            }
            c(i2, i3);
        }
        if (!(i5 == 0 && i4 == 0)) {
            i(i5, i4);
        }
        if (!awakenScrollBars()) {
            invalidate();
        }
        return (i5 == 0 && i4 == 0) ? false : true;
    }

    @Override // android.support.v4.view.ScrollingView
    public int computeHorizontalScrollOffset() {
        if (this.n != null && this.n.e()) {
            return this.n.c(this.D);
        }
        return 0;
    }

    @Override // android.support.v4.view.ScrollingView
    public int computeHorizontalScrollExtent() {
        if (this.n != null && this.n.e()) {
            return this.n.e(this.D);
        }
        return 0;
    }

    @Override // android.support.v4.view.ScrollingView
    public int computeHorizontalScrollRange() {
        if (this.n != null && this.n.e()) {
            return this.n.g(this.D);
        }
        return 0;
    }

    @Override // android.support.v4.view.ScrollingView
    public int computeVerticalScrollOffset() {
        if (this.n != null && this.n.f()) {
            return this.n.d(this.D);
        }
        return 0;
    }

    @Override // android.support.v4.view.ScrollingView
    public int computeVerticalScrollExtent() {
        if (this.n != null && this.n.f()) {
            return this.n.f(this.D);
        }
        return 0;
    }

    @Override // android.support.v4.view.ScrollingView
    public int computeVerticalScrollRange() {
        if (this.n != null && this.n.f()) {
            return this.n.h(this.D);
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.W++;
        if (this.W == 1 && !this.v) {
            this.u = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        if (this.W < 1) {
            this.W = 1;
        }
        if (!z2 && !this.v) {
            this.u = false;
        }
        if (this.W == 1) {
            if (z2 && this.u && !this.v && this.n != null && this.m != null) {
                q();
            }
            if (!this.v) {
                this.u = false;
            }
        }
        this.W--;
    }

    public void setLayoutFrozen(boolean z2) {
        if (z2 != this.v) {
            a("Do not setLayoutFrozen in layout or scroll");
            if (!z2) {
                this.v = false;
                if (!(!this.u || this.n == null || this.m == null)) {
                    requestLayout();
                }
                this.u = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0));
            this.v = true;
            this.aa = true;
            f();
        }
    }

    public void a(@Px int i2, @Px int i3) {
        a(i2, i3, (Interpolator) null);
    }

    public void a(@Px int i2, @Px int i3, @Nullable Interpolator interpolator) {
        int i4 = 0;
        if (this.n == null) {
            Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.v) {
            if (!this.n.e()) {
                i2 = 0;
            }
            if (this.n.f()) {
                i4 = i3;
            }
            if (i2 != 0 || i4 != 0) {
                this.A.a(i2, i4, interpolator);
            }
        }
    }

    public boolean b(int i2, int i3) {
        boolean z2;
        int i4;
        if (this.n == null) {
            Log.e("RecyclerView", "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            return false;
        } else if (this.v) {
            return false;
        } else {
            boolean e2 = this.n.e();
            boolean f2 = this.n.f();
            if (!e2 || Math.abs(i2) < this.au) {
                i2 = 0;
            }
            if (!f2 || Math.abs(i3) < this.au) {
                i3 = 0;
            }
            if ((i2 == 0 && i3 == 0) || dispatchNestedPreFling((float) i2, (float) i3)) {
                return false;
            }
            if (e2 || f2) {
                z2 = true;
            } else {
                z2 = false;
            }
            dispatchNestedFling((float) i2, (float) i3, z2);
            if (this.at != null && this.at.a(i2, i3)) {
                return true;
            }
            if (!z2) {
                return false;
            }
            if (e2) {
                i4 = 1;
            } else {
                i4 = 0;
            }
            if (f2) {
                i4 |= 2;
            }
            startNestedScroll(i4, 1);
            this.A.a(Math.max(-this.av, Math.min(i2, this.av)), Math.max(-this.av, Math.min(i3, this.av)));
            return true;
        }
    }

    public void f() {
        setScrollState(0);
        B();
    }

    private void B() {
        this.A.b();
        if (this.n != null) {
            this.n.H();
        }
    }

    public int getMinFlingVelocity() {
        return this.au;
    }

    public int getMaxFlingVelocity() {
        return this.av;
    }

    private void a(float f2, float f3, float f4, float f5) {
        boolean z2 = true;
        boolean z3 = false;
        if (f3 < 0.0f) {
            g();
            EdgeEffectCompat.onPull(this.ah, (-f3) / ((float) getWidth()), 1.0f - (f4 / ((float) getHeight())));
            z3 = true;
        } else if (f3 > 0.0f) {
            h();
            EdgeEffectCompat.onPull(this.aj, f3 / ((float) getWidth()), f4 / ((float) getHeight()));
            z3 = true;
        }
        if (f5 < 0.0f) {
            i();
            EdgeEffectCompat.onPull(this.ai, (-f5) / ((float) getHeight()), f2 / ((float) getWidth()));
        } else if (f5 > 0.0f) {
            j();
            EdgeEffectCompat.onPull(this.ak, f5 / ((float) getHeight()), 1.0f - (f2 / ((float) getWidth())));
        } else {
            z2 = z3;
        }
        if (z2 || f3 != 0.0f || f5 != 0.0f) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private void C() {
        boolean z2 = false;
        if (this.ah != null) {
            this.ah.onRelease();
            z2 = this.ah.isFinished();
        }
        if (this.ai != null) {
            this.ai.onRelease();
            z2 |= this.ai.isFinished();
        }
        if (this.aj != null) {
            this.aj.onRelease();
            z2 |= this.aj.isFinished();
        }
        if (this.ak != null) {
            this.ak.onRelease();
            z2 |= this.ak.isFinished();
        }
        if (z2) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i2, int i3) {
        boolean z2 = false;
        if (this.ah != null && !this.ah.isFinished() && i2 > 0) {
            this.ah.onRelease();
            z2 = this.ah.isFinished();
        }
        if (this.aj != null && !this.aj.isFinished() && i2 < 0) {
            this.aj.onRelease();
            z2 |= this.aj.isFinished();
        }
        if (this.ai != null && !this.ai.isFinished() && i3 > 0) {
            this.ai.onRelease();
            z2 |= this.ai.isFinished();
        }
        if (this.ak != null && !this.ak.isFinished() && i3 < 0) {
            this.ak.onRelease();
            z2 |= this.ak.isFinished();
        }
        if (z2) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void d(int i2, int i3) {
        if (i2 < 0) {
            g();
            this.ah.onAbsorb(-i2);
        } else if (i2 > 0) {
            h();
            this.aj.onAbsorb(i2);
        }
        if (i3 < 0) {
            i();
            this.ai.onAbsorb(-i3);
        } else if (i3 > 0) {
            j();
            this.ak.onAbsorb(i3);
        }
        if (i2 != 0 || i3 != 0) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (this.ah == null) {
            this.ah = this.ag.a(this, 0);
            if (this.i) {
                this.ah.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.ah.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.aj == null) {
            this.aj = this.ag.a(this, 2);
            if (this.i) {
                this.aj.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.aj.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.ai == null) {
            this.ai = this.ag.a(this, 1);
            if (this.i) {
                this.ai.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.ai.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (this.ak == null) {
            this.ak = this.ag.a(this, 3);
            if (this.i) {
                this.ak.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.ak.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        this.ak = null;
        this.ai = null;
        this.aj = null;
        this.ah = null;
    }

    public void setEdgeEffectFactory(@NonNull e eVar) {
        Preconditions.checkNotNull(eVar);
        this.ag = eVar;
        k();
    }

    @NonNull
    public e getEdgeEffectFactory() {
        return this.ag;
    }

    public View focusSearch(View view, int i2) {
        View view2;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        View d2 = this.n.d(view, i2);
        if (d2 != null) {
            return d2;
        }
        boolean z6 = this.m != null && this.n != null && !o() && !this.v;
        FocusFinder instance = FocusFinder.getInstance();
        if (!z6 || !(i2 == 2 || i2 == 1)) {
            View findNextFocus = instance.findNextFocus(this, view, i2);
            if (findNextFocus != null || !z6) {
                view2 = findNextFocus;
            } else {
                d();
                if (c(view) == null) {
                    return null;
                }
                e();
                view2 = this.n.a(view, i2, this.e, this.D);
                a(false);
            }
        } else {
            if (this.n.f()) {
                int i3 = i2 == 2 ? 130 : 33;
                if (instance.findNextFocus(this, view, i3) == null) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (O) {
                    i2 = i3;
                }
            } else {
                z2 = false;
            }
            if (z2 || !this.n.e()) {
                z5 = z2;
            } else {
                if (this.n.t() == 1) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (i2 == 2) {
                    z4 = true;
                } else {
                    z4 = false;
                }
                int i4 = z4 ^ z3 ? 66 : 17;
                if (instance.findNextFocus(this, view, i4) != null) {
                    z5 = false;
                }
                if (O) {
                    i2 = i4;
                }
            }
            if (z5) {
                d();
                if (c(view) == null) {
                    return null;
                }
                e();
                this.n.a(view, i2, this.e, this.D);
                a(false);
            }
            view2 = instance.findNextFocus(this, view, i2);
        }
        if (view2 == null || view2.hasFocusable()) {
            if (!a(view, view2, i2)) {
                view2 = super.focusSearch(view, i2);
            }
            return view2;
        } else if (getFocusedChild() == null) {
            return super.focusSearch(view, i2);
        } else {
            a(view2, (View) null);
            return view;
        }
    }

    private boolean a(View view, View view2, int i2) {
        int i3;
        char c2 = 65535;
        boolean z2 = false;
        if (view2 == null || view2 == this) {
            return false;
        }
        if (c(view2) == null) {
            return false;
        }
        if (view == null || c(view) == null) {
            return true;
        }
        this.k.set(0, 0, view.getWidth(), view.getHeight());
        this.T.set(0, 0, view2.getWidth(), view2.getHeight());
        offsetDescendantRectToMyCoords(view, this.k);
        offsetDescendantRectToMyCoords(view2, this.T);
        int i4 = this.n.t() == 1 ? -1 : 1;
        if ((this.k.left < this.T.left || this.k.right <= this.T.left) && this.k.right < this.T.right) {
            i3 = 1;
        } else if ((this.k.right > this.T.right || this.k.left >= this.T.right) && this.k.left > this.T.left) {
            i3 = -1;
        } else {
            i3 = 0;
        }
        if ((this.k.top < this.T.top || this.k.bottom <= this.T.top) && this.k.bottom < this.T.bottom) {
            c2 = 1;
        } else if ((this.k.bottom <= this.T.bottom && this.k.top < this.T.bottom) || this.k.top <= this.T.top) {
            c2 = 0;
        }
        switch (i2) {
            case 1:
                if (c2 < 0 || (c2 == 0 && i4 * i3 <= 0)) {
                    z2 = true;
                }
                return z2;
            case 2:
                if (c2 > 0 || (c2 == 0 && i4 * i3 >= 0)) {
                    z2 = true;
                }
                return z2;
            case 17:
                return i3 < 0;
            case 33:
                return c2 < 0;
            case 66:
                return i3 > 0;
            case 130:
                return c2 > 0;
            default:
                throw new IllegalArgumentException("Invalid direction: " + i2 + a());
        }
    }

    public void requestChildFocus(View view, View view2) {
        if (!this.n.a(this, this.D, view, view2) && view2 != null) {
            a(view, view2);
        }
        super.requestChildFocus(view, view2);
    }

    private void a(@NonNull View view, @Nullable View view2) {
        boolean z2 = true;
        View view3 = view2 != null ? view2 : view;
        this.k.set(0, 0, view3.getWidth(), view3.getHeight());
        ViewGroup.LayoutParams layoutParams = view3.getLayoutParams();
        if (layoutParams instanceof j) {
            j jVar = (j) layoutParams;
            if (!jVar.e) {
                Rect rect = jVar.d;
                this.k.left -= rect.left;
                this.k.right += rect.right;
                this.k.top -= rect.top;
                Rect rect2 = this.k;
                rect2.bottom = rect.bottom + rect2.bottom;
            }
        }
        if (view2 != null) {
            offsetDescendantRectToMyCoords(view2, this.k);
            offsetRectIntoDescendantCoords(view, this.k);
        }
        i iVar = this.n;
        Rect rect3 = this.k;
        boolean z3 = !this.t;
        if (view2 != null) {
            z2 = false;
        }
        iVar.a(this, view, rect3, z3, z2);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z2) {
        return this.n.a(this, view, rect, z2);
    }

    @Override // android.view.View, android.view.ViewGroup
    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        if (this.n == null || !this.n.a(this, arrayList, i2, i3)) {
            super.addFocusables(arrayList, i2, i3);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        if (o()) {
            return false;
        }
        return super.onRequestFocusInDescendants(i2, rect);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004f, code lost:
        if (r0 >= 30.0f) goto L_0x0051;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAttachedToWindow() {
        /*
        // Method dump skipped, instructions count: 108
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.onAttachedToWindow():void");
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.z != null) {
            this.z.d();
        }
        f();
        this.q = false;
        if (this.n != null) {
            this.n.b(this, this.e);
        }
        this.K.clear();
        removeCallbacks(this.aH);
        this.h.b();
        if (d && this.B != null) {
            this.B.b(this);
            this.B = null;
        }
    }

    public boolean isAttachedToWindow() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (o()) {
            if (str == null) {
                throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling" + a());
            }
            throw new IllegalStateException(str);
        } else if (this.af > 0) {
            Log.w("RecyclerView", "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame.", new IllegalStateException("" + a()));
        }
    }

    public void a(@NonNull m mVar) {
        this.U.add(mVar);
    }

    public void b(@NonNull m mVar) {
        this.U.remove(mVar);
        if (this.V == mVar) {
            this.V = null;
        }
    }

    private boolean a(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 3 || action == 0) {
            this.V = null;
        }
        int size = this.U.size();
        for (int i2 = 0; i2 < size; i2++) {
            m mVar = this.U.get(i2);
            if (mVar.a(this, motionEvent) && action != 3) {
                this.V = mVar;
                return true;
            }
        }
        return false;
    }

    private boolean b(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (this.V != null) {
            if (action == 0) {
                this.V = null;
            } else {
                this.V.b(this, motionEvent);
                if (action == 3 || action == 1) {
                    this.V = null;
                }
                return true;
            }
        }
        if (action != 0) {
            int size = this.U.size();
            for (int i2 = 0; i2 < size; i2++) {
                m mVar = this.U.get(i2);
                if (mVar.a(this, motionEvent)) {
                    this.V = mVar;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        int i2;
        boolean z3;
        if (this.v) {
            return false;
        }
        if (a(motionEvent)) {
            E();
            return true;
        } else if (this.n == null) {
            return false;
        } else {
            boolean e2 = this.n.e();
            boolean f2 = this.n.f();
            if (this.an == null) {
                this.an = VelocityTracker.obtain();
            }
            this.an.addMovement(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            int actionIndex = motionEvent.getActionIndex();
            switch (actionMasked) {
                case 0:
                    if (this.aa) {
                        this.aa = false;
                    }
                    this.am = motionEvent.getPointerId(0);
                    int x2 = (int) (motionEvent.getX() + 0.5f);
                    this.aq = x2;
                    this.ao = x2;
                    int y2 = (int) (motionEvent.getY() + 0.5f);
                    this.ar = y2;
                    this.ap = y2;
                    if (this.al == 2) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        setScrollState(1);
                    }
                    int[] iArr = this.aG;
                    this.aG[1] = 0;
                    iArr[0] = 0;
                    if (e2) {
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    if (f2) {
                        i2 |= 2;
                    }
                    startNestedScroll(i2, 0);
                    break;
                case 1:
                    this.an.clear();
                    stopNestedScroll(0);
                    break;
                case 2:
                    int findPointerIndex = motionEvent.findPointerIndex(this.am);
                    if (findPointerIndex >= 0) {
                        int x3 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
                        int y3 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
                        if (this.al != 1) {
                            int i3 = x3 - this.ao;
                            int i4 = y3 - this.ap;
                            if (!e2 || Math.abs(i3) <= this.as) {
                                z2 = false;
                            } else {
                                this.aq = x3;
                                z2 = true;
                            }
                            if (f2 && Math.abs(i4) > this.as) {
                                this.ar = y3;
                                z2 = true;
                            }
                            if (z2) {
                                setScrollState(1);
                                break;
                            }
                        }
                    } else {
                        Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.am + " not found. Did any MotionEvents get skipped?");
                        return false;
                    }
                    break;
                case 3:
                    E();
                    break;
                case 5:
                    this.am = motionEvent.getPointerId(actionIndex);
                    int x4 = (int) (motionEvent.getX(actionIndex) + 0.5f);
                    this.aq = x4;
                    this.ao = x4;
                    int y4 = (int) (motionEvent.getY(actionIndex) + 0.5f);
                    this.ar = y4;
                    this.ap = y4;
                    break;
                case 6:
                    c(motionEvent);
                    break;
            }
            if (this.al == 1) {
                z3 = true;
            } else {
                z3 = false;
            }
            return z3;
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        int size = this.U.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.U.get(i2).a(z2);
        }
        super.requestDisallowInterceptTouchEvent(z2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f2;
        int i2;
        boolean z2;
        int i3;
        int i4;
        int i5;
        boolean z3 = false;
        if (this.v || this.aa) {
            return false;
        }
        if (b(motionEvent)) {
            E();
            return true;
        } else if (this.n == null) {
            return false;
        } else {
            boolean e2 = this.n.e();
            boolean f3 = this.n.f();
            if (this.an == null) {
                this.an = VelocityTracker.obtain();
            }
            MotionEvent obtain = MotionEvent.obtain(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            int actionIndex = motionEvent.getActionIndex();
            if (actionMasked == 0) {
                int[] iArr = this.aG;
                this.aG[1] = 0;
                iArr[0] = 0;
            }
            obtain.offsetLocation((float) this.aG[0], (float) this.aG[1]);
            switch (actionMasked) {
                case 0:
                    this.am = motionEvent.getPointerId(0);
                    int x2 = (int) (motionEvent.getX() + 0.5f);
                    this.aq = x2;
                    this.ao = x2;
                    int y2 = (int) (motionEvent.getY() + 0.5f);
                    this.ar = y2;
                    this.ap = y2;
                    if (e2) {
                        i5 = 1;
                    } else {
                        i5 = 0;
                    }
                    if (f3) {
                        i5 |= 2;
                    }
                    startNestedScroll(i5, 0);
                    break;
                case 1:
                    this.an.addMovement(obtain);
                    this.an.computeCurrentVelocity(1000, (float) this.av);
                    float f4 = e2 ? -this.an.getXVelocity(this.am) : 0.0f;
                    if (f3) {
                        f2 = -this.an.getYVelocity(this.am);
                    } else {
                        f2 = 0.0f;
                    }
                    if ((f4 == 0.0f && f2 == 0.0f) || !b((int) f4, (int) f2)) {
                        setScrollState(0);
                    }
                    D();
                    z3 = true;
                    break;
                case 2:
                    int findPointerIndex = motionEvent.findPointerIndex(this.am);
                    if (findPointerIndex >= 0) {
                        int x3 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
                        int y3 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
                        int i6 = this.aq - x3;
                        int i7 = this.ar - y3;
                        if (dispatchNestedPreScroll(i6, i7, this.I, this.aF, 0)) {
                            i6 -= this.I[0];
                            i7 -= this.I[1];
                            obtain.offsetLocation((float) this.aF[0], (float) this.aF[1]);
                            int[] iArr2 = this.aG;
                            iArr2[0] = iArr2[0] + this.aF[0];
                            int[] iArr3 = this.aG;
                            iArr3[1] = iArr3[1] + this.aF[1];
                        }
                        if (this.al != 1) {
                            if (!e2 || Math.abs(i6) <= this.as) {
                                z2 = false;
                            } else {
                                if (i6 > 0) {
                                    i4 = i6 - this.as;
                                } else {
                                    i4 = this.as + i6;
                                }
                                z2 = true;
                                i6 = i4;
                            }
                            if (f3 && Math.abs(i7) > this.as) {
                                if (i7 > 0) {
                                    i3 = i7 - this.as;
                                } else {
                                    i3 = this.as + i7;
                                }
                                z2 = true;
                                i7 = i3;
                            }
                            if (z2) {
                                setScrollState(1);
                            }
                        }
                        if (this.al == 1) {
                            this.aq = x3 - this.aF[0];
                            this.ar = y3 - this.aF[1];
                            int i8 = e2 ? i6 : 0;
                            if (f3) {
                                i2 = i7;
                            } else {
                                i2 = 0;
                            }
                            if (a(i8, i2, obtain)) {
                                getParent().requestDisallowInterceptTouchEvent(true);
                            }
                            if (!(this.B == null || (i6 == 0 && i7 == 0))) {
                                this.B.a(this, i6, i7);
                                break;
                            }
                        }
                    } else {
                        Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.am + " not found. Did any MotionEvents get skipped?");
                        return false;
                    }
                    break;
                case 3:
                    E();
                    break;
                case 5:
                    this.am = motionEvent.getPointerId(actionIndex);
                    int x4 = (int) (motionEvent.getX(actionIndex) + 0.5f);
                    this.aq = x4;
                    this.ao = x4;
                    int y4 = (int) (motionEvent.getY(actionIndex) + 0.5f);
                    this.ar = y4;
                    this.ap = y4;
                    break;
                case 6:
                    c(motionEvent);
                    break;
            }
            if (!z3) {
                this.an.addMovement(obtain);
            }
            obtain.recycle();
            return true;
        }
    }

    private void D() {
        if (this.an != null) {
            this.an.clear();
        }
        stopNestedScroll(0);
        C();
    }

    private void E() {
        D();
        setScrollState(0);
    }

    private void c(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.am) {
            int i2 = actionIndex == 0 ? 1 : 0;
            this.am = motionEvent.getPointerId(i2);
            int x2 = (int) (motionEvent.getX(i2) + 0.5f);
            this.aq = x2;
            this.ao = x2;
            int y2 = (int) (motionEvent.getY(i2) + 0.5f);
            this.ar = y2;
            this.ap = y2;
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float f2;
        float f3;
        float f4;
        float f5;
        if (this.n != null && !this.v && motionEvent.getAction() == 8) {
            if ((motionEvent.getSource() & 2) != 0) {
                if (this.n.f()) {
                    f5 = -motionEvent.getAxisValue(9);
                } else {
                    f5 = 0.0f;
                }
                if (this.n.e()) {
                    f2 = motionEvent.getAxisValue(10);
                    f3 = f5;
                } else {
                    f2 = 0.0f;
                    f3 = f5;
                }
            } else if ((motionEvent.getSource() & 4194304) != 0) {
                float axisValue = motionEvent.getAxisValue(26);
                if (this.n.f()) {
                    f4 = -axisValue;
                    axisValue = 0.0f;
                } else if (this.n.e()) {
                    f4 = 0.0f;
                } else {
                    axisValue = 0.0f;
                    f4 = 0.0f;
                }
                f2 = axisValue;
                f3 = f4;
            } else {
                f2 = 0.0f;
                f3 = 0.0f;
            }
            if (!(f3 == 0.0f && f2 == 0.0f)) {
                a((int) (this.aw * f2), (int) (this.ax * f3), motionEvent);
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean z2 = false;
        if (this.n == null) {
            e(i2, i3);
        } else if (this.n.c()) {
            int mode = View.MeasureSpec.getMode(i2);
            int mode2 = View.MeasureSpec.getMode(i3);
            this.n.a(this.e, this.D, i2, i3);
            if (mode == 1073741824 && mode2 == 1073741824) {
                z2 = true;
            }
            if (!z2 && this.m != null) {
                if (this.D.d == 1) {
                    M();
                }
                this.n.c(i2, i3);
                this.D.i = true;
                N();
                this.n.d(i2, i3);
                if (this.n.l()) {
                    this.n.c(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), C.ENCODING_PCM_32BIT), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), C.ENCODING_PCM_32BIT));
                    this.D.i = true;
                    N();
                    this.n.d(i2, i3);
                }
            }
        } else if (this.r) {
            this.n.a(this.e, this.D, i2, i3);
        } else {
            if (this.w) {
                e();
                l();
                H();
                m();
                if (this.D.k) {
                    this.D.g = true;
                } else {
                    this.f.e();
                    this.D.g = false;
                }
                this.w = false;
                a(false);
            } else if (this.D.k) {
                setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
                return;
            }
            if (this.m != null) {
                this.D.e = this.m.a();
            } else {
                this.D.e = 0;
            }
            e();
            this.n.a(this.e, this.D, i2, i3);
            a(false);
            this.D.g = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void e(int i2, int i3) {
        setMeasuredDimension(i.a(i2, getPaddingLeft() + getPaddingRight(), ViewCompat.getMinimumWidth(this)), i.a(i3, getPaddingTop() + getPaddingBottom(), ViewCompat.getMinimumHeight(this)));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4 || i3 != i5) {
            k();
        }
    }

    public void setItemAnimator(@Nullable f fVar) {
        if (this.z != null) {
            this.z.d();
            this.z.a(null);
        }
        this.z = fVar;
        if (this.z != null) {
            this.z.a(this.aB);
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        this.ae++;
    }

    /* access modifiers changed from: package-private */
    public void m() {
        b(true);
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.ae--;
        if (this.ae < 1) {
            this.ae = 0;
            if (z2) {
                F();
                x();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean n() {
        return this.ac != null && this.ac.isEnabled();
    }

    private void F() {
        int i2 = this.ab;
        this.ab = 0;
        if (i2 != 0 && n()) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            obtain.setEventType(2048);
            AccessibilityEventCompat.setContentChangeTypes(obtain, i2);
            sendAccessibilityEventUnchecked(obtain);
        }
    }

    public boolean o() {
        return this.ae > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean a(AccessibilityEvent accessibilityEvent) {
        int i2;
        int i3 = 0;
        if (!o()) {
            return false;
        }
        if (accessibilityEvent != null) {
            i2 = AccessibilityEventCompat.getContentChangeTypes(accessibilityEvent);
        } else {
            i2 = 0;
        }
        if (i2 != 0) {
            i3 = i2;
        }
        this.ab = i3 | this.ab;
        return true;
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        if (!a(accessibilityEvent)) {
            super.sendAccessibilityEventUnchecked(accessibilityEvent);
        }
    }

    @Nullable
    public f getItemAnimator() {
        return this.z;
    }

    /* access modifiers changed from: package-private */
    public void p() {
        if (!this.G && this.q) {
            ViewCompat.postOnAnimation(this, this.aH);
            this.G = true;
        }
    }

    private boolean G() {
        return this.z != null && this.n.b();
    }

    private void H() {
        boolean z2;
        boolean z3;
        boolean z4 = true;
        if (this.x) {
            this.f.a();
            if (this.y) {
                this.n.a(this);
            }
        }
        if (G()) {
            this.f.b();
        } else {
            this.f.e();
        }
        if (this.E || this.F) {
            z2 = true;
        } else {
            z2 = false;
        }
        t tVar = this.D;
        if (!this.t || this.z == null || ((!this.x && !z2 && !this.n.u) || (this.x && !this.m.e()))) {
            z3 = false;
        } else {
            z3 = true;
        }
        tVar.j = z3;
        t tVar2 = this.D;
        if (!this.D.j || !z2 || this.x || !G()) {
            z4 = false;
        }
        tVar2.k = z4;
    }

    /* access modifiers changed from: package-private */
    public void q() {
        if (this.m == null) {
            Log.e("RecyclerView", "No adapter attached; skipping layout");
        } else if (this.n == null) {
            Log.e("RecyclerView", "No layout manager attached; skipping layout");
        } else {
            this.D.i = false;
            if (this.D.d == 1) {
                M();
                this.n.f(this);
                N();
            } else if (!this.f.f() && this.n.y() == getWidth() && this.n.z() == getHeight()) {
                this.n.f(this);
            } else {
                this.n.f(this);
                N();
            }
            O();
        }
    }

    private void I() {
        View view;
        int e2;
        if (!this.ay || !hasFocus() || this.m == null) {
            view = null;
        } else {
            view = getFocusedChild();
        }
        w d2 = view == null ? null : d(view);
        if (d2 == null) {
            J();
            return;
        }
        this.D.m = this.m.e() ? d2.g() : -1;
        t tVar = this.D;
        if (this.x) {
            e2 = -1;
        } else if (d2.q()) {
            e2 = d2.d;
        } else {
            e2 = d2.e();
        }
        tVar.l = e2;
        this.D.n = m(d2.f590a);
    }

    private void J() {
        this.D.m = -1;
        this.D.l = -1;
        this.D.n = -1;
    }

    @Nullable
    private View K() {
        int i2 = this.D.l != -1 ? this.D.l : 0;
        int e2 = this.D.e();
        for (int i3 = i2; i3 < e2; i3++) {
            w c2 = c(i3);
            if (c2 == null) {
                break;
            } else if (c2.f590a.hasFocusable()) {
                return c2.f590a;
            }
        }
        for (int min = Math.min(e2, i2) - 1; min >= 0; min--) {
            w c3 = c(min);
            if (c3 == null) {
                return null;
            }
            if (c3.f590a.hasFocusable()) {
                return c3.f590a;
            }
        }
        return null;
    }

    private void L() {
        w wVar;
        View view;
        View view2 = null;
        if (this.ay && this.m != null && hasFocus() && getDescendantFocusability() != 393216) {
            if (getDescendantFocusability() != 131072 || !isFocused()) {
                if (!isFocused()) {
                    View focusedChild = getFocusedChild();
                    if (!P || (focusedChild.getParent() != null && focusedChild.hasFocus())) {
                        if (!this.g.c(focusedChild)) {
                            return;
                        }
                    } else if (this.g.b() == 0) {
                        requestFocus();
                        return;
                    }
                }
                if (this.D.m == -1 || !this.m.e()) {
                    wVar = null;
                } else {
                    wVar = a(this.D.m);
                }
                if (wVar != null && !this.g.c(wVar.f590a) && wVar.f590a.hasFocusable()) {
                    view2 = wVar.f590a;
                } else if (this.g.b() > 0) {
                    view2 = K();
                }
                if (view2 != null) {
                    if (((long) this.D.n) == -1 || (view = view2.findViewById(this.D.n)) == null || !view.isFocusable()) {
                        view = view2;
                    }
                    view.requestFocus();
                }
            }
        }
    }

    private int m(View view) {
        int id = view.getId();
        View view2 = view;
        while (!view2.isFocused() && (view2 instanceof ViewGroup) && view2.hasFocus()) {
            view2 = ((ViewGroup) view2).getFocusedChild();
            if (view2.getId() != -1) {
                id = view2.getId();
            }
        }
        return id;
    }

    /* access modifiers changed from: package-private */
    public final void a(t tVar) {
        if (getScrollState() == 2) {
            OverScroller overScroller = this.A.f589a;
            tVar.o = overScroller.getFinalX() - overScroller.getCurrX();
            tVar.p = overScroller.getFinalY() - overScroller.getCurrY();
            return;
        }
        tVar.o = 0;
        tVar.p = 0;
    }

    private void M() {
        boolean z2 = true;
        this.D.a(1);
        a(this.D);
        this.D.i = false;
        e();
        this.h.a();
        l();
        H();
        I();
        t tVar = this.D;
        if (!this.D.j || !this.F) {
            z2 = false;
        }
        tVar.h = z2;
        this.F = false;
        this.E = false;
        this.D.g = this.D.k;
        this.D.e = this.m.a();
        a(this.aD);
        if (this.D.j) {
            int b2 = this.g.b();
            for (int i2 = 0; i2 < b2; i2++) {
                w e2 = e(this.g.b(i2));
                if (!e2.c() && (!e2.n() || this.m.e())) {
                    this.h.a(e2, this.z.a(this.D, e2, f.e(e2), e2.u()));
                    if (this.D.h && e2.z() && !e2.q() && !e2.c() && !e2.n()) {
                        this.h.a(a(e2), e2);
                    }
                }
            }
        }
        if (this.D.k) {
            s();
            boolean z3 = this.D.f;
            this.D.f = false;
            this.n.c(this.e, this.D);
            this.D.f = z3;
            for (int i3 = 0; i3 < this.g.b(); i3++) {
                w e3 = e(this.g.b(i3));
                if (!e3.c() && !this.h.d(e3)) {
                    int e4 = f.e(e3);
                    boolean a2 = e3.a(8192);
                    if (!a2) {
                        e4 |= 4096;
                    }
                    f.c a3 = this.z.a(this.D, e3, e4, e3.u());
                    if (a2) {
                        a(e3, a3);
                    } else {
                        this.h.b(e3, a3);
                    }
                }
            }
            t();
        } else {
            t();
        }
        m();
        a(false);
        this.D.d = 2;
    }

    private void N() {
        boolean z2;
        e();
        l();
        this.D.a(6);
        this.f.e();
        this.D.e = this.m.a();
        this.D.c = 0;
        this.D.g = false;
        this.n.c(this.e, this.D);
        this.D.f = false;
        this.S = null;
        t tVar = this.D;
        if (!this.D.j || this.z == null) {
            z2 = false;
        } else {
            z2 = true;
        }
        tVar.j = z2;
        this.D.d = 4;
        m();
        a(false);
    }

    private void O() {
        this.D.a(4);
        e();
        l();
        this.D.d = 1;
        if (this.D.j) {
            for (int b2 = this.g.b() - 1; b2 >= 0; b2--) {
                w e2 = e(this.g.b(b2));
                if (!e2.c()) {
                    long a2 = a(e2);
                    f.c a3 = this.z.a(this.D, e2);
                    w a4 = this.h.a(a2);
                    if (a4 == null || a4.c()) {
                        this.h.c(e2, a3);
                    } else {
                        boolean a5 = this.h.a(a4);
                        boolean a6 = this.h.a(e2);
                        if (!a5 || a4 != e2) {
                            f.c b3 = this.h.b(a4);
                            this.h.c(e2, a3);
                            f.c c2 = this.h.c(e2);
                            if (b3 == null) {
                                a(a2, e2, a4);
                            } else {
                                a(a4, e2, b3, c2, a5, a6);
                            }
                        } else {
                            this.h.c(e2, a3);
                        }
                    }
                }
            }
            this.h.a(this.aI);
        }
        this.n.b(this.e);
        this.D.b = this.D.e;
        this.x = false;
        this.y = false;
        this.D.j = false;
        this.D.k = false;
        this.n.u = false;
        if (this.e.b != null) {
            this.e.b.clear();
        }
        if (this.n.y) {
            this.n.x = 0;
            this.n.y = false;
            this.e.b();
        }
        this.n.a(this.D);
        m();
        a(false);
        this.h.a();
        if (j(this.aD[0], this.aD[1])) {
            i(0, 0);
        }
        L();
        J();
    }

    private void a(long j2, w wVar, w wVar2) {
        int b2 = this.g.b();
        for (int i2 = 0; i2 < b2; i2++) {
            w e2 = e(this.g.b(i2));
            if (e2 != wVar && a(e2) == j2) {
                if (this.m == null || !this.m.e()) {
                    throw new IllegalStateException("Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:" + e2 + " \n View Holder 2:" + wVar + a());
                } else {
                    throw new IllegalStateException("Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:" + e2 + " \n View Holder 2:" + wVar + a());
                }
            }
        }
        Log.e("RecyclerView", "Problem while matching changed view holders with the newones. The pre-layout information for the change holder " + wVar2 + " cannot be found but it is necessary for " + wVar + a());
    }

    /* access modifiers changed from: package-private */
    public void a(w wVar, f.c cVar) {
        wVar.a(0, 8192);
        if (this.D.h && wVar.z() && !wVar.q() && !wVar.c()) {
            this.h.a(a(wVar), wVar);
        }
        this.h.a(wVar, cVar);
    }

    private void a(int[] iArr) {
        int b2 = this.g.b();
        if (b2 == 0) {
            iArr[0] = -1;
            iArr[1] = -1;
            return;
        }
        int i2 = Integer.MAX_VALUE;
        int i3 = Integer.MIN_VALUE;
        for (int i4 = 0; i4 < b2; i4++) {
            w e2 = e(this.g.b(i4));
            if (!e2.c()) {
                int d2 = e2.d();
                if (d2 < i2) {
                    i2 = d2;
                }
                if (d2 > i3) {
                    i3 = d2;
                }
            }
        }
        iArr[0] = i2;
        iArr[1] = i3;
    }

    private boolean j(int i2, int i3) {
        a(this.aD);
        return (this.aD[0] == i2 && this.aD[1] == i3) ? false : true;
    }

    /* access modifiers changed from: protected */
    public void removeDetachedView(View view, boolean z2) {
        w e2 = e(view);
        if (e2 != null) {
            if (e2.r()) {
                e2.m();
            } else if (!e2.c()) {
                throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + e2 + a());
            }
        }
        view.clearAnimation();
        k(view);
        super.removeDetachedView(view, z2);
    }

    /* access modifiers changed from: package-private */
    public long a(w wVar) {
        return this.m.e() ? wVar.g() : (long) wVar.c;
    }

    /* access modifiers changed from: package-private */
    public void a(@NonNull w wVar, @Nullable f.c cVar, @NonNull f.c cVar2) {
        wVar.a(false);
        if (this.z.b(wVar, cVar, cVar2)) {
            p();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(@NonNull w wVar, @NonNull f.c cVar, @Nullable f.c cVar2) {
        e(wVar);
        wVar.a(false);
        if (this.z.a(wVar, cVar, cVar2)) {
            p();
        }
    }

    private void a(@NonNull w wVar, @NonNull w wVar2, @NonNull f.c cVar, @NonNull f.c cVar2, boolean z2, boolean z3) {
        wVar.a(false);
        if (z2) {
            e(wVar);
        }
        if (wVar != wVar2) {
            if (z3) {
                e(wVar2);
            }
            wVar.h = wVar2;
            e(wVar);
            this.e.c(wVar);
            wVar2.a(false);
            wVar2.i = wVar;
        }
        if (this.z.a(wVar, wVar2, cVar, cVar2)) {
            p();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        TraceCompat.beginSection("RV OnLayout");
        q();
        TraceCompat.endSection();
        this.t = true;
    }

    public void requestLayout() {
        if (this.W != 0 || this.v) {
            this.u = true;
        } else {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void r() {
        int c2 = this.g.c();
        for (int i2 = 0; i2 < c2; i2++) {
            ((j) this.g.d(i2).getLayoutParams()).e = true;
        }
        this.e.j();
    }

    public void draw(Canvas canvas) {
        boolean z2;
        boolean z3 = true;
        boolean z4 = false;
        super.draw(canvas);
        int size = this.p.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.p.get(i2).a(canvas, this, this.D);
        }
        if (this.ah == null || this.ah.isFinished()) {
            z2 = false;
        } else {
            int save = canvas.save();
            int paddingBottom = this.i ? getPaddingBottom() : 0;
            canvas.rotate(270.0f);
            canvas.translate((float) (paddingBottom + (-getHeight())), 0.0f);
            z2 = this.ah != null && this.ah.draw(canvas);
            canvas.restoreToCount(save);
        }
        if (this.ai != null && !this.ai.isFinished()) {
            int save2 = canvas.save();
            if (this.i) {
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            }
            z2 |= this.ai != null && this.ai.draw(canvas);
            canvas.restoreToCount(save2);
        }
        if (this.aj != null && !this.aj.isFinished()) {
            int save3 = canvas.save();
            int width = getWidth();
            int paddingTop = this.i ? getPaddingTop() : 0;
            canvas.rotate(90.0f);
            canvas.translate((float) (-paddingTop), (float) (-width));
            z2 |= this.aj != null && this.aj.draw(canvas);
            canvas.restoreToCount(save3);
        }
        if (this.ak != null && !this.ak.isFinished()) {
            int save4 = canvas.save();
            canvas.rotate(180.0f);
            if (this.i) {
                canvas.translate((float) ((-getWidth()) + getPaddingRight()), (float) ((-getHeight()) + getPaddingBottom()));
            } else {
                canvas.translate((float) (-getWidth()), (float) (-getHeight()));
            }
            if (this.ak != null && this.ak.draw(canvas)) {
                z4 = true;
            }
            z2 |= z4;
            canvas.restoreToCount(save4);
        }
        if (z2 || this.z == null || this.p.size() <= 0 || !this.z.b()) {
            z3 = z2;
        }
        if (z3) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = this.p.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.p.get(i2).b(canvas, this, this.D);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof j) && this.n.a((j) layoutParams);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        if (this.n != null) {
            return this.n.a();
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + a());
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        if (this.n != null) {
            return this.n.a(getContext(), attributeSet);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + a());
    }

    /* access modifiers changed from: protected */
    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (this.n != null) {
            return this.n.a(layoutParams);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + a());
    }

    /* access modifiers changed from: package-private */
    public void s() {
        int c2 = this.g.c();
        for (int i2 = 0; i2 < c2; i2++) {
            w e2 = e(this.g.d(i2));
            if (!e2.c()) {
                e2.b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void t() {
        int c2 = this.g.c();
        for (int i2 = 0; i2 < c2; i2++) {
            w e2 = e(this.g.d(i2));
            if (!e2.c()) {
                e2.a();
            }
        }
        this.e.i();
    }

    /* access modifiers changed from: package-private */
    public void f(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int c2 = this.g.c();
        if (i2 < i3) {
            i4 = -1;
            i5 = i3;
            i6 = i2;
        } else {
            i4 = 1;
            i5 = i2;
            i6 = i3;
        }
        for (int i7 = 0; i7 < c2; i7++) {
            w e2 = e(this.g.d(i7));
            if (e2 != null && e2.c >= i6 && e2.c <= i5) {
                if (e2.c == i2) {
                    e2.a(i3 - i2, false);
                } else {
                    e2.a(i4, false);
                }
                this.D.f = true;
            }
        }
        this.e.a(i2, i3);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void g(int i2, int i3) {
        int c2 = this.g.c();
        for (int i4 = 0; i4 < c2; i4++) {
            w e2 = e(this.g.d(i4));
            if (e2 != null && !e2.c() && e2.c >= i2) {
                e2.a(i3, false);
                this.D.f = true;
            }
        }
        this.e.b(i2, i3);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, boolean z2) {
        int i4 = i2 + i3;
        int c2 = this.g.c();
        for (int i5 = 0; i5 < c2; i5++) {
            w e2 = e(this.g.d(i5));
            if (e2 != null && !e2.c()) {
                if (e2.c >= i4) {
                    e2.a(-i3, z2);
                    this.D.f = true;
                } else if (e2.c >= i2) {
                    e2.a(i2 - 1, -i3, z2);
                    this.D.f = true;
                }
            }
        }
        this.e.a(i2, i3, z2);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, Object obj) {
        int c2 = this.g.c();
        int i4 = i2 + i3;
        for (int i5 = 0; i5 < c2; i5++) {
            View d2 = this.g.d(i5);
            w e2 = e(d2);
            if (e2 != null && !e2.c() && e2.c >= i2 && e2.c < i4) {
                e2.b(2);
                e2.a(obj);
                ((j) d2.getLayoutParams()).e = true;
            }
        }
        this.e.c(i2, i3);
    }

    /* access modifiers changed from: package-private */
    public boolean b(w wVar) {
        return this.z == null || this.z.a(wVar, wVar.u());
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z2) {
        this.y |= z2;
        this.x = true;
        u();
    }

    /* access modifiers changed from: package-private */
    public void u() {
        int c2 = this.g.c();
        for (int i2 = 0; i2 < c2; i2++) {
            w e2 = e(this.g.d(i2));
            if (e2 != null && !e2.c()) {
                e2.b(6);
            }
        }
        r();
        this.e.h();
    }

    public boolean getPreserveFocusAfterLayout() {
        return this.ay;
    }

    public void setPreserveFocusAfterLayout(boolean z2) {
        this.ay = z2;
    }

    public w b(@NonNull View view) {
        ViewParent parent = view.getParent();
        if (parent == null || parent == this) {
            return e(view);
        }
        throw new IllegalArgumentException("View " + view + " is not a direct child of " + this);
    }

    @Nullable
    public View c(@NonNull View view) {
        ViewParent parent = view.getParent();
        View view2 = view;
        while (parent != null && parent != this && (parent instanceof View)) {
            view2 = (View) parent;
            parent = view2.getParent();
        }
        if (parent == this) {
            return view2;
        }
        return null;
    }

    @Nullable
    public w d(@NonNull View view) {
        View c2 = c(view);
        if (c2 == null) {
            return null;
        }
        return b(c2);
    }

    static w e(View view) {
        if (view == null) {
            return null;
        }
        return ((j) view.getLayoutParams()).c;
    }

    public int f(@NonNull View view) {
        w e2 = e(view);
        if (e2 != null) {
            return e2.d();
        }
        return -1;
    }

    @Nullable
    public w c(int i2) {
        if (this.x) {
            return null;
        }
        int c2 = this.g.c();
        int i3 = 0;
        w wVar = null;
        while (i3 < c2) {
            w e2 = e(this.g.d(i3));
            if (e2 == null || e2.q() || d(e2) != i2) {
                e2 = wVar;
            } else if (!this.g.c(e2.f590a)) {
                return e2;
            }
            i3++;
            wVar = e2;
        }
        return wVar;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public w a(int i2, boolean z2) {
        int c2 = this.g.c();
        w wVar = null;
        for (int i3 = 0; i3 < c2; i3++) {
            w e2 = e(this.g.d(i3));
            if (e2 != null && !e2.q()) {
                if (z2) {
                    if (e2.c != i2) {
                        continue;
                    }
                } else if (e2.d() != i2) {
                    continue;
                }
                if (!this.g.c(e2.f590a)) {
                    return e2;
                }
                wVar = e2;
            }
        }
        return wVar;
    }

    public w a(long j2) {
        if (this.m == null || !this.m.e()) {
            return null;
        }
        int c2 = this.g.c();
        int i2 = 0;
        w wVar = null;
        while (i2 < c2) {
            w e2 = e(this.g.d(i2));
            if (e2 == null || e2.q() || e2.g() != j2) {
                e2 = wVar;
            } else if (!this.g.c(e2.f590a)) {
                return e2;
            }
            i2++;
            wVar = e2;
        }
        return wVar;
    }

    public boolean drawChild(Canvas canvas, View view, long j2) {
        return super.drawChild(canvas, view, j2);
    }

    public void d(@Px int i2) {
        int b2 = this.g.b();
        for (int i3 = 0; i3 < b2; i3++) {
            this.g.b(i3).offsetTopAndBottom(i2);
        }
    }

    public void g(@NonNull View view) {
    }

    public void h(@NonNull View view) {
    }

    public void e(@Px int i2) {
        int b2 = this.g.b();
        for (int i3 = 0; i3 < b2; i3++) {
            this.g.b(i3).offsetLeftAndRight(i2);
        }
    }

    static void a(View view, Rect rect) {
        j jVar = (j) view.getLayoutParams();
        Rect rect2 = jVar.d;
        int left = (view.getLeft() - rect2.left) - jVar.leftMargin;
        int top = (view.getTop() - rect2.top) - jVar.topMargin;
        int right = view.getRight() + rect2.right + jVar.rightMargin;
        int bottom = view.getBottom();
        rect.set(left, top, right, jVar.bottomMargin + rect2.bottom + bottom);
    }

    /* access modifiers changed from: package-private */
    public Rect i(View view) {
        j jVar = (j) view.getLayoutParams();
        if (!jVar.e) {
            return jVar.d;
        }
        if (this.D.a() && (jVar.e() || jVar.c())) {
            return jVar.d;
        }
        Rect rect = jVar.d;
        rect.set(0, 0, 0, 0);
        int size = this.p.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.k.set(0, 0, 0, 0);
            this.p.get(i2).a(this.k, view, this, this.D);
            rect.left += this.k.left;
            rect.top += this.k.top;
            rect.right += this.k.right;
            rect.bottom += this.k.bottom;
        }
        jVar.e = false;
        return rect;
    }

    public void h(@Px int i2, @Px int i3) {
    }

    /* access modifiers changed from: package-private */
    public void i(int i2, int i3) {
        this.af++;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX, scrollY);
        h(i2, i3);
        if (this.az != null) {
            this.az.a(this, i2, i3);
        }
        if (this.aA != null) {
            for (int size = this.aA.size() - 1; size >= 0; size--) {
                this.aA.get(size).a(this, i2, i3);
            }
        }
        this.af--;
    }

    public void f(int i2) {
    }

    /* access modifiers changed from: package-private */
    public void g(int i2) {
        if (this.n != null) {
            this.n.l(i2);
        }
        f(i2);
        if (this.az != null) {
            this.az.a(this, i2);
        }
        if (this.aA != null) {
            for (int size = this.aA.size() - 1; size >= 0; size--) {
                this.aA.get(size).a(this, i2);
            }
        }
    }

    public boolean v() {
        return !this.t || this.x || this.f.d();
    }

    /* access modifiers changed from: package-private */
    public class v implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        OverScroller f589a;
        Interpolator b = RecyclerView.L;
        private int d;
        private int e;
        private boolean f = false;
        private boolean g = false;

        v() {
            this.f589a = new OverScroller(RecyclerView.this.getContext(), RecyclerView.L);
        }

        public void run() {
            int i;
            int i2;
            int i3;
            int i4;
            int i5;
            int i6;
            int i7;
            if (RecyclerView.this.n == null) {
                b();
                return;
            }
            c();
            RecyclerView.this.d();
            OverScroller overScroller = this.f589a;
            s sVar = RecyclerView.this.n.t;
            if (overScroller.computeScrollOffset()) {
                int[] iArr = RecyclerView.this.I;
                int currX = overScroller.getCurrX();
                int currY = overScroller.getCurrY();
                int i8 = currX - this.d;
                int i9 = currY - this.e;
                this.d = currX;
                this.e = currY;
                if (RecyclerView.this.dispatchNestedPreScroll(i8, i9, iArr, null, 1)) {
                    int i10 = i8 - iArr[0];
                    i = i9 - iArr[1];
                    i2 = i10;
                } else {
                    i = i9;
                    i2 = i8;
                }
                if (RecyclerView.this.m != null) {
                    RecyclerView.this.a(i2, i, RecyclerView.this.J);
                    i6 = RecyclerView.this.J[0];
                    i5 = RecyclerView.this.J[1];
                    i4 = i2 - i6;
                    i3 = i - i5;
                    if (sVar != null && !sVar.c() && sVar.d()) {
                        int e2 = RecyclerView.this.D.e();
                        if (e2 == 0) {
                            sVar.b();
                        } else if (sVar.e() >= e2) {
                            sVar.a(e2 - 1);
                            sVar.a(i2 - i4, i - i3);
                        } else {
                            sVar.a(i2 - i4, i - i3);
                        }
                    }
                } else {
                    i3 = 0;
                    i4 = 0;
                    i5 = 0;
                    i6 = 0;
                }
                if (!RecyclerView.this.p.isEmpty()) {
                    RecyclerView.this.invalidate();
                }
                if (RecyclerView.this.getOverScrollMode() != 2) {
                    RecyclerView.this.c(i2, i);
                }
                if (!RecyclerView.this.dispatchNestedScroll(i6, i5, i4, i3, null, 1) && !(i4 == 0 && i3 == 0)) {
                    int currVelocity = (int) overScroller.getCurrVelocity();
                    if (i4 != currX) {
                        i7 = i4 < 0 ? -currVelocity : i4 > 0 ? currVelocity : 0;
                    } else {
                        i7 = 0;
                    }
                    if (i3 == currY) {
                        currVelocity = 0;
                    } else if (i3 < 0) {
                        currVelocity = -currVelocity;
                    } else if (i3 <= 0) {
                        currVelocity = 0;
                    }
                    if (RecyclerView.this.getOverScrollMode() != 2) {
                        RecyclerView.this.d(i7, currVelocity);
                    }
                    if ((i7 != 0 || i4 == currX || overScroller.getFinalX() == 0) && (currVelocity != 0 || i3 == currY || overScroller.getFinalY() == 0)) {
                        overScroller.abortAnimation();
                    }
                }
                if (!(i6 == 0 && i5 == 0)) {
                    RecyclerView.this.i(i6, i5);
                }
                if (!RecyclerView.this.awakenScrollBars()) {
                    RecyclerView.this.invalidate();
                }
                boolean z = (i2 == 0 && i == 0) || (i2 != 0 && RecyclerView.this.n.e() && i6 == i2) || (i != 0 && RecyclerView.this.n.f() && i5 == i);
                if (overScroller.isFinished() || (!z && !RecyclerView.this.hasNestedScrollingParent(1))) {
                    RecyclerView.this.setScrollState(0);
                    if (RecyclerView.d) {
                        RecyclerView.this.C.a();
                    }
                    RecyclerView.this.stopNestedScroll(1);
                } else {
                    a();
                    if (RecyclerView.this.B != null) {
                        RecyclerView.this.B.a(RecyclerView.this, i2, i);
                    }
                }
            }
            if (sVar != null) {
                if (sVar.c()) {
                    sVar.a(0, 0);
                }
                if (!this.g) {
                    sVar.b();
                }
            }
            d();
        }

        private void c() {
            this.g = false;
            this.f = true;
        }

        private void d() {
            this.f = false;
            if (this.g) {
                a();
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (this.f) {
                this.g = true;
                return;
            }
            RecyclerView.this.removeCallbacks(this);
            ViewCompat.postOnAnimation(RecyclerView.this, this);
        }

        public void a(int i, int i2) {
            RecyclerView.this.setScrollState(2);
            this.e = 0;
            this.d = 0;
            this.f589a.fling(0, 0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
            a();
        }

        public void b(int i, int i2) {
            a(i, i2, 0, 0);
        }

        public void a(int i, int i2, int i3, int i4) {
            a(i, i2, b(i, i2, i3, i4));
        }

        private float a(float f2) {
            return (float) Math.sin((double) ((f2 - 0.5f) * 0.47123894f));
        }

        private int b(int i, int i2, int i3, int i4) {
            int i5;
            int i6;
            int abs = Math.abs(i);
            int abs2 = Math.abs(i2);
            boolean z = abs > abs2;
            int sqrt = (int) Math.sqrt((double) ((i3 * i3) + (i4 * i4)));
            int sqrt2 = (int) Math.sqrt((double) ((i * i) + (i2 * i2)));
            int width = z ? RecyclerView.this.getWidth() : RecyclerView.this.getHeight();
            int i7 = width / 2;
            float a2 = (a(Math.min(1.0f, (((float) sqrt2) * 1.0f) / ((float) width))) * ((float) i7)) + ((float) i7);
            if (sqrt > 0) {
                i6 = Math.round(1000.0f * Math.abs(a2 / ((float) sqrt))) * 4;
            } else {
                if (z) {
                    i5 = abs;
                } else {
                    i5 = abs2;
                }
                i6 = (int) (((((float) i5) / ((float) width)) + 1.0f) * 300.0f);
            }
            return Math.min(i6, 2000);
        }

        public void a(int i, int i2, int i3) {
            a(i, i2, i3, RecyclerView.L);
        }

        public void a(int i, int i2, Interpolator interpolator) {
            int b2 = b(i, i2, 0, 0);
            if (interpolator == null) {
                interpolator = RecyclerView.L;
            }
            a(i, i2, b2, interpolator);
        }

        public void a(int i, int i2, int i3, Interpolator interpolator) {
            if (this.b != interpolator) {
                this.b = interpolator;
                this.f589a = new OverScroller(RecyclerView.this.getContext(), interpolator);
            }
            RecyclerView.this.setScrollState(2);
            this.e = 0;
            this.d = 0;
            this.f589a.startScroll(0, 0, i, i2, i3);
            if (Build.VERSION.SDK_INT < 23) {
                this.f589a.computeScrollOffset();
            }
            a();
        }

        public void b() {
            RecyclerView.this.removeCallbacks(this);
            this.f589a.abortAnimation();
        }
    }

    /* access modifiers changed from: package-private */
    public void w() {
        int b2 = this.g.b();
        for (int i2 = 0; i2 < b2; i2++) {
            View b3 = this.g.b(i2);
            w b4 = b(b3);
            if (!(b4 == null || b4.i == null)) {
                View view = b4.i.f590a;
                int left = b3.getLeft();
                int top = b3.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public class r extends c {
        r() {
        }

        @Override // android.support.v7.widget.RecyclerView.c
        public void a() {
            RecyclerView.this.a((String) null);
            RecyclerView.this.D.f = true;
            RecyclerView.this.c(true);
            if (!RecyclerView.this.f.d()) {
                RecyclerView.this.requestLayout();
            }
        }
    }

    public static class e {
        /* access modifiers changed from: protected */
        @NonNull
        public EdgeEffect a(@NonNull RecyclerView recyclerView, int i) {
            return new EdgeEffect(recyclerView.getContext());
        }
    }

    public static class o {

        /* renamed from: a  reason: collision with root package name */
        SparseArray<a> f582a = new SparseArray<>();
        private int b = 0;

        /* access modifiers changed from: package-private */
        public static class a {

            /* renamed from: a  reason: collision with root package name */
            final ArrayList<w> f583a = new ArrayList<>();
            int b = 5;
            long c = 0;
            long d = 0;

            a() {
            }
        }

        public void a() {
            for (int i = 0; i < this.f582a.size(); i++) {
                this.f582a.valueAt(i).f583a.clear();
            }
        }

        @Nullable
        public w a(int i) {
            a aVar = this.f582a.get(i);
            if (aVar == null || aVar.f583a.isEmpty()) {
                return null;
            }
            ArrayList<w> arrayList = aVar.f583a;
            return arrayList.remove(arrayList.size() - 1);
        }

        public void a(w wVar) {
            int h = wVar.h();
            ArrayList<w> arrayList = b(h).f583a;
            if (this.f582a.get(h).b > arrayList.size()) {
                wVar.v();
                arrayList.add(wVar);
            }
        }

        /* access modifiers changed from: package-private */
        public long a(long j, long j2) {
            return j == 0 ? j2 : ((j / 4) * 3) + (j2 / 4);
        }

        /* access modifiers changed from: package-private */
        public void a(int i, long j) {
            a b2 = b(i);
            b2.c = a(b2.c, j);
        }

        /* access modifiers changed from: package-private */
        public void b(int i, long j) {
            a b2 = b(i);
            b2.d = a(b2.d, j);
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i, long j, long j2) {
            long j3 = b(i).c;
            return j3 == 0 || j3 + j < j2;
        }

        /* access modifiers changed from: package-private */
        public boolean b(int i, long j, long j2) {
            long j3 = b(i).d;
            return j3 == 0 || j3 + j < j2;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.b++;
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.b--;
        }

        /* access modifiers changed from: package-private */
        public void a(a aVar, a aVar2, boolean z) {
            if (aVar != null) {
                c();
            }
            if (!z && this.b == 0) {
                a();
            }
            if (aVar2 != null) {
                b();
            }
        }

        private a b(int i) {
            a aVar = this.f582a.get(i);
            if (aVar != null) {
                return aVar;
            }
            a aVar2 = new a();
            this.f582a.put(i, aVar2);
            return aVar2;
        }
    }

    @Nullable
    static RecyclerView j(@NonNull View view) {
        if (!(view instanceof ViewGroup)) {
            return null;
        }
        if (view instanceof RecyclerView) {
            return (RecyclerView) view;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            RecyclerView j2 = j(viewGroup.getChildAt(i2));
            if (j2 != null) {
                return j2;
            }
        }
        return null;
    }

    static void c(@NonNull w wVar) {
        if (wVar.b != null) {
            RecyclerView recyclerView = wVar.b.get();
            while (recyclerView != null) {
                if (recyclerView != wVar.f590a) {
                    ViewParent parent = recyclerView.getParent();
                    recyclerView = parent instanceof View ? (View) parent : null;
                } else {
                    return;
                }
            }
            wVar.b = null;
        }
    }

    /* access modifiers changed from: package-private */
    public long getNanoTime() {
        if (d) {
            return System.nanoTime();
        }
        return 0;
    }

    public final class p {

        /* renamed from: a  reason: collision with root package name */
        final ArrayList<w> f584a = new ArrayList<>();
        ArrayList<w> b = null;
        final ArrayList<w> c = new ArrayList<>();
        int d = 2;
        o e;
        private final List<w> g = Collections.unmodifiableList(this.f584a);
        private int h = 2;
        private u i;

        public p() {
        }

        public void a() {
            this.f584a.clear();
            d();
        }

        public void a(int i2) {
            this.h = i2;
            b();
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.d = (RecyclerView.this.n != null ? RecyclerView.this.n.x : 0) + this.h;
            for (int size = this.c.size() - 1; size >= 0 && this.c.size() > this.d; size--) {
                d(size);
            }
        }

        @NonNull
        public List<w> c() {
            return this.g;
        }

        /* access modifiers changed from: package-private */
        public boolean a(w wVar) {
            if (wVar.q()) {
                return RecyclerView.this.D.a();
            }
            if (wVar.c < 0 || wVar.c >= RecyclerView.this.m.a()) {
                throw new IndexOutOfBoundsException("Inconsistency detected. Invalid view holder adapter position" + wVar + RecyclerView.this.a());
            } else if (RecyclerView.this.D.a() || RecyclerView.this.m.b(wVar.c) == wVar.h()) {
                return !RecyclerView.this.m.e() || wVar.g() == RecyclerView.this.m.a(wVar.c);
            } else {
                return false;
            }
        }

        private boolean a(@NonNull w wVar, int i2, int i3, long j) {
            wVar.p = RecyclerView.this;
            int h2 = wVar.h();
            long nanoTime = RecyclerView.this.getNanoTime();
            if (j != Long.MAX_VALUE && !this.e.b(h2, nanoTime, j)) {
                return false;
            }
            RecyclerView.this.m.b(wVar, i2);
            this.e.b(wVar.h(), RecyclerView.this.getNanoTime() - nanoTime);
            e(wVar);
            if (RecyclerView.this.D.a()) {
                wVar.g = i3;
            }
            return true;
        }

        public int b(int i2) {
            if (i2 >= 0 && i2 < RecyclerView.this.D.e()) {
                return !RecyclerView.this.D.a() ? i2 : RecyclerView.this.f.b(i2);
            }
            throw new IndexOutOfBoundsException("invalid position " + i2 + ". State " + "item count is " + RecyclerView.this.D.e() + RecyclerView.this.a());
        }

        @NonNull
        public View c(int i2) {
            return a(i2, false);
        }

        /* access modifiers changed from: package-private */
        public View a(int i2, boolean z) {
            return a(i2, z, Long.MAX_VALUE).f590a;
        }

        /* access modifiers changed from: package-private */
        @Nullable
        public w a(int i2, boolean z, long j) {
            w wVar;
            boolean z2;
            w wVar2;
            w wVar3;
            boolean z3;
            boolean a2;
            j jVar;
            boolean z4;
            RecyclerView j2;
            View a3;
            boolean z5 = true;
            if (i2 < 0 || i2 >= RecyclerView.this.D.e()) {
                throw new IndexOutOfBoundsException("Invalid item position " + i2 + "(" + i2 + "). Item count:" + RecyclerView.this.D.e() + RecyclerView.this.a());
            }
            if (RecyclerView.this.D.a()) {
                wVar = f(i2);
                z2 = wVar != null;
            } else {
                wVar = null;
                z2 = false;
            }
            if (wVar == null) {
                wVar2 = b(i2, z);
                if (wVar2 != null) {
                    if (!a(wVar2)) {
                        if (!z) {
                            wVar2.b(4);
                            if (wVar2.i()) {
                                RecyclerView.this.removeDetachedView(wVar2.f590a, false);
                                wVar2.j();
                            } else if (wVar2.k()) {
                                wVar2.l();
                            }
                            b(wVar2);
                        }
                        wVar2 = null;
                    } else {
                        z2 = true;
                    }
                }
            } else {
                wVar2 = wVar;
            }
            if (wVar2 == null) {
                int b2 = RecyclerView.this.f.b(i2);
                if (b2 < 0 || b2 >= RecyclerView.this.m.a()) {
                    throw new IndexOutOfBoundsException("Inconsistency detected. Invalid item position " + i2 + "(offset:" + b2 + ")." + "state:" + RecyclerView.this.D.e() + RecyclerView.this.a());
                }
                int b3 = RecyclerView.this.m.b(b2);
                if (!RecyclerView.this.m.e() || (wVar2 = a(RecyclerView.this.m.a(b2), b3, z)) == null) {
                    z4 = z2;
                } else {
                    wVar2.c = b2;
                    z4 = true;
                }
                if (!(wVar2 != null || this.i == null || (a3 = this.i.a(this, i2, b3)) == null)) {
                    wVar2 = RecyclerView.this.b(a3);
                    if (wVar2 == null) {
                        throw new IllegalArgumentException("getViewForPositionAndType returned a view which does not have a ViewHolder" + RecyclerView.this.a());
                    } else if (wVar2.c()) {
                        throw new IllegalArgumentException("getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view." + RecyclerView.this.a());
                    }
                }
                if (wVar2 == null && (wVar2 = g().a(b3)) != null) {
                    wVar2.v();
                    if (RecyclerView.f567a) {
                        f(wVar2);
                    }
                }
                if (wVar2 == null) {
                    long nanoTime = RecyclerView.this.getNanoTime();
                    if (j != Long.MAX_VALUE && !this.e.a(b3, nanoTime, j)) {
                        return null;
                    }
                    wVar2 = RecyclerView.this.m.c(RecyclerView.this, b3);
                    if (RecyclerView.d && (j2 = RecyclerView.j(wVar2.f590a)) != null) {
                        wVar2.b = new WeakReference<>(j2);
                    }
                    this.e.a(b3, RecyclerView.this.getNanoTime() - nanoTime);
                }
                wVar3 = wVar2;
                z3 = z4;
            } else {
                wVar3 = wVar2;
                z3 = z2;
            }
            if (z3 && !RecyclerView.this.D.a() && wVar3.a(8192)) {
                wVar3.a(0, 8192);
                if (RecyclerView.this.D.j) {
                    RecyclerView.this.a(wVar3, RecyclerView.this.z.a(RecyclerView.this.D, wVar3, f.e(wVar3) | 4096, wVar3.u()));
                }
            }
            if (RecyclerView.this.D.a() && wVar3.p()) {
                wVar3.g = i2;
                a2 = false;
            } else if (!wVar3.p() || wVar3.o() || wVar3.n()) {
                a2 = a(wVar3, RecyclerView.this.f.b(i2), i2, j);
            } else {
                a2 = false;
            }
            ViewGroup.LayoutParams layoutParams = wVar3.f590a.getLayoutParams();
            if (layoutParams == null) {
                jVar = (j) RecyclerView.this.generateDefaultLayoutParams();
                wVar3.f590a.setLayoutParams(jVar);
            } else if (!RecyclerView.this.checkLayoutParams(layoutParams)) {
                jVar = (j) RecyclerView.this.generateLayoutParams(layoutParams);
                wVar3.f590a.setLayoutParams(jVar);
            } else {
                jVar = (j) layoutParams;
            }
            jVar.c = wVar3;
            if (!z3 || !a2) {
                z5 = false;
            }
            jVar.f = z5;
            return wVar3;
        }

        private void e(w wVar) {
            if (RecyclerView.this.n()) {
                View view = wVar.f590a;
                if (ViewCompat.getImportantForAccessibility(view) == 0) {
                    ViewCompat.setImportantForAccessibility(view, 1);
                }
                if (!ViewCompat.hasAccessibilityDelegate(view)) {
                    wVar.b(16384);
                    ViewCompat.setAccessibilityDelegate(view, RecyclerView.this.H.b());
                }
            }
        }

        private void f(w wVar) {
            if (wVar.f590a instanceof ViewGroup) {
                a((ViewGroup) wVar.f590a, false);
            }
        }

        private void a(ViewGroup viewGroup, boolean z) {
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (childAt instanceof ViewGroup) {
                    a((ViewGroup) childAt, true);
                }
            }
            if (z) {
                if (viewGroup.getVisibility() == 4) {
                    viewGroup.setVisibility(0);
                    viewGroup.setVisibility(4);
                    return;
                }
                int visibility = viewGroup.getVisibility();
                viewGroup.setVisibility(4);
                viewGroup.setVisibility(visibility);
            }
        }

        public void a(@NonNull View view) {
            w e2 = RecyclerView.e(view);
            if (e2.r()) {
                RecyclerView.this.removeDetachedView(view, false);
            }
            if (e2.i()) {
                e2.j();
            } else if (e2.k()) {
                e2.l();
            }
            b(e2);
        }

        /* access modifiers changed from: package-private */
        public void d() {
            for (int size = this.c.size() - 1; size >= 0; size--) {
                d(size);
            }
            this.c.clear();
            if (RecyclerView.d) {
                RecyclerView.this.C.a();
            }
        }

        /* access modifiers changed from: package-private */
        public void d(int i2) {
            a(this.c.get(i2), true);
            this.c.remove(i2);
        }

        /* access modifiers changed from: package-private */
        public void b(w wVar) {
            boolean z;
            boolean z2 = true;
            if (wVar.i() || wVar.f590a.getParent() != null) {
                throw new IllegalArgumentException("Scrapped or attached views may not be recycled. isScrap:" + wVar.i() + " isAttached:" + (wVar.f590a.getParent() != null) + RecyclerView.this.a());
            } else if (wVar.r()) {
                throw new IllegalArgumentException("Tmp detached view should be removed from RecyclerView before it can be recycled: " + wVar + RecyclerView.this.a());
            } else if (wVar.c()) {
                throw new IllegalArgumentException("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle." + RecyclerView.this.a());
            } else {
                boolean y = wVar.y();
                if ((RecyclerView.this.m != null && y && RecyclerView.this.m.b(wVar)) || wVar.w()) {
                    if (this.d <= 0 || wVar.a(526)) {
                        z = false;
                    } else {
                        int size = this.c.size();
                        if (size >= this.d && size > 0) {
                            d(0);
                            size--;
                        }
                        if (RecyclerView.d && size > 0 && !RecyclerView.this.C.a(wVar.c)) {
                            int i2 = size - 1;
                            while (i2 >= 0) {
                                if (!RecyclerView.this.C.a(this.c.get(i2).c)) {
                                    break;
                                }
                                i2--;
                            }
                            size = i2 + 1;
                        }
                        this.c.add(size, wVar);
                        z = true;
                    }
                    if (!z) {
                        a(wVar, true);
                    } else {
                        z2 = false;
                    }
                } else {
                    z2 = false;
                    z = false;
                }
                RecyclerView.this.h.g(wVar);
                if (!z && !z2 && y) {
                    wVar.p = null;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(@NonNull w wVar, boolean z) {
            RecyclerView.c(wVar);
            if (wVar.a(16384)) {
                wVar.a(0, 16384);
                ViewCompat.setAccessibilityDelegate(wVar.f590a, null);
            }
            if (z) {
                d(wVar);
            }
            wVar.p = null;
            g().a(wVar);
        }

        /* access modifiers changed from: package-private */
        public void b(View view) {
            w e2 = RecyclerView.e(view);
            e2.m = null;
            e2.n = false;
            e2.l();
            b(e2);
        }

        /* access modifiers changed from: package-private */
        public void c(View view) {
            w e2 = RecyclerView.e(view);
            if (!e2.a(12) && e2.z() && !RecyclerView.this.b(e2)) {
                if (this.b == null) {
                    this.b = new ArrayList<>();
                }
                e2.a(this, true);
                this.b.add(e2);
            } else if (!e2.n() || e2.q() || RecyclerView.this.m.e()) {
                e2.a(this, false);
                this.f584a.add(e2);
            } else {
                throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool." + RecyclerView.this.a());
            }
        }

        /* access modifiers changed from: package-private */
        public void c(w wVar) {
            if (wVar.n) {
                this.b.remove(wVar);
            } else {
                this.f584a.remove(wVar);
            }
            wVar.m = null;
            wVar.n = false;
            wVar.l();
        }

        /* access modifiers changed from: package-private */
        public int e() {
            return this.f584a.size();
        }

        /* access modifiers changed from: package-private */
        public View e(int i2) {
            return this.f584a.get(i2).f590a;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.f584a.clear();
            if (this.b != null) {
                this.b.clear();
            }
        }

        /* access modifiers changed from: package-private */
        public w f(int i2) {
            int size;
            int b2;
            if (this.b == null || (size = this.b.size()) == 0) {
                return null;
            }
            for (int i3 = 0; i3 < size; i3++) {
                w wVar = this.b.get(i3);
                if (!wVar.k() && wVar.d() == i2) {
                    wVar.b(32);
                    return wVar;
                }
            }
            if (RecyclerView.this.m.e() && (b2 = RecyclerView.this.f.b(i2)) > 0 && b2 < RecyclerView.this.m.a()) {
                long a2 = RecyclerView.this.m.a(b2);
                for (int i4 = 0; i4 < size; i4++) {
                    w wVar2 = this.b.get(i4);
                    if (!wVar2.k() && wVar2.g() == a2) {
                        wVar2.b(32);
                        return wVar2;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public w b(int i2, boolean z) {
            View c2;
            int size = this.f584a.size();
            for (int i3 = 0; i3 < size; i3++) {
                w wVar = this.f584a.get(i3);
                if (!wVar.k() && wVar.d() == i2 && !wVar.n() && (RecyclerView.this.D.g || !wVar.q())) {
                    wVar.b(32);
                    return wVar;
                }
            }
            if (z || (c2 = RecyclerView.this.g.c(i2)) == null) {
                int size2 = this.c.size();
                for (int i4 = 0; i4 < size2; i4++) {
                    w wVar2 = this.c.get(i4);
                    if (!wVar2.n() && wVar2.d() == i2) {
                        if (z) {
                            return wVar2;
                        } else {
                            this.c.remove(i4);
                            return wVar2;
                        }
                    }
                }
                return null;
            }
            w e2 = RecyclerView.e(c2);
            RecyclerView.this.g.e(c2);
            int b2 = RecyclerView.this.g.b(c2);
            if (b2 == -1) {
                throw new IllegalStateException("layout index should not be -1 after unhiding a view:" + e2 + RecyclerView.this.a());
            }
            RecyclerView.this.g.e(b2);
            c(c2);
            e2.b(8224);
            return e2;
        }

        /* access modifiers changed from: package-private */
        public w a(long j, int i2, boolean z) {
            for (int size = this.f584a.size() - 1; size >= 0; size--) {
                w wVar = this.f584a.get(size);
                if (wVar.g() == j && !wVar.k()) {
                    if (i2 == wVar.h()) {
                        wVar.b(32);
                        if (!wVar.q() || RecyclerView.this.D.a()) {
                            return wVar;
                        }
                        wVar.a(2, 14);
                        return wVar;
                    } else if (!z) {
                        this.f584a.remove(size);
                        RecyclerView.this.removeDetachedView(wVar.f590a, false);
                        b(wVar.f590a);
                    }
                }
            }
            for (int size2 = this.c.size() - 1; size2 >= 0; size2--) {
                w wVar2 = this.c.get(size2);
                if (wVar2.g() == j) {
                    if (i2 == wVar2.h()) {
                        if (z) {
                            return wVar2;
                        }
                        this.c.remove(size2);
                        return wVar2;
                    } else if (!z) {
                        d(size2);
                        return null;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public void d(@NonNull w wVar) {
            if (RecyclerView.this.o != null) {
                RecyclerView.this.o.a(wVar);
            }
            if (RecyclerView.this.m != null) {
                RecyclerView.this.m.a(wVar);
            }
            if (RecyclerView.this.D != null) {
                RecyclerView.this.h.g(wVar);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(a aVar, a aVar2, boolean z) {
            a();
            g().a(aVar, aVar2, z);
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3) {
            int i4;
            int i5;
            int i6;
            if (i2 < i3) {
                i4 = -1;
                i5 = i3;
                i6 = i2;
            } else {
                i4 = 1;
                i5 = i2;
                i6 = i3;
            }
            int size = this.c.size();
            for (int i7 = 0; i7 < size; i7++) {
                w wVar = this.c.get(i7);
                if (wVar != null && wVar.c >= i6 && wVar.c <= i5) {
                    if (wVar.c == i2) {
                        wVar.a(i3 - i2, false);
                    } else {
                        wVar.a(i4, false);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i2, int i3) {
            int size = this.c.size();
            for (int i4 = 0; i4 < size; i4++) {
                w wVar = this.c.get(i4);
                if (wVar != null && wVar.c >= i2) {
                    wVar.a(i3, true);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3, boolean z) {
            int i4 = i2 + i3;
            for (int size = this.c.size() - 1; size >= 0; size--) {
                w wVar = this.c.get(size);
                if (wVar != null) {
                    if (wVar.c >= i4) {
                        wVar.a(-i3, z);
                    } else if (wVar.c >= i2) {
                        wVar.b(8);
                        d(size);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(u uVar) {
            this.i = uVar;
        }

        /* access modifiers changed from: package-private */
        public void a(o oVar) {
            if (this.e != null) {
                this.e.c();
            }
            this.e = oVar;
            if (this.e != null && RecyclerView.this.getAdapter() != null) {
                this.e.b();
            }
        }

        /* access modifiers changed from: package-private */
        public o g() {
            if (this.e == null) {
                this.e = new o();
            }
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public void c(int i2, int i3) {
            int i4;
            int i5 = i2 + i3;
            for (int size = this.c.size() - 1; size >= 0; size--) {
                w wVar = this.c.get(size);
                if (wVar != null && (i4 = wVar.c) >= i2 && i4 < i5) {
                    wVar.b(2);
                    d(size);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void h() {
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                w wVar = this.c.get(i2);
                if (wVar != null) {
                    wVar.b(6);
                    wVar.a((Object) null);
                }
            }
            if (RecyclerView.this.m == null || !RecyclerView.this.m.e()) {
                d();
            }
        }

        /* access modifiers changed from: package-private */
        public void i() {
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.c.get(i2).a();
            }
            int size2 = this.f584a.size();
            for (int i3 = 0; i3 < size2; i3++) {
                this.f584a.get(i3).a();
            }
            if (this.b != null) {
                int size3 = this.b.size();
                for (int i4 = 0; i4 < size3; i4++) {
                    this.b.get(i4).a();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void j() {
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                j jVar = (j) this.c.get(i2).f590a.getLayoutParams();
                if (jVar != null) {
                    jVar.e = true;
                }
            }
        }
    }

    public static abstract class a<VH extends w> {

        /* renamed from: a  reason: collision with root package name */
        private final b f574a = new b();
        private boolean b = false;

        public abstract int a();

        public abstract void a(@NonNull VH vh, int i);

        @NonNull
        public abstract VH b(@NonNull ViewGroup viewGroup, int i);

        public void a(@NonNull VH vh, int i, @NonNull List<Object> list) {
            a(vh, i);
        }

        @NonNull
        public final VH c(@NonNull ViewGroup viewGroup, int i) {
            try {
                TraceCompat.beginSection("RV CreateView");
                VH b2 = b(viewGroup, i);
                if (b2.f590a.getParent() != null) {
                    throw new IllegalStateException("ViewHolder views must not be attached when created. Ensure that you are not passing 'true' to the attachToRoot parameter of LayoutInflater.inflate(..., boolean attachToRoot)");
                }
                b2.f = i;
                return b2;
            } finally {
                TraceCompat.endSection();
            }
        }

        public final void b(@NonNull VH vh, int i) {
            vh.c = i;
            if (e()) {
                vh.e = a(i);
            }
            vh.a(1, 519);
            TraceCompat.beginSection("RV OnBindView");
            a(vh, i, vh.u());
            vh.t();
            ViewGroup.LayoutParams layoutParams = vh.f590a.getLayoutParams();
            if (layoutParams instanceof j) {
                ((j) layoutParams).e = true;
            }
            TraceCompat.endSection();
        }

        public int b(int i) {
            return 0;
        }

        public long a(int i) {
            return -1;
        }

        public final boolean e() {
            return this.b;
        }

        public void a(@NonNull VH vh) {
        }

        public boolean b(@NonNull VH vh) {
            return false;
        }

        public void c(@NonNull VH vh) {
        }

        public void d(@NonNull VH vh) {
        }

        public void a(@NonNull c cVar) {
            this.f574a.registerObserver(cVar);
        }

        public void b(@NonNull c cVar) {
            this.f574a.unregisterObserver(cVar);
        }

        public void a(@NonNull RecyclerView recyclerView) {
        }

        public void b(@NonNull RecyclerView recyclerView) {
        }

        public final void f() {
            this.f574a.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void k(View view) {
        w e2 = e(view);
        h(view);
        if (!(this.m == null || e2 == null)) {
            this.m.d(e2);
        }
        if (this.ad != null) {
            for (int size = this.ad.size() - 1; size >= 0; size--) {
                this.ad.get(size).b(view);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void l(View view) {
        w e2 = e(view);
        g(view);
        if (!(this.m == null || e2 == null)) {
            this.m.c(e2);
        }
        if (this.ad != null) {
            for (int size = this.ad.size() - 1; size >= 0; size--) {
                this.ad.get(size).a(view);
            }
        }
    }

    public static abstract class i {

        /* renamed from: a  reason: collision with root package name */
        private final bq.b f578a = new bq.b() {
            /* class android.support.v7.widget.RecyclerView.i.AnonymousClass1 */

            @Override // android.support.v7.widget.bq.b
            public View a(int i) {
                return i.this.i(i);
            }

            @Override // android.support.v7.widget.bq.b
            public int a() {
                return i.this.A();
            }

            @Override // android.support.v7.widget.bq.b
            public int b() {
                return i.this.y() - i.this.C();
            }

            @Override // android.support.v7.widget.bq.b
            public int a(View view) {
                return i.this.h(view) - ((j) view.getLayoutParams()).leftMargin;
            }

            @Override // android.support.v7.widget.bq.b
            public int b(View view) {
                return ((j) view.getLayoutParams()).rightMargin + i.this.j(view);
            }
        };
        private final bq.b b = new bq.b() {
            /* class android.support.v7.widget.RecyclerView.i.AnonymousClass2 */

            @Override // android.support.v7.widget.bq.b
            public View a(int i) {
                return i.this.i(i);
            }

            @Override // android.support.v7.widget.bq.b
            public int a() {
                return i.this.B();
            }

            @Override // android.support.v7.widget.bq.b
            public int b() {
                return i.this.z() - i.this.D();
            }

            @Override // android.support.v7.widget.bq.b
            public int a(View view) {
                return i.this.i(view) - ((j) view.getLayoutParams()).topMargin;
            }

            @Override // android.support.v7.widget.bq.b
            public int b(View view) {
                return ((j) view.getLayoutParams()).bottomMargin + i.this.k(view);
            }
        };
        private boolean c = true;
        private boolean d = true;
        private int e;
        private int f;
        private int g;
        private int h;
        af p;
        RecyclerView q;
        bq r = new bq(this.f578a);
        bq s = new bq(this.b);
        @Nullable
        s t;
        boolean u = false;
        boolean v = false;
        boolean w = false;
        int x;
        boolean y;

        public interface a {
            void b(int i, int i2);
        }

        public static class b {

            /* renamed from: a  reason: collision with root package name */
            public int f581a;
            public int b;
            public boolean c;
            public boolean d;
        }

        public abstract j a();

        /* access modifiers changed from: package-private */
        public void b(RecyclerView recyclerView) {
            if (recyclerView == null) {
                this.q = null;
                this.p = null;
                this.g = 0;
                this.h = 0;
            } else {
                this.q = recyclerView;
                this.p = recyclerView.g;
                this.g = recyclerView.getWidth();
                this.h = recyclerView.getHeight();
            }
            this.e = C.ENCODING_PCM_32BIT;
            this.f = C.ENCODING_PCM_32BIT;
        }

        /* access modifiers changed from: package-private */
        public void c(int i, int i2) {
            this.g = View.MeasureSpec.getSize(i);
            this.e = View.MeasureSpec.getMode(i);
            if (this.e == 0 && !RecyclerView.b) {
                this.g = 0;
            }
            this.h = View.MeasureSpec.getSize(i2);
            this.f = View.MeasureSpec.getMode(i2);
            if (this.f == 0 && !RecyclerView.b) {
                this.h = 0;
            }
        }

        /* access modifiers changed from: package-private */
        public void d(int i, int i2) {
            int v2 = v();
            if (v2 == 0) {
                this.q.e(i, i2);
                return;
            }
            int i3 = Integer.MIN_VALUE;
            int i4 = Integer.MIN_VALUE;
            int i5 = Integer.MAX_VALUE;
            int i6 = Integer.MAX_VALUE;
            for (int i7 = 0; i7 < v2; i7++) {
                View i8 = i(i7);
                Rect rect = this.q.k;
                a(i8, rect);
                if (rect.left < i6) {
                    i6 = rect.left;
                }
                if (rect.right > i4) {
                    i4 = rect.right;
                }
                if (rect.top < i5) {
                    i5 = rect.top;
                }
                if (rect.bottom > i3) {
                    i3 = rect.bottom;
                }
            }
            this.q.k.set(i6, i5, i4, i3);
            a(this.q.k, i, i2);
        }

        public void a(Rect rect, int i, int i2) {
            f(a(i, rect.width() + A() + C(), F()), a(i2, rect.height() + B() + D(), G()));
        }

        public void o() {
            if (this.q != null) {
                this.q.requestLayout();
            }
        }

        public static int a(int i, int i2, int i3) {
            int mode = View.MeasureSpec.getMode(i);
            int size = View.MeasureSpec.getSize(i);
            switch (mode) {
                case Integer.MIN_VALUE:
                    return Math.min(size, Math.max(i2, i3));
                case C.ENCODING_PCM_32BIT /*{ENCODED_INT: 1073741824}*/:
                    return size;
                default:
                    return Math.max(i2, i3);
            }
        }

        public void a(String str) {
            if (this.q != null) {
                this.q.a(str);
            }
        }

        public boolean c() {
            return this.w;
        }

        public boolean b() {
            return false;
        }

        public final boolean p() {
            return this.d;
        }

        public void a(int i, int i2, t tVar, a aVar) {
        }

        public void a(int i, a aVar) {
        }

        /* access modifiers changed from: package-private */
        public void c(RecyclerView recyclerView) {
            this.v = true;
            d(recyclerView);
        }

        /* access modifiers changed from: package-private */
        public void b(RecyclerView recyclerView, p pVar) {
            this.v = false;
            a(recyclerView, pVar);
        }

        public boolean q() {
            return this.v;
        }

        public boolean a(Runnable runnable) {
            if (this.q != null) {
                return this.q.removeCallbacks(runnable);
            }
            return false;
        }

        @CallSuper
        public void d(RecyclerView recyclerView) {
        }

        @Deprecated
        public void e(RecyclerView recyclerView) {
        }

        @CallSuper
        public void a(RecyclerView recyclerView, p pVar) {
            e(recyclerView);
        }

        public boolean r() {
            return this.q != null && this.q.i;
        }

        public void c(p pVar, t tVar) {
            Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
        }

        public void a(t tVar) {
        }

        public boolean a(j jVar) {
            return jVar != null;
        }

        public j a(ViewGroup.LayoutParams layoutParams) {
            if (layoutParams instanceof j) {
                return new j((j) layoutParams);
            }
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                return new j((ViewGroup.MarginLayoutParams) layoutParams);
            }
            return new j(layoutParams);
        }

        public j a(Context context, AttributeSet attributeSet) {
            return new j(context, attributeSet);
        }

        public int a(int i, p pVar, t tVar) {
            return 0;
        }

        public int b(int i, p pVar, t tVar) {
            return 0;
        }

        public boolean e() {
            return false;
        }

        public boolean f() {
            return false;
        }

        public void e(int i) {
        }

        public boolean s() {
            return this.t != null && this.t.d();
        }

        public int t() {
            return ViewCompat.getLayoutDirection(this.q);
        }

        public void a(View view) {
            a(view, -1);
        }

        public void a(View view, int i) {
            a(view, i, true);
        }

        public void b(View view) {
            b(view, -1);
        }

        public void b(View view, int i) {
            a(view, i, false);
        }

        private void a(View view, int i, boolean z) {
            w e2 = RecyclerView.e(view);
            if (z || e2.q()) {
                this.q.h.e(e2);
            } else {
                this.q.h.f(e2);
            }
            j jVar = (j) view.getLayoutParams();
            if (e2.k() || e2.i()) {
                if (e2.i()) {
                    e2.j();
                } else {
                    e2.l();
                }
                this.p.a(view, i, view.getLayoutParams(), false);
            } else if (view.getParent() == this.q) {
                int b2 = this.p.b(view);
                if (i == -1) {
                    i = this.p.b();
                }
                if (b2 == -1) {
                    throw new IllegalStateException("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:" + this.q.indexOfChild(view) + this.q.a());
                } else if (b2 != i) {
                    this.q.n.e(b2, i);
                }
            } else {
                this.p.a(view, i, false);
                jVar.e = true;
                if (this.t != null && this.t.d()) {
                    this.t.b(view);
                }
            }
            if (jVar.f) {
                e2.f590a.invalidate();
                jVar.f = false;
            }
        }

        public void c(View view) {
            this.p.a(view);
        }

        public void g(int i) {
            if (i(i) != null) {
                this.p.a(i);
            }
        }

        public int u() {
            return -1;
        }

        public int d(@NonNull View view) {
            return ((j) view.getLayoutParams()).f();
        }

        @Nullable
        public View e(@NonNull View view) {
            View c2;
            if (this.q == null || (c2 = this.q.c(view)) == null || this.p.c(c2)) {
                return null;
            }
            return c2;
        }

        @Nullable
        public View c(int i) {
            int v2 = v();
            for (int i2 = 0; i2 < v2; i2++) {
                View i3 = i(i2);
                w e2 = RecyclerView.e(i3);
                if (e2 != null && e2.d() == i && !e2.c() && (this.q.D.a() || !e2.q())) {
                    return i3;
                }
            }
            return null;
        }

        public void h(int i) {
            a(i, i(i));
        }

        private void a(int i, @NonNull View view) {
            this.p.e(i);
        }

        public void a(@NonNull View view, int i, j jVar) {
            w e2 = RecyclerView.e(view);
            if (e2.q()) {
                this.q.h.e(e2);
            } else {
                this.q.h.f(e2);
            }
            this.p.a(view, i, jVar, e2.q());
        }

        public void c(@NonNull View view, int i) {
            a(view, i, (j) view.getLayoutParams());
        }

        public void e(int i, int i2) {
            View i3 = i(i);
            if (i3 == null) {
                throw new IllegalArgumentException("Cannot move a child from non-existing index:" + i + this.q.toString());
            }
            h(i);
            c(i3, i2);
        }

        public void a(@NonNull View view, @NonNull p pVar) {
            c(view);
            pVar.a(view);
        }

        public void a(int i, @NonNull p pVar) {
            View i2 = i(i);
            g(i);
            pVar.a(i2);
        }

        public int v() {
            if (this.p != null) {
                return this.p.b();
            }
            return 0;
        }

        @Nullable
        public View i(int i) {
            if (this.p != null) {
                return this.p.b(i);
            }
            return null;
        }

        public int w() {
            return this.e;
        }

        public int x() {
            return this.f;
        }

        @Px
        public int y() {
            return this.g;
        }

        @Px
        public int z() {
            return this.h;
        }

        @Px
        public int A() {
            if (this.q != null) {
                return this.q.getPaddingLeft();
            }
            return 0;
        }

        @Px
        public int B() {
            if (this.q != null) {
                return this.q.getPaddingTop();
            }
            return 0;
        }

        @Px
        public int C() {
            if (this.q != null) {
                return this.q.getPaddingRight();
            }
            return 0;
        }

        @Px
        public int D() {
            if (this.q != null) {
                return this.q.getPaddingBottom();
            }
            return 0;
        }

        @Nullable
        public View E() {
            View focusedChild;
            if (this.q == null || (focusedChild = this.q.getFocusedChild()) == null || this.p.c(focusedChild)) {
                return null;
            }
            return focusedChild;
        }

        public void j(@Px int i) {
            if (this.q != null) {
                this.q.e(i);
            }
        }

        public void k(@Px int i) {
            if (this.q != null) {
                this.q.d(i);
            }
        }

        public void a(@NonNull p pVar) {
            for (int v2 = v() - 1; v2 >= 0; v2--) {
                a(pVar, v2, i(v2));
            }
        }

        private void a(p pVar, int i, View view) {
            w e2 = RecyclerView.e(view);
            if (!e2.c()) {
                if (!e2.n() || e2.q() || this.q.m.e()) {
                    h(i);
                    pVar.c(view);
                    this.q.h.h(e2);
                    return;
                }
                g(i);
                pVar.b(e2);
            }
        }

        /* access modifiers changed from: package-private */
        public void b(p pVar) {
            int e2 = pVar.e();
            for (int i = e2 - 1; i >= 0; i--) {
                View e3 = pVar.e(i);
                w e4 = RecyclerView.e(e3);
                if (!e4.c()) {
                    e4.a(false);
                    if (e4.r()) {
                        this.q.removeDetachedView(e3, false);
                    }
                    if (this.q.z != null) {
                        this.q.z.d(e4);
                    }
                    e4.a(true);
                    pVar.b(e3);
                }
            }
            pVar.f();
            if (e2 > 0) {
                this.q.invalidate();
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view, int i, int i2, j jVar) {
            return !this.c || !b(view.getMeasuredWidth(), i, jVar.width) || !b(view.getMeasuredHeight(), i2, jVar.height);
        }

        /* access modifiers changed from: package-private */
        public boolean b(View view, int i, int i2, j jVar) {
            return view.isLayoutRequested() || !this.c || !b(view.getWidth(), i, jVar.width) || !b(view.getHeight(), i2, jVar.height);
        }

        private static boolean b(int i, int i2, int i3) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            if (i3 > 0 && i != i3) {
                return false;
            }
            switch (mode) {
                case Integer.MIN_VALUE:
                    return size >= i;
                case 0:
                    return true;
                case C.ENCODING_PCM_32BIT /*{ENCODED_INT: 1073741824}*/:
                    return size == i;
                default:
                    return false;
            }
        }

        public void a(@NonNull View view, int i, int i2) {
            j jVar = (j) view.getLayoutParams();
            Rect i3 = this.q.i(view);
            int i4 = i3.top;
            int a2 = a(y(), w(), i3.left + i3.right + i + A() + C() + jVar.leftMargin + jVar.rightMargin, jVar.width, e());
            int a3 = a(z(), x(), i3.bottom + i4 + i2 + B() + D() + jVar.topMargin + jVar.bottomMargin, jVar.height, f());
            if (b(view, a2, a3, jVar)) {
                view.measure(a2, a3);
            }
        }

        public static int a(int i, int i2, int i3, int i4, boolean z) {
            int max = Math.max(0, i - i3);
            if (z) {
                if (i4 >= 0) {
                    i2 = 1073741824;
                    max = i4;
                } else if (i4 == -1) {
                    switch (i2) {
                        case Integer.MIN_VALUE:
                        case C.ENCODING_PCM_32BIT /*{ENCODED_INT: 1073741824}*/:
                            break;
                        case 0:
                            i2 = 0;
                            max = 0;
                            break;
                        default:
                            i2 = 0;
                            max = 0;
                            break;
                    }
                } else {
                    if (i4 == -2) {
                        i2 = 0;
                        max = 0;
                    }
                    i2 = 0;
                    max = 0;
                }
            } else if (i4 >= 0) {
                i2 = 1073741824;
                max = i4;
            } else if (i4 != -1) {
                if (i4 == -2) {
                    i2 = (i2 == Integer.MIN_VALUE || i2 == 1073741824) ? Integer.MIN_VALUE : 0;
                }
                i2 = 0;
                max = 0;
            }
            return View.MeasureSpec.makeMeasureSpec(max, i2);
        }

        public int f(@NonNull View view) {
            Rect rect = ((j) view.getLayoutParams()).d;
            return rect.right + view.getMeasuredWidth() + rect.left;
        }

        public int g(@NonNull View view) {
            Rect rect = ((j) view.getLayoutParams()).d;
            return rect.bottom + view.getMeasuredHeight() + rect.top;
        }

        public void a(@NonNull View view, int i, int i2, int i3, int i4) {
            j jVar = (j) view.getLayoutParams();
            Rect rect = jVar.d;
            view.layout(rect.left + i + jVar.leftMargin, rect.top + i2 + jVar.topMargin, (i3 - rect.right) - jVar.rightMargin, (i4 - rect.bottom) - jVar.bottomMargin);
        }

        public void a(@NonNull View view, boolean z, @NonNull Rect rect) {
            Matrix matrix;
            if (z) {
                Rect rect2 = ((j) view.getLayoutParams()).d;
                rect.set(-rect2.left, -rect2.top, view.getWidth() + rect2.right, rect2.bottom + view.getHeight());
            } else {
                rect.set(0, 0, view.getWidth(), view.getHeight());
            }
            if (!(this.q == null || (matrix = view.getMatrix()) == null || matrix.isIdentity())) {
                RectF rectF = this.q.l;
                rectF.set(rect);
                matrix.mapRect(rectF);
                rect.set((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
            }
            rect.offset(view.getLeft(), view.getTop());
        }

        public void a(@NonNull View view, @NonNull Rect rect) {
            RecyclerView.a(view, rect);
        }

        public int h(@NonNull View view) {
            return view.getLeft() - n(view);
        }

        public int i(@NonNull View view) {
            return view.getTop() - l(view);
        }

        public int j(@NonNull View view) {
            return view.getRight() + o(view);
        }

        public int k(@NonNull View view) {
            return view.getBottom() + m(view);
        }

        public void b(@NonNull View view, @NonNull Rect rect) {
            if (this.q == null) {
                rect.set(0, 0, 0, 0);
            } else {
                rect.set(this.q.i(view));
            }
        }

        public int l(@NonNull View view) {
            return ((j) view.getLayoutParams()).d.top;
        }

        public int m(@NonNull View view) {
            return ((j) view.getLayoutParams()).d.bottom;
        }

        public int n(@NonNull View view) {
            return ((j) view.getLayoutParams()).d.left;
        }

        public int o(@NonNull View view) {
            return ((j) view.getLayoutParams()).d.right;
        }

        @Nullable
        public View a(@NonNull View view, int i, @NonNull p pVar, @NonNull t tVar) {
            return null;
        }

        @Nullable
        public View d(@NonNull View view, int i) {
            return null;
        }

        private int[] b(RecyclerView recyclerView, View view, Rect rect, boolean z) {
            int min;
            int i;
            int min2;
            int[] iArr = new int[2];
            int A = A();
            int B = B();
            int y2 = y() - C();
            int z2 = z() - D();
            int left = (view.getLeft() + rect.left) - view.getScrollX();
            int top = (view.getTop() + rect.top) - view.getScrollY();
            int width = left + rect.width();
            int height = top + rect.height();
            int min3 = Math.min(0, left - A);
            int min4 = Math.min(0, top - B);
            int max = Math.max(0, width - y2);
            int max2 = Math.max(0, height - z2);
            if (t() == 1) {
                if (max == 0) {
                    max = Math.max(min3, width - y2);
                }
                i = max;
            } else {
                if (min3 != 0) {
                    min = min3;
                } else {
                    min = Math.min(left - A, max);
                }
                i = min;
            }
            if (min4 != 0) {
                min2 = min4;
            } else {
                min2 = Math.min(top - B, max2);
            }
            iArr[0] = i;
            iArr[1] = min2;
            return iArr;
        }

        public boolean a(@NonNull RecyclerView recyclerView, @NonNull View view, @NonNull Rect rect, boolean z) {
            return a(recyclerView, view, rect, z, false);
        }

        public boolean a(@NonNull RecyclerView recyclerView, @NonNull View view, @NonNull Rect rect, boolean z, boolean z2) {
            int[] b2 = b(recyclerView, view, rect, z);
            int i = b2[0];
            int i2 = b2[1];
            if (z2 && !d(recyclerView, i, i2)) {
                return false;
            }
            if (i == 0 && i2 == 0) {
                return false;
            }
            if (z) {
                recyclerView.scrollBy(i, i2);
            } else {
                recyclerView.a(i, i2);
            }
            return true;
        }

        public boolean a(@NonNull View view, boolean z, boolean z2) {
            boolean z3;
            if (!this.r.a(view, 24579) || !this.s.a(view, 24579)) {
                z3 = false;
            } else {
                z3 = true;
            }
            if (z) {
                return z3;
            }
            return !z3;
        }

        private boolean d(RecyclerView recyclerView, int i, int i2) {
            View focusedChild = recyclerView.getFocusedChild();
            if (focusedChild == null) {
                return false;
            }
            int A = A();
            int B = B();
            int y2 = y() - C();
            int z = z() - D();
            Rect rect = this.q.k;
            a(focusedChild, rect);
            if (rect.left - i >= y2 || rect.right - i <= A || rect.top - i2 >= z || rect.bottom - i2 <= B) {
                return false;
            }
            return true;
        }

        @Deprecated
        public boolean a(@NonNull RecyclerView recyclerView, @NonNull View view, @Nullable View view2) {
            return s() || recyclerView.o();
        }

        public boolean a(@NonNull RecyclerView recyclerView, @NonNull t tVar, @NonNull View view, @Nullable View view2) {
            return a(recyclerView, view, view2);
        }

        public void a(@Nullable a aVar, @Nullable a aVar2) {
        }

        public boolean a(@NonNull RecyclerView recyclerView, @NonNull ArrayList<View> arrayList, int i, int i2) {
            return false;
        }

        public void a(@NonNull RecyclerView recyclerView) {
        }

        public void a(@NonNull RecyclerView recyclerView, int i, int i2) {
        }

        public void b(@NonNull RecyclerView recyclerView, int i, int i2) {
        }

        public void c(@NonNull RecyclerView recyclerView, int i, int i2) {
        }

        public void a(@NonNull RecyclerView recyclerView, int i, int i2, @Nullable Object obj) {
            c(recyclerView, i, i2);
        }

        public void a(@NonNull RecyclerView recyclerView, int i, int i2, int i3) {
        }

        public int e(@NonNull t tVar) {
            return 0;
        }

        public int c(@NonNull t tVar) {
            return 0;
        }

        public int g(@NonNull t tVar) {
            return 0;
        }

        public int f(@NonNull t tVar) {
            return 0;
        }

        public int d(@NonNull t tVar) {
            return 0;
        }

        public int h(@NonNull t tVar) {
            return 0;
        }

        public void a(@NonNull p pVar, @NonNull t tVar, int i, int i2) {
            this.q.e(i, i2);
        }

        public void f(int i, int i2) {
            this.q.setMeasuredDimension(i, i2);
        }

        @Px
        public int F() {
            return ViewCompat.getMinimumWidth(this.q);
        }

        @Px
        public int G() {
            return ViewCompat.getMinimumHeight(this.q);
        }

        @Nullable
        public Parcelable d() {
            return null;
        }

        public void a(Parcelable parcelable) {
        }

        /* access modifiers changed from: package-private */
        public void H() {
            if (this.t != null) {
                this.t.b();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(s sVar) {
            if (this.t == sVar) {
                this.t = null;
            }
        }

        public void l(int i) {
        }

        public void c(@NonNull p pVar) {
            for (int v2 = v() - 1; v2 >= 0; v2--) {
                if (!RecyclerView.e(i(v2)).c()) {
                    a(v2, pVar);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            a(this.q.e, this.q.D, accessibilityNodeInfoCompat);
        }

        public void a(@NonNull p pVar, @NonNull t tVar, @NonNull AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            if (this.q.canScrollVertically(-1) || this.q.canScrollHorizontally(-1)) {
                accessibilityNodeInfoCompat.addAction(8192);
                accessibilityNodeInfoCompat.setScrollable(true);
            }
            if (this.q.canScrollVertically(1) || this.q.canScrollHorizontally(1)) {
                accessibilityNodeInfoCompat.addAction(4096);
                accessibilityNodeInfoCompat.setScrollable(true);
            }
            accessibilityNodeInfoCompat.setCollectionInfo(AccessibilityNodeInfoCompat.CollectionInfoCompat.obtain(a(pVar, tVar), b(pVar, tVar), e(pVar, tVar), d(pVar, tVar)));
        }

        public void a(@NonNull AccessibilityEvent accessibilityEvent) {
            a(this.q.e, this.q.D, accessibilityEvent);
        }

        public void a(@NonNull p pVar, @NonNull t tVar, @NonNull AccessibilityEvent accessibilityEvent) {
            boolean z = true;
            if (this.q != null && accessibilityEvent != null) {
                if (!this.q.canScrollVertically(1) && !this.q.canScrollVertically(-1) && !this.q.canScrollHorizontally(-1) && !this.q.canScrollHorizontally(1)) {
                    z = false;
                }
                accessibilityEvent.setScrollable(z);
                if (this.q.m != null) {
                    accessibilityEvent.setItemCount(this.q.m.a());
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            w e2 = RecyclerView.e(view);
            if (e2 != null && !e2.q() && !this.p.c(e2.f590a)) {
                a(this.q.e, this.q.D, view, accessibilityNodeInfoCompat);
            }
        }

        public void a(@NonNull p pVar, @NonNull t tVar, @NonNull View view, @NonNull AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            int i;
            int d2 = f() ? d(view) : 0;
            if (e()) {
                i = d(view);
            } else {
                i = 0;
            }
            accessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(d2, 1, i, 1, false, false));
        }

        public void I() {
            this.u = true;
        }

        public int d(@NonNull p pVar, @NonNull t tVar) {
            return 0;
        }

        public int a(@NonNull p pVar, @NonNull t tVar) {
            if (this.q == null || this.q.m == null || !f()) {
                return 1;
            }
            return this.q.m.a();
        }

        public int b(@NonNull p pVar, @NonNull t tVar) {
            if (this.q == null || this.q.m == null || !e()) {
                return 1;
            }
            return this.q.m.a();
        }

        public boolean e(@NonNull p pVar, @NonNull t tVar) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i, @Nullable Bundle bundle) {
            return a(this.q.e, this.q.D, i, bundle);
        }

        public boolean a(@NonNull p pVar, @NonNull t tVar, int i, @Nullable Bundle bundle) {
            int i2;
            int i3;
            if (this.q == null) {
                return false;
            }
            switch (i) {
                case 4096:
                    if (this.q.canScrollVertically(1)) {
                        i2 = (z() - B()) - D();
                    } else {
                        i2 = 0;
                    }
                    if (this.q.canScrollHorizontally(1)) {
                        i3 = (y() - A()) - C();
                        break;
                    }
                    i3 = 0;
                    break;
                case 8192:
                    if (this.q.canScrollVertically(-1)) {
                        i2 = -((z() - B()) - D());
                    } else {
                        i2 = 0;
                    }
                    if (this.q.canScrollHorizontally(-1)) {
                        i3 = -((y() - A()) - C());
                        break;
                    }
                    i3 = 0;
                    break;
                default:
                    i3 = 0;
                    i2 = 0;
                    break;
            }
            if (i2 == 0 && i3 == 0) {
                return false;
            }
            this.q.a(i3, i2);
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean a(@NonNull View view, int i, @Nullable Bundle bundle) {
            return a(this.q.e, this.q.D, view, i, bundle);
        }

        public boolean a(@NonNull p pVar, @NonNull t tVar, @NonNull View view, int i, @Nullable Bundle bundle) {
            return false;
        }

        public static b a(@NonNull Context context, @Nullable AttributeSet attributeSet, int i, int i2) {
            b bVar = new b();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.RecyclerView, i, i2);
            bVar.f581a = obtainStyledAttributes.getInt(R.styleable.RecyclerView_android_orientation, 1);
            bVar.b = obtainStyledAttributes.getInt(R.styleable.RecyclerView_spanCount, 1);
            bVar.c = obtainStyledAttributes.getBoolean(R.styleable.RecyclerView_reverseLayout, false);
            bVar.d = obtainStyledAttributes.getBoolean(R.styleable.RecyclerView_stackFromEnd, false);
            obtainStyledAttributes.recycle();
            return bVar;
        }

        /* access modifiers changed from: package-private */
        public void f(RecyclerView recyclerView) {
            c(View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), C.ENCODING_PCM_32BIT), View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), C.ENCODING_PCM_32BIT));
        }

        /* access modifiers changed from: package-private */
        public boolean l() {
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean J() {
            int v2 = v();
            for (int i = 0; i < v2; i++) {
                ViewGroup.LayoutParams layoutParams = i(i).getLayoutParams();
                if (layoutParams.width < 0 && layoutParams.height < 0) {
                    return true;
                }
            }
            return false;
        }
    }

    public static abstract class h {
        public void b(@NonNull Canvas canvas, @NonNull RecyclerView recyclerView, @NonNull t tVar) {
            a(canvas, recyclerView);
        }

        @Deprecated
        public void a(@NonNull Canvas canvas, @NonNull RecyclerView recyclerView) {
        }

        public void a(@NonNull Canvas canvas, @NonNull RecyclerView recyclerView, @NonNull t tVar) {
            b(canvas, recyclerView);
        }

        @Deprecated
        public void b(@NonNull Canvas canvas, @NonNull RecyclerView recyclerView) {
        }

        @Deprecated
        public void a(@NonNull Rect rect, int i, @NonNull RecyclerView recyclerView) {
            rect.set(0, 0, 0, 0);
        }

        public void a(@NonNull Rect rect, @NonNull View view, @NonNull RecyclerView recyclerView, @NonNull t tVar) {
            a(rect, ((j) view.getLayoutParams()).f(), recyclerView);
        }
    }

    public static abstract class n {
        public void a(@NonNull RecyclerView recyclerView, int i) {
        }

        public void a(@NonNull RecyclerView recyclerView, int i, int i2) {
        }
    }

    public static abstract class w {
        private static final List<Object> q = Collections.emptyList();
        @NonNull

        /* renamed from: a  reason: collision with root package name */
        public final View f590a;
        WeakReference<RecyclerView> b;
        int c = -1;
        int d = -1;
        long e = -1;
        int f = -1;
        int g = -1;
        w h = null;
        w i = null;
        int j;
        List<Object> k = null;
        List<Object> l = null;
        p m = null;
        boolean n = false;
        @VisibleForTesting
        int o = -1;
        RecyclerView p;
        private int r = 0;
        private int s = 0;

        public w(@NonNull View view) {
            if (view == null) {
                throw new IllegalArgumentException("itemView may not be null");
            }
            this.f590a = view;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3, boolean z) {
            b(8);
            a(i3, z);
            this.c = i2;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, boolean z) {
            if (this.d == -1) {
                this.d = this.c;
            }
            if (this.g == -1) {
                this.g = this.c;
            }
            if (z) {
                this.g += i2;
            }
            this.c += i2;
            if (this.f590a.getLayoutParams() != null) {
                ((j) this.f590a.getLayoutParams()).e = true;
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.d = -1;
            this.g = -1;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (this.d == -1) {
                this.d = this.c;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean c() {
            return (this.j & 128) != 0;
        }

        public final int d() {
            return this.g == -1 ? this.c : this.g;
        }

        public final int e() {
            if (this.p == null) {
                return -1;
            }
            return this.p.d(this);
        }

        public final int f() {
            return this.d;
        }

        public final long g() {
            return this.e;
        }

        public final int h() {
            return this.f;
        }

        /* access modifiers changed from: package-private */
        public boolean i() {
            return this.m != null;
        }

        /* access modifiers changed from: package-private */
        public void j() {
            this.m.c(this);
        }

        /* access modifiers changed from: package-private */
        public boolean k() {
            return (this.j & 32) != 0;
        }

        /* access modifiers changed from: package-private */
        public void l() {
            this.j &= -33;
        }

        /* access modifiers changed from: package-private */
        public void m() {
            this.j &= -257;
        }

        /* access modifiers changed from: package-private */
        public void a(p pVar, boolean z) {
            this.m = pVar;
            this.n = z;
        }

        /* access modifiers changed from: package-private */
        public boolean n() {
            return (this.j & 4) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean o() {
            return (this.j & 2) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean p() {
            return (this.j & 1) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean q() {
            return (this.j & 8) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i2) {
            return (this.j & i2) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean r() {
            return (this.j & 256) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean s() {
            return (this.j & 512) != 0 || n();
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3) {
            this.j = (this.j & (i3 ^ -1)) | (i2 & i3);
        }

        /* access modifiers changed from: package-private */
        public void b(int i2) {
            this.j |= i2;
        }

        /* access modifiers changed from: package-private */
        public void a(Object obj) {
            if (obj == null) {
                b(1024);
            } else if ((this.j & 1024) == 0) {
                A();
                this.k.add(obj);
            }
        }

        private void A() {
            if (this.k == null) {
                this.k = new ArrayList();
                this.l = Collections.unmodifiableList(this.k);
            }
        }

        /* access modifiers changed from: package-private */
        public void t() {
            if (this.k != null) {
                this.k.clear();
            }
            this.j &= -1025;
        }

        /* access modifiers changed from: package-private */
        public List<Object> u() {
            if ((this.j & 1024) != 0) {
                return q;
            }
            if (this.k == null || this.k.size() == 0) {
                return q;
            }
            return this.l;
        }

        /* access modifiers changed from: package-private */
        public void v() {
            this.j = 0;
            this.c = -1;
            this.d = -1;
            this.e = -1;
            this.g = -1;
            this.r = 0;
            this.h = null;
            this.i = null;
            t();
            this.s = 0;
            this.o = -1;
            RecyclerView.c(this);
        }

        /* access modifiers changed from: package-private */
        public void a(RecyclerView recyclerView) {
            if (this.o != -1) {
                this.s = this.o;
            } else {
                this.s = ViewCompat.getImportantForAccessibility(this.f590a);
            }
            recyclerView.a(this, 4);
        }

        /* access modifiers changed from: package-private */
        public void b(RecyclerView recyclerView) {
            recyclerView.a(this, this.s);
            this.s = 0;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("ViewHolder{" + Integer.toHexString(hashCode()) + " position=" + this.c + " id=" + this.e + ", oldPos=" + this.d + ", pLpos:" + this.g);
            if (i()) {
                sb.append(" scrap ").append(this.n ? "[changeScrap]" : "[attachedScrap]");
            }
            if (n()) {
                sb.append(" invalid");
            }
            if (!p()) {
                sb.append(" unbound");
            }
            if (o()) {
                sb.append(" update");
            }
            if (q()) {
                sb.append(" removed");
            }
            if (c()) {
                sb.append(" ignored");
            }
            if (r()) {
                sb.append(" tmpDetached");
            }
            if (!w()) {
                sb.append(" not recyclable(" + this.r + ")");
            }
            if (s()) {
                sb.append(" undefined adapter position");
            }
            if (this.f590a.getParent() == null) {
                sb.append(" no parent");
            }
            sb.append("}");
            return sb.toString();
        }

        public final void a(boolean z) {
            this.r = z ? this.r - 1 : this.r + 1;
            if (this.r < 0) {
                this.r = 0;
                Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
            } else if (!z && this.r == 1) {
                this.j |= 16;
            } else if (z && this.r == 0) {
                this.j &= -17;
            }
        }

        public final boolean w() {
            return (this.j & 16) == 0 && !ViewCompat.hasTransientState(this.f590a);
        }

        /* access modifiers changed from: package-private */
        public boolean x() {
            return (this.j & 16) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean y() {
            return (this.j & 16) == 0 && ViewCompat.hasTransientState(this.f590a);
        }

        /* access modifiers changed from: package-private */
        public boolean z() {
            return (this.j & 2) != 0;
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean a(w wVar, int i2) {
        if (o()) {
            wVar.o = i2;
            this.K.add(wVar);
            return false;
        }
        ViewCompat.setImportantForAccessibility(wVar.f590a, i2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void x() {
        int i2;
        for (int size = this.K.size() - 1; size >= 0; size--) {
            w wVar = this.K.get(size);
            if (wVar.f590a.getParent() == this && !wVar.c() && (i2 = wVar.o) != -1) {
                ViewCompat.setImportantForAccessibility(wVar.f590a, i2);
                wVar.o = -1;
            }
        }
        this.K.clear();
    }

    /* access modifiers changed from: package-private */
    public int d(w wVar) {
        if (wVar.a(524) || !wVar.p()) {
            return -1;
        }
        return this.f.c(wVar.c);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2) {
        if (stateListDrawable == null || drawable == null || stateListDrawable2 == null || drawable2 == null) {
            throw new IllegalArgumentException("Trying to set fast scroller without both required drawables." + a());
        }
        Resources resources = getContext().getResources();
        new al(this, stateListDrawable, drawable, stateListDrawable2, drawable2, resources.getDimensionPixelSize(R.dimen.fastscroll_default_thickness), resources.getDimensionPixelSize(R.dimen.fastscroll_minimum_range), resources.getDimensionPixelOffset(R.dimen.fastscroll_margin));
    }

    @Override // android.support.v4.view.NestedScrollingChild
    public void setNestedScrollingEnabled(boolean z2) {
        getScrollingChildHelper().setNestedScrollingEnabled(z2);
    }

    @Override // android.support.v4.view.NestedScrollingChild
    public boolean isNestedScrollingEnabled() {
        return getScrollingChildHelper().isNestedScrollingEnabled();
    }

    @Override // android.support.v4.view.NestedScrollingChild
    public boolean startNestedScroll(int i2) {
        return getScrollingChildHelper().startNestedScroll(i2);
    }

    @Override // android.support.v4.view.NestedScrollingChild2
    public boolean startNestedScroll(int i2, int i3) {
        return getScrollingChildHelper().startNestedScroll(i2, i3);
    }

    @Override // android.support.v4.view.NestedScrollingChild
    public void stopNestedScroll() {
        getScrollingChildHelper().stopNestedScroll();
    }

    @Override // android.support.v4.view.NestedScrollingChild2
    public void stopNestedScroll(int i2) {
        getScrollingChildHelper().stopNestedScroll(i2);
    }

    @Override // android.support.v4.view.NestedScrollingChild
    public boolean hasNestedScrollingParent() {
        return getScrollingChildHelper().hasNestedScrollingParent();
    }

    @Override // android.support.v4.view.NestedScrollingChild2
    public boolean hasNestedScrollingParent(int i2) {
        return getScrollingChildHelper().hasNestedScrollingParent(i2);
    }

    @Override // android.support.v4.view.NestedScrollingChild
    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return getScrollingChildHelper().dispatchNestedScroll(i2, i3, i4, i5, iArr);
    }

    @Override // android.support.v4.view.NestedScrollingChild2
    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr, int i6) {
        return getScrollingChildHelper().dispatchNestedScroll(i2, i3, i4, i5, iArr, i6);
    }

    @Override // android.support.v4.view.NestedScrollingChild
    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return getScrollingChildHelper().dispatchNestedPreScroll(i2, i3, iArr, iArr2);
    }

    @Override // android.support.v4.view.NestedScrollingChild2
    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2, int i4) {
        return getScrollingChildHelper().dispatchNestedPreScroll(i2, i3, iArr, iArr2, i4);
    }

    @Override // android.support.v4.view.NestedScrollingChild
    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return getScrollingChildHelper().dispatchNestedFling(f2, f3, z2);
    }

    @Override // android.support.v4.view.NestedScrollingChild
    public boolean dispatchNestedPreFling(float f2, float f3) {
        return getScrollingChildHelper().dispatchNestedPreFling(f2, f3);
    }

    public static class j extends ViewGroup.MarginLayoutParams {
        w c;
        final Rect d = new Rect();
        boolean e = true;
        boolean f = false;

        public j(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public j(int i, int i2) {
            super(i, i2);
        }

        public j(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public j(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public j(j jVar) {
            super((ViewGroup.LayoutParams) jVar);
        }

        public boolean c() {
            return this.c.n();
        }

        public boolean d() {
            return this.c.q();
        }

        public boolean e() {
            return this.c.z();
        }

        public int f() {
            return this.c.d();
        }
    }

    public static abstract class c {
        public void a() {
        }
    }

    public static abstract class s {

        /* renamed from: a  reason: collision with root package name */
        private int f586a;
        private RecyclerView b;
        private i c;
        private boolean d;
        private boolean e;
        private View f;
        private final a g;

        public interface b {
            @Nullable
            PointF d(int i);
        }

        /* access modifiers changed from: protected */
        public abstract void a(@Px int i, @Px int i2, @NonNull t tVar, @NonNull a aVar);

        /* access modifiers changed from: protected */
        public abstract void a(@NonNull View view, @NonNull t tVar, @NonNull a aVar);

        /* access modifiers changed from: protected */
        public abstract void f();

        public void a(int i) {
            this.f586a = i;
        }

        @Nullable
        public PointF b(int i) {
            i a2 = a();
            if (a2 instanceof b) {
                return ((b) a2).d(i);
            }
            Log.w("RecyclerView", "You should override computeScrollVectorForPosition when the LayoutManager does not implement " + b.class.getCanonicalName());
            return null;
        }

        @Nullable
        public i a() {
            return this.c;
        }

        /* access modifiers changed from: protected */
        public final void b() {
            if (this.e) {
                this.e = false;
                f();
                this.b.D.f588a = -1;
                this.f = null;
                this.f586a = -1;
                this.d = false;
                this.c.a(this);
                this.c = null;
                this.b = null;
            }
        }

        public boolean c() {
            return this.d;
        }

        public boolean d() {
            return this.e;
        }

        public int e() {
            return this.f586a;
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2) {
            PointF b2;
            RecyclerView recyclerView = this.b;
            if (!this.e || this.f586a == -1 || recyclerView == null) {
                b();
            }
            if (!(!this.d || this.f != null || this.c == null || (b2 = b(this.f586a)) == null || (b2.x == 0.0f && b2.y == 0.0f))) {
                recyclerView.a((int) Math.signum(b2.x), (int) Math.signum(b2.y), (int[]) null);
            }
            this.d = false;
            if (this.f != null) {
                if (a(this.f) == this.f586a) {
                    a(this.f, recyclerView.D, this.g);
                    this.g.a(recyclerView);
                    b();
                } else {
                    Log.e("RecyclerView", "Passed over target position while smooth scrolling.");
                    this.f = null;
                }
            }
            if (this.e) {
                a(i, i2, recyclerView.D, this.g);
                boolean a2 = this.g.a();
                this.g.a(recyclerView);
                if (!a2) {
                    return;
                }
                if (this.e) {
                    this.d = true;
                    recyclerView.A.a();
                    return;
                }
                b();
            }
        }

        public int a(View view) {
            return this.b.f(view);
        }

        /* access modifiers changed from: protected */
        public void b(View view) {
            if (a(view) == e()) {
                this.f = view;
            }
        }

        public static class a {

            /* renamed from: a  reason: collision with root package name */
            private int f587a;
            private int b;
            private int c;
            private int d;
            private Interpolator e;
            private boolean f;
            private int g;

            /* access modifiers changed from: package-private */
            public boolean a() {
                return this.d >= 0;
            }

            /* access modifiers changed from: package-private */
            public void a(RecyclerView recyclerView) {
                if (this.d >= 0) {
                    int i = this.d;
                    this.d = -1;
                    recyclerView.b(i);
                    this.f = false;
                } else if (this.f) {
                    b();
                    if (this.e != null) {
                        recyclerView.A.a(this.f587a, this.b, this.c, this.e);
                    } else if (this.c == Integer.MIN_VALUE) {
                        recyclerView.A.b(this.f587a, this.b);
                    } else {
                        recyclerView.A.a(this.f587a, this.b, this.c);
                    }
                    this.g++;
                    if (this.g > 10) {
                        Log.e("RecyclerView", "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
                    }
                    this.f = false;
                } else {
                    this.g = 0;
                }
            }

            private void b() {
                if (this.e != null && this.c < 1) {
                    throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
                } else if (this.c < 1) {
                    throw new IllegalStateException("Scroll duration must be a positive number");
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public static class b extends Observable<c> {
        b() {
        }

        public void a() {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((c) this.mObservers.get(size)).a();
            }
        }
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            /* class android.support.v7.widget.RecyclerView.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        Parcelable f573a;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f573a = parcel.readParcelable(classLoader == null ? i.class.getClassLoader() : classLoader);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        @Override // android.support.v4.view.AbsSavedState
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeParcelable(this.f573a, 0);
        }

        /* access modifiers changed from: package-private */
        public void a(SavedState savedState) {
            this.f573a = savedState.f573a;
        }
    }

    public static class t {

        /* renamed from: a  reason: collision with root package name */
        int f588a = -1;
        int b = 0;
        int c = 0;
        int d = 1;
        int e = 0;
        boolean f = false;
        boolean g = false;
        boolean h = false;
        boolean i = false;
        boolean j = false;
        boolean k = false;
        int l;
        long m;
        int n;
        int o;
        int p;
        private SparseArray<Object> q;

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            if ((this.d & i2) == 0) {
                throw new IllegalStateException("Layout state should be one of " + Integer.toBinaryString(i2) + " but it is " + Integer.toBinaryString(this.d));
            }
        }

        /* access modifiers changed from: package-private */
        public void a(a aVar) {
            this.d = 1;
            this.e = aVar.a();
            this.g = false;
            this.h = false;
            this.i = false;
        }

        public boolean a() {
            return this.g;
        }

        public boolean b() {
            return this.k;
        }

        public int c() {
            return this.f588a;
        }

        public boolean d() {
            return this.f588a != -1;
        }

        public int e() {
            return this.g ? this.b - this.c : this.e;
        }

        public String toString() {
            return "State{mTargetPosition=" + this.f588a + ", mData=" + this.q + ", mItemCount=" + this.e + ", mIsMeasuring=" + this.i + ", mPreviousLayoutItemCount=" + this.b + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.c + ", mStructureChanged=" + this.f + ", mInPreLayout=" + this.g + ", mRunSimpleAnimations=" + this.j + ", mRunPredictiveAnimations=" + this.k + '}';
        }
    }

    private class g implements f.b {
        g() {
        }

        @Override // android.support.v7.widget.RecyclerView.f.b
        public void a(w wVar) {
            wVar.a(true);
            if (wVar.h != null && wVar.i == null) {
                wVar.h = null;
            }
            wVar.i = null;
            if (!wVar.x() && !RecyclerView.this.a(wVar.f590a) && wVar.r()) {
                RecyclerView.this.removeDetachedView(wVar.f590a, false);
            }
        }
    }

    public static abstract class f {

        /* renamed from: a  reason: collision with root package name */
        private b f575a = null;
        private ArrayList<a> b = new ArrayList<>();
        private long c = 120;
        private long d = 120;
        private long e = 250;
        private long f = 250;

        public interface a {
            void a();
        }

        /* access modifiers changed from: package-private */
        public interface b {
            void a(@NonNull w wVar);
        }

        public abstract void a();

        public abstract boolean a(@NonNull w wVar, @NonNull c cVar, @Nullable c cVar2);

        public abstract boolean a(@NonNull w wVar, @NonNull w wVar2, @NonNull c cVar, @NonNull c cVar2);

        public abstract boolean b();

        public abstract boolean b(@NonNull w wVar, @Nullable c cVar, @NonNull c cVar2);

        public abstract boolean c(@NonNull w wVar, @NonNull c cVar, @NonNull c cVar2);

        public abstract void d();

        public abstract void d(@NonNull w wVar);

        public long e() {
            return this.e;
        }

        public long f() {
            return this.c;
        }

        public long g() {
            return this.d;
        }

        public long h() {
            return this.f;
        }

        /* access modifiers changed from: package-private */
        public void a(b bVar) {
            this.f575a = bVar;
        }

        @NonNull
        public c a(@NonNull t tVar, @NonNull w wVar, int i, @NonNull List<Object> list) {
            return j().a(wVar);
        }

        @NonNull
        public c a(@NonNull t tVar, @NonNull w wVar) {
            return j().a(wVar);
        }

        static int e(w wVar) {
            int i = wVar.j & 14;
            if (wVar.n()) {
                return 4;
            }
            if ((i & 4) != 0) {
                return i;
            }
            int f2 = wVar.f();
            int e2 = wVar.e();
            if (f2 == -1 || e2 == -1 || f2 == e2) {
                return i;
            }
            return i | 2048;
        }

        public final void f(@NonNull w wVar) {
            g(wVar);
            if (this.f575a != null) {
                this.f575a.a(wVar);
            }
        }

        public void g(@NonNull w wVar) {
        }

        public boolean h(@NonNull w wVar) {
            return true;
        }

        public boolean a(@NonNull w wVar, @NonNull List<Object> list) {
            return h(wVar);
        }

        public final void i() {
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                this.b.get(i).a();
            }
            this.b.clear();
        }

        @NonNull
        public c j() {
            return new c();
        }

        public static class c {

            /* renamed from: a  reason: collision with root package name */
            public int f576a;
            public int b;
            public int c;
            public int d;

            @NonNull
            public c a(@NonNull w wVar) {
                return a(wVar, 0);
            }

            @NonNull
            public c a(@NonNull w wVar, int i) {
                View view = wVar.f590a;
                this.f576a = view.getLeft();
                this.b = view.getTop();
                this.c = view.getRight();
                this.d = view.getBottom();
                return this;
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.aC == null) {
            return super.getChildDrawingOrder(i2, i3);
        }
        return this.aC.a(i2, i3);
    }

    private NestedScrollingChildHelper getScrollingChildHelper() {
        if (this.aE == null) {
            this.aE = new NestedScrollingChildHelper(this);
        }
        return this.aE;
    }
}
