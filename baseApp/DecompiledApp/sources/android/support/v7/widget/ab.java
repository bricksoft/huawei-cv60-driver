package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.View;

@RequiresApi(21)
class ab implements ae {
    ab() {
    }

    @Override // android.support.v7.widget.ae
    public void a(ad adVar, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        adVar.a(new ay(colorStateList, f));
        View d = adVar.d();
        d.setClipToOutline(true);
        d.setElevation(f2);
        b(adVar, f3);
    }

    @Override // android.support.v7.widget.ae
    public void a(ad adVar, float f) {
        j(adVar).a(f);
    }

    @Override // android.support.v7.widget.ae
    public void a() {
    }

    @Override // android.support.v7.widget.ae
    public void b(ad adVar, float f) {
        j(adVar).a(f, adVar.a(), adVar.b());
        f(adVar);
    }

    @Override // android.support.v7.widget.ae
    public float a(ad adVar) {
        return j(adVar).a();
    }

    @Override // android.support.v7.widget.ae
    public float b(ad adVar) {
        return d(adVar) * 2.0f;
    }

    @Override // android.support.v7.widget.ae
    public float c(ad adVar) {
        return d(adVar) * 2.0f;
    }

    @Override // android.support.v7.widget.ae
    public float d(ad adVar) {
        return j(adVar).b();
    }

    @Override // android.support.v7.widget.ae
    public void c(ad adVar, float f) {
        adVar.d().setElevation(f);
    }

    @Override // android.support.v7.widget.ae
    public float e(ad adVar) {
        return adVar.d().getElevation();
    }

    public void f(ad adVar) {
        if (!adVar.a()) {
            adVar.a(0, 0, 0, 0);
            return;
        }
        float a2 = a(adVar);
        float d = d(adVar);
        int ceil = (int) Math.ceil((double) az.b(a2, d, adVar.b()));
        int ceil2 = (int) Math.ceil((double) az.a(a2, d, adVar.b()));
        adVar.a(ceil, ceil2, ceil, ceil2);
    }

    @Override // android.support.v7.widget.ae
    public void g(ad adVar) {
        b(adVar, a(adVar));
    }

    @Override // android.support.v7.widget.ae
    public void h(ad adVar) {
        b(adVar, a(adVar));
    }

    @Override // android.support.v7.widget.ae
    public void a(ad adVar, @Nullable ColorStateList colorStateList) {
        j(adVar).a(colorStateList);
    }

    @Override // android.support.v7.widget.ae
    public ColorStateList i(ad adVar) {
        return j(adVar).c();
    }

    private ay j(ad adVar) {
        return (ay) adVar.c();
    }
}
