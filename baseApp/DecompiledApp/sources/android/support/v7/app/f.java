package android.support.v7.app;

import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.util.LongSparseArray;
import java.lang.reflect.Field;
import java.util.Map;

class f {

    /* renamed from: a  reason: collision with root package name */
    private static Field f467a;
    private static boolean b;
    private static Class c;
    private static boolean d;
    private static Field e;
    private static boolean f;
    private static Field g;
    private static boolean h;

    static void a(@NonNull Resources resources) {
        if (Build.VERSION.SDK_INT < 28) {
            if (Build.VERSION.SDK_INT >= 24) {
                d(resources);
            } else if (Build.VERSION.SDK_INT >= 23) {
                c(resources);
            } else if (Build.VERSION.SDK_INT >= 21) {
                b(resources);
            }
        }
    }

    @RequiresApi(21)
    private static void b(@NonNull Resources resources) {
        Map map;
        if (!b) {
            try {
                f467a = Resources.class.getDeclaredField("mDrawableCache");
                f467a.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", e2);
            }
            b = true;
        }
        if (f467a != null) {
            try {
                map = (Map) f467a.get(resources);
            } catch (IllegalAccessException e3) {
                Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", e3);
                map = null;
            }
            if (map != null) {
                map.clear();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @android.support.annotation.RequiresApi(23)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void c(@android.support.annotation.NonNull android.content.res.Resources r4) {
        /*
            r3 = 1
            boolean r0 = android.support.v7.app.f.b
            if (r0 != 0) goto L_0x0017
            java.lang.Class<android.content.res.Resources> r0 = android.content.res.Resources.class
            java.lang.String r1 = "mDrawableCache"
            java.lang.reflect.Field r0 = r0.getDeclaredField(r1)     // Catch:{ NoSuchFieldException -> 0x0025 }
            android.support.v7.app.f.f467a = r0     // Catch:{ NoSuchFieldException -> 0x0025 }
            java.lang.reflect.Field r0 = android.support.v7.app.f.f467a     // Catch:{ NoSuchFieldException -> 0x0025 }
            r1 = 1
            r0.setAccessible(r1)     // Catch:{ NoSuchFieldException -> 0x0025 }
        L_0x0015:
            android.support.v7.app.f.b = r3
        L_0x0017:
            r1 = 0
            java.lang.reflect.Field r0 = android.support.v7.app.f.f467a
            if (r0 == 0) goto L_0x0036
            java.lang.reflect.Field r0 = android.support.v7.app.f.f467a     // Catch:{ IllegalAccessException -> 0x002e }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ IllegalAccessException -> 0x002e }
        L_0x0022:
            if (r0 != 0) goto L_0x0038
        L_0x0024:
            return
        L_0x0025:
            r0 = move-exception
            java.lang.String r1 = "ResourcesFlusher"
            java.lang.String r2 = "Could not retrieve Resources#mDrawableCache field"
            android.util.Log.e(r1, r2, r0)
            goto L_0x0015
        L_0x002e:
            r0 = move-exception
            java.lang.String r2 = "ResourcesFlusher"
            java.lang.String r3 = "Could not retrieve value from Resources#mDrawableCache"
            android.util.Log.e(r2, r3, r0)
        L_0x0036:
            r0 = r1
            goto L_0x0022
        L_0x0038:
            a(r0)
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.f.c(android.content.res.Resources):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    @android.support.annotation.RequiresApi(24)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void d(@android.support.annotation.NonNull android.content.res.Resources r6) {
        /*
        // Method dump skipped, instructions count: 116
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.f.d(android.content.res.Resources):void");
    }

    @RequiresApi(16)
    private static void a(@NonNull Object obj) {
        LongSparseArray longSparseArray;
        if (!d) {
            try {
                c = Class.forName("android.content.res.ThemedResourceCache");
            } catch (ClassNotFoundException e2) {
                Log.e("ResourcesFlusher", "Could not find ThemedResourceCache class", e2);
            }
            d = true;
        }
        if (c != null) {
            if (!f) {
                try {
                    e = c.getDeclaredField("mUnthemedEntries");
                    e.setAccessible(true);
                } catch (NoSuchFieldException e3) {
                    Log.e("ResourcesFlusher", "Could not retrieve ThemedResourceCache#mUnthemedEntries field", e3);
                }
                f = true;
            }
            if (e != null) {
                try {
                    longSparseArray = (LongSparseArray) e.get(obj);
                } catch (IllegalAccessException e4) {
                    Log.e("ResourcesFlusher", "Could not retrieve value from ThemedResourceCache#mUnthemedEntries", e4);
                    longSparseArray = null;
                }
                if (longSparseArray != null) {
                    longSparseArray.clear();
                }
            }
        }
    }
}
