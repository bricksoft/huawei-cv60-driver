package android.support.v7.app;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.a;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.o;
import android.support.v7.widget.ah;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import java.util.ArrayList;

class g extends a {

    /* renamed from: a  reason: collision with root package name */
    ah f468a;
    Window.Callback b;
    private boolean c;
    private boolean d;
    private ArrayList<a.b> e;
    private final Runnable f;

    @Override // android.support.v7.app.a
    public void a(boolean z) {
    }

    @Override // android.support.v7.app.a
    public void a(float f2) {
        ViewCompat.setElevation(this.f468a.a(), f2);
    }

    @Override // android.support.v7.app.a
    public Context a() {
        return this.f468a.b();
    }

    @Override // android.support.v7.app.a
    public void c(boolean z) {
    }

    @Override // android.support.v7.app.a
    public void d(boolean z) {
    }

    @Override // android.support.v7.app.a
    public void a(CharSequence charSequence) {
        this.f468a.a(charSequence);
    }

    @Override // android.support.v7.app.a
    public boolean b() {
        this.f468a.a().removeCallbacks(this.f);
        ViewCompat.postOnAnimation(this.f468a.a(), this.f);
        return true;
    }

    @Override // android.support.v7.app.a
    public boolean c() {
        if (!this.f468a.c()) {
            return false;
        }
        this.f468a.d();
        return true;
    }

    @Override // android.support.v7.app.a
    public boolean a(int i, KeyEvent keyEvent) {
        boolean z;
        Menu d2 = d();
        if (d2 == null) {
            return false;
        }
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1) {
            z = true;
        } else {
            z = false;
        }
        d2.setQwertyMode(z);
        return d2.performShortcut(i, keyEvent, 0);
    }

    @Override // android.support.v7.app.a
    public void e(boolean z) {
        if (z != this.d) {
            this.d = z;
            int size = this.e.size();
            for (int i = 0; i < size; i++) {
                this.e.get(i).a(z);
            }
        }
    }

    private Menu d() {
        if (!this.c) {
            this.f468a.a(new a(), new b());
            this.c = true;
        }
        return this.f468a.q();
    }

    /* access modifiers changed from: private */
    public final class a implements o.a {
        private boolean b;

        a() {
        }

        @Override // android.support.v7.view.menu.o.a
        public boolean a(h hVar) {
            if (g.this.b == null) {
                return false;
            }
            g.this.b.onMenuOpened(108, hVar);
            return true;
        }

        @Override // android.support.v7.view.menu.o.a
        public void a(h hVar, boolean z) {
            if (!this.b) {
                this.b = true;
                g.this.f468a.n();
                if (g.this.b != null) {
                    g.this.b.onPanelClosed(108, hVar);
                }
                this.b = false;
            }
        }
    }

    /* access modifiers changed from: private */
    public final class b implements h.a {
        b() {
        }

        @Override // android.support.v7.view.menu.h.a
        public boolean a(h hVar, MenuItem menuItem) {
            return false;
        }

        @Override // android.support.v7.view.menu.h.a
        public void a(h hVar) {
            if (g.this.b == null) {
                return;
            }
            if (g.this.f468a.i()) {
                g.this.b.onPanelClosed(108, hVar);
            } else if (g.this.b.onPreparePanel(0, null, hVar)) {
                g.this.b.onMenuOpened(108, hVar);
            }
        }
    }
}
