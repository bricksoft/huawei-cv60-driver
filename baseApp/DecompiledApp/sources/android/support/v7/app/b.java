package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v7.app.AlertController;
import android.support.v7.appcompat.R;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;

public class b extends e implements DialogInterface {

    /* renamed from: a  reason: collision with root package name */
    final AlertController f462a = new AlertController(getContext(), this, getWindow());

    protected b(@NonNull Context context, @StyleRes int i) {
        super(context, a(context, i));
    }

    static int a(@NonNull Context context, @StyleRes int i) {
        if (((i >>> 24) & 255) >= 1) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @Override // android.support.v7.app.e, android.app.Dialog
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f462a.a(charSequence);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.app.e
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f462a.a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.f462a.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.f462a.b(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final AlertController.a f463a;
        private final int b;

        public a(@NonNull Context context) {
            this(context, b.a(context, 0));
        }

        public a(@NonNull Context context, @StyleRes int i) {
            this.f463a = new AlertController.a(new ContextThemeWrapper(context, b.a(context, i)));
            this.b = i;
        }

        @NonNull
        public Context a() {
            return this.f463a.f435a;
        }

        public a a(@Nullable CharSequence charSequence) {
            this.f463a.f = charSequence;
            return this;
        }

        public a a(@Nullable View view) {
            this.f463a.g = view;
            return this;
        }

        public a a(@Nullable Drawable drawable) {
            this.f463a.d = drawable;
            return this;
        }

        public a a(DialogInterface.OnKeyListener onKeyListener) {
            this.f463a.u = onKeyListener;
            return this;
        }

        public a a(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            this.f463a.w = listAdapter;
            this.f463a.x = onClickListener;
            return this;
        }

        public b b() {
            b bVar = new b(this.f463a.f435a, this.b);
            this.f463a.a(bVar.f462a);
            bVar.setCancelable(this.f463a.r);
            if (this.f463a.r) {
                bVar.setCanceledOnTouchOutside(true);
            }
            bVar.setOnCancelListener(this.f463a.s);
            bVar.setOnDismissListener(this.f463a.t);
            if (this.f463a.u != null) {
                bVar.setOnKeyListener(this.f463a.u);
            }
            return bVar;
        }
    }
}
