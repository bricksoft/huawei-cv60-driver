package android.support.v7.app;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    private static int f464a = -1;

    @Nullable
    public abstract <T extends View> T a(@IdRes int i);

    public abstract void a();

    public abstract void a(Bundle bundle);

    public abstract void a(View view);

    public abstract void a(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void a(@Nullable CharSequence charSequence);

    public abstract void b();

    public abstract void b(@LayoutRes int i);

    public abstract void b(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void c();

    public abstract boolean c(int i);

    public abstract boolean d();

    public static d a(Dialog dialog, c cVar) {
        return new AppCompatDelegateImpl(dialog.getContext(), dialog.getWindow(), cVar);
    }

    d() {
    }

    public static int e() {
        return f464a;
    }
}
