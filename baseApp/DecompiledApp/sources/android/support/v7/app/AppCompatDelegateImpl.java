package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.NavUtils;
import android.support.v4.view.KeyEventDispatcher;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.PointerIconCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.b;
import android.support.v7.view.f;
import android.support.v7.view.i;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.o;
import android.support.v7.view.menu.p;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.ViewStubCompat;
import android.support.v7.widget.ag;
import android.support.v7.widget.am;
import android.support.v7.widget.bk;
import android.support.v7.widget.bp;
import android.support.v7.widget.bs;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.lang.Thread;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

/* access modifiers changed from: package-private */
public class AppCompatDelegateImpl extends d implements h.a, LayoutInflater.Factory2 {
    private static final boolean t = (Build.VERSION.SDK_INT < 21);
    private static final int[] u = {16842836};
    private static boolean v = true;
    private boolean A = true;
    private boolean B;
    private ViewGroup C;
    private TextView D;
    private View E;
    private boolean F;
    private boolean G;
    private boolean H;
    private PanelFeatureState[] I;
    private PanelFeatureState J;
    private boolean K;
    private int L = -100;
    private boolean M;
    private d N;
    private final Runnable O = new Runnable() {
        /* class android.support.v7.app.AppCompatDelegateImpl.AnonymousClass2 */

        public void run() {
            if ((AppCompatDelegateImpl.this.s & 1) != 0) {
                AppCompatDelegateImpl.this.g(0);
            }
            if ((AppCompatDelegateImpl.this.s & 4096) != 0) {
                AppCompatDelegateImpl.this.g(108);
            }
            AppCompatDelegateImpl.this.r = false;
            AppCompatDelegateImpl.this.s = 0;
        }
    };
    private boolean P;
    private Rect Q;
    private Rect R;
    private AppCompatViewInflater S;

    /* renamed from: a  reason: collision with root package name */
    final Context f441a;
    final Window b;
    final Window.Callback c;
    final Window.Callback d;
    final c e;
    a f;
    android.support.v7.view.b g;
    ActionBarContextView h;
    PopupWindow i;
    Runnable j;
    ViewPropertyAnimatorCompat k = null;
    boolean l;
    boolean m;
    boolean n;
    boolean o;
    boolean p;
    boolean q;
    boolean r;
    int s;
    private CharSequence w;
    private ag x;
    private a y;
    private f z;

    static {
        if (t && !v) {
            final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                /* class android.support.v7.app.AppCompatDelegateImpl.AnonymousClass1 */

                public void uncaughtException(Thread thread, Throwable th) {
                    if (a(th)) {
                        Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                        notFoundException.initCause(th.getCause());
                        notFoundException.setStackTrace(th.getStackTrace());
                        defaultUncaughtExceptionHandler.uncaughtException(thread, notFoundException);
                        return;
                    }
                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                }

                private boolean a(Throwable th) {
                    String message;
                    if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                        return false;
                    }
                    if (message.contains("drawable") || message.contains("Drawable")) {
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    AppCompatDelegateImpl(Context context, Window window, c cVar) {
        this.f441a = context;
        this.b = window;
        this.e = cVar;
        this.c = this.b.getCallback();
        if (this.c instanceof c) {
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        this.d = new c(this.c);
        this.b.setCallback(this.d);
        bk a2 = bk.a(context, (AttributeSet) null, u);
        Drawable b2 = a2.b(0);
        if (b2 != null) {
            this.b.setBackgroundDrawable(b2);
        }
        a2.a();
    }

    @Override // android.support.v7.app.d
    public void a(Bundle bundle) {
        String str;
        if (this.c instanceof Activity) {
            try {
                str = NavUtils.getParentActivityName((Activity) this.c);
            } catch (IllegalArgumentException e2) {
                str = null;
            }
            if (str != null) {
                a g2 = g();
                if (g2 == null) {
                    this.P = true;
                } else {
                    g2.c(true);
                }
            }
        }
        if (bundle != null && this.L == -100) {
            this.L = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    public a f() {
        p();
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final a g() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final Window.Callback h() {
        return this.b.getCallback();
    }

    private void p() {
        q();
        if (this.l && this.f == null) {
            if (this.c instanceof Activity) {
                this.f = new j((Activity) this.c, this.m);
            } else if (this.c instanceof Dialog) {
                this.f = new j((Dialog) this.c);
            }
            if (this.f != null) {
                this.f.c(this.P);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final Context i() {
        Context context = null;
        a f2 = f();
        if (f2 != null) {
            context = f2.a();
        }
        if (context == null) {
            return this.f441a;
        }
        return context;
    }

    @Override // android.support.v7.app.d
    @Nullable
    public <T extends View> T a(@IdRes int i2) {
        q();
        return (T) this.b.findViewById(i2);
    }

    @Override // android.support.v7.app.d
    public void a() {
        a f2 = f();
        if (f2 != null) {
            f2.d(false);
        }
        if (this.N != null) {
            this.N.d();
        }
    }

    @Override // android.support.v7.app.d
    public void a(View view) {
        q();
        ViewGroup viewGroup = (ViewGroup) this.C.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.c.onContentChanged();
    }

    @Override // android.support.v7.app.d
    public void b(int i2) {
        q();
        ViewGroup viewGroup = (ViewGroup) this.C.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f441a).inflate(i2, viewGroup);
        this.c.onContentChanged();
    }

    @Override // android.support.v7.app.d
    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        q();
        ViewGroup viewGroup = (ViewGroup) this.C.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.c.onContentChanged();
    }

    @Override // android.support.v7.app.d
    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        q();
        ((ViewGroup) this.C.findViewById(16908290)).addView(view, layoutParams);
        this.c.onContentChanged();
    }

    private void q() {
        if (!this.B) {
            this.C = r();
            CharSequence j2 = j();
            if (!TextUtils.isEmpty(j2)) {
                if (this.x != null) {
                    this.x.setWindowTitle(j2);
                } else if (g() != null) {
                    g().a(j2);
                } else if (this.D != null) {
                    this.D.setText(j2);
                }
            }
            s();
            a(this.C);
            this.B = true;
            PanelFeatureState a2 = a(0, false);
            if (this.q) {
                return;
            }
            if (a2 == null || a2.j == null) {
                j(108);
            }
        }
    }

    private ViewGroup r() {
        ViewGroup viewGroup;
        ViewGroup viewGroup2;
        Context context;
        TypedArray obtainStyledAttributes = this.f441a.obtainStyledAttributes(R.styleable.AppCompatTheme);
        if (!obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowActionBar)) {
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowNoTitle, false)) {
            c(1);
        } else if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowActionBar, false)) {
            c(108);
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowActionBarOverlay, false)) {
            c(109);
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowActionModeOverlay, false)) {
            c(10);
        }
        this.o = obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_android_windowIsFloating, false);
        obtainStyledAttributes.recycle();
        this.b.getDecorView();
        LayoutInflater from = LayoutInflater.from(this.f441a);
        if (this.p) {
            if (this.n) {
                viewGroup = (ViewGroup) from.inflate(R.layout.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
            } else {
                viewGroup = (ViewGroup) from.inflate(R.layout.abc_screen_simple, (ViewGroup) null);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                ViewCompat.setOnApplyWindowInsetsListener(viewGroup, new OnApplyWindowInsetsListener() {
                    /* class android.support.v7.app.AppCompatDelegateImpl.AnonymousClass3 */

                    @Override // android.support.v4.view.OnApplyWindowInsetsListener
                    public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                        int systemWindowInsetTop = windowInsetsCompat.getSystemWindowInsetTop();
                        int h = AppCompatDelegateImpl.this.h(systemWindowInsetTop);
                        if (systemWindowInsetTop != h) {
                            windowInsetsCompat = windowInsetsCompat.replaceSystemWindowInsets(windowInsetsCompat.getSystemWindowInsetLeft(), h, windowInsetsCompat.getSystemWindowInsetRight(), windowInsetsCompat.getSystemWindowInsetBottom());
                        }
                        return ViewCompat.onApplyWindowInsets(view, windowInsetsCompat);
                    }
                });
                viewGroup2 = viewGroup;
            } else {
                ((am) viewGroup).setOnFitSystemWindowsListener(new am.a() {
                    /* class android.support.v7.app.AppCompatDelegateImpl.AnonymousClass4 */

                    @Override // android.support.v7.widget.am.a
                    public void a(Rect rect) {
                        rect.top = AppCompatDelegateImpl.this.h(rect.top);
                    }
                });
                viewGroup2 = viewGroup;
            }
        } else if (this.o) {
            this.m = false;
            this.l = false;
            viewGroup2 = (ViewGroup) from.inflate(R.layout.abc_dialog_title_material, (ViewGroup) null);
        } else if (this.l) {
            TypedValue typedValue = new TypedValue();
            this.f441a.getTheme().resolveAttribute(R.attr.actionBarTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                context = new android.support.v7.view.d(this.f441a, typedValue.resourceId);
            } else {
                context = this.f441a;
            }
            ViewGroup viewGroup3 = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.abc_screen_toolbar, (ViewGroup) null);
            this.x = (ag) viewGroup3.findViewById(R.id.decor_content_parent);
            this.x.setWindowCallback(h());
            if (this.m) {
                this.x.a(109);
            }
            if (this.F) {
                this.x.a(2);
            }
            if (this.G) {
                this.x.a(5);
            }
            viewGroup2 = viewGroup3;
        } else {
            viewGroup2 = null;
        }
        if (viewGroup2 == null) {
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.l + ", windowActionBarOverlay: " + this.m + ", android:windowIsFloating: " + this.o + ", windowActionModeOverlay: " + this.n + ", windowNoTitle: " + this.p + " }");
        }
        if (this.x == null) {
            this.D = (TextView) viewGroup2.findViewById(R.id.title);
        }
        bs.b(viewGroup2);
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup2.findViewById(R.id.action_bar_activity_content);
        ViewGroup viewGroup4 = (ViewGroup) this.b.findViewById(16908290);
        if (viewGroup4 != null) {
            while (viewGroup4.getChildCount() > 0) {
                View childAt = viewGroup4.getChildAt(0);
                viewGroup4.removeViewAt(0);
                contentFrameLayout.addView(childAt);
            }
            viewGroup4.setId(-1);
            contentFrameLayout.setId(16908290);
            if (viewGroup4 instanceof FrameLayout) {
                ((FrameLayout) viewGroup4).setForeground(null);
            }
        }
        this.b.setContentView(viewGroup2);
        contentFrameLayout.setAttachListener(new ContentFrameLayout.a() {
            /* class android.support.v7.app.AppCompatDelegateImpl.AnonymousClass5 */

            @Override // android.support.v7.widget.ContentFrameLayout.a
            public void a() {
            }

            @Override // android.support.v7.widget.ContentFrameLayout.a
            public void b() {
                AppCompatDelegateImpl.this.o();
            }
        });
        return viewGroup2;
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup) {
    }

    private void s() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.C.findViewById(16908290);
        View decorView = this.b.getDecorView();
        contentFrameLayout.a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.f441a.obtainStyledAttributes(R.styleable.AppCompatTheme);
        obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    @Override // android.support.v7.app.d
    public boolean c(int i2) {
        int k2 = k(i2);
        if (this.p && k2 == 108) {
            return false;
        }
        if (this.l && k2 == 1) {
            this.l = false;
        }
        switch (k2) {
            case 1:
                t();
                this.p = true;
                return true;
            case 2:
                t();
                this.F = true;
                return true;
            case 5:
                t();
                this.G = true;
                return true;
            case 10:
                t();
                this.n = true;
                return true;
            case 108:
                t();
                this.l = true;
                return true;
            case 109:
                t();
                this.m = true;
                return true;
            default:
                return this.b.requestFeature(k2);
        }
    }

    @Override // android.support.v7.app.d
    public final void a(CharSequence charSequence) {
        this.w = charSequence;
        if (this.x != null) {
            this.x.setWindowTitle(charSequence);
        } else if (g() != null) {
            g().a(charSequence);
        } else if (this.D != null) {
            this.D.setText(charSequence);
        }
    }

    /* access modifiers changed from: package-private */
    public final CharSequence j() {
        if (this.c instanceof Activity) {
            return ((Activity) this.c).getTitle();
        }
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public void d(int i2) {
        if (i2 == 108) {
            a f2 = f();
            if (f2 != null) {
                f2.e(false);
            }
        } else if (i2 == 0) {
            PanelFeatureState a2 = a(i2, true);
            if (a2.o) {
                a(a2, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e(int i2) {
        a f2;
        if (i2 == 108 && (f2 = f()) != null) {
            f2.e(true);
        }
    }

    @Override // android.support.v7.view.menu.h.a
    public boolean a(h hVar, MenuItem menuItem) {
        PanelFeatureState a2;
        Window.Callback h2 = h();
        if (h2 == null || this.q || (a2 = a((Menu) hVar.q())) == null) {
            return false;
        }
        return h2.onMenuItemSelected(a2.f450a, menuItem);
    }

    @Override // android.support.v7.view.menu.h.a
    public void a(h hVar) {
        a(hVar, true);
    }

    public android.support.v7.view.b a(@NonNull b.a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
        if (this.g != null) {
            this.g.c();
        }
        b bVar = new b(aVar);
        a f2 = f();
        if (f2 != null) {
            this.g = f2.a(bVar);
            if (!(this.g == null || this.e == null)) {
                this.e.a(this.g);
            }
        }
        if (this.g == null) {
            this.g = b(bVar);
        }
        return this.g;
    }

    @Override // android.support.v7.app.d
    public void b() {
        a f2 = f();
        if (f2 == null || !f2.b()) {
            j(0);
        }
    }

    /* access modifiers changed from: package-private */
    public android.support.v7.view.b b(@NonNull b.a aVar) {
        android.support.v7.view.b bVar;
        boolean z2;
        Context context;
        m();
        if (this.g != null) {
            this.g.c();
        }
        if (!(aVar instanceof b)) {
            aVar = new b(aVar);
        }
        if (this.e == null || this.q) {
            bVar = null;
        } else {
            try {
                bVar = this.e.a(aVar);
            } catch (AbstractMethodError e2) {
                bVar = null;
            }
        }
        if (bVar != null) {
            this.g = bVar;
        } else {
            if (this.h == null) {
                if (this.o) {
                    TypedValue typedValue = new TypedValue();
                    Resources.Theme theme = this.f441a.getTheme();
                    theme.resolveAttribute(R.attr.actionBarTheme, typedValue, true);
                    if (typedValue.resourceId != 0) {
                        Resources.Theme newTheme = this.f441a.getResources().newTheme();
                        newTheme.setTo(theme);
                        newTheme.applyStyle(typedValue.resourceId, true);
                        context = new android.support.v7.view.d(this.f441a, 0);
                        context.getTheme().setTo(newTheme);
                    } else {
                        context = this.f441a;
                    }
                    this.h = new ActionBarContextView(context);
                    this.i = new PopupWindow(context, (AttributeSet) null, R.attr.actionModePopupWindowStyle);
                    PopupWindowCompat.setWindowLayoutType(this.i, 2);
                    this.i.setContentView(this.h);
                    this.i.setWidth(-1);
                    context.getTheme().resolveAttribute(R.attr.actionBarSize, typedValue, true);
                    this.h.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, context.getResources().getDisplayMetrics()));
                    this.i.setHeight(-2);
                    this.j = new Runnable() {
                        /* class android.support.v7.app.AppCompatDelegateImpl.AnonymousClass6 */

                        public void run() {
                            AppCompatDelegateImpl.this.i.showAtLocation(AppCompatDelegateImpl.this.h, 55, 0, 0);
                            AppCompatDelegateImpl.this.m();
                            if (AppCompatDelegateImpl.this.k()) {
                                AppCompatDelegateImpl.this.h.setAlpha(0.0f);
                                AppCompatDelegateImpl.this.k = ViewCompat.animate(AppCompatDelegateImpl.this.h).alpha(1.0f);
                                AppCompatDelegateImpl.this.k.setListener(new ViewPropertyAnimatorListenerAdapter() {
                                    /* class android.support.v7.app.AppCompatDelegateImpl.AnonymousClass6.AnonymousClass1 */

                                    @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
                                    public void onAnimationStart(View view) {
                                        AppCompatDelegateImpl.this.h.setVisibility(0);
                                    }

                                    @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
                                    public void onAnimationEnd(View view) {
                                        AppCompatDelegateImpl.this.h.setAlpha(1.0f);
                                        AppCompatDelegateImpl.this.k.setListener(null);
                                        AppCompatDelegateImpl.this.k = null;
                                    }
                                });
                                return;
                            }
                            AppCompatDelegateImpl.this.h.setAlpha(1.0f);
                            AppCompatDelegateImpl.this.h.setVisibility(0);
                        }
                    };
                } else {
                    ViewStubCompat viewStubCompat = (ViewStubCompat) this.C.findViewById(R.id.action_mode_bar_stub);
                    if (viewStubCompat != null) {
                        viewStubCompat.setLayoutInflater(LayoutInflater.from(i()));
                        this.h = (ActionBarContextView) viewStubCompat.a();
                    }
                }
            }
            if (this.h != null) {
                m();
                this.h.c();
                Context context2 = this.h.getContext();
                ActionBarContextView actionBarContextView = this.h;
                if (this.i == null) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                android.support.v7.view.e eVar = new android.support.v7.view.e(context2, actionBarContextView, aVar, z2);
                if (aVar.a(eVar, eVar.b())) {
                    eVar.d();
                    this.h.a(eVar);
                    this.g = eVar;
                    if (k()) {
                        this.h.setAlpha(0.0f);
                        this.k = ViewCompat.animate(this.h).alpha(1.0f);
                        this.k.setListener(new ViewPropertyAnimatorListenerAdapter() {
                            /* class android.support.v7.app.AppCompatDelegateImpl.AnonymousClass7 */

                            @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
                            public void onAnimationStart(View view) {
                                AppCompatDelegateImpl.this.h.setVisibility(0);
                                AppCompatDelegateImpl.this.h.sendAccessibilityEvent(32);
                                if (AppCompatDelegateImpl.this.h.getParent() instanceof View) {
                                    ViewCompat.requestApplyInsets((View) AppCompatDelegateImpl.this.h.getParent());
                                }
                            }

                            @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
                            public void onAnimationEnd(View view) {
                                AppCompatDelegateImpl.this.h.setAlpha(1.0f);
                                AppCompatDelegateImpl.this.k.setListener(null);
                                AppCompatDelegateImpl.this.k = null;
                            }
                        });
                    } else {
                        this.h.setAlpha(1.0f);
                        this.h.setVisibility(0);
                        this.h.sendAccessibilityEvent(32);
                        if (this.h.getParent() instanceof View) {
                            ViewCompat.requestApplyInsets((View) this.h.getParent());
                        }
                    }
                    if (this.i != null) {
                        this.b.getDecorView().post(this.j);
                    }
                } else {
                    this.g = null;
                }
            }
        }
        if (!(this.g == null || this.e == null)) {
            this.e.a(this.g);
        }
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final boolean k() {
        return this.B && this.C != null && ViewCompat.isLaidOut(this.C);
    }

    public boolean l() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public void m() {
        if (this.k != null) {
            this.k.cancel();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean n() {
        if (this.g != null) {
            this.g.c();
            return true;
        }
        a f2 = f();
        if (f2 == null || !f2.c()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, KeyEvent keyEvent) {
        a f2 = f();
        if (f2 != null && f2.a(i2, keyEvent)) {
            return true;
        }
        if (this.J == null || !a(this.J, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.J == null) {
                PanelFeatureState a2 = a(0, true);
                b(a2, keyEvent);
                boolean a3 = a(a2, keyEvent.getKeyCode(), keyEvent, 1);
                a2.m = false;
                if (a3) {
                    return true;
                }
            }
            return false;
        } else if (this.J == null) {
            return true;
        } else {
            this.J.n = true;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(KeyEvent keyEvent) {
        View decorView;
        boolean z2 = true;
        if (((this.c instanceof KeyEventDispatcher.Component) || (this.c instanceof e)) && (decorView = this.b.getDecorView()) != null && KeyEventDispatcher.dispatchBeforeHierarchy(decorView, keyEvent)) {
            return true;
        }
        if (keyEvent.getKeyCode() == 82 && this.c.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? c(keyCode, keyEvent) : b(keyCode, keyEvent);
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2, KeyEvent keyEvent) {
        switch (i2) {
            case 82:
                e(0, keyEvent);
                return true;
            case 4:
                boolean z2 = this.K;
                this.K = false;
                PanelFeatureState a2 = a(0, false);
                if (a2 == null || !a2.o) {
                    if (n()) {
                        return true;
                    }
                } else if (z2) {
                    return true;
                } else {
                    a(a2, true);
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i2, KeyEvent keyEvent) {
        boolean z2 = true;
        switch (i2) {
            case 82:
                d(0, keyEvent);
                return true;
            case 4:
                if ((keyEvent.getFlags() & 128) == 0) {
                    z2 = false;
                }
                this.K = z2;
                break;
        }
        return false;
    }

    public View a(View view, String str, @NonNull Context context, @NonNull AttributeSet attributeSet) {
        boolean z2;
        boolean a2;
        if (this.S == null) {
            String string = this.f441a.obtainStyledAttributes(R.styleable.AppCompatTheme).getString(R.styleable.AppCompatTheme_viewInflaterClass);
            if (string == null || AppCompatViewInflater.class.getName().equals(string)) {
                this.S = new AppCompatViewInflater();
            } else {
                try {
                    this.S = (AppCompatViewInflater) Class.forName(string).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                } catch (Throwable th) {
                    Log.i("AppCompatDelegate", "Failed to instantiate custom view inflater " + string + ". Falling back to default.", th);
                    this.S = new AppCompatViewInflater();
                }
            }
        }
        if (t) {
            if (!(attributeSet instanceof XmlPullParser)) {
                a2 = a((ViewParent) view);
            } else if (((XmlPullParser) attributeSet).getDepth() > 1) {
                a2 = true;
            } else {
                a2 = false;
            }
            z2 = a2;
        } else {
            z2 = false;
        }
        return this.S.createView(view, str, context, attributeSet, z2, t, true, bp.a());
    }

    private boolean a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.b.getDecorView();
        for (ViewParent viewParent2 = viewParent; viewParent2 != null; viewParent2 = viewParent2.getParent()) {
            if (viewParent2 == decorView || !(viewParent2 instanceof View) || ViewCompat.isAttachedToWindow((View) viewParent2)) {
                return false;
            }
        }
        return true;
    }

    @Override // android.support.v7.app.d
    public void c() {
        LayoutInflater from = LayoutInflater.from(this.f441a);
        if (from.getFactory() == null) {
            LayoutInflaterCompat.setFactory2(from, this);
        } else if (!(from.getFactory2() instanceof AppCompatDelegateImpl)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return a(view, str, context, attributeSet);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    private void a(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        ViewGroup.LayoutParams layoutParams;
        ViewGroup.LayoutParams layoutParams2;
        int i2 = -1;
        if (!panelFeatureState.o && !this.q) {
            if (panelFeatureState.f450a == 0) {
                if ((this.f441a.getResources().getConfiguration().screenLayout & 15) == 4) {
                    return;
                }
            }
            Window.Callback h2 = h();
            if (h2 == null || h2.onMenuOpened(panelFeatureState.f450a, panelFeatureState.j)) {
                WindowManager windowManager = (WindowManager) this.f441a.getSystemService("window");
                if (windowManager != null && b(panelFeatureState, keyEvent)) {
                    if (panelFeatureState.g == null || panelFeatureState.q) {
                        if (panelFeatureState.g == null) {
                            if (!a(panelFeatureState) || panelFeatureState.g == null) {
                                return;
                            }
                        } else if (panelFeatureState.q && panelFeatureState.g.getChildCount() > 0) {
                            panelFeatureState.g.removeAllViews();
                        }
                        if (c(panelFeatureState) && panelFeatureState.a()) {
                            ViewGroup.LayoutParams layoutParams3 = panelFeatureState.h.getLayoutParams();
                            if (layoutParams3 == null) {
                                layoutParams = new ViewGroup.LayoutParams(-2, -2);
                            } else {
                                layoutParams = layoutParams3;
                            }
                            panelFeatureState.g.setBackgroundResource(panelFeatureState.b);
                            ViewParent parent = panelFeatureState.h.getParent();
                            if (parent != null && (parent instanceof ViewGroup)) {
                                ((ViewGroup) parent).removeView(panelFeatureState.h);
                            }
                            panelFeatureState.g.addView(panelFeatureState.h, layoutParams);
                            if (!panelFeatureState.h.hasFocus()) {
                                panelFeatureState.h.requestFocus();
                            }
                            i2 = -2;
                        } else {
                            return;
                        }
                    } else if (panelFeatureState.i == null || (layoutParams2 = panelFeatureState.i.getLayoutParams()) == null || layoutParams2.width != -1) {
                        i2 = -2;
                    }
                    panelFeatureState.n = false;
                    WindowManager.LayoutParams layoutParams4 = new WindowManager.LayoutParams(i2, -2, panelFeatureState.d, panelFeatureState.e, PointerIconCompat.TYPE_HAND, 8519680, -3);
                    layoutParams4.gravity = panelFeatureState.c;
                    layoutParams4.windowAnimations = panelFeatureState.f;
                    windowManager.addView(panelFeatureState.g, layoutParams4);
                    panelFeatureState.o = true;
                    return;
                }
                return;
            }
            a(panelFeatureState, true);
        }
    }

    private boolean a(PanelFeatureState panelFeatureState) {
        panelFeatureState.a(i());
        panelFeatureState.g = new e(panelFeatureState.l);
        panelFeatureState.c = 81;
        return true;
    }

    private void a(h hVar, boolean z2) {
        if (this.x == null || !this.x.e() || (ViewConfiguration.get(this.f441a).hasPermanentMenuKey() && !this.x.g())) {
            PanelFeatureState a2 = a(0, true);
            a2.q = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        Window.Callback h2 = h();
        if (this.x.f() && z2) {
            this.x.i();
            if (!this.q) {
                h2.onPanelClosed(108, a(0, true).j);
            }
        } else if (h2 != null && !this.q) {
            if (this.r && (this.s & 1) != 0) {
                this.b.getDecorView().removeCallbacks(this.O);
                this.O.run();
            }
            PanelFeatureState a3 = a(0, true);
            if (a3.j != null && !a3.r && h2.onPreparePanel(0, a3.i, a3.j)) {
                h2.onMenuOpened(108, a3.j);
                this.x.h();
            }
        }
    }

    private boolean b(PanelFeatureState panelFeatureState) {
        Context context;
        Context context2 = this.f441a;
        if ((panelFeatureState.f450a == 0 || panelFeatureState.f450a == 108) && this.x != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context2.getTheme();
            theme.resolveAttribute(R.attr.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context2.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(R.attr.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(R.attr.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context2.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                context = new android.support.v7.view.d(context2, 0);
                context.getTheme().setTo(theme2);
                h hVar = new h(context);
                hVar.a(this);
                panelFeatureState.a(hVar);
                return true;
            }
        }
        context = context2;
        h hVar2 = new h(context);
        hVar2.a(this);
        panelFeatureState.a(hVar2);
        return true;
    }

    private boolean c(PanelFeatureState panelFeatureState) {
        if (panelFeatureState.i != null) {
            panelFeatureState.h = panelFeatureState.i;
            return true;
        } else if (panelFeatureState.j == null) {
            return false;
        } else {
            if (this.z == null) {
                this.z = new f();
            }
            panelFeatureState.h = (View) panelFeatureState.a(this.z);
            return panelFeatureState.h != null;
        }
    }

    private boolean b(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        boolean z2;
        if (this.q) {
            return false;
        }
        if (panelFeatureState.m) {
            return true;
        }
        if (!(this.J == null || this.J == panelFeatureState)) {
            a(this.J, false);
        }
        Window.Callback h2 = h();
        if (h2 != null) {
            panelFeatureState.i = h2.onCreatePanelView(panelFeatureState.f450a);
        }
        boolean z3 = panelFeatureState.f450a == 0 || panelFeatureState.f450a == 108;
        if (z3 && this.x != null) {
            this.x.j();
        }
        if (panelFeatureState.i == null && (!z3 || !(g() instanceof g))) {
            if (panelFeatureState.j == null || panelFeatureState.r) {
                if (panelFeatureState.j == null && (!b(panelFeatureState) || panelFeatureState.j == null)) {
                    return false;
                }
                if (z3 && this.x != null) {
                    if (this.y == null) {
                        this.y = new a();
                    }
                    this.x.a(panelFeatureState.j, this.y);
                }
                panelFeatureState.j.h();
                if (!h2.onCreatePanelMenu(panelFeatureState.f450a, panelFeatureState.j)) {
                    panelFeatureState.a((h) null);
                    if (!z3 || this.x == null) {
                        return false;
                    }
                    this.x.a(null, this.y);
                    return false;
                }
                panelFeatureState.r = false;
            }
            panelFeatureState.j.h();
            if (panelFeatureState.s != null) {
                panelFeatureState.j.d(panelFeatureState.s);
                panelFeatureState.s = null;
            }
            if (!h2.onPreparePanel(0, panelFeatureState.i, panelFeatureState.j)) {
                if (z3 && this.x != null) {
                    this.x.a(null, this.y);
                }
                panelFeatureState.j.i();
                return false;
            }
            if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1) {
                z2 = true;
            } else {
                z2 = false;
            }
            panelFeatureState.p = z2;
            panelFeatureState.j.setQwertyMode(panelFeatureState.p);
            panelFeatureState.j.i();
        }
        panelFeatureState.m = true;
        panelFeatureState.n = false;
        this.J = panelFeatureState;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(h hVar) {
        if (!this.H) {
            this.H = true;
            this.x.k();
            Window.Callback h2 = h();
            if (h2 != null && !this.q) {
                h2.onPanelClosed(108, hVar);
            }
            this.H = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void f(int i2) {
        a(a(i2, true), true);
    }

    /* access modifiers changed from: package-private */
    public void a(PanelFeatureState panelFeatureState, boolean z2) {
        if (!z2 || panelFeatureState.f450a != 0 || this.x == null || !this.x.f()) {
            WindowManager windowManager = (WindowManager) this.f441a.getSystemService("window");
            if (!(windowManager == null || !panelFeatureState.o || panelFeatureState.g == null)) {
                windowManager.removeView(panelFeatureState.g);
                if (z2) {
                    a(panelFeatureState.f450a, panelFeatureState, null);
                }
            }
            panelFeatureState.m = false;
            panelFeatureState.n = false;
            panelFeatureState.o = false;
            panelFeatureState.h = null;
            panelFeatureState.q = true;
            if (this.J == panelFeatureState) {
                this.J = null;
                return;
            }
            return;
        }
        b(panelFeatureState.j);
    }

    private boolean d(int i2, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() == 0) {
            PanelFeatureState a2 = a(i2, true);
            if (!a2.o) {
                return b(a2, keyEvent);
            }
        }
        return false;
    }

    private boolean e(int i2, KeyEvent keyEvent) {
        boolean z2;
        boolean z3 = true;
        if (this.g != null) {
            return false;
        }
        PanelFeatureState a2 = a(i2, true);
        if (i2 != 0 || this.x == null || !this.x.e() || ViewConfiguration.get(this.f441a).hasPermanentMenuKey()) {
            if (a2.o || a2.n) {
                boolean z4 = a2.o;
                a(a2, true);
                z3 = z4;
            } else {
                if (a2.m) {
                    if (a2.r) {
                        a2.m = false;
                        z2 = b(a2, keyEvent);
                    } else {
                        z2 = true;
                    }
                    if (z2) {
                        a(a2, keyEvent);
                    }
                }
                z3 = false;
            }
        } else if (!this.x.f()) {
            if (!this.q && b(a2, keyEvent)) {
                z3 = this.x.h();
            }
            z3 = false;
        } else {
            z3 = this.x.i();
        }
        if (z3) {
            AudioManager audioManager = (AudioManager) this.f441a.getSystemService("audio");
            if (audioManager != null) {
                audioManager.playSoundEffect(0);
            } else {
                Log.w("AppCompatDelegate", "Couldn't get audio manager");
            }
        }
        return z3;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, PanelFeatureState panelFeatureState, Menu menu) {
        if (menu == null) {
            if (panelFeatureState == null && i2 >= 0 && i2 < this.I.length) {
                panelFeatureState = this.I[i2];
            }
            if (panelFeatureState != null) {
                menu = panelFeatureState.j;
            }
        }
        if ((panelFeatureState == null || panelFeatureState.o) && !this.q) {
            this.c.onPanelClosed(i2, menu);
        }
    }

    /* access modifiers changed from: package-private */
    public PanelFeatureState a(Menu menu) {
        PanelFeatureState[] panelFeatureStateArr = this.I;
        int length = panelFeatureStateArr != null ? panelFeatureStateArr.length : 0;
        for (int i2 = 0; i2 < length; i2++) {
            PanelFeatureState panelFeatureState = panelFeatureStateArr[i2];
            if (panelFeatureState != null && panelFeatureState.j == menu) {
                return panelFeatureState;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public PanelFeatureState a(int i2, boolean z2) {
        PanelFeatureState[] panelFeatureStateArr = this.I;
        if (panelFeatureStateArr == null || panelFeatureStateArr.length <= i2) {
            PanelFeatureState[] panelFeatureStateArr2 = new PanelFeatureState[(i2 + 1)];
            if (panelFeatureStateArr != null) {
                System.arraycopy(panelFeatureStateArr, 0, panelFeatureStateArr2, 0, panelFeatureStateArr.length);
            }
            this.I = panelFeatureStateArr2;
            panelFeatureStateArr = panelFeatureStateArr2;
        }
        PanelFeatureState panelFeatureState = panelFeatureStateArr[i2];
        if (panelFeatureState != null) {
            return panelFeatureState;
        }
        PanelFeatureState panelFeatureState2 = new PanelFeatureState(i2);
        panelFeatureStateArr[i2] = panelFeatureState2;
        return panelFeatureState2;
    }

    private boolean a(PanelFeatureState panelFeatureState, int i2, KeyEvent keyEvent, int i3) {
        boolean z2 = false;
        if (!keyEvent.isSystem()) {
            if ((panelFeatureState.m || b(panelFeatureState, keyEvent)) && panelFeatureState.j != null) {
                z2 = panelFeatureState.j.performShortcut(i2, keyEvent, i3);
            }
            if (z2 && (i3 & 1) == 0 && this.x == null) {
                a(panelFeatureState, true);
            }
        }
        return z2;
    }

    private void j(int i2) {
        this.s |= 1 << i2;
        if (!this.r) {
            ViewCompat.postOnAnimation(this.b.getDecorView(), this.O);
            this.r = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void g(int i2) {
        PanelFeatureState a2;
        PanelFeatureState a3 = a(i2, true);
        if (a3.j != null) {
            Bundle bundle = new Bundle();
            a3.j.c(bundle);
            if (bundle.size() > 0) {
                a3.s = bundle;
            }
            a3.j.h();
            a3.j.clear();
        }
        a3.r = true;
        a3.q = true;
        if ((i2 == 108 || i2 == 0) && this.x != null && (a2 = a(0, false)) != null) {
            a2.m = false;
            b(a2, (KeyEvent) null);
        }
    }

    /* access modifiers changed from: package-private */
    public int h(int i2) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        int i3 = 0;
        if (this.h == null || !(this.h.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.h.getLayoutParams();
            if (this.h.isShown()) {
                if (this.Q == null) {
                    this.Q = new Rect();
                    this.R = new Rect();
                }
                Rect rect = this.Q;
                Rect rect2 = this.R;
                rect.set(0, i2, 0, 0);
                bs.a(this.C, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i2 : 0)) {
                    marginLayoutParams.topMargin = i2;
                    if (this.E == null) {
                        this.E = new View(this.f441a);
                        this.E.setBackgroundColor(this.f441a.getResources().getColor(R.color.abc_input_method_navigation_guard));
                        this.C.addView(this.E, -1, new ViewGroup.LayoutParams(-1, i2));
                        z3 = true;
                    } else {
                        ViewGroup.LayoutParams layoutParams = this.E.getLayoutParams();
                        if (layoutParams.height != i2) {
                            layoutParams.height = i2;
                            this.E.setLayoutParams(layoutParams);
                        }
                        z3 = true;
                    }
                } else {
                    z3 = false;
                }
                if (this.E == null) {
                    z5 = false;
                }
                if (!this.n && z5) {
                    i2 = 0;
                }
                z4 = z5;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z3 = true;
                z4 = false;
            } else {
                z3 = false;
                z4 = false;
            }
            if (z3) {
                this.h.setLayoutParams(marginLayoutParams);
            }
            z2 = z4;
        }
        if (this.E != null) {
            View view = this.E;
            if (!z2) {
                i3 = 8;
            }
            view.setVisibility(i3);
        }
        return i2;
    }

    private void t() {
        if (this.B) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    private int k(int i2) {
        if (i2 == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i2 != 9) {
            return i2;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    /* access modifiers changed from: package-private */
    public void o() {
        if (this.x != null) {
            this.x.k();
        }
        if (this.i != null) {
            this.b.getDecorView().removeCallbacks(this.j);
            if (this.i.isShowing()) {
                try {
                    this.i.dismiss();
                } catch (IllegalArgumentException e2) {
                }
            }
            this.i = null;
        }
        m();
        PanelFeatureState a2 = a(0, false);
        if (a2 != null && a2.j != null) {
            a2.j.close();
        }
    }

    @Override // android.support.v7.app.d
    public boolean d() {
        boolean z2 = false;
        int u2 = u();
        int i2 = i(u2);
        if (i2 != -1) {
            z2 = l(i2);
        }
        if (u2 == 0) {
            v();
            this.N.c();
        }
        this.M = true;
        return z2;
    }

    /* access modifiers changed from: package-private */
    public int i(int i2) {
        switch (i2) {
            case -100:
                return -1;
            case 0:
                if (Build.VERSION.SDK_INT >= 23 && ((UiModeManager) this.f441a.getSystemService(UiModeManager.class)).getNightMode() == 0) {
                    return -1;
                }
                v();
                return this.N.a();
            default:
                return i2;
        }
    }

    private int u() {
        return this.L != -100 ? this.L : e();
    }

    private boolean l(int i2) {
        Resources resources = this.f441a.getResources();
        Configuration configuration = resources.getConfiguration();
        int i3 = configuration.uiMode & 48;
        int i4 = i2 == 2 ? 32 : 16;
        if (i3 == i4) {
            return false;
        }
        if (w()) {
            ((Activity) this.f441a).recreate();
        } else {
            Configuration configuration2 = new Configuration(configuration);
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            configuration2.uiMode = i4 | (configuration2.uiMode & -49);
            resources.updateConfiguration(configuration2, displayMetrics);
            if (Build.VERSION.SDK_INT < 26) {
                f.a(resources);
            }
        }
        return true;
    }

    private void v() {
        if (this.N == null) {
            this.N = new d(i.a(this.f441a));
        }
    }

    private boolean w() {
        if (!this.M || !(this.f441a instanceof Activity)) {
            return false;
        }
        try {
            return (this.f441a.getPackageManager().getActivityInfo(new ComponentName(this.f441a, this.f441a.getClass()), 0).configChanges & 512) == 0;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e2);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public class b implements b.a {
        private b.a b;

        public b(b.a aVar) {
            this.b = aVar;
        }

        @Override // android.support.v7.view.b.a
        public boolean a(android.support.v7.view.b bVar, Menu menu) {
            return this.b.a(bVar, menu);
        }

        @Override // android.support.v7.view.b.a
        public boolean b(android.support.v7.view.b bVar, Menu menu) {
            return this.b.b(bVar, menu);
        }

        @Override // android.support.v7.view.b.a
        public boolean a(android.support.v7.view.b bVar, MenuItem menuItem) {
            return this.b.a(bVar, menuItem);
        }

        @Override // android.support.v7.view.b.a
        public void a(android.support.v7.view.b bVar) {
            this.b.a(bVar);
            if (AppCompatDelegateImpl.this.i != null) {
                AppCompatDelegateImpl.this.b.getDecorView().removeCallbacks(AppCompatDelegateImpl.this.j);
            }
            if (AppCompatDelegateImpl.this.h != null) {
                AppCompatDelegateImpl.this.m();
                AppCompatDelegateImpl.this.k = ViewCompat.animate(AppCompatDelegateImpl.this.h).alpha(0.0f);
                AppCompatDelegateImpl.this.k.setListener(new ViewPropertyAnimatorListenerAdapter() {
                    /* class android.support.v7.app.AppCompatDelegateImpl.b.AnonymousClass1 */

                    @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
                    public void onAnimationEnd(View view) {
                        AppCompatDelegateImpl.this.h.setVisibility(8);
                        if (AppCompatDelegateImpl.this.i != null) {
                            AppCompatDelegateImpl.this.i.dismiss();
                        } else if (AppCompatDelegateImpl.this.h.getParent() instanceof View) {
                            ViewCompat.requestApplyInsets((View) AppCompatDelegateImpl.this.h.getParent());
                        }
                        AppCompatDelegateImpl.this.h.removeAllViews();
                        AppCompatDelegateImpl.this.k.setListener(null);
                        AppCompatDelegateImpl.this.k = null;
                    }
                });
            }
            if (AppCompatDelegateImpl.this.e != null) {
                AppCompatDelegateImpl.this.e.b(AppCompatDelegateImpl.this.g);
            }
            AppCompatDelegateImpl.this.g = null;
        }
    }

    /* access modifiers changed from: private */
    public final class f implements o.a {
        f() {
        }

        @Override // android.support.v7.view.menu.o.a
        public void a(h hVar, boolean z) {
            h q = hVar.q();
            boolean z2 = q != hVar;
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (z2) {
                hVar = q;
            }
            PanelFeatureState a2 = appCompatDelegateImpl.a((Menu) hVar);
            if (a2 == null) {
                return;
            }
            if (z2) {
                AppCompatDelegateImpl.this.a(a2.f450a, a2, q);
                AppCompatDelegateImpl.this.a(a2, true);
                return;
            }
            AppCompatDelegateImpl.this.a(a2, z);
        }

        @Override // android.support.v7.view.menu.o.a
        public boolean a(h hVar) {
            Window.Callback h;
            if (hVar != null || !AppCompatDelegateImpl.this.l || (h = AppCompatDelegateImpl.this.h()) == null || AppCompatDelegateImpl.this.q) {
                return true;
            }
            h.onMenuOpened(108, hVar);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public final class a implements o.a {
        a() {
        }

        @Override // android.support.v7.view.menu.o.a
        public boolean a(h hVar) {
            Window.Callback h = AppCompatDelegateImpl.this.h();
            if (h == null) {
                return true;
            }
            h.onMenuOpened(108, hVar);
            return true;
        }

        @Override // android.support.v7.view.menu.o.a
        public void a(h hVar, boolean z) {
            AppCompatDelegateImpl.this.b(hVar);
        }
    }

    /* access modifiers changed from: protected */
    public static final class PanelFeatureState {

        /* renamed from: a  reason: collision with root package name */
        int f450a;
        int b;
        int c;
        int d;
        int e;
        int f;
        ViewGroup g;
        View h;
        View i;
        h j;
        android.support.v7.view.menu.f k;
        Context l;
        boolean m;
        boolean n;
        boolean o;
        public boolean p;
        boolean q = false;
        boolean r;
        Bundle s;

        PanelFeatureState(int i2) {
            this.f450a = i2;
        }

        public boolean a() {
            if (this.h == null) {
                return false;
            }
            return this.i != null || this.k.d().getCount() > 0;
        }

        /* access modifiers changed from: package-private */
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(R.attr.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(R.attr.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(R.style.Theme_AppCompat_CompactMenu, true);
            }
            android.support.v7.view.d dVar = new android.support.v7.view.d(context, 0);
            dVar.getTheme().setTo(newTheme);
            this.l = dVar;
            TypedArray obtainStyledAttributes = dVar.obtainStyledAttributes(R.styleable.AppCompatTheme);
            this.b = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTheme_panelBackground, 0);
            this.f = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        /* access modifiers changed from: package-private */
        public void a(h hVar) {
            if (hVar != this.j) {
                if (this.j != null) {
                    this.j.b(this.k);
                }
                this.j = hVar;
                if (hVar != null && this.k != null) {
                    hVar.a(this.k);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public p a(o.a aVar) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                this.k = new android.support.v7.view.menu.f(this.l, R.layout.abc_list_menu_item_layout);
                this.k.a(aVar);
                this.j.a(this.k);
            }
            return this.k.a(this.g);
        }

        /* access modifiers changed from: private */
        public static class SavedState implements Parcelable {
            public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
                /* class android.support.v7.app.AppCompatDelegateImpl.PanelFeatureState.SavedState.AnonymousClass1 */

                /* renamed from: a */
                public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return SavedState.a(parcel, classLoader);
                }

                /* renamed from: a */
                public SavedState createFromParcel(Parcel parcel) {
                    return SavedState.a(parcel, null);
                }

                /* renamed from: a */
                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };

            /* renamed from: a  reason: collision with root package name */
            int f451a;
            boolean b;
            Bundle c;

            SavedState() {
            }

            public int describeContents() {
                return 0;
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f451a);
                parcel.writeInt(this.b ? 1 : 0);
                if (this.b) {
                    parcel.writeBundle(this.c);
                }
            }

            static SavedState a(Parcel parcel, ClassLoader classLoader) {
                boolean z = true;
                SavedState savedState = new SavedState();
                savedState.f451a = parcel.readInt();
                if (parcel.readInt() != 1) {
                    z = false;
                }
                savedState.b = z;
                if (savedState.b) {
                    savedState.c = parcel.readBundle(classLoader);
                }
                return savedState;
            }
        }
    }

    /* access modifiers changed from: private */
    public class e extends ContentFrameLayout {
        public e(Context context) {
            super(context);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImpl.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            AppCompatDelegateImpl.this.f(0);
            return true;
        }

        public void setBackgroundResource(int i) {
            setBackgroundDrawable(android.support.v7.a.a.a.b(getContext(), i));
        }

        private boolean a(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }
    }

    class c extends i {
        c(Window.Callback callback) {
            super(callback);
        }

        @Override // android.support.v7.view.i
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImpl.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @Override // android.support.v7.view.i
        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || AppCompatDelegateImpl.this.a(keyEvent.getKeyCode(), keyEvent);
        }

        @Override // android.support.v7.view.i
        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof h)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        @Override // android.support.v7.view.i
        public void onContentChanged() {
        }

        @Override // android.support.v7.view.i
        public boolean onPreparePanel(int i, View view, Menu menu) {
            h hVar;
            if (menu instanceof h) {
                hVar = (h) menu;
            } else {
                hVar = null;
            }
            if (i == 0 && hVar == null) {
                return false;
            }
            if (hVar != null) {
                hVar.c(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (hVar == null) {
                return onPreparePanel;
            }
            hVar.c(false);
            return onPreparePanel;
        }

        @Override // android.support.v7.view.i
        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            AppCompatDelegateImpl.this.e(i);
            return true;
        }

        @Override // android.support.v7.view.i
        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            AppCompatDelegateImpl.this.d(i);
        }

        @Override // android.support.v7.view.i
        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            if (AppCompatDelegateImpl.this.l()) {
                return a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        /* access modifiers changed from: package-private */
        public final ActionMode a(ActionMode.Callback callback) {
            f.a aVar = new f.a(AppCompatDelegateImpl.this.f441a, callback);
            android.support.v7.view.b a2 = AppCompatDelegateImpl.this.a(aVar);
            if (a2 != null) {
                return aVar.b(a2);
            }
            return null;
        }

        @Override // android.support.v7.view.i
        @RequiresApi(23)
        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            if (AppCompatDelegateImpl.this.l()) {
                switch (i) {
                    case 0:
                        return a(callback);
                }
            }
            return super.onWindowStartingActionMode(callback, i);
        }

        @Override // android.support.v7.view.i, android.view.Window.Callback
        @RequiresApi(24)
        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
            PanelFeatureState a2 = AppCompatDelegateImpl.this.a(0, true);
            if (a2 == null || a2.j == null) {
                super.onProvideKeyboardShortcuts(list, menu, i);
            } else {
                super.onProvideKeyboardShortcuts(list, a2.j, i);
            }
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final class d {
        private i b;
        private boolean c;
        private BroadcastReceiver d;
        private IntentFilter e;

        d(i iVar) {
            this.b = iVar;
            this.c = iVar.a();
        }

        /* access modifiers changed from: package-private */
        public int a() {
            this.c = this.b.a();
            return this.c ? 2 : 1;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            boolean a2 = this.b.a();
            if (a2 != this.c) {
                this.c = a2;
                AppCompatDelegateImpl.this.d();
            }
        }

        /* access modifiers changed from: package-private */
        public void c() {
            d();
            if (this.d == null) {
                this.d = new BroadcastReceiver() {
                    /* class android.support.v7.app.AppCompatDelegateImpl.d.AnonymousClass1 */

                    public void onReceive(Context context, Intent intent) {
                        d.this.b();
                    }
                };
            }
            if (this.e == null) {
                this.e = new IntentFilter();
                this.e.addAction("android.intent.action.TIME_SET");
                this.e.addAction("android.intent.action.TIMEZONE_CHANGED");
                this.e.addAction("android.intent.action.TIME_TICK");
            }
            AppCompatDelegateImpl.this.f441a.registerReceiver(this.d, this.e);
        }

        /* access modifiers changed from: package-private */
        public void d() {
            if (this.d != null) {
                AppCompatDelegateImpl.this.f441a.unregisterReceiver(this.d);
                this.d = null;
            }
        }
    }
}
