package android.support.v7.app;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.KeyEventDispatcher;
import android.support.v7.appcompat.R;
import android.support.v7.view.b;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

public class e extends Dialog implements c {

    /* renamed from: a  reason: collision with root package name */
    private d f465a;
    private final KeyEventDispatcher.Component b = new KeyEventDispatcher.Component() {
        /* class android.support.v7.app.e.AnonymousClass1 */

        @Override // android.support.v4.view.KeyEventDispatcher.Component
        public boolean superDispatchKeyEvent(KeyEvent keyEvent) {
            return e.this.a(keyEvent);
        }
    };

    public e(Context context, int i) {
        super(context, a(context, i));
        a().a((Bundle) null);
        a().d();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        a().c();
        super.onCreate(bundle);
        a().a(bundle);
    }

    @Override // android.app.Dialog
    public void setContentView(@LayoutRes int i) {
        a().b(i);
    }

    @Override // android.app.Dialog
    public void setContentView(View view) {
        a().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().a(view, layoutParams);
    }

    @Override // android.app.Dialog
    @Nullable
    public <T extends View> T findViewById(@IdRes int i) {
        return (T) a().a(i);
    }

    @Override // android.app.Dialog
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        a().a(charSequence);
    }

    @Override // android.app.Dialog
    public void setTitle(int i) {
        super.setTitle(i);
        a().a(getContext().getString(i));
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().b(view, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        a().a();
    }

    public boolean a(int i) {
        return a().c(i);
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public void invalidateOptionsMenu() {
        a().b();
    }

    public d a() {
        if (this.f465a == null) {
            this.f465a = d.a(this, this);
        }
        return this.f465a;
    }

    private static int a(Context context, int i) {
        if (i != 0) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.dialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @Override // android.support.v7.app.c
    public void a(b bVar) {
    }

    @Override // android.support.v7.app.c
    public void b(b bVar) {
    }

    @Override // android.support.v7.app.c
    @Nullable
    public b a(b.a aVar) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return KeyEventDispatcher.dispatchKeyEvent(this.b, getWindow().getDecorView(), this, keyEvent);
    }
}
