package android.support.v7.a.a;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ColorStateListInflaterCompat;
import android.support.v7.widget.k;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import java.util.WeakHashMap;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadLocal<TypedValue> f426a = new ThreadLocal<>();
    private static final WeakHashMap<Context, SparseArray<C0019a>> b = new WeakHashMap<>(0);
    private static final Object c = new Object();

    public static ColorStateList a(@NonNull Context context, @ColorRes int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return context.getColorStateList(i);
        }
        ColorStateList d = d(context, i);
        if (d != null) {
            return d;
        }
        ColorStateList c2 = c(context, i);
        if (c2 == null) {
            return ContextCompat.getColorStateList(context, i);
        }
        a(context, i, c2);
        return c2;
    }

    @Nullable
    public static Drawable b(@NonNull Context context, @DrawableRes int i) {
        return k.a().a(context, i);
    }

    @Nullable
    private static ColorStateList c(Context context, int i) {
        if (e(context, i)) {
            return null;
        }
        Resources resources = context.getResources();
        try {
            return ColorStateListInflaterCompat.createFromXml(resources, resources.getXml(i), context.getTheme());
        } catch (Exception e) {
            Log.e("AppCompatResources", "Failed to inflate ColorStateList, leaving it to the framework", e);
            return null;
        }
    }

    @Nullable
    private static ColorStateList d(@NonNull Context context, @ColorRes int i) {
        C0019a aVar;
        synchronized (c) {
            SparseArray<C0019a> sparseArray = b.get(context);
            if (!(sparseArray == null || sparseArray.size() <= 0 || (aVar = sparseArray.get(i)) == null)) {
                if (aVar.b.equals(context.getResources().getConfiguration())) {
                    return aVar.f427a;
                }
                sparseArray.remove(i);
            }
            return null;
        }
    }

    private static void a(@NonNull Context context, @ColorRes int i, @NonNull ColorStateList colorStateList) {
        synchronized (c) {
            SparseArray<C0019a> sparseArray = b.get(context);
            if (sparseArray == null) {
                sparseArray = new SparseArray<>();
                b.put(context, sparseArray);
            }
            sparseArray.append(i, new C0019a(colorStateList, context.getResources().getConfiguration()));
        }
    }

    private static boolean e(@NonNull Context context, @ColorRes int i) {
        Resources resources = context.getResources();
        TypedValue a2 = a();
        resources.getValue(i, a2, true);
        if (a2.type < 28 || a2.type > 31) {
            return false;
        }
        return true;
    }

    @NonNull
    private static TypedValue a() {
        TypedValue typedValue = f426a.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        f426a.set(typedValue2);
        return typedValue2;
    }

    /* access modifiers changed from: private */
    /* renamed from: android.support.v7.a.a.a$a  reason: collision with other inner class name */
    public static class C0019a {

        /* renamed from: a  reason: collision with root package name */
        final ColorStateList f427a;
        final Configuration b;

        C0019a(@NonNull ColorStateList colorStateList, @NonNull Configuration configuration) {
            this.f427a = colorStateList;
            this.b = configuration;
        }
    }
}
