package android.support.v7.view;

import android.content.Context;
import android.support.annotation.RestrictTo;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.view.b;
import android.support.v7.view.menu.q;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class f extends ActionMode {

    /* renamed from: a  reason: collision with root package name */
    final Context f494a;
    final b b;

    public f(Context context, b bVar) {
        this.f494a = context;
        this.b = bVar;
    }

    public Object getTag() {
        return this.b.j();
    }

    public void setTag(Object obj) {
        this.b.a(obj);
    }

    @Override // android.view.ActionMode
    public void setTitle(CharSequence charSequence) {
        this.b.b(charSequence);
    }

    @Override // android.view.ActionMode
    public void setSubtitle(CharSequence charSequence) {
        this.b.a(charSequence);
    }

    public void invalidate() {
        this.b.d();
    }

    public void finish() {
        this.b.c();
    }

    public Menu getMenu() {
        return q.a(this.f494a, (SupportMenu) this.b.b());
    }

    public CharSequence getTitle() {
        return this.b.f();
    }

    @Override // android.view.ActionMode
    public void setTitle(int i) {
        this.b.a(i);
    }

    public CharSequence getSubtitle() {
        return this.b.g();
    }

    @Override // android.view.ActionMode
    public void setSubtitle(int i) {
        this.b.b(i);
    }

    public View getCustomView() {
        return this.b.i();
    }

    public void setCustomView(View view) {
        this.b.a(view);
    }

    public MenuInflater getMenuInflater() {
        return this.b.a();
    }

    public boolean getTitleOptionalHint() {
        return this.b.k();
    }

    public void setTitleOptionalHint(boolean z) {
        this.b.a(z);
    }

    public boolean isTitleOptional() {
        return this.b.h();
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public static class a implements b.a {

        /* renamed from: a  reason: collision with root package name */
        final ActionMode.Callback f495a;
        final Context b;
        final ArrayList<f> c = new ArrayList<>();
        final SimpleArrayMap<Menu, Menu> d = new SimpleArrayMap<>();

        public a(Context context, ActionMode.Callback callback) {
            this.b = context;
            this.f495a = callback;
        }

        @Override // android.support.v7.view.b.a
        public boolean a(b bVar, Menu menu) {
            return this.f495a.onCreateActionMode(b(bVar), a(menu));
        }

        @Override // android.support.v7.view.b.a
        public boolean b(b bVar, Menu menu) {
            return this.f495a.onPrepareActionMode(b(bVar), a(menu));
        }

        @Override // android.support.v7.view.b.a
        public boolean a(b bVar, MenuItem menuItem) {
            return this.f495a.onActionItemClicked(b(bVar), q.a(this.b, (SupportMenuItem) menuItem));
        }

        @Override // android.support.v7.view.b.a
        public void a(b bVar) {
            this.f495a.onDestroyActionMode(b(bVar));
        }

        private Menu a(Menu menu) {
            Menu menu2 = this.d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            Menu a2 = q.a(this.b, (SupportMenu) menu);
            this.d.put(menu, a2);
            return a2;
        }

        public ActionMode b(b bVar) {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                f fVar = this.c.get(i);
                if (fVar != null && fVar.b == bVar) {
                    return fVar;
                }
            }
            f fVar2 = new f(this.b, bVar);
            this.c.add(fVar2);
            return fVar2;
        }
    }
}
