package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.o;
import android.support.v7.widget.at;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

/* access modifiers changed from: package-private */
public final class t extends m implements o, View.OnKeyListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {
    private static final int e = R.layout.abc_popup_menu_item_layout;

    /* renamed from: a  reason: collision with root package name */
    final at f528a;
    final ViewTreeObserver.OnGlobalLayoutListener b = new ViewTreeObserver.OnGlobalLayoutListener() {
        /* class android.support.v7.view.menu.t.AnonymousClass1 */

        public void onGlobalLayout() {
            if (t.this.f() && !t.this.f528a.c()) {
                View view = t.this.c;
                if (view == null || !view.isShown()) {
                    t.this.e();
                } else {
                    t.this.f528a.d();
                }
            }
        }
    };
    View c;
    ViewTreeObserver d;
    private final Context f;
    private final h g;
    private final g h;
    private final boolean i;
    private final int j;
    private final int k;
    private final int l;
    private final View.OnAttachStateChangeListener m = new View.OnAttachStateChangeListener() {
        /* class android.support.v7.view.menu.t.AnonymousClass2 */

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            if (t.this.d != null) {
                if (!t.this.d.isAlive()) {
                    t.this.d = view.getViewTreeObserver();
                }
                t.this.d.removeGlobalOnLayoutListener(t.this.b);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };
    private PopupWindow.OnDismissListener n;
    private View o;
    private o.a p;
    private boolean q;
    private boolean r;
    private int s;
    private int t = 0;
    private boolean u;

    public t(Context context, h hVar, View view, int i2, int i3, boolean z) {
        this.f = context;
        this.g = hVar;
        this.i = z;
        this.h = new g(hVar, LayoutInflater.from(context), this.i, e);
        this.k = i2;
        this.l = i3;
        Resources resources = context.getResources();
        this.j = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(R.dimen.abc_config_prefDialogWidth));
        this.o = view;
        this.f528a = new at(this.f, null, this.k, this.l);
        hVar.a(this, context);
    }

    @Override // android.support.v7.view.menu.m
    public void b(boolean z) {
        this.h.a(z);
    }

    @Override // android.support.v7.view.menu.m
    public void a(int i2) {
        this.t = i2;
    }

    private boolean j() {
        if (f()) {
            return true;
        }
        if (this.q || this.o == null) {
            return false;
        }
        this.c = this.o;
        this.f528a.a((PopupWindow.OnDismissListener) this);
        this.f528a.a((AdapterView.OnItemClickListener) this);
        this.f528a.a(true);
        View view = this.c;
        boolean z = this.d == null;
        this.d = view.getViewTreeObserver();
        if (z) {
            this.d.addOnGlobalLayoutListener(this.b);
        }
        view.addOnAttachStateChangeListener(this.m);
        this.f528a.b(view);
        this.f528a.e(this.t);
        if (!this.r) {
            this.s = a(this.h, null, this.f, this.j);
            this.r = true;
        }
        this.f528a.g(this.s);
        this.f528a.h(2);
        this.f528a.a(i());
        this.f528a.d();
        ListView g2 = this.f528a.g();
        g2.setOnKeyListener(this);
        if (this.u && this.g.n() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.f).inflate(R.layout.abc_popup_menu_header_item_layout, (ViewGroup) g2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.g.n());
            }
            frameLayout.setEnabled(false);
            g2.addHeaderView(frameLayout, null, false);
        }
        this.f528a.a((ListAdapter) this.h);
        this.f528a.d();
        return true;
    }

    @Override // android.support.v7.view.menu.s
    public void d() {
        if (!j()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    @Override // android.support.v7.view.menu.s
    public void e() {
        if (f()) {
            this.f528a.e();
        }
    }

    @Override // android.support.v7.view.menu.m
    public void a(h hVar) {
    }

    @Override // android.support.v7.view.menu.s
    public boolean f() {
        return !this.q && this.f528a.f();
    }

    public void onDismiss() {
        this.q = true;
        this.g.close();
        if (this.d != null) {
            if (!this.d.isAlive()) {
                this.d = this.c.getViewTreeObserver();
            }
            this.d.removeGlobalOnLayoutListener(this.b);
            this.d = null;
        }
        this.c.removeOnAttachStateChangeListener(this.m);
        if (this.n != null) {
            this.n.onDismiss();
        }
    }

    @Override // android.support.v7.view.menu.o
    public void a(boolean z) {
        this.r = false;
        if (this.h != null) {
            this.h.notifyDataSetChanged();
        }
    }

    @Override // android.support.v7.view.menu.o
    public void a(o.a aVar) {
        this.p = aVar;
    }

    @Override // android.support.v7.view.menu.o
    public boolean a(u uVar) {
        if (uVar.hasVisibleItems()) {
            n nVar = new n(this.f, uVar, this.c, this.i, this.k, this.l);
            nVar.a(this.p);
            nVar.a(m.b(uVar));
            nVar.a(this.n);
            this.n = null;
            this.g.b(false);
            int j2 = this.f528a.j();
            int k2 = this.f528a.k();
            if ((Gravity.getAbsoluteGravity(this.t, ViewCompat.getLayoutDirection(this.o)) & 7) == 5) {
                j2 += this.o.getWidth();
            }
            if (nVar.a(j2, k2)) {
                if (this.p != null) {
                    this.p.a(uVar);
                }
                return true;
            }
        }
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public void a(h hVar, boolean z) {
        if (hVar == this.g) {
            e();
            if (this.p != null) {
                this.p.a(hVar, z);
            }
        }
    }

    @Override // android.support.v7.view.menu.o
    public boolean a() {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public Parcelable c() {
        return null;
    }

    @Override // android.support.v7.view.menu.o
    public void a(Parcelable parcelable) {
    }

    @Override // android.support.v7.view.menu.m
    public void a(View view) {
        this.o = view;
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        e();
        return true;
    }

    @Override // android.support.v7.view.menu.m
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.n = onDismissListener;
    }

    @Override // android.support.v7.view.menu.s
    public ListView g() {
        return this.f528a.g();
    }

    @Override // android.support.v7.view.menu.m
    public void b(int i2) {
        this.f528a.c(i2);
    }

    @Override // android.support.v7.view.menu.m
    public void c(int i2) {
        this.f528a.d(i2);
    }

    @Override // android.support.v7.view.menu.m
    public void c(boolean z) {
        this.u = z;
    }
}
