package android.support.v7.view.menu;

/* access modifiers changed from: package-private */
public class d<T> {
    final T b;

    d(T t) {
        if (t == null) {
            throw new IllegalArgumentException("Wrapped Object can not be null.");
        }
        this.b = t;
    }
}
