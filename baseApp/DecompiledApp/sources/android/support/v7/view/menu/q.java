package android.support.v7.view.menu;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RestrictTo;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.internal.view.SupportSubMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public final class q {
    public static Menu a(Context context, SupportMenu supportMenu) {
        return new r(context, supportMenu);
    }

    public static MenuItem a(Context context, SupportMenuItem supportMenuItem) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new l(context, supportMenuItem);
        }
        return new k(context, supportMenuItem);
    }

    public static SubMenu a(Context context, SupportSubMenu supportSubMenu) {
        return new v(context, supportSubMenu);
    }
}
