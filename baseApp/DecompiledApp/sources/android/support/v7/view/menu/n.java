package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.annotation.StyleRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.o;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class n {

    /* renamed from: a  reason: collision with root package name */
    private final Context f526a;
    private final h b;
    private final boolean c;
    private final int d;
    private final int e;
    private View f;
    private int g;
    private boolean h;
    private o.a i;
    private m j;
    private PopupWindow.OnDismissListener k;
    private final PopupWindow.OnDismissListener l;

    public n(@NonNull Context context, @NonNull h hVar, @NonNull View view, boolean z, @AttrRes int i2) {
        this(context, hVar, view, z, i2, 0);
    }

    public n(@NonNull Context context, @NonNull h hVar, @NonNull View view, boolean z, @AttrRes int i2, @StyleRes int i3) {
        this.g = GravityCompat.START;
        this.l = new PopupWindow.OnDismissListener() {
            /* class android.support.v7.view.menu.n.AnonymousClass1 */

            public void onDismiss() {
                n.this.e();
            }
        };
        this.f526a = context;
        this.b = hVar;
        this.f = view;
        this.c = z;
        this.d = i2;
        this.e = i3;
    }

    public void a(@Nullable PopupWindow.OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    public void a(@NonNull View view) {
        this.f = view;
    }

    public void a(boolean z) {
        this.h = z;
        if (this.j != null) {
            this.j.b(z);
        }
    }

    public void a(int i2) {
        this.g = i2;
    }

    public void a() {
        if (!c()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    @NonNull
    public m b() {
        if (this.j == null) {
            this.j = g();
        }
        return this.j;
    }

    public boolean c() {
        if (f()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(0, 0, false, false);
        return true;
    }

    public boolean a(int i2, int i3) {
        if (f()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(i2, i3, true, true);
        return true;
    }

    @NonNull
    private m g() {
        m tVar;
        Display defaultDisplay = ((WindowManager) this.f526a.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else {
            defaultDisplay.getSize(point);
        }
        if (Math.min(point.x, point.y) >= this.f526a.getResources().getDimensionPixelSize(R.dimen.abc_cascading_menus_min_smallest_width)) {
            tVar = new e(this.f526a, this.f, this.d, this.e, this.c);
        } else {
            tVar = new t(this.f526a, this.b, this.f, this.d, this.e, this.c);
        }
        tVar.a(this.b);
        tVar.a(this.l);
        tVar.a(this.f);
        tVar.a(this.i);
        tVar.b(this.h);
        tVar.a(this.g);
        return tVar;
    }

    private void a(int i2, int i3, boolean z, boolean z2) {
        m b2 = b();
        b2.c(z2);
        if (z) {
            if ((GravityCompat.getAbsoluteGravity(this.g, ViewCompat.getLayoutDirection(this.f)) & 7) == 5) {
                i2 -= this.f.getWidth();
            }
            b2.b(i2);
            b2.c(i3);
            int i4 = (int) ((this.f526a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            b2.a(new Rect(i2 - i4, i3 - i4, i2 + i4, i4 + i3));
        }
        b2.d();
    }

    public void d() {
        if (f()) {
            this.j.e();
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.j = null;
        if (this.k != null) {
            this.k.onDismiss();
        }
    }

    public boolean f() {
        return this.j != null && this.j.f();
    }

    public void a(@Nullable o.a aVar) {
        this.i = aVar;
        if (this.j != null) {
            this.j.a(aVar);
        }
    }
}
