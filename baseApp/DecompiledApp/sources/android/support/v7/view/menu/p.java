package android.support.v7.view.menu;

import android.support.annotation.RestrictTo;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public interface p {

    public interface a {
        void a(j jVar, int i);

        boolean a();

        j getItemData();
    }

    void a(h hVar);
}
