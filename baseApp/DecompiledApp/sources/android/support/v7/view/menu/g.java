package android.support.v7.view.menu;

import android.support.annotation.RestrictTo;
import android.support.v7.view.menu.p;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class g extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    h f516a;
    private int b = -1;
    private boolean c;
    private final boolean d;
    private final LayoutInflater e;
    private final int f;

    public g(h hVar, LayoutInflater layoutInflater, boolean z, int i) {
        this.d = z;
        this.e = layoutInflater;
        this.f516a = hVar;
        this.f = i;
        b();
    }

    public void a(boolean z) {
        this.c = z;
    }

    public int getCount() {
        ArrayList<j> m = this.d ? this.f516a.m() : this.f516a.j();
        if (this.b < 0) {
            return m.size();
        }
        return m.size() - 1;
    }

    public h a() {
        return this.f516a;
    }

    /* renamed from: a */
    public j getItem(int i) {
        ArrayList<j> m = this.d ? this.f516a.m() : this.f516a.j();
        if (this.b >= 0 && i >= this.b) {
            i++;
        }
        return m.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        boolean z;
        if (view == null) {
            view2 = this.e.inflate(this.f, viewGroup, false);
        } else {
            view2 = view;
        }
        int groupId = getItem(i).getGroupId();
        int groupId2 = i + -1 >= 0 ? getItem(i - 1).getGroupId() : groupId;
        ListMenuItemView listMenuItemView = (ListMenuItemView) view2;
        if (!this.f516a.b() || groupId == groupId2) {
            z = false;
        } else {
            z = true;
        }
        listMenuItemView.setGroupDividerEnabled(z);
        p.a aVar = (p.a) view2;
        if (this.c) {
            ((ListMenuItemView) view2).setForceShowIcon(true);
        }
        aVar.a(getItem(i), 0);
        return view2;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        j s = this.f516a.s();
        if (s != null) {
            ArrayList<j> m = this.f516a.m();
            int size = m.size();
            for (int i = 0; i < size; i++) {
                if (m.get(i) == s) {
                    this.b = i;
                    return;
                }
            }
        }
        this.b = -1;
    }

    public void notifyDataSetChanged() {
        b();
        super.notifyDataSetChanged();
    }
}
