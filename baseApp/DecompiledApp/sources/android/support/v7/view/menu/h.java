package android.support.v7.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.v4.content.ContextCompat;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.ViewConfigurationCompat;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewConfiguration;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class h implements SupportMenu {
    private static final int[] d = {1, 4, 5, 3, 2, 0};
    private boolean A;

    /* renamed from: a  reason: collision with root package name */
    CharSequence f517a;
    Drawable b;
    View c;
    private final Context e;
    private final Resources f;
    private boolean g;
    private boolean h;
    private a i;
    private ArrayList<j> j;
    private ArrayList<j> k;
    private boolean l;
    private ArrayList<j> m;
    private ArrayList<j> n;
    private boolean o;
    private int p = 0;
    private ContextMenu.ContextMenuInfo q;
    private boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    private boolean v = false;
    private ArrayList<j> w = new ArrayList<>();
    private CopyOnWriteArrayList<WeakReference<o>> x = new CopyOnWriteArrayList<>();
    private j y;
    private boolean z = false;

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public interface a {
        void a(h hVar);

        boolean a(h hVar, MenuItem menuItem);
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public interface b {
        boolean a(j jVar);
    }

    public h(Context context) {
        this.e = context;
        this.f = context.getResources();
        this.j = new ArrayList<>();
        this.k = new ArrayList<>();
        this.l = true;
        this.m = new ArrayList<>();
        this.n = new ArrayList<>();
        this.o = true;
        e(true);
    }

    public h a(int i2) {
        this.p = i2;
        return this;
    }

    public void a(o oVar) {
        a(oVar, this.e);
    }

    public void a(o oVar, Context context) {
        this.x.add(new WeakReference<>(oVar));
        oVar.a(context, this);
        this.o = true;
    }

    public void b(o oVar) {
        Iterator<WeakReference<o>> it = this.x.iterator();
        while (it.hasNext()) {
            WeakReference<o> next = it.next();
            o oVar2 = next.get();
            if (oVar2 == null || oVar2 == oVar) {
                this.x.remove(next);
            }
        }
    }

    private void d(boolean z2) {
        if (!this.x.isEmpty()) {
            h();
            Iterator<WeakReference<o>> it = this.x.iterator();
            while (it.hasNext()) {
                WeakReference<o> next = it.next();
                o oVar = next.get();
                if (oVar == null) {
                    this.x.remove(next);
                } else {
                    oVar.a(z2);
                }
            }
            i();
        }
    }

    private boolean a(u uVar, o oVar) {
        boolean z2 = false;
        if (this.x.isEmpty()) {
            return false;
        }
        if (oVar != null) {
            z2 = oVar.a(uVar);
        }
        Iterator<WeakReference<o>> it = this.x.iterator();
        while (true) {
            boolean z3 = z2;
            if (!it.hasNext()) {
                return z3;
            }
            WeakReference<o> next = it.next();
            o oVar2 = next.get();
            if (oVar2 == null) {
                this.x.remove(next);
            } else if (!z3) {
                z3 = oVar2.a(uVar);
            }
            z2 = z3;
        }
    }

    private void e(Bundle bundle) {
        Parcelable c2;
        if (!this.x.isEmpty()) {
            SparseArray<? extends Parcelable> sparseArray = new SparseArray<>();
            Iterator<WeakReference<o>> it = this.x.iterator();
            while (it.hasNext()) {
                WeakReference<o> next = it.next();
                o oVar = next.get();
                if (oVar == null) {
                    this.x.remove(next);
                } else {
                    int b2 = oVar.b();
                    if (b2 > 0 && (c2 = oVar.c()) != null) {
                        sparseArray.put(b2, c2);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:presenters", sparseArray);
        }
    }

    private void f(Bundle bundle) {
        Parcelable parcelable;
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:presenters");
        if (sparseParcelableArray != null && !this.x.isEmpty()) {
            Iterator<WeakReference<o>> it = this.x.iterator();
            while (it.hasNext()) {
                WeakReference<o> next = it.next();
                o oVar = next.get();
                if (oVar == null) {
                    this.x.remove(next);
                } else {
                    int b2 = oVar.b();
                    if (b2 > 0 && (parcelable = (Parcelable) sparseParcelableArray.get(b2)) != null) {
                        oVar.a(parcelable);
                    }
                }
            }
        }
    }

    public void a(Bundle bundle) {
        e(bundle);
    }

    public void b(Bundle bundle) {
        f(bundle);
    }

    public void c(Bundle bundle) {
        SparseArray<? extends Parcelable> sparseArray = null;
        int size = size();
        int i2 = 0;
        while (i2 < size) {
            MenuItem item = getItem(i2);
            View actionView = item.getActionView();
            if (!(actionView == null || actionView.getId() == -1)) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray<>();
                }
                actionView.saveHierarchyState(sparseArray);
                if (item.isActionViewExpanded()) {
                    bundle.putInt("android:menu:expandedactionview", item.getItemId());
                }
            }
            if (item.hasSubMenu()) {
                ((u) item.getSubMenu()).c(bundle);
            }
            i2++;
            sparseArray = sparseArray;
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(a(), sparseArray);
        }
    }

    public void d(Bundle bundle) {
        MenuItem findItem;
        if (bundle != null) {
            SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray(a());
            int size = size();
            for (int i2 = 0; i2 < size; i2++) {
                MenuItem item = getItem(i2);
                View actionView = item.getActionView();
                if (!(actionView == null || actionView.getId() == -1)) {
                    actionView.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((u) item.getSubMenu()).d(bundle);
                }
            }
            int i3 = bundle.getInt("android:menu:expandedactionview");
            if (i3 > 0 && (findItem = findItem(i3)) != null) {
                findItem.expandActionView();
            }
        }
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "android:menu:actionviewstates";
    }

    public void a(a aVar) {
        this.i = aVar;
    }

    /* access modifiers changed from: protected */
    public MenuItem a(int i2, int i3, int i4, CharSequence charSequence) {
        int f2 = f(i4);
        j a2 = a(i2, i3, i4, f2, charSequence, this.p);
        if (this.q != null) {
            a2.a(this.q);
        }
        this.j.add(a(this.j, f2), a2);
        a(true);
        return a2;
    }

    private j a(int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        return new j(this, i2, i3, i4, i5, charSequence, i6);
    }

    @Override // android.view.Menu
    public MenuItem add(CharSequence charSequence) {
        return a(0, 0, 0, charSequence);
    }

    @Override // android.view.Menu
    public MenuItem add(int i2) {
        return a(0, 0, 0, this.f.getString(i2));
    }

    @Override // android.view.Menu
    public MenuItem add(int i2, int i3, int i4, CharSequence charSequence) {
        return a(i2, i3, i4, charSequence);
    }

    @Override // android.view.Menu
    public MenuItem add(int i2, int i3, int i4, int i5) {
        return a(i2, i3, i4, this.f.getString(i5));
    }

    @Override // android.view.Menu
    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    @Override // android.view.Menu
    public SubMenu addSubMenu(int i2) {
        return addSubMenu(0, 0, 0, this.f.getString(i2));
    }

    @Override // android.view.Menu
    public SubMenu addSubMenu(int i2, int i3, int i4, CharSequence charSequence) {
        j jVar = (j) a(i2, i3, i4, charSequence);
        u uVar = new u(this.e, this, jVar);
        jVar.a(uVar);
        return uVar;
    }

    @Override // android.view.Menu
    public SubMenu addSubMenu(int i2, int i3, int i4, int i5) {
        return addSubMenu(i2, i3, i4, this.f.getString(i5));
    }

    @Override // android.support.v4.internal.view.SupportMenu
    public void setGroupDividerEnabled(boolean z2) {
        this.z = z2;
    }

    public boolean b() {
        return this.z;
    }

    public int addIntentOptions(int i2, int i3, int i4, ComponentName componentName, Intent[] intentArr, Intent intent, int i5, MenuItem[] menuItemArr) {
        Intent intent2;
        PackageManager packageManager = this.e.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i5 & 1) == 0) {
            removeGroup(i2);
        }
        for (int i6 = 0; i6 < size; i6++) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i6);
            if (resolveInfo.specificIndex < 0) {
                intent2 = intent;
            } else {
                intent2 = intentArr[resolveInfo.specificIndex];
            }
            Intent intent3 = new Intent(intent2);
            intent3.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent4 = add(i2, i3, i4, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent3);
            if (menuItemArr != null && resolveInfo.specificIndex >= 0) {
                menuItemArr[resolveInfo.specificIndex] = intent4;
            }
        }
        return size;
    }

    public void removeItem(int i2) {
        a(b(i2), true);
    }

    public void removeGroup(int i2) {
        int c2 = c(i2);
        if (c2 >= 0) {
            int size = this.j.size() - c2;
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                if (i3 >= size || this.j.get(c2).getGroupId() != i2) {
                    a(true);
                } else {
                    a(c2, false);
                    i3 = i4;
                }
            }
            a(true);
        }
    }

    private void a(int i2, boolean z2) {
        if (i2 >= 0 && i2 < this.j.size()) {
            this.j.remove(i2);
            if (z2) {
                a(true);
            }
        }
    }

    public void clear() {
        if (this.y != null) {
            d(this.y);
        }
        this.j.clear();
        a(true);
    }

    /* access modifiers changed from: package-private */
    public void a(MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.j.size();
        h();
        for (int i2 = 0; i2 < size; i2++) {
            j jVar = this.j.get(i2);
            if (jVar.getGroupId() == groupId && jVar.f() && jVar.isCheckable()) {
                jVar.b(jVar == menuItem);
            }
        }
        i();
    }

    public void setGroupCheckable(int i2, boolean z2, boolean z3) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            j jVar = this.j.get(i3);
            if (jVar.getGroupId() == i2) {
                jVar.a(z3);
                jVar.setCheckable(z2);
            }
        }
    }

    public void setGroupVisible(int i2, boolean z2) {
        boolean z3;
        int size = this.j.size();
        int i3 = 0;
        boolean z4 = false;
        while (i3 < size) {
            j jVar = this.j.get(i3);
            if (jVar.getGroupId() != i2 || !jVar.c(z2)) {
                z3 = z4;
            } else {
                z3 = true;
            }
            i3++;
            z4 = z3;
        }
        if (z4) {
            a(true);
        }
    }

    public void setGroupEnabled(int i2, boolean z2) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            j jVar = this.j.get(i3);
            if (jVar.getGroupId() == i2) {
                jVar.setEnabled(z2);
            }
        }
    }

    public boolean hasVisibleItems() {
        if (this.A) {
            return true;
        }
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (this.j.get(i2).isVisible()) {
                return true;
            }
        }
        return false;
    }

    public MenuItem findItem(int i2) {
        MenuItem findItem;
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            j jVar = this.j.get(i3);
            if (jVar.getItemId() == i2) {
                return jVar;
            }
            if (jVar.hasSubMenu() && (findItem = jVar.getSubMenu().findItem(i2)) != null) {
                return findItem;
            }
        }
        return null;
    }

    public int b(int i2) {
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            if (this.j.get(i3).getItemId() == i2) {
                return i3;
            }
        }
        return -1;
    }

    public int c(int i2) {
        return a(i2, 0);
    }

    public int a(int i2, int i3) {
        int size = size();
        if (i3 < 0) {
            i3 = 0;
        }
        for (int i4 = i3; i4 < size; i4++) {
            if (this.j.get(i4).getGroupId() == i2) {
                return i4;
            }
        }
        return -1;
    }

    public int size() {
        return this.j.size();
    }

    public MenuItem getItem(int i2) {
        return this.j.get(i2);
    }

    public boolean isShortcutKey(int i2, KeyEvent keyEvent) {
        return a(i2, keyEvent) != null;
    }

    public void setQwertyMode(boolean z2) {
        this.g = z2;
        a(false);
    }

    private static int f(int i2) {
        int i3 = (-65536 & i2) >> 16;
        if (i3 >= 0 && i3 < d.length) {
            return (d[i3] << 16) | (65535 & i2);
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.g;
    }

    private void e(boolean z2) {
        boolean z3 = true;
        if (!z2 || this.f.getConfiguration().keyboard == 1 || !ViewConfigurationCompat.shouldShowMenuShortcutsWhenKeyboardPresent(ViewConfiguration.get(this.e), this.e)) {
            z3 = false;
        }
        this.h = z3;
    }

    public boolean d() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public Resources e() {
        return this.f;
    }

    public Context f() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public boolean a(h hVar, MenuItem menuItem) {
        return this.i != null && this.i.a(hVar, menuItem);
    }

    public void g() {
        if (this.i != null) {
            this.i.a(this);
        }
    }

    private static int a(ArrayList<j> arrayList, int i2) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (arrayList.get(size).b() <= i2) {
                return size + 1;
            }
        }
        return 0;
    }

    public boolean performShortcut(int i2, KeyEvent keyEvent, int i3) {
        j a2 = a(i2, keyEvent);
        boolean z2 = false;
        if (a2 != null) {
            z2 = a(a2, i3);
        }
        if ((i3 & 2) != 0) {
            b(true);
        }
        return z2;
    }

    /* access modifiers changed from: package-private */
    public void a(List<j> list, int i2, KeyEvent keyEvent) {
        boolean c2 = c();
        int modifiers = keyEvent.getModifiers();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i2 == 67) {
            int size = this.j.size();
            for (int i3 = 0; i3 < size; i3++) {
                j jVar = this.j.get(i3);
                if (jVar.hasSubMenu()) {
                    ((h) jVar.getSubMenu()).a(list, i2, keyEvent);
                }
                char alphabeticShortcut = c2 ? jVar.getAlphabeticShortcut() : jVar.getNumericShortcut();
                if (((modifiers & SupportMenu.SUPPORTED_MODIFIERS_MASK) == ((c2 ? jVar.getAlphabeticModifiers() : jVar.getNumericModifiers()) & SupportMenu.SUPPORTED_MODIFIERS_MASK)) && alphabeticShortcut != 0 && ((alphabeticShortcut == keyData.meta[0] || alphabeticShortcut == keyData.meta[2] || (c2 && alphabeticShortcut == '\b' && i2 == 67)) && jVar.isEnabled())) {
                    list.add(jVar);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public j a(int i2, KeyEvent keyEvent) {
        char numericShortcut;
        ArrayList<j> arrayList = this.w;
        arrayList.clear();
        a(arrayList, i2, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return arrayList.get(0);
        }
        boolean c2 = c();
        for (int i3 = 0; i3 < size; i3++) {
            j jVar = arrayList.get(i3);
            if (c2) {
                numericShortcut = jVar.getAlphabeticShortcut();
            } else {
                numericShortcut = jVar.getNumericShortcut();
            }
            if (numericShortcut == keyData.meta[0] && (metaState & 2) == 0) {
                return jVar;
            }
            if (numericShortcut == keyData.meta[2] && (metaState & 2) != 0) {
                return jVar;
            }
            if (c2 && numericShortcut == '\b' && i2 == 67) {
                return jVar;
            }
        }
        return null;
    }

    public boolean performIdentifierAction(int i2, int i3) {
        return a(findItem(i2), i3);
    }

    public boolean a(MenuItem menuItem, int i2) {
        return a(menuItem, (o) null, i2);
    }

    public boolean a(MenuItem menuItem, o oVar, int i2) {
        boolean z2;
        j jVar = (j) menuItem;
        if (jVar == null || !jVar.isEnabled()) {
            return false;
        }
        boolean a2 = jVar.a();
        ActionProvider supportActionProvider = jVar.getSupportActionProvider();
        if (supportActionProvider == null || !supportActionProvider.hasSubMenu()) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (jVar.m()) {
            boolean expandActionView = jVar.expandActionView() | a2;
            if (!expandActionView) {
                return expandActionView;
            }
            b(true);
            return expandActionView;
        } else if (jVar.hasSubMenu() || z2) {
            if ((i2 & 4) == 0) {
                b(false);
            }
            if (!jVar.hasSubMenu()) {
                jVar.a(new u(f(), this, jVar));
            }
            u uVar = (u) jVar.getSubMenu();
            if (z2) {
                supportActionProvider.onPrepareSubMenu(uVar);
            }
            boolean a3 = a(uVar, oVar) | a2;
            if (a3) {
                return a3;
            }
            b(true);
            return a3;
        } else {
            if ((i2 & 1) == 0) {
                b(true);
            }
            return a2;
        }
    }

    public final void b(boolean z2) {
        if (!this.v) {
            this.v = true;
            Iterator<WeakReference<o>> it = this.x.iterator();
            while (it.hasNext()) {
                WeakReference<o> next = it.next();
                o oVar = next.get();
                if (oVar == null) {
                    this.x.remove(next);
                } else {
                    oVar.a(this, z2);
                }
            }
            this.v = false;
        }
    }

    public void close() {
        b(true);
    }

    public void a(boolean z2) {
        if (!this.r) {
            if (z2) {
                this.l = true;
                this.o = true;
            }
            d(z2);
            return;
        }
        this.s = true;
        if (z2) {
            this.t = true;
        }
    }

    public void h() {
        if (!this.r) {
            this.r = true;
            this.s = false;
            this.t = false;
        }
    }

    public void i() {
        this.r = false;
        if (this.s) {
            this.s = false;
            a(this.t);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar) {
        this.l = true;
        a(true);
    }

    /* access modifiers changed from: package-private */
    public void b(j jVar) {
        this.o = true;
        a(true);
    }

    @NonNull
    public ArrayList<j> j() {
        if (!this.l) {
            return this.k;
        }
        this.k.clear();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            j jVar = this.j.get(i2);
            if (jVar.isVisible()) {
                this.k.add(jVar);
            }
        }
        this.l = false;
        this.o = true;
        return this.k;
    }

    public void k() {
        boolean a2;
        ArrayList<j> j2 = j();
        if (this.o) {
            Iterator<WeakReference<o>> it = this.x.iterator();
            boolean z2 = false;
            while (it.hasNext()) {
                WeakReference<o> next = it.next();
                o oVar = next.get();
                if (oVar == null) {
                    this.x.remove(next);
                    a2 = z2;
                } else {
                    a2 = oVar.a() | z2;
                }
                z2 = a2;
            }
            if (z2) {
                this.m.clear();
                this.n.clear();
                int size = j2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    j jVar = j2.get(i2);
                    if (jVar.i()) {
                        this.m.add(jVar);
                    } else {
                        this.n.add(jVar);
                    }
                }
            } else {
                this.m.clear();
                this.n.clear();
                this.n.addAll(j());
            }
            this.o = false;
        }
    }

    public ArrayList<j> l() {
        k();
        return this.m;
    }

    public ArrayList<j> m() {
        k();
        return this.n;
    }

    public void clearHeader() {
        this.b = null;
        this.f517a = null;
        this.c = null;
        a(false);
    }

    private void a(int i2, CharSequence charSequence, int i3, Drawable drawable, View view) {
        Resources e2 = e();
        if (view != null) {
            this.c = view;
            this.f517a = null;
            this.b = null;
        } else {
            if (i2 > 0) {
                this.f517a = e2.getText(i2);
            } else if (charSequence != null) {
                this.f517a = charSequence;
            }
            if (i3 > 0) {
                this.b = ContextCompat.getDrawable(f(), i3);
            } else if (drawable != null) {
                this.b = drawable;
            }
            this.c = null;
        }
        a(false);
    }

    /* access modifiers changed from: protected */
    public h a(CharSequence charSequence) {
        a(0, charSequence, 0, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public h d(int i2) {
        a(i2, null, 0, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public h a(Drawable drawable) {
        a(0, null, 0, drawable, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public h e(int i2) {
        a(0, null, i2, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public h a(View view) {
        a(0, null, 0, null, view);
        return this;
    }

    public CharSequence n() {
        return this.f517a;
    }

    public Drawable o() {
        return this.b;
    }

    public View p() {
        return this.c;
    }

    public h q() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public boolean r() {
        return this.u;
    }

    public boolean c(j jVar) {
        boolean z2 = false;
        if (!this.x.isEmpty()) {
            h();
            Iterator<WeakReference<o>> it = this.x.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z2 = z2;
                    break;
                }
                WeakReference<o> next = it.next();
                o oVar = next.get();
                if (oVar == null) {
                    this.x.remove(next);
                    z2 = z2;
                } else {
                    z2 = oVar.a(this, jVar);
                    if (z2) {
                        break;
                    }
                }
            }
            i();
            if (z2) {
                this.y = jVar;
            }
        }
        return z2;
    }

    public boolean d(j jVar) {
        boolean z2 = false;
        if (!this.x.isEmpty() && this.y == jVar) {
            h();
            Iterator<WeakReference<o>> it = this.x.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z2 = z2;
                    break;
                }
                WeakReference<o> next = it.next();
                o oVar = next.get();
                if (oVar == null) {
                    this.x.remove(next);
                    z2 = z2;
                } else {
                    z2 = oVar.b(this, jVar);
                    if (z2) {
                        break;
                    }
                }
            }
            i();
            if (z2) {
                this.y = null;
            }
        }
        return z2;
    }

    public j s() {
        return this.y;
    }

    public void c(boolean z2) {
        this.A = z2;
    }
}
