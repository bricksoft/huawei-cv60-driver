package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.o;
import android.support.v7.widget.as;
import android.support.v7.widget.at;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/* access modifiers changed from: package-private */
public final class e extends m implements o, View.OnKeyListener, PopupWindow.OnDismissListener {
    private static final int g = R.layout.abc_cascading_menu_item_layout;
    private PopupWindow.OnDismissListener A;

    /* renamed from: a  reason: collision with root package name */
    final Handler f508a;
    final List<a> b = new ArrayList();
    final ViewTreeObserver.OnGlobalLayoutListener c = new ViewTreeObserver.OnGlobalLayoutListener() {
        /* class android.support.v7.view.menu.e.AnonymousClass1 */

        public void onGlobalLayout() {
            if (e.this.f() && e.this.b.size() > 0 && !e.this.b.get(0).f513a.c()) {
                View view = e.this.d;
                if (view == null || !view.isShown()) {
                    e.this.e();
                    return;
                }
                for (a aVar : e.this.b) {
                    aVar.f513a.d();
                }
            }
        }
    };
    View d;
    ViewTreeObserver e;
    boolean f;
    private final Context h;
    private final int i;
    private final int j;
    private final int k;
    private final boolean l;
    private final List<h> m = new ArrayList();
    private final View.OnAttachStateChangeListener n = new View.OnAttachStateChangeListener() {
        /* class android.support.v7.view.menu.e.AnonymousClass2 */

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            if (e.this.e != null) {
                if (!e.this.e.isAlive()) {
                    e.this.e = view.getViewTreeObserver();
                }
                e.this.e.removeGlobalOnLayoutListener(e.this.c);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    };
    private final as o = new as() {
        /* class android.support.v7.view.menu.e.AnonymousClass3 */

        @Override // android.support.v7.widget.as
        public void a(@NonNull h hVar, @NonNull MenuItem menuItem) {
            e.this.f508a.removeCallbacksAndMessages(hVar);
        }

        @Override // android.support.v7.widget.as
        public void b(@NonNull final h hVar, @NonNull final MenuItem menuItem) {
            int i;
            final a aVar;
            e.this.f508a.removeCallbacksAndMessages(null);
            int i2 = 0;
            int size = e.this.b.size();
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                } else if (hVar == e.this.b.get(i2).b) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i != -1) {
                int i3 = i + 1;
                if (i3 < e.this.b.size()) {
                    aVar = e.this.b.get(i3);
                } else {
                    aVar = null;
                }
                e.this.f508a.postAtTime(new Runnable() {
                    /* class android.support.v7.view.menu.e.AnonymousClass3.AnonymousClass1 */

                    public void run() {
                        if (aVar != null) {
                            e.this.f = true;
                            aVar.b.b(false);
                            e.this.f = false;
                        }
                        if (menuItem.isEnabled() && menuItem.hasSubMenu()) {
                            hVar.a(menuItem, 4);
                        }
                    }
                }, hVar, SystemClock.uptimeMillis() + 200);
            }
        }
    };
    private int p = 0;
    private int q = 0;
    private View r;
    private int s;
    private boolean t;
    private boolean u;
    private int v;
    private int w;
    private boolean x;
    private boolean y;
    private o.a z;

    public e(@NonNull Context context, @NonNull View view, @AttrRes int i2, @StyleRes int i3, boolean z2) {
        this.h = context;
        this.r = view;
        this.j = i2;
        this.k = i3;
        this.l = z2;
        this.x = false;
        this.s = k();
        Resources resources = context.getResources();
        this.i = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(R.dimen.abc_config_prefDialogWidth));
        this.f508a = new Handler();
    }

    @Override // android.support.v7.view.menu.m
    public void b(boolean z2) {
        this.x = z2;
    }

    private at j() {
        at atVar = new at(this.h, null, this.j, this.k);
        atVar.a(this.o);
        atVar.a((AdapterView.OnItemClickListener) this);
        atVar.a((PopupWindow.OnDismissListener) this);
        atVar.b(this.r);
        atVar.e(this.q);
        atVar.a(true);
        atVar.h(2);
        return atVar;
    }

    @Override // android.support.v7.view.menu.s
    public void d() {
        if (!f()) {
            for (h hVar : this.m) {
                c(hVar);
            }
            this.m.clear();
            this.d = this.r;
            if (this.d != null) {
                boolean z2 = this.e == null;
                this.e = this.d.getViewTreeObserver();
                if (z2) {
                    this.e.addOnGlobalLayoutListener(this.c);
                }
                this.d.addOnAttachStateChangeListener(this.n);
            }
        }
    }

    @Override // android.support.v7.view.menu.s
    public void e() {
        int size = this.b.size();
        if (size > 0) {
            a[] aVarArr = (a[]) this.b.toArray(new a[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                a aVar = aVarArr[i2];
                if (aVar.f513a.f()) {
                    aVar.f513a.e();
                }
            }
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        e();
        return true;
    }

    private int k() {
        if (ViewCompat.getLayoutDirection(this.r) == 1) {
            return 0;
        }
        return 1;
    }

    private int d(int i2) {
        ListView a2 = this.b.get(this.b.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.d.getWindowVisibleDisplayFrame(rect);
        if (this.s != 1) {
            return iArr[0] - i2 < 0 ? 1 : 0;
        }
        return (a2.getWidth() + iArr[0]) + i2 > rect.right ? 0 : 1;
    }

    @Override // android.support.v7.view.menu.m
    public void a(h hVar) {
        hVar.a(this, this.h);
        if (f()) {
            c(hVar);
        } else {
            this.m.add(hVar);
        }
    }

    private void c(@NonNull h hVar) {
        View view;
        a aVar;
        boolean z2;
        int i2;
        int i3;
        int i4;
        LayoutInflater from = LayoutInflater.from(this.h);
        g gVar = new g(hVar, from, this.l, g);
        if (!f() && this.x) {
            gVar.a(true);
        } else if (f()) {
            gVar.a(m.b(hVar));
        }
        int a2 = a(gVar, null, this.h, this.i);
        at j2 = j();
        j2.a((ListAdapter) gVar);
        j2.g(a2);
        j2.e(this.q);
        if (this.b.size() > 0) {
            a aVar2 = this.b.get(this.b.size() - 1);
            view = a(aVar2, hVar);
            aVar = aVar2;
        } else {
            view = null;
            aVar = null;
        }
        if (view != null) {
            j2.c(false);
            j2.a((Object) null);
            int d2 = d(a2);
            if (d2 == 1) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.s = d2;
            if (Build.VERSION.SDK_INT >= 26) {
                j2.b(view);
                i3 = 0;
                i2 = 0;
            } else {
                int[] iArr = new int[2];
                this.r.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                if ((this.q & 7) == 5) {
                    iArr[0] = iArr[0] + this.r.getWidth();
                    iArr2[0] = iArr2[0] + view.getWidth();
                }
                i2 = iArr2[0] - iArr[0];
                i3 = iArr2[1] - iArr[1];
            }
            if ((this.q & 5) == 5) {
                if (z2) {
                    i4 = i2 + a2;
                } else {
                    i4 = i2 - view.getWidth();
                }
            } else if (z2) {
                i4 = view.getWidth() + i2;
            } else {
                i4 = i2 - a2;
            }
            j2.c(i4);
            j2.b(true);
            j2.d(i3);
        } else {
            if (this.t) {
                j2.c(this.v);
            }
            if (this.u) {
                j2.d(this.w);
            }
            j2.a(i());
        }
        this.b.add(new a(j2, hVar, this.s));
        j2.d();
        ListView g2 = j2.g();
        g2.setOnKeyListener(this);
        if (aVar == null && this.y && hVar.n() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(R.layout.abc_popup_menu_header_item_layout, (ViewGroup) g2, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(hVar.n());
            g2.addHeaderView(frameLayout, null, false);
            j2.d();
        }
    }

    private MenuItem a(@NonNull h hVar, @NonNull h hVar2) {
        int size = hVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = hVar.getItem(i2);
            if (item.hasSubMenu() && hVar2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    @Nullable
    private View a(@NonNull a aVar, @NonNull h hVar) {
        g gVar;
        int i2;
        int i3;
        int i4 = 0;
        MenuItem a2 = a(aVar.b, hVar);
        if (a2 == null) {
            return null;
        }
        ListView a3 = aVar.a();
        ListAdapter adapter = a3.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            gVar = (g) headerViewListAdapter.getWrappedAdapter();
        } else {
            gVar = (g) adapter;
            i2 = 0;
        }
        int count = gVar.getCount();
        while (true) {
            if (i4 >= count) {
                i3 = -1;
                break;
            } else if (a2 == gVar.getItem(i4)) {
                i3 = i4;
                break;
            } else {
                i4++;
            }
        }
        if (i3 == -1) {
            return null;
        }
        int firstVisiblePosition = (i3 + i2) - a3.getFirstVisiblePosition();
        if (firstVisiblePosition < 0 || firstVisiblePosition >= a3.getChildCount()) {
            return null;
        }
        return a3.getChildAt(firstVisiblePosition);
    }

    @Override // android.support.v7.view.menu.s
    public boolean f() {
        return this.b.size() > 0 && this.b.get(0).f513a.f();
    }

    public void onDismiss() {
        a aVar;
        int size = this.b.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                aVar = null;
                break;
            }
            aVar = this.b.get(i2);
            if (!aVar.f513a.f()) {
                break;
            }
            i2++;
        }
        if (aVar != null) {
            aVar.b.b(false);
        }
    }

    @Override // android.support.v7.view.menu.o
    public void a(boolean z2) {
        for (a aVar : this.b) {
            a(aVar.a().getAdapter()).notifyDataSetChanged();
        }
    }

    @Override // android.support.v7.view.menu.o
    public void a(o.a aVar) {
        this.z = aVar;
    }

    @Override // android.support.v7.view.menu.o
    public boolean a(u uVar) {
        for (a aVar : this.b) {
            if (uVar == aVar.b) {
                aVar.a().requestFocus();
                return true;
            }
        }
        if (!uVar.hasVisibleItems()) {
            return false;
        }
        a((h) uVar);
        if (this.z != null) {
            this.z.a(uVar);
        }
        return true;
    }

    private int d(@NonNull h hVar) {
        int size = this.b.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (hVar == this.b.get(i2).b) {
                return i2;
            }
        }
        return -1;
    }

    @Override // android.support.v7.view.menu.o
    public void a(h hVar, boolean z2) {
        int d2 = d(hVar);
        if (d2 >= 0) {
            int i2 = d2 + 1;
            if (i2 < this.b.size()) {
                this.b.get(i2).b.b(false);
            }
            a remove = this.b.remove(d2);
            remove.b.b(this);
            if (this.f) {
                remove.f513a.b((Object) null);
                remove.f513a.b(0);
            }
            remove.f513a.e();
            int size = this.b.size();
            if (size > 0) {
                this.s = this.b.get(size - 1).c;
            } else {
                this.s = k();
            }
            if (size == 0) {
                e();
                if (this.z != null) {
                    this.z.a(hVar, true);
                }
                if (this.e != null) {
                    if (this.e.isAlive()) {
                        this.e.removeGlobalOnLayoutListener(this.c);
                    }
                    this.e = null;
                }
                this.d.removeOnAttachStateChangeListener(this.n);
                this.A.onDismiss();
            } else if (z2) {
                this.b.get(0).b.b(false);
            }
        }
    }

    @Override // android.support.v7.view.menu.o
    public boolean a() {
        return false;
    }

    @Override // android.support.v7.view.menu.o
    public Parcelable c() {
        return null;
    }

    @Override // android.support.v7.view.menu.o
    public void a(Parcelable parcelable) {
    }

    @Override // android.support.v7.view.menu.m
    public void a(int i2) {
        if (this.p != i2) {
            this.p = i2;
            this.q = GravityCompat.getAbsoluteGravity(i2, ViewCompat.getLayoutDirection(this.r));
        }
    }

    @Override // android.support.v7.view.menu.m
    public void a(@NonNull View view) {
        if (this.r != view) {
            this.r = view;
            this.q = GravityCompat.getAbsoluteGravity(this.p, ViewCompat.getLayoutDirection(this.r));
        }
    }

    @Override // android.support.v7.view.menu.m
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.A = onDismissListener;
    }

    @Override // android.support.v7.view.menu.s
    public ListView g() {
        if (this.b.isEmpty()) {
            return null;
        }
        return this.b.get(this.b.size() - 1).a();
    }

    @Override // android.support.v7.view.menu.m
    public void b(int i2) {
        this.t = true;
        this.v = i2;
    }

    @Override // android.support.v7.view.menu.m
    public void c(int i2) {
        this.u = true;
        this.w = i2;
    }

    @Override // android.support.v7.view.menu.m
    public void c(boolean z2) {
        this.y = z2;
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.view.menu.m
    public boolean h() {
        return false;
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public final at f513a;
        public final h b;
        public final int c;

        public a(@NonNull at atVar, @NonNull h hVar, int i) {
            this.f513a = atVar;
            this.b = hVar;
            this.c = i;
        }

        public ListView a() {
            return this.f513a.g();
        }
    }
}
