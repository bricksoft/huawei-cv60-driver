package android.support.v7.view;

import android.support.annotation.RestrictTo;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class h {

    /* renamed from: a  reason: collision with root package name */
    final ArrayList<ViewPropertyAnimatorCompat> f499a = new ArrayList<>();
    ViewPropertyAnimatorListener b;
    private long c = -1;
    private Interpolator d;
    private boolean e;
    private final ViewPropertyAnimatorListenerAdapter f = new ViewPropertyAnimatorListenerAdapter() {
        /* class android.support.v7.view.h.AnonymousClass1 */
        private boolean b = false;
        private int c = 0;

        @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
        public void onAnimationStart(View view) {
            if (!this.b) {
                this.b = true;
                if (h.this.b != null) {
                    h.this.b.onAnimationStart(null);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.c = 0;
            this.b = false;
            h.this.b();
        }

        @Override // android.support.v4.view.ViewPropertyAnimatorListenerAdapter, android.support.v4.view.ViewPropertyAnimatorListener
        public void onAnimationEnd(View view) {
            int i = this.c + 1;
            this.c = i;
            if (i == h.this.f499a.size()) {
                if (h.this.b != null) {
                    h.this.b.onAnimationEnd(null);
                }
                a();
            }
        }
    };

    public h a(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat) {
        if (!this.e) {
            this.f499a.add(viewPropertyAnimatorCompat);
        }
        return this;
    }

    public h a(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2) {
        this.f499a.add(viewPropertyAnimatorCompat);
        viewPropertyAnimatorCompat2.setStartDelay(viewPropertyAnimatorCompat.getDuration());
        this.f499a.add(viewPropertyAnimatorCompat2);
        return this;
    }

    public void a() {
        if (!this.e) {
            Iterator<ViewPropertyAnimatorCompat> it = this.f499a.iterator();
            while (it.hasNext()) {
                ViewPropertyAnimatorCompat next = it.next();
                if (this.c >= 0) {
                    next.setDuration(this.c);
                }
                if (this.d != null) {
                    next.setInterpolator(this.d);
                }
                if (this.b != null) {
                    next.setListener(this.f);
                }
                next.start();
            }
            this.e = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.e = false;
    }

    public void c() {
        if (this.e) {
            Iterator<ViewPropertyAnimatorCompat> it = this.f499a.iterator();
            while (it.hasNext()) {
                it.next().cancel();
            }
            this.e = false;
        }
    }

    public h a(long j) {
        if (!this.e) {
            this.c = j;
        }
        return this;
    }

    public h a(Interpolator interpolator) {
        if (!this.e) {
            this.d = interpolator;
        }
        return this;
    }

    public h a(ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        if (!this.e) {
            this.b = viewPropertyAnimatorListener;
        }
        return this;
    }
}
