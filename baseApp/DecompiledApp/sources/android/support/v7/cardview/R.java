package android.support.v7.cardview;

public final class R {

    public static final class attr {
        public static final int cardBackgroundColor = 2130772201;
        public static final int cardCornerRadius = 2130772202;
        public static final int cardElevation = 2130772203;
        public static final int cardMaxElevation = 2130772204;
        public static final int cardPreventCornerOverlap = 2130772206;
        public static final int cardUseCompatPadding = 2130772205;
        public static final int cardViewStyle = 2130771972;
        public static final int contentPadding = 2130772207;
        public static final int contentPaddingBottom = 2130772211;
        public static final int contentPaddingLeft = 2130772208;
        public static final int contentPaddingRight = 2130772209;
        public static final int contentPaddingTop = 2130772210;
    }

    public static final class color {
        public static final int cardview_dark_background = 2131689510;
        public static final int cardview_light_background = 2131689511;
        public static final int cardview_shadow_end_color = 2131689512;
        public static final int cardview_shadow_start_color = 2131689513;
    }

    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131427417;
        public static final int cardview_default_elevation = 2131427418;
        public static final int cardview_default_radius = 2131427419;
    }

    public static final class style {
        public static final int Base_CardView = 2131493036;
        public static final int CardView = 2131493009;
        public static final int CardView_Dark = 2131493114;
        public static final int CardView_Light = 2131493115;
    }

    public static final class styleable {
        public static final int[] CardView = {16843071, 16843072, com.huawei.cvIntl60.R.attr.cardBackgroundColor, com.huawei.cvIntl60.R.attr.cardCornerRadius, com.huawei.cvIntl60.R.attr.cardElevation, com.huawei.cvIntl60.R.attr.cardMaxElevation, com.huawei.cvIntl60.R.attr.cardUseCompatPadding, com.huawei.cvIntl60.R.attr.cardPreventCornerOverlap, com.huawei.cvIntl60.R.attr.contentPadding, com.huawei.cvIntl60.R.attr.contentPaddingLeft, com.huawei.cvIntl60.R.attr.contentPaddingRight, com.huawei.cvIntl60.R.attr.contentPaddingTop, com.huawei.cvIntl60.R.attr.contentPaddingBottom};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 7;
        public static final int CardView_cardUseCompatPadding = 6;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 12;
        public static final int CardView_contentPaddingLeft = 9;
        public static final int CardView_contentPaddingRight = 10;
        public static final int CardView_contentPaddingTop = 11;
    }
}
