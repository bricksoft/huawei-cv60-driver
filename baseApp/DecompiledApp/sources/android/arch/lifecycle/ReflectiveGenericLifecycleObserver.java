package android.arch.lifecycle;

import android.arch.lifecycle.a;
import android.arch.lifecycle.c;

class ReflectiveGenericLifecycleObserver implements GenericLifecycleObserver {

    /* renamed from: a  reason: collision with root package name */
    private final Object f202a;
    private final a.C0012a b = a.f204a.b(this.f202a.getClass());

    ReflectiveGenericLifecycleObserver(Object obj) {
        this.f202a = obj;
    }

    @Override // android.arch.lifecycle.GenericLifecycleObserver
    public void a(e eVar, c.a aVar) {
        this.b.a(eVar, aVar, this.f202a);
    }
}
