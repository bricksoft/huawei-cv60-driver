package android.arch.lifecycle;

import android.arch.lifecycle.c;
import android.support.annotation.RestrictTo;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class CompositeGeneratedAdaptersObserver implements GenericLifecycleObserver {

    /* renamed from: a  reason: collision with root package name */
    private final b[] f196a;

    CompositeGeneratedAdaptersObserver(b[] bVarArr) {
        this.f196a = bVarArr;
    }

    @Override // android.arch.lifecycle.GenericLifecycleObserver
    public void a(e eVar, c.a aVar) {
        i iVar = new i();
        for (b bVar : this.f196a) {
            bVar.a(eVar, aVar, false, iVar);
        }
        for (b bVar2 : this.f196a) {
            bVar2.a(eVar, aVar, true, iVar);
        }
    }
}
