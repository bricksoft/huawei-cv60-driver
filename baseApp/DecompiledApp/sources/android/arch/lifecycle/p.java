package android.arch.lifecycle;

import java.util.HashMap;

public class p {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap<String, n> f215a = new HashMap<>();

    /* access modifiers changed from: package-private */
    public final void a(String str, n nVar) {
        n put = this.f215a.put(str, nVar);
        if (put != null) {
            put.onCleared();
        }
    }

    /* access modifiers changed from: package-private */
    public final n a(String str) {
        return this.f215a.get(str);
    }

    public final void a() {
        for (n nVar : this.f215a.values()) {
            nVar.onCleared();
        }
        this.f215a.clear();
    }
}
