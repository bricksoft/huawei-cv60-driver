package android.arch.lifecycle;

import android.support.annotation.Nullable;

public interface k<T> {
    void onChanged(@Nullable T t);
}
