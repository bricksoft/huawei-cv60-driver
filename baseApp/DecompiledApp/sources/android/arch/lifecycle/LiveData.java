package android.arch.lifecycle;

import android.arch.lifecycle.c;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Iterator;
import java.util.Map;

public abstract class LiveData<T> {
    private static final Object NOT_SET = new Object();
    static final int START_VERSION = -1;
    private int mActiveCount = 0;
    private volatile Object mData = NOT_SET;
    private final Object mDataLock = new Object();
    private boolean mDispatchInvalidated;
    private boolean mDispatchingValue;
    private android.arch.a.b.b<k<T>, LiveData<T>.b> mObservers = new android.arch.a.b.b<>();
    private volatile Object mPendingData = NOT_SET;
    private final Runnable mPostValueRunnable = new Runnable() {
        /* class android.arch.lifecycle.LiveData.AnonymousClass1 */

        /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: android.arch.lifecycle.LiveData */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            Object obj;
            synchronized (LiveData.this.mDataLock) {
                obj = LiveData.this.mPendingData;
                LiveData.this.mPendingData = LiveData.NOT_SET;
            }
            LiveData.this.setValue(obj);
        }
    };
    private int mVersion = -1;

    /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: android.arch.lifecycle.k<T> */
    /* JADX WARN: Multi-variable type inference failed */
    private void considerNotify(LiveData<T>.b bVar) {
        if (bVar.d) {
            if (!bVar.a()) {
                bVar.a(false);
            } else if (bVar.e < this.mVersion) {
                bVar.e = this.mVersion;
                bVar.c.onChanged(this.mData);
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void dispatchingValue(@Nullable LiveData<T>.b bVar) {
        if (this.mDispatchingValue) {
            this.mDispatchInvalidated = true;
            return;
        }
        this.mDispatchingValue = true;
        do {
            this.mDispatchInvalidated = false;
            if (bVar == null) {
                android.arch.a.b.b<K, V>.d c = this.mObservers.c();
                while (c.hasNext()) {
                    considerNotify((b) ((Map.Entry) c.next()).getValue());
                    if (this.mDispatchInvalidated) {
                        break;
                    }
                }
            } else {
                considerNotify(bVar);
                bVar = null;
            }
        } while (this.mDispatchInvalidated);
        this.mDispatchingValue = false;
    }

    @MainThread
    public void observe(@NonNull e eVar, @NonNull k<T> kVar) {
        if (eVar.getLifecycle().a() != c.b.DESTROYED) {
            LifecycleBoundObserver lifecycleBoundObserver = new LifecycleBoundObserver(eVar, kVar);
            LiveData<T>.b a2 = this.mObservers.a(kVar, lifecycleBoundObserver);
            if (a2 != null && !a2.a(eVar)) {
                throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
            } else if (a2 == null) {
                eVar.getLifecycle().a(lifecycleBoundObserver);
            }
        }
    }

    @MainThread
    public void observeForever(@NonNull k<T> kVar) {
        a aVar = new a(kVar);
        LiveData<T>.b a2 = this.mObservers.a(kVar, aVar);
        if (a2 != null && (a2 instanceof LifecycleBoundObserver)) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        } else if (a2 == null) {
            aVar.a(true);
        }
    }

    @MainThread
    public void removeObserver(@NonNull k<T> kVar) {
        assertMainThread("removeObserver");
        LiveData<T>.b b2 = this.mObservers.b(kVar);
        if (b2 != null) {
            b2.b();
            b2.a(false);
        }
    }

    @MainThread
    public void removeObservers(@NonNull e eVar) {
        assertMainThread("removeObservers");
        Iterator<Map.Entry<k<T>, LiveData<T>.b>> it = this.mObservers.iterator();
        while (it.hasNext()) {
            Map.Entry<k<T>, LiveData<T>.b> next = it.next();
            if (next.getValue().a(eVar)) {
                removeObserver(next.getKey());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void postValue(T t) {
        boolean z;
        synchronized (this.mDataLock) {
            z = this.mPendingData == NOT_SET;
            this.mPendingData = t;
        }
        if (z) {
            android.arch.a.a.a.a().b(this.mPostValueRunnable);
        }
    }

    /* access modifiers changed from: protected */
    @MainThread
    public void setValue(T t) {
        assertMainThread("setValue");
        this.mVersion++;
        this.mData = t;
        dispatchingValue(null);
    }

    @Nullable
    public T getValue() {
        T t = (T) this.mData;
        if (t != NOT_SET) {
            return t;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public int getVersion() {
        return this.mVersion;
    }

    /* access modifiers changed from: protected */
    public void onActive() {
    }

    /* access modifiers changed from: protected */
    public void onInactive() {
    }

    public boolean hasObservers() {
        return this.mObservers.a() > 0;
    }

    public boolean hasActiveObservers() {
        return this.mActiveCount > 0;
    }

    class LifecycleBoundObserver extends LiveData<T>.b implements GenericLifecycleObserver {
        @NonNull

        /* renamed from: a  reason: collision with root package name */
        final e f200a;

        LifecycleBoundObserver(e eVar, @NonNull k<T> kVar) {
            super(kVar);
            this.f200a = eVar;
        }

        /* access modifiers changed from: package-private */
        @Override // android.arch.lifecycle.LiveData.b
        public boolean a() {
            return this.f200a.getLifecycle().a().a(c.b.STARTED);
        }

        @Override // android.arch.lifecycle.GenericLifecycleObserver
        public void a(e eVar, c.a aVar) {
            if (this.f200a.getLifecycle().a() == c.b.DESTROYED) {
                LiveData.this.removeObserver(this.c);
            } else {
                a(a());
            }
        }

        /* access modifiers changed from: package-private */
        @Override // android.arch.lifecycle.LiveData.b
        public boolean a(e eVar) {
            return this.f200a == eVar;
        }

        /* access modifiers changed from: package-private */
        @Override // android.arch.lifecycle.LiveData.b
        public void b() {
            this.f200a.getLifecycle().b(this);
        }
    }

    /* access modifiers changed from: private */
    public abstract class b {
        final k<T> c;
        boolean d;
        int e = -1;

        /* access modifiers changed from: package-private */
        public abstract boolean a();

        b(k<T> kVar) {
            this.c = kVar;
        }

        /* access modifiers changed from: package-private */
        public boolean a(e eVar) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public void b() {
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            int i = 1;
            if (z != this.d) {
                this.d = z;
                boolean z2 = LiveData.this.mActiveCount == 0;
                LiveData liveData = LiveData.this;
                int i2 = liveData.mActiveCount;
                if (!this.d) {
                    i = -1;
                }
                liveData.mActiveCount = i + i2;
                if (z2 && this.d) {
                    LiveData.this.onActive();
                }
                if (LiveData.this.mActiveCount == 0 && !this.d) {
                    LiveData.this.onInactive();
                }
                if (this.d) {
                    LiveData.this.dispatchingValue(this);
                }
            }
        }
    }

    private class a extends LiveData<T>.b {
        a(k<T> kVar) {
            super(kVar);
        }

        /* access modifiers changed from: package-private */
        @Override // android.arch.lifecycle.LiveData.b
        public boolean a() {
            return true;
        }
    }

    private static void assertMainThread(String str) {
        if (!android.arch.a.a.a.a().b()) {
            throw new IllegalStateException("Cannot invoke " + str + " on a background" + " thread");
        }
    }
}
