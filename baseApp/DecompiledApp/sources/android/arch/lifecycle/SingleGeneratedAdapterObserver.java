package android.arch.lifecycle;

import android.arch.lifecycle.c;
import android.support.annotation.RestrictTo;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class SingleGeneratedAdapterObserver implements GenericLifecycleObserver {

    /* renamed from: a  reason: collision with root package name */
    private final b f203a;

    SingleGeneratedAdapterObserver(b bVar) {
        this.f203a = bVar;
    }

    @Override // android.arch.lifecycle.GenericLifecycleObserver
    public void a(e eVar, c.a aVar) {
        this.f203a.a(eVar, aVar, false, null);
        this.f203a.a(eVar, aVar, true, null);
    }
}
