package android.arch.lifecycle;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

public class o {

    /* renamed from: a  reason: collision with root package name */
    private final a f214a;
    private final p b;

    public interface a {
        @NonNull
        <T extends n> T create(@NonNull Class<T> cls);
    }

    public o(@NonNull p pVar, @NonNull a aVar) {
        this.f214a = aVar;
        this.b = pVar;
    }

    @NonNull
    @MainThread
    public <T extends n> T a(@NonNull Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return (T) a("android.arch.lifecycle.ViewModelProvider.DefaultKey:" + canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    @NonNull
    @MainThread
    public <T extends n> T a(@NonNull String str, @NonNull Class<T> cls) {
        T t = (T) this.b.a(str);
        if (cls.isInstance(t)) {
            return t;
        }
        if (t != null) {
        }
        T t2 = (T) this.f214a.create(cls);
        this.b.a(str, t2);
        return t2;
    }
}
