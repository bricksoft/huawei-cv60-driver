package android.arch.lifecycle;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

public abstract class c {

    public enum a {
        ON_CREATE,
        ON_START,
        ON_RESUME,
        ON_PAUSE,
        ON_STOP,
        ON_DESTROY,
        ON_ANY
    }

    @NonNull
    @MainThread
    public abstract b a();

    @MainThread
    public abstract void a(@NonNull d dVar);

    @MainThread
    public abstract void b(@NonNull d dVar);

    public enum b {
        DESTROYED,
        INITIALIZED,
        CREATED,
        STARTED,
        RESUMED;

        public boolean a(@NonNull b bVar) {
            return compareTo(bVar) >= 0;
        }
    }
}
