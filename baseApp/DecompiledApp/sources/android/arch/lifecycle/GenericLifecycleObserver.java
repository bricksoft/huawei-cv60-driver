package android.arch.lifecycle;

import android.arch.lifecycle.c;
import android.support.annotation.RestrictTo;

@RestrictTo({RestrictTo.Scope.LIBRARY})
public interface GenericLifecycleObserver extends d {
    void a(e eVar, c.a aVar);
}
