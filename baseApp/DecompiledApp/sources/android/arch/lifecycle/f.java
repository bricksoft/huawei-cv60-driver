package android.arch.lifecycle;

import android.arch.a.b.b;
import android.arch.lifecycle.c;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class f extends c {

    /* renamed from: a  reason: collision with root package name */
    private android.arch.a.b.a<d, a> f208a = new android.arch.a.b.a<>();
    private c.b b;
    private final WeakReference<e> c;
    private int d = 0;
    private boolean e = false;
    private boolean f = false;
    private ArrayList<c.b> g = new ArrayList<>();

    public f(@NonNull e eVar) {
        this.c = new WeakReference<>(eVar);
        this.b = c.b.INITIALIZED;
    }

    @MainThread
    public void a(@NonNull c.b bVar) {
        b(bVar);
    }

    public void a(@NonNull c.a aVar) {
        b(b(aVar));
    }

    private void b(c.b bVar) {
        if (this.b != bVar) {
            this.b = bVar;
            if (this.e || this.d != 0) {
                this.f = true;
                return;
            }
            this.e = true;
            d();
            this.e = false;
        }
    }

    private boolean b() {
        if (this.f208a.a() == 0) {
            return true;
        }
        c.b bVar = this.f208a.d().getValue().f210a;
        c.b bVar2 = this.f208a.e().getValue().f210a;
        return bVar == bVar2 && this.b == bVar2;
    }

    private c.b c(d dVar) {
        c.b bVar;
        c.b bVar2;
        Map.Entry<d, a> d2 = this.f208a.d(dVar);
        if (d2 != null) {
            bVar = d2.getValue().f210a;
        } else {
            bVar = null;
        }
        if (!this.g.isEmpty()) {
            bVar2 = this.g.get(this.g.size() - 1);
        } else {
            bVar2 = null;
        }
        return a(a(this.b, bVar), bVar2);
    }

    @Override // android.arch.lifecycle.c
    public void a(@NonNull d dVar) {
        e eVar;
        a aVar = new a(dVar, this.b == c.b.DESTROYED ? c.b.DESTROYED : c.b.INITIALIZED);
        if (this.f208a.a(dVar, aVar) == null && (eVar = this.c.get()) != null) {
            boolean z = this.d != 0 || this.e;
            c.b c2 = c(dVar);
            this.d++;
            while (aVar.f210a.compareTo((Enum) c2) < 0 && this.f208a.c(dVar)) {
                c(aVar.f210a);
                aVar.a(eVar, e(aVar.f210a));
                c();
                c2 = c(dVar);
            }
            if (!z) {
                d();
            }
            this.d--;
        }
    }

    private void c() {
        this.g.remove(this.g.size() - 1);
    }

    private void c(c.b bVar) {
        this.g.add(bVar);
    }

    @Override // android.arch.lifecycle.c
    public void b(@NonNull d dVar) {
        this.f208a.b(dVar);
    }

    @Override // android.arch.lifecycle.c
    @NonNull
    public c.b a() {
        return this.b;
    }

    static c.b b(c.a aVar) {
        switch (aVar) {
            case ON_CREATE:
            case ON_STOP:
                return c.b.CREATED;
            case ON_START:
            case ON_PAUSE:
                return c.b.STARTED;
            case ON_RESUME:
                return c.b.RESUMED;
            case ON_DESTROY:
                return c.b.DESTROYED;
            default:
                throw new IllegalArgumentException("Unexpected event value " + aVar);
        }
    }

    private static c.a d(c.b bVar) {
        switch (bVar) {
            case INITIALIZED:
                throw new IllegalArgumentException();
            case CREATED:
                return c.a.ON_DESTROY;
            case STARTED:
                return c.a.ON_STOP;
            case RESUMED:
                return c.a.ON_PAUSE;
            case DESTROYED:
                throw new IllegalArgumentException();
            default:
                throw new IllegalArgumentException("Unexpected state value " + bVar);
        }
    }

    private static c.a e(c.b bVar) {
        switch (bVar) {
            case INITIALIZED:
            case DESTROYED:
                return c.a.ON_CREATE;
            case CREATED:
                return c.a.ON_START;
            case STARTED:
                return c.a.ON_RESUME;
            case RESUMED:
                throw new IllegalArgumentException();
            default:
                throw new IllegalArgumentException("Unexpected state value " + bVar);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v3, resolved type: android.arch.a.b.a<android.arch.lifecycle.d, android.arch.lifecycle.f$a> */
    /* JADX WARN: Multi-variable type inference failed */
    private void a(e eVar) {
        b<K, V>.d c2 = this.f208a.c();
        while (c2.hasNext() && !this.f) {
            Map.Entry entry = (Map.Entry) c2.next();
            a aVar = (a) entry.getValue();
            while (aVar.f210a.compareTo((Enum) this.b) < 0 && !this.f && this.f208a.c(entry.getKey())) {
                c(aVar.f210a);
                aVar.a(eVar, e(aVar.f210a));
                c();
            }
        }
    }

    private void b(e eVar) {
        Iterator<Map.Entry<d, a>> b2 = this.f208a.b();
        while (b2.hasNext() && !this.f) {
            Map.Entry<d, a> next = b2.next();
            a value = next.getValue();
            while (value.f210a.compareTo((Enum) this.b) > 0 && !this.f && this.f208a.c(next.getKey())) {
                c.a d2 = d(value.f210a);
                c(b(d2));
                value.a(eVar, d2);
                c();
            }
        }
    }

    private void d() {
        e eVar = this.c.get();
        if (eVar == null) {
            Log.w("LifecycleRegistry", "LifecycleOwner is garbage collected, you shouldn't try dispatch new events from it.");
            return;
        }
        while (!b()) {
            this.f = false;
            if (this.b.compareTo((Enum) this.f208a.d().getValue().f210a) < 0) {
                b(eVar);
            }
            Map.Entry<d, a> e2 = this.f208a.e();
            if (!this.f && e2 != null && this.b.compareTo((Enum) e2.getValue().f210a) > 0) {
                a(eVar);
            }
        }
        this.f = false;
    }

    static c.b a(@NonNull c.b bVar, @Nullable c.b bVar2) {
        return (bVar2 == null || bVar2.compareTo(bVar) >= 0) ? bVar : bVar2;
    }

    /* access modifiers changed from: package-private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        c.b f210a;
        GenericLifecycleObserver b;

        a(d dVar, c.b bVar) {
            this.b = h.a(dVar);
            this.f210a = bVar;
        }

        /* access modifiers changed from: package-private */
        public void a(e eVar, c.a aVar) {
            c.b b2 = f.b(aVar);
            this.f210a = f.a(this.f210a, b2);
            this.b.a(eVar, aVar);
            this.f210a = b2;
        }
    }
}
