package android.arch.a.b;

import android.arch.a.b.b;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import java.util.HashMap;
import java.util.Map;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class a<K, V> extends b<K, V> {

    /* renamed from: a  reason: collision with root package name */
    private HashMap<K, b.c<K, V>> f191a = new HashMap<>();

    /* access modifiers changed from: protected */
    @Override // android.arch.a.b.b
    public b.c<K, V> a(K k) {
        return this.f191a.get(k);
    }

    @Override // android.arch.a.b.b
    public V a(@NonNull K k, @NonNull V v) {
        b.c<K, V> a2 = a((Object) k);
        if (a2 != null) {
            return a2.b;
        }
        this.f191a.put(k, b(k, v));
        return null;
    }

    @Override // android.arch.a.b.b
    public V b(@NonNull K k) {
        V v = (V) super.b(k);
        this.f191a.remove(k);
        return v;
    }

    public boolean c(K k) {
        return this.f191a.containsKey(k);
    }

    public Map.Entry<K, V> d(K k) {
        if (c(k)) {
            return this.f191a.get(k).d;
        }
        return null;
    }
}
