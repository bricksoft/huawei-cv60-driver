package android.arch.a.a;

import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import java.util.concurrent.Executor;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class a extends c {

    /* renamed from: a  reason: collision with root package name */
    private static volatile a f189a;
    @NonNull
    private static final Executor d = new Executor() {
        /* class android.arch.a.a.a.AnonymousClass1 */

        public void execute(Runnable runnable) {
            a.a().b(runnable);
        }
    };
    @NonNull
    private static final Executor e = new Executor() {
        /* class android.arch.a.a.a.AnonymousClass2 */

        public void execute(Runnable runnable) {
            a.a().a(runnable);
        }
    };
    @NonNull
    private c b = this.c;
    @NonNull
    private c c = new b();

    private a() {
    }

    @NonNull
    public static a a() {
        if (f189a != null) {
            return f189a;
        }
        synchronized (a.class) {
            if (f189a == null) {
                f189a = new a();
            }
        }
        return f189a;
    }

    @Override // android.arch.a.a.c
    public void a(Runnable runnable) {
        this.b.a(runnable);
    }

    @Override // android.arch.a.a.c
    public void b(Runnable runnable) {
        this.b.b(runnable);
    }

    @Override // android.arch.a.a.c
    public boolean b() {
        return this.b.b();
    }
}
