package android.arch.a.a;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class b extends c {

    /* renamed from: a  reason: collision with root package name */
    private final Object f190a = new Object();
    private ExecutorService b = Executors.newFixedThreadPool(2);
    @Nullable
    private volatile Handler c;

    @Override // android.arch.a.a.c
    public void a(Runnable runnable) {
        this.b.execute(runnable);
    }

    @Override // android.arch.a.a.c
    public void b(Runnable runnable) {
        if (this.c == null) {
            synchronized (this.f190a) {
                if (this.c == null) {
                    this.c = new Handler(Looper.getMainLooper());
                }
            }
        }
        this.c.post(runnable);
    }

    @Override // android.arch.a.a.c
    public boolean b() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
}
