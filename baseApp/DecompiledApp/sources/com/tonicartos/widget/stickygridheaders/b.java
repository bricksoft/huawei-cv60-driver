package com.tonicartos.widget.stickygridheaders;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import com.google.android.exoplayer.C;

public class b extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private final Context f1250a;
    private int b;
    private boolean c = false;
    private DataSetObserver d = new DataSetObserver() {
        /* class com.tonicartos.widget.stickygridheaders.b.AnonymousClass1 */

        public void onChanged() {
            b.this.a();
        }

        public void onInvalidated() {
            b.this.c = false;
        }
    };
    private final a e;
    private StickyGridHeadersGridView f;
    private View g;
    private View h;
    private int i = 1;

    public b(Context context, StickyGridHeadersGridView stickyGridHeadersGridView, a aVar) {
        this.f1250a = context;
        this.e = aVar;
        this.f = stickyGridHeadersGridView;
        aVar.registerDataSetObserver(this.d);
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public int getCount() {
        if (this.c) {
            return this.b;
        }
        this.b = 0;
        int a2 = this.e.a();
        if (a2 == 0) {
            this.b = this.e.getCount();
            this.c = true;
            return this.b;
        }
        for (int i2 = 0; i2 < a2; i2++) {
            this.b += this.e.a(i2) + d(i2) + this.i;
        }
        this.c = true;
        return this.b;
    }

    public Object getItem(int i2) {
        c c2 = c(i2);
        if (c2.b == -1 || c2.b == -2) {
            return null;
        }
        return this.e.getItem(c2.b);
    }

    public long getItemId(int i2) {
        c c2 = c(i2);
        if (c2.b == -2) {
            return -1;
        }
        if (c2.b == -1) {
            return -2;
        }
        if (c2.b == -3) {
            return -3;
        }
        return this.e.getItemId(c2.b);
    }

    public int getItemViewType(int i2) {
        c c2 = c(i2);
        if (c2.b == -2) {
            return 1;
        }
        if (c2.b == -1) {
            return 0;
        }
        if (c2.b == -3) {
            return 2;
        }
        int itemViewType = this.e.getItemViewType(c2.b);
        return itemViewType != -1 ? itemViewType + 3 : itemViewType;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        c c2 = c(i2);
        if (c2.b == -2) {
            C0098b b2 = b(c2.f1254a, view, viewGroup);
            View a2 = this.e.a(c2.f1254a, (View) b2.getTag(), viewGroup);
            this.f.b((View) b2.getTag());
            b2.setTag(a2);
            this.f.a(a2);
            this.g = b2;
            b2.forceLayout();
            return b2;
        } else if (c2.b == -3) {
            a a3 = a(view, viewGroup, this.g);
            a3.forceLayout();
            return a3;
        } else if (c2.b == -1) {
            return a(view, viewGroup, this.h);
        } else {
            View view2 = this.e.getView(c2.b, view, viewGroup);
            this.h = view2;
            return view2;
        }
    }

    public int getViewTypeCount() {
        return this.e.getViewTypeCount() + 3;
    }

    public boolean hasStableIds() {
        return this.e.hasStableIds();
    }

    public boolean isEmpty() {
        return this.e.isEmpty();
    }

    public boolean isEnabled(int i2) {
        c c2 = c(i2);
        if (c2.b == -1 || c2.b == -2) {
            return false;
        }
        return this.e.isEnabled(c2.b);
    }

    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
        super.registerDataSetObserver(dataSetObserver);
        this.e.registerDataSetObserver(dataSetObserver);
    }

    public void a(int i2) {
        this.i = i2;
        this.c = false;
    }

    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
        super.unregisterDataSetObserver(dataSetObserver);
        this.e.unregisterDataSetObserver(dataSetObserver);
    }

    private a a(View view, ViewGroup viewGroup, View view2) {
        a aVar = (a) view;
        if (aVar == null) {
            aVar = new a(this.f1250a);
        }
        aVar.setMeasureTarget(view2);
        return aVar;
    }

    private C0098b b(int i2, View view, ViewGroup viewGroup) {
        C0098b bVar = (C0098b) view;
        if (bVar == null) {
            return new C0098b(this.f1250a);
        }
        return bVar;
    }

    private int d(int i2) {
        int a2;
        if (this.i == 0 || (a2 = this.e.a(i2) % this.i) == 0) {
            return 0;
        }
        return this.i - a2;
    }

    /* access modifiers changed from: protected */
    public long b(int i2) {
        return (long) c(i2).f1254a;
    }

    /* access modifiers changed from: protected */
    public View a(int i2, View view, ViewGroup viewGroup) {
        if (this.e.a() == 0) {
            return null;
        }
        return this.e.a(c(i2).f1254a, view, viewGroup);
    }

    /* access modifiers changed from: protected */
    public c c(int i2) {
        int i3 = 0;
        int a2 = this.e.a();
        if (a2 != 0) {
            int i4 = i2;
            int i5 = i2;
            while (i3 < a2) {
                int a3 = this.e.a(i3);
                if (i4 == 0) {
                    return new c(-2, i3);
                }
                int i6 = i4 - this.i;
                if (i6 < 0) {
                    return new c(-3, i3);
                }
                int i7 = i5 - this.i;
                if (i6 < a3) {
                    return new c(i7, i3);
                }
                int d2 = d(i3);
                i5 = i7 - d2;
                int i8 = i6 - (a3 + d2);
                if (i8 < 0) {
                    return new c(-1, i3);
                }
                i3++;
                i4 = i8;
            }
            return new c(-1, i3);
        } else if (i2 >= this.e.getCount()) {
            return new c(-1, 0);
        } else {
            return new c(i2, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b = 0;
        int a2 = this.e.a();
        if (a2 == 0) {
            this.b = this.e.getCount();
            this.c = true;
            return;
        }
        for (int i2 = 0; i2 < a2; i2++) {
            this.b += this.e.a(i2) + this.i;
        }
        this.c = true;
    }

    /* access modifiers changed from: protected */
    public class a extends View {
        private View b;

        public a(Context context) {
            super(context);
        }

        public void setMeasureTarget(View view) {
            this.b = view;
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(this.b.getMeasuredHeight(), C.ENCODING_PCM_32BIT));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: com.tonicartos.widget.stickygridheaders.b$b  reason: collision with other inner class name */
    public class C0098b extends FrameLayout {
        private int b;

        public C0098b(Context context) {
            super(context);
        }

        public int getHeaderId() {
            return this.b;
        }

        public void setHeaderId(int i) {
            this.b = i;
        }

        /* access modifiers changed from: protected */
        @Override // android.widget.FrameLayout, android.widget.FrameLayout
        public FrameLayout.LayoutParams generateDefaultLayoutParams() {
            return new FrameLayout.LayoutParams(-1, -1);
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            View view = (View) getTag();
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = generateDefaultLayoutParams();
                view.setLayoutParams(layoutParams);
            }
            if (view.getVisibility() != 8) {
                view.measure(getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(b.this.f.getWidth(), C.ENCODING_PCM_32BIT), 0, layoutParams.width), getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(0, 0), 0, layoutParams.height));
            }
            setMeasuredDimension(View.MeasureSpec.getSize(i), view.getMeasuredHeight());
        }
    }

    /* access modifiers changed from: protected */
    public class c {

        /* renamed from: a  reason: collision with root package name */
        protected int f1254a;
        protected int b;

        protected c(int i, int i2) {
            this.b = i;
            this.f1254a = i2;
        }
    }
}
