package com.tonicartos.widget.stickygridheaders;

import android.database.DataSetObserver;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class e extends BaseAdapter implements a {

    /* renamed from: a  reason: collision with root package name */
    private d f1257a;
    private b[] b;

    public e(d dVar) {
        this.f1257a = dVar;
        dVar.registerDataSetObserver(new a());
        this.b = a(dVar);
    }

    public int getCount() {
        return this.f1257a.getCount();
    }

    @Override // com.tonicartos.widget.stickygridheaders.a
    public int a(int i) {
        return this.b[i].a();
    }

    @Override // com.tonicartos.widget.stickygridheaders.a
    public View a(int i, View view, ViewGroup viewGroup) {
        return this.f1257a.a(this.b[i].b(), view, viewGroup);
    }

    public Object getItem(int i) {
        return this.f1257a.getItem(i);
    }

    public long getItemId(int i) {
        return this.f1257a.getItemId(i);
    }

    public int getItemViewType(int i) {
        return this.f1257a.getItemViewType(i);
    }

    @Override // com.tonicartos.widget.stickygridheaders.a
    public int a() {
        return this.b.length;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return this.f1257a.getView(i, view, viewGroup);
    }

    public int getViewTypeCount() {
        return this.f1257a.getViewTypeCount();
    }

    public boolean hasStableIds() {
        return this.f1257a.hasStableIds();
    }

    /* access modifiers changed from: protected */
    public b[] a(d dVar) {
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < dVar.getCount(); i++) {
            long a2 = dVar.a(i);
            b bVar = (b) hashMap.get(Long.valueOf(a2));
            if (bVar == null) {
                bVar = new b(i);
                arrayList.add(bVar);
            }
            bVar.c();
            hashMap.put(Long.valueOf(a2), bVar);
        }
        return (b[]) arrayList.toArray(new b[arrayList.size()]);
    }

    private final class a extends DataSetObserver {
        private a() {
        }

        public void onChanged() {
            Log.e(getClass().getSimpleName(), "onChange");
            e.this.b = e.this.a(e.this.f1257a);
            e.this.notifyDataSetChanged();
        }

        public void onInvalidated() {
            e.this.b = e.this.a(e.this.f1257a);
            e.this.notifyDataSetInvalidated();
        }
    }

    /* access modifiers changed from: private */
    public class b {
        private int b = 0;
        private int c;

        public b(int i) {
            this.c = i;
        }

        public int a() {
            return this.b;
        }

        public int b() {
            return this.c;
        }

        public void c() {
            this.b++;
        }
    }
}
