package com.tonicartos.widget.stickygridheaders;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.google.android.exoplayer.C;
import com.tonicartos.widget.stickygridheaders.b;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class StickyGridHeadersGridView extends GridView implements AbsListView.OnScrollListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    static final String f1240a = StickyGridHeadersGridView.class.getSimpleName();
    private static final String i = ("Error supporting platform " + Build.VERSION.SDK_INT + ".");
    private AdapterView.OnItemClickListener A;
    private AdapterView.OnItemLongClickListener B;
    private AdapterView.OnItemSelectedListener C;
    private e D;
    private AbsListView.OnScrollListener E;
    private int F;
    private View G;
    private Runnable H;
    private int I;
    private int J;
    public a b;
    public b c;
    protected b d;
    protected boolean e;
    protected int f;
    protected int g;
    boolean h;
    private boolean j;
    private final Rect k;
    private boolean l;
    private boolean m;
    private int n;
    private long o;
    private DataSetObserver p;
    private int q;
    private boolean r;
    private int s;
    private boolean t;
    private float u;
    private int v;
    private boolean w;
    private int x;
    private c y;
    private d z;

    public interface c {
        void a(AdapterView<?> adapterView, View view, long j);
    }

    public interface d {
        boolean a(AdapterView<?> adapterView, View view, long j);
    }

    private static MotionEvent.PointerCoords[] a(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        MotionEvent.PointerCoords[] pointerCoordsArr = new MotionEvent.PointerCoords[pointerCount];
        for (int i2 = 0; i2 < pointerCount; i2++) {
            pointerCoordsArr[i2] = new MotionEvent.PointerCoords();
            motionEvent.getPointerCoords(i2, pointerCoordsArr[i2]);
        }
        return pointerCoordsArr;
    }

    private static int[] b(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        int[] iArr = new int[pointerCount];
        for (int i2 = 0; i2 < pointerCount; i2++) {
            iArr[i2] = motionEvent.getPointerId(i2);
        }
        return iArr;
    }

    public StickyGridHeadersGridView(Context context) {
        this(context, null);
    }

    public StickyGridHeadersGridView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842865);
    }

    public StickyGridHeadersGridView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.j = true;
        this.k = new Rect();
        this.o = -1;
        this.p = new DataSetObserver() {
            /* class com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView.AnonymousClass1 */

            public void onChanged() {
                StickyGridHeadersGridView.this.c();
            }

            public void onInvalidated() {
                StickyGridHeadersGridView.this.c();
            }
        };
        this.t = true;
        this.x = 1;
        this.F = 0;
        this.h = false;
        super.setOnScrollListener(this);
        setVerticalFadingEdgeEnabled(false);
        if (!this.w) {
            this.v = -1;
        }
        this.I = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public View a(int i2) {
        if (i2 == -2) {
            return this.G;
        }
        try {
            return (View) getChildAt(i2).getTag();
        } catch (Exception e2) {
            return null;
        }
    }

    public View getStickiedHeader() {
        return this.G;
    }

    public boolean getStickyHeaderIsTranscluent() {
        return !this.t;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.A.onItemClick(adapterView, view, this.d.c(i2).b, j2);
    }

    @Override // android.widget.AdapterView.OnItemLongClickListener
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        return this.B.onItemLongClick(adapterView, view, this.d.c(i2).b, j2);
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.C.onItemSelected(adapterView, view, this.d.c(i2).b, j2);
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onNothingSelected(AdapterView<?> adapterView) {
        this.C.onNothingSelected(adapterView);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.j = savedState.f1244a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1244a = this.j;
        return savedState;
    }

    public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        if (this.E != null) {
            this.E.onScroll(absListView, i2, i3, i4);
        }
        if (Build.VERSION.SDK_INT >= 8) {
            c(i2);
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i2) {
        if (this.E != null) {
            this.E.onScrollStateChanged(absListView, i2);
        }
        this.F = i2;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        final View childAt;
        int action = motionEvent.getAction();
        boolean z2 = this.h;
        if (this.h) {
            View a2 = a(this.f);
            if (this.f == -2) {
                childAt = a2;
            } else {
                childAt = getChildAt(this.f);
            }
            if (action == 1 || action == 3) {
                this.h = false;
            }
            if (a2 != null) {
                a2.dispatchTouchEvent(a(motionEvent, this.f));
                a2.invalidate();
                a2.postDelayed(new Runnable() {
                    /* class com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView.AnonymousClass2 */

                    public void run() {
                        StickyGridHeadersGridView.this.invalidate(0, childAt.getTop(), StickyGridHeadersGridView.this.getWidth(), childAt.getTop() + childAt.getHeight());
                    }
                }, (long) ViewConfiguration.getPressedStateDuration());
                invalidate(0, childAt.getTop(), getWidth(), childAt.getHeight() + childAt.getTop());
            }
        }
        switch (action & 255) {
            case 0:
                if (this.c == null) {
                    this.c = new b();
                }
                postDelayed(this.c, (long) ViewConfiguration.getTapTimeout());
                int y2 = (int) motionEvent.getY();
                this.u = (float) y2;
                this.f = a((float) y2);
                if (!(this.f == -1 || this.F == 2)) {
                    View a3 = a(this.f);
                    if (a3 != null) {
                        if (a3.dispatchTouchEvent(a(motionEvent, this.f))) {
                            this.h = true;
                            a3.setPressed(true);
                        }
                        a3.invalidate();
                        if (this.f != -2) {
                            a3 = getChildAt(this.f);
                        }
                        invalidate(0, a3.getTop(), getWidth(), a3.getHeight() + a3.getTop());
                    }
                    this.g = 0;
                    return true;
                }
            case 1:
                if (this.g == -2) {
                    this.g = -1;
                    return true;
                } else if (!(this.g == -1 || this.f == -1)) {
                    final View a4 = a(this.f);
                    if (!z2 && a4 != null) {
                        if (this.g != 0) {
                            a4.setPressed(false);
                        }
                        if (this.D == null) {
                            this.D = new e();
                        }
                        final e eVar = this.D;
                        eVar.f1247a = this.f;
                        eVar.a();
                        if (this.g == 0 || this.g == 1) {
                            Handler handler = getHandler();
                            if (handler != null) {
                                handler.removeCallbacks(this.g == 0 ? this.c : this.b);
                            }
                            if (!this.e) {
                                this.g = 1;
                                a4.setPressed(true);
                                setPressed(true);
                                if (this.H != null) {
                                    removeCallbacks(this.H);
                                }
                                this.H = new Runnable() {
                                    /* class com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView.AnonymousClass3 */

                                    public void run() {
                                        StickyGridHeadersGridView.this.f = -1;
                                        StickyGridHeadersGridView.this.H = null;
                                        StickyGridHeadersGridView.this.g = -1;
                                        a4.setPressed(false);
                                        StickyGridHeadersGridView.this.setPressed(false);
                                        a4.invalidate();
                                        StickyGridHeadersGridView.this.invalidate(0, a4.getTop(), StickyGridHeadersGridView.this.getWidth(), a4.getHeight());
                                        if (!StickyGridHeadersGridView.this.e) {
                                            eVar.run();
                                        }
                                    }
                                };
                                postDelayed(this.H, (long) ViewConfiguration.getPressedStateDuration());
                            } else {
                                this.g = -1;
                            }
                        } else if (!this.e) {
                            eVar.run();
                        }
                    }
                    this.g = -1;
                    return true;
                }
                break;
            case 2:
                if (this.f != -1 && Math.abs(motionEvent.getY() - this.u) > ((float) this.I)) {
                    this.g = -1;
                    View a5 = a(this.f);
                    if (a5 != null) {
                        a5.setPressed(false);
                        a5.invalidate();
                    }
                    Handler handler2 = getHandler();
                    if (handler2 != null) {
                        handler2.removeCallbacks(this.b);
                    }
                    this.f = -1;
                    break;
                }
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean a(View view, long j2) {
        if (this.y == null) {
            return false;
        }
        playSoundEffect(0);
        if (view != null) {
            view.sendAccessibilityEvent(1);
        }
        this.y.a(this, view, j2);
        return true;
    }

    public boolean b(View view, long j2) {
        boolean z2;
        if (this.z != null) {
            z2 = this.z.a(this, view, j2);
        } else {
            z2 = false;
        }
        if (z2) {
            if (view != null) {
                view.sendAccessibilityEvent(2);
            }
            performHapticFeedback(0);
        }
        return z2;
    }

    @Override // android.widget.GridView, android.widget.AbsListView
    public void setAdapter(ListAdapter listAdapter) {
        a cVar;
        if (!(this.d == null || this.p == null)) {
            this.d.unregisterDataSetObserver(this.p);
        }
        if (!this.m) {
            this.l = true;
        }
        if (listAdapter instanceof a) {
            cVar = (a) listAdapter;
        } else if (listAdapter instanceof d) {
            cVar = new e((d) listAdapter);
        } else {
            cVar = new c(listAdapter);
        }
        this.d = new b(getContext(), this, cVar);
        this.d.registerDataSetObserver(this.p);
        c();
        super.setAdapter((ListAdapter) this.d);
    }

    public void setAreHeadersSticky(boolean z2) {
        if (z2 != this.j) {
            this.j = z2;
            requestLayout();
        }
    }

    public void setClipToPadding(boolean z2) {
        super.setClipToPadding(z2);
        this.l = z2;
        this.m = true;
    }

    public void setColumnWidth(int i2) {
        super.setColumnWidth(i2);
        this.n = i2;
    }

    public void setHeadersIgnorePadding(boolean z2) {
        this.r = z2;
    }

    public void setHorizontalSpacing(int i2) {
        super.setHorizontalSpacing(i2);
        this.s = i2;
    }

    public void setNumColumns(int i2) {
        super.setNumColumns(i2);
        this.w = true;
        this.v = i2;
        if (i2 != -1 && this.d != null) {
            this.d.a(i2);
        }
    }

    public void setOnHeaderClickListener(c cVar) {
        this.y = cVar;
    }

    public void setOnHeaderLongClickListener(d dVar) {
        if (!isLongClickable()) {
            setLongClickable(true);
        }
        this.z = dVar;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.A = onItemClickListener;
        super.setOnItemClickListener(this);
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener onItemLongClickListener) {
        this.B = onItemLongClickListener;
        super.setOnItemLongClickListener(this);
    }

    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
        this.C = onItemSelectedListener;
        super.setOnItemSelectedListener(this);
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.E = onScrollListener;
    }

    public void setStickyHeaderIsTranscluent(boolean z2) {
        this.t = !z2;
    }

    public void setVerticalSpacing(int i2) {
        super.setVerticalSpacing(i2);
        this.J = i2;
    }

    private int a(float f2) {
        if (this.G != null && f2 <= ((float) this.q)) {
            return -2;
        }
        int i2 = 0;
        int firstVisiblePosition = getFirstVisiblePosition();
        while (firstVisiblePosition <= getLastVisiblePosition()) {
            if (getItemIdAtPosition(firstVisiblePosition) == -1) {
                View childAt = getChildAt(i2);
                int bottom = childAt.getBottom();
                int top = childAt.getTop();
                if (f2 <= ((float) bottom) && f2 >= ((float) top)) {
                    return i2;
                }
            }
            firstVisiblePosition += this.x;
            i2 += this.x;
        }
        return -1;
    }

    private int getHeaderHeight() {
        if (this.G != null) {
            return this.G.getMeasuredHeight();
        }
        return 0;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private long b(int i2) {
        if (i2 == -2) {
            return this.o;
        }
        return this.d.b(getFirstVisiblePosition() + i2);
    }

    private void b() {
        int makeMeasureSpec;
        int makeMeasureSpec2;
        if (this.G != null) {
            if (this.r) {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getWidth(), C.ENCODING_PCM_32BIT);
            } else {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec((getWidth() - getPaddingLeft()) - getPaddingRight(), C.ENCODING_PCM_32BIT);
            }
            ViewGroup.LayoutParams layoutParams = this.G.getLayoutParams();
            if (layoutParams == null || layoutParams.height <= 0) {
                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
            } else {
                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(layoutParams.height, C.ENCODING_PCM_32BIT);
            }
            this.G.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            this.G.measure(makeMeasureSpec, makeMeasureSpec2);
            if (this.r) {
                this.G.layout(getLeft(), 0, getRight(), this.G.getMeasuredHeight());
            } else {
                this.G.layout(getLeft() + getPaddingLeft(), 0, getRight() - getPaddingRight(), this.G.getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void c() {
        this.q = 0;
        c((View) null);
        this.o = Long.MIN_VALUE;
    }

    private void c(int i2) {
        long b2;
        int top;
        if (this.d != null && this.d.getCount() != 0 && this.j && getChildAt(0) != null) {
            int i3 = i2 - this.x;
            if (i3 < 0) {
                i3 = i2;
            }
            int i4 = this.x + i2;
            if (i4 >= this.d.getCount()) {
                i4 = i2;
            }
            if (this.J == 0) {
                b2 = this.d.b(i2);
                i4 = i2;
            } else if (this.J < 0) {
                this.d.b(i2);
                if (getChildAt(this.x).getTop() <= 0) {
                    b2 = this.d.b(i4);
                } else {
                    b2 = this.d.b(i2);
                    i4 = i2;
                }
            } else {
                int top2 = getChildAt(0).getTop();
                if (top2 <= 0 || top2 >= this.J) {
                    b2 = this.d.b(i2);
                    i4 = i2;
                } else {
                    b2 = this.d.b(i3);
                    i4 = i3;
                }
            }
            if (this.o != b2) {
                c(this.d.a(i4, this.G, this));
                b();
                this.o = b2;
            }
            int childCount = getChildCount();
            if (childCount != 0) {
                View view = null;
                int i5 = 99999;
                int i6 = 0;
                while (i6 < childCount) {
                    View childAt = super.getChildAt(i6);
                    if (this.l) {
                        top = childAt.getTop() - getPaddingTop();
                    } else {
                        top = childAt.getTop();
                    }
                    if (top >= 0 && this.d.getItemId(getPositionForView(childAt)) == -1 && top < i5) {
                        i5 = top;
                        view = childAt;
                    }
                    i6 = this.x + i6;
                }
                int headerHeight = getHeaderHeight();
                if (view == null) {
                    this.q = headerHeight;
                    if (this.l) {
                        this.q += getPaddingTop();
                    }
                } else if (i2 == 0 && super.getChildAt(0).getTop() > 0 && !this.l) {
                    this.q = 0;
                } else if (this.l) {
                    this.q = Math.min(view.getTop(), getPaddingTop() + headerHeight);
                    this.q = this.q < getPaddingTop() ? headerHeight + getPaddingTop() : this.q;
                } else {
                    this.q = Math.min(view.getTop(), headerHeight);
                    if (this.q >= 0) {
                        headerHeight = this.q;
                    }
                    this.q = headerHeight;
                }
            }
        }
    }

    private void c(View view) {
        b(this.G);
        a(view);
        this.G = view;
    }

    private MotionEvent a(MotionEvent motionEvent, int i2) {
        if (i2 == -2) {
            return motionEvent;
        }
        long downTime = motionEvent.getDownTime();
        long eventTime = motionEvent.getEventTime();
        int action = motionEvent.getAction();
        int pointerCount = motionEvent.getPointerCount();
        int[] b2 = b(motionEvent);
        MotionEvent.PointerCoords[] a2 = a(motionEvent);
        int metaState = motionEvent.getMetaState();
        float xPrecision = motionEvent.getXPrecision();
        float yPrecision = motionEvent.getYPrecision();
        int deviceId = motionEvent.getDeviceId();
        int edgeFlags = motionEvent.getEdgeFlags();
        int source = motionEvent.getSource();
        int flags = motionEvent.getFlags();
        View childAt = getChildAt(i2);
        for (int i3 = 0; i3 < pointerCount; i3++) {
            a2[i3].y -= (float) childAt.getTop();
        }
        return MotionEvent.obtain(downTime, eventTime, action, pointerCount, b2, a2, metaState, xPrecision, yPrecision, deviceId, edgeFlags, source, flags);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        int width;
        int makeMeasureSpec;
        int makeMeasureSpec2;
        if (Build.VERSION.SDK_INT < 8) {
            c(getFirstVisiblePosition());
        }
        boolean z2 = this.G != null && this.j && this.G.getVisibility() == 0;
        int headerHeight = getHeaderHeight();
        int i2 = this.q - headerHeight;
        if (z2 && this.t) {
            if (this.r) {
                this.k.left = 0;
                this.k.right = getWidth();
            } else {
                this.k.left = getPaddingLeft();
                this.k.right = getWidth() - getPaddingRight();
            }
            this.k.top = this.q;
            this.k.bottom = getHeight();
            canvas.save();
            canvas.clipRect(this.k);
        }
        super.dispatchDraw(canvas);
        ArrayList arrayList = new ArrayList();
        int i3 = 0;
        int firstVisiblePosition = getFirstVisiblePosition();
        while (firstVisiblePosition <= getLastVisiblePosition()) {
            if (getItemIdAtPosition(firstVisiblePosition) == -1) {
                arrayList.add(Integer.valueOf(i3));
            }
            firstVisiblePosition += this.x;
            i3 += this.x;
        }
        for (int i4 = 0; i4 < arrayList.size(); i4++) {
            View childAt = getChildAt(((Integer) arrayList.get(i4)).intValue());
            try {
                View view = (View) childAt.getTag();
                boolean z3 = ((long) ((b.C0098b) childAt).getHeaderId()) == this.o && childAt.getTop() < 0 && this.j;
                if (view.getVisibility() == 0 && !z3) {
                    if (this.r) {
                        makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(getWidth(), C.ENCODING_PCM_32BIT);
                    } else {
                        makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec((getWidth() - getPaddingLeft()) - getPaddingRight(), C.ENCODING_PCM_32BIT);
                    }
                    int makeMeasureSpec3 = View.MeasureSpec.makeMeasureSpec(0, 0);
                    view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                    view.measure(makeMeasureSpec2, makeMeasureSpec3);
                    if (this.r) {
                        view.layout(getLeft(), 0, getRight(), childAt.getHeight());
                    } else {
                        view.layout(getLeft() + getPaddingLeft(), 0, getRight() - getPaddingRight(), childAt.getHeight());
                    }
                    if (this.r) {
                        this.k.left = 0;
                        this.k.right = getWidth();
                    } else {
                        this.k.left = getPaddingLeft();
                        this.k.right = getWidth() - getPaddingRight();
                    }
                    this.k.bottom = childAt.getBottom();
                    this.k.top = childAt.getTop();
                    canvas.save();
                    canvas.clipRect(this.k);
                    if (this.r) {
                        canvas.translate(0.0f, (float) childAt.getTop());
                    } else {
                        canvas.translate((float) getPaddingLeft(), (float) childAt.getTop());
                    }
                    view.draw(canvas);
                    canvas.restore();
                }
            } catch (Exception e2) {
                return;
            }
        }
        if (z2 && this.t) {
            canvas.restore();
        } else if (!z2) {
            return;
        }
        if (this.r) {
            width = getWidth();
        } else {
            width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        }
        if (this.G.getWidth() != width) {
            if (this.r) {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getWidth(), C.ENCODING_PCM_32BIT);
            } else {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec((getWidth() - getPaddingLeft()) - getPaddingRight(), C.ENCODING_PCM_32BIT);
            }
            int makeMeasureSpec4 = View.MeasureSpec.makeMeasureSpec(0, 0);
            this.G.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            this.G.measure(makeMeasureSpec, makeMeasureSpec4);
            if (this.r) {
                this.G.layout(getLeft(), 0, getRight(), this.G.getHeight());
            } else {
                this.G.layout(getLeft() + getPaddingLeft(), 0, getRight() - getPaddingRight(), this.G.getHeight());
            }
        }
        if (this.r) {
            this.k.left = 0;
            this.k.right = getWidth();
        } else {
            this.k.left = getPaddingLeft();
            this.k.right = getWidth() - getPaddingRight();
        }
        this.k.bottom = i2 + headerHeight;
        if (this.l) {
            this.k.top = getPaddingTop();
        } else {
            this.k.top = 0;
        }
        canvas.save();
        canvas.clipRect(this.k);
        if (this.r) {
            canvas.translate(0.0f, (float) i2);
        } else {
            canvas.translate((float) getPaddingLeft(), (float) i2);
        }
        if (this.q != headerHeight) {
            canvas.saveLayerAlpha(0.0f, 0.0f, (float) canvas.getWidth(), (float) canvas.getHeight(), (this.q * 255) / headerHeight, 31);
        }
        this.G.draw(canvas);
        if (this.q != headerHeight) {
            canvas.restore();
        }
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4 = 1;
        if (this.v == -1) {
            if (this.n > 0) {
                int max = Math.max((View.MeasureSpec.getSize(i2) - getPaddingLeft()) - getPaddingRight(), 0);
                int i5 = max / this.n;
                if (i5 > 0) {
                    while (i5 != 1 && (this.n * i5) + ((i5 - 1) * this.s) > max) {
                        i5--;
                    }
                    i4 = i5;
                }
            } else {
                i4 = 2;
            }
            this.x = i4;
        } else {
            this.x = this.v;
        }
        if (this.d != null) {
            this.d.a(this.x);
        }
        b();
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        if (view != null) {
            try {
                Field declaredField = View.class.getDeclaredField("mAttachInfo");
                declaredField.setAccessible(true);
                Method declaredMethod = View.class.getDeclaredMethod("dispatchAttachedToWindow", Class.forName("android.view.View$AttachInfo"), Integer.TYPE);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(view, declaredField.get(this), 8);
            } catch (NoSuchMethodException e2) {
                throw new f(e2);
            } catch (ClassNotFoundException e3) {
                throw new f(e3);
            } catch (IllegalArgumentException e4) {
                throw new f(e4);
            } catch (IllegalAccessException e5) {
                throw new f(e5);
            } catch (InvocationTargetException e6) {
                throw new f(e6);
            } catch (NoSuchFieldException e7) {
                throw new f(e7);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view) {
        if (view != null) {
            try {
                Method declaredMethod = View.class.getDeclaredMethod("dispatchDetachedFromWindow", new Class[0]);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(view, new Object[0]);
            } catch (NoSuchMethodException e2) {
                throw new f(e2);
            } catch (IllegalArgumentException e3) {
                throw new f(e3);
            } catch (IllegalAccessException e4) {
                throw new f(e4);
            } catch (InvocationTargetException e5) {
                throw new f(e5);
            }
        }
    }

    private class a extends g implements Runnable {
        private a() {
            super();
        }

        public void run() {
            boolean z;
            View a2 = StickyGridHeadersGridView.this.a(StickyGridHeadersGridView.this.f);
            if (a2 != null) {
                long b = StickyGridHeadersGridView.this.b((StickyGridHeadersGridView) StickyGridHeadersGridView.this.f);
                if (!b() || StickyGridHeadersGridView.this.e) {
                    z = false;
                } else {
                    z = StickyGridHeadersGridView.this.b(a2, b);
                }
                if (z) {
                    StickyGridHeadersGridView.this.g = -2;
                    StickyGridHeadersGridView.this.setPressed(false);
                    a2.setPressed(false);
                    return;
                }
                StickyGridHeadersGridView.this.g = 2;
            }
        }
    }

    private class e extends g implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        int f1247a;

        private e() {
            super();
        }

        public void run() {
            View a2;
            if (!StickyGridHeadersGridView.this.e && StickyGridHeadersGridView.this.d != null && StickyGridHeadersGridView.this.d.getCount() > 0 && this.f1247a != -1 && this.f1247a < StickyGridHeadersGridView.this.d.getCount() && b() && (a2 = StickyGridHeadersGridView.this.a(this.f1247a)) != null) {
                StickyGridHeadersGridView.this.a(a2, StickyGridHeadersGridView.this.b((StickyGridHeadersGridView) this.f1247a));
            }
        }
    }

    private class g {

        /* renamed from: a  reason: collision with root package name */
        private int f1249a;

        private g() {
        }

        public void a() {
            this.f1249a = StickyGridHeadersGridView.this.getWindowAttachCount();
        }

        public boolean b() {
            return StickyGridHeadersGridView.this.hasWindowFocus() && StickyGridHeadersGridView.this.getWindowAttachCount() == this.f1249a;
        }
    }

    final class b implements Runnable {
        b() {
        }

        public void run() {
            if (StickyGridHeadersGridView.this.g == 0) {
                StickyGridHeadersGridView.this.g = 1;
                View a2 = StickyGridHeadersGridView.this.a(StickyGridHeadersGridView.this.f);
                if (a2 != null && !StickyGridHeadersGridView.this.h) {
                    if (!StickyGridHeadersGridView.this.e) {
                        a2.setPressed(true);
                        StickyGridHeadersGridView.this.setPressed(true);
                        StickyGridHeadersGridView.this.refreshDrawableState();
                        int longPressTimeout = ViewConfiguration.getLongPressTimeout();
                        if (StickyGridHeadersGridView.this.isLongClickable()) {
                            if (StickyGridHeadersGridView.this.b == null) {
                                StickyGridHeadersGridView.this.b = new a();
                            }
                            StickyGridHeadersGridView.this.b.a();
                            StickyGridHeadersGridView.this.postDelayed(StickyGridHeadersGridView.this.b, (long) longPressTimeout);
                            return;
                        }
                        StickyGridHeadersGridView.this.g = 2;
                        return;
                    }
                    StickyGridHeadersGridView.this.g = 2;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public class f extends RuntimeException {
        private static final long serialVersionUID = -6512098808936536538L;

        public f(Exception exc) {
            super(StickyGridHeadersGridView.i, exc);
        }
    }

    /* access modifiers changed from: package-private */
    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* class com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView.SavedState.AnonymousClass1 */

            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        boolean f1244a;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f1244a = parcel.readByte() != 0;
        }

        public String toString() {
            return "StickyGridHeadersGridView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " areHeadersSticky=" + this.f1244a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeByte((byte) (this.f1244a ? 1 : 0));
        }
    }
}
