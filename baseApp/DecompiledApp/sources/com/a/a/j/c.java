package com.a.a.j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

public class c extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private static final Queue<c> f971a = i.a(0);
    private InputStream b;
    private IOException c;

    public static c a(InputStream inputStream) {
        c poll;
        synchronized (f971a) {
            poll = f971a.poll();
        }
        if (poll == null) {
            poll = new c();
        }
        poll.b(inputStream);
        return poll;
    }

    c() {
    }

    /* access modifiers changed from: package-private */
    public void b(InputStream inputStream) {
        this.b = inputStream;
    }

    @Override // java.io.InputStream
    public int available() {
        return this.b.available();
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
    public void close() {
        this.b.close();
    }

    public void mark(int i) {
        this.b.mark(i);
    }

    public boolean markSupported() {
        return this.b.markSupported();
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        try {
            return this.b.read(bArr);
        } catch (IOException e) {
            this.c = e;
            return -1;
        }
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        try {
            return this.b.read(bArr, i, i2);
        } catch (IOException e) {
            this.c = e;
            return -1;
        }
    }

    @Override // java.io.InputStream
    public synchronized void reset() {
        this.b.reset();
    }

    @Override // java.io.InputStream
    public long skip(long j) {
        try {
            return this.b.skip(j);
        } catch (IOException e) {
            this.c = e;
            return 0;
        }
    }

    @Override // java.io.InputStream
    public int read() {
        try {
            return this.b.read();
        } catch (IOException e) {
            this.c = e;
            return -1;
        }
    }

    public IOException a() {
        return this.c;
    }

    public void b() {
        this.c = null;
        this.b = null;
        synchronized (f971a) {
            f971a.offer(this);
        }
    }
}
