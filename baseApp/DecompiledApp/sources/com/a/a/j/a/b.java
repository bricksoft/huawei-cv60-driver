package com.a.a.j.a;

public abstract class b {
    /* access modifiers changed from: package-private */
    public abstract void a(boolean z);

    public abstract void b();

    public static b a() {
        return new a();
    }

    private b() {
    }

    /* access modifiers changed from: private */
    public static class a extends b {

        /* renamed from: a  reason: collision with root package name */
        private volatile boolean f968a;

        a() {
            super();
        }

        @Override // com.a.a.j.a.b
        public void b() {
            if (this.f968a) {
                throw new IllegalStateException("Already released");
            }
        }

        @Override // com.a.a.j.a.b
        public void a(boolean z) {
            this.f968a = z;
        }
    }
}
