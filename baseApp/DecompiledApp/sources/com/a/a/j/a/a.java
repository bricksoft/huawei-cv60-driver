package com.a.a.j.a;

import android.support.v4.util.Pools;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final d<Object> f965a = new d<Object>() {
        /* class com.a.a.j.a.a.AnonymousClass1 */

        @Override // com.a.a.j.a.a.d
        public void a(Object obj) {
        }
    };

    /* renamed from: com.a.a.j.a.a$a  reason: collision with other inner class name */
    public interface AbstractC0041a<T> {
        T b();
    }

    public interface c {
        b a_();
    }

    public interface d<T> {
        void a(T t);
    }

    public static <T extends c> Pools.Pool<T> a(int i, AbstractC0041a<T> aVar) {
        return a(new Pools.SimplePool(i), aVar);
    }

    public static <T extends c> Pools.Pool<T> b(int i, AbstractC0041a<T> aVar) {
        return a(new Pools.SynchronizedPool(i), aVar);
    }

    public static <T> Pools.Pool<List<T>> a() {
        return a(20);
    }

    public static <T> Pools.Pool<List<T>> a(int i) {
        return a(new Pools.SynchronizedPool(i), new AbstractC0041a<List<T>>() {
            /* class com.a.a.j.a.a.AnonymousClass2 */

            /* renamed from: a */
            public List<T> b() {
                return new ArrayList();
            }
        }, new d<List<T>>() {
            /* class com.a.a.j.a.a.AnonymousClass3 */

            public void a(List<T> list) {
                list.clear();
            }
        });
    }

    private static <T extends c> Pools.Pool<T> a(Pools.Pool<T> pool, AbstractC0041a<T> aVar) {
        return a(pool, aVar, b());
    }

    private static <T> Pools.Pool<T> a(Pools.Pool<T> pool, AbstractC0041a<T> aVar, d<T> dVar) {
        return new b(pool, aVar, dVar);
    }

    private static <T> d<T> b() {
        return (d<T>) f965a;
    }

    /* access modifiers changed from: private */
    public static final class b<T> implements Pools.Pool<T> {

        /* renamed from: a  reason: collision with root package name */
        private final AbstractC0041a<T> f966a;
        private final d<T> b;
        private final Pools.Pool<T> c;

        b(Pools.Pool<T> pool, AbstractC0041a<T> aVar, d<T> dVar) {
            this.c = pool;
            this.f966a = aVar;
            this.b = dVar;
        }

        @Override // android.support.v4.util.Pools.Pool
        public T acquire() {
            T acquire = this.c.acquire();
            if (acquire == null) {
                acquire = this.f966a.b();
                if (Log.isLoggable("FactoryPools", 2)) {
                    Log.v("FactoryPools", "Created new " + acquire.getClass());
                }
            }
            if (acquire instanceof c) {
                acquire.a_().a(false);
            }
            return acquire;
        }

        @Override // android.support.v4.util.Pools.Pool
        public boolean release(T t) {
            if (t instanceof c) {
                t.a_().a(true);
            }
            this.b.a(t);
            return this.c.release(t);
        }
    }
}
