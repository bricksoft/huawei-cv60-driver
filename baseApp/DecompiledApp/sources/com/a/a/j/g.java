package com.a.a.j;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private Class<?> f975a;
    private Class<?> b;
    private Class<?> c;

    public g() {
    }

    public g(Class<?> cls, Class<?> cls2) {
        a(cls, cls2);
    }

    public g(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        a(cls, cls2, cls3);
    }

    public void a(Class<?> cls, Class<?> cls2) {
        a(cls, cls2, null);
    }

    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        this.f975a = cls;
        this.b = cls2;
        this.c = cls3;
    }

    public String toString() {
        return "MultiClassKey{first=" + this.f975a + ", second=" + this.b + '}';
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        if (!this.f975a.equals(gVar.f975a)) {
            return false;
        }
        if (!this.b.equals(gVar.b)) {
            return false;
        }
        return i.a(this.c, gVar.c);
    }

    public int hashCode() {
        return (this.c != null ? this.c.hashCode() : 0) + (((this.f975a.hashCode() * 31) + this.b.hashCode()) * 31);
    }
}
