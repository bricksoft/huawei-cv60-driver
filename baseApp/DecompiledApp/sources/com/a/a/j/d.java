package com.a.a.j;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.SystemClock;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final double f972a;

    static {
        double d = 1.0d;
        if (17 <= Build.VERSION.SDK_INT) {
            d = 1.0d / Math.pow(10.0d, 6.0d);
        }
        f972a = d;
    }

    @TargetApi(17)
    public static long a() {
        if (17 <= Build.VERSION.SDK_INT) {
            return SystemClock.elapsedRealtimeNanos();
        }
        return SystemClock.uptimeMillis();
    }

    public static double a(long j) {
        return ((double) (a() - j)) * f972a;
    }
}
