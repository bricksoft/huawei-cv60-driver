package com.a.a.j;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final AtomicReference<byte[]> f963a = new AtomicReference<>();

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001f A[SYNTHETIC, Splitter:B:12:0x001f] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001a A[SYNTHETIC, Splitter:B:9:0x001a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.nio.ByteBuffer a(java.io.File r7) {
        /*
            r3 = 0
            long r4 = r7.length()     // Catch:{ all -> 0x0014 }
            r0 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0023
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0014 }
            java.lang.String r1 = "File too large to map into memory"
            r0.<init>(r1)     // Catch:{ all -> 0x0014 }
            throw r0     // Catch:{ all -> 0x0014 }
        L_0x0014:
            r0 = move-exception
            r1 = r0
            r2 = r3
            r4 = r3
        L_0x0018:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x0049 }
        L_0x001d:
            if (r4 == 0) goto L_0x0022
            r4.close()     // Catch:{ IOException -> 0x004b }
        L_0x0022:
            throw r1
        L_0x0023:
            java.io.RandomAccessFile r6 = new java.io.RandomAccessFile
            java.lang.String r0 = "r"
            r6.<init>(r7, r0)
            java.nio.channels.FileChannel r0 = r6.getChannel()     // Catch:{ all -> 0x004d }
            java.nio.channels.FileChannel$MapMode r1 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ all -> 0x0052 }
            r2 = 0
            java.nio.MappedByteBuffer r1 = r0.map(r1, r2, r4)     // Catch:{ all -> 0x0052 }
            java.nio.MappedByteBuffer r1 = r1.load()     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x003f
            r0.close()     // Catch:{ IOException -> 0x0045 }
        L_0x003f:
            if (r6 == 0) goto L_0x0044
            r6.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0044:
            return r1
        L_0x0045:
            r0 = move-exception
            goto L_0x003f
        L_0x0047:
            r0 = move-exception
            goto L_0x0044
        L_0x0049:
            r0 = move-exception
            goto L_0x001d
        L_0x004b:
            r0 = move-exception
            goto L_0x0022
        L_0x004d:
            r0 = move-exception
            r1 = r0
            r2 = r3
            r4 = r6
            goto L_0x0018
        L_0x0052:
            r1 = move-exception
            r2 = r0
            r4 = r6
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.j.a.a(java.io.File):java.nio.ByteBuffer");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002d A[SYNTHETIC, Splitter:B:16:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0032 A[SYNTHETIC, Splitter:B:19:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.nio.ByteBuffer r4, java.io.File r5) {
        /*
            r2 = 0
            r0 = 0
            r4.position(r0)
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0028 }
            java.lang.String r0 = "rw"
            r3.<init>(r5, r0)     // Catch:{ all -> 0x0028 }
            java.nio.channels.FileChannel r1 = r3.getChannel()     // Catch:{ all -> 0x003e }
            r1.write(r4)     // Catch:{ all -> 0x0041 }
            r0 = 0
            r1.force(r0)     // Catch:{ all -> 0x0041 }
            r1.close()     // Catch:{ all -> 0x0041 }
            r3.close()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ IOException -> 0x0036 }
        L_0x0022:
            if (r3 == 0) goto L_0x0027
            r3.close()     // Catch:{ IOException -> 0x0038 }
        L_0x0027:
            return
        L_0x0028:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x002b:
            if (r1 == 0) goto L_0x0030
            r1.close()     // Catch:{ IOException -> 0x003a }
        L_0x0030:
            if (r3 == 0) goto L_0x0035
            r3.close()     // Catch:{ IOException -> 0x003c }
        L_0x0035:
            throw r0
        L_0x0036:
            r0 = move-exception
            goto L_0x0022
        L_0x0038:
            r0 = move-exception
            goto L_0x0027
        L_0x003a:
            r1 = move-exception
            goto L_0x0030
        L_0x003c:
            r1 = move-exception
            goto L_0x0035
        L_0x003e:
            r0 = move-exception
            r1 = r2
            goto L_0x002b
        L_0x0041:
            r0 = move-exception
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.j.a.a(java.nio.ByteBuffer, java.io.File):void");
    }

    public static byte[] a(ByteBuffer byteBuffer) {
        b c = c(byteBuffer);
        if (c != null && c.f967a == 0 && c.b == c.c.length) {
            return byteBuffer.array();
        }
        ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
        byte[] bArr = new byte[asReadOnlyBuffer.limit()];
        asReadOnlyBuffer.position(0);
        asReadOnlyBuffer.get(bArr);
        return bArr;
    }

    public static InputStream b(ByteBuffer byteBuffer) {
        return new C0040a(byteBuffer);
    }

    private static b c(ByteBuffer byteBuffer) {
        if (byteBuffer.isReadOnly() || !byteBuffer.hasArray()) {
            return null;
        }
        return new b(byteBuffer.array(), byteBuffer.arrayOffset(), byteBuffer.limit());
    }

    /* access modifiers changed from: package-private */
    public static final class b {

        /* renamed from: a  reason: collision with root package name */
        final int f967a;
        final int b;
        final byte[] c;

        public b(byte[] bArr, int i, int i2) {
            this.c = bArr;
            this.f967a = i;
            this.b = i2;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: com.a.a.j.a$a  reason: collision with other inner class name */
    public static class C0040a extends InputStream {

        /* renamed from: a  reason: collision with root package name */
        private final ByteBuffer f964a;
        private int b = -1;

        public C0040a(ByteBuffer byteBuffer) {
            this.f964a = byteBuffer;
        }

        @Override // java.io.InputStream
        public int available() {
            return this.f964a.remaining();
        }

        @Override // java.io.InputStream
        public int read() {
            if (!this.f964a.hasRemaining()) {
                return -1;
            }
            return this.f964a.get();
        }

        public synchronized void mark(int i) {
            this.b = this.f964a.position();
        }

        public boolean markSupported() {
            return true;
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            if (!this.f964a.hasRemaining()) {
                return -1;
            }
            int min = Math.min(i2, available());
            this.f964a.get(bArr, i, min);
            return min;
        }

        @Override // java.io.InputStream
        public synchronized void reset() {
            if (this.b == -1) {
                throw new IOException("Cannot reset to unset mark position");
            }
            this.f964a.position(this.b);
        }

        @Override // java.io.InputStream
        public long skip(long j) {
            if (!this.f964a.hasRemaining()) {
                return -1;
            }
            long min = Math.min(j, (long) available());
            this.f964a.position((int) (((long) this.f964a.position()) + min));
            return min;
        }
    }
}
