package com.a.a.j;

import java.io.FilterInputStream;
import java.io.InputStream;

public class f extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private int f974a = Integer.MIN_VALUE;

    public f(InputStream inputStream) {
        super(inputStream);
    }

    public void mark(int i) {
        super.mark(i);
        this.f974a = i;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() {
        if (a(1) == -1) {
            return -1;
        }
        int read = super.read();
        b(1);
        return read;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        int a2 = (int) a((long) i2);
        if (a2 == -1) {
            return -1;
        }
        int read = super.read(bArr, i, a2);
        b((long) read);
        return read;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public void reset() {
        super.reset();
        this.f974a = Integer.MIN_VALUE;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) {
        long a2 = a(j);
        if (a2 == -1) {
            return -1;
        }
        long skip = super.skip(a2);
        b(skip);
        return skip;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int available() {
        if (this.f974a == Integer.MIN_VALUE) {
            return super.available();
        }
        return Math.min(this.f974a, super.available());
    }

    private long a(long j) {
        if (this.f974a == 0) {
            return -1;
        }
        if (this.f974a == Integer.MIN_VALUE || j <= ((long) this.f974a)) {
            return j;
        }
        return (long) this.f974a;
    }

    private void b(long j) {
        if (this.f974a != Integer.MIN_VALUE && j != -1) {
            this.f974a = (int) (((long) this.f974a) - j);
        }
    }
}
