package com.a.a.a;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class a implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    final ThreadPoolExecutor f736a = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new ThreadFactoryC0026a());
    private final File b;
    private final File c;
    private final File d;
    private final File e;
    private final int f;
    private long g;
    private final int h;
    private long i = 0;
    private Writer j;
    private final LinkedHashMap<String, c> k = new LinkedHashMap<>(0, 0.75f, true);
    private int l;
    private long m = 0;
    private final Callable<Void> n = new Callable<Void>() {
        /* class com.a.a.a.a.AnonymousClass1 */

        /* renamed from: a */
        public Void call() {
            synchronized (a.this) {
                if (a.this.j != null) {
                    a.this.g();
                    if (a.this.e()) {
                        a.this.d();
                        a.this.l = 0;
                    }
                }
            }
            return null;
        }
    };

    private a(File file, int i2, int i3, long j2) {
        this.b = file;
        this.f = i2;
        this.c = new File(file, "journal");
        this.d = new File(file, "journal.tmp");
        this.e = new File(file, "journal.bkp");
        this.h = i3;
        this.g = j2;
    }

    public static a a(File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 <= 0) {
            throw new IllegalArgumentException("valueCount <= 0");
        } else {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    a(file2, file3, false);
                }
            }
            a aVar = new a(file, i2, i3, j2);
            if (aVar.c.exists()) {
                try {
                    aVar.b();
                    aVar.c();
                    return aVar;
                } catch (IOException e2) {
                    System.out.println("DiskLruCache " + file + " is corrupt: " + e2.getMessage() + ", removing");
                    aVar.a();
                }
            }
            file.mkdirs();
            a aVar2 = new a(file, i2, i3, j2);
            aVar2.d();
            return aVar2;
        }
    }

    private void b() {
        b bVar = new b(new FileInputStream(this.c), c.f743a);
        try {
            String a2 = bVar.a();
            String a3 = bVar.a();
            String a4 = bVar.a();
            String a5 = bVar.a();
            String a6 = bVar.a();
            if (!"libcore.io.DiskLruCache".equals(a2) || !"1".equals(a3) || !Integer.toString(this.f).equals(a4) || !Integer.toString(this.h).equals(a5) || !"".equals(a6)) {
                throw new IOException("unexpected journal header: [" + a2 + ", " + a3 + ", " + a5 + ", " + a6 + "]");
            }
            int i2 = 0;
            while (true) {
                try {
                    d(bVar.a());
                    i2++;
                } catch (EOFException e2) {
                    this.l = i2 - this.k.size();
                    if (bVar.b()) {
                        d();
                    } else {
                        this.j = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.c, true), c.f743a));
                    }
                    return;
                }
            }
        } finally {
            c.a(bVar);
        }
    }

    private void d(String str) {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf == -1) {
            throw new IOException("unexpected journal line: " + str);
        }
        int i2 = indexOf + 1;
        int indexOf2 = str.indexOf(32, i2);
        if (indexOf2 == -1) {
            String substring = str.substring(i2);
            if (indexOf != "REMOVE".length() || !str.startsWith("REMOVE")) {
                str2 = substring;
            } else {
                this.k.remove(substring);
                return;
            }
        } else {
            str2 = str.substring(i2, indexOf2);
        }
        c cVar = this.k.get(str2);
        if (cVar == null) {
            cVar = new c(str2);
            this.k.put(str2, cVar);
        }
        if (indexOf2 != -1 && indexOf == "CLEAN".length() && str.startsWith("CLEAN")) {
            String[] split = str.substring(indexOf2 + 1).split(" ");
            cVar.f = true;
            cVar.g = null;
            cVar.a((c) split);
        } else if (indexOf2 == -1 && indexOf == "DIRTY".length() && str.startsWith("DIRTY")) {
            cVar.g = new b(cVar);
        } else if (indexOf2 != -1 || indexOf != "READ".length() || !str.startsWith("READ")) {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    private void c() {
        a(this.d);
        Iterator<c> it = this.k.values().iterator();
        while (it.hasNext()) {
            c next = it.next();
            if (next.g == null) {
                for (int i2 = 0; i2 < this.h; i2++) {
                    this.i += next.e[i2];
                }
            } else {
                next.g = null;
                for (int i3 = 0; i3 < this.h; i3++) {
                    a(next.a(i3));
                    a(next.b(i3));
                }
                it.remove();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private synchronized void d() {
        if (this.j != null) {
            this.j.close();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.d), c.f743a));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write("\n");
            bufferedWriter.write("1");
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.f));
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.h));
            bufferedWriter.write("\n");
            bufferedWriter.write("\n");
            for (c cVar : this.k.values()) {
                if (cVar.g != null) {
                    bufferedWriter.write("DIRTY " + cVar.d + '\n');
                } else {
                    bufferedWriter.write("CLEAN " + cVar.d + cVar.a() + '\n');
                }
            }
            bufferedWriter.close();
            if (this.c.exists()) {
                a(this.c, this.e, true);
            }
            a(this.d, this.c, false);
            this.e.delete();
            this.j = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.c, true), c.f743a));
        } catch (Throwable th) {
            bufferedWriter.close();
            throw th;
        }
    }

    private static void a(File file) {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    private static void a(File file, File file2, boolean z) {
        if (z) {
            a(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    public synchronized d a(String str) {
        d dVar = null;
        synchronized (this) {
            f();
            c cVar = this.k.get(str);
            if (cVar != null && cVar.f) {
                File[] fileArr = cVar.f739a;
                int length = fileArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 < length) {
                        if (!fileArr[i2].exists()) {
                            break;
                        }
                        i2++;
                    } else {
                        this.l++;
                        this.j.append((CharSequence) "READ");
                        this.j.append(' ');
                        this.j.append((CharSequence) str);
                        this.j.append('\n');
                        if (e()) {
                            this.f736a.submit(this.n);
                        }
                        dVar = new d(str, cVar.h, cVar.f739a, cVar.e);
                    }
                }
            }
        }
        return dVar;
    }

    public b b(String str) {
        return a(str, -1);
    }

    private synchronized b a(String str, long j2) {
        c cVar;
        b bVar;
        f();
        c cVar2 = this.k.get(str);
        if (j2 == -1 || (cVar2 != null && cVar2.h == j2)) {
            if (cVar2 == null) {
                c cVar3 = new c(str);
                this.k.put(str, cVar3);
                cVar = cVar3;
            } else if (cVar2.g != null) {
                bVar = null;
            } else {
                cVar = cVar2;
            }
            bVar = new b(cVar);
            cVar.g = bVar;
            this.j.append((CharSequence) "DIRTY");
            this.j.append(' ');
            this.j.append((CharSequence) str);
            this.j.append('\n');
            this.j.flush();
        } else {
            bVar = null;
        }
        return bVar;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private synchronized void a(b bVar, boolean z) {
        synchronized (this) {
            c cVar = bVar.b;
            if (cVar.g != bVar) {
                throw new IllegalStateException();
            }
            if (z && !cVar.f) {
                int i2 = 0;
                while (true) {
                    if (i2 >= this.h) {
                        break;
                    } else if (!bVar.c[i2]) {
                        bVar.b();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                    } else if (!cVar.b(i2).exists()) {
                        bVar.b();
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            for (int i3 = 0; i3 < this.h; i3++) {
                File b2 = cVar.b(i3);
                if (!z) {
                    a(b2);
                } else if (b2.exists()) {
                    File a2 = cVar.a(i3);
                    b2.renameTo(a2);
                    long j2 = cVar.e[i3];
                    long length = a2.length();
                    cVar.e[i3] = length;
                    this.i = (this.i - j2) + length;
                }
            }
            this.l++;
            cVar.g = null;
            if (cVar.f || z) {
                cVar.f = true;
                this.j.append((CharSequence) "CLEAN");
                this.j.append(' ');
                this.j.append((CharSequence) cVar.d);
                this.j.append((CharSequence) cVar.a());
                this.j.append('\n');
                if (z) {
                    long j3 = this.m;
                    this.m = 1 + j3;
                    cVar.h = j3;
                }
            } else {
                this.k.remove(cVar.d);
                this.j.append((CharSequence) "REMOVE");
                this.j.append(' ');
                this.j.append((CharSequence) cVar.d);
                this.j.append('\n');
            }
            this.j.flush();
            if (this.i > this.g || e()) {
                this.f736a.submit(this.n);
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean e() {
        return this.l >= 2000 && this.l >= this.k.size();
    }

    public synchronized boolean c(String str) {
        boolean z;
        synchronized (this) {
            f();
            c cVar = this.k.get(str);
            if (cVar == null || cVar.g != null) {
                z = false;
            } else {
                for (int i2 = 0; i2 < this.h; i2++) {
                    File a2 = cVar.a(i2);
                    if (!a2.exists() || a2.delete()) {
                        this.i -= cVar.e[i2];
                        cVar.e[i2] = 0;
                    } else {
                        throw new IOException("failed to delete " + a2);
                    }
                }
                this.l++;
                this.j.append((CharSequence) "REMOVE");
                this.j.append(' ');
                this.j.append((CharSequence) str);
                this.j.append('\n');
                this.k.remove(str);
                if (e()) {
                    this.f736a.submit(this.n);
                }
                z = true;
            }
        }
        return z;
    }

    private void f() {
        if (this.j == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        if (this.j != null) {
            Iterator it = new ArrayList(this.k.values()).iterator();
            while (it.hasNext()) {
                c cVar = (c) it.next();
                if (cVar.g != null) {
                    cVar.g.b();
                }
            }
            g();
            this.j.close();
            this.j = null;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void g() {
        while (this.i > this.g) {
            c(this.k.entrySet().iterator().next().getKey());
        }
    }

    public void a() {
        close();
        c.a(this.b);
    }

    public final class d {
        private final String b;
        private final long c;
        private final long[] d;
        private final File[] e;

        private d(String str, long j, File[] fileArr, long[] jArr) {
            this.b = str;
            this.c = j;
            this.e = fileArr;
            this.d = jArr;
        }

        public File a(int i) {
            return this.e[i];
        }
    }

    public final class b {
        private final c b;
        private final boolean[] c;
        private boolean d;

        private b(c cVar) {
            this.b = cVar;
            this.c = cVar.f ? null : new boolean[a.this.h];
        }

        public File a(int i) {
            File b2;
            synchronized (a.this) {
                if (this.b.g != this) {
                    throw new IllegalStateException();
                }
                if (!this.b.f) {
                    this.c[i] = true;
                }
                b2 = this.b.b(i);
                if (!a.this.b.exists()) {
                    a.this.b.mkdirs();
                }
            }
            return b2;
        }

        public void a() {
            a.this.a((a) this, (b) true);
            this.d = true;
        }

        public void b() {
            a.this.a((a) this, (b) false);
        }

        public void c() {
            if (!this.d) {
                try {
                    b();
                } catch (IOException e) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public final class c {

        /* renamed from: a  reason: collision with root package name */
        File[] f739a;
        File[] b;
        private final String d;
        private final long[] e;
        private boolean f;
        private b g;
        private long h;

        private c(String str) {
            this.d = str;
            this.e = new long[a.this.h];
            this.f739a = new File[a.this.h];
            this.b = new File[a.this.h];
            StringBuilder append = new StringBuilder(str).append('.');
            int length = append.length();
            for (int i = 0; i < a.this.h; i++) {
                append.append(i);
                this.f739a[i] = new File(a.this.b, append.toString());
                append.append(".tmp");
                this.b[i] = new File(a.this.b, append.toString());
                append.setLength(length);
            }
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            for (long j : this.e) {
                sb.append(' ').append(j);
            }
            return sb.toString();
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void a(String[] strArr) {
            if (strArr.length != a.this.h) {
                throw b(strArr);
            }
            for (int i = 0; i < strArr.length; i++) {
                try {
                    this.e[i] = Long.parseLong(strArr[i]);
                } catch (NumberFormatException e2) {
                    throw b(strArr);
                }
            }
        }

        private IOException b(String[] strArr) {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        public File a(int i) {
            return this.f739a[i];
        }

        public File b(int i) {
            return this.b[i];
        }
    }

    /* renamed from: com.a.a.a.a$a  reason: collision with other inner class name */
    private static final class ThreadFactoryC0026a implements ThreadFactory {
        private ThreadFactoryC0026a() {
        }

        public synchronized Thread newThread(Runnable runnable) {
            Thread thread;
            thread = new Thread(runnable, "glide-disk-lru-cache-thread");
            thread.setPriority(1);
            return thread;
        }
    }
}
