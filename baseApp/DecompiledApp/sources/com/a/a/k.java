package com.a.a;

import com.a.a.h.b.a;
import com.a.a.h.b.c;
import com.a.a.k;

public abstract class k<CHILD extends k<CHILD, TranscodeType>, TranscodeType> implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private c<? super TranscodeType> f978a = a.a();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final CHILD clone() {
        try {
            return (CHILD) ((k) super.clone());
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public final c<? super TranscodeType> b() {
        return this.f978a;
    }
}
