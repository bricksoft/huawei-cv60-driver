package com.a.a;

public enum f {
    LOW(0.5f),
    NORMAL(1.0f),
    HIGH(1.5f);
    
    private float d;

    private f(float f) {
        this.d = f;
    }
}
