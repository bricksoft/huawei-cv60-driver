package com.a.a;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import com.a.a.d.b.h;
import com.a.a.h.a;
import com.a.a.h.b;
import com.a.a.h.c;
import com.a.a.h.d;
import com.a.a.h.f;
import com.a.a.h.g;

public class i<TranscodeType> implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    protected static final d f958a = new d().b(h.c).a(g.LOW).a(true);
    @NonNull
    protected d b;
    private final e c;
    private final j d;
    private final Class<TranscodeType> e;
    private final d f;
    private final c g;
    @NonNull
    private k<?, ? super TranscodeType> h;
    @Nullable
    private Object i;
    @Nullable
    private c<TranscodeType> j;
    @Nullable
    private i<TranscodeType> k;
    @Nullable
    private Float l;
    private boolean m = true;
    private boolean n;
    private boolean o;

    protected i(c cVar, j jVar, Class<TranscodeType> cls) {
        this.g = cVar;
        this.d = jVar;
        this.c = cVar.e();
        this.e = cls;
        this.f = jVar.h();
        this.h = jVar.b(cls);
        this.b = this.f;
    }

    public i<TranscodeType> a(@NonNull d dVar) {
        com.a.a.j.h.a(dVar);
        this.b = a().a(dVar);
        return this;
    }

    /* access modifiers changed from: protected */
    public d a() {
        return this.f == this.b ? this.b.clone() : this.b;
    }

    public i<TranscodeType> a(@Nullable Object obj) {
        return b(obj);
    }

    private i<TranscodeType> b(@Nullable Object obj) {
        this.i = obj;
        this.n = true;
        return this;
    }

    @CheckResult
    /* renamed from: b */
    public i<TranscodeType> clone() {
        try {
            i<TranscodeType> iVar = (i) super.clone();
            iVar.b = iVar.b.clone();
            iVar.h = iVar.h.clone();
            return iVar;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    public <Y extends com.a.a.h.a.h<TranscodeType>> Y a(@NonNull Y y) {
        return (Y) a(y, a());
    }

    private <Y extends com.a.a.h.a.h<TranscodeType>> Y a(@NonNull Y y, d dVar) {
        com.a.a.j.i.a();
        com.a.a.j.h.a(y);
        if (!this.n) {
            throw new IllegalArgumentException("You must call #load() before calling #into()");
        }
        a b2 = b(y, dVar.h());
        a b3 = y.b();
        if (b2.a(b3)) {
            b2.i();
            if (!((a) com.a.a.j.h.a(b3)).e()) {
                b3.a();
            }
        } else {
            this.d.a((com.a.a.h.a.h<?>) y);
            y.a(b2);
            this.d.a(y, b2);
        }
        return y;
    }

    public com.a.a.h.a.h<TranscodeType> a(ImageView imageView) {
        com.a.a.j.i.a();
        com.a.a.j.h.a(imageView);
        d dVar = this.b;
        if (!dVar.c() && dVar.b() && imageView.getScaleType() != null) {
            switch (AnonymousClass1.f959a[imageView.getScaleType().ordinal()]) {
                case 1:
                    dVar = dVar.clone().d();
                    break;
                case 2:
                    dVar = dVar.clone().f();
                    break;
                case 3:
                case 4:
                case 5:
                    dVar = dVar.clone().e();
                    break;
                case 6:
                    dVar = dVar.clone().f();
                    break;
            }
        }
        return a(this.c.a(imageView, this.e), dVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.a.a.i$1  reason: invalid class name */
    public static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f959a = new int[ImageView.ScaleType.values().length];

        static {
            b = new int[g.values().length];
            try {
                b[g.LOW.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[g.NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[g.HIGH.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                b[g.IMMEDIATE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f959a[ImageView.ScaleType.CENTER_CROP.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f959a[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f959a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f959a[ImageView.ScaleType.FIT_START.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f959a[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f959a[ImageView.ScaleType.FIT_XY.ordinal()] = 6;
            } catch (NoSuchFieldError e10) {
            }
            try {
                f959a[ImageView.ScaleType.CENTER.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                f959a[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
        }
    }

    private g a(g gVar) {
        switch (gVar) {
            case LOW:
                return g.NORMAL;
            case NORMAL:
                return g.HIGH;
            case HIGH:
            case IMMEDIATE:
                return g.IMMEDIATE;
            default:
                throw new IllegalArgumentException("unknown priority: " + this.b.x());
        }
    }

    private a b(com.a.a.h.a.h<TranscodeType> hVar, d dVar) {
        return a(hVar, (g) null, this.h, dVar.x(), dVar.y(), dVar.A(), dVar);
    }

    private a a(com.a.a.h.a.h<TranscodeType> hVar, @Nullable g gVar, k<?, ? super TranscodeType> kVar, g gVar2, int i2, int i3, d dVar) {
        k<?, ? super TranscodeType> kVar2;
        g a2;
        int i4;
        int i5;
        if (this.k != null) {
            if (this.o) {
                throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
            }
            k<?, ? super TranscodeType> kVar3 = this.k.h;
            if (this.k.m) {
                kVar2 = kVar;
            } else {
                kVar2 = kVar3;
            }
            if (this.k.b.w()) {
                a2 = this.k.b.x();
            } else {
                a2 = a(gVar2);
            }
            int y = this.k.b.y();
            int A = this.k.b.A();
            if (!com.a.a.j.i.a(i2, i3) || this.k.b.z()) {
                i4 = A;
                i5 = y;
            } else {
                int y2 = dVar.y();
                i4 = dVar.A();
                i5 = y2;
            }
            g gVar3 = new g(gVar);
            a a3 = a(hVar, dVar, gVar3, kVar, gVar2, i2, i3);
            this.o = true;
            a a4 = this.k.a(hVar, gVar3, kVar2, a2, i5, i4, this.k.b);
            this.o = false;
            gVar3.a(a3, a4);
            return gVar3;
        } else if (this.l == null) {
            return a(hVar, dVar, gVar, kVar, gVar2, i2, i3);
        } else {
            g gVar4 = new g(gVar);
            gVar4.a(a(hVar, dVar, gVar4, kVar, gVar2, i2, i3), a(hVar, dVar.clone().a(this.l.floatValue()), gVar4, kVar, a(gVar2), i2, i3));
            return gVar4;
        }
    }

    private a a(com.a.a.h.a.h<TranscodeType> hVar, d dVar, b bVar, k<?, ? super TranscodeType> kVar, g gVar, int i2, int i3) {
        return f.a(this.c, this.i, this.e, dVar, i2, i3, gVar, hVar, this.j, bVar, this.c.b(), kVar.b());
    }
}
