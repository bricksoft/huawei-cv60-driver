package com.a.a.g;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.a.a.d.b.q;
import com.a.a.j.g;
import java.util.concurrent.atomic.AtomicReference;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayMap<g, q<?, ?, ?>> f941a = new ArrayMap<>();
    private final AtomicReference<g> b = new AtomicReference<>();

    public boolean a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        boolean containsKey;
        g c = c(cls, cls2, cls3);
        synchronized (this.f941a) {
            containsKey = this.f941a.containsKey(c);
        }
        this.b.set(c);
        return containsKey;
    }

    @Nullable
    public <Data, TResource, Transcode> q<Data, TResource, Transcode> b(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        q<Data, TResource, Transcode> qVar;
        g c = c(cls, cls2, cls3);
        synchronized (this.f941a) {
            qVar = (q<Data, TResource, Transcode>) this.f941a.get(c);
        }
        this.b.set(c);
        return qVar;
    }

    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, q<?, ?, ?> qVar) {
        synchronized (this.f941a) {
            this.f941a.put(new g(cls, cls2, cls3), qVar);
        }
    }

    private g c(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        g andSet = this.b.getAndSet(null);
        if (andSet == null) {
            andSet = new g();
        }
        andSet.a(cls, cls2, cls3);
        return andSet;
    }
}
