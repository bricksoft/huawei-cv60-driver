package com.a.a.g;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.a.a.j.g;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicReference<g> f942a = new AtomicReference<>();
    private final ArrayMap<g, List<Class<?>>> b = new ArrayMap<>();

    @Nullable
    public List<Class<?>> a(Class<?> cls, Class<?> cls2) {
        g gVar;
        List<Class<?>> list;
        g andSet = this.f942a.getAndSet(null);
        if (andSet == null) {
            gVar = new g(cls, cls2);
        } else {
            andSet.a(cls, cls2);
            gVar = andSet;
        }
        synchronized (this.b) {
            list = this.b.get(gVar);
        }
        this.f942a.set(gVar);
        return list;
    }

    public void a(Class<?> cls, Class<?> cls2, List<Class<?>> list) {
        synchronized (this.b) {
            this.b.put(new g(cls, cls2), list);
        }
    }
}
