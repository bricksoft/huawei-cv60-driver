package com.a.a.g;

import com.a.a.d.k;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final List<String> f943a = new ArrayList();
    private final Map<String, List<a<?, ?>>> b = new HashMap();

    public synchronized void a(List<String> list) {
        ArrayList<String> arrayList = new ArrayList(this.f943a);
        this.f943a.clear();
        this.f943a.addAll(list);
        for (String str : arrayList) {
            if (!list.contains(str)) {
                this.f943a.add(str);
            }
        }
    }

    public synchronized <T, R> List<k<T, R>> a(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (String str : this.f943a) {
            List<a<?, ?>> list = this.b.get(str);
            if (list != null) {
                for (a<?, ?> aVar : list) {
                    if (aVar.a(cls, cls2)) {
                        arrayList.add(aVar.b);
                    }
                }
            }
        }
        return arrayList;
    }

    public synchronized <T, R> List<Class<R>> b(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (String str : this.f943a) {
            List<a<?, ?>> list = this.b.get(str);
            if (list != null) {
                for (a<?, ?> aVar : list) {
                    if (aVar.a(cls, cls2)) {
                        arrayList.add(aVar.f944a);
                    }
                }
            }
        }
        return arrayList;
    }

    public synchronized <T, R> void a(String str, k<T, R> kVar, Class<T> cls, Class<R> cls2) {
        a(str).add(new a<>(cls, cls2, kVar));
    }

    private synchronized List<a<?, ?>> a(String str) {
        List<a<?, ?>> list;
        if (!this.f943a.contains(str)) {
            this.f943a.add(str);
        }
        list = this.b.get(str);
        if (list == null) {
            list = new ArrayList<>();
            this.b.put(str, list);
        }
        return list;
    }

    /* access modifiers changed from: private */
    public static class a<T, R> {

        /* renamed from: a  reason: collision with root package name */
        final Class<R> f944a;
        final k<T, R> b;
        private final Class<T> c;

        public a(Class<T> cls, Class<R> cls2, k<T, R> kVar) {
            this.c = cls;
            this.f944a = cls2;
            this.b = kVar;
        }

        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.c.isAssignableFrom(cls) && cls2.isAssignableFrom(this.f944a);
        }
    }
}
