package com.a.a.g;

import android.support.annotation.Nullable;
import com.a.a.d.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final List<C0037a<?>> f938a = new ArrayList();

    @Nullable
    public synchronized <T> d<T> a(Class<T> cls) {
        d<T> dVar;
        Iterator<C0037a<?>> it = this.f938a.iterator();
        while (true) {
            if (!it.hasNext()) {
                dVar = null;
                break;
            }
            C0037a<?> next = it.next();
            if (next.a(cls)) {
                dVar = next.f939a;
                break;
            }
        }
        return dVar;
    }

    public synchronized <T> void a(Class<T> cls, d<T> dVar) {
        this.f938a.add(new C0037a<>(cls, dVar));
    }

    /* access modifiers changed from: private */
    /* renamed from: com.a.a.g.a$a  reason: collision with other inner class name */
    public static final class C0037a<T> {

        /* renamed from: a  reason: collision with root package name */
        final d<T> f939a;
        private final Class<T> b;

        public C0037a(Class<T> cls, d<T> dVar) {
            this.b = cls;
            this.f939a = dVar;
        }

        public boolean a(Class<?> cls) {
            return this.b.isAssignableFrom(cls);
        }
    }
}
