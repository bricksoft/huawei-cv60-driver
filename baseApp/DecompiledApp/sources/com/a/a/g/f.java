package com.a.a.g;

import android.support.annotation.Nullable;
import com.a.a.d.d;
import com.a.a.d.l;
import java.util.ArrayList;
import java.util.List;

public class f {

    /* renamed from: a  reason: collision with root package name */
    final List<a<?>> f945a = new ArrayList();

    public synchronized <Z> void a(Class<Z> cls, l<Z> lVar) {
        this.f945a.add(new a<>(cls, lVar));
    }

    @Nullable
    public synchronized <Z> l<Z> a(Class<Z> cls) {
        d dVar;
        int size = this.f945a.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                dVar = (l<Z>) null;
                break;
            }
            a<?> aVar = this.f945a.get(i);
            if (aVar.a(cls)) {
                dVar = (l<T>) aVar.f946a;
                break;
            }
            i++;
        }
        return (l<Z>) dVar;
    }

    /* access modifiers changed from: private */
    public static final class a<T> {

        /* renamed from: a  reason: collision with root package name */
        final l<T> f946a;
        private final Class<T> b;

        a(Class<T> cls, l<T> lVar) {
            this.b = cls;
            this.f946a = lVar;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Class<?> cls) {
            return this.b.isAssignableFrom(cls);
        }
    }
}
