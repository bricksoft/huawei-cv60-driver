package com.a.a;

import android.support.v4.util.Pools;
import com.a.a.d.a.c;
import com.a.a.d.b.g;
import com.a.a.d.b.q;
import com.a.a.d.b.s;
import com.a.a.d.c.n;
import com.a.a.d.c.o;
import com.a.a.d.c.p;
import com.a.a.d.k;
import com.a.a.d.l;
import com.a.a.g.f;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private final p f947a = new p(this.j);
    private final com.a.a.g.a b = new com.a.a.g.a();
    private final com.a.a.g.e c = new com.a.a.g.e();
    private final f d = new f();
    private final com.a.a.d.a.d e = new com.a.a.d.a.d();
    private final com.a.a.d.d.f.e f = new com.a.a.d.d.f.e();
    private final com.a.a.g.b g = new com.a.a.g.b();
    private final com.a.a.g.d h = new com.a.a.g.d();
    private final com.a.a.g.c i = new com.a.a.g.c();
    private final Pools.Pool<List<Exception>> j = com.a.a.j.a.a.a();

    public h() {
        a(Arrays.asList("Gif", "Bitmap", "BitmapDrawable"));
    }

    public <Data> h a(Class<Data> cls, com.a.a.d.d<Data> dVar) {
        this.b.a(cls, dVar);
        return this;
    }

    public <Data, TResource> h a(Class<Data> cls, Class<TResource> cls2, k<Data, TResource> kVar) {
        a("legacy_append", cls, cls2, kVar);
        return this;
    }

    public <Data, TResource> h a(String str, Class<Data> cls, Class<TResource> cls2, k<Data, TResource> kVar) {
        this.c.a(str, kVar, cls, cls2);
        return this;
    }

    public final h a(List<String> list) {
        ArrayList arrayList = new ArrayList(list);
        arrayList.add(0, "legacy_prepend_all");
        arrayList.add("legacy_append");
        this.c.a(arrayList);
        return this;
    }

    public <TResource> h a(Class<TResource> cls, l<TResource> lVar) {
        this.d.a(cls, lVar);
        return this;
    }

    public h a(c.a aVar) {
        this.e.a((c.a<?>) aVar);
        return this;
    }

    public <TResource, Transcode> h a(Class<TResource> cls, Class<Transcode> cls2, com.a.a.d.d.f.d<TResource, Transcode> dVar) {
        this.f.a(cls, cls2, dVar);
        return this;
    }

    public h a(com.a.a.d.f fVar) {
        this.g.a(fVar);
        return this;
    }

    public <Model, Data> h a(Class<Model> cls, Class<Data> cls2, o<Model, Data> oVar) {
        this.f947a.a(cls, cls2, oVar);
        return this;
    }

    public <Data, TResource, Transcode> q<Data, TResource, Transcode> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        q<Data, TResource, Transcode> b2 = this.i.b(cls, cls2, cls3);
        if (b2 == null && !this.i.a(cls, cls2, cls3)) {
            List<g<Data, TResource, Transcode>> c2 = c(cls, cls2, cls3);
            if (c2.isEmpty()) {
                b2 = null;
            } else {
                b2 = new q<>(cls, cls2, cls3, c2, this.j);
            }
            this.i.a(cls, cls2, cls3, b2);
        }
        return b2;
    }

    private <Data, TResource, Transcode> List<g<Data, TResource, Transcode>> c(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        ArrayList arrayList = new ArrayList();
        for (Class cls4 : this.c.b(cls, cls2)) {
            for (Class cls5 : this.f.b(cls4, cls3)) {
                arrayList.add(new g(cls, cls4, cls5, this.c.a(cls, cls4), this.f.a(cls4, cls5), this.j));
            }
        }
        return arrayList;
    }

    public <Model, TResource, Transcode> List<Class<?>> b(Class<Model> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        List<Class<?>> a2 = this.h.a(cls, cls2);
        if (a2 != null) {
            return a2;
        }
        ArrayList arrayList = new ArrayList();
        for (Class<?> cls4 : this.f947a.a((Class<?>) cls)) {
            for (Class cls5 : this.c.b(cls4, cls2)) {
                if (!this.f.b(cls5, cls3).isEmpty() && !arrayList.contains(cls5)) {
                    arrayList.add(cls5);
                }
            }
        }
        this.h.a(cls, cls2, Collections.unmodifiableList(arrayList));
        return arrayList;
    }

    public boolean a(s<?> sVar) {
        return this.d.a(sVar.b()) != null;
    }

    public <X> l<X> b(s<X> sVar) {
        l<X> a2 = this.d.a(sVar.b());
        if (a2 != null) {
            return a2;
        }
        throw new d(sVar.b());
    }

    public <X> com.a.a.d.d<X> a(X x) {
        com.a.a.d.d<X> a2 = this.b.a(x.getClass());
        if (a2 != null) {
            return a2;
        }
        throw new e(x.getClass());
    }

    public <X> com.a.a.d.a.c<X> b(X x) {
        return this.e.a((Object) x);
    }

    public <Model> List<n<Model, ?>> c(Model model) {
        List<n<Model, ?>> a2 = this.f947a.a((Object) model);
        if (!a2.isEmpty()) {
            return a2;
        }
        throw new c(model);
    }

    public List<com.a.a.d.f> a() {
        List<com.a.a.d.f> a2 = this.g.a();
        if (!a2.isEmpty()) {
            return a2;
        }
        throw new b();
    }

    public static class c extends a {
        public c(Object obj) {
            super("Failed to find any ModelLoaders for model: " + obj);
        }

        public c(Class<?> cls, Class<?> cls2) {
            super("Failed to find any ModelLoaders for model: " + cls + " and data: " + cls2);
        }
    }

    public static class d extends a {
        public d(Class<?> cls) {
            super("Failed to find result encoder for resource class: " + cls);
        }
    }

    public static class e extends a {
        public e(Class<?> cls) {
            super("Failed to find source encoder for data class: " + cls);
        }
    }

    public static class a extends RuntimeException {
        public a(String str) {
            super(str);
        }
    }

    public static final class b extends a {
        public b() {
            super("Failed to find image header parser.");
        }
    }
}
