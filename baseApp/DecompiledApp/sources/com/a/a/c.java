package com.a.a;

import android.annotation.TargetApi;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.util.Log;
import com.a.a.d.a.c;
import com.a.a.d.a.i;
import com.a.a.d.b.a.b;
import com.a.a.d.b.a.e;
import com.a.a.d.b.b.h;
import com.a.a.d.b.d.a;
import com.a.a.d.b.i;
import com.a.a.d.c.a;
import com.a.a.d.c.a.a;
import com.a.a.d.c.a.b;
import com.a.a.d.c.a.c;
import com.a.a.d.c.a.d;
import com.a.a.d.c.a.e;
import com.a.a.d.c.b;
import com.a.a.d.c.d;
import com.a.a.d.c.e;
import com.a.a.d.c.f;
import com.a.a.d.c.k;
import com.a.a.d.c.s;
import com.a.a.d.c.t;
import com.a.a.d.c.u;
import com.a.a.d.c.v;
import com.a.a.d.c.w;
import com.a.a.d.c.x;
import com.a.a.d.d.a.g;
import com.a.a.d.d.a.j;
import com.a.a.d.d.a.q;
import com.a.a.d.d.a.s;
import com.a.a.d.d.b.a;
import com.a.a.d.f;
import com.a.a.e.d;
import com.a.a.e.l;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@TargetApi(14)
public class c implements ComponentCallbacks2 {

    /* renamed from: a  reason: collision with root package name */
    private static volatile c f748a;
    private static volatile boolean b;
    private final i c;
    private final e d;
    private final h e;
    private final a f;
    private final e g;
    private final h h;
    private final b i;
    private final l j;
    private final d k;
    private final List<j> l = new ArrayList();
    private f m = f.NORMAL;

    public static c a(Context context) {
        if (f748a == null) {
            synchronized (c.class) {
                if (f748a == null) {
                    c(context);
                }
            }
        }
        return f748a;
    }

    private static void c(Context context) {
        if (b) {
            throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
        }
        b = true;
        d(context);
        b = false;
    }

    private static void d(Context context) {
        List<com.a.a.f.c> list;
        Context applicationContext = context.getApplicationContext();
        a i2 = i();
        List<com.a.a.f.c> emptyList = Collections.emptyList();
        if (i2 == null || i2.c()) {
            list = new com.a.a.f.e(applicationContext).a();
        } else {
            list = emptyList;
        }
        if (i2 != null && !i2.a().isEmpty()) {
            Set<Class<?>> a2 = i2.a();
            Iterator<com.a.a.f.c> it = list.iterator();
            while (it.hasNext()) {
                com.a.a.f.c next = it.next();
                if (a2.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            Iterator<com.a.a.f.c> it2 = list.iterator();
            while (it2.hasNext()) {
                Log.d("Glide", "Discovered GlideModule from manifest: " + it2.next().getClass());
            }
        }
        d a3 = new d().a(i2 != null ? i2.b() : null);
        for (com.a.a.f.c cVar : list) {
            cVar.a(applicationContext, a3);
        }
        if (i2 != null) {
            i2.a(applicationContext, a3);
        }
        c a4 = a3.a(applicationContext);
        for (com.a.a.f.c cVar2 : list) {
            cVar2.a(applicationContext, a4, a4.h);
        }
        if (i2 != null) {
            i2.a(applicationContext, a4, a4.h);
        }
        context.getApplicationContext().registerComponentCallbacks(a4);
        f748a = a4;
    }

    @Nullable
    private static a i() {
        try {
            return (a) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").newInstance();
        } catch (ClassNotFoundException e2) {
            if (Log.isLoggable("Glide", 5)) {
                Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            }
            return null;
        } catch (InstantiationException e3) {
            throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", e3);
        } catch (IllegalAccessException e4) {
            throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", e4);
        }
    }

    @TargetApi(14)
    c(Context context, i iVar, h hVar, e eVar, b bVar, l lVar, d dVar, int i2, com.a.a.h.d dVar2, Map<Class<?>, k<?, ?>> map) {
        this.c = iVar;
        this.d = eVar;
        this.i = bVar;
        this.e = hVar;
        this.j = lVar;
        this.k = dVar;
        this.f = new a(hVar, eVar, (com.a.a.d.b) dVar2.k().a(com.a.a.d.d.a.l.f892a));
        Resources resources = context.getResources();
        this.h = new h();
        this.h.a((f) new j());
        com.a.a.d.d.a.l lVar2 = new com.a.a.d.d.a.l(this.h.a(), resources.getDisplayMetrics(), eVar, bVar);
        com.a.a.d.d.e.a aVar = new com.a.a.d.d.e.a(context, this.h.a(), eVar, bVar);
        this.h.a(ByteBuffer.class, new com.a.a.d.c.c()).a(InputStream.class, new t(bVar)).a("Bitmap", ByteBuffer.class, Bitmap.class, new g(lVar2)).a("Bitmap", InputStream.class, Bitmap.class, new q(lVar2, bVar)).a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, new s(eVar)).a(Bitmap.class, (com.a.a.d.l) new com.a.a.d.d.a.d()).a("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new com.a.a.d.d.a.a(resources, eVar, new g(lVar2))).a("BitmapDrawable", InputStream.class, BitmapDrawable.class, new com.a.a.d.d.a.a(resources, eVar, new q(lVar2, bVar))).a("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new com.a.a.d.d.a.a(resources, eVar, new s(eVar))).a(BitmapDrawable.class, (com.a.a.d.l) new com.a.a.d.d.a.b(eVar, new com.a.a.d.d.a.d())).a("Gif", InputStream.class, com.a.a.d.d.e.c.class, new com.a.a.d.d.e.i(this.h.a(), aVar, bVar)).a("Gif", ByteBuffer.class, com.a.a.d.d.e.c.class, aVar).a(com.a.a.d.d.e.c.class, (com.a.a.d.l) new com.a.a.d.d.e.d()).a(com.a.a.b.a.class, com.a.a.b.a.class, new v.a()).a("Bitmap", com.a.a.b.a.class, Bitmap.class, new com.a.a.d.d.e.h(eVar)).a((c.a) new a.C0035a()).a(File.class, ByteBuffer.class, new d.b()).a(File.class, InputStream.class, new f.e()).a(File.class, File.class, new com.a.a.d.d.d.a()).a(File.class, ParcelFileDescriptor.class, new f.b()).a(File.class, File.class, new v.a()).a((c.a) new i.a(bVar)).a(Integer.TYPE, InputStream.class, new s.b(resources)).a(Integer.TYPE, ParcelFileDescriptor.class, new s.a(resources)).a(Integer.class, InputStream.class, new s.b(resources)).a(Integer.class, ParcelFileDescriptor.class, new s.a(resources)).a(String.class, InputStream.class, new e.c()).a(String.class, InputStream.class, new u.b()).a(String.class, ParcelFileDescriptor.class, new u.a()).a(Uri.class, InputStream.class, new b.a()).a(Uri.class, InputStream.class, new a.c(context.getAssets())).a(Uri.class, ParcelFileDescriptor.class, new a.b(context.getAssets())).a(Uri.class, InputStream.class, new c.a(context)).a(Uri.class, InputStream.class, new d.a(context)).a(Uri.class, InputStream.class, new w.c(context.getContentResolver())).a(Uri.class, ParcelFileDescriptor.class, new w.a(context.getContentResolver())).a(Uri.class, InputStream.class, new x.a()).a(URL.class, InputStream.class, new e.a()).a(Uri.class, File.class, new k.a(context)).a(com.a.a.d.c.g.class, InputStream.class, new a.C0032a()).a(byte[].class, ByteBuffer.class, new b.a()).a(byte[].class, InputStream.class, new b.d()).a(Bitmap.class, BitmapDrawable.class, new com.a.a.d.d.f.b(resources, eVar)).a(Bitmap.class, byte[].class, new com.a.a.d.d.f.a()).a(com.a.a.d.d.e.c.class, byte[].class, new com.a.a.d.d.f.c());
        this.g = new e(context, this.h, new com.a.a.h.a.e(), dVar2, map, iVar, i2);
    }

    public com.a.a.d.b.a.e a() {
        return this.d;
    }

    public com.a.a.d.b.a.b b() {
        return this.i;
    }

    public Context c() {
        return this.g.getBaseContext();
    }

    /* access modifiers changed from: package-private */
    public com.a.a.e.d d() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public e e() {
        return this.g;
    }

    public void f() {
        com.a.a.j.i.a();
        this.e.a();
        this.d.a();
        this.i.a();
    }

    public void a(int i2) {
        com.a.a.j.i.a();
        this.e.a(i2);
        this.d.a(i2);
        this.i.a(i2);
    }

    public l g() {
        return this.j;
    }

    private static l e(@Nullable Context context) {
        com.a.a.j.h.a(context, "You cannot start a load on a not yet attached View or a  Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return a(context).g();
    }

    public static j b(Context context) {
        return e(context).a(context);
    }

    public h h() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void a(com.a.a.h.a.h<?> hVar) {
        synchronized (this.l) {
            for (j jVar : this.l) {
                if (jVar.b(hVar)) {
                    return;
                }
            }
            throw new IllegalStateException("Failed to remove target from managers");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar) {
        synchronized (this.l) {
            if (this.l.contains(jVar)) {
                throw new IllegalStateException("Cannot register already registered manager");
            }
            this.l.add(jVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(j jVar) {
        synchronized (this.l) {
            if (!this.l.contains(jVar)) {
                throw new IllegalStateException("Cannot register not yet registered manager");
            }
            this.l.remove(jVar);
        }
    }

    public void onTrimMemory(int i2) {
        a(i2);
    }

    public void onConfigurationChanged(Configuration configuration) {
    }

    public void onLowMemory() {
        f();
    }
}
