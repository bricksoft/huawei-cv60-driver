package com.a.a.e;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import com.a.a.e.c;

public class f implements d {
    @Override // com.a.a.e.d
    @NonNull
    public c a(@NonNull Context context, @NonNull c.a aVar) {
        return ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_NETWORK_STATE") == 0 ? new e(context, aVar) : new j();
    }
}
