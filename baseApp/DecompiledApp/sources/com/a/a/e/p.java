package com.a.a.e;

import com.a.a.h.a.h;
import com.a.a.j.i;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public final class p implements i {

    /* renamed from: a  reason: collision with root package name */
    private final Set<h<?>> f934a = Collections.newSetFromMap(new WeakHashMap());

    public void a(h<?> hVar) {
        this.f934a.add(hVar);
    }

    public void b(h<?> hVar) {
        this.f934a.remove(hVar);
    }

    @Override // com.a.a.e.i
    public void c() {
        for (h hVar : i.a(this.f934a)) {
            hVar.c();
        }
    }

    @Override // com.a.a.e.i
    public void d() {
        for (h hVar : i.a(this.f934a)) {
            hVar.d();
        }
    }

    @Override // com.a.a.e.i
    public void e() {
        for (h hVar : i.a(this.f934a)) {
            hVar.e();
        }
    }

    public List<h<?>> a() {
        return new ArrayList(this.f934a);
    }

    public void b() {
        this.f934a.clear();
    }
}
