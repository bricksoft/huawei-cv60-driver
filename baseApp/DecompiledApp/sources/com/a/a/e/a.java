package com.a.a.e;

import com.a.a.j.i;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/* access modifiers changed from: package-private */
public class a implements h {

    /* renamed from: a  reason: collision with root package name */
    private final Set<i> f925a = Collections.newSetFromMap(new WeakHashMap());
    private boolean b;
    private boolean c;

    a() {
    }

    @Override // com.a.a.e.h
    public void a(i iVar) {
        this.f925a.add(iVar);
        if (this.c) {
            iVar.e();
        } else if (this.b) {
            iVar.c();
        } else {
            iVar.d();
        }
    }

    @Override // com.a.a.e.h
    public void b(i iVar) {
        this.f925a.remove(iVar);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b = true;
        for (i iVar : i.a(this.f925a)) {
            iVar.c();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.b = false;
        for (i iVar : i.a(this.f925a)) {
            iVar.d();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.c = true;
        for (i iVar : i.a(this.f925a)) {
            iVar.e();
        }
    }
}
