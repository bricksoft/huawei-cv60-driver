package com.a.a.e;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.a.a.e.c;

class e implements c {

    /* renamed from: a  reason: collision with root package name */
    final c.a f926a;
    boolean b;
    private final Context c;
    private boolean d;
    private final BroadcastReceiver e = new BroadcastReceiver() {
        /* class com.a.a.e.e.AnonymousClass1 */

        public void onReceive(Context context, Intent intent) {
            boolean z = e.this.b;
            e.this.b = e.this.a(context);
            if (z != e.this.b) {
                e.this.f926a.a(e.this.b);
            }
        }
    };

    public e(Context context, c.a aVar) {
        this.c = context.getApplicationContext();
        this.f926a = aVar;
    }

    private void a() {
        if (!this.d) {
            this.b = a(this.c);
            this.c.registerReceiver(this.e, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.d = true;
        }
    }

    private void b() {
        if (this.d) {
            this.c.unregisterReceiver(this.e);
            this.d = false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override // com.a.a.e.i
    public void c() {
        a();
    }

    @Override // com.a.a.e.i
    public void d() {
        b();
    }

    @Override // com.a.a.e.i
    public void e() {
    }
}
