package com.a.a.e;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Log;
import com.a.a.c;
import com.a.a.j;
import java.util.HashSet;

public class k extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private final a f928a;
    private final m b;
    private final HashSet<k> c;
    @Nullable
    private j d;
    @Nullable
    private k e;
    @Nullable
    private Fragment f;

    public k() {
        this(new a());
    }

    @SuppressLint({"ValidFragment"})
    k(a aVar) {
        this.b = new a();
        this.c = new HashSet<>();
        this.f928a = aVar;
    }

    public void a(j jVar) {
        this.d = jVar;
    }

    /* access modifiers changed from: package-private */
    public a a() {
        return this.f928a;
    }

    @Nullable
    public j b() {
        return this.d;
    }

    public m c() {
        return this.b;
    }

    private void a(k kVar) {
        this.c.add(kVar);
    }

    private void b(k kVar) {
        this.c.remove(kVar);
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
        this.f = fragment;
        if (fragment != null && fragment.getActivity() != null) {
            a(fragment.getActivity());
        }
    }

    @TargetApi(17)
    private Fragment d() {
        Fragment fragment;
        if (Build.VERSION.SDK_INT >= 17) {
            fragment = getParentFragment();
        } else {
            fragment = null;
        }
        return fragment != null ? fragment : this.f;
    }

    private void a(Activity activity) {
        e();
        this.e = c.a(activity).g().a(activity.getFragmentManager(), (Fragment) null);
        if (this.e != this) {
            this.e.a(this);
        }
    }

    private void e() {
        if (this.e != null) {
            this.e.b(this);
            this.e = null;
        }
    }

    @Override // android.app.Fragment
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            a(activity);
        } catch (IllegalStateException e2) {
            if (Log.isLoggable("RMFragment", 5)) {
                Log.w("RMFragment", "Unable to register fragment with root", e2);
            }
        }
    }

    public void onDetach() {
        super.onDetach();
        e();
    }

    public void onStart() {
        super.onStart();
        this.f928a.a();
    }

    public void onStop() {
        super.onStop();
        this.f928a.b();
    }

    public void onDestroy() {
        super.onDestroy();
        this.f928a.c();
        e();
    }

    public String toString() {
        return super.toString() + "{parent=" + d() + "}";
    }

    private class a implements m {
        a() {
        }

        public String toString() {
            return super.toString() + "{fragment=" + k.this + "}";
        }
    }
}
