package com.a.a.e;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.a.a.c;
import com.a.a.j;
import java.util.HashSet;

public class o extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private final a f932a;
    private final m b;
    private final HashSet<o> c;
    @Nullable
    private o d;
    @Nullable
    private j e;
    @Nullable
    private Fragment f;

    public o() {
        this(new a());
    }

    @SuppressLint({"ValidFragment"})
    public o(a aVar) {
        this.b = new a();
        this.c = new HashSet<>();
        this.f932a = aVar;
    }

    public void a(j jVar) {
        this.e = jVar;
    }

    /* access modifiers changed from: package-private */
    public a a() {
        return this.f932a;
    }

    @Nullable
    public j b() {
        return this.e;
    }

    public m c() {
        return this.b;
    }

    private void a(o oVar) {
        this.c.add(oVar);
    }

    private void b(o oVar) {
        this.c.remove(oVar);
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
        this.f = fragment;
        if (fragment != null && fragment.getActivity() != null) {
            a(fragment.getActivity());
        }
    }

    private Fragment d() {
        Fragment parentFragment = getParentFragment();
        return parentFragment != null ? parentFragment : this.f;
    }

    private void a(FragmentActivity fragmentActivity) {
        e();
        this.d = c.a(fragmentActivity).g().a(fragmentActivity.getSupportFragmentManager(), (Fragment) null);
        if (this.d != this) {
            this.d.a(this);
        }
    }

    private void e() {
        if (this.d != null) {
            this.d.b(this);
            this.d = null;
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            a(getActivity());
        } catch (IllegalStateException e2) {
            if (Log.isLoggable("SupportRMFragment", 5)) {
                Log.w("SupportRMFragment", "Unable to register fragment with root", e2);
            }
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.f = null;
        e();
    }

    @Override // android.support.v4.app.Fragment
    public void onStart() {
        super.onStart();
        this.f932a.a();
    }

    @Override // android.support.v4.app.Fragment
    public void onStop() {
        super.onStop();
        this.f932a.b();
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        this.f932a.c();
        e();
    }

    @Override // android.support.v4.app.Fragment
    public String toString() {
        return super.toString() + "{parent=" + d() + "}";
    }

    private class a implements m {
        a() {
        }

        public String toString() {
            return super.toString() + "{fragment=" + o.this + "}";
        }
    }
}
