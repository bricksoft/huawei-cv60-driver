package com.a.a.e;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.View;
import com.a.a.c;
import com.a.a.j;
import com.a.a.j.i;
import java.util.HashMap;
import java.util.Map;

public class l implements Handler.Callback {
    private static final a i = new a() {
        /* class com.a.a.e.l.AnonymousClass1 */

        @Override // com.a.a.e.l.a
        public j a(c cVar, h hVar, m mVar) {
            return new j(cVar, hVar, mVar);
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final Map<FragmentManager, k> f930a = new HashMap();
    final Map<android.support.v4.app.FragmentManager, o> b = new HashMap();
    private volatile j c;
    private final Handler d;
    private final a e;
    private final ArrayMap<View, Fragment> f = new ArrayMap<>();
    private final ArrayMap<View, android.app.Fragment> g = new ArrayMap<>();
    private final Bundle h = new Bundle();

    public interface a {
        j a(c cVar, h hVar, m mVar);
    }

    public l(@Nullable a aVar) {
        this.e = aVar == null ? i : aVar;
        this.d = new Handler(Looper.getMainLooper(), this);
    }

    private j b(Context context) {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = this.e.a(c.a(context), new b(), new g());
                }
            }
        }
        return this.c;
    }

    public j a(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("You cannot start a load on a null Context");
        }
        if (i.b() && !(context instanceof Application)) {
            if (context instanceof FragmentActivity) {
                return a((FragmentActivity) context);
            }
            if (context instanceof Activity) {
                return a((Activity) context);
            }
            if (context instanceof ContextWrapper) {
                return a(((ContextWrapper) context).getBaseContext());
            }
        }
        return b(context);
    }

    public j a(FragmentActivity fragmentActivity) {
        if (i.c()) {
            return a(fragmentActivity.getApplicationContext());
        }
        b((Activity) fragmentActivity);
        return a(fragmentActivity, fragmentActivity.getSupportFragmentManager(), (Fragment) null);
    }

    public j a(Activity activity) {
        if (i.c()) {
            return a(activity.getApplicationContext());
        }
        b(activity);
        return a(activity, activity.getFragmentManager(), (android.app.Fragment) null);
    }

    @TargetApi(17)
    private static void b(Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(17)
    public k a(FragmentManager fragmentManager, android.app.Fragment fragment) {
        k kVar = (k) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (kVar != null) {
            return kVar;
        }
        k kVar2 = this.f930a.get(fragmentManager);
        if (kVar2 != null) {
            return kVar2;
        }
        k kVar3 = new k();
        kVar3.a(fragment);
        this.f930a.put(fragmentManager, kVar3);
        fragmentManager.beginTransaction().add(kVar3, "com.bumptech.glide.manager").commitAllowingStateLoss();
        this.d.obtainMessage(1, fragmentManager).sendToTarget();
        return kVar3;
    }

    private j a(Context context, FragmentManager fragmentManager, android.app.Fragment fragment) {
        k a2 = a(fragmentManager, fragment);
        j b2 = a2.b();
        if (b2 != null) {
            return b2;
        }
        j a3 = this.e.a(c.a(context), a2.a(), a2.c());
        a2.a(a3);
        return a3;
    }

    /* access modifiers changed from: package-private */
    public o a(android.support.v4.app.FragmentManager fragmentManager, Fragment fragment) {
        o oVar = (o) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (oVar != null) {
            return oVar;
        }
        o oVar2 = this.b.get(fragmentManager);
        if (oVar2 != null) {
            return oVar2;
        }
        o oVar3 = new o();
        oVar3.a(fragment);
        this.b.put(fragmentManager, oVar3);
        fragmentManager.beginTransaction().add(oVar3, "com.bumptech.glide.manager").commitAllowingStateLoss();
        this.d.obtainMessage(2, fragmentManager).sendToTarget();
        return oVar3;
    }

    private j a(Context context, android.support.v4.app.FragmentManager fragmentManager, Fragment fragment) {
        o a2 = a(fragmentManager, fragment);
        j b2 = a2.b();
        if (b2 != null) {
            return b2;
        }
        j a3 = this.e.a(c.a(context), a2.a(), a2.c());
        a2.a(a3);
        return a3;
    }

    public boolean handleMessage(Message message) {
        Object obj;
        k remove;
        boolean z = true;
        switch (message.what) {
            case 1:
                obj = (FragmentManager) message.obj;
                remove = this.f930a.remove(obj);
                break;
            case 2:
                obj = (android.support.v4.app.FragmentManager) message.obj;
                remove = this.b.remove(obj);
                break;
            default:
                z = false;
                obj = null;
                remove = null;
                break;
        }
        if (z && remove == null && Log.isLoggable("RMRetriever", 5)) {
            Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj);
        }
        return z;
    }
}
