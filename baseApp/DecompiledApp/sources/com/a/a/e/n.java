package com.a.a.e;

import com.a.a.h.a;
import com.a.a.j.i;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public class n {

    /* renamed from: a  reason: collision with root package name */
    private final Set<a> f931a = Collections.newSetFromMap(new WeakHashMap());
    private final List<a> b = new ArrayList();
    private boolean c;

    public void a(a aVar) {
        this.f931a.add(aVar);
        if (!this.c) {
            aVar.a();
        } else {
            this.b.add(aVar);
        }
    }

    public boolean b(a aVar) {
        boolean z = false;
        if (aVar != null) {
            boolean remove = this.f931a.remove(aVar);
            if (this.b.remove(aVar) || remove) {
                z = true;
            }
            if (z) {
                aVar.c();
                aVar.i();
            }
        }
        return z;
    }

    public void a() {
        this.c = true;
        for (a aVar : i.a(this.f931a)) {
            if (aVar.e()) {
                aVar.b();
                this.b.add(aVar);
            }
        }
    }

    public void b() {
        this.c = false;
        for (a aVar : i.a(this.f931a)) {
            if (!aVar.f() && !aVar.h() && !aVar.e()) {
                aVar.a();
            }
        }
        this.b.clear();
    }

    public void c() {
        for (a aVar : i.a(this.f931a)) {
            b(aVar);
        }
        this.b.clear();
    }

    public void d() {
        for (a aVar : i.a(this.f931a)) {
            if (!aVar.f() && !aVar.h()) {
                aVar.b();
                if (!this.c) {
                    aVar.a();
                } else {
                    this.b.add(aVar);
                }
            }
        }
    }

    public String toString() {
        return super.toString() + "{numRequests=" + this.f931a.size() + ", isPaused=" + this.c + "}";
    }
}
