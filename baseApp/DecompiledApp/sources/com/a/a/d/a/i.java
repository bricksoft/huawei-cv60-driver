package com.a.a.d.a;

import com.a.a.d.a.c;
import com.a.a.d.b.a.b;
import com.a.a.d.d.a.p;
import java.io.InputStream;

public final class i implements c<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final p f764a;

    i(InputStream inputStream, b bVar) {
        this.f764a = new p(inputStream, bVar);
        this.f764a.mark(5242880);
    }

    /* renamed from: c */
    public InputStream a() {
        this.f764a.reset();
        return this.f764a;
    }

    @Override // com.a.a.d.a.c
    public void b() {
        this.f764a.b();
    }

    public static final class a implements c.a<InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final b f765a;

        public a(b bVar) {
            this.f765a = bVar;
        }

        public c<InputStream> a(InputStream inputStream) {
            return new i(inputStream, this.f765a);
        }

        @Override // com.a.a.d.a.c.a
        public Class<InputStream> a() {
            return InputStream.class;
        }
    }
}
