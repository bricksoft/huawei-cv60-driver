package com.a.a.d.a;

import com.a.a.d.a.c;
import com.a.a.j.h;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class d {
    private static final c.a<?> b = new c.a<Object>() {
        /* class com.a.a.d.a.d.AnonymousClass1 */

        @Override // com.a.a.d.a.c.a
        public c<Object> a(Object obj) {
            return new a(obj);
        }

        @Override // com.a.a.d.a.c.a
        public Class<Object> a() {
            throw new UnsupportedOperationException("Not implemented");
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Map<Class<?>, c.a<?>> f760a = new HashMap();

    public synchronized void a(c.a<?> aVar) {
        this.f760a.put(aVar.a(), aVar);
    }

    public synchronized <T> c<T> a(T t) {
        c.a<?> aVar;
        h.a((Object) t);
        aVar = this.f760a.get(t.getClass());
        if (aVar == null) {
            Iterator<c.a<?>> it = this.f760a.values().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                c.a<?> next = it.next();
                if (next.a().isAssignableFrom(t.getClass())) {
                    aVar = next;
                    break;
                }
            }
        }
        if (aVar == null) {
            aVar = b;
        }
        return (c<T>) aVar.a(t);
    }

    private static class a implements c<Object> {

        /* renamed from: a  reason: collision with root package name */
        private final Object f761a;

        public a(Object obj) {
            this.f761a = obj;
        }

        @Override // com.a.a.d.a.c
        public Object a() {
            return this.f761a;
        }

        @Override // com.a.a.d.a.c
        public void b() {
        }
    }
}
