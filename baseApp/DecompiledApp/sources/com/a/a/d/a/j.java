package com.a.a.d.a;

import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import com.a.a.d.a;
import com.a.a.d.a.b;
import com.a.a.g;
import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class j<T> implements b<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Uri f766a;
    private final ContentResolver b;
    private T c;

    /* access modifiers changed from: protected */
    public abstract void a(T t);

    /* access modifiers changed from: protected */
    public abstract T b(Uri uri, ContentResolver contentResolver);

    public j(ContentResolver contentResolver, Uri uri) {
        this.b = contentResolver;
        this.f766a = uri;
    }

    @Override // com.a.a.d.a.b
    public final void a(g gVar, b.a<? super T> aVar) {
        try {
            this.c = b(this.f766a, this.b);
            aVar.a((Object) this.c);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("LocalUriFetcher", 3)) {
                Log.d("LocalUriFetcher", "Failed to open Uri", e);
            }
            aVar.a((Exception) e);
        }
    }

    @Override // com.a.a.d.a.b
    public void a() {
        if (this.c != null) {
            try {
                a(this.c);
            } catch (IOException e) {
            }
        }
    }

    @Override // com.a.a.d.a.b
    public void b() {
    }

    @Override // com.a.a.d.a.b
    @NonNull
    public a c() {
        return a.LOCAL;
    }
}
