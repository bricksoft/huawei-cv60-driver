package com.a.a.d.a;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class l extends j<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private static final UriMatcher f767a = new UriMatcher(-1);

    static {
        f767a.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        f767a.addURI("com.android.contacts", "contacts/lookup/*", 1);
        f767a.addURI("com.android.contacts", "contacts/#/photo", 2);
        f767a.addURI("com.android.contacts", "contacts/#", 3);
        f767a.addURI("com.android.contacts", "contacts/#/display_photo", 4);
        f767a.addURI("com.android.contacts", "phone_lookup/*", 5);
    }

    public l(ContentResolver contentResolver, Uri uri) {
        super(contentResolver, uri);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public InputStream b(Uri uri, ContentResolver contentResolver) {
        InputStream c = c(uri, contentResolver);
        if (c != null) {
            return c;
        }
        throw new FileNotFoundException("InputStream is null for " + uri);
    }

    private InputStream c(Uri uri, ContentResolver contentResolver) {
        switch (f767a.match(uri)) {
            case 1:
            case 5:
                Uri lookupContact = ContactsContract.Contacts.lookupContact(contentResolver, uri);
                if (lookupContact != null) {
                    return a(contentResolver, lookupContact);
                }
                throw new FileNotFoundException("Contact cannot be found");
            case 2:
            case 4:
            default:
                return contentResolver.openInputStream(uri);
            case 3:
                return a(contentResolver, uri);
        }
    }

    @TargetApi(14)
    private InputStream a(ContentResolver contentResolver, Uri uri) {
        return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
    }

    /* access modifiers changed from: protected */
    public void a(InputStream inputStream) {
        inputStream.close();
    }

    @Override // com.a.a.d.a.b
    @NonNull
    public Class<InputStream> d() {
        return InputStream.class;
    }
}
