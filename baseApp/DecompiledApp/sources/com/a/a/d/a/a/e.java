package com.a.a.d.a.a;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.a.a.d.b.a.b;
import com.a.a.d.f;
import com.a.a.d.g;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

class e {

    /* renamed from: a  reason: collision with root package name */
    private static final a f759a = new a();
    private final a b;
    private final d c;
    private final b d;
    private final ContentResolver e;
    private final List<f> f;

    public e(List<f> list, d dVar, b bVar, ContentResolver contentResolver) {
        this(list, f759a, dVar, bVar, contentResolver);
    }

    public e(List<f> list, a aVar, d dVar, b bVar, ContentResolver contentResolver) {
        this.b = aVar;
        this.c = dVar;
        this.d = bVar;
        this.e = contentResolver;
        this.f = list;
    }

    public int a(Uri uri) {
        InputStream inputStream = null;
        try {
            inputStream = this.e.openInputStream(uri);
            int b2 = g.b(this.f, inputStream, this.d);
            if (inputStream == null) {
                return b2;
            }
            try {
                inputStream.close();
                return b2;
            } catch (IOException e2) {
                return b2;
            }
        } catch (IOException | NullPointerException e3) {
            if (Log.isLoggable("ThumbStreamOpener", 3)) {
                Log.d("ThumbStreamOpener", "Failed to open uri: " + uri, e3);
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                }
            }
            return -1;
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                }
            }
            throw th;
        }
    }

    public InputStream b(Uri uri) {
        Uri uri2;
        Cursor a2 = this.c.a(uri);
        if (a2 != null) {
            try {
                if (a2.moveToFirst()) {
                    String string = a2.getString(0);
                    if (TextUtils.isEmpty(string)) {
                    }
                    File a3 = this.b.a(string);
                    if (!this.b.a(a3) || this.b.b(a3) <= 0) {
                        uri2 = null;
                    } else {
                        uri2 = Uri.fromFile(a3);
                    }
                    if (a2 != null) {
                        a2.close();
                    }
                    if (uri2 == null) {
                        return null;
                    }
                    try {
                        return this.e.openInputStream(uri2);
                    } catch (NullPointerException e2) {
                        throw ((FileNotFoundException) new FileNotFoundException("NPE opening uri: " + uri2).initCause(e2));
                    }
                }
            } finally {
                if (a2 != null) {
                    a2.close();
                }
            }
        }
        if (a2 == null) {
            return null;
        }
        a2.close();
        return null;
    }
}
