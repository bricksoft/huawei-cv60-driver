package com.a.a.d.a.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import com.a.a.d.a.b;
import com.a.a.d.a.e;
import com.a.a.g;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class c implements com.a.a.d.a.b<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final Uri f756a;
    private final e b;
    private InputStream c;

    public static c a(Context context, Uri uri) {
        return a(context, uri, new a(context.getContentResolver()));
    }

    public static c b(Context context, Uri uri) {
        return a(context, uri, new b(context.getContentResolver()));
    }

    private static c a(Context context, Uri uri, d dVar) {
        return new c(uri, new e(com.a.a.c.a(context).h().a(), dVar, com.a.a.c.a(context).b(), context.getContentResolver()));
    }

    c(Uri uri, e eVar) {
        this.f756a = uri;
        this.b = eVar;
    }

    @Override // com.a.a.d.a.b
    public void a(g gVar, b.a<? super InputStream> aVar) {
        try {
            this.c = e();
            aVar.a(this.c);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e);
            }
            aVar.a((Exception) e);
        }
    }

    private InputStream e() {
        int i;
        InputStream b2 = this.b.b(this.f756a);
        if (b2 != null) {
            i = this.b.a(this.f756a);
        } else {
            i = -1;
        }
        if (i != -1) {
            return new e(b2, i);
        }
        return b2;
    }

    @Override // com.a.a.d.a.b
    public void a() {
        if (this.c != null) {
            try {
                this.c.close();
            } catch (IOException e) {
            }
        }
    }

    @Override // com.a.a.d.a.b
    public void b() {
    }

    @Override // com.a.a.d.a.b
    @NonNull
    public Class<InputStream> d() {
        return InputStream.class;
    }

    @Override // com.a.a.d.a.b
    @NonNull
    public com.a.a.d.a c() {
        return com.a.a.d.a.LOCAL;
    }

    /* access modifiers changed from: package-private */
    public static class b implements d {
        private static final String[] b = {"_data"};

        /* renamed from: a  reason: collision with root package name */
        private final ContentResolver f758a;

        b(ContentResolver contentResolver) {
            this.f758a = contentResolver;
        }

        @Override // com.a.a.d.a.a.d
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.f758a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    /* access modifiers changed from: package-private */
    public static class a implements d {
        private static final String[] b = {"_data"};

        /* renamed from: a  reason: collision with root package name */
        private final ContentResolver f757a;

        a(ContentResolver contentResolver) {
            this.f757a = contentResolver;
        }

        @Override // com.a.a.d.a.a.d
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.f757a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{lastPathSegment}, null);
        }
    }
}
