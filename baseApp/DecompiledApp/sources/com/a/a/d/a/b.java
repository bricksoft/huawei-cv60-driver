package com.a.a.d.a;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.a.a.g;

public interface b<T> {

    public interface a<T> {
        void a(Exception exc);

        void a(@Nullable T t);
    }

    void a();

    void a(g gVar, a<? super T> aVar);

    void b();

    @NonNull
    com.a.a.d.a c();

    @NonNull
    Class<T> d();
}
