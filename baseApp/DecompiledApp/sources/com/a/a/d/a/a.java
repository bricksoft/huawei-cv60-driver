package com.a.a.d.a;

import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import android.util.Log;
import com.a.a.d.a.b;
import com.a.a.g;
import java.io.IOException;

public abstract class a<T> implements b<T> {

    /* renamed from: a  reason: collision with root package name */
    private final String f755a;
    private final AssetManager b;
    private T c;

    /* access modifiers changed from: protected */
    public abstract T a(AssetManager assetManager, String str);

    /* access modifiers changed from: protected */
    public abstract void a(T t);

    public a(AssetManager assetManager, String str) {
        this.b = assetManager;
        this.f755a = str;
    }

    @Override // com.a.a.d.a.b
    public void a(g gVar, b.a<? super T> aVar) {
        try {
            this.c = a(this.b, this.f755a);
            aVar.a((Object) this.c);
        } catch (IOException e) {
            if (Log.isLoggable("AssetPathFetcher", 3)) {
                Log.d("AssetPathFetcher", "Failed to load data from asset manager", e);
            }
            aVar.a((Exception) e);
        }
    }

    @Override // com.a.a.d.a.b
    public void a() {
        if (this.c != null) {
            try {
                a(this.c);
            } catch (IOException e) {
            }
        }
    }

    @Override // com.a.a.d.a.b
    public void b() {
    }

    @Override // com.a.a.d.a.b
    @NonNull
    public com.a.a.d.a c() {
        return com.a.a.d.a.LOCAL;
    }
}
