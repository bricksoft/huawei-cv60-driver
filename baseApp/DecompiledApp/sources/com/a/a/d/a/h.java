package com.a.a.d.a;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.a.a.d.a.b;
import com.a.a.d.c.g;
import com.a.a.d.e;
import com.a.a.j.d;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

public class h implements b<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    static final b f763a = new a();
    private final g b;
    private final int c;
    private final b d;
    private HttpURLConnection e;
    private InputStream f;
    private volatile boolean g;

    /* access modifiers changed from: package-private */
    public interface b {
        HttpURLConnection a(URL url);
    }

    public h(g gVar, int i) {
        this(gVar, i, f763a);
    }

    h(g gVar, int i, b bVar) {
        this.b = gVar;
        this.c = i;
        this.d = bVar;
    }

    @Override // com.a.a.d.a.b
    public void a(com.a.a.g gVar, b.a<? super InputStream> aVar) {
        long a2 = d.a();
        try {
            InputStream a3 = a(this.b.a(), 0, null, this.b.b());
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                Log.v("HttpUrlFetcher", "Finished http url fetcher fetch in " + d.a(a2) + " ms and loaded " + a3);
            }
            aVar.a(a3);
        } catch (IOException e2) {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Failed to load data for url", e2);
            }
            aVar.a((Exception) e2);
        }
    }

    private InputStream a(URL url, int i, URL url2, Map<String, String> map) {
        if (i >= 5) {
            throw new e("Too many (> 5) redirects!");
        }
        if (url2 != null) {
            try {
                if (url.toURI().equals(url2.toURI())) {
                    throw new e("In re-direct loop");
                }
            } catch (URISyntaxException e2) {
            }
        }
        this.e = this.d.a(url);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            this.e.addRequestProperty(entry.getKey(), entry.getValue());
        }
        this.e.setConnectTimeout(this.c);
        this.e.setReadTimeout(this.c);
        this.e.setUseCaches(false);
        this.e.setDoInput(true);
        this.e.setInstanceFollowRedirects(false);
        this.e.connect();
        this.f = this.e.getInputStream();
        if (this.g) {
            return null;
        }
        int responseCode = this.e.getResponseCode();
        if (responseCode / 100 == 2) {
            return a(this.e);
        }
        if (responseCode / 100 == 3) {
            String headerField = this.e.getHeaderField("Location");
            if (TextUtils.isEmpty(headerField)) {
                throw new e("Received empty or null redirect url");
            }
            URL url3 = new URL(url, headerField);
            a();
            return a(url3, i + 1, url, map);
        } else if (responseCode == -1) {
            throw new e(responseCode);
        } else {
            throw new e(this.e.getResponseMessage(), responseCode);
        }
    }

    private InputStream a(HttpURLConnection httpURLConnection) {
        if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
            this.f = com.a.a.j.b.a(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
        } else {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Got non empty content encoding: " + httpURLConnection.getContentEncoding());
            }
            this.f = httpURLConnection.getInputStream();
        }
        return this.f;
    }

    @Override // com.a.a.d.a.b
    public void a() {
        if (this.f != null) {
            try {
                this.f.close();
            } catch (IOException e2) {
            }
        }
        if (this.e != null) {
            this.e.disconnect();
        }
        this.e = null;
    }

    @Override // com.a.a.d.a.b
    public void b() {
        this.g = true;
    }

    @Override // com.a.a.d.a.b
    @NonNull
    public Class<InputStream> d() {
        return InputStream.class;
    }

    @Override // com.a.a.d.a.b
    @NonNull
    public com.a.a.d.a c() {
        return com.a.a.d.a.REMOTE;
    }

    private static class a implements b {
        a() {
        }

        @Override // com.a.a.d.a.h.b
        public HttpURLConnection a(URL url) {
            return (HttpURLConnection) url.openConnection();
        }
    }
}
