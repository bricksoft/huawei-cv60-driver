package com.a.a.d.a;

import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import java.io.InputStream;

public class k extends a<InputStream> {
    public k(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public InputStream a(AssetManager assetManager, String str) {
        return assetManager.open(str);
    }

    /* access modifiers changed from: protected */
    public void a(InputStream inputStream) {
        inputStream.close();
    }

    @Override // com.a.a.d.a.b
    @NonNull
    public Class<InputStream> d() {
        return InputStream.class;
    }
}
