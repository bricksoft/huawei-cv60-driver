package com.a.a.d.b;

import android.os.Looper;
import com.a.a.d.h;

/* access modifiers changed from: package-private */
public class n<Z> implements s<Z> {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f821a;
    private a b;
    private h c;
    private int d;
    private boolean e;
    private final s<Z> f;

    /* access modifiers changed from: package-private */
    public interface a {
        void b(h hVar, n<?> nVar);
    }

    n(s<Z> sVar, boolean z) {
        this.f = (s) com.a.a.j.h.a(sVar);
        this.f821a = z;
    }

    /* access modifiers changed from: package-private */
    public void a(h hVar, a aVar) {
        this.c = hVar;
        this.b = aVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.f821a;
    }

    @Override // com.a.a.d.b.s
    public Class<Z> b() {
        return this.f.b();
    }

    @Override // com.a.a.d.b.s
    public Z c() {
        return this.f.c();
    }

    @Override // com.a.a.d.b.s
    public int d() {
        return this.f.d();
    }

    @Override // com.a.a.d.b.s
    public void e() {
        if (this.d > 0) {
            throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
        } else if (this.e) {
            throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
        } else {
            this.e = true;
            this.f.e();
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.e) {
            throw new IllegalStateException("Cannot acquire a recycled resource");
        } else if (!Looper.getMainLooper().equals(Looper.myLooper())) {
            throw new IllegalThreadStateException("Must call acquire on the main thread");
        } else {
            this.d++;
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (this.d <= 0) {
            throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
        } else if (!Looper.getMainLooper().equals(Looper.myLooper())) {
            throw new IllegalThreadStateException("Must call release on the main thread");
        } else {
            int i = this.d - 1;
            this.d = i;
            if (i == 0) {
                this.b.b(this.c, this);
            }
        }
    }

    public String toString() {
        return "EngineResource{isCacheable=" + this.f821a + ", listener=" + this.b + ", key=" + this.c + ", acquired=" + this.d + ", isRecycled=" + this.e + ", resource=" + this.f + '}';
    }
}
