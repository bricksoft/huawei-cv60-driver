package com.a.a.d.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.a.a.j.i;

/* access modifiers changed from: package-private */
public class v {

    /* renamed from: a  reason: collision with root package name */
    private boolean f827a;
    private final Handler b = new Handler(Looper.getMainLooper(), new a());

    v() {
    }

    public void a(s<?> sVar) {
        i.a();
        if (this.f827a) {
            this.b.obtainMessage(1, sVar).sendToTarget();
            return;
        }
        this.f827a = true;
        sVar.e();
        this.f827a = false;
    }

    private static class a implements Handler.Callback {
        a() {
        }

        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((s) message.obj).e();
            return true;
        }
    }
}
