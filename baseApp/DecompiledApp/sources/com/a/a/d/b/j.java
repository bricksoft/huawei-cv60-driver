package com.a.a.d.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.util.Pools;
import com.a.a.d.b.f;
import com.a.a.d.h;
import com.a.a.h.e;
import com.a.a.j.a.a;
import com.a.a.j.i;
import java.util.ArrayList;
import java.util.List;

/* access modifiers changed from: package-private */
public class j<R> implements f.a<R>, a.c {

    /* renamed from: a  reason: collision with root package name */
    private static final a f820a = new a();
    private static final Handler b = new Handler(Looper.getMainLooper(), new b());
    private final List<e> c;
    private final com.a.a.j.a.b d;
    private final Pools.Pool<j<?>> e;
    private final a f;
    private final k g;
    private final com.a.a.d.b.c.a h;
    private final com.a.a.d.b.c.a i;
    private final com.a.a.d.b.c.a j;
    private h k;
    private boolean l;
    private boolean m;
    private s<?> n;
    private com.a.a.d.a o;
    private boolean p;
    private o q;
    private boolean r;
    private List<e> s;
    private n<?> t;
    private f<R> u;
    private volatile boolean v;

    j(com.a.a.d.b.c.a aVar, com.a.a.d.b.c.a aVar2, com.a.a.d.b.c.a aVar3, k kVar, Pools.Pool<j<?>> pool) {
        this(aVar, aVar2, aVar3, kVar, pool, f820a);
    }

    j(com.a.a.d.b.c.a aVar, com.a.a.d.b.c.a aVar2, com.a.a.d.b.c.a aVar3, k kVar, Pools.Pool<j<?>> pool, a aVar4) {
        this.c = new ArrayList(2);
        this.d = com.a.a.j.a.b.a();
        this.h = aVar;
        this.i = aVar2;
        this.j = aVar3;
        this.g = kVar;
        this.e = pool;
        this.f = aVar4;
    }

    /* access modifiers changed from: package-private */
    public j<R> a(h hVar, boolean z, boolean z2) {
        this.k = hVar;
        this.l = z;
        this.m = z2;
        return this;
    }

    public void b(f<R> fVar) {
        com.a.a.d.b.c.a f2;
        this.u = fVar;
        if (fVar.a()) {
            f2 = this.h;
        } else {
            f2 = f();
        }
        f2.execute(fVar);
    }

    public void a(e eVar) {
        i.a();
        this.d.b();
        if (this.p) {
            eVar.a(this.t, this.o);
        } else if (this.r) {
            eVar.a(this.q);
        } else {
            this.c.add(eVar);
        }
    }

    public void b(e eVar) {
        i.a();
        this.d.b();
        if (this.p || this.r) {
            c(eVar);
            return;
        }
        this.c.remove(eVar);
        if (this.c.isEmpty()) {
            a();
        }
    }

    private com.a.a.d.b.c.a f() {
        return this.m ? this.j : this.i;
    }

    private void c(e eVar) {
        if (this.s == null) {
            this.s = new ArrayList(2);
        }
        if (!this.s.contains(eVar)) {
            this.s.add(eVar);
        }
    }

    private boolean d(e eVar) {
        return this.s != null && this.s.contains(eVar);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (!this.r && !this.p && !this.v) {
            this.v = true;
            this.u.b();
            this.g.a(this, this.k);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.d.b();
        if (this.v) {
            this.n.e();
            a(false);
        } else if (this.c.isEmpty()) {
            throw new IllegalStateException("Received a resource without any callbacks to notify");
        } else if (this.p) {
            throw new IllegalStateException("Already have resource");
        } else {
            this.t = this.f.a(this.n, this.l);
            this.p = true;
            this.t.f();
            this.g.a(this.k, this.t);
            for (e eVar : this.c) {
                if (!d(eVar)) {
                    this.t.f();
                    eVar.a(this.t, this.o);
                }
            }
            this.t.g();
            a(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.d.b();
        if (!this.v) {
            throw new IllegalStateException("Not cancelled");
        }
        this.g.a(this, this.k);
        a(false);
    }

    private void a(boolean z) {
        i.a();
        this.c.clear();
        this.k = null;
        this.t = null;
        this.n = null;
        if (this.s != null) {
            this.s.clear();
        }
        this.r = false;
        this.v = false;
        this.p = false;
        this.u.a(z);
        this.u = null;
        this.q = null;
        this.o = null;
        this.e.release(this);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.a.a.d.b.s<R> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.a.a.d.b.f.a
    public void a(s<R> sVar, com.a.a.d.a aVar) {
        this.n = sVar;
        this.o = aVar;
        b.obtainMessage(1, this).sendToTarget();
    }

    @Override // com.a.a.d.b.f.a
    public void a(o oVar) {
        this.q = oVar;
        b.obtainMessage(2, this).sendToTarget();
    }

    @Override // com.a.a.d.b.f.a
    public void a(f<?> fVar) {
        f().execute(fVar);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.d.b();
        if (this.v) {
            a(false);
        } else if (this.c.isEmpty()) {
            throw new IllegalStateException("Received an exception without any callbacks to notify");
        } else if (this.r) {
            throw new IllegalStateException("Already failed once");
        } else {
            this.r = true;
            this.g.a(this.k, (n<?>) null);
            for (e eVar : this.c) {
                if (!d(eVar)) {
                    eVar.a(this.q);
                }
            }
            a(false);
        }
    }

    @Override // com.a.a.j.a.a.c
    public com.a.a.j.a.b a_() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public static class a {
        a() {
        }

        public <R> n<R> a(s<R> sVar, boolean z) {
            return new n<>(sVar, z);
        }
    }

    private static class b implements Handler.Callback {
        b() {
        }

        public boolean handleMessage(Message message) {
            j jVar = (j) message.obj;
            switch (message.what) {
                case 1:
                    jVar.b();
                    return true;
                case 2:
                    jVar.e();
                    return true;
                case 3:
                    jVar.c();
                    return true;
                default:
                    throw new IllegalStateException("Unrecognized message: " + message.what);
            }
        }
    }
}
