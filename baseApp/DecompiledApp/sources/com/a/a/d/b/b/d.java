package com.a.a.d.b.b;

import com.a.a.d.b.b.a;
import java.io.File;

public class d implements a.AbstractC0028a {

    /* renamed from: a  reason: collision with root package name */
    private final int f784a;
    private final a b;

    public interface a {
        File a();
    }

    public d(a aVar, int i) {
        this.f784a = i;
        this.b = aVar;
    }

    @Override // com.a.a.d.b.b.a.AbstractC0028a
    public a a() {
        File a2 = this.b.a();
        if (a2 == null) {
            return null;
        }
        if (a2.mkdirs() || (a2.exists() && a2.isDirectory())) {
            return e.a(a2, this.f784a);
        }
        return null;
    }
}
