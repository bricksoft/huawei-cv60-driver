package com.a.a.d.b;

import android.util.Log;
import com.a.a.d.a;
import com.a.a.d.a.b;
import com.a.a.d.b.d;
import com.a.a.d.c.n;
import com.a.a.d.h;
import java.util.Collections;
import java.util.List;

/* access modifiers changed from: package-private */
public class w implements b.a<Object>, d, d.a {

    /* renamed from: a  reason: collision with root package name */
    private final e<?> f828a;
    private final d.a b;
    private int c;
    private a d;
    private Object e;
    private volatile n.a<?> f;
    private b g;

    public w(e<?> eVar, d.a aVar) {
        this.f828a = eVar;
        this.b = aVar;
    }

    @Override // com.a.a.d.b.d
    public boolean a() {
        if (this.e != null) {
            Object obj = this.e;
            this.e = null;
            b(obj);
        }
        if (this.d != null && this.d.a()) {
            return true;
        }
        this.d = null;
        this.f = null;
        boolean z = false;
        while (!z && d()) {
            List<n.a<?>> k = this.f828a.k();
            int i = this.c;
            this.c = i + 1;
            this.f = k.get(i);
            if (this.f != null && (this.f828a.c().a(this.f.c.c()) || this.f828a.a((Class<?>) this.f.c.d()))) {
                this.f.c.a(this.f828a.d(), this);
                z = true;
            }
        }
        return z;
    }

    private boolean d() {
        return this.c < this.f828a.k().size();
    }

    /* JADX INFO: finally extract failed */
    private void b(Object obj) {
        long a2 = com.a.a.j.d.a();
        try {
            com.a.a.d.d<X> a3 = this.f828a.a(obj);
            c cVar = new c(a3, obj, this.f828a.e());
            this.g = new b(this.f.f862a, this.f828a.f());
            this.f828a.b().a(this.g, cVar);
            if (Log.isLoggable("SourceGenerator", 2)) {
                Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.g + ", data: " + obj + ", encoder: " + a3 + ", duration: " + com.a.a.j.d.a(a2));
            }
            this.f.c.a();
            this.d = new a(Collections.singletonList(this.f.f862a), this.f828a, this);
        } catch (Throwable th) {
            this.f.c.a();
            throw th;
        }
    }

    @Override // com.a.a.d.b.d
    public void b() {
        n.a<?> aVar = this.f;
        if (aVar != null) {
            aVar.c.b();
        }
    }

    @Override // com.a.a.d.a.b.a
    public void a(Object obj) {
        h c2 = this.f828a.c();
        if (obj == null || !c2.a(this.f.c.c())) {
            this.b.a(this.f.f862a, obj, this.f.c, this.f.c.c(), this.g);
            return;
        }
        this.e = obj;
        this.b.c();
    }

    @Override // com.a.a.d.a.b.a
    public void a(Exception exc) {
        this.b.a(this.g, exc, this.f.c, this.f.c.c());
    }

    @Override // com.a.a.d.b.d.a
    public void c() {
        throw new UnsupportedOperationException();
    }

    @Override // com.a.a.d.b.d.a
    public void a(h hVar, Object obj, b<?> bVar, a aVar, h hVar2) {
        this.b.a(hVar, obj, bVar, this.f.c.c(), hVar);
    }

    @Override // com.a.a.d.b.d.a
    public void a(h hVar, Exception exc, b<?> bVar, a aVar) {
        this.b.a(hVar, exc, bVar, this.f.c.c());
    }
}
