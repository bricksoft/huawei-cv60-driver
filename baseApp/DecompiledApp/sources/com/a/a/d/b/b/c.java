package com.a.a.d.b.b;

import com.a.a.j.h;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class c {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, a> f781a = new HashMap();
    private final b b = new b();

    c() {
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        a aVar;
        synchronized (this) {
            aVar = this.f781a.get(str);
            if (aVar == null) {
                aVar = this.b.a();
                this.f781a.put(str, aVar);
            }
            aVar.b++;
        }
        aVar.f782a.lock();
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        a aVar;
        synchronized (this) {
            aVar = (a) h.a(this.f781a.get(str));
            if (aVar.b < 1) {
                throw new IllegalStateException("Cannot release a lock that is not held, safeKey: " + str + ", interestedThreads: " + aVar.b);
            }
            aVar.b--;
            if (aVar.b == 0) {
                a remove = this.f781a.remove(str);
                if (!remove.equals(aVar)) {
                    throw new IllegalStateException("Removed the wrong lock, expected to remove: " + aVar + ", but actually removed: " + remove + ", safeKey: " + str);
                }
                this.b.a(remove);
            }
        }
        aVar.f782a.unlock();
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final Lock f782a = new ReentrantLock();
        int b;

        a() {
        }
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        private final Queue<a> f783a = new ArrayDeque();

        b() {
        }

        /* access modifiers changed from: package-private */
        public a a() {
            a poll;
            synchronized (this.f783a) {
                poll = this.f783a.poll();
            }
            if (poll == null) {
                return new a();
            }
            return poll;
        }

        /* access modifiers changed from: package-private */
        public void a(a aVar) {
            synchronized (this.f783a) {
                if (this.f783a.size() < 10) {
                    this.f783a.offer(aVar);
                }
            }
        }
    }
}
