package com.a.a.d.b;

import android.support.v4.util.Pools;
import android.util.Log;
import com.a.a.d.a.c;
import com.a.a.d.d.f.d;
import com.a.a.d.j;
import com.a.a.d.k;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class g<DataType, ResourceType, Transcode> {

    /* renamed from: a  reason: collision with root package name */
    private final Class<DataType> f809a;
    private final List<? extends k<DataType, ResourceType>> b;
    private final d<ResourceType, Transcode> c;
    private final Pools.Pool<List<Exception>> d;
    private final String e;

    interface a<ResourceType> {
        s<ResourceType> a(s<ResourceType> sVar);
    }

    public g(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends k<DataType, ResourceType>> list, d<ResourceType, Transcode> dVar, Pools.Pool<List<Exception>> pool) {
        this.f809a = cls;
        this.b = list;
        this.c = dVar;
        this.d = pool;
        this.e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    public s<Transcode> a(c<DataType> cVar, int i, int i2, j jVar, a<ResourceType> aVar) {
        return this.c.a(aVar.a(a(cVar, i, i2, jVar)), jVar);
    }

    private s<ResourceType> a(c<DataType> cVar, int i, int i2, j jVar) {
        List<Exception> acquire = this.d.acquire();
        try {
            return a(cVar, i, i2, jVar, acquire);
        } finally {
            this.d.release(acquire);
        }
    }

    private s<ResourceType> a(c<DataType> cVar, int i, int i2, j jVar, List<Exception> list) {
        s<ResourceType> sVar;
        s<ResourceType> sVar2 = null;
        int size = this.b.size();
        for (int i3 = 0; i3 < size; i3++) {
            k kVar = (k) this.b.get(i3);
            try {
                if (kVar.a(cVar.a(), jVar)) {
                    sVar = kVar.a(cVar.a(), i, i2, jVar);
                } else {
                    sVar = sVar2;
                }
                sVar2 = sVar;
            } catch (IOException | RuntimeException e2) {
                if (Log.isLoggable("DecodePath", 2)) {
                    Log.v("DecodePath", "Failed to decode data for " + kVar, e2);
                }
                list.add(e2);
            }
            if (sVar2 != null) {
                break;
            }
        }
        if (sVar2 != null) {
            return sVar2;
        }
        throw new o(this.e, new ArrayList(list));
    }

    public String toString() {
        return "DecodePath{ dataClass=" + this.f809a + ", decoders=" + this.b + ", transcoder=" + this.c + '}';
    }
}
