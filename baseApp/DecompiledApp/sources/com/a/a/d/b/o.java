package com.a.a.d.b;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.a.a.d.h;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class o extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private static final StackTraceElement[] f822a = new StackTraceElement[0];
    private final List<Exception> b;
    private h c;
    private com.a.a.d.a d;
    private Class<?> e;

    public o(String str) {
        this(str, Collections.emptyList());
    }

    public o(String str, Exception exc) {
        this(str, Collections.singletonList(exc));
    }

    public o(String str, List<Exception> list) {
        super(str);
        setStackTrace(f822a);
        this.b = list;
    }

    /* access modifiers changed from: package-private */
    public void a(h hVar, com.a.a.d.a aVar) {
        a(hVar, aVar, null);
    }

    /* access modifiers changed from: package-private */
    public void a(h hVar, com.a.a.d.a aVar, Class<?> cls) {
        this.c = hVar;
        this.d = aVar;
        this.e = cls;
    }

    public Throwable fillInStackTrace() {
        return this;
    }

    public List<Exception> a() {
        return this.b;
    }

    public List<Exception> b() {
        ArrayList arrayList = new ArrayList();
        a(this, arrayList);
        return arrayList;
    }

    public void a(String str) {
        List<Exception> b2 = b();
        int size = b2.size();
        for (int i = 0; i < size; i++) {
            Log.i(str, "Root cause (" + (i + 1) + " of " + size + ")", b2.get(i));
        }
    }

    private void a(Exception exc, List<Exception> list) {
        if (exc instanceof o) {
            for (Exception exc2 : ((o) exc).a()) {
                a(exc2, list);
            }
            return;
        }
        list.add(exc);
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    @Override // java.lang.Throwable
    public void printStackTrace(PrintStream printStream) {
        a(printStream);
    }

    @Override // java.lang.Throwable
    public void printStackTrace(PrintWriter printWriter) {
        a(printWriter);
    }

    private void a(Appendable appendable) {
        a(this, appendable);
        a(a(), new a(appendable));
    }

    public String getMessage() {
        return super.getMessage() + (this.e != null ? ", " + this.e : "") + (this.d != null ? ", " + this.d : "") + (this.c != null ? ", " + this.c : "");
    }

    private static void a(Exception exc, Appendable appendable) {
        try {
            appendable.append(exc.getClass().toString()).append(": ").append(exc.getMessage()).append('\n');
        } catch (IOException e2) {
            throw new RuntimeException(exc);
        }
    }

    private static void a(List<Exception> list, Appendable appendable) {
        try {
            b(list, appendable);
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    private static void b(List<Exception> list, Appendable appendable) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            appendable.append("Cause (").append(String.valueOf(i + 1)).append(" of ").append(String.valueOf(size)).append("): ");
            Exception exc = list.get(i);
            if (exc instanceof o) {
                ((o) exc).a(appendable);
            } else {
                a(exc, appendable);
            }
        }
    }

    /* access modifiers changed from: private */
    public static final class a implements Appendable {

        /* renamed from: a  reason: collision with root package name */
        private final Appendable f823a;
        private boolean b = true;

        a(Appendable appendable) {
            this.f823a = appendable;
        }

        @Override // java.lang.Appendable
        public Appendable append(char c) {
            boolean z = false;
            if (this.b) {
                this.b = false;
                this.f823a.append("  ");
            }
            if (c == '\n') {
                z = true;
            }
            this.b = z;
            this.f823a.append(c);
            return this;
        }

        @Override // java.lang.Appendable
        public Appendable append(@Nullable CharSequence charSequence) {
            CharSequence a2 = a(charSequence);
            return append(a2, 0, a2.length());
        }

        @Override // java.lang.Appendable
        public Appendable append(@Nullable CharSequence charSequence, int i, int i2) {
            boolean z = false;
            CharSequence a2 = a(charSequence);
            if (this.b) {
                this.b = false;
                this.f823a.append("  ");
            }
            if (a2.length() > 0 && a2.charAt(i2 - 1) == '\n') {
                z = true;
            }
            this.b = z;
            this.f823a.append(a2, i, i2);
            return this;
        }

        @NonNull
        private CharSequence a(@Nullable CharSequence charSequence) {
            if (charSequence == null) {
                return "";
            }
            return charSequence;
        }
    }
}
