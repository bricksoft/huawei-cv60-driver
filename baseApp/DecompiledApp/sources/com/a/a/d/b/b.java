package com.a.a.d.b;

import com.a.a.d.h;
import java.security.MessageDigest;

final class b implements h {
    private final h b;
    private final h c;

    public b(h hVar, h hVar2) {
        this.b = hVar;
        this.c = hVar2;
    }

    @Override // com.a.a.d.h
    public boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (!this.b.equals(bVar.b) || !this.c.equals(bVar.c)) {
            return false;
        }
        return true;
    }

    @Override // com.a.a.d.h
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    public String toString() {
        return "DataCacheKey{sourceKey=" + this.b + ", signature=" + this.c + '}';
    }

    @Override // com.a.a.d.h
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
        this.c.a(messageDigest);
    }
}
