package com.a.a.d.b.a;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;

/* access modifiers changed from: package-private */
public interface l {
    @Nullable
    Bitmap a();

    @Nullable
    Bitmap a(int i, int i2, Bitmap.Config config);

    void a(Bitmap bitmap);

    String b(int i, int i2, Bitmap.Config config);

    String b(Bitmap bitmap);

    int c(Bitmap bitmap);
}
