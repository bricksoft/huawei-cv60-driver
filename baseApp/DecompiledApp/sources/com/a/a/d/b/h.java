package com.a.a.d.b;

import com.a.a.d.a;
import com.a.a.d.c;

public abstract class h {

    /* renamed from: a  reason: collision with root package name */
    public static final h f810a = new h() {
        /* class com.a.a.d.b.h.AnonymousClass1 */

        @Override // com.a.a.d.b.h
        public boolean a(a aVar) {
            return aVar == a.REMOTE;
        }

        @Override // com.a.a.d.b.h
        public boolean a(boolean z, a aVar, c cVar) {
            return (aVar == a.RESOURCE_DISK_CACHE || aVar == a.MEMORY_CACHE) ? false : true;
        }

        @Override // com.a.a.d.b.h
        public boolean a() {
            return true;
        }

        @Override // com.a.a.d.b.h
        public boolean b() {
            return true;
        }
    };
    public static final h b = new h() {
        /* class com.a.a.d.b.h.AnonymousClass2 */

        @Override // com.a.a.d.b.h
        public boolean a(a aVar) {
            return false;
        }

        @Override // com.a.a.d.b.h
        public boolean a(boolean z, a aVar, c cVar) {
            return false;
        }

        @Override // com.a.a.d.b.h
        public boolean a() {
            return false;
        }

        @Override // com.a.a.d.b.h
        public boolean b() {
            return false;
        }
    };
    public static final h c = new h() {
        /* class com.a.a.d.b.h.AnonymousClass3 */

        @Override // com.a.a.d.b.h
        public boolean a(a aVar) {
            return (aVar == a.DATA_DISK_CACHE || aVar == a.MEMORY_CACHE) ? false : true;
        }

        @Override // com.a.a.d.b.h
        public boolean a(boolean z, a aVar, c cVar) {
            return false;
        }

        @Override // com.a.a.d.b.h
        public boolean a() {
            return false;
        }

        @Override // com.a.a.d.b.h
        public boolean b() {
            return true;
        }
    };
    public static final h d = new h() {
        /* class com.a.a.d.b.h.AnonymousClass4 */

        @Override // com.a.a.d.b.h
        public boolean a(a aVar) {
            return false;
        }

        @Override // com.a.a.d.b.h
        public boolean a(boolean z, a aVar, c cVar) {
            return (aVar == a.RESOURCE_DISK_CACHE || aVar == a.MEMORY_CACHE) ? false : true;
        }

        @Override // com.a.a.d.b.h
        public boolean a() {
            return true;
        }

        @Override // com.a.a.d.b.h
        public boolean b() {
            return false;
        }
    };
    public static final h e = new h() {
        /* class com.a.a.d.b.h.AnonymousClass5 */

        @Override // com.a.a.d.b.h
        public boolean a(a aVar) {
            return aVar == a.REMOTE;
        }

        @Override // com.a.a.d.b.h
        public boolean a(boolean z, a aVar, c cVar) {
            return ((z && aVar == a.DATA_DISK_CACHE) || aVar == a.LOCAL) && cVar == c.TRANSFORMED;
        }

        @Override // com.a.a.d.b.h
        public boolean a() {
            return true;
        }

        @Override // com.a.a.d.b.h
        public boolean b() {
            return true;
        }
    };

    public abstract boolean a();

    public abstract boolean a(a aVar);

    public abstract boolean a(boolean z, a aVar, c cVar);

    public abstract boolean b();
}
