package com.a.a.d.b.b;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private final int f788a;
    private final int b;
    private final Context c;
    private final int d;

    /* access modifiers changed from: package-private */
    public interface c {
        int a();

        int b();
    }

    i(a aVar) {
        int i;
        this.c = aVar.b;
        if (b(aVar.c)) {
            i = aVar.i / 2;
        } else {
            i = aVar.i;
        }
        this.d = i;
        int a2 = a(aVar.c, aVar.g, aVar.h);
        int a3 = aVar.d.a() * aVar.d.b() * 4;
        int round = Math.round(((float) a3) * aVar.f);
        int round2 = Math.round(((float) a3) * aVar.e);
        int i2 = a2 - this.d;
        if (round2 + round <= i2) {
            this.b = round2;
            this.f788a = round;
        } else {
            float f = ((float) i2) / (aVar.f + aVar.e);
            this.b = Math.round(aVar.e * f);
            this.f788a = Math.round(f * aVar.f);
        }
        if (Log.isLoggable("MemorySizeCalculator", 3)) {
            Log.d("MemorySizeCalculator", "Calculation complete, Calculated memory cache size: " + a(this.b) + ", pool size: " + a(this.f788a) + ", byte array size: " + a(this.d) + ", memory class limited? " + (round2 + round > a2) + ", max size: " + a(a2) + ", memoryClass: " + aVar.c.getMemoryClass() + ", isLowMemoryDevice: " + b(aVar.c));
        }
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.f788a;
    }

    public int c() {
        return this.d;
    }

    private static int a(ActivityManager activityManager, float f, float f2) {
        boolean b2 = b(activityManager);
        float memoryClass = (float) (activityManager.getMemoryClass() * 1024 * 1024);
        if (!b2) {
            f2 = f;
        }
        return Math.round(memoryClass * f2);
    }

    private String a(int i) {
        return Formatter.formatFileSize(this.c, (long) i);
    }

    /* access modifiers changed from: private */
    public static boolean b(ActivityManager activityManager) {
        if (Build.VERSION.SDK_INT >= 19) {
            return activityManager.isLowRamDevice();
        }
        return false;
    }

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        static final int f789a = (Build.VERSION.SDK_INT < 26 ? 4 : 1);
        private final Context b;
        private ActivityManager c;
        private c d;
        private float e = 2.0f;
        private float f = ((float) f789a);
        private float g = 0.4f;
        private float h = 0.33f;
        private int i = 4194304;

        public a(Context context) {
            this.b = context;
            this.c = (ActivityManager) context.getSystemService("activity");
            this.d = new b(context.getResources().getDisplayMetrics());
            if (Build.VERSION.SDK_INT >= 26 && i.b(this.c)) {
                this.f = 0.0f;
            }
        }

        public i a() {
            return new i(this);
        }
    }

    private static final class b implements c {

        /* renamed from: a  reason: collision with root package name */
        private final DisplayMetrics f790a;

        public b(DisplayMetrics displayMetrics) {
            this.f790a = displayMetrics;
        }

        @Override // com.a.a.d.b.b.i.c
        public int a() {
            return this.f790a.widthPixels;
        }

        @Override // com.a.a.d.b.b.i.c
        public int b() {
            return this.f790a.heightPixels;
        }
    }
}
