package com.a.a.d.b;

import com.a.a.d.a.b;
import com.a.a.d.b.d;
import com.a.a.d.c.n;
import com.a.a.d.h;
import java.io.File;
import java.util.List;

class a implements b.a<Object>, d {

    /* renamed from: a  reason: collision with root package name */
    private List<h> f769a;
    private final e<?> b;
    private final d.a c;
    private int d;
    private h e;
    private List<n<File, ?>> f;
    private int g;
    private volatile n.a<?> h;
    private File i;

    a(e<?> eVar, d.a aVar) {
        this(eVar.l(), eVar, aVar);
    }

    a(List<h> list, e<?> eVar, d.a aVar) {
        this.d = -1;
        this.f769a = list;
        this.b = eVar;
        this.c = aVar;
    }

    @Override // com.a.a.d.b.d
    public boolean a() {
        boolean z;
        boolean z2 = false;
        while (true) {
            if (this.f == null || !c()) {
                this.d++;
                if (this.d >= this.f769a.size()) {
                    break;
                }
                h hVar = this.f769a.get(this.d);
                this.i = this.b.b().a(new b(hVar, this.b.f()));
                if (this.i != null) {
                    this.e = hVar;
                    this.f = this.b.a(this.i);
                    this.g = 0;
                }
            } else {
                this.h = null;
                while (!z2 && c()) {
                    List<n<File, ?>> list = this.f;
                    int i2 = this.g;
                    this.g = i2 + 1;
                    this.h = list.get(i2).a(this.i, this.b.g(), this.b.h(), this.b.e());
                    if (this.h == null || !this.b.a((Class<?>) this.h.c.d())) {
                        z = z2;
                    } else {
                        z = true;
                        this.h.c.a(this.b.d(), this);
                    }
                    z2 = z;
                }
            }
        }
        return z2;
    }

    private boolean c() {
        return this.g < this.f.size();
    }

    @Override // com.a.a.d.b.d
    public void b() {
        n.a<?> aVar = this.h;
        if (aVar != null) {
            aVar.c.b();
        }
    }

    @Override // com.a.a.d.a.b.a
    public void a(Object obj) {
        this.c.a(this.e, obj, this.h.c, com.a.a.d.a.DATA_DISK_CACHE, this.e);
    }

    @Override // com.a.a.d.a.b.a
    public void a(Exception exc) {
        this.c.a(this.e, exc, this.h.c, com.a.a.d.a.DATA_DISK_CACHE);
    }
}
