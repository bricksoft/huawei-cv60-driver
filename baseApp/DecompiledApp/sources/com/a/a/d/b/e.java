package com.a.a.d.b;

import com.a.a.d.b.b.a;
import com.a.a.d.b.f;
import com.a.a.d.c.n;
import com.a.a.d.d;
import com.a.a.d.d.b;
import com.a.a.d.h;
import com.a.a.d.j;
import com.a.a.d.l;
import com.a.a.d.m;
import com.a.a.g;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* access modifiers changed from: package-private */
public final class e<Transcode> {

    /* renamed from: a  reason: collision with root package name */
    private final List<n.a<?>> f801a = new ArrayList();
    private final List<h> b = new ArrayList();
    private com.a.a.e c;
    private Object d;
    private int e;
    private int f;
    private Class<?> g;
    private f.d h;
    private j i;
    private Map<Class<?>, m<?>> j;
    private Class<Transcode> k;
    private boolean l;
    private boolean m;
    private h n;
    private g o;
    private h p;
    private boolean q;
    private boolean r;

    e() {
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.a.a.d.b.e<Transcode> */
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.lang.Class<R> */
    /* JADX WARN: Multi-variable type inference failed */
    /* access modifiers changed from: package-private */
    public <R> e<R> a(com.a.a.e eVar, Object obj, h hVar, int i2, int i3, h hVar2, Class<?> cls, Class<R> cls2, g gVar, j jVar, Map<Class<?>, m<?>> map, boolean z, boolean z2, f.d dVar) {
        this.c = eVar;
        this.d = obj;
        this.n = hVar;
        this.e = i2;
        this.f = i3;
        this.p = hVar2;
        this.g = cls;
        this.h = dVar;
        this.k = cls2;
        this.o = gVar;
        this.i = jVar;
        this.j = map;
        this.q = z;
        this.r = z2;
        return this;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c = null;
        this.d = null;
        this.n = null;
        this.g = null;
        this.k = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.p = null;
        this.f801a.clear();
        this.l = false;
        this.b.clear();
        this.m = false;
    }

    /* access modifiers changed from: package-private */
    public a b() {
        return this.h.a();
    }

    /* access modifiers changed from: package-private */
    public h c() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public g d() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public j e() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public h f() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public int h() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public List<Class<?>> i() {
        return this.c.c().b(this.d.getClass(), this.g, this.k);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Class<?> */
    /* JADX WARN: Multi-variable type inference failed */
    /* access modifiers changed from: package-private */
    public boolean a(Class<?> cls) {
        return b(cls) != null;
    }

    /* access modifiers changed from: package-private */
    public <Data> q<Data, ?, Transcode> b(Class<Data> cls) {
        return this.c.c().a(cls, this.g, this.k);
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public <Z> m<Z> c(Class<Z> cls) {
        m<Z> mVar = (m<Z>) this.j.get(cls);
        if (mVar != null) {
            return mVar;
        }
        if (!this.j.isEmpty() || !this.q) {
            return b.a();
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    /* access modifiers changed from: package-private */
    public boolean a(s<?> sVar) {
        return this.c.c().a(sVar);
    }

    /* access modifiers changed from: package-private */
    public <Z> l<Z> b(s<Z> sVar) {
        return this.c.c().b((s) sVar);
    }

    /* access modifiers changed from: package-private */
    public List<n<File, ?>> a(File file) {
        return this.c.c().c(file);
    }

    /* access modifiers changed from: package-private */
    public boolean a(h hVar) {
        List<n.a<?>> k2 = k();
        int size = k2.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (k2.get(i2).f862a.equals(hVar)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public List<n.a<?>> k() {
        if (!this.l) {
            this.l = true;
            this.f801a.clear();
            List c2 = this.c.c().c(this.d);
            int size = c2.size();
            for (int i2 = 0; i2 < size; i2++) {
                n.a<?> a2 = ((n) c2.get(i2)).a(this.d, this.e, this.f, this.i);
                if (a2 != null) {
                    this.f801a.add(a2);
                }
            }
        }
        return this.f801a;
    }

    /* access modifiers changed from: package-private */
    public List<h> l() {
        if (!this.m) {
            this.m = true;
            this.b.clear();
            List<n.a<?>> k2 = k();
            int size = k2.size();
            for (int i2 = 0; i2 < size; i2++) {
                n.a<?> aVar = k2.get(i2);
                if (!this.b.contains(aVar.f862a)) {
                    this.b.add(aVar.f862a);
                }
                for (int i3 = 0; i3 < aVar.b.size(); i3++) {
                    if (!this.b.contains(aVar.b.get(i3))) {
                        this.b.add(aVar.b.get(i3));
                    }
                }
            }
        }
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public <X> d<X> a(X x) {
        return this.c.c().a((Object) x);
    }
}
