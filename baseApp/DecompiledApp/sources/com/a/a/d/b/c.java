package com.a.a.d.b;

import com.a.a.d.b.b.a;
import com.a.a.d.d;
import com.a.a.d.j;
import java.io.File;

class c<DataType> implements a.b {

    /* renamed from: a  reason: collision with root package name */
    private final d<DataType> f794a;
    private final DataType b;
    private final j c;

    c(d<DataType> dVar, DataType datatype, j jVar) {
        this.f794a = dVar;
        this.b = datatype;
        this.c = jVar;
    }

    @Override // com.a.a.d.b.b.a.b
    public boolean a(File file) {
        return this.f794a.a(this.b, file, this.c);
    }
}
