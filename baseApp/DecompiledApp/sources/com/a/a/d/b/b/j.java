package com.a.a.d.b.b;

import android.support.v4.util.Pools;
import com.a.a.d.h;
import com.a.a.j.a.a;
import com.a.a.j.a.b;
import com.a.a.j.e;
import com.a.a.j.i;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private final e<h, String> f791a = new e<>(1000);
    private final Pools.Pool<a> b = com.a.a.j.a.a.b(10, new a.AbstractC0041a<a>() {
        /* class com.a.a.d.b.b.j.AnonymousClass1 */

        /* renamed from: a */
        public a b() {
            try {
                return new a(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    });

    public String a(h hVar) {
        String b2;
        synchronized (this.f791a) {
            b2 = this.f791a.b(hVar);
        }
        if (b2 == null) {
            b2 = b(hVar);
        }
        synchronized (this.f791a) {
            this.f791a.b(hVar, b2);
        }
        return b2;
    }

    private String b(h hVar) {
        a acquire = this.b.acquire();
        try {
            hVar.a(acquire.f793a);
            return i.a(acquire.f793a.digest());
        } finally {
            this.b.release(acquire);
        }
    }

    /* access modifiers changed from: private */
    public static final class a implements a.c {

        /* renamed from: a  reason: collision with root package name */
        final MessageDigest f793a;
        private final b b = b.a();

        a(MessageDigest messageDigest) {
            this.f793a = messageDigest;
        }

        @Override // com.a.a.j.a.a.c
        public b a_() {
            return this.b;
        }
    }
}
