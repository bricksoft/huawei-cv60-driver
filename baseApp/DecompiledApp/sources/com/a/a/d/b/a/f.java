package com.a.a.d.b.a;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

public class f implements e {
    @Override // com.a.a.d.b.a.e
    public void a(Bitmap bitmap) {
        bitmap.recycle();
    }

    @Override // com.a.a.d.b.a.e
    @NonNull
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return Bitmap.createBitmap(i, i2, config);
    }

    @Override // com.a.a.d.b.a.e
    @NonNull
    public Bitmap b(int i, int i2, Bitmap.Config config) {
        return a(i, i2, config);
    }

    @Override // com.a.a.d.b.a.e
    public void a() {
    }

    @Override // com.a.a.d.b.a.e
    public void a(int i) {
    }
}
