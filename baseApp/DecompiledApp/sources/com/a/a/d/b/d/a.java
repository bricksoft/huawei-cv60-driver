package com.a.a.d.b.d;

import android.os.Handler;
import android.os.Looper;
import com.a.a.d.b;
import com.a.a.d.b.a.e;
import com.a.a.d.b.b.h;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final h f800a;
    private final e b;
    private final b c;
    private final Handler d = new Handler(Looper.getMainLooper());

    public a(h hVar, e eVar, b bVar) {
        this.f800a = hVar;
        this.b = eVar;
        this.c = bVar;
    }
}
