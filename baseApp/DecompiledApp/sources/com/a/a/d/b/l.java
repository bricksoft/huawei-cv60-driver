package com.a.a.d.b;

import com.a.a.d.h;
import com.a.a.d.j;
import com.a.a.d.m;
import java.security.MessageDigest;
import java.util.Map;

/* access modifiers changed from: package-private */
public class l implements h {
    private final Object b;
    private final int c;
    private final int d;
    private final Class<?> e;
    private final Class<?> f;
    private final h g;
    private final Map<Class<?>, m<?>> h;
    private final j i;
    private int j;

    public l(Object obj, h hVar, int i2, int i3, Map<Class<?>, m<?>> map, Class<?> cls, Class<?> cls2, j jVar) {
        this.b = com.a.a.j.h.a(obj);
        this.g = (h) com.a.a.j.h.a(hVar, "Signature must not be null");
        this.c = i2;
        this.d = i3;
        this.h = (Map) com.a.a.j.h.a(map);
        this.e = (Class) com.a.a.j.h.a(cls, "Resource class must not be null");
        this.f = (Class) com.a.a.j.h.a(cls2, "Transcode class must not be null");
        this.i = (j) com.a.a.j.h.a(jVar);
    }

    @Override // com.a.a.d.h
    public boolean equals(Object obj) {
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        if (!this.b.equals(lVar.b) || !this.g.equals(lVar.g) || this.d != lVar.d || this.c != lVar.c || !this.h.equals(lVar.h) || !this.e.equals(lVar.e) || !this.f.equals(lVar.f) || !this.i.equals(lVar.i)) {
            return false;
        }
        return true;
    }

    @Override // com.a.a.d.h
    public int hashCode() {
        if (this.j == 0) {
            this.j = this.b.hashCode();
            this.j = (this.j * 31) + this.g.hashCode();
            this.j = (this.j * 31) + this.c;
            this.j = (this.j * 31) + this.d;
            this.j = (this.j * 31) + this.h.hashCode();
            this.j = (this.j * 31) + this.e.hashCode();
            this.j = (this.j * 31) + this.f.hashCode();
            this.j = (this.j * 31) + this.i.hashCode();
        }
        return this.j;
    }

    public String toString() {
        return "EngineKey{model=" + this.b + ", width=" + this.c + ", height=" + this.d + ", resourceClass=" + this.e + ", transcodeClass=" + this.f + ", signature=" + this.g + ", hashCode=" + this.j + ", transformations=" + this.h + ", options=" + this.i + '}';
    }

    @Override // com.a.a.d.h
    public void a(MessageDigest messageDigest) {
        throw new UnsupportedOperationException();
    }
}
