package com.a.a.d.b.a;

import com.a.a.d.b.a.m;
import com.a.a.j.i;
import java.util.Queue;

/* access modifiers changed from: package-private */
public abstract class d<T extends m> {

    /* renamed from: a  reason: collision with root package name */
    private final Queue<T> f772a = i.a(20);

    /* access modifiers changed from: protected */
    public abstract T b();

    d() {
    }

    /* access modifiers changed from: protected */
    public T c() {
        T poll = this.f772a.poll();
        if (poll == null) {
            return b();
        }
        return poll;
    }

    public void a(T t) {
        if (this.f772a.size() < 20) {
            this.f772a.offer(t);
        }
    }
}
