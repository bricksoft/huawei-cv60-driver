package com.a.a.d.b;

import android.support.v4.util.Pools;
import com.a.a.d.a.c;
import com.a.a.d.b.g;
import com.a.a.d.j;
import com.a.a.j.h;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class q<Data, ResourceType, Transcode> {

    /* renamed from: a  reason: collision with root package name */
    private final Class<Data> f824a;
    private final Pools.Pool<List<Exception>> b;
    private final List<? extends g<Data, ResourceType, Transcode>> c;
    private final String d;

    public q(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<g<Data, ResourceType, Transcode>> list, Pools.Pool<List<Exception>> pool) {
        this.f824a = cls;
        this.b = pool;
        this.c = (List) h.a((Collection) list);
        this.d = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    public s<Transcode> a(c<Data> cVar, j jVar, int i, int i2, g.a<ResourceType> aVar) {
        List<Exception> acquire = this.b.acquire();
        try {
            return a(cVar, jVar, i, i2, aVar, acquire);
        } finally {
            this.b.release(acquire);
        }
    }

    private s<Transcode> a(c<Data> cVar, j jVar, int i, int i2, g.a<ResourceType> aVar, List<Exception> list) {
        s<Transcode> sVar;
        int size = this.c.size();
        s<Transcode> sVar2 = null;
        int i3 = 0;
        while (true) {
            if (i3 >= size) {
                sVar = sVar2;
                break;
            }
            try {
                sVar = ((g) this.c.get(i3)).a(cVar, i, i2, jVar, aVar);
            } catch (o e) {
                list.add(e);
                sVar = sVar2;
            }
            if (sVar != null) {
                break;
            }
            i3++;
            sVar2 = sVar;
        }
        if (sVar != null) {
            return sVar;
        }
        throw new o(this.d, new ArrayList(list));
    }

    public String toString() {
        return "LoadPath{decodePaths=" + Arrays.toString(this.c.toArray(new g[this.c.size()])) + '}';
    }
}
