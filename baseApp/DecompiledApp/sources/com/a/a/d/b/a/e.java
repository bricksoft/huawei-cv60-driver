package com.a.a.d.b.a;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

public interface e {
    @NonNull
    Bitmap a(int i, int i2, Bitmap.Config config);

    void a();

    void a(int i);

    void a(Bitmap bitmap);

    @NonNull
    Bitmap b(int i, int i2, Bitmap.Config config);
}
