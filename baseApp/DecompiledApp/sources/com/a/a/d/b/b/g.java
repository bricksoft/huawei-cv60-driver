package com.a.a.d.b.b;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;
import com.a.a.d.b.b.h;
import com.a.a.d.b.s;
import com.a.a.d.h;
import com.a.a.j.e;

public class g extends e<h, s<?>> implements h {

    /* renamed from: a  reason: collision with root package name */
    private h.a f787a;

    @Override // com.a.a.d.b.b.h
    @Nullable
    public /* synthetic */ s a(com.a.a.d.h hVar) {
        return (s) super.c(hVar);
    }

    @Override // com.a.a.d.b.b.h
    public /* bridge */ /* synthetic */ s b(com.a.a.d.h hVar, s sVar) {
        return (s) super.b((Object) hVar, (Object) sVar);
    }

    public g(int i) {
        super(i);
    }

    @Override // com.a.a.d.b.b.h
    public void a(h.a aVar) {
        this.f787a = aVar;
    }

    /* access modifiers changed from: protected */
    public void a(com.a.a.d.h hVar, s<?> sVar) {
        if (this.f787a != null) {
            this.f787a.b(sVar);
        }
    }

    /* access modifiers changed from: protected */
    public int a(s<?> sVar) {
        return sVar.d();
    }

    @Override // com.a.a.d.b.b.h
    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 40) {
            a();
        } else if (i >= 20) {
            b(b() / 2);
        }
    }
}
