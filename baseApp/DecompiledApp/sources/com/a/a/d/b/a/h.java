package com.a.a.d.b.a;

import android.support.annotation.Nullable;
import com.a.a.d.b.a.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* access modifiers changed from: package-private */
public class h<K extends m, V> {

    /* renamed from: a  reason: collision with root package name */
    private final a<K, V> f773a = new a<>();
    private final Map<K, a<K, V>> b = new HashMap();

    h() {
    }

    public void a(K k, V v) {
        a<K, V> aVar = this.b.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            b(aVar);
            this.b.put(k, aVar);
        } else {
            k.a();
        }
        aVar.a(v);
    }

    @Nullable
    public V a(K k) {
        a<K, V> aVar = this.b.get(k);
        if (aVar == null) {
            aVar = new a<>(k);
            this.b.put(k, aVar);
        } else {
            k.a();
        }
        a(aVar);
        return aVar.a();
    }

    @Nullable
    public V a() {
        for (a<K, V> aVar = this.f773a.c; !aVar.equals(this.f773a); aVar = aVar.c) {
            V a2 = aVar.a();
            if (a2 != null) {
                return a2;
            }
            d(aVar);
            this.b.remove(aVar.f774a);
            aVar.f774a.a();
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GroupedLinkedMap( ");
        boolean z = false;
        for (a<K, V> aVar = this.f773a.b; !aVar.equals(this.f773a); aVar = aVar.b) {
            z = true;
            sb.append('{').append((Object) aVar.f774a).append(':').append(aVar.b()).append("}, ");
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        return sb.append(" )").toString();
    }

    private void a(a<K, V> aVar) {
        d(aVar);
        aVar.c = this.f773a;
        aVar.b = this.f773a.b;
        c(aVar);
    }

    private void b(a<K, V> aVar) {
        d(aVar);
        aVar.c = this.f773a.c;
        aVar.b = this.f773a;
        c(aVar);
    }

    private static <K, V> void c(a<K, V> aVar) {
        aVar.b.c = aVar;
        aVar.c.b = aVar;
    }

    private static <K, V> void d(a<K, V> aVar) {
        aVar.c.b = aVar.b;
        aVar.b.c = aVar.c;
    }

    /* access modifiers changed from: private */
    public static class a<K, V> {

        /* renamed from: a  reason: collision with root package name */
        final K f774a;
        a<K, V> b;
        a<K, V> c;
        private List<V> d;

        public a() {
            this(null);
        }

        public a(K k) {
            this.c = this;
            this.b = this;
            this.f774a = k;
        }

        @Nullable
        public V a() {
            int b2 = b();
            if (b2 > 0) {
                return this.d.remove(b2 - 1);
            }
            return null;
        }

        public int b() {
            if (this.d != null) {
                return this.d.size();
            }
            return 0;
        }

        public void a(V v) {
            if (this.d == null) {
                this.d = new ArrayList();
            }
            this.d.add(v);
        }
    }
}
