package com.a.a.d.b;

import android.support.v4.util.Pools;
import com.a.a.j.a.a;
import com.a.a.j.a.b;

/* access modifiers changed from: package-private */
public final class r<Z> implements s<Z>, a.c {

    /* renamed from: a  reason: collision with root package name */
    private static final Pools.Pool<r<?>> f825a = a.b(20, new a.AbstractC0041a<r<?>>() {
        /* class com.a.a.d.b.r.AnonymousClass1 */

        /* renamed from: a */
        public r<?> b() {
            return new r<>();
        }
    });
    private final b b = b.a();
    private s<Z> c;
    private boolean d;
    private boolean e;

    static <Z> r<Z> a(s<Z> sVar) {
        r<Z> rVar = (r<Z>) f825a.acquire();
        rVar.b(sVar);
        return rVar;
    }

    r() {
    }

    private void b(s<Z> sVar) {
        this.e = false;
        this.d = true;
        this.c = sVar;
    }

    private void f() {
        this.c = null;
        f825a.release(this);
    }

    public synchronized void a() {
        this.b.b();
        if (!this.d) {
            throw new IllegalStateException("Already unlocked");
        }
        this.d = false;
        if (this.e) {
            e();
        }
    }

    @Override // com.a.a.d.b.s
    public Class<Z> b() {
        return this.c.b();
    }

    @Override // com.a.a.d.b.s
    public Z c() {
        return this.c.c();
    }

    @Override // com.a.a.d.b.s
    public int d() {
        return this.c.d();
    }

    @Override // com.a.a.d.b.s
    public synchronized void e() {
        this.b.b();
        this.e = true;
        if (!this.d) {
            this.c.e();
            f();
        }
    }

    @Override // com.a.a.j.a.a.c
    public b a_() {
        return this.b;
    }
}
