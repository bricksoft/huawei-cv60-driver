package com.a.a.d.b.b;

import android.support.annotation.Nullable;
import com.a.a.d.b.s;

public interface h {

    public interface a {
        void b(s<?> sVar);
    }

    @Nullable
    s<?> a(com.a.a.d.h hVar);

    void a();

    void a(int i);

    void a(a aVar);

    @Nullable
    s<?> b(com.a.a.d.h hVar, s<?> sVar);
}
