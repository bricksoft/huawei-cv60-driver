package com.a.a.d.b;

import com.a.a.d.h;
import com.a.a.d.j;
import com.a.a.d.m;
import com.a.a.j.e;
import com.a.a.j.i;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

final class u implements h {
    private static final e<Class<?>, byte[]> b = new e<>(50);
    private final h c;
    private final h d;
    private final int e;
    private final int f;
    private final Class<?> g;
    private final j h;
    private final m<?> i;

    public u(h hVar, h hVar2, int i2, int i3, m<?> mVar, Class<?> cls, j jVar) {
        this.c = hVar;
        this.d = hVar2;
        this.e = i2;
        this.f = i3;
        this.i = mVar;
        this.g = cls;
        this.h = jVar;
    }

    @Override // com.a.a.d.h
    public boolean equals(Object obj) {
        if (!(obj instanceof u)) {
            return false;
        }
        u uVar = (u) obj;
        if (this.f != uVar.f || this.e != uVar.e || !i.a(this.i, uVar.i) || !this.g.equals(uVar.g) || !this.c.equals(uVar.c) || !this.d.equals(uVar.d) || !this.h.equals(uVar.h)) {
            return false;
        }
        return true;
    }

    @Override // com.a.a.d.h
    public int hashCode() {
        int hashCode = (((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + this.e) * 31) + this.f;
        if (this.i != null) {
            hashCode = (hashCode * 31) + this.i.hashCode();
        }
        return (((hashCode * 31) + this.g.hashCode()) * 31) + this.h.hashCode();
    }

    @Override // com.a.a.d.h
    public void a(MessageDigest messageDigest) {
        byte[] array = ByteBuffer.allocate(8).putInt(this.e).putInt(this.f).array();
        this.d.a(messageDigest);
        this.c.a(messageDigest);
        messageDigest.update(array);
        if (this.i != null) {
            this.i.a(messageDigest);
        }
        this.h.a(messageDigest);
        messageDigest.update(a());
    }

    private byte[] a() {
        byte[] b2 = b.b(this.g);
        if (b2 != null) {
            return b2;
        }
        byte[] bytes = this.g.getName().getBytes(f922a);
        b.b(this.g, bytes);
        return bytes;
    }

    public String toString() {
        return "ResourceCacheKey{sourceKey=" + this.c + ", signature=" + this.d + ", width=" + this.e + ", height=" + this.f + ", decodedResourceClass=" + this.g + ", transformation='" + this.i + '\'' + ", options=" + this.h + '}';
    }
}
