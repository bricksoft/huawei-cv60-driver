package com.a.a.d.b.a;

public final class g implements a<byte[]> {
    @Override // com.a.a.d.b.a.a
    public String a() {
        return "ByteArrayPool";
    }

    public int a(byte[] bArr) {
        return bArr.length;
    }

    /* renamed from: b */
    public byte[] a(int i) {
        return new byte[i];
    }

    @Override // com.a.a.d.b.a.a
    public int b() {
        return 1;
    }
}
