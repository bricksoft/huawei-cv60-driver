package com.a.a.d.b.c;

import android.os.Process;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.util.Log;
import java.io.File;
import java.io.FilenameFilter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public final class a extends ThreadPoolExecutor {
    private static final long b = TimeUnit.SECONDS.toMillis(10);

    /* renamed from: a  reason: collision with root package name */
    private final boolean f795a;

    public interface b {

        /* renamed from: a  reason: collision with root package name */
        public static final b f799a = new b() {
            /* class com.a.a.d.b.c.a.b.AnonymousClass1 */

            @Override // com.a.a.d.b.c.a.b
            public void a(Throwable th) {
            }
        };
        public static final b b = new b() {
            /* class com.a.a.d.b.c.a.b.AnonymousClass2 */

            @Override // com.a.a.d.b.c.a.b
            public void a(Throwable th) {
                if (th != null && Log.isLoggable("GlideExecutor", 6)) {
                    Log.e("GlideExecutor", "Request threw uncaught throwable", th);
                }
            }
        };
        public static final b c = new b() {
            /* class com.a.a.d.b.c.a.b.AnonymousClass3 */

            @Override // com.a.a.d.b.c.a.b
            public void a(Throwable th) {
                if (th != null) {
                    throw new RuntimeException("Request threw uncaught throwable", th);
                }
            }
        };
        public static final b d = b;

        void a(Throwable th);
    }

    public static a a() {
        return a(1, "disk-cache", b.d);
    }

    public static a a(int i, String str, b bVar) {
        return new a(i, str, bVar, true, false);
    }

    public static a b() {
        return b(d(), "source", b.d);
    }

    public static a b(int i, String str, b bVar) {
        return new a(i, str, bVar, false, false);
    }

    public static a c() {
        return new a(0, Integer.MAX_VALUE, b, "source-unlimited", b.d, false, false, new SynchronousQueue());
    }

    a(int i, String str, b bVar, boolean z, boolean z2) {
        this(i, i, 0, str, bVar, z, z2);
    }

    a(int i, int i2, long j, String str, b bVar, boolean z, boolean z2) {
        this(i, i2, j, str, bVar, z, z2, new PriorityBlockingQueue());
    }

    a(int i, int i2, long j, String str, b bVar, boolean z, boolean z2, BlockingQueue<Runnable> blockingQueue) {
        super(i, i2, j, TimeUnit.MILLISECONDS, blockingQueue, new ThreadFactoryC0029a(str, bVar, z));
        this.f795a = z2;
    }

    public void execute(Runnable runnable) {
        if (this.f795a) {
            runnable.run();
        } else {
            super.execute(runnable);
        }
    }

    @Override // java.util.concurrent.AbstractExecutorService, java.util.concurrent.ExecutorService
    @NonNull
    public Future<?> submit(Runnable runnable) {
        return a(super.submit(runnable));
    }

    private <T> Future<T> a(Future<T> future) {
        if (this.f795a) {
            boolean z = false;
            while (!future.isDone()) {
                try {
                    try {
                        future.get();
                    } catch (ExecutionException e) {
                        throw new RuntimeException(e);
                    } catch (InterruptedException e2) {
                        z = true;
                    }
                } finally {
                    if (z) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
        return future;
    }

    @Override // java.util.concurrent.AbstractExecutorService, java.util.concurrent.ExecutorService
    @NonNull
    public <T> Future<T> submit(Runnable runnable, T t) {
        return a(super.submit(runnable, t));
    }

    @Override // java.util.concurrent.AbstractExecutorService, java.util.concurrent.ExecutorService
    public <T> Future<T> submit(Callable<T> callable) {
        return a(super.submit(callable));
    }

    /* JADX INFO: finally extract failed */
    public static int d() {
        File[] fileArr;
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            File file = new File("/sys/devices/system/cpu/");
            final Pattern compile = Pattern.compile("cpu[0-9]+");
            fileArr = file.listFiles(new FilenameFilter() {
                /* class com.a.a.d.b.c.a.AnonymousClass1 */

                public boolean accept(File file, String str) {
                    return compile.matcher(str).matches();
                }
            });
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
        return Math.min(4, Math.max(Math.max(1, Runtime.getRuntime().availableProcessors()), fileArr != null ? fileArr.length : 0));
    }

    /* renamed from: com.a.a.d.b.c.a$a  reason: collision with other inner class name */
    private static final class ThreadFactoryC0029a implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        final b f797a;
        final boolean b;
        private final String c;
        private int d;

        ThreadFactoryC0029a(String str, b bVar, boolean z) {
            this.c = str;
            this.f797a = bVar;
            this.b = z;
        }

        public synchronized Thread newThread(@NonNull Runnable runnable) {
            AnonymousClass1 r0;
            r0 = new Thread(runnable, "glide-" + this.c + "-thread-" + this.d) {
                /* class com.a.a.d.b.c.a.ThreadFactoryC0029a.AnonymousClass1 */

                public void run() {
                    Process.setThreadPriority(9);
                    if (ThreadFactoryC0029a.this.b) {
                        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                    }
                    try {
                        super.run();
                    } catch (Throwable th) {
                        ThreadFactoryC0029a.this.f797a.a(th);
                    }
                }
            };
            this.d++;
            return r0;
        }
    }
}
