package com.a.a.d.b.b;

import android.content.Context;
import com.a.a.d.b.b.d;
import java.io.File;

public final class f extends d {
    public f(Context context) {
        this(context, "image_manager_disk_cache", 262144000);
    }

    public f(final Context context, final String str, int i) {
        super(new d.a() {
            /* class com.a.a.d.b.b.f.AnonymousClass1 */

            @Override // com.a.a.d.b.b.d.a
            public File a() {
                File cacheDir = context.getCacheDir();
                if (cacheDir == null) {
                    return null;
                }
                if (str != null) {
                    return new File(cacheDir, str);
                }
                return cacheDir;
            }
        }, i);
    }
}
