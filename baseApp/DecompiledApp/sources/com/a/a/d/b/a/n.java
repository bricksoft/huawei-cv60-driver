package com.a.a.d.b.a;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import com.a.a.j.i;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

@TargetApi(19)
public class n implements l {

    /* renamed from: a  reason: collision with root package name */
    private static final Bitmap.Config[] f778a = {Bitmap.Config.ARGB_8888, null};
    private static final Bitmap.Config[] b = {Bitmap.Config.RGB_565};
    private static final Bitmap.Config[] c = {Bitmap.Config.ARGB_4444};
    private static final Bitmap.Config[] d = {Bitmap.Config.ALPHA_8};
    private final b e = new b();
    private final h<a, Bitmap> f = new h<>();
    private final Map<Bitmap.Config, NavigableMap<Integer, Integer>> g = new HashMap();

    @Override // com.a.a.d.b.a.l
    public void a(Bitmap bitmap) {
        a a2 = this.e.a(i.a(bitmap), bitmap.getConfig());
        this.f.a(a2, bitmap);
        NavigableMap<Integer, Integer> a3 = a(bitmap.getConfig());
        Integer num = (Integer) a3.get(Integer.valueOf(a2.f780a));
        a3.put(Integer.valueOf(a2.f780a), Integer.valueOf(num == null ? 1 : num.intValue() + 1));
    }

    @Override // com.a.a.d.b.a.l
    @Nullable
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        a b2 = b(i.a(i, i2, config), config);
        Bitmap a2 = this.f.a(b2);
        if (a2 != null) {
            a(Integer.valueOf(b2.f780a), a2);
            a2.reconfigure(i, i2, a2.getConfig() != null ? a2.getConfig() : Bitmap.Config.ARGB_8888);
        }
        return a2;
    }

    private a b(int i, Bitmap.Config config) {
        a a2 = this.e.a(i, config);
        Bitmap.Config[] b2 = b(config);
        int length = b2.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                break;
            }
            Bitmap.Config config2 = b2[i2];
            Integer ceilingKey = a(config2).ceilingKey(Integer.valueOf(i));
            if (ceilingKey == null || ceilingKey.intValue() > i * 8) {
                i2++;
            } else if (ceilingKey.intValue() != i || (config2 != null ? !config2.equals(config) : config != null)) {
                this.e.a(a2);
                return this.e.a(ceilingKey.intValue(), config2);
            }
        }
        return a2;
    }

    @Override // com.a.a.d.b.a.l
    @Nullable
    public Bitmap a() {
        Bitmap a2 = this.f.a();
        if (a2 != null) {
            a(Integer.valueOf(i.a(a2)), a2);
        }
        return a2;
    }

    private void a(Integer num, Bitmap bitmap) {
        NavigableMap<Integer, Integer> a2 = a(bitmap.getConfig());
        Integer num2 = (Integer) a2.get(num);
        if (num2 == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + num + ", removed: " + b(bitmap) + ", this: " + this);
        } else if (num2.intValue() == 1) {
            a2.remove(num);
        } else {
            a2.put(num, Integer.valueOf(num2.intValue() - 1));
        }
    }

    private NavigableMap<Integer, Integer> a(Bitmap.Config config) {
        NavigableMap<Integer, Integer> navigableMap = this.g.get(config);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.g.put(config, treeMap);
        return treeMap;
    }

    @Override // com.a.a.d.b.a.l
    public String b(Bitmap bitmap) {
        return a(i.a(bitmap), bitmap.getConfig());
    }

    @Override // com.a.a.d.b.a.l
    public String b(int i, int i2, Bitmap.Config config) {
        return a(i.a(i, i2, config), config);
    }

    @Override // com.a.a.d.b.a.l
    public int c(Bitmap bitmap) {
        return i.a(bitmap);
    }

    public String toString() {
        StringBuilder append = new StringBuilder().append("SizeConfigStrategy{groupedMap=").append(this.f).append(", sortedSizes=(");
        for (Map.Entry<Bitmap.Config, NavigableMap<Integer, Integer>> entry : this.g.entrySet()) {
            append.append(entry.getKey()).append('[').append(entry.getValue()).append("], ");
        }
        if (!this.g.isEmpty()) {
            append.replace(append.length() - 2, append.length(), "");
        }
        return append.append(")}").toString();
    }

    /* access modifiers changed from: package-private */
    public static class b extends d<a> {
        b() {
        }

        public a a(int i, Bitmap.Config config) {
            a aVar = (a) c();
            aVar.a(i, config);
            return aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public a b() {
            return new a(this);
        }
    }

    /* access modifiers changed from: package-private */
    public static final class a implements m {

        /* renamed from: a  reason: collision with root package name */
        int f780a;
        private final b b;
        private Bitmap.Config c;

        public a(b bVar) {
            this.b = bVar;
        }

        public void a(int i, Bitmap.Config config) {
            this.f780a = i;
            this.c = config;
        }

        @Override // com.a.a.d.b.a.m
        public void a() {
            this.b.a(this);
        }

        public String toString() {
            return n.a(this.f780a, this.c);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.f780a != aVar.f780a || !i.a(this.c, aVar.c)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (this.c != null ? this.c.hashCode() : 0) + (this.f780a * 31);
        }
    }

    static String a(int i, Bitmap.Config config) {
        return "[" + i + "](" + config + ")";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.a.a.d.b.a.n$1  reason: invalid class name */
    public static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f779a = new int[Bitmap.Config.values().length];

        static {
            try {
                f779a[Bitmap.Config.ARGB_8888.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f779a[Bitmap.Config.RGB_565.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f779a[Bitmap.Config.ARGB_4444.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f779a[Bitmap.Config.ALPHA_8.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    private static Bitmap.Config[] b(Bitmap.Config config) {
        switch (AnonymousClass1.f779a[config.ordinal()]) {
            case 1:
                return f778a;
            case 2:
                return b;
            case 3:
                return c;
            case 4:
                return d;
            default:
                return new Bitmap.Config[]{config};
        }
    }
}
