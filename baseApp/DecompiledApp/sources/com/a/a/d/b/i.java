package com.a.a.d.b;

import android.os.Looper;
import android.os.MessageQueue;
import android.support.v4.util.Pools;
import android.util.Log;
import com.a.a.d.b.b.a;
import com.a.a.d.b.b.h;
import com.a.a.d.b.f;
import com.a.a.d.b.n;
import com.a.a.d.j;
import com.a.a.d.m;
import com.a.a.g;
import com.a.a.j.a.a;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class i implements h.a, k, n.a {

    /* renamed from: a  reason: collision with root package name */
    private final Map<com.a.a.d.h, j<?>> f811a;
    private final m b;
    private final h c;
    private final b d;
    private final Map<com.a.a.d.h, WeakReference<n<?>>> e;
    private final v f;
    private final c g;
    private final a h;
    private ReferenceQueue<n<?>> i;

    public static class d {

        /* renamed from: a  reason: collision with root package name */
        private final j<?> f817a;
        private final com.a.a.h.e b;

        public d(com.a.a.h.e eVar, j<?> jVar) {
            this.b = eVar;
            this.f817a = jVar;
        }

        public void a() {
            this.f817a.b(this.b);
        }
    }

    public i(h hVar, a.AbstractC0028a aVar, com.a.a.d.b.c.a aVar2, com.a.a.d.b.c.a aVar3, com.a.a.d.b.c.a aVar4) {
        this(hVar, aVar, aVar2, aVar3, aVar4, null, null, null, null, null, null);
    }

    i(h hVar, a.AbstractC0028a aVar, com.a.a.d.b.c.a aVar2, com.a.a.d.b.c.a aVar3, com.a.a.d.b.c.a aVar4, Map<com.a.a.d.h, j<?>> map, m mVar, Map<com.a.a.d.h, WeakReference<n<?>>> map2, b bVar, a aVar5, v vVar) {
        this.c = hVar;
        this.g = new c(aVar);
        this.e = map2 == null ? new HashMap<>() : map2;
        this.b = mVar == null ? new m() : mVar;
        this.f811a = map == null ? new HashMap<>() : map;
        this.d = bVar == null ? new b(aVar2, aVar3, aVar4, this) : bVar;
        this.h = aVar5 == null ? new a(this.g) : aVar5;
        this.f = vVar == null ? new v() : vVar;
        hVar.a(this);
    }

    public <R> d a(com.a.a.e eVar, Object obj, com.a.a.d.h hVar, int i2, int i3, Class<?> cls, Class<R> cls2, g gVar, h hVar2, Map<Class<?>, m<?>> map, boolean z, boolean z2, j jVar, boolean z3, boolean z4, boolean z5, com.a.a.h.e eVar2) {
        com.a.a.j.i.a();
        long a2 = com.a.a.j.d.a();
        l a3 = this.b.a(obj, hVar, i2, i3, map, cls, cls2, jVar);
        n<?> b2 = b(a3, z3);
        if (b2 != null) {
            eVar2.a(b2, com.a.a.d.a.MEMORY_CACHE);
            if (Log.isLoggable("Engine", 2)) {
                a("Loaded resource from cache", a2, a3);
            }
            return null;
        }
        n<?> a4 = a(a3, z3);
        if (a4 != null) {
            eVar2.a(a4, com.a.a.d.a.MEMORY_CACHE);
            if (Log.isLoggable("Engine", 2)) {
                a("Loaded resource from active resources", a2, a3);
            }
            return null;
        }
        j<?> jVar2 = this.f811a.get(a3);
        if (jVar2 != null) {
            jVar2.a(eVar2);
            if (Log.isLoggable("Engine", 2)) {
                a("Added to existing load", a2, a3);
            }
            return new d(eVar2, jVar2);
        }
        j<R> a5 = this.d.a(a3, z3, z4);
        f<R> a6 = this.h.a(eVar, obj, a3, hVar, i2, i3, cls, cls2, gVar, hVar2, map, z, z2, z5, jVar, a5);
        this.f811a.put(a3, a5);
        a5.a(eVar2);
        a5.b(a6);
        if (Log.isLoggable("Engine", 2)) {
            a("Started new load", a2, a3);
        }
        return new d(eVar2, a5);
    }

    private static void a(String str, long j, com.a.a.d.h hVar) {
        Log.v("Engine", str + " in " + com.a.a.j.d.a(j) + "ms, key: " + hVar);
    }

    private n<?> a(com.a.a.d.h hVar, boolean z) {
        n<?> nVar;
        if (!z) {
            return null;
        }
        WeakReference<n<?>> weakReference = this.e.get(hVar);
        if (weakReference != null) {
            nVar = weakReference.get();
            if (nVar != null) {
                nVar.f();
            } else {
                this.e.remove(hVar);
            }
        } else {
            nVar = null;
        }
        return nVar;
    }

    private n<?> b(com.a.a.d.h hVar, boolean z) {
        if (!z) {
            return null;
        }
        n<?> a2 = a(hVar);
        if (a2 == null) {
            return a2;
        }
        a2.f();
        this.e.put(hVar, new f(hVar, a2, a()));
        return a2;
    }

    private n<?> a(com.a.a.d.h hVar) {
        s<?> a2 = this.c.a(hVar);
        if (a2 == null) {
            return null;
        }
        if (a2 instanceof n) {
            return (n) a2;
        }
        return new n<>(a2, true);
    }

    public void a(s<?> sVar) {
        com.a.a.j.i.a();
        if (sVar instanceof n) {
            ((n) sVar).g();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    @Override // com.a.a.d.b.k
    public void a(com.a.a.d.h hVar, n<?> nVar) {
        com.a.a.j.i.a();
        if (nVar != null) {
            nVar.a(hVar, this);
            if (nVar.a()) {
                this.e.put(hVar, new f(hVar, nVar, a()));
            }
        }
        this.f811a.remove(hVar);
    }

    @Override // com.a.a.d.b.k
    public void a(j jVar, com.a.a.d.h hVar) {
        com.a.a.j.i.a();
        if (jVar.equals(this.f811a.get(hVar))) {
            this.f811a.remove(hVar);
        }
    }

    @Override // com.a.a.d.b.b.h.a
    public void b(s<?> sVar) {
        com.a.a.j.i.a();
        this.f.a(sVar);
    }

    @Override // com.a.a.d.b.n.a
    public void b(com.a.a.d.h hVar, n nVar) {
        com.a.a.j.i.a();
        this.e.remove(hVar);
        if (nVar.a()) {
            this.c.b(hVar, nVar);
        } else {
            this.f.a(nVar);
        }
    }

    private ReferenceQueue<n<?>> a() {
        if (this.i == null) {
            this.i = new ReferenceQueue<>();
            Looper.myQueue().addIdleHandler(new e(this.e, this.i));
        }
        return this.i;
    }

    private static class c implements f.d {

        /* renamed from: a  reason: collision with root package name */
        private final a.AbstractC0028a f816a;
        private volatile com.a.a.d.b.b.a b;

        public c(a.AbstractC0028a aVar) {
            this.f816a = aVar;
        }

        @Override // com.a.a.d.b.f.d
        public com.a.a.d.b.b.a a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        this.b = this.f816a.a();
                    }
                    if (this.b == null) {
                        this.b = new com.a.a.d.b.b.b();
                    }
                }
            }
            return this.b;
        }
    }

    /* access modifiers changed from: private */
    public static class f extends WeakReference<n<?>> {

        /* renamed from: a  reason: collision with root package name */
        final com.a.a.d.h f819a;

        public f(com.a.a.d.h hVar, n<?> nVar, ReferenceQueue<? super n<?>> referenceQueue) {
            super(nVar, referenceQueue);
            this.f819a = hVar;
        }
    }

    /* access modifiers changed from: private */
    public static class e implements MessageQueue.IdleHandler {

        /* renamed from: a  reason: collision with root package name */
        private final Map<com.a.a.d.h, WeakReference<n<?>>> f818a;
        private final ReferenceQueue<n<?>> b;

        public e(Map<com.a.a.d.h, WeakReference<n<?>>> map, ReferenceQueue<n<?>> referenceQueue) {
            this.f818a = map;
            this.b = referenceQueue;
        }

        public boolean queueIdle() {
            f fVar = (f) this.b.poll();
            if (fVar == null) {
                return true;
            }
            this.f818a.remove(fVar.f819a);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final f.d f812a;
        final Pools.Pool<f<?>> b = com.a.a.j.a.a.a(150, new a.AbstractC0041a<f<?>>() {
            /* class com.a.a.d.b.i.a.AnonymousClass1 */

            /* renamed from: a */
            public f<?> b() {
                return new f<>(a.this.f812a, a.this.b);
            }
        });
        private int c;

        a(f.d dVar) {
            this.f812a = dVar;
        }

        /* access modifiers changed from: package-private */
        public <R> f<R> a(com.a.a.e eVar, Object obj, l lVar, com.a.a.d.h hVar, int i, int i2, Class<?> cls, Class<R> cls2, g gVar, h hVar2, Map<Class<?>, m<?>> map, boolean z, boolean z2, boolean z3, j jVar, f.a<R> aVar) {
            int i3 = this.c;
            this.c = i3 + 1;
            return (f<R>) this.b.acquire().a(eVar, obj, lVar, hVar, i, i2, cls, cls2, gVar, hVar2, map, z, z2, z3, jVar, aVar, i3);
        }
    }

    /* access modifiers changed from: package-private */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        final com.a.a.d.b.c.a f814a;
        final com.a.a.d.b.c.a b;
        final com.a.a.d.b.c.a c;
        final k d;
        final Pools.Pool<j<?>> e = com.a.a.j.a.a.a(150, new a.AbstractC0041a<j<?>>() {
            /* class com.a.a.d.b.i.b.AnonymousClass1 */

            /* renamed from: a */
            public j<?> b() {
                return new j<>(b.this.f814a, b.this.b, b.this.c, b.this.d, b.this.e);
            }
        });

        b(com.a.a.d.b.c.a aVar, com.a.a.d.b.c.a aVar2, com.a.a.d.b.c.a aVar3, k kVar) {
            this.f814a = aVar;
            this.b = aVar2;
            this.c = aVar3;
            this.d = kVar;
        }

        /* access modifiers changed from: package-private */
        public <R> j<R> a(com.a.a.d.h hVar, boolean z, boolean z2) {
            return (j<R>) this.e.acquire().a(hVar, z, z2);
        }
    }
}
