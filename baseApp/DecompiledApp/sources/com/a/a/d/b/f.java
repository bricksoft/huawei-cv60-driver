package com.a.a.d.b;

import android.os.Build;
import android.support.v4.os.TraceCompat;
import android.support.v4.util.Pools;
import android.util.Log;
import com.a.a.d.b.d;
import com.a.a.d.b.g;
import com.a.a.d.d.a.l;
import com.a.a.d.h;
import com.a.a.d.j;
import com.a.a.d.m;
import com.a.a.h;
import com.a.a.j.a.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* access modifiers changed from: package-private */
public class f<R> implements d.a, a.c, Comparable<f<?>>, Runnable {
    private com.a.a.d.a.b<?> A;
    private volatile d B;
    private volatile boolean C;
    private volatile boolean D;

    /* renamed from: a  reason: collision with root package name */
    final e<R> f802a = new e<>();
    final c<?> b = new c<>();
    h c;
    int d;
    int e;
    h f;
    j g;
    h h;
    private final List<Exception> i = new ArrayList();
    private final com.a.a.j.a.b j = com.a.a.j.a.b.a();
    private final d k;
    private final Pools.Pool<f<?>> l;
    private final e m = new e();
    private com.a.a.e n;
    private com.a.a.g o;
    private l p;
    private a<R> q;
    private int r;
    private g s;
    private EnumC0030f t;
    private long u;
    private boolean v;
    private Thread w;
    private h x;
    private Object y;
    private com.a.a.d.a z;

    /* access modifiers changed from: package-private */
    public interface a<R> {
        void a(f<?> fVar);

        void a(o oVar);

        void a(s<R> sVar, com.a.a.d.a aVar);
    }

    /* access modifiers changed from: package-private */
    public interface d {
        com.a.a.d.b.b.a a();
    }

    /* access modifiers changed from: private */
    /* renamed from: com.a.a.d.b.f$f  reason: collision with other inner class name */
    public enum EnumC0030f {
        INITIALIZE,
        SWITCH_TO_SOURCE_SERVICE,
        DECODE_DATA
    }

    /* access modifiers changed from: private */
    public enum g {
        INITIALIZE,
        RESOURCE_CACHE,
        DATA_CACHE,
        SOURCE,
        ENCODE,
        FINISHED
    }

    f(d dVar, Pools.Pool<f<?>> pool) {
        this.k = dVar;
        this.l = pool;
    }

    /* access modifiers changed from: package-private */
    public f<R> a(com.a.a.e eVar, Object obj, l lVar, h hVar, int i2, int i3, Class<?> cls, Class<R> cls2, com.a.a.g gVar, h hVar2, Map<Class<?>, m<?>> map, boolean z2, boolean z3, boolean z4, j jVar, a<R> aVar, int i4) {
        this.f802a.a(eVar, obj, hVar, i2, i3, hVar2, cls, cls2, gVar, jVar, map, z2, z3, this.k);
        this.n = eVar;
        this.c = hVar;
        this.o = gVar;
        this.p = lVar;
        this.d = i2;
        this.e = i3;
        this.f = hVar2;
        this.v = z4;
        this.g = jVar;
        this.q = aVar;
        this.r = i4;
        this.t = EnumC0030f.INITIALIZE;
        return this;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        g a2 = a(g.INITIALIZE);
        return a2 == g.RESOURCE_CACHE || a2 == g.DATA_CACHE;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        if (this.m.a(z2)) {
            g();
        }
    }

    private void e() {
        if (this.m.a()) {
            g();
        }
    }

    private void f() {
        if (this.m.b()) {
            g();
        }
    }

    private void g() {
        this.m.c();
        this.b.b();
        this.f802a.a();
        this.C = false;
        this.n = null;
        this.c = null;
        this.g = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.s = null;
        this.B = null;
        this.w = null;
        this.h = null;
        this.y = null;
        this.z = null;
        this.A = null;
        this.u = 0;
        this.D = false;
        this.i.clear();
        this.l.release(this);
    }

    /* renamed from: a */
    public int compareTo(f<?> fVar) {
        int h2 = h() - fVar.h();
        if (h2 == 0) {
            return this.r - fVar.r;
        }
        return h2;
    }

    private int h() {
        return this.o.ordinal();
    }

    public void b() {
        this.D = true;
        d dVar = this.B;
        if (dVar != null) {
            dVar.b();
        }
    }

    public void run() {
        boolean z2 = false;
        TraceCompat.beginSection("DecodeJob#run");
        com.a.a.d.a.b<?> bVar = this.A;
        try {
            if (this.D) {
                l();
                if (bVar == null || this.A == null || bVar.equals(this.A)) {
                    z2 = true;
                }
                com.a.a.j.h.a(z2, "Fetchers don't match!, old: " + bVar + " new: " + this.A);
                if (bVar != null) {
                    bVar.a();
                }
                TraceCompat.endSection();
                return;
            }
            i();
            if (bVar == null || this.A == null || bVar.equals(this.A)) {
                z2 = true;
            }
            com.a.a.j.h.a(z2, "Fetchers don't match!, old: " + bVar + " new: " + this.A);
            if (bVar != null) {
                bVar.a();
            }
            TraceCompat.endSection();
        } catch (RuntimeException e2) {
            if (Log.isLoggable("DecodeJob", 3)) {
                Log.d("DecodeJob", "DecodeJob threw unexpectedly, isCancelled: " + this.D + ", stage: " + this.s, e2);
            }
            if (this.s != g.ENCODE) {
                l();
            }
            if (!this.D) {
                throw e2;
            }
            if (bVar == null || this.A == null || bVar.equals(this.A)) {
                z2 = true;
            }
            com.a.a.j.h.a(z2, "Fetchers don't match!, old: " + bVar + " new: " + this.A);
            if (bVar != null) {
                bVar.a();
            }
            TraceCompat.endSection();
        } catch (Throwable th) {
            if (bVar == null || this.A == null || bVar.equals(this.A)) {
                z2 = true;
            }
            com.a.a.j.h.a(z2, "Fetchers don't match!, old: " + bVar + " new: " + this.A);
            if (bVar != null) {
                bVar.a();
            }
            TraceCompat.endSection();
            throw th;
        }
    }

    private void i() {
        switch (this.t) {
            case INITIALIZE:
                this.s = a(g.INITIALIZE);
                this.B = j();
                k();
                return;
            case SWITCH_TO_SOURCE_SERVICE:
                k();
                return;
            case DECODE_DATA:
                n();
                return;
            default:
                throw new IllegalStateException("Unrecognized run reason: " + this.t);
        }
    }

    private d j() {
        switch (this.s) {
            case RESOURCE_CACHE:
                return new t(this.f802a, this);
            case DATA_CACHE:
                return new a(this.f802a, this);
            case SOURCE:
                return new w(this.f802a, this);
            case FINISHED:
                return null;
            default:
                throw new IllegalStateException("Unrecognized stage: " + this.s);
        }
    }

    private void k() {
        this.w = Thread.currentThread();
        this.u = com.a.a.j.d.a();
        boolean z2 = false;
        while (!this.D && this.B != null && !(z2 = this.B.a())) {
            this.s = a(this.s);
            this.B = j();
            if (this.s == g.SOURCE) {
                c();
                return;
            }
        }
        if ((this.s == g.FINISHED || this.D) && !z2) {
            l();
        }
    }

    private void l() {
        m();
        this.q.a(new o("Failed to load resource", new ArrayList(this.i)));
        f();
    }

    private void a(s<R> sVar, com.a.a.d.a aVar) {
        m();
        this.q.a(sVar, aVar);
    }

    private void m() {
        this.j.b();
        if (this.C) {
            throw new IllegalStateException("Already notified");
        }
        this.C = true;
    }

    private g a(g gVar) {
        switch (gVar) {
            case RESOURCE_CACHE:
                if (this.f.b()) {
                    return g.DATA_CACHE;
                }
                return a(g.DATA_CACHE);
            case DATA_CACHE:
                return this.v ? g.FINISHED : g.SOURCE;
            case SOURCE:
            case FINISHED:
                return g.FINISHED;
            case INITIALIZE:
                if (this.f.a()) {
                    return g.RESOURCE_CACHE;
                }
                return a(g.RESOURCE_CACHE);
            default:
                throw new IllegalArgumentException("Unrecognized stage: " + gVar);
        }
    }

    @Override // com.a.a.d.b.d.a
    public void c() {
        this.t = EnumC0030f.SWITCH_TO_SOURCE_SERVICE;
        this.q.a((f<?>) this);
    }

    @Override // com.a.a.d.b.d.a
    public void a(h hVar, Object obj, com.a.a.d.a.b<?> bVar, com.a.a.d.a aVar, h hVar2) {
        this.h = hVar;
        this.y = obj;
        this.A = bVar;
        this.z = aVar;
        this.x = hVar2;
        if (Thread.currentThread() != this.w) {
            this.t = EnumC0030f.DECODE_DATA;
            this.q.a((f<?>) this);
            return;
        }
        TraceCompat.beginSection("DecodeJob.decodeFromRetrievedData");
        try {
            n();
        } finally {
            TraceCompat.endSection();
        }
    }

    @Override // com.a.a.d.b.d.a
    public void a(h hVar, Exception exc, com.a.a.d.a.b<?> bVar, com.a.a.d.a aVar) {
        bVar.a();
        o oVar = new o("Fetching data failed", exc);
        oVar.a(hVar, aVar, bVar.d());
        this.i.add(oVar);
        if (Thread.currentThread() != this.w) {
            this.t = EnumC0030f.SWITCH_TO_SOURCE_SERVICE;
            this.q.a((f<?>) this);
            return;
        }
        k();
    }

    private void n() {
        s<R> sVar;
        if (Log.isLoggable("DecodeJob", 2)) {
            a("Retrieved data", this.u, "data: " + this.y + ", cache key: " + this.h + ", fetcher: " + this.A);
        }
        try {
            sVar = a(this.A, this.y, this.z);
        } catch (o e2) {
            e2.a(this.x, this.z);
            this.i.add(e2);
            sVar = null;
        }
        if (sVar != null) {
            b(sVar, this.z);
        } else {
            k();
        }
    }

    private void b(s<R> sVar, com.a.a.d.a aVar) {
        if (sVar instanceof p) {
            ((p) sVar).a();
        }
        r rVar = null;
        if (this.b.a()) {
            r a2 = r.a(sVar);
            rVar = a2;
            sVar = a2;
        }
        a((s) sVar, aVar);
        this.s = g.ENCODE;
        try {
            if (this.b.a()) {
                this.b.a(this.k, this.g);
            }
        } finally {
            if (rVar != null) {
                rVar.a();
            }
            e();
        }
    }

    private <Data> s<R> a(com.a.a.d.a.b<?> bVar, Data data, com.a.a.d.a aVar) {
        if (data == null) {
            bVar.a();
            return null;
        }
        try {
            long a2 = com.a.a.j.d.a();
            s<R> a3 = a((Object) data, aVar);
            if (Log.isLoggable("DecodeJob", 2)) {
                a("Decoded result " + a3, a2);
            }
            return a3;
        } finally {
            bVar.a();
        }
    }

    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: com.a.a.d.b.q<Data, ?, R>, com.a.a.d.b.q<Data, ResourceType, R> */
    private <Data> s<R> a(Data data, com.a.a.d.a aVar) {
        return a(data, aVar, (q<Data, ?, R>) this.f802a.b(data.getClass()));
    }

    private j a(com.a.a.d.a aVar) {
        j jVar = this.g;
        if (Build.VERSION.SDK_INT < 26 || jVar.a(l.d) != null) {
            return jVar;
        }
        if (aVar != com.a.a.d.a.RESOURCE_DISK_CACHE && !this.f802a.j()) {
            return jVar;
        }
        j jVar2 = new j();
        jVar2.a(this.g);
        jVar2.a(l.d, true);
        return jVar2;
    }

    private <Data, ResourceType> s<R> a(Data data, com.a.a.d.a aVar, q<Data, ResourceType, R> qVar) {
        j a2 = a(aVar);
        com.a.a.d.a.c<Data> b2 = this.n.c().b((Object) data);
        try {
            return qVar.a(b2, a2, this.d, this.e, new b(aVar));
        } finally {
            b2.b();
        }
    }

    private void a(String str, long j2) {
        a(str, j2, (String) null);
    }

    private void a(String str, long j2, String str2) {
        Log.v("DecodeJob", str + " in " + com.a.a.j.d.a(j2) + ", load key: " + this.p + (str2 != null ? ", " + str2 : "") + ", thread: " + Thread.currentThread().getName());
    }

    @Override // com.a.a.j.a.a.c
    public com.a.a.j.a.b a_() {
        return this.j;
    }

    /* access modifiers changed from: private */
    public final class b<Z> implements g.a<Z> {
        private final com.a.a.d.a b;

        b(com.a.a.d.a aVar) {
            this.b = aVar;
        }

        @Override // com.a.a.d.b.g.a
        public s<Z> a(s<Z> sVar) {
            s<Z> sVar2;
            m<Z> mVar;
            com.a.a.d.c cVar;
            com.a.a.d.l<X> lVar;
            h uVar;
            Class<Z> b2 = b(sVar);
            if (this.b != com.a.a.d.a.RESOURCE_DISK_CACHE) {
                mVar = f.this.f802a.c(b2);
                sVar2 = mVar.a(f.this.n, sVar, f.this.d, f.this.e);
            } else {
                sVar2 = sVar;
                mVar = null;
            }
            if (!sVar.equals(sVar2)) {
                sVar.e();
            }
            if (f.this.f802a.a((s<?>) sVar2)) {
                com.a.a.d.l<Z> b3 = f.this.f802a.b(sVar2);
                cVar = b3.a(f.this.g);
                lVar = (com.a.a.d.l<X>) b3;
            } else {
                cVar = com.a.a.d.c.NONE;
                lVar = null;
            }
            if (!f.this.f.a(!f.this.f802a.a(f.this.h), this.b, cVar)) {
                return sVar2;
            }
            if (lVar == null) {
                throw new h.d(sVar2.c().getClass());
            }
            if (cVar == com.a.a.d.c.SOURCE) {
                uVar = new b(f.this.h, f.this.c);
            } else if (cVar == com.a.a.d.c.TRANSFORMED) {
                uVar = new u(f.this.h, f.this.c, f.this.d, f.this.e, mVar, b2, f.this.g);
            } else {
                throw new IllegalArgumentException("Unknown strategy: " + cVar);
            }
            r a2 = r.a(sVar2);
            f.this.b.a(uVar, lVar, a2);
            return a2;
        }

        private Class<Z> b(s<Z> sVar) {
            return (Class<Z>) sVar.c().getClass();
        }
    }

    /* access modifiers changed from: private */
    public static class e {

        /* renamed from: a  reason: collision with root package name */
        private boolean f806a;
        private boolean b;
        private boolean c;

        e() {
        }

        /* access modifiers changed from: package-private */
        public synchronized boolean a(boolean z) {
            this.f806a = true;
            return b(z);
        }

        /* access modifiers changed from: package-private */
        public synchronized boolean a() {
            this.b = true;
            return b(false);
        }

        /* access modifiers changed from: package-private */
        public synchronized boolean b() {
            this.c = true;
            return b(false);
        }

        /* access modifiers changed from: package-private */
        public synchronized void c() {
            this.b = false;
            this.f806a = false;
            this.c = false;
        }

        private boolean b(boolean z) {
            return (this.c || z || this.b) && this.f806a;
        }
    }

    /* access modifiers changed from: private */
    public static class c<Z> {

        /* renamed from: a  reason: collision with root package name */
        private com.a.a.d.h f805a;
        private com.a.a.d.l<Z> b;
        private r<Z> c;

        c() {
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.a.a.d.l<X> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.a.a.d.b.r<X> */
        /* JADX WARN: Multi-variable type inference failed */
        /* access modifiers changed from: package-private */
        public <X> void a(com.a.a.d.h hVar, com.a.a.d.l<X> lVar, r<X> rVar) {
            this.f805a = hVar;
            this.b = lVar;
            this.c = rVar;
        }

        /* access modifiers changed from: package-private */
        public void a(d dVar, j jVar) {
            TraceCompat.beginSection("DecodeJob.encode");
            try {
                dVar.a().a(this.f805a, new c(this.b, this.c, jVar));
            } finally {
                this.c.a();
                TraceCompat.endSection();
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.c != null;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f805a = null;
            this.b = null;
            this.c = null;
        }
    }
}
