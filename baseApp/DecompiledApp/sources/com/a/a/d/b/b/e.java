package com.a.a.d.b.b;

import android.util.Log;
import com.a.a.a.a;
import com.a.a.d.b.b.a;
import com.a.a.d.h;
import java.io.File;
import java.io.IOException;

public class e implements a {

    /* renamed from: a  reason: collision with root package name */
    private static e f785a = null;
    private final j b;
    private final File c;
    private final int d;
    private final c e = new c();
    private a f;

    public static synchronized a a(File file, int i) {
        e eVar;
        synchronized (e.class) {
            if (f785a == null) {
                f785a = new e(file, i);
            }
            eVar = f785a;
        }
        return eVar;
    }

    protected e(File file, int i) {
        this.c = file;
        this.d = i;
        this.b = new j();
    }

    private synchronized a a() {
        if (this.f == null) {
            this.f = a.a(this.c, 1, 1, (long) this.d);
        }
        return this.f;
    }

    @Override // com.a.a.d.b.b.a
    public File a(h hVar) {
        String a2 = this.b.a(hVar);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            Log.v("DiskLruCacheWrapper", "Get: Obtained: " + a2 + " for for Key: " + hVar);
        }
        try {
            a.d a3 = a().a(a2);
            if (a3 != null) {
                return a3.a(0);
            }
            return null;
        } catch (IOException e2) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e2);
            return null;
        }
    }

    @Override // com.a.a.d.b.b.a
    public void a(h hVar, a.b bVar) {
        String a2 = this.b.a(hVar);
        this.e.a(a2);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                Log.v("DiskLruCacheWrapper", "Put: Obtained: " + a2 + " for for Key: " + hVar);
            }
            try {
                com.a.a.a.a a3 = a();
                if (a3.a(a2) == null) {
                    a.b b2 = a3.b(a2);
                    if (b2 == null) {
                        throw new IllegalStateException("Had two simultaneous puts for: " + a2);
                    }
                    try {
                        if (bVar.a(b2.a(0))) {
                            b2.a();
                        }
                        this.e.b(a2);
                    } finally {
                        b2.c();
                    }
                }
            } catch (IOException e2) {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e2);
                }
            }
        } finally {
            this.e.b(a2);
        }
    }
}
