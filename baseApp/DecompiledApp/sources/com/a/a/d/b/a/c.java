package com.a.a.d.b.a;

import android.graphics.Bitmap;
import com.a.a.j.i;

/* access modifiers changed from: package-private */
public class c implements l {

    /* renamed from: a  reason: collision with root package name */
    private final b f770a = new b();
    private final h<a, Bitmap> b = new h<>();

    c() {
    }

    @Override // com.a.a.d.b.a.l
    public void a(Bitmap bitmap) {
        this.b.a(this.f770a.a(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }

    @Override // com.a.a.d.b.a.l
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.b.a(this.f770a.a(i, i2, config));
    }

    @Override // com.a.a.d.b.a.l
    public Bitmap a() {
        return this.b.a();
    }

    @Override // com.a.a.d.b.a.l
    public String b(Bitmap bitmap) {
        return d(bitmap);
    }

    @Override // com.a.a.d.b.a.l
    public String b(int i, int i2, Bitmap.Config config) {
        return c(i, i2, config);
    }

    @Override // com.a.a.d.b.a.l
    public int c(Bitmap bitmap) {
        return i.a(bitmap);
    }

    public String toString() {
        return "AttributeStrategy:\n  " + this.b;
    }

    private static String d(Bitmap bitmap) {
        return c(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    static String c(int i, int i2, Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    static class b extends d<a> {
        b() {
        }

        public a a(int i, int i2, Bitmap.Config config) {
            a aVar = (a) c();
            aVar.a(i, i2, config);
            return aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public a b() {
            return new a(this);
        }
    }

    /* access modifiers changed from: package-private */
    public static class a implements m {

        /* renamed from: a  reason: collision with root package name */
        private final b f771a;
        private int b;
        private int c;
        private Bitmap.Config d;

        public a(b bVar) {
            this.f771a = bVar;
        }

        public void a(int i, int i2, Bitmap.Config config) {
            this.b = i;
            this.c = i2;
            this.d = config;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b == aVar.b && this.c == aVar.c && this.d == aVar.d) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (this.d != null ? this.d.hashCode() : 0) + (((this.b * 31) + this.c) * 31);
        }

        public String toString() {
            return c.c(this.b, this.c, this.d);
        }

        @Override // com.a.a.d.b.a.m
        public void a() {
            this.f771a.a(this);
        }
    }
}
