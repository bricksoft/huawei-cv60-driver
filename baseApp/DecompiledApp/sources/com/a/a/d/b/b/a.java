package com.a.a.d.b.b;

import android.support.annotation.Nullable;
import com.a.a.d.h;
import java.io.File;

public interface a {

    /* renamed from: com.a.a.d.b.b.a$a  reason: collision with other inner class name */
    public interface AbstractC0028a {
        @Nullable
        a a();
    }

    public interface b {
        boolean a(File file);
    }

    @Nullable
    File a(h hVar);

    void a(h hVar, b bVar);
}
