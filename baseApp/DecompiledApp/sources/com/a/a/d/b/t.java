package com.a.a.d.b;

import com.a.a.d.a;
import com.a.a.d.a.b;
import com.a.a.d.b.d;
import com.a.a.d.c.n;
import com.a.a.d.h;
import java.io.File;
import java.util.List;

class t implements b.a<Object>, d {

    /* renamed from: a  reason: collision with root package name */
    private final d.a f826a;
    private final e<?> b;
    private int c = 0;
    private int d = -1;
    private h e;
    private List<n<File, ?>> f;
    private int g;
    private volatile n.a<?> h;
    private File i;
    private u j;

    public t(e<?> eVar, d.a aVar) {
        this.b = eVar;
        this.f826a = aVar;
    }

    @Override // com.a.a.d.b.d
    public boolean a() {
        boolean z;
        List<h> l = this.b.l();
        if (l.isEmpty()) {
            return false;
        }
        List<Class<?>> i2 = this.b.i();
        while (true) {
            if (this.f == null || !c()) {
                this.d++;
                if (this.d >= i2.size()) {
                    this.c++;
                    if (this.c >= l.size()) {
                        return false;
                    }
                    this.d = 0;
                }
                h hVar = l.get(this.c);
                Class<?> cls = i2.get(this.d);
                this.j = new u(hVar, this.b.f(), this.b.g(), this.b.h(), this.b.c(cls), cls, this.b.e());
                this.i = this.b.b().a(this.j);
                if (this.i != null) {
                    this.e = hVar;
                    this.f = this.b.a(this.i);
                    this.g = 0;
                }
            } else {
                this.h = null;
                boolean z2 = false;
                while (!z2 && c()) {
                    List<n<File, ?>> list = this.f;
                    int i3 = this.g;
                    this.g = i3 + 1;
                    this.h = list.get(i3).a(this.i, this.b.g(), this.b.h(), this.b.e());
                    if (this.h == null || !this.b.a((Class<?>) this.h.c.d())) {
                        z = z2;
                    } else {
                        z = true;
                        this.h.c.a(this.b.d(), this);
                    }
                    z2 = z;
                }
                return z2;
            }
        }
    }

    private boolean c() {
        return this.g < this.f.size();
    }

    @Override // com.a.a.d.b.d
    public void b() {
        n.a<?> aVar = this.h;
        if (aVar != null) {
            aVar.c.b();
        }
    }

    @Override // com.a.a.d.a.b.a
    public void a(Object obj) {
        this.f826a.a(this.e, obj, this.h.c, a.RESOURCE_DISK_CACHE, this.j);
    }

    @Override // com.a.a.d.a.b.a
    public void a(Exception exc) {
        this.f826a.a(this.j, exc, this.h.c, a.RESOURCE_DISK_CACHE);
    }
}
