package com.a.a.d.c;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.a.a.d.a.b;
import com.a.a.d.c.n;
import com.a.a.d.j;
import com.a.a.g;
import java.io.File;
import java.io.FileNotFoundException;

public final class k implements n<Uri, File> {

    /* renamed from: a  reason: collision with root package name */
    private final Context f856a;

    k(Context context) {
        this.f856a = context;
    }

    public n.a<File> a(Uri uri, int i, int i2, j jVar) {
        return new n.a<>(new com.a.a.i.b(uri), new b(this.f856a, uri));
    }

    public boolean a(Uri uri) {
        return com.a.a.d.a.a.b.a(uri);
    }

    /* access modifiers changed from: private */
    public static class b implements com.a.a.d.a.b<File> {

        /* renamed from: a  reason: collision with root package name */
        private static final String[] f858a = {"_data"};
        private final Context b;
        private final Uri c;

        b(Context context, Uri uri) {
            this.b = context;
            this.c = uri;
        }

        @Override // com.a.a.d.a.b
        public void a(g gVar, b.a<? super File> aVar) {
            String str = null;
            Cursor query = this.b.getContentResolver().query(this.c, f858a, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(str)) {
                aVar.a((Exception) new FileNotFoundException("Failed to find file path for: " + this.c));
            } else {
                aVar.a(new File(str));
            }
        }

        @Override // com.a.a.d.a.b
        public void a() {
        }

        @Override // com.a.a.d.a.b
        public void b() {
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public Class<File> d() {
            return File.class;
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public com.a.a.d.a c() {
            return com.a.a.d.a.LOCAL;
        }
    }

    public static final class a implements o<Uri, File> {

        /* renamed from: a  reason: collision with root package name */
        private final Context f857a;

        public a(Context context) {
            this.f857a = context;
        }

        @Override // com.a.a.d.c.o
        public n<Uri, File> a(r rVar) {
            return new k(this.f857a);
        }
    }
}
