package com.a.a.d.c;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.a.a.d.a.f;
import com.a.a.d.a.k;
import com.a.a.d.c.n;
import com.a.a.d.j;
import java.io.InputStream;

public class a<Data> implements n<Uri, Data> {

    /* renamed from: a  reason: collision with root package name */
    private static final int f830a = "file:///android_asset/".length();
    private final AssetManager b;
    private final AbstractC0031a<Data> c;

    /* renamed from: com.a.a.d.c.a$a  reason: collision with other inner class name */
    public interface AbstractC0031a<Data> {
        com.a.a.d.a.b<Data> a(AssetManager assetManager, String str);
    }

    public a(AssetManager assetManager, AbstractC0031a<Data> aVar) {
        this.b = assetManager;
        this.c = aVar;
    }

    public n.a<Data> a(Uri uri, int i, int i2, j jVar) {
        return new n.a<>(new com.a.a.i.b(uri), this.c.a(this.b, uri.toString().substring(f830a)));
    }

    public boolean a(Uri uri) {
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }

    public static class c implements AbstractC0031a<InputStream>, o<Uri, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final AssetManager f835a;

        public c(AssetManager assetManager) {
            this.f835a = assetManager;
        }

        @Override // com.a.a.d.c.o
        public n<Uri, InputStream> a(r rVar) {
            return new a(this.f835a, this);
        }

        @Override // com.a.a.d.c.a.AbstractC0031a
        public com.a.a.d.a.b<InputStream> a(AssetManager assetManager, String str) {
            return new k(assetManager, str);
        }
    }

    public static class b implements AbstractC0031a<ParcelFileDescriptor>, o<Uri, ParcelFileDescriptor> {

        /* renamed from: a  reason: collision with root package name */
        private final AssetManager f833a;

        public b(AssetManager assetManager) {
            this.f833a = assetManager;
        }

        @Override // com.a.a.d.c.o
        public n<Uri, ParcelFileDescriptor> a(r rVar) {
            return new a(this.f833a, this);
        }

        @Override // com.a.a.d.c.a.AbstractC0031a
        public com.a.a.d.a.b<ParcelFileDescriptor> a(AssetManager assetManager, String str) {
            return new f(assetManager, str);
        }
    }
}
