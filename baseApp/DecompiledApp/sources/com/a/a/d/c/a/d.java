package com.a.a.d.c.a;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import com.a.a.d.a.a.b;
import com.a.a.d.a.a.c;
import com.a.a.d.c.n;
import com.a.a.d.c.o;
import com.a.a.d.c.r;
import com.a.a.d.d.a.s;
import com.a.a.d.j;
import java.io.InputStream;

public class d implements n<Uri, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final Context f838a;

    d(Context context) {
        this.f838a = context.getApplicationContext();
    }

    @Nullable
    public n.a<InputStream> a(Uri uri, int i, int i2, j jVar) {
        if (!b.a(i, i2) || !a(jVar)) {
            return null;
        }
        return new n.a<>(new com.a.a.i.b(uri), c.b(this.f838a, uri));
    }

    private boolean a(j jVar) {
        Long l = (Long) jVar.a(s.f899a);
        return l != null && l.longValue() == -1;
    }

    public boolean a(Uri uri) {
        return b.b(uri);
    }

    public static class a implements o<Uri, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final Context f839a;

        public a(Context context) {
            this.f839a = context;
        }

        @Override // com.a.a.d.c.o
        public n<Uri, InputStream> a(r rVar) {
            return new d(this.f839a);
        }
    }
}
