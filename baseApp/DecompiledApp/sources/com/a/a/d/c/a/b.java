package com.a.a.d.c.a;

import android.net.Uri;
import com.a.a.d.c.g;
import com.a.a.d.c.n;
import com.a.a.d.c.o;
import com.a.a.d.c.r;
import com.a.a.d.j;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class b implements n<Uri, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private static final Set<String> f834a = Collections.unmodifiableSet(new HashSet(Arrays.asList("http", "https")));
    private final n<g, InputStream> b;

    public b(n<g, InputStream> nVar) {
        this.b = nVar;
    }

    public n.a<InputStream> a(Uri uri, int i, int i2, j jVar) {
        return this.b.a(new g(uri.toString()), i, i2, jVar);
    }

    public boolean a(Uri uri) {
        return f834a.contains(uri.getScheme());
    }

    public static class a implements o<Uri, InputStream> {
        @Override // com.a.a.d.c.o
        public n<Uri, InputStream> a(r rVar) {
            return new b(rVar.a(g.class, InputStream.class));
        }
    }
}
