package com.a.a.d.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pools;
import com.a.a.d.a.b;
import com.a.a.d.b.o;
import com.a.a.d.c.n;
import com.a.a.d.h;
import com.a.a.d.j;
import com.a.a.g;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/* access modifiers changed from: package-private */
public class q<Model, Data> implements n<Model, Data> {

    /* renamed from: a  reason: collision with root package name */
    private final List<n<Model, Data>> f866a;
    private final Pools.Pool<List<Exception>> b;

    q(List<n<Model, Data>> list, Pools.Pool<List<Exception>> pool) {
        this.f866a = list;
        this.b = pool;
    }

    @Override // com.a.a.d.c.n
    public n.a<Data> a(Model model, int i, int i2, j jVar) {
        h hVar;
        n.a<Data> a2;
        int size = this.f866a.size();
        ArrayList arrayList = new ArrayList(size);
        int i3 = 0;
        h hVar2 = null;
        while (i3 < size) {
            n<Model, Data> nVar = this.f866a.get(i3);
            if (!nVar.a(model) || (a2 = nVar.a(model, i, i2, jVar)) == null) {
                hVar = hVar2;
            } else {
                hVar = a2.f862a;
                arrayList.add(a2.c);
            }
            i3++;
            hVar2 = hVar;
        }
        if (!arrayList.isEmpty()) {
            return new n.a<>(hVar2, new a(arrayList, this.b));
        }
        return null;
    }

    @Override // com.a.a.d.c.n
    public boolean a(Model model) {
        for (n<Model, Data> nVar : this.f866a) {
            if (nVar.a(model)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.f866a.toArray(new n[this.f866a.size()])) + '}';
    }

    static class a<Data> implements b<Data>, b.a<Data> {

        /* renamed from: a  reason: collision with root package name */
        private final List<b<Data>> f867a;
        private final Pools.Pool<List<Exception>> b;
        private int c = 0;
        private g d;
        private b.a<? super Data> e;
        @Nullable
        private List<Exception> f;

        a(List<b<Data>> list, Pools.Pool<List<Exception>> pool) {
            this.b = pool;
            com.a.a.j.h.a((Collection) list);
            this.f867a = list;
        }

        @Override // com.a.a.d.a.b
        public void a(g gVar, b.a<? super Data> aVar) {
            this.d = gVar;
            this.e = aVar;
            this.f = this.b.acquire();
            this.f867a.get(this.c).a(gVar, this);
        }

        @Override // com.a.a.d.a.b
        public void a() {
            if (this.f != null) {
                this.b.release(this.f);
            }
            this.f = null;
            for (b<Data> bVar : this.f867a) {
                bVar.a();
            }
        }

        @Override // com.a.a.d.a.b
        public void b() {
            for (b<Data> bVar : this.f867a) {
                bVar.b();
            }
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public Class<Data> d() {
            return this.f867a.get(0).d();
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public com.a.a.d.a c() {
            return this.f867a.get(0).c();
        }

        @Override // com.a.a.d.a.b.a
        public void a(Data data) {
            if (data != null) {
                this.e.a((Object) data);
            } else {
                e();
            }
        }

        @Override // com.a.a.d.a.b.a
        public void a(Exception exc) {
            this.f.add(exc);
            e();
        }

        private void e() {
            if (this.c < this.f867a.size() - 1) {
                this.c++;
                a(this.d, this.e);
                return;
            }
            this.e.a((Exception) new o("Fetch failed", new ArrayList(this.f)));
        }
    }
}
