package com.a.a.d.c.a;

import com.a.a.d.c.g;
import com.a.a.d.c.n;
import com.a.a.d.c.o;
import com.a.a.d.c.r;
import com.a.a.d.j;
import java.io.InputStream;
import java.net.URL;

public class e implements n<URL, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final n<g, InputStream> f840a;

    public e(n<g, InputStream> nVar) {
        this.f840a = nVar;
    }

    public n.a<InputStream> a(URL url, int i, int i2, j jVar) {
        return this.f840a.a(new g(url), i, i2, jVar);
    }

    public boolean a(URL url) {
        return true;
    }

    public static class a implements o<URL, InputStream> {
        @Override // com.a.a.d.c.o
        public n<URL, InputStream> a(r rVar) {
            return new e(rVar.a(g.class, InputStream.class));
        }
    }
}
