package com.a.a.d.c.a;

import android.content.Context;
import android.net.Uri;
import com.a.a.d.a.a.b;
import com.a.a.d.c.n;
import com.a.a.d.c.o;
import com.a.a.d.c.r;
import com.a.a.d.j;
import java.io.InputStream;

public class c implements n<Uri, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    public final Context f836a;

    public c(Context context) {
        this.f836a = context.getApplicationContext();
    }

    public n.a<InputStream> a(Uri uri, int i, int i2, j jVar) {
        if (b.a(i, i2)) {
            return new n.a<>(new com.a.a.i.b(uri), com.a.a.d.a.a.c.a(this.f836a, uri));
        }
        return null;
    }

    public boolean a(Uri uri) {
        return b.c(uri);
    }

    public static class a implements o<Uri, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final Context f837a;

        public a(Context context) {
            this.f837a = context;
        }

        @Override // com.a.a.d.c.o
        public n<Uri, InputStream> a(r rVar) {
            return new c(this.f837a);
        }
    }
}
