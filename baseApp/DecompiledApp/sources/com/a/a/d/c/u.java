package com.a.a.d.c;

import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.a.a.d.c.n;
import com.a.a.d.j;
import java.io.File;
import java.io.InputStream;

public class u<Data> implements n<String, Data> {

    /* renamed from: a  reason: collision with root package name */
    private final n<Uri, Data> f874a;

    public u(n<Uri, Data> nVar) {
        this.f874a = nVar;
    }

    public n.a<Data> a(String str, int i, int i2, j jVar) {
        Uri b2 = b(str);
        if (b2 == null) {
            return null;
        }
        return this.f874a.a(b2, i, i2, jVar);
    }

    public boolean a(String str) {
        return true;
    }

    @Nullable
    private static Uri b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith("/")) {
            return c(str);
        }
        Uri parse = Uri.parse(str);
        if (parse.getScheme() == null) {
            return c(str);
        }
        return parse;
    }

    private static Uri c(String str) {
        return Uri.fromFile(new File(str));
    }

    public static class b implements o<String, InputStream> {
        @Override // com.a.a.d.c.o
        public n<String, InputStream> a(r rVar) {
            return new u(rVar.a(Uri.class, InputStream.class));
        }
    }

    public static class a implements o<String, ParcelFileDescriptor> {
        @Override // com.a.a.d.c.o
        public n<String, ParcelFileDescriptor> a(r rVar) {
            return new u(rVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }
}
