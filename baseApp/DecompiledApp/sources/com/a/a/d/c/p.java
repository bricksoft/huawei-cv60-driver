package com.a.a.d.c;

import android.support.v4.util.Pools;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class p {

    /* renamed from: a  reason: collision with root package name */
    private final r f863a;
    private final a b;

    public p(Pools.Pool<List<Exception>> pool) {
        this(new r(pool));
    }

    p(r rVar) {
        this.b = new a();
        this.f863a = rVar;
    }

    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, o<Model, Data> oVar) {
        this.f863a.a(cls, cls2, oVar);
        this.b.a();
    }

    public synchronized <A> List<n<A, ?>> a(A a2) {
        ArrayList arrayList;
        List<n<A, ?>> b2 = b((Class) b((Object) a2));
        int size = b2.size();
        arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            n<A, ?> nVar = b2.get(i);
            if (nVar.a(a2)) {
                arrayList.add(nVar);
            }
        }
        return arrayList;
    }

    public synchronized List<Class<?>> a(Class<?> cls) {
        return this.f863a.b(cls);
    }

    private <A> List<n<A, ?>> b(Class<A> cls) {
        List<n<A, ?>> a2 = this.b.a(cls);
        if (a2 != null) {
            return a2;
        }
        List<n<A, ?>> unmodifiableList = Collections.unmodifiableList(this.f863a.a(cls));
        this.b.a(cls, unmodifiableList);
        return unmodifiableList;
    }

    private static <A> Class<A> b(A a2) {
        return (Class<A>) a2.getClass();
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final Map<Class<?>, C0034a<?>> f864a = new HashMap();

        a() {
        }

        public void a() {
            this.f864a.clear();
        }

        public <Model> void a(Class<Model> cls, List<n<Model, ?>> list) {
            if (this.f864a.put(cls, new C0034a<>(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }

        public <Model> List<n<Model, ?>> a(Class<Model> cls) {
            C0034a<?> aVar = this.f864a.get(cls);
            if (aVar == null) {
                return null;
            }
            return aVar.f865a;
        }

        /* access modifiers changed from: private */
        /* renamed from: com.a.a.d.c.p$a$a  reason: collision with other inner class name */
        public static class C0034a<Model> {

            /* renamed from: a  reason: collision with root package name */
            final List<n<Model, ?>> f865a;

            public C0034a(List<n<Model, ?>> list) {
                this.f865a = list;
            }
        }
    }
}
