package com.a.a.d.c;

import android.support.annotation.NonNull;
import android.util.Log;
import com.a.a.d.a.b;
import com.a.a.d.c.n;
import com.a.a.d.j;
import com.a.a.g;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class d implements n<File, ByteBuffer> {
    public n.a<ByteBuffer> a(File file, int i, int i2, j jVar) {
        return new n.a<>(new com.a.a.i.b(file), new a(file));
    }

    public boolean a(File file) {
        return true;
    }

    public static class b implements o<File, ByteBuffer> {
        @Override // com.a.a.d.c.o
        public n<File, ByteBuffer> a(r rVar) {
            return new d();
        }
    }

    /* access modifiers changed from: private */
    public static class a implements com.a.a.d.a.b<ByteBuffer> {

        /* renamed from: a  reason: collision with root package name */
        private final File f845a;

        public a(File file) {
            this.f845a = file;
        }

        @Override // com.a.a.d.a.b
        public void a(g gVar, b.a<? super ByteBuffer> aVar) {
            try {
                aVar.a(com.a.a.j.a.a(this.f845a));
            } catch (IOException e) {
                if (Log.isLoggable("ByteBufferFileLoader", 3)) {
                    Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e);
                }
                aVar.a((Exception) e);
            }
        }

        @Override // com.a.a.d.a.b
        public void a() {
        }

        @Override // com.a.a.d.a.b
        public void b() {
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public Class<ByteBuffer> d() {
            return ByteBuffer.class;
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public com.a.a.d.a c() {
            return com.a.a.d.a.LOCAL;
        }
    }
}
