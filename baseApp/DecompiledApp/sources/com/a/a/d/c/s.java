package com.a.a.d.c;

import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.util.Log;
import com.a.a.d.c.n;
import com.a.a.d.j;
import java.io.InputStream;

public class s<Data> implements n<Integer, Data> {

    /* renamed from: a  reason: collision with root package name */
    private final n<Uri, Data> f870a;
    private final Resources b;

    public s(Resources resources, n<Uri, Data> nVar) {
        this.b = resources;
        this.f870a = nVar;
    }

    public n.a<Data> a(Integer num, int i, int i2, j jVar) {
        Uri b2 = b(num);
        if (b2 == null) {
            return null;
        }
        return this.f870a.a(b2, i, i2, jVar);
    }

    @Nullable
    private Uri b(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (Log.isLoggable("ResourceLoader", 5)) {
                Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            }
            return null;
        }
    }

    public boolean a(Integer num) {
        return true;
    }

    public static class b implements o<Integer, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final Resources f872a;

        public b(Resources resources) {
            this.f872a = resources;
        }

        @Override // com.a.a.d.c.o
        public n<Integer, InputStream> a(r rVar) {
            return new s(this.f872a, rVar.a(Uri.class, InputStream.class));
        }
    }

    public static class a implements o<Integer, ParcelFileDescriptor> {

        /* renamed from: a  reason: collision with root package name */
        private final Resources f871a;

        public a(Resources resources) {
            this.f871a = resources;
        }

        @Override // com.a.a.d.c.o
        public n<Integer, ParcelFileDescriptor> a(r rVar) {
            return new s(this.f871a, rVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }
}
