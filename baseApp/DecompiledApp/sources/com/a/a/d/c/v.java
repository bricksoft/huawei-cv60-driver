package com.a.a.d.c;

import android.support.annotation.NonNull;
import com.a.a.d.a.b;
import com.a.a.d.c.n;
import com.a.a.d.j;
import com.a.a.g;

public class v<Model> implements n<Model, Model> {
    @Override // com.a.a.d.c.n
    public n.a<Model> a(Model model, int i, int i2, j jVar) {
        return new n.a<>(new com.a.a.i.b(model), new b(model));
    }

    @Override // com.a.a.d.c.n
    public boolean a(Model model) {
        return true;
    }

    private static class b<Model> implements com.a.a.d.a.b<Model> {

        /* renamed from: a  reason: collision with root package name */
        private final Model f875a;

        public b(Model model) {
            this.f875a = model;
        }

        @Override // com.a.a.d.a.b
        public void a(g gVar, b.a<? super Model> aVar) {
            aVar.a((Object) this.f875a);
        }

        @Override // com.a.a.d.a.b
        public void a() {
        }

        @Override // com.a.a.d.a.b
        public void b() {
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public Class<Model> d() {
            return (Class<Model>) this.f875a.getClass();
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public com.a.a.d.a c() {
            return com.a.a.d.a.LOCAL;
        }
    }

    public static class a<Model> implements o<Model, Model> {
        @Override // com.a.a.d.c.o
        public n<Model, Model> a(r rVar) {
            return new v();
        }
    }
}
