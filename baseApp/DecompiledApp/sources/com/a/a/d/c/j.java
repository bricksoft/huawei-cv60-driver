package com.a.a.d.c;

import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class j implements h {
    private final Map<String, List<i>> c;
    private volatile Map<String, String> d;

    j(Map<String, List<i>> map) {
        this.c = Collections.unmodifiableMap(map);
    }

    @Override // com.a.a.d.c.h
    public Map<String, String> a() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    this.d = Collections.unmodifiableMap(b());
                }
            }
        }
        return this.d;
    }

    private Map<String, String> b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry<String, List<i>> entry : this.c.entrySet()) {
            StringBuilder sb = new StringBuilder();
            List<i> value = entry.getValue();
            int size = value.size();
            for (int i = 0; i < size; i++) {
                String a2 = value.get(i).a();
                if (!TextUtils.isEmpty(a2)) {
                    sb.append(a2);
                    if (i != value.size() - 1) {
                        sb.append(',');
                    }
                }
            }
            if (!TextUtils.isEmpty(sb.toString())) {
                hashMap.put(entry.getKey(), sb.toString());
            }
        }
        return hashMap;
    }

    public String toString() {
        return "LazyHeaders{headers=" + this.c + '}';
    }

    public boolean equals(Object obj) {
        if (obj instanceof j) {
            return this.c.equals(((j) obj).c);
        }
        return false;
    }

    public int hashCode() {
        return this.c.hashCode();
    }

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private static final String f854a = b();
        private static final Map<String, List<i>> b;
        private boolean c = true;
        private Map<String, List<i>> d = b;
        private boolean e = true;

        static {
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(f854a)) {
                hashMap.put("User-Agent", Collections.singletonList(new b(f854a)));
            }
            b = Collections.unmodifiableMap(hashMap);
        }

        public j a() {
            this.c = true;
            return new j(this.d);
        }

        @VisibleForTesting
        static String b() {
            String property = System.getProperty("http.agent");
            if (TextUtils.isEmpty(property)) {
                return property;
            }
            int length = property.length();
            StringBuilder sb = new StringBuilder(property.length());
            for (int i = 0; i < length; i++) {
                char charAt = property.charAt(i);
                if ((charAt > 31 || charAt == '\t') && charAt < 127) {
                    sb.append(charAt);
                } else {
                    sb.append('?');
                }
            }
            return sb.toString();
        }
    }

    static final class b implements i {

        /* renamed from: a  reason: collision with root package name */
        private final String f855a;

        b(String str) {
            this.f855a = str;
        }

        @Override // com.a.a.d.c.i
        public String a() {
            return this.f855a;
        }

        public String toString() {
            return "StringHeaderFactory{value='" + this.f855a + '\'' + '}';
        }

        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return this.f855a.equals(((b) obj).f855a);
            }
            return false;
        }

        public int hashCode() {
            return this.f855a.hashCode();
        }
    }
}
