package com.a.a.d.c;

import android.support.annotation.Nullable;
import android.support.v4.util.Pools;
import com.a.a.d.c.n;
import com.a.a.d.j;
import com.a.a.h;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class r {

    /* renamed from: a  reason: collision with root package name */
    private static final c f868a = new c();
    private static final n<Object, Object> b = new a();
    private final List<b<?, ?>> c;
    private final c d;
    private final Set<b<?, ?>> e;
    private final Pools.Pool<List<Exception>> f;

    public r(Pools.Pool<List<Exception>> pool) {
        this(pool, f868a);
    }

    r(Pools.Pool<List<Exception>> pool, c cVar) {
        this.c = new ArrayList();
        this.e = new HashSet();
        this.f = pool;
        this.d = cVar;
    }

    /* access modifiers changed from: package-private */
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, o<Model, Data> oVar) {
        a(cls, cls2, oVar, true);
    }

    private <Model, Data> void a(Class<Model> cls, Class<Data> cls2, o<Model, Data> oVar, boolean z) {
        this.c.add(z ? this.c.size() : 0, new b<>(cls, cls2, oVar));
    }

    /* access modifiers changed from: package-private */
    public synchronized <Model> List<n<Model, ?>> a(Class<Model> cls) {
        ArrayList arrayList;
        try {
            arrayList = new ArrayList();
            for (b<?, ?> bVar : this.c) {
                if (!this.e.contains(bVar) && bVar.a(cls)) {
                    this.e.add(bVar);
                    arrayList.add(a(bVar));
                    this.e.remove(bVar);
                }
            }
        } catch (Throwable th) {
            this.e.clear();
            throw th;
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public synchronized List<Class<?>> b(Class<?> cls) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (b<?, ?> bVar : this.c) {
            if (!arrayList.contains(bVar.f869a) && bVar.a(cls)) {
                arrayList.add(bVar.f869a);
            }
        }
        return arrayList;
    }

    public synchronized <Model, Data> n<Model, Data> a(Class<Model> cls, Class<Data> cls2) {
        n<Model, Data> a2;
        try {
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (b<?, ?> bVar : this.c) {
                if (this.e.contains(bVar)) {
                    z = true;
                } else if (bVar.a(cls, cls2)) {
                    this.e.add(bVar);
                    arrayList.add(a(bVar));
                    this.e.remove(bVar);
                }
            }
            if (arrayList.size() > 1) {
                a2 = this.d.a(arrayList, this.f);
            } else if (arrayList.size() == 1) {
                a2 = (n) arrayList.get(0);
            } else if (z) {
                a2 = a();
            } else {
                throw new h.c(cls, cls2);
            }
        } catch (Throwable th) {
            this.e.clear();
            throw th;
        }
        return a2;
    }

    private <Model, Data> n<Model, Data> a(b<?, ?> bVar) {
        return (n) com.a.a.j.h.a(bVar.b.a(this));
    }

    private static <Model, Data> n<Model, Data> a() {
        return (n<Model, Data>) b;
    }

    /* access modifiers changed from: private */
    public static class b<Model, Data> {

        /* renamed from: a  reason: collision with root package name */
        final Class<Data> f869a;
        final o<Model, Data> b;
        private final Class<Model> c;

        public b(Class<Model> cls, Class<Data> cls2, o<Model, Data> oVar) {
            this.c = cls;
            this.f869a = cls2;
            this.b = oVar;
        }

        public boolean a(Class<?> cls, Class<?> cls2) {
            return a(cls) && this.f869a.isAssignableFrom(cls2);
        }

        public boolean a(Class<?> cls) {
            return this.c.isAssignableFrom(cls);
        }
    }

    /* access modifiers changed from: package-private */
    public static class c {
        c() {
        }

        public <Model, Data> q<Model, Data> a(List<n<Model, Data>> list, Pools.Pool<List<Exception>> pool) {
            return new q<>(list, pool);
        }
    }

    private static class a implements n<Object, Object> {
        a() {
        }

        @Override // com.a.a.d.c.n
        @Nullable
        public n.a<Object> a(Object obj, int i, int i2, j jVar) {
            return null;
        }

        @Override // com.a.a.d.c.n
        public boolean a(Object obj) {
            return false;
        }
    }
}
