package com.a.a.d.c;

import android.support.annotation.Nullable;
import com.a.a.d.a.b;
import com.a.a.d.h;
import com.a.a.d.j;
import java.util.Collections;
import java.util.List;

public interface n<Model, Data> {
    @Nullable
    a<Data> a(Model model, int i, int i2, j jVar);

    boolean a(Model model);

    public static class a<Data> {

        /* renamed from: a  reason: collision with root package name */
        public final h f862a;
        public final List<h> b;
        public final b<Data> c;

        public a(h hVar, b<Data> bVar) {
            this(hVar, Collections.emptyList(), bVar);
        }

        public a(h hVar, List<h> list, b<Data> bVar) {
            this.f862a = (h) com.a.a.j.h.a(hVar);
            this.b = (List) com.a.a.j.h.a((Object) list);
            this.c = (b) com.a.a.j.h.a(bVar);
        }
    }
}
