package com.a.a.d.c;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.a.a.d.a.g;
import com.a.a.d.a.l;
import com.a.a.d.c.n;
import com.a.a.d.j;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class w<Data> implements n<Uri, Data> {

    /* renamed from: a  reason: collision with root package name */
    private static final Set<String> f876a = Collections.unmodifiableSet(new HashSet(Arrays.asList("file", "android.resource", "content")));
    private final b<Data> b;

    public interface b<Data> {
        com.a.a.d.a.b<Data> a(Uri uri);
    }

    public w(b<Data> bVar) {
        this.b = bVar;
    }

    public n.a<Data> a(Uri uri, int i, int i2, j jVar) {
        return new n.a<>(new com.a.a.i.b(uri), this.b.a(uri));
    }

    public boolean a(Uri uri) {
        return f876a.contains(uri.getScheme());
    }

    public static class c implements o<Uri, InputStream>, b<InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final ContentResolver f878a;

        public c(ContentResolver contentResolver) {
            this.f878a = contentResolver;
        }

        @Override // com.a.a.d.c.w.b
        public com.a.a.d.a.b<InputStream> a(Uri uri) {
            return new l(this.f878a, uri);
        }

        @Override // com.a.a.d.c.o
        public n<Uri, InputStream> a(r rVar) {
            return new w(this);
        }
    }

    public static class a implements o<Uri, ParcelFileDescriptor>, b<ParcelFileDescriptor> {

        /* renamed from: a  reason: collision with root package name */
        private final ContentResolver f877a;

        public a(ContentResolver contentResolver) {
            this.f877a = contentResolver;
        }

        @Override // com.a.a.d.c.w.b
        public com.a.a.d.a.b<ParcelFileDescriptor> a(Uri uri) {
            return new g(this.f877a, uri);
        }

        @Override // com.a.a.d.c.o
        public n<Uri, ParcelFileDescriptor> a(r rVar) {
            return new w(this);
        }
    }
}
