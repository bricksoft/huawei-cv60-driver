package com.a.a.d.c;

import android.support.annotation.NonNull;
import com.a.a.d.a.b;
import com.a.a.d.c.n;
import com.a.a.d.j;
import com.a.a.g;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class b<Data> implements n<byte[], Data> {

    /* renamed from: a  reason: collision with root package name */
    private final AbstractC0033b<Data> f841a;

    /* renamed from: com.a.a.d.c.b$b  reason: collision with other inner class name */
    public interface AbstractC0033b<Data> {
        Class<Data> a();

        Data b(byte[] bArr);
    }

    public b(AbstractC0033b<Data> bVar) {
        this.f841a = bVar;
    }

    public n.a<Data> a(byte[] bArr, int i, int i2, j jVar) {
        return new n.a<>(com.a.a.i.a.a(), new c(bArr, this.f841a));
    }

    public boolean a(byte[] bArr) {
        return true;
    }

    /* access modifiers changed from: private */
    public static class c<Data> implements com.a.a.d.a.b<Data> {

        /* renamed from: a  reason: collision with root package name */
        private final byte[] f843a;
        private final AbstractC0033b<Data> b;

        public c(byte[] bArr, AbstractC0033b<Data> bVar) {
            this.f843a = bArr;
            this.b = bVar;
        }

        @Override // com.a.a.d.a.b
        public void a(g gVar, b.a<? super Data> aVar) {
            aVar.a((Object) this.b.b(this.f843a));
        }

        @Override // com.a.a.d.a.b
        public void a() {
        }

        @Override // com.a.a.d.a.b
        public void b() {
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public Class<Data> d() {
            return this.b.a();
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public com.a.a.d.a c() {
            return com.a.a.d.a.LOCAL;
        }
    }

    public static class a implements o<byte[], ByteBuffer> {
        @Override // com.a.a.d.c.o
        public n<byte[], ByteBuffer> a(r rVar) {
            return new b(new AbstractC0033b<ByteBuffer>() {
                /* class com.a.a.d.c.b.a.AnonymousClass1 */

                /* renamed from: a */
                public ByteBuffer b(byte[] bArr) {
                    return ByteBuffer.wrap(bArr);
                }

                @Override // com.a.a.d.c.b.AbstractC0033b
                public Class<ByteBuffer> a() {
                    return ByteBuffer.class;
                }
            });
        }
    }

    public static class d implements o<byte[], InputStream> {
        @Override // com.a.a.d.c.o
        public n<byte[], InputStream> a(r rVar) {
            return new b(new AbstractC0033b<InputStream>() {
                /* class com.a.a.d.c.b.d.AnonymousClass1 */

                /* renamed from: a */
                public InputStream b(byte[] bArr) {
                    return new ByteArrayInputStream(bArr);
                }

                @Override // com.a.a.d.c.b.AbstractC0033b
                public Class<InputStream> a() {
                    return InputStream.class;
                }
            });
        }
    }
}
