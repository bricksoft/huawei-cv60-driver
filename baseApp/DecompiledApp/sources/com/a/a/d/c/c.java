package com.a.a.d.c;

import android.util.Log;
import com.a.a.d.d;
import com.a.a.d.j;
import com.a.a.j.a;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class c implements d<ByteBuffer> {
    public boolean a(ByteBuffer byteBuffer, File file, j jVar) {
        try {
            a.a(byteBuffer, file);
            return true;
        } catch (IOException e) {
            if (!Log.isLoggable("ByteBufferEncoder", 3)) {
                return false;
            }
            Log.d("ByteBufferEncoder", "Failed to write data", e);
            return false;
        }
    }
}
