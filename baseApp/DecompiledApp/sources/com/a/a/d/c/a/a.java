package com.a.a.d.c.a;

import android.support.annotation.Nullable;
import com.a.a.d.a.h;
import com.a.a.d.c.g;
import com.a.a.d.c.m;
import com.a.a.d.c.n;
import com.a.a.d.c.o;
import com.a.a.d.c.r;
import com.a.a.d.i;
import com.a.a.d.j;
import com.google.android.exoplayer.ExoPlayer;
import java.io.InputStream;

public class a implements n<g, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    public static final i<Integer> f831a = i.a("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", Integer.valueOf((int) ExoPlayer.Factory.DEFAULT_MIN_BUFFER_MS));
    @Nullable
    private final m<g, g> b;

    public a() {
        this(null);
    }

    public a(m<g, g> mVar) {
        this.b = mVar;
    }

    public n.a<InputStream> a(g gVar, int i, int i2, j jVar) {
        if (this.b != null) {
            g a2 = this.b.a(gVar, 0, 0);
            if (a2 == null) {
                this.b.a(gVar, 0, 0, gVar);
            } else {
                gVar = a2;
            }
        }
        return new n.a<>(gVar, new h(gVar, ((Integer) jVar.a(f831a)).intValue()));
    }

    public boolean a(g gVar) {
        return true;
    }

    /* renamed from: com.a.a.d.c.a.a$a  reason: collision with other inner class name */
    public static class C0032a implements o<g, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final m<g, g> f832a = new m<>(500);

        @Override // com.a.a.d.c.o
        public n<g, InputStream> a(r rVar) {
            return new a(this.f832a);
        }
    }
}
