package com.a.a.d.c;

import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.util.Log;
import com.a.a.d.a.b;
import com.a.a.d.c.n;
import com.a.a.d.j;
import com.a.a.g;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class f<Data> implements n<File, Data> {

    /* renamed from: a  reason: collision with root package name */
    private final d<Data> f850a;

    public interface d<Data> {
        Class<Data> a();

        void a(Data data);

        Data b(File file);
    }

    public f(d<Data> dVar) {
        this.f850a = dVar;
    }

    public n.a<Data> a(File file, int i, int i2, j jVar) {
        return new n.a<>(new com.a.a.i.b(file), new c(file, this.f850a));
    }

    public boolean a(File file) {
        return true;
    }

    /* access modifiers changed from: private */
    public static class c<Data> implements com.a.a.d.a.b<Data> {

        /* renamed from: a  reason: collision with root package name */
        private final File f852a;
        private final d<Data> b;
        private Data c;

        public c(File file, d<Data> dVar) {
            this.f852a = file;
            this.b = dVar;
        }

        @Override // com.a.a.d.a.b
        public void a(g gVar, b.a<? super Data> aVar) {
            try {
                this.c = this.b.b(this.f852a);
                aVar.a((Object) this.c);
            } catch (FileNotFoundException e) {
                if (Log.isLoggable("FileLoader", 3)) {
                    Log.d("FileLoader", "Failed to open file", e);
                }
                aVar.a((Exception) e);
            }
        }

        @Override // com.a.a.d.a.b
        public void a() {
            if (this.c != null) {
                try {
                    this.b.a(this.c);
                } catch (IOException e) {
                }
            }
        }

        @Override // com.a.a.d.a.b
        public void b() {
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public Class<Data> d() {
            return this.b.a();
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public com.a.a.d.a c() {
            return com.a.a.d.a.LOCAL;
        }
    }

    public static class a<Data> implements o<File, Data> {

        /* renamed from: a  reason: collision with root package name */
        private final d<Data> f851a;

        public a(d<Data> dVar) {
            this.f851a = dVar;
        }

        @Override // com.a.a.d.c.o
        public final n<File, Data> a(r rVar) {
            return new f(this.f851a);
        }
    }

    public static class e extends a<InputStream> {
        public e() {
            super(new d<InputStream>() {
                /* class com.a.a.d.c.f.e.AnonymousClass1 */

                /* renamed from: a */
                public InputStream b(File file) {
                    return new FileInputStream(file);
                }

                public void a(InputStream inputStream) {
                    inputStream.close();
                }

                @Override // com.a.a.d.c.f.d
                public Class<InputStream> a() {
                    return InputStream.class;
                }
            });
        }
    }

    public static class b extends a<ParcelFileDescriptor> {
        public b() {
            super(new d<ParcelFileDescriptor>() {
                /* class com.a.a.d.c.f.b.AnonymousClass1 */

                /* renamed from: a */
                public ParcelFileDescriptor b(File file) {
                    return ParcelFileDescriptor.open(file, 268435456);
                }

                public void a(ParcelFileDescriptor parcelFileDescriptor) {
                    parcelFileDescriptor.close();
                }

                @Override // com.a.a.d.c.f.d
                public Class<ParcelFileDescriptor> a() {
                    return ParcelFileDescriptor.class;
                }
            });
        }
    }
}
