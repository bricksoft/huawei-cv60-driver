package com.a.a.d.c;

import android.support.annotation.NonNull;
import android.util.Base64;
import com.a.a.d.a.b;
import com.a.a.d.c.n;
import com.a.a.d.j;
import com.a.a.g;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class e<Data> implements n<String, Data> {

    /* renamed from: a  reason: collision with root package name */
    private final a<Data> f846a;

    public interface a<Data> {
        Class<Data> a();

        Data a(String str);

        void a(Data data);
    }

    public e(a<Data> aVar) {
        this.f846a = aVar;
    }

    public n.a<Data> a(String str, int i, int i2, j jVar) {
        return new n.a<>(new com.a.a.i.b(str), new b(str, this.f846a));
    }

    public boolean a(String str) {
        return str.startsWith("data:image");
    }

    /* access modifiers changed from: private */
    public static final class b<Data> implements com.a.a.d.a.b<Data> {

        /* renamed from: a  reason: collision with root package name */
        private final String f847a;
        private final a<Data> b;
        private Data c;

        public b(String str, a<Data> aVar) {
            this.f847a = str;
            this.b = aVar;
        }

        @Override // com.a.a.d.a.b
        public void a(g gVar, b.a<? super Data> aVar) {
            try {
                this.c = this.b.a(this.f847a);
                aVar.a((Object) this.c);
            } catch (IllegalArgumentException e) {
                aVar.a((Exception) e);
            }
        }

        @Override // com.a.a.d.a.b
        public void a() {
            try {
                this.b.a((Object) this.c);
            } catch (IOException e) {
            }
        }

        @Override // com.a.a.d.a.b
        public void b() {
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public Class<Data> d() {
            return this.b.a();
        }

        @Override // com.a.a.d.a.b
        @NonNull
        public com.a.a.d.a c() {
            return com.a.a.d.a.LOCAL;
        }
    }

    public static final class c implements o<String, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final a<InputStream> f848a = new a<InputStream>() {
            /* class com.a.a.d.c.e.c.AnonymousClass1 */

            /* renamed from: b */
            public InputStream a(String str) {
                if (!str.startsWith("data:image")) {
                    throw new IllegalArgumentException("Not a valid image data URL.");
                }
                int indexOf = str.indexOf(44);
                if (indexOf == -1) {
                    throw new IllegalArgumentException("Missing comma in data URL.");
                } else if (str.substring(0, indexOf).endsWith(";base64")) {
                    return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                } else {
                    throw new IllegalArgumentException("Not a base64 image data URL.");
                }
            }

            public void a(InputStream inputStream) {
                inputStream.close();
            }

            @Override // com.a.a.d.c.e.a
            public Class<InputStream> a() {
                return InputStream.class;
            }
        };

        @Override // com.a.a.d.c.o
        public final n<String, InputStream> a(r rVar) {
            return new e(this.f848a);
        }
    }
}
