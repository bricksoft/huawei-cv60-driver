package com.a.a.d.c;

import android.support.annotation.Nullable;
import com.a.a.j.e;
import com.a.a.j.i;
import java.util.Queue;

public class m<A, B> {

    /* renamed from: a  reason: collision with root package name */
    private final e<a<A>, B> f859a;

    public m() {
        this(250);
    }

    public m(int i) {
        this.f859a = new e<a<A>, B>(i) {
            /* class com.a.a.d.c.m.AnonymousClass1 */

            /* access modifiers changed from: protected */
            @Override // com.a.a.j.e
            public /* bridge */ /* synthetic */ void a(Object obj, Object obj2) {
                a((a) ((a) obj), obj2);
            }

            /* access modifiers changed from: protected */
            public void a(a<A> aVar, B b) {
                aVar.a();
            }
        };
    }

    @Nullable
    public B a(A a2, int i, int i2) {
        a<A> a3 = a.a(a2, i, i2);
        B b = this.f859a.b(a3);
        a3.a();
        return b;
    }

    public void a(A a2, int i, int i2, B b) {
        this.f859a.b(a.a(a2, i, i2), b);
    }

    /* access modifiers changed from: package-private */
    public static final class a<A> {

        /* renamed from: a  reason: collision with root package name */
        private static final Queue<a<?>> f861a = i.a(0);
        private int b;
        private int c;
        private A d;

        static <A> a<A> a(A a2, int i, int i2) {
            a<A> aVar;
            synchronized (f861a) {
                aVar = (a<A>) f861a.poll();
            }
            if (aVar == null) {
                aVar = new a<>();
            }
            aVar.b(a2, i, i2);
            return aVar;
        }

        private a() {
        }

        private void b(A a2, int i, int i2) {
            this.d = a2;
            this.c = i;
            this.b = i2;
        }

        public void a() {
            synchronized (f861a) {
                f861a.offer(this);
            }
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.c == aVar.c && this.b == aVar.b && this.d.equals(aVar.d)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (((this.b * 31) + this.c) * 31) + this.d.hashCode();
        }
    }
}
