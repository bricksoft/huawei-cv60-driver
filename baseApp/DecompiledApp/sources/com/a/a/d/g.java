package com.a.a.d;

import android.support.annotation.Nullable;
import com.a.a.d.b.a.b;
import com.a.a.d.d.a.p;
import com.a.a.d.f;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

public final class g {
    public static f.a a(List<f> list, @Nullable InputStream inputStream, b bVar) {
        if (inputStream == null) {
            return f.a.UNKNOWN;
        }
        if (!inputStream.markSupported()) {
            inputStream = new p(inputStream, bVar);
        }
        inputStream.mark(5242880);
        for (f fVar : list) {
            try {
                f.a a2 = fVar.a(inputStream);
                if (a2 != f.a.UNKNOWN) {
                    inputStream.reset();
                    return a2;
                }
            } finally {
                inputStream.reset();
            }
        }
        return f.a.UNKNOWN;
    }

    public static f.a a(List<f> list, @Nullable ByteBuffer byteBuffer) {
        if (byteBuffer == null) {
            return f.a.UNKNOWN;
        }
        for (f fVar : list) {
            f.a a2 = fVar.a(byteBuffer);
            if (a2 != f.a.UNKNOWN) {
                return a2;
            }
        }
        return f.a.UNKNOWN;
    }

    public static int b(List<f> list, @Nullable InputStream inputStream, b bVar) {
        if (inputStream == null) {
            return -1;
        }
        if (!inputStream.markSupported()) {
            inputStream = new p(inputStream, bVar);
        }
        inputStream.mark(5242880);
        for (f fVar : list) {
            try {
                int a2 = fVar.a(inputStream, bVar);
                if (a2 != -1) {
                    inputStream.reset();
                    return a2;
                }
            } finally {
                inputStream.reset();
            }
        }
        return -1;
    }
}
