package com.a.a.d;

import android.support.v4.util.ArrayMap;
import android.support.v4.util.SimpleArrayMap;
import java.security.MessageDigest;
import java.util.Map;

public final class j implements h {
    private final ArrayMap<i<?>, Object> b = new ArrayMap<>();

    public void a(j jVar) {
        this.b.putAll((SimpleArrayMap<? extends i<?>, ? extends Object>) jVar.b);
    }

    public <T> j a(i<T> iVar, T t) {
        this.b.put(iVar, t);
        return this;
    }

    public <T> T a(i<T> iVar) {
        return this.b.containsKey(iVar) ? (T) this.b.get(iVar) : iVar.a();
    }

    @Override // com.a.a.d.h
    public boolean equals(Object obj) {
        if (obj instanceof j) {
            return this.b.equals(((j) obj).b);
        }
        return false;
    }

    @Override // com.a.a.d.h
    public int hashCode() {
        return this.b.hashCode();
    }

    @Override // com.a.a.d.h
    public void a(MessageDigest messageDigest) {
        for (Map.Entry<i<?>, Object> entry : this.b.entrySet()) {
            a(entry.getKey(), entry.getValue(), messageDigest);
        }
    }

    public String toString() {
        return "Options{values=" + this.b + '}';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    private static <T> void a(i<T> iVar, Object obj, MessageDigest messageDigest) {
        iVar.a(obj, messageDigest);
    }
}
