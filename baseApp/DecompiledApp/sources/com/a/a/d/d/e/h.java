package com.a.a.d.d.e;

import android.graphics.Bitmap;
import com.a.a.b.a;
import com.a.a.d.b.a.e;
import com.a.a.d.b.s;
import com.a.a.d.j;
import com.a.a.d.k;

public final class h implements k<a, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final e f913a;

    public h(e eVar) {
        this.f913a = eVar;
    }

    public boolean a(a aVar, j jVar) {
        return true;
    }

    public s<Bitmap> a(a aVar, int i, int i2, j jVar) {
        return com.a.a.d.d.a.e.a(aVar.h(), this.f913a);
    }
}
