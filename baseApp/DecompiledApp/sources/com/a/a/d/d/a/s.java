package com.a.a.d.d.a;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.ParcelFileDescriptor;
import com.a.a.d.b.a.e;
import com.a.a.d.i;
import com.a.a.d.j;
import com.a.a.d.k;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class s implements k<ParcelFileDescriptor, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    public static final i<Long> f899a = i.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", -1L, new i.a<Long>() {
        /* class com.a.a.d.d.a.s.AnonymousClass1 */

        /* renamed from: a  reason: collision with root package name */
        private final ByteBuffer f900a = ByteBuffer.allocate(8);

        public void a(byte[] bArr, Long l, MessageDigest messageDigest) {
            messageDigest.update(bArr);
            synchronized (this.f900a) {
                this.f900a.position(0);
                messageDigest.update(this.f900a.putLong(l.longValue()).array());
            }
        }
    });
    public static final i<Integer> b = i.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", null, new i.a<Integer>() {
        /* class com.a.a.d.d.a.s.AnonymousClass2 */

        /* renamed from: a  reason: collision with root package name */
        private final ByteBuffer f901a = ByteBuffer.allocate(4);

        public void a(byte[] bArr, Integer num, MessageDigest messageDigest) {
            if (num != null) {
                messageDigest.update(bArr);
                synchronized (this.f901a) {
                    this.f901a.position(0);
                    messageDigest.update(this.f901a.putInt(num.intValue()).array());
                }
            }
        }
    });
    private static final a c = new a();
    private final e d;
    private final a e;

    public s(e eVar) {
        this(eVar, c);
    }

    s(e eVar, a aVar) {
        this.d = eVar;
        this.e = aVar;
    }

    public boolean a(ParcelFileDescriptor parcelFileDescriptor, j jVar) {
        return true;
    }

    public com.a.a.d.b.s<Bitmap> a(ParcelFileDescriptor parcelFileDescriptor, int i, int i2, j jVar) {
        Bitmap frameAtTime;
        long longValue = ((Long) jVar.a(f899a)).longValue();
        if (longValue >= 0 || longValue == -1) {
            Integer num = (Integer) jVar.a(b);
            MediaMetadataRetriever a2 = this.e.a();
            try {
                a2.setDataSource(parcelFileDescriptor.getFileDescriptor());
                if (longValue == -1) {
                    frameAtTime = a2.getFrameAtTime();
                } else if (num == null) {
                    frameAtTime = a2.getFrameAtTime(longValue);
                } else {
                    frameAtTime = a2.getFrameAtTime(longValue, num.intValue());
                }
                a2.release();
                parcelFileDescriptor.close();
                return e.a(frameAtTime, this.d);
            } catch (RuntimeException e2) {
                throw new IOException(e2);
            } catch (Throwable th) {
                a2.release();
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Requested frame must be non-negative, or DEFAULT_FRAME, given: " + longValue);
        }
    }

    /* access modifiers changed from: package-private */
    public static class a {
        a() {
        }

        public MediaMetadataRetriever a() {
            return new MediaMetadataRetriever();
        }
    }
}
