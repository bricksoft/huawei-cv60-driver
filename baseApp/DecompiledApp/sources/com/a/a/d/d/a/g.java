package com.a.a.d.d.a;

import android.graphics.Bitmap;
import com.a.a.d.b.s;
import com.a.a.d.j;
import com.a.a.d.k;
import com.a.a.j.a;
import java.nio.ByteBuffer;

public class g implements k<ByteBuffer, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final l f885a;

    public g(l lVar) {
        this.f885a = lVar;
    }

    public boolean a(ByteBuffer byteBuffer, j jVar) {
        return this.f885a.a(byteBuffer);
    }

    public s<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, j jVar) {
        return this.f885a.a(a.b(byteBuffer), i, i2, jVar);
    }
}
