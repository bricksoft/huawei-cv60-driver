package com.a.a.d.d.c;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.a.a.d.b.p;
import com.a.a.d.b.s;
import com.a.a.d.d.e.c;
import com.a.a.j.h;

public abstract class a<T extends Drawable> implements p, s<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final T f904a;

    public a(T t) {
        this.f904a = (T) ((Drawable) h.a(t));
    }

    /* renamed from: f */
    public final T c() {
        return (T) this.f904a.getConstantState().newDrawable();
    }

    @Override // com.a.a.d.b.p
    public void a() {
        if (this.f904a instanceof BitmapDrawable) {
            ((BitmapDrawable) this.f904a).getBitmap().prepareToDraw();
        } else if (this.f904a instanceof c) {
            ((c) this.f904a).b().prepareToDraw();
        }
    }
}
