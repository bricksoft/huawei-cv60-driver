package com.a.a.d.d.e;

import android.util.Log;
import com.a.a.d.b.a.b;
import com.a.a.d.b.s;
import com.a.a.d.f;
import com.a.a.d.g;
import com.a.a.d.j;
import com.a.a.d.k;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

public class i implements k<InputStream, c> {

    /* renamed from: a  reason: collision with root package name */
    public static final com.a.a.d.i<Boolean> f914a = com.a.a.d.i.a("com.bumptech.glide.load.resource.gif.ByteBufferGifDecoder.DisableAnimation", (Object) false);
    private final List<f> b;
    private final k<ByteBuffer, c> c;
    private final b d;

    public i(List<f> list, k<ByteBuffer, c> kVar, b bVar) {
        this.b = list;
        this.c = kVar;
        this.d = bVar;
    }

    public boolean a(InputStream inputStream, j jVar) {
        return !((Boolean) jVar.a(f914a)).booleanValue() && g.a(this.b, inputStream, this.d) == f.a.GIF;
    }

    public s<c> a(InputStream inputStream, int i, int i2, j jVar) {
        byte[] a2 = a(inputStream);
        if (a2 == null) {
            return null;
        }
        return this.c.a(ByteBuffer.wrap(a2), i, i2, jVar);
    }

    private static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            byte[] bArr = new byte[16384];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            if (Log.isLoggable("StreamGifDecoder", 5)) {
                Log.w("StreamGifDecoder", "Error reading data from stream", e);
            }
            return null;
        }
    }
}
