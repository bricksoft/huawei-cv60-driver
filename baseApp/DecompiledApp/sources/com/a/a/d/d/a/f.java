package com.a.a.d.d.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.a.a.c;
import com.a.a.d.b.a.e;
import com.a.a.d.b.s;
import com.a.a.d.m;
import com.a.a.j.i;

public abstract class f implements m<Bitmap> {
    /* access modifiers changed from: protected */
    public abstract Bitmap a(@NonNull e eVar, @NonNull Bitmap bitmap, int i, int i2);

    @Override // com.a.a.d.m
    public final s<Bitmap> a(Context context, s<Bitmap> sVar, int i, int i2) {
        if (!i.a(i, i2)) {
            throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
        }
        e a2 = c.a(context).a();
        Bitmap c = sVar.c();
        if (i == Integer.MIN_VALUE) {
            i = c.getWidth();
        }
        if (i2 == Integer.MIN_VALUE) {
            i2 = c.getHeight();
        }
        Bitmap a3 = a(a2, c, i, i2);
        if (c.equals(a3)) {
            return sVar;
        }
        return e.a(a3, a2);
    }
}
