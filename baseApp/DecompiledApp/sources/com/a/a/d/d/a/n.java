package com.a.a.d.d.a;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import com.a.a.d.b;
import java.io.File;

/* access modifiers changed from: package-private */
public final class n {

    /* renamed from: a  reason: collision with root package name */
    private static final File f893a = new File("/proc/self/fd");
    private static volatile n d;
    private volatile int b;
    private volatile boolean c = true;

    static n a() {
        if (d == null) {
            synchronized (n.class) {
                if (d == null) {
                    d = new n();
                }
            }
        }
        return d;
    }

    private n() {
    }

    /* access modifiers changed from: package-private */
    @TargetApi(26)
    public boolean a(int i, int i2, BitmapFactory.Options options, b bVar, boolean z, boolean z2) {
        boolean z3;
        if (!z || Build.VERSION.SDK_INT < 26 || bVar == b.PREFER_ARGB_8888_DISALLOW_HARDWARE || z2) {
            return false;
        }
        if (i < 128 || i2 < 128 || !b()) {
            z3 = false;
        } else {
            z3 = true;
        }
        if (!z3) {
            return z3;
        }
        options.inPreferredConfig = Bitmap.Config.HARDWARE;
        options.inMutable = false;
        return z3;
    }

    private synchronized boolean b() {
        boolean z;
        boolean z2 = false;
        synchronized (this) {
            int i = this.b + 1;
            this.b = i;
            if (i >= 50) {
                this.b = 0;
                int length = f893a.list().length;
                if (length < 700) {
                    z2 = true;
                }
                this.c = z2;
                if (!this.c && Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors " + length + ", limit " + 700);
                }
            }
            z = this.c;
        }
        return z;
    }
}
