package com.a.a.d.d.a;

import android.graphics.Bitmap;
import com.a.a.d.c;
import com.a.a.d.i;
import com.a.a.d.j;
import com.a.a.d.l;

public class d implements l<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    public static final i<Integer> f883a = i.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", (Object) 90);
    public static final i<Bitmap.CompressFormat> b = i.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c9 A[Catch:{ all -> 0x00ec }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d2 A[SYNTHETIC, Splitter:B:25:0x00d2] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00de A[SYNTHETIC, Splitter:B:33:0x00de] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.a.a.d.b.s<android.graphics.Bitmap> r10, java.io.File r11, com.a.a.d.j r12) {
        /*
        // Method dump skipped, instructions count: 243
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.d.d.a.d.a(com.a.a.d.b.s, java.io.File, com.a.a.d.j):boolean");
    }

    private Bitmap.CompressFormat a(Bitmap bitmap, j jVar) {
        Bitmap.CompressFormat compressFormat = (Bitmap.CompressFormat) jVar.a(b);
        if (compressFormat != null) {
            return compressFormat;
        }
        if (bitmap.hasAlpha()) {
            return Bitmap.CompressFormat.PNG;
        }
        return Bitmap.CompressFormat.JPEG;
    }

    @Override // com.a.a.d.l
    public c a(j jVar) {
        return c.TRANSFORMED;
    }
}
