package com.a.a.d.d.f;

import java.util.ArrayList;
import java.util.List;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final List<a<?, ?>> f917a = new ArrayList();

    public synchronized <Z, R> void a(Class<Z> cls, Class<R> cls2, d<Z, R> dVar) {
        this.f917a.add(new a<>(cls, cls2, dVar));
    }

    public synchronized <Z, R> d<Z, R> a(Class<Z> cls, Class<R> cls2) {
        d<Z, R> dVar;
        if (cls2.isAssignableFrom(cls)) {
            dVar = f.a();
        } else {
            for (a<?, ?> aVar : this.f917a) {
                if (aVar.a(cls, cls2)) {
                    dVar = aVar.f918a;
                }
            }
            throw new IllegalArgumentException("No transcoder registered to transcode from " + cls + " to " + cls2);
        }
        return dVar;
    }

    public synchronized <Z, R> List<Class<R>> b(Class<Z> cls, Class<R> cls2) {
        ArrayList arrayList;
        ArrayList arrayList2 = new ArrayList();
        if (cls2.isAssignableFrom(cls)) {
            arrayList2.add(cls2);
            arrayList = arrayList2;
        } else {
            for (a<?, ?> aVar : this.f917a) {
                if (aVar.a(cls, cls2)) {
                    arrayList2.add(cls2);
                }
            }
            arrayList = arrayList2;
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public static final class a<Z, R> {

        /* renamed from: a  reason: collision with root package name */
        final d<Z, R> f918a;
        private final Class<Z> b;
        private final Class<R> c;

        a(Class<Z> cls, Class<R> cls2, d<Z, R> dVar) {
            this.b = cls;
            this.c = cls2;
            this.f918a = dVar;
        }

        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.b.isAssignableFrom(cls) && cls2.isAssignableFrom(this.c);
        }
    }
}
