package com.a.a.d.d.a;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.a.a.d.b.a.e;
import com.a.a.d.b.s;
import com.a.a.d.c;
import com.a.a.d.j;
import com.a.a.d.l;
import java.io.File;

public class b implements l<BitmapDrawable> {

    /* renamed from: a  reason: collision with root package name */
    private final e f882a;
    private final l<Bitmap> b;

    public b(e eVar, l<Bitmap> lVar) {
        this.f882a = eVar;
        this.b = lVar;
    }

    public boolean a(s<BitmapDrawable> sVar, File file, j jVar) {
        return this.b.a(new e(sVar.c().getBitmap(), this.f882a), file, jVar);
    }

    @Override // com.a.a.d.l
    public c a(j jVar) {
        return this.b.a(jVar);
    }
}
