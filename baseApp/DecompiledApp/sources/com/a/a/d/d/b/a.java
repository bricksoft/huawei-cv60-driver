package com.a.a.d.d.b;

import com.a.a.d.a.c;
import java.nio.ByteBuffer;

public class a implements c<ByteBuffer> {

    /* renamed from: a  reason: collision with root package name */
    private final ByteBuffer f902a;

    public a(ByteBuffer byteBuffer) {
        this.f902a = byteBuffer;
    }

    /* renamed from: c */
    public ByteBuffer a() {
        this.f902a.position(0);
        return this.f902a;
    }

    @Override // com.a.a.d.a.c
    public void b() {
    }

    /* renamed from: com.a.a.d.d.b.a$a  reason: collision with other inner class name */
    public static class C0035a implements c.a<ByteBuffer> {
        public c<ByteBuffer> a(ByteBuffer byteBuffer) {
            return new a(byteBuffer);
        }

        @Override // com.a.a.d.a.c.a
        public Class<ByteBuffer> a() {
            return ByteBuffer.class;
        }
    }
}
