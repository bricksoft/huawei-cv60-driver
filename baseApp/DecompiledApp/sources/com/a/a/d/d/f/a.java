package com.a.a.d.d.f;

import android.graphics.Bitmap;
import com.a.a.d.b.s;
import com.a.a.d.d.b.b;
import com.a.a.d.j;
import java.io.ByteArrayOutputStream;

public class a implements d<Bitmap, byte[]> {

    /* renamed from: a  reason: collision with root package name */
    private final Bitmap.CompressFormat f915a;
    private final int b;

    public a() {
        this(Bitmap.CompressFormat.JPEG, 100);
    }

    public a(Bitmap.CompressFormat compressFormat, int i) {
        this.f915a = compressFormat;
        this.b = i;
    }

    @Override // com.a.a.d.d.f.d
    public s<byte[]> a(s<Bitmap> sVar, j jVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        sVar.c().compress(this.f915a, this.b, byteArrayOutputStream);
        sVar.e();
        return new b(byteArrayOutputStream.toByteArray());
    }
}
