package com.a.a.d.d.e;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import com.a.a.d.b.a.e;
import com.a.a.d.d.e.g;
import com.a.a.d.m;
import com.a.a.j.h;
import java.nio.ByteBuffer;

public class c extends Drawable implements Animatable, g.b {

    /* renamed from: a  reason: collision with root package name */
    private final a f908a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    private int f;
    private int g;
    private boolean h;
    private Paint i;
    private Rect j;

    public c(Context context, com.a.a.b.a aVar, e eVar, m<Bitmap> mVar, int i2, int i3, Bitmap bitmap) {
        this(new a(eVar, new g(com.a.a.c.a(context), aVar, i2, i3, mVar, bitmap)));
    }

    c(a aVar) {
        this.e = true;
        this.g = -1;
        this.f908a = (a) h.a(aVar);
    }

    public int a() {
        return this.f908a.b.d();
    }

    public Bitmap b() {
        return this.f908a.b.a();
    }

    public void a(m<Bitmap> mVar, Bitmap bitmap) {
        this.f908a.b.a(mVar, bitmap);
    }

    public ByteBuffer c() {
        return this.f908a.b.f();
    }

    public int d() {
        return this.f908a.b.g();
    }

    public int e() {
        return this.f908a.b.e();
    }

    private void h() {
        this.f = 0;
    }

    public void start() {
        this.c = true;
        h();
        if (this.e) {
            i();
        }
    }

    public void stop() {
        this.c = false;
        j();
    }

    private void i() {
        h.a(!this.d, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.f908a.b.g() == 1) {
            invalidateSelf();
        } else if (!this.b) {
            this.b = true;
            this.f908a.b.a(this);
            invalidateSelf();
        }
    }

    private void j() {
        this.b = false;
        this.f908a.b.b(this);
    }

    public boolean setVisible(boolean z, boolean z2) {
        h.a(!this.d, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.e = z;
        if (!z) {
            j();
        } else if (this.c) {
            i();
        }
        return super.setVisible(z, z2);
    }

    public int getIntrinsicWidth() {
        return this.f908a.b.b();
    }

    public int getIntrinsicHeight() {
        return this.f908a.b.c();
    }

    public boolean isRunning() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.h = true;
    }

    public void draw(Canvas canvas) {
        if (!this.d) {
            if (this.h) {
                Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), k());
                this.h = false;
            }
            canvas.drawBitmap(this.f908a.b.i(), (Rect) null, k(), l());
        }
    }

    public void setAlpha(int i2) {
        l().setAlpha(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        l().setColorFilter(colorFilter);
    }

    private Rect k() {
        if (this.j == null) {
            this.j = new Rect();
        }
        return this.j;
    }

    private Paint l() {
        if (this.i == null) {
            this.i = new Paint(2);
        }
        return this.i;
    }

    public int getOpacity() {
        return -2;
    }

    @Override // com.a.a.d.d.e.g.b
    public void f() {
        if (getCallback() == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        if (e() == d() - 1) {
            this.f++;
        }
        if (this.g != -1 && this.f >= this.g) {
            stop();
        }
    }

    public Drawable.ConstantState getConstantState() {
        return this.f908a;
    }

    public void g() {
        this.d = true;
        this.f908a.b.h();
    }

    /* access modifiers changed from: package-private */
    public static class a extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        final e f909a;
        final g b;

        public a(e eVar, g gVar) {
            this.f909a = eVar;
            this.b = gVar;
        }

        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }

        public Drawable newDrawable() {
            return new c(this);
        }

        public int getChangingConfigurations() {
            return 0;
        }
    }
}
