package com.a.a.d.d.a;

public abstract class k {

    /* renamed from: a  reason: collision with root package name */
    public static final k f890a = new e();
    public static final k b = new d();
    public static final k c = new a();
    public static final k d = new b();
    public static final k e = new c();
    public static final k f = new f();
    public static final k g = b;

    public enum g {
        MEMORY,
        QUALITY
    }

    public abstract float a(int i, int i2, int i3, int i4);

    public abstract g b(int i, int i2, int i3, int i4);

    private static class e extends k {
        e() {
        }

        @Override // com.a.a.d.d.a.k
        public float a(int i, int i2, int i3, int i4) {
            return Math.min(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
        }

        @Override // com.a.a.d.d.a.k
        public g b(int i, int i2, int i3, int i4) {
            return g.QUALITY;
        }
    }

    private static class d extends k {
        d() {
        }

        @Override // com.a.a.d.d.a.k
        public float a(int i, int i2, int i3, int i4) {
            return Math.max(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
        }

        @Override // com.a.a.d.d.a.k
        public g b(int i, int i2, int i3, int i4) {
            return g.QUALITY;
        }
    }

    private static class a extends k {
        a() {
        }

        @Override // com.a.a.d.d.a.k
        public float a(int i, int i2, int i3, int i4) {
            int min = Math.min(i2 / i4, i / i3);
            if (min == 0) {
                return 1.0f;
            }
            return 1.0f / ((float) Integer.highestOneBit(min));
        }

        @Override // com.a.a.d.d.a.k
        public g b(int i, int i2, int i3, int i4) {
            return g.QUALITY;
        }
    }

    private static class b extends k {
        b() {
        }

        @Override // com.a.a.d.d.a.k
        public float a(int i, int i2, int i3, int i4) {
            int i5 = 1;
            int ceil = (int) Math.ceil((double) Math.max(((float) i2) / ((float) i4), ((float) i) / ((float) i3)));
            int max = Math.max(1, Integer.highestOneBit(ceil));
            if (max >= ceil) {
                i5 = 0;
            }
            return 1.0f / ((float) (max << i5));
        }

        @Override // com.a.a.d.d.a.k
        public g b(int i, int i2, int i3, int i4) {
            return g.MEMORY;
        }
    }

    private static class f extends k {
        f() {
        }

        @Override // com.a.a.d.d.a.k
        public float a(int i, int i2, int i3, int i4) {
            return 1.0f;
        }

        @Override // com.a.a.d.d.a.k
        public g b(int i, int i2, int i3, int i4) {
            return g.QUALITY;
        }
    }

    private static class c extends k {
        c() {
        }

        @Override // com.a.a.d.d.a.k
        public float a(int i, int i2, int i3, int i4) {
            return Math.min(1.0f, f890a.a(i, i2, i3, i4));
        }

        @Override // com.a.a.d.d.a.k
        public g b(int i, int i2, int i3, int i4) {
            return g.QUALITY;
        }
    }
}
