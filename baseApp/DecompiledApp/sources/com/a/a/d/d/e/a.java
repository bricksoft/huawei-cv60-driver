package com.a.a.d.d.e;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.a.a.b.a;
import com.a.a.b.c;
import com.a.a.b.d;
import com.a.a.d.b.a.e;
import com.a.a.d.f;
import com.a.a.d.g;
import com.a.a.d.i;
import com.a.a.d.j;
import com.a.a.d.k;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;

public class a implements k<ByteBuffer, c> {

    /* renamed from: a  reason: collision with root package name */
    public static final i<Boolean> f905a = i.a("com.bumptech.glide.load.resource.gif.ByteBufferGifDecoder.DisableAnimation", (Object) false);
    private static final C0036a b = new C0036a();
    private static final b c = new b();
    private final Context d;
    private final List<f> e;
    private final b f;
    private final e g;
    private final C0036a h;
    private final b i;

    public a(Context context, List<f> list, e eVar, com.a.a.d.b.a.b bVar) {
        this(context, list, eVar, bVar, c, b);
    }

    a(Context context, List<f> list, e eVar, com.a.a.d.b.a.b bVar, b bVar2, C0036a aVar) {
        this.d = context.getApplicationContext();
        this.e = list;
        this.g = eVar;
        this.h = aVar;
        this.i = new b(eVar, bVar);
        this.f = bVar2;
    }

    public boolean a(ByteBuffer byteBuffer, j jVar) {
        return !((Boolean) jVar.a(f905a)).booleanValue() && g.a(this.e, byteBuffer) == f.a.GIF;
    }

    public e a(ByteBuffer byteBuffer, int i2, int i3, j jVar) {
        d a2 = this.f.a(byteBuffer);
        try {
            return a(byteBuffer, i2, i3, a2);
        } finally {
            this.f.a(a2);
        }
    }

    private e a(ByteBuffer byteBuffer, int i2, int i3, d dVar) {
        long a2 = com.a.a.j.d.a();
        c b2 = dVar.b();
        if (b2.c() <= 0 || b2.d() != 0) {
            return null;
        }
        com.a.a.b.a a3 = this.h.a(this.i, b2, byteBuffer, a(b2, i2, i3));
        a3.b();
        Bitmap h2 = a3.h();
        if (h2 == null) {
            return null;
        }
        c cVar = new c(this.d, a3, this.g, com.a.a.d.d.b.a(), i2, i3, h2);
        if (Log.isLoggable("BufferGifDecoder", 2)) {
            Log.v("BufferGifDecoder", "Decoded GIF from stream in " + com.a.a.j.d.a(a2));
        }
        return new e(cVar);
    }

    private static int a(c cVar, int i2, int i3) {
        int min = Math.min(cVar.a() / i3, cVar.b() / i2);
        int max = Math.max(1, min == 0 ? 0 : Integer.highestOneBit(min));
        if (Log.isLoggable("BufferGifDecoder", 2)) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i2 + "x" + i3 + "], actual dimens: [" + cVar.b() + "x" + cVar.a() + "]");
        }
        return max;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.a.a.d.d.e.a$a  reason: collision with other inner class name */
    public static class C0036a {
        C0036a() {
        }

        public com.a.a.b.a a(a.AbstractC0027a aVar, c cVar, ByteBuffer byteBuffer, int i) {
            return new com.a.a.b.e(aVar, cVar, byteBuffer, i);
        }
    }

    /* access modifiers changed from: package-private */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private final Queue<d> f906a = com.a.a.j.i.a(0);

        b() {
        }

        public synchronized d a(ByteBuffer byteBuffer) {
            d poll;
            poll = this.f906a.poll();
            if (poll == null) {
                poll = new d();
            }
            return poll.a(byteBuffer);
        }

        public synchronized void a(d dVar) {
            dVar.a();
            this.f906a.offer(dVar);
        }
    }
}
