package com.a.a.d.d.a;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.a.a.d.b.a.e;
import com.a.a.d.b.s;
import com.a.a.d.j;
import com.a.a.d.k;
import com.a.a.j.h;

public class a<DataType> implements k<DataType, BitmapDrawable> {

    /* renamed from: a  reason: collision with root package name */
    private final k<DataType, Bitmap> f881a;
    private final Resources b;
    private final e c;

    public a(Resources resources, e eVar, k<DataType, Bitmap> kVar) {
        this.b = (Resources) h.a(resources);
        this.c = (e) h.a(eVar);
        this.f881a = (k) h.a(kVar);
    }

    @Override // com.a.a.d.k
    public boolean a(DataType datatype, j jVar) {
        return this.f881a.a(datatype, jVar);
    }

    @Override // com.a.a.d.k
    public s<BitmapDrawable> a(DataType datatype, int i, int i2, j jVar) {
        s<Bitmap> a2 = this.f881a.a(datatype, i, i2, jVar);
        if (a2 == null) {
            return null;
        }
        return o.a(this.b, this.c, a2.c());
    }
}
