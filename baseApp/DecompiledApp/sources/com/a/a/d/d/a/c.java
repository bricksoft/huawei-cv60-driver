package com.a.a.d.d.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.a.a.d.b.s;
import com.a.a.d.m;
import com.a.a.j.h;
import java.security.MessageDigest;

public class c implements m<BitmapDrawable> {
    private final m<Bitmap> b;

    public c(m<Bitmap> mVar) {
        this.b = (m) h.a(mVar);
    }

    @Override // com.a.a.d.m
    public s<BitmapDrawable> a(Context context, s<BitmapDrawable> sVar, int i, int i2) {
        e a2 = e.a(sVar.c().getBitmap(), com.a.a.c.a(context).a());
        s<Bitmap> a3 = this.b.a(context, a2, i, i2);
        return a3.equals(a2) ? sVar : o.a(context, a3.c());
    }

    @Override // com.a.a.d.m, com.a.a.d.h
    public boolean equals(Object obj) {
        if (obj instanceof c) {
            return this.b.equals(((c) obj).b);
        }
        return false;
    }

    @Override // com.a.a.d.m, com.a.a.d.h
    public int hashCode() {
        return this.b.hashCode();
    }

    @Override // com.a.a.d.h
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
