package com.a.a.d.d.b;

import com.a.a.d.b.s;
import com.a.a.j.h;

public class b implements s<byte[]> {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f903a;

    public b(byte[] bArr) {
        this.f903a = (byte[]) h.a(bArr);
    }

    @Override // com.a.a.d.b.s
    public Class<byte[]> b() {
        return byte[].class;
    }

    /* renamed from: a */
    public byte[] c() {
        return this.f903a;
    }

    @Override // com.a.a.d.b.s
    public int d() {
        return this.f903a.length;
    }

    @Override // com.a.a.d.b.s
    public void e() {
    }
}
