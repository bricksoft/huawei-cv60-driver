package com.a.a.d.d.a;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.a.a.d.b.a.e;
import java.security.MessageDigest;

public class i extends f {
    private static final byte[] b = "com.bumptech.glide.load.resource.bitmap.CenterInside".getBytes(f922a);

    /* access modifiers changed from: protected */
    @Override // com.a.a.d.d.a.f
    public Bitmap a(@NonNull e eVar, @NonNull Bitmap bitmap, int i, int i2) {
        return r.c(eVar, bitmap, i, i2);
    }

    @Override // com.a.a.d.m, com.a.a.d.h
    public boolean equals(Object obj) {
        return obj instanceof i;
    }

    @Override // com.a.a.d.m, com.a.a.d.h
    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.CenterInside".hashCode();
    }

    @Override // com.a.a.d.h
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }
}
