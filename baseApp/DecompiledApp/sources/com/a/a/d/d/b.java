package com.a.a.d.d;

import android.content.Context;
import com.a.a.d.b.s;
import com.a.a.d.m;
import java.security.MessageDigest;

public final class b<T> implements m<T> {
    private static final m<?> b = new b();

    public static <T> b<T> a() {
        return (b) b;
    }

    private b() {
    }

    @Override // com.a.a.d.m
    public s<T> a(Context context, s<T> sVar, int i, int i2) {
        return sVar;
    }

    @Override // com.a.a.d.h
    public void a(MessageDigest messageDigest) {
    }
}
