package com.a.a.d.d.a;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.a.a.c;
import com.a.a.d.b.a.e;
import com.a.a.d.b.p;
import com.a.a.d.b.s;
import com.a.a.j.h;
import com.a.a.j.i;

public class o implements p, s<BitmapDrawable> {

    /* renamed from: a  reason: collision with root package name */
    private final Bitmap f894a;
    private final Resources b;
    private final e c;

    public static o a(Context context, Bitmap bitmap) {
        return a(context.getResources(), c.a(context).a(), bitmap);
    }

    public static o a(Resources resources, e eVar, Bitmap bitmap) {
        return new o(resources, eVar, bitmap);
    }

    o(Resources resources, e eVar, Bitmap bitmap) {
        this.b = (Resources) h.a(resources);
        this.c = (e) h.a(eVar);
        this.f894a = (Bitmap) h.a(bitmap);
    }

    @Override // com.a.a.d.b.s
    public Class<BitmapDrawable> b() {
        return BitmapDrawable.class;
    }

    /* renamed from: f */
    public BitmapDrawable c() {
        return new BitmapDrawable(this.b, this.f894a);
    }

    @Override // com.a.a.d.b.s
    public int d() {
        return i.a(this.f894a);
    }

    @Override // com.a.a.d.b.s
    public void e() {
        this.c.a(this.f894a);
    }

    @Override // com.a.a.d.b.p
    public void a() {
        this.f894a.prepareToDraw();
    }
}
