package com.a.a.d.d.a;

import android.graphics.Bitmap;
import com.a.a.d.b.a.b;
import com.a.a.d.b.a.e;
import com.a.a.d.b.s;
import com.a.a.d.d.a.l;
import com.a.a.d.j;
import com.a.a.d.k;
import com.a.a.j.c;
import com.a.a.j.f;
import java.io.IOException;
import java.io.InputStream;

public class q implements k<InputStream, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final l f896a;
    private final b b;

    public q(l lVar, b bVar) {
        this.f896a = lVar;
        this.b = bVar;
    }

    public boolean a(InputStream inputStream, j jVar) {
        return this.f896a.a(inputStream);
    }

    public s<Bitmap> a(InputStream inputStream, int i, int i2, j jVar) {
        boolean z;
        p pVar;
        if (inputStream instanceof p) {
            pVar = (p) inputStream;
            z = false;
        } else {
            z = true;
            pVar = new p(inputStream, this.b);
        }
        c a2 = c.a(pVar);
        try {
            return this.f896a.a(new f(a2), i, i2, jVar, new a(pVar, a2));
        } finally {
            a2.b();
            if (z) {
                pVar.b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public static class a implements l.a {

        /* renamed from: a  reason: collision with root package name */
        private final p f897a;
        private final c b;

        public a(p pVar, c cVar) {
            this.f897a = pVar;
            this.b = cVar;
        }

        @Override // com.a.a.d.d.a.l.a
        public void a() {
            this.f897a.a();
        }

        @Override // com.a.a.d.d.a.l.a
        public void a(e eVar, Bitmap bitmap) {
            IOException a2 = this.b.a();
            if (a2 != null) {
                if (bitmap != null) {
                    eVar.a(bitmap);
                }
                throw a2;
            }
        }
    }
}
