package com.a.a.d.d.f;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.a.a.d.b.a.e;
import com.a.a.d.b.s;
import com.a.a.d.d.a.o;
import com.a.a.d.j;
import com.a.a.j.h;

public class b implements d<Bitmap, BitmapDrawable> {

    /* renamed from: a  reason: collision with root package name */
    private final Resources f916a;
    private final e b;

    public b(Resources resources, e eVar) {
        this.f916a = (Resources) h.a(resources);
        this.b = (e) h.a(eVar);
    }

    @Override // com.a.a.d.d.f.d
    public s<BitmapDrawable> a(s<Bitmap> sVar, j jVar) {
        return o.a(this.f916a, this.b, sVar.c());
    }
}
