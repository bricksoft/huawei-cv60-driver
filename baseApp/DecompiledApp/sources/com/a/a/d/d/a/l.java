package com.a.a.d.d.a;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import com.a.a.d.b;
import com.a.a.d.b.a.e;
import com.a.a.d.b.s;
import com.a.a.d.d.a.k;
import com.a.a.d.f;
import com.a.a.d.g;
import com.a.a.d.i;
import com.a.a.d.j;
import com.a.a.j.d;
import com.a.a.j.h;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    public static final i<b> f892a = i.a("com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", b.d);
    public static final i<k> b = i.a("com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy", k.g);
    public static final i<Boolean> c = i.a("com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", (Object) false);
    public static final i<Boolean> d = i.a("com.bumtpech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", (Object) null);
    private static final Set<String> e = Collections.unmodifiableSet(new HashSet(Arrays.asList("image/vnd.wap.wbmp", "image/x-ico")));
    private static final a f = new a() {
        /* class com.a.a.d.d.a.l.AnonymousClass1 */

        @Override // com.a.a.d.d.a.l.a
        public void a() {
        }

        @Override // com.a.a.d.d.a.l.a
        public void a(e eVar, Bitmap bitmap) {
        }
    };
    private static final Set<f.a> g = Collections.unmodifiableSet(EnumSet.of(f.a.JPEG, f.a.PNG_A, f.a.PNG));
    private static final Queue<BitmapFactory.Options> h = com.a.a.j.i.a(0);
    private final e i;
    private final DisplayMetrics j;
    private final com.a.a.d.b.a.b k;
    private final List<f> l;
    private final n m = n.a();

    public interface a {
        void a();

        void a(e eVar, Bitmap bitmap);
    }

    public l(List<f> list, DisplayMetrics displayMetrics, e eVar, com.a.a.d.b.a.b bVar) {
        this.l = list;
        this.j = (DisplayMetrics) h.a(displayMetrics);
        this.i = (e) h.a(eVar);
        this.k = (com.a.a.d.b.a.b) h.a(bVar);
    }

    public boolean a(InputStream inputStream) {
        return true;
    }

    public boolean a(ByteBuffer byteBuffer) {
        return true;
    }

    public s<Bitmap> a(InputStream inputStream, int i2, int i3, j jVar) {
        return a(inputStream, i2, i3, jVar, f);
    }

    public s<Bitmap> a(InputStream inputStream, int i2, int i3, j jVar, a aVar) {
        boolean z;
        h.a(inputStream.markSupported(), "You must provide an InputStream that supports mark()");
        byte[] bArr = (byte[]) this.k.a(65536, byte[].class);
        BitmapFactory.Options a2 = a();
        a2.inTempStorage = bArr;
        b bVar = (b) jVar.a(f892a);
        k kVar = (k) jVar.a(b);
        boolean booleanValue = ((Boolean) jVar.a(c)).booleanValue();
        boolean z2 = jVar.a(d) != null && ((Boolean) jVar.a(d)).booleanValue();
        if (bVar == b.PREFER_ARGB_8888_DISALLOW_HARDWARE) {
            z = false;
        } else {
            z = z2;
        }
        try {
            return e.a(a(inputStream, a2, kVar, bVar, z, i2, i3, booleanValue, aVar), this.i);
        } finally {
            c(a2);
            this.k.a(bArr, byte[].class);
        }
    }

    private Bitmap a(InputStream inputStream, BitmapFactory.Options options, k kVar, b bVar, boolean z, int i2, int i3, boolean z2, a aVar) {
        boolean z3;
        int i4;
        long a2 = d.a();
        int[] a3 = a(inputStream, options, aVar, this.i);
        int i5 = a3[0];
        int i6 = a3[1];
        String str = options.outMimeType;
        if (i5 == -1 || i6 == -1) {
            z3 = false;
        } else {
            z3 = z;
        }
        int b2 = g.b(this.l, inputStream, this.k);
        int a4 = r.a(b2);
        boolean b3 = r.b(b2);
        int i7 = i2 == Integer.MIN_VALUE ? i5 : i2;
        if (i3 == Integer.MIN_VALUE) {
            i4 = i6;
        } else {
            i4 = i3;
        }
        f.a a5 = g.a(this.l, inputStream, this.k);
        a(a5, inputStream, aVar, this.i, kVar, a4, i5, i6, i7, i4, options);
        a(inputStream, bVar, z3, b3, options, i7, i4);
        boolean z4 = Build.VERSION.SDK_INT >= 19;
        if ((options.inSampleSize == 1 || z4) && a(a5)) {
            if (!z2 || !z4) {
                float f2 = a(options) ? ((float) options.inTargetDensity) / ((float) options.inDensity) : 1.0f;
                int i8 = options.inSampleSize;
                i7 = Math.round(((float) ((int) Math.ceil((double) (((float) i5) / ((float) i8))))) * f2);
                i4 = Math.round(((float) ((int) Math.ceil((double) (((float) i6) / ((float) i8))))) * f2);
                if (Log.isLoggable("Downsampler", 2)) {
                    Log.v("Downsampler", "Calculated target [" + i7 + "x" + i4 + "] for source [" + i5 + "x" + i6 + "], sampleSize: " + i8 + ", targetDensity: " + options.inTargetDensity + ", density: " + options.inDensity + ", density multiplier: " + f2);
                }
            }
            if (i7 > 0 && i4 > 0) {
                a(options, this.i, i7, i4);
            }
        }
        Bitmap b4 = b(inputStream, options, aVar, this.i);
        aVar.a(this.i, b4);
        if (Log.isLoggable("Downsampler", 2)) {
            a(i5, i6, str, options, b4, i2, i3, a2);
        }
        Bitmap bitmap = null;
        if (b4 != null) {
            b4.setDensity(this.j.densityDpi);
            bitmap = r.a(this.i, b4, b2);
            if (!b4.equals(bitmap)) {
                this.i.a(b4);
            }
        }
        return bitmap;
    }

    static void a(f.a aVar, InputStream inputStream, a aVar2, e eVar, k kVar, int i2, int i3, int i4, int i5, int i6, BitmapFactory.Options options) {
        float f2;
        int min;
        int max;
        int floor;
        int floor2;
        if (i3 > 0 && i4 > 0) {
            if (i2 == 90 || i2 == 270) {
                f2 = kVar.a(i4, i3, i5, i6);
            } else {
                f2 = kVar.a(i3, i4, i5, i6);
            }
            if (f2 <= 0.0f) {
                throw new IllegalArgumentException("Cannot scale with factor: " + f2 + " from: " + kVar + ", source: [" + i3 + "x" + i4 + "], target: [" + i5 + "x" + i6 + "]");
            }
            k.g b2 = kVar.b(i3, i4, i5, i6);
            if (b2 == null) {
                throw new IllegalArgumentException("Cannot round with null rounding");
            }
            int b3 = i3 / b((double) (((float) i3) * f2));
            int b4 = i4 / b((double) (((float) i4) * f2));
            if (b2 == k.g.MEMORY) {
                min = Math.max(b3, b4);
            } else {
                min = Math.min(b3, b4);
            }
            if (Build.VERSION.SDK_INT > 23 || !e.contains(options.outMimeType)) {
                max = Math.max(1, Integer.highestOneBit(min));
                if (b2 == k.g.MEMORY && ((float) max) < 1.0f / f2) {
                    max <<= 1;
                }
            } else {
                max = 1;
            }
            options.inSampleSize = max;
            if (aVar == f.a.JPEG) {
                int min2 = Math.min(max, 8);
                floor = (int) Math.ceil((double) (((float) i3) / ((float) min2)));
                floor2 = (int) Math.ceil((double) (((float) i4) / ((float) min2)));
                int i7 = max / 8;
                if (i7 > 0) {
                    floor /= i7;
                    floor2 /= i7;
                }
            } else if (aVar == f.a.PNG || aVar == f.a.PNG_A) {
                floor = (int) Math.floor((double) (((float) i3) / ((float) max)));
                floor2 = (int) Math.floor((double) (((float) i4) / ((float) max)));
            } else if (aVar == f.a.WEBP || aVar == f.a.WEBP_A) {
                if (Build.VERSION.SDK_INT >= 24) {
                    floor = Math.round(((float) i3) / ((float) max));
                    floor2 = Math.round(((float) i4) / ((float) max));
                } else {
                    floor = (int) Math.floor((double) (((float) i3) / ((float) max)));
                    floor2 = (int) Math.floor((double) (((float) i4) / ((float) max)));
                }
            } else if (i3 % max == 0 && i4 % max == 0) {
                floor = i3 / max;
                floor2 = i4 / max;
            } else {
                int[] a2 = a(inputStream, options, aVar2, eVar);
                floor = a2[0];
                floor2 = a2[1];
            }
            double a3 = (double) kVar.a(floor, floor2, i5, i6);
            if (Build.VERSION.SDK_INT >= 19) {
                options.inTargetDensity = a(a3);
                options.inDensity = 1000000000;
            }
            if (a(options)) {
                options.inScaled = true;
            } else {
                options.inTargetDensity = 0;
                options.inDensity = 0;
            }
            if (Log.isLoggable("Downsampler", 2)) {
                Log.v("Downsampler", "Calculate scaling, source: [" + i3 + "x" + i4 + "], target: [" + i5 + "x" + i6 + "], power of two scaled: [" + floor + "x" + floor2 + "], exact scale factor: " + f2 + ", power of 2 sample size: " + max + ", adjusted scale factor: " + a3 + ", target density: " + options.inTargetDensity + ", density: " + options.inDensity);
            }
        }
    }

    private static int a(double d2) {
        int b2 = b(1.0E9d * d2);
        return b(((double) b2) * (d2 / ((double) (((float) b2) / 1.0E9f))));
    }

    private static int b(double d2) {
        return (int) (0.5d + d2);
    }

    private boolean a(f.a aVar) {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return g.contains(aVar);
    }

    private void a(InputStream inputStream, b bVar, boolean z, boolean z2, BitmapFactory.Options options, int i2, int i3) {
        boolean z3;
        if (!this.m.a(i2, i3, options, bVar, z, z2)) {
            if (bVar == b.PREFER_ARGB_8888 || bVar == b.PREFER_ARGB_8888_DISALLOW_HARDWARE || Build.VERSION.SDK_INT == 16) {
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                return;
            }
            try {
                z3 = g.a(this.l, inputStream, this.k).a();
            } catch (IOException e2) {
                if (Log.isLoggable("Downsampler", 3)) {
                    Log.d("Downsampler", "Cannot determine whether the image has alpha or not from header, format " + bVar, e2);
                }
                z3 = false;
            }
            options.inPreferredConfig = z3 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
            if (options.inPreferredConfig == Bitmap.Config.RGB_565 || options.inPreferredConfig == Bitmap.Config.ARGB_4444 || options.inPreferredConfig == Bitmap.Config.ALPHA_8) {
                options.inDither = true;
            }
        }
    }

    private static int[] a(InputStream inputStream, BitmapFactory.Options options, a aVar, e eVar) {
        options.inJustDecodeBounds = true;
        b(inputStream, options, aVar, eVar);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }

    private static Bitmap b(InputStream inputStream, BitmapFactory.Options options, a aVar, e eVar) {
        Bitmap b2;
        if (options.inJustDecodeBounds) {
            inputStream.mark(MediaHttpUploader.DEFAULT_CHUNK_SIZE);
        } else {
            aVar.a();
        }
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        String str = options.outMimeType;
        r.a().lock();
        try {
            b2 = BitmapFactory.decodeStream(inputStream, null, options);
            r.a().unlock();
            if (options.inJustDecodeBounds) {
                inputStream.reset();
            }
        } catch (IllegalArgumentException e2) {
            IOException a2 = a(e2, i2, i3, str, options);
            if (Log.isLoggable("Downsampler", 3)) {
                Log.d("Downsampler", "Failed to decode with inBitmap, trying again without Bitmap re-use", a2);
            }
            if (options.inBitmap != null) {
                try {
                    inputStream.reset();
                    eVar.a(options.inBitmap);
                    options.inBitmap = null;
                    b2 = b(inputStream, options, aVar, eVar);
                    r.a().unlock();
                } catch (IOException e3) {
                    throw a2;
                }
            } else {
                throw a2;
            }
        } catch (Throwable th) {
            r.a().unlock();
            throw th;
        }
        return b2;
    }

    private static boolean a(BitmapFactory.Options options) {
        return options.inTargetDensity > 0 && options.inDensity > 0 && options.inTargetDensity != options.inDensity;
    }

    private static void a(int i2, int i3, String str, BitmapFactory.Options options, Bitmap bitmap, int i4, int i5, long j2) {
        Log.v("Downsampler", "Decoded " + a(bitmap) + " from [" + i2 + "x" + i3 + "] " + str + " with inBitmap " + b(options) + " for [" + i4 + "x" + i5 + "], sample size: " + options.inSampleSize + ", density: " + options.inDensity + ", target density: " + options.inTargetDensity + ", thread: " + Thread.currentThread().getName() + ", duration: " + d.a(j2));
    }

    private static String b(BitmapFactory.Options options) {
        return a(options.inBitmap);
    }

    @TargetApi(19)
    @Nullable
    private static String a(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        return "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig() + (Build.VERSION.SDK_INT >= 19 ? " (" + bitmap.getAllocationByteCount() + ")" : "");
    }

    private static IOException a(IllegalArgumentException illegalArgumentException, int i2, int i3, String str, BitmapFactory.Options options) {
        return new IOException("Exception decoding bitmap, outWidth: " + i2 + ", outHeight: " + i3 + ", outMimeType: " + str + ", inBitmap: " + b(options), illegalArgumentException);
    }

    @TargetApi(26)
    private static void a(BitmapFactory.Options options, e eVar, int i2, int i3) {
        if (Build.VERSION.SDK_INT < 26 || options.inPreferredConfig != Bitmap.Config.HARDWARE) {
            options.inBitmap = eVar.b(i2, i3, options.inPreferredConfig);
        }
    }

    private static synchronized BitmapFactory.Options a() {
        BitmapFactory.Options poll;
        synchronized (l.class) {
            synchronized (h) {
                poll = h.poll();
            }
            if (poll == null) {
                poll = new BitmapFactory.Options();
                d(poll);
            }
        }
        return poll;
    }

    private static void c(BitmapFactory.Options options) {
        d(options);
        synchronized (h) {
            h.offer(options);
        }
    }

    private static void d(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.inDensity = 0;
        options.inTargetDensity = 0;
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        options.inBitmap = null;
        options.inMutable = true;
    }
}
