package com.a.a.d.d.e;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.a.a.d.b.a.e;
import com.a.a.d.m;
import com.a.a.h.a.f;
import com.a.a.h.d;
import com.a.a.i;
import com.a.a.j;
import com.a.a.j.h;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/* access modifiers changed from: package-private */
public class g {

    /* renamed from: a  reason: collision with root package name */
    final j f910a;
    private final com.a.a.b.a b;
    private final Handler c;
    private final List<b> d;
    private final e e;
    private boolean f;
    private boolean g;
    private boolean h;
    private i<Bitmap> i;
    private a j;
    private boolean k;
    private a l;
    private Bitmap m;
    private m<Bitmap> n;

    public interface b {
        void f();
    }

    public g(com.a.a.c cVar, com.a.a.b.a aVar, int i2, int i3, m<Bitmap> mVar, Bitmap bitmap) {
        this(cVar.a(), com.a.a.c.b(cVar.c()), aVar, null, a(com.a.a.c.b(cVar.c()), i2, i3), mVar, bitmap);
    }

    g(e eVar, j jVar, com.a.a.b.a aVar, Handler handler, i<Bitmap> iVar, m<Bitmap> mVar, Bitmap bitmap) {
        this.d = new ArrayList();
        this.f = false;
        this.g = false;
        this.h = false;
        this.f910a = jVar;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new c()) : handler;
        this.e = eVar;
        this.c = handler;
        this.i = iVar;
        this.b = aVar;
        a(mVar, bitmap);
    }

    /* access modifiers changed from: package-private */
    public void a(m<Bitmap> mVar, Bitmap bitmap) {
        this.n = (m) h.a(mVar);
        this.m = (Bitmap) h.a(bitmap);
        this.i = this.i.a(new d().a(mVar));
    }

    /* access modifiers changed from: package-private */
    public Bitmap a() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar) {
        if (this.k) {
            throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
        }
        boolean isEmpty = this.d.isEmpty();
        if (this.d.contains(bVar)) {
            throw new IllegalStateException("Cannot subscribe twice in a row");
        }
        this.d.add(bVar);
        if (isEmpty) {
            l();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(b bVar) {
        this.d.remove(bVar);
        if (this.d.isEmpty()) {
            m();
        }
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return i().getWidth();
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return i().getHeight();
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.b.g() + k();
    }

    /* access modifiers changed from: package-private */
    public int e() {
        if (this.j != null) {
            return this.j.f911a;
        }
        return -1;
    }

    private int k() {
        return com.a.a.j.i.a(i().getWidth(), i().getHeight(), i().getConfig());
    }

    /* access modifiers changed from: package-private */
    public ByteBuffer f() {
        return this.b.a().asReadOnlyBuffer();
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.b.d();
    }

    private void l() {
        if (!this.f) {
            this.f = true;
            this.k = false;
            n();
        }
    }

    private void m() {
        this.f = false;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.d.clear();
        o();
        m();
        if (this.j != null) {
            this.f910a.a((com.a.a.h.a.h<?>) this.j);
            this.j = null;
        }
        if (this.l != null) {
            this.f910a.a((com.a.a.h.a.h<?>) this.l);
            this.l = null;
        }
        this.b.i();
        this.k = true;
    }

    /* access modifiers changed from: package-private */
    public Bitmap i() {
        return this.j != null ? this.j.a() : this.m;
    }

    private void n() {
        if (this.f && !this.g) {
            if (this.h) {
                this.b.f();
                this.h = false;
            }
            this.g = true;
            long c2 = ((long) this.b.c()) + SystemClock.uptimeMillis();
            this.b.b();
            this.l = new a(this.c, this.b.e(), c2);
            this.i.clone().a(d.a(j())).a(this.b).a((com.a.a.h.a.h) this.l);
        }
    }

    private void o() {
        if (this.m != null) {
            this.e.a(this.m);
            this.m = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        if (this.k) {
            this.c.obtainMessage(2, aVar).sendToTarget();
            return;
        }
        if (aVar.a() != null) {
            o();
            a aVar2 = this.j;
            this.j = aVar;
            for (int size = this.d.size() - 1; size >= 0; size--) {
                this.d.get(size).f();
            }
            if (aVar2 != null) {
                this.c.obtainMessage(2, aVar2).sendToTarget();
            }
        }
        this.g = false;
        n();
    }

    private class c implements Handler.Callback {
        c() {
        }

        public boolean handleMessage(Message message) {
            if (message.what == 1) {
                g.this.a((a) message.obj);
                return true;
            }
            if (message.what == 2) {
                g.this.f910a.a((com.a.a.h.a.h<?>) ((a) message.obj));
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public static class a extends f<Bitmap> {

        /* renamed from: a  reason: collision with root package name */
        final int f911a;
        private final Handler b;
        private final long c;
        private Bitmap d;

        @Override // com.a.a.h.a.h
        public /* bridge */ /* synthetic */ void a(Object obj, com.a.a.h.b.b bVar) {
            a((Bitmap) obj, (com.a.a.h.b.b<? super Bitmap>) bVar);
        }

        a(Handler handler, int i, long j) {
            this.b = handler;
            this.f911a = i;
            this.c = j;
        }

        /* access modifiers changed from: package-private */
        public Bitmap a() {
            return this.d;
        }

        public void a(Bitmap bitmap, com.a.a.h.b.b<? super Bitmap> bVar) {
            this.d = bitmap;
            this.b.sendMessageAtTime(this.b.obtainMessage(1, this), this.c);
        }
    }

    private static i<Bitmap> a(j jVar, int i2, int i3) {
        return jVar.f().a(d.a(com.a.a.d.b.h.b).a(true).a(i2, i3));
    }

    static com.a.a.d.h j() {
        return new com.a.a.i.b(Double.valueOf(Math.random()));
    }
}
