package com.a.a.d.d.a;

import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import com.a.a.d.f;
import com.a.a.j.h;
import com.google.android.exoplayer.C;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public final class j implements f {

    /* renamed from: a  reason: collision with root package name */
    static final byte[] f886a = "Exif\u0000\u0000".getBytes(Charset.forName(C.UTF8_NAME));
    private static final int[] b = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};

    /* access modifiers changed from: private */
    public interface c {
        int a();

        int a(byte[] bArr, int i);

        long a(long j);

        short b();

        int c();
    }

    @Override // com.a.a.d.f
    public f.a a(InputStream inputStream) {
        return a(new d((InputStream) h.a(inputStream)));
    }

    @Override // com.a.a.d.f
    public f.a a(ByteBuffer byteBuffer) {
        return a(new a((ByteBuffer) h.a(byteBuffer)));
    }

    @Override // com.a.a.d.f
    public int a(InputStream inputStream, com.a.a.d.b.a.b bVar) {
        return a(new d((InputStream) h.a(inputStream)), (com.a.a.d.b.a.b) h.a(bVar));
    }

    private f.a a(c cVar) {
        int a2 = cVar.a();
        if (a2 == 65496) {
            return f.a.JPEG;
        }
        int a3 = ((a2 << 16) & SupportMenu.CATEGORY_MASK) | (cVar.a() & SupportMenu.USER_MASK);
        if (a3 == -1991225785) {
            cVar.a(21);
            return cVar.c() >= 3 ? f.a.PNG_A : f.a.PNG;
        } else if ((a3 >> 8) == 4671814) {
            return f.a.GIF;
        } else {
            if (a3 != 1380533830) {
                return f.a.UNKNOWN;
            }
            cVar.a(4);
            if ((((cVar.a() << 16) & SupportMenu.CATEGORY_MASK) | (cVar.a() & SupportMenu.USER_MASK)) != 1464156752) {
                return f.a.UNKNOWN;
            }
            int a4 = ((cVar.a() << 16) & SupportMenu.CATEGORY_MASK) | (cVar.a() & SupportMenu.USER_MASK);
            if ((a4 & InputDeviceCompat.SOURCE_ANY) != 1448097792) {
                return f.a.UNKNOWN;
            }
            if ((a4 & 255) == 88) {
                cVar.a(4);
                return (cVar.c() & 16) != 0 ? f.a.WEBP_A : f.a.WEBP;
            } else if ((a4 & 255) != 76) {
                return f.a.WEBP;
            } else {
                cVar.a(4);
                return (cVar.c() & 8) != 0 ? f.a.WEBP_A : f.a.WEBP;
            }
        }
    }

    private int a(c cVar, com.a.a.d.b.a.b bVar) {
        int a2 = cVar.a();
        if (a(a2)) {
            int b2 = b(cVar);
            if (b2 != -1) {
                byte[] bArr = (byte[]) bVar.a(b2, byte[].class);
                try {
                    return a(cVar, bArr, b2);
                } finally {
                    bVar.a(bArr, byte[].class);
                }
            } else if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            } else {
                Log.d("DfltImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
                return -1;
            }
        } else if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
            return -1;
        } else {
            Log.d("DfltImageHeaderParser", "Parser doesn't handle magic number: " + a2);
            return -1;
        }
    }

    private int a(c cVar, byte[] bArr, int i) {
        int a2 = cVar.a(bArr, i);
        if (a2 != i) {
            if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            }
            Log.d("DfltImageHeaderParser", "Unable to read exif segment data, length: " + i + ", actually read: " + a2);
            return -1;
        } else if (a(bArr, i)) {
            return a(new b(bArr, i));
        } else {
            if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            }
            Log.d("DfltImageHeaderParser", "Missing jpeg exif preamble");
            return -1;
        }
    }

    private boolean a(byte[] bArr, int i) {
        boolean z = bArr != null && i > f886a.length;
        if (z) {
            for (int i2 = 0; i2 < f886a.length; i2++) {
                if (bArr[i2] != f886a[i2]) {
                    return false;
                }
            }
        }
        return z;
    }

    private int b(c cVar) {
        short b2;
        int a2;
        long a3;
        do {
            short b3 = cVar.b();
            if (b3 == 255) {
                b2 = cVar.b();
                if (b2 == 218) {
                    return -1;
                }
                if (b2 != 217) {
                    a2 = cVar.a() - 2;
                    if (b2 == 225) {
                        return a2;
                    }
                    a3 = cVar.a((long) a2);
                } else if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                    return -1;
                } else {
                    Log.d("DfltImageHeaderParser", "Found MARKER_EOI in exif segment");
                    return -1;
                }
            } else if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
                return -1;
            } else {
                Log.d("DfltImageHeaderParser", "Unknown segmentId=" + ((int) b3));
                return -1;
            }
        } while (a3 == ((long) a2));
        if (!Log.isLoggable("DfltImageHeaderParser", 3)) {
            return -1;
        }
        Log.d("DfltImageHeaderParser", "Unable to skip enough data, type: " + ((int) b2) + ", wanted to skip: " + a2 + ", but actually skipped: " + a3);
        return -1;
    }

    private static int a(b bVar) {
        ByteOrder byteOrder;
        int length = "Exif\u0000\u0000".length();
        short b2 = bVar.b(length);
        if (b2 == 19789) {
            byteOrder = ByteOrder.BIG_ENDIAN;
        } else if (b2 == 18761) {
            byteOrder = ByteOrder.LITTLE_ENDIAN;
        } else {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Unknown endianness = " + ((int) b2));
            }
            byteOrder = ByteOrder.BIG_ENDIAN;
        }
        bVar.a(byteOrder);
        int a2 = length + bVar.a(length + 4);
        short b3 = bVar.b(a2);
        for (int i = 0; i < b3; i++) {
            int a3 = a(a2, i);
            short b4 = bVar.b(a3);
            if (b4 == 274) {
                short b5 = bVar.b(a3 + 2);
                if (b5 >= 1 && b5 <= 12) {
                    int a4 = bVar.a(a3 + 4);
                    if (a4 >= 0) {
                        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got tagIndex=" + i + " tagType=" + ((int) b4) + " formatCode=" + ((int) b5) + " componentCount=" + a4);
                        }
                        int i2 = a4 + b[b5];
                        if (i2 <= 4) {
                            int i3 = a3 + 8;
                            if (i3 < 0 || i3 > bVar.a()) {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal tagValueOffset=" + i3 + " tagType=" + ((int) b4));
                                }
                            } else if (i2 >= 0 && i3 + i2 <= bVar.a()) {
                                return bVar.b(i3);
                            } else {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + ((int) b4));
                                }
                            }
                        } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + ((int) b5));
                        }
                    } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                        Log.d("DfltImageHeaderParser", "Negative tiff component count");
                    }
                } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Got invalid format code = " + ((int) b5));
                }
            }
        }
        return -1;
    }

    private static int a(int i, int i2) {
        return i + 2 + (i2 * 12);
    }

    private static boolean a(int i) {
        return (i & 65496) == 65496 || i == 19789 || i == 18761;
    }

    /* access modifiers changed from: private */
    public static final class b {

        /* renamed from: a  reason: collision with root package name */
        private final ByteBuffer f888a;

        b(byte[] bArr, int i) {
            this.f888a = (ByteBuffer) ByteBuffer.wrap(bArr).order(ByteOrder.BIG_ENDIAN).limit(i);
        }

        /* access modifiers changed from: package-private */
        public void a(ByteOrder byteOrder) {
            this.f888a.order(byteOrder);
        }

        /* access modifiers changed from: package-private */
        public int a() {
            return this.f888a.remaining();
        }

        /* access modifiers changed from: package-private */
        public int a(int i) {
            if (a(i, 4)) {
                return this.f888a.getInt(i);
            }
            return -1;
        }

        /* access modifiers changed from: package-private */
        public short b(int i) {
            if (a(i, 2)) {
                return this.f888a.getShort(i);
            }
            return -1;
        }

        private boolean a(int i, int i2) {
            return this.f888a.remaining() - i >= i2;
        }
    }

    private static final class a implements c {

        /* renamed from: a  reason: collision with root package name */
        private final ByteBuffer f887a;

        a(ByteBuffer byteBuffer) {
            this.f887a = byteBuffer;
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
        }

        @Override // com.a.a.d.d.a.j.c
        public int a() {
            return ((c() << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK) | (c() & 255);
        }

        @Override // com.a.a.d.d.a.j.c
        public short b() {
            return (short) (c() & 255);
        }

        @Override // com.a.a.d.d.a.j.c
        public long a(long j) {
            int min = (int) Math.min((long) this.f887a.remaining(), j);
            this.f887a.position(this.f887a.position() + min);
            return (long) min;
        }

        @Override // com.a.a.d.d.a.j.c
        public int a(byte[] bArr, int i) {
            int min = Math.min(i, this.f887a.remaining());
            if (min == 0) {
                return -1;
            }
            this.f887a.get(bArr, 0, min);
            return min;
        }

        @Override // com.a.a.d.d.a.j.c
        public int c() {
            if (this.f887a.remaining() < 1) {
                return -1;
            }
            return this.f887a.get();
        }
    }

    private static final class d implements c {

        /* renamed from: a  reason: collision with root package name */
        private final InputStream f889a;

        d(InputStream inputStream) {
            this.f889a = inputStream;
        }

        @Override // com.a.a.d.d.a.j.c
        public int a() {
            return ((this.f889a.read() << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK) | (this.f889a.read() & 255);
        }

        @Override // com.a.a.d.d.a.j.c
        public short b() {
            return (short) (this.f889a.read() & 255);
        }

        @Override // com.a.a.d.d.a.j.c
        public long a(long j) {
            if (j < 0) {
                return 0;
            }
            long j2 = j;
            while (j2 > 0) {
                long skip = this.f889a.skip(j2);
                if (skip > 0) {
                    j2 -= skip;
                } else if (this.f889a.read() == -1) {
                    break;
                } else {
                    j2--;
                }
            }
            return j - j2;
        }

        @Override // com.a.a.d.d.a.j.c
        public int a(byte[] bArr, int i) {
            int i2 = i;
            while (i2 > 0) {
                int read = this.f889a.read(bArr, i - i2, i2);
                if (read == -1) {
                    break;
                }
                i2 -= read;
            }
            return i - i2;
        }

        @Override // com.a.a.d.d.a.j.c
        public int c() {
            return this.f889a.read();
        }
    }
}
