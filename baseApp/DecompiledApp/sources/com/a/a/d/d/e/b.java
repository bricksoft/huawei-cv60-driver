package com.a.a.d.d.e;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.a.a.b.a;
import com.a.a.d.b.a.e;

public final class b implements a.AbstractC0027a {

    /* renamed from: a  reason: collision with root package name */
    private final e f907a;
    @Nullable
    private final com.a.a.d.b.a.b b;

    public b(e eVar, com.a.a.d.b.a.b bVar) {
        this.f907a = eVar;
        this.b = bVar;
    }

    @Override // com.a.a.b.a.AbstractC0027a
    @NonNull
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.f907a.b(i, i2, config);
    }

    @Override // com.a.a.b.a.AbstractC0027a
    public void a(Bitmap bitmap) {
        this.f907a.a(bitmap);
    }

    @Override // com.a.a.b.a.AbstractC0027a
    public byte[] a(int i) {
        if (this.b == null) {
            return new byte[i];
        }
        return (byte[]) this.b.a(i, byte[].class);
    }

    @Override // com.a.a.b.a.AbstractC0027a
    public void a(byte[] bArr) {
        if (this.b != null) {
            this.b.a(bArr, byte[].class);
        }
    }

    @Override // com.a.a.b.a.AbstractC0027a
    public int[] b(int i) {
        if (this.b == null) {
            return new int[i];
        }
        return (int[]) this.b.a(i, int[].class);
    }

    @Override // com.a.a.b.a.AbstractC0027a
    public void a(int[] iArr) {
        if (this.b != null) {
            this.b.a(iArr, int[].class);
        }
    }
}
