package com.a.a.d.d.e;

import android.content.Context;
import android.graphics.Bitmap;
import com.a.a.c;
import com.a.a.d.b.s;
import com.a.a.d.d.a.e;
import com.a.a.d.m;
import com.a.a.j.h;
import java.security.MessageDigest;

public class f implements m<c> {
    private final m<Bitmap> b;

    public f(m<Bitmap> mVar) {
        this.b = (m) h.a(mVar);
    }

    @Override // com.a.a.d.m
    public s<c> a(Context context, s<c> sVar, int i, int i2) {
        c c = sVar.c();
        s<Bitmap> eVar = new e(c.b(), c.a(context).a());
        s<Bitmap> a2 = this.b.a(context, eVar, i, i2);
        if (!eVar.equals(a2)) {
            eVar.e();
        }
        c.a(this.b, a2.c());
        return sVar;
    }

    @Override // com.a.a.d.m, com.a.a.d.h
    public boolean equals(Object obj) {
        if (obj instanceof f) {
            return this.b.equals(((f) obj).b);
        }
        return false;
    }

    @Override // com.a.a.d.m, com.a.a.d.h
    public int hashCode() {
        return this.b.hashCode();
    }

    @Override // com.a.a.d.h
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
