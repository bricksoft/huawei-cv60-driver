package com.a.a.d.d.a;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import com.a.a.d.b.p;
import com.a.a.d.b.s;
import com.a.a.j.h;
import com.a.a.j.i;

public class e implements p, s<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final Bitmap f884a;
    private final com.a.a.d.b.a.e b;

    @Nullable
    public static e a(@Nullable Bitmap bitmap, com.a.a.d.b.a.e eVar) {
        if (bitmap == null) {
            return null;
        }
        return new e(bitmap, eVar);
    }

    public e(Bitmap bitmap, com.a.a.d.b.a.e eVar) {
        this.f884a = (Bitmap) h.a(bitmap, "Bitmap must not be null");
        this.b = (com.a.a.d.b.a.e) h.a(eVar, "BitmapPool must not be null");
    }

    @Override // com.a.a.d.b.s
    public Class<Bitmap> b() {
        return Bitmap.class;
    }

    /* renamed from: f */
    public Bitmap c() {
        return this.f884a;
    }

    @Override // com.a.a.d.b.s
    public int d() {
        return i.a(this.f884a);
    }

    @Override // com.a.a.d.b.s
    public void e() {
        this.b.a(this.f884a);
    }

    @Override // com.a.a.d.b.p
    public void a() {
        this.f884a.prepareToDraw();
    }
}
