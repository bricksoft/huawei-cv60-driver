package com.a.a.d.d.e;

import android.util.Log;
import com.a.a.d.b.s;
import com.a.a.d.c;
import com.a.a.d.j;
import com.a.a.d.l;
import com.a.a.j.a;
import java.io.File;
import java.io.IOException;

public class d implements l<c> {
    @Override // com.a.a.d.l
    public c a(j jVar) {
        return c.SOURCE;
    }

    public boolean a(s<c> sVar, File file, j jVar) {
        try {
            a.a(sVar.c().c(), file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", e);
            }
            return false;
        }
    }
}
