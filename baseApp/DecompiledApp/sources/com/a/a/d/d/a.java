package com.a.a.d.d;

import com.a.a.d.b.s;
import com.a.a.j.h;

public class a<T> implements s<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final T f880a;

    public a(T t) {
        this.f880a = (T) h.a((Object) t);
    }

    @Override // com.a.a.d.b.s
    public Class<T> b() {
        return (Class<T>) this.f880a.getClass();
    }

    @Override // com.a.a.d.b.s
    public final T c() {
        return this.f880a;
    }

    @Override // com.a.a.d.b.s
    public final int d() {
        return 1;
    }

    @Override // com.a.a.d.b.s
    public void e() {
    }
}
