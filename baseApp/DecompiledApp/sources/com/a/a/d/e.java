package com.a.a.d;

import android.support.annotation.Nullable;
import java.io.IOException;

public final class e extends IOException {

    /* renamed from: a  reason: collision with root package name */
    private final int f920a;

    public e(int i) {
        this("Http request failed with status code: " + i, i);
    }

    public e(String str) {
        this(str, -1);
    }

    public e(String str, int i) {
        this(str, i, null);
    }

    public e(String str, int i, @Nullable Throwable th) {
        super(str, th);
        this.f920a = i;
    }
}
