package com.a.a.d;

import com.google.android.exoplayer.C;
import java.nio.charset.Charset;
import java.security.MessageDigest;

public interface h {

    /* renamed from: a  reason: collision with root package name */
    public static final Charset f922a = Charset.forName(C.UTF8_NAME);

    void a(MessageDigest messageDigest);

    boolean equals(Object obj);

    int hashCode();
}
