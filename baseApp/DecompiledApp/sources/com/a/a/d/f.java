package com.a.a.d;

import com.a.a.d.b.a.b;
import java.io.InputStream;
import java.nio.ByteBuffer;

public interface f {
    int a(InputStream inputStream, b bVar);

    a a(InputStream inputStream);

    a a(ByteBuffer byteBuffer);

    public enum a {
        GIF(true),
        JPEG(false),
        RAW(false),
        PNG_A(true),
        PNG(false),
        WEBP_A(true),
        WEBP(false),
        UNKNOWN(false);
        
        private final boolean i;

        private a(boolean z) {
            this.i = z;
        }

        public boolean a() {
            return this.i;
        }
    }
}
