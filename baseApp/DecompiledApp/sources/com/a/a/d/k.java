package com.a.a.d;

import android.support.annotation.Nullable;
import com.a.a.d.b.s;

public interface k<T, Z> {
    @Nullable
    s<Z> a(T t, int i, int i2, j jVar);

    boolean a(T t, j jVar);
}
