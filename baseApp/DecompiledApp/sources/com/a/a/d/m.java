package com.a.a.d;

import android.content.Context;
import com.a.a.d.b.s;

public interface m<T> extends h {
    s<T> a(Context context, s<T> sVar, int i, int i2);

    @Override // com.a.a.d.h
    boolean equals(Object obj);

    @Override // com.a.a.d.h
    int hashCode();
}
