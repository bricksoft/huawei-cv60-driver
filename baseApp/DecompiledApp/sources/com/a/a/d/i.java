package com.a.a.d;

import android.support.annotation.Nullable;
import com.a.a.j.h;
import java.security.MessageDigest;

public final class i<T> {

    /* renamed from: a  reason: collision with root package name */
    private static final a<Object> f923a = new a<Object>() {
        /* class com.a.a.d.i.AnonymousClass1 */

        @Override // com.a.a.d.i.a
        public void a(byte[] bArr, Object obj, MessageDigest messageDigest) {
        }
    };
    private final T b;
    private final a<T> c;
    private final String d;
    private volatile byte[] e;

    public interface a<T> {
        void a(byte[] bArr, T t, MessageDigest messageDigest);
    }

    public static <T> i<T> a(String str) {
        return new i<>(str, null, c());
    }

    public static <T> i<T> a(String str, T t) {
        return new i<>(str, t, c());
    }

    public static <T> i<T> a(String str, T t, a<T> aVar) {
        return new i<>(str, t, aVar);
    }

    i(String str, T t, a<T> aVar) {
        this.d = h.a(str);
        this.b = t;
        this.c = (a) h.a(aVar);
    }

    @Nullable
    public T a() {
        return this.b;
    }

    public void a(T t, MessageDigest messageDigest) {
        this.c.a(b(), t, messageDigest);
    }

    private byte[] b() {
        if (this.e == null) {
            this.e = this.d.getBytes(h.f922a);
        }
        return this.e;
    }

    public boolean equals(Object obj) {
        if (obj instanceof i) {
            return this.d.equals(((i) obj).d);
        }
        return false;
    }

    public int hashCode() {
        return this.d.hashCode();
    }

    private static <T> a<T> c() {
        return (a<T>) f923a;
    }

    public String toString() {
        return "Option{key='" + this.d + '\'' + '}';
    }
}
