package com.a.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.widget.ImageView;
import com.a.a.d.b.i;
import com.a.a.h.a.h;
import com.a.a.h.d;
import java.util.Map;

@TargetApi(14)
public class e extends ContextWrapper {
    @VisibleForTesting

    /* renamed from: a  reason: collision with root package name */
    static final k<?, ?> f924a = new b();
    private final Handler b = new Handler(Looper.getMainLooper());
    private final h c;
    private final com.a.a.h.a.e d;
    private final d e;
    private final Map<Class<?>, k<?, ?>> f;
    private final i g;
    private final int h;

    public e(Context context, h hVar, com.a.a.h.a.e eVar, d dVar, Map<Class<?>, k<?, ?>> map, i iVar, int i) {
        super(context.getApplicationContext());
        this.c = hVar;
        this.d = eVar;
        this.e = dVar;
        this.f = map;
        this.g = iVar;
        this.h = i;
    }

    public d a() {
        return this.e;
    }

    @NonNull
    public <T> k<?, T> a(Class<T> cls) {
        k<?, T> kVar = (k<?, T>) this.f.get(cls);
        if (kVar == null) {
            for (Map.Entry<Class<?>, k<?, ?>> entry : this.f.entrySet()) {
                kVar = entry.getKey().isAssignableFrom(cls) ? (k<?, T>) entry.getValue() : kVar;
            }
            kVar = kVar;
        }
        return kVar == null ? (k<?, T>) f924a : kVar;
    }

    public <X> h<X> a(ImageView imageView, Class<X> cls) {
        return this.d.a(imageView, cls);
    }

    public i b() {
        return this.g;
    }

    public h c() {
        return this.c;
    }

    public int d() {
        return this.h;
    }
}
