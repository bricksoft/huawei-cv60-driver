package com.a.a.h.a;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import com.a.a.j.h;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class i<T extends View, Z> extends a<Z> {
    private static boolean b = false;
    @Nullable
    private static Integer c = null;

    /* renamed from: a  reason: collision with root package name */
    protected final T f950a;
    private final a d;

    public i(T t) {
        this(t, false);
    }

    public i(T t, boolean z) {
        this.f950a = (T) ((View) h.a(t));
        this.d = new a(t, z);
    }

    @Override // com.a.a.h.a.h
    public void a(g gVar) {
        this.d.a(gVar);
    }

    @Override // com.a.a.h.a.h
    public void b(g gVar) {
        this.d.b(gVar);
    }

    @Override // com.a.a.h.a.a, com.a.a.h.a.h
    public void a(Drawable drawable) {
        super.a(drawable);
        this.d.b();
    }

    @Override // com.a.a.h.a.a, com.a.a.h.a.h
    public void a(@Nullable com.a.a.h.a aVar) {
        a((Object) aVar);
    }

    @Override // com.a.a.h.a.a, com.a.a.h.a.h
    @Nullable
    public com.a.a.h.a b() {
        Object a2 = a();
        if (a2 == null) {
            return null;
        }
        if (a2 instanceof com.a.a.h.a) {
            return (com.a.a.h.a) a2;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    public String toString() {
        return "Target for: " + this.f950a;
    }

    private void a(@Nullable Object obj) {
        if (c == null) {
            b = true;
            this.f950a.setTag(obj);
            return;
        }
        this.f950a.setTag(c.intValue(), obj);
    }

    @Nullable
    private Object a() {
        if (c == null) {
            return this.f950a.getTag();
        }
        return this.f950a.getTag(c.intValue());
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public static final class a {
        @VisibleForTesting
        @Nullable

        /* renamed from: a  reason: collision with root package name */
        static Integer f951a;
        private final View b;
        private final boolean c;
        private final List<g> d = new ArrayList();
        @Nullable
        private ViewTreeObserver$OnPreDrawListenerC0038a e;

        a(View view, boolean z) {
            this.b = view;
            this.c = z;
        }

        private static int a(Context context) {
            if (f951a == null) {
                Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                f951a = Integer.valueOf(Math.max(point.x, point.y));
            }
            return f951a.intValue();
        }

        private void a(int i, int i2) {
            Iterator it = new ArrayList(this.d).iterator();
            while (it.hasNext()) {
                ((g) it.next()).a(i, i2);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (!this.d.isEmpty()) {
                int d2 = d();
                int c2 = c();
                if (b(d2, c2)) {
                    a(d2, c2);
                    b();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(g gVar) {
            int d2 = d();
            int c2 = c();
            if (b(d2, c2)) {
                gVar.a(d2, c2);
                return;
            }
            if (!this.d.contains(gVar)) {
                this.d.add(gVar);
            }
            if (this.e == null) {
                ViewTreeObserver viewTreeObserver = this.b.getViewTreeObserver();
                this.e = new ViewTreeObserver$OnPreDrawListenerC0038a(this);
                viewTreeObserver.addOnPreDrawListener(this.e);
            }
        }

        /* access modifiers changed from: package-private */
        public void b(g gVar) {
            this.d.remove(gVar);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            ViewTreeObserver viewTreeObserver = this.b.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.e);
            }
            this.e = null;
            this.d.clear();
        }

        private boolean b(int i, int i2) {
            return a(i) && a(i2);
        }

        private int c() {
            int paddingBottom = this.b.getPaddingBottom() + this.b.getPaddingTop();
            ViewGroup.LayoutParams layoutParams = this.b.getLayoutParams();
            return a(this.b.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingBottom);
        }

        private int d() {
            int paddingRight = this.b.getPaddingRight() + this.b.getPaddingLeft();
            ViewGroup.LayoutParams layoutParams = this.b.getLayoutParams();
            return a(this.b.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingRight);
        }

        private int a(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.c && this.b.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.b.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            if (Log.isLoggable("ViewTarget", 4)) {
                Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use .override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            return a(this.b.getContext());
        }

        private boolean a(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        /* access modifiers changed from: private */
        /* renamed from: com.a.a.h.a.i$a$a  reason: collision with other inner class name */
        public static final class ViewTreeObserver$OnPreDrawListenerC0038a implements ViewTreeObserver.OnPreDrawListener {

            /* renamed from: a  reason: collision with root package name */
            private final WeakReference<a> f952a;

            ViewTreeObserver$OnPreDrawListenerC0038a(a aVar) {
                this.f952a = new WeakReference<>(aVar);
            }

            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called listener=" + this);
                }
                a aVar = this.f952a.get();
                if (aVar == null) {
                    return true;
                }
                aVar.a();
                return true;
            }
        }
    }
}
