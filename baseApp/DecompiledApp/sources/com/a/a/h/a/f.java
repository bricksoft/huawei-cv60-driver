package com.a.a.h.a;

import com.a.a.j.i;

public abstract class f<Z> extends a<Z> {

    /* renamed from: a  reason: collision with root package name */
    private final int f949a;
    private final int b;

    public f() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public f(int i, int i2) {
        this.f949a = i;
        this.b = i2;
    }

    @Override // com.a.a.h.a.h
    public final void a(g gVar) {
        if (!i.a(this.f949a, this.b)) {
            throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + this.f949a + " and height: " + this.b + ", either provide dimensions in the constructor or call override()");
        }
        gVar.a(this.f949a, this.b);
    }

    @Override // com.a.a.h.a.h
    public void b(g gVar) {
    }
}
