package com.a.a.h.a;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.widget.ImageView;

public class c extends d<Drawable> {
    public c(ImageView imageView) {
        super(imageView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void a(@Nullable Drawable drawable) {
        ((ImageView) this.f950a).setImageDrawable(drawable);
    }
}
