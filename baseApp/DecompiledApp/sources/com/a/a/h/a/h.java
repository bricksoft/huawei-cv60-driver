package com.a.a.h.a;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import com.a.a.e.i;
import com.a.a.h.a;
import com.a.a.h.b.b;

public interface h<R> extends i {
    void a(@Nullable Drawable drawable);

    void a(g gVar);

    void a(@Nullable a aVar);

    void a(R r, b<? super R> bVar);

    @Nullable
    a b();

    void b(@Nullable Drawable drawable);

    void b(g gVar);

    void c(@Nullable Drawable drawable);
}
