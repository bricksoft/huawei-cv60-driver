package com.a.a.h.a;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import com.a.a.h.b.b;

public abstract class d<Z> extends i<ImageView, Z> implements b.a {
    @Nullable
    private Animatable b;

    /* access modifiers changed from: protected */
    public abstract void a(@Nullable Z z);

    public d(ImageView imageView) {
        super(imageView);
    }

    public void e(Drawable drawable) {
        ((ImageView) this.f950a).setImageDrawable(drawable);
    }

    @Override // com.a.a.h.a.a, com.a.a.h.a.h
    public void b(@Nullable Drawable drawable) {
        super.b(drawable);
        b((Object) null);
        e(drawable);
    }

    @Override // com.a.a.h.a.a, com.a.a.h.a.h
    public void c(@Nullable Drawable drawable) {
        super.c(drawable);
        b((Object) null);
        e(drawable);
    }

    @Override // com.a.a.h.a.a, com.a.a.h.a.h, com.a.a.h.a.i
    public void a(@Nullable Drawable drawable) {
        super.a(drawable);
        b((Object) null);
        e(drawable);
    }

    @Override // com.a.a.h.a.h
    public void a(Z z, @Nullable b<? super Z> bVar) {
        if (bVar == null || !bVar.a(z, this)) {
            b((Object) z);
        } else {
            c((Object) z);
        }
    }

    @Override // com.a.a.e.i, com.a.a.h.a.a
    public void c() {
        if (this.b != null) {
            this.b.start();
        }
    }

    @Override // com.a.a.e.i, com.a.a.h.a.a
    public void d() {
        if (this.b != null) {
            this.b.stop();
        }
    }

    private void b(@Nullable Z z) {
        c((Object) z);
        a((Object) z);
    }

    private void c(@Nullable Z z) {
        if (z instanceof Animatable) {
            this.b = z;
            this.b.start();
            return;
        }
        this.b = null;
    }
}
