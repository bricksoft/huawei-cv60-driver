package com.a.a.h.a;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;

public abstract class a<Z> implements h<Z> {

    /* renamed from: a  reason: collision with root package name */
    private com.a.a.h.a f948a;

    @Override // com.a.a.h.a.h
    public void a(@Nullable com.a.a.h.a aVar) {
        this.f948a = aVar;
    }

    @Override // com.a.a.h.a.h
    @Nullable
    public com.a.a.h.a b() {
        return this.f948a;
    }

    @Override // com.a.a.h.a.h
    public void a(@Nullable Drawable drawable) {
    }

    @Override // com.a.a.h.a.h
    public void b(@Nullable Drawable drawable) {
    }

    @Override // com.a.a.h.a.h
    public void c(@Nullable Drawable drawable) {
    }

    @Override // com.a.a.e.i
    public void c() {
    }

    @Override // com.a.a.e.i
    public void d() {
    }

    @Override // com.a.a.e.i
    public void e() {
    }
}
