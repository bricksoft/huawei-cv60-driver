package com.a.a.h;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.a.a.d.b.h;
import com.a.a.d.d.a.c;
import com.a.a.d.d.a.k;
import com.a.a.d.d.a.l;
import com.a.a.d.d.e.f;
import com.a.a.d.i;
import com.a.a.d.j;
import com.a.a.d.m;
import com.a.a.g;
import com.a.a.i.a;
import java.util.HashMap;
import java.util.Map;

public class d implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f954a;
    private float b = 1.0f;
    @NonNull
    private h c = h.e;
    @NonNull
    private g d = g.NORMAL;
    @Nullable
    private Drawable e;
    private int f;
    @Nullable
    private Drawable g;
    private int h;
    private boolean i = true;
    private int j = -1;
    private int k = -1;
    @NonNull
    private com.a.a.d.h l = a.a();
    private boolean m;
    private boolean n = true;
    @Nullable
    private Drawable o;
    private int p;
    @NonNull
    private j q = new j();
    @NonNull
    private Map<Class<?>, m<?>> r = new HashMap();
    @NonNull
    private Class<?> s = Object.class;
    private boolean t;
    @Nullable
    private Resources.Theme u;
    private boolean v;
    private boolean w;
    private boolean x;
    private boolean y = true;

    @CheckResult
    public static d a(@NonNull h hVar) {
        return new d().b(hVar);
    }

    @CheckResult
    public static d a(@NonNull com.a.a.d.h hVar) {
        return new d().b(hVar);
    }

    @CheckResult
    public static d a(@NonNull Class<?> cls) {
        return new d().b(cls);
    }

    private static boolean b(int i2, int i3) {
        return (i2 & i3) != 0;
    }

    @CheckResult
    public d a(float f2) {
        if (this.v) {
            return clone().a(f2);
        }
        if (f2 < 0.0f || f2 > 1.0f) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }
        this.b = f2;
        this.f954a |= 2;
        return F();
    }

    @CheckResult
    public d b(@NonNull h hVar) {
        if (this.v) {
            return clone().b(hVar);
        }
        this.c = (h) com.a.a.j.h.a(hVar);
        this.f954a |= 4;
        return F();
    }

    @CheckResult
    public d a(@NonNull g gVar) {
        if (this.v) {
            return clone().a(gVar);
        }
        this.d = (g) com.a.a.j.h.a(gVar);
        this.f954a |= 8;
        return F();
    }

    @CheckResult
    public d a(boolean z) {
        boolean z2 = true;
        if (this.v) {
            return clone().a(true);
        }
        if (z) {
            z2 = false;
        }
        this.i = z2;
        this.f954a |= 256;
        return F();
    }

    @CheckResult
    public d a(int i2, int i3) {
        if (this.v) {
            return clone().a(i2, i3);
        }
        this.k = i2;
        this.j = i3;
        this.f954a |= 512;
        return F();
    }

    @CheckResult
    public d b(@NonNull com.a.a.d.h hVar) {
        if (this.v) {
            return clone().b(hVar);
        }
        this.l = (com.a.a.d.h) com.a.a.j.h.a(hVar);
        this.f954a |= 1024;
        return F();
    }

    @CheckResult
    /* renamed from: a */
    public d clone() {
        try {
            d dVar = (d) super.clone();
            dVar.q = new j();
            dVar.q.a(this.q);
            dVar.r = new HashMap();
            dVar.r.putAll(this.r);
            dVar.t = false;
            dVar.v = false;
            return dVar;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    @CheckResult
    public <T> d a(@NonNull i<T> iVar, @NonNull T t2) {
        if (this.v) {
            return clone().a(iVar, t2);
        }
        com.a.a.j.h.a(iVar);
        com.a.a.j.h.a((Object) t2);
        this.q.a(iVar, t2);
        return F();
    }

    @CheckResult
    public d b(@NonNull Class<?> cls) {
        if (this.v) {
            return clone().b(cls);
        }
        this.s = (Class) com.a.a.j.h.a(cls);
        this.f954a |= 4096;
        return F();
    }

    public final boolean b() {
        return this.n;
    }

    public final boolean c() {
        return a(2048);
    }

    @CheckResult
    public d a(@NonNull k kVar) {
        return a(l.b, com.a.a.j.h.a(kVar));
    }

    @CheckResult
    public d d() {
        return a(k.b, new com.a.a.d.d.a.h());
    }

    @CheckResult
    public d e() {
        return c(k.f890a, new com.a.a.d.d.a.m());
    }

    @CheckResult
    public d f() {
        return c(k.e, new com.a.a.d.d.a.i());
    }

    /* access modifiers changed from: package-private */
    public final d a(k kVar, m<Bitmap> mVar) {
        if (this.v) {
            return clone().a(kVar, mVar);
        }
        a(kVar);
        return b(mVar);
    }

    /* access modifiers changed from: package-private */
    @CheckResult
    public final d b(k kVar, m<Bitmap> mVar) {
        if (this.v) {
            return clone().b(kVar, mVar);
        }
        a(kVar);
        return a(mVar);
    }

    private d c(k kVar, m<Bitmap> mVar) {
        return a(kVar, mVar, false);
    }

    private d a(k kVar, m<Bitmap> mVar, boolean z) {
        d b2 = z ? b(kVar, mVar) : a(kVar, mVar);
        b2.y = true;
        return b2;
    }

    @CheckResult
    public d a(@NonNull m<Bitmap> mVar) {
        if (this.v) {
            return clone().a(mVar);
        }
        b(mVar);
        this.m = true;
        this.f954a |= 131072;
        return F();
    }

    @CheckResult
    public d b(m<Bitmap> mVar) {
        if (this.v) {
            return clone().b(mVar);
        }
        a(Bitmap.class, mVar);
        a(BitmapDrawable.class, new c(mVar));
        a(com.a.a.d.d.e.c.class, new f(mVar));
        return F();
    }

    @CheckResult
    public <T> d a(Class<T> cls, m<T> mVar) {
        if (this.v) {
            return clone().a(cls, mVar);
        }
        com.a.a.j.h.a(cls);
        com.a.a.j.h.a(mVar);
        this.r.put(cls, mVar);
        this.f954a |= 2048;
        this.n = true;
        this.f954a |= 65536;
        this.y = false;
        return F();
    }

    @CheckResult
    public d a(d dVar) {
        if (this.v) {
            return clone().a(dVar);
        }
        if (b(dVar.f954a, 2)) {
            this.b = dVar.b;
        }
        if (b(dVar.f954a, 262144)) {
            this.w = dVar.w;
        }
        if (b(dVar.f954a, 4)) {
            this.c = dVar.c;
        }
        if (b(dVar.f954a, 8)) {
            this.d = dVar.d;
        }
        if (b(dVar.f954a, 16)) {
            this.e = dVar.e;
        }
        if (b(dVar.f954a, 32)) {
            this.f = dVar.f;
        }
        if (b(dVar.f954a, 64)) {
            this.g = dVar.g;
        }
        if (b(dVar.f954a, 128)) {
            this.h = dVar.h;
        }
        if (b(dVar.f954a, 256)) {
            this.i = dVar.i;
        }
        if (b(dVar.f954a, 512)) {
            this.k = dVar.k;
            this.j = dVar.j;
        }
        if (b(dVar.f954a, 1024)) {
            this.l = dVar.l;
        }
        if (b(dVar.f954a, 4096)) {
            this.s = dVar.s;
        }
        if (b(dVar.f954a, 8192)) {
            this.o = dVar.o;
        }
        if (b(dVar.f954a, 16384)) {
            this.p = dVar.p;
        }
        if (b(dVar.f954a, 32768)) {
            this.u = dVar.u;
        }
        if (b(dVar.f954a, 65536)) {
            this.n = dVar.n;
        }
        if (b(dVar.f954a, 131072)) {
            this.m = dVar.m;
        }
        if (b(dVar.f954a, 2048)) {
            this.r.putAll(dVar.r);
            this.y = dVar.y;
        }
        if (b(dVar.f954a, 524288)) {
            this.x = dVar.x;
        }
        if (!this.n) {
            this.r.clear();
            this.f954a &= -2049;
            this.m = false;
            this.f954a &= -131073;
            this.y = true;
        }
        this.f954a |= dVar.f954a;
        this.q.a(dVar.q);
        return F();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        if (Float.compare(dVar.b, this.b) == 0 && this.f == dVar.f && com.a.a.j.i.a(this.e, dVar.e) && this.h == dVar.h && com.a.a.j.i.a(this.g, dVar.g) && this.p == dVar.p && com.a.a.j.i.a(this.o, dVar.o) && this.i == dVar.i && this.j == dVar.j && this.k == dVar.k && this.m == dVar.m && this.n == dVar.n && this.w == dVar.w && this.x == dVar.x && this.c.equals(dVar.c) && this.d == dVar.d && this.q.equals(dVar.q) && this.r.equals(dVar.r) && this.s.equals(dVar.s) && com.a.a.j.i.a(this.l, dVar.l) && com.a.a.j.i.a(this.u, dVar.u)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return com.a.a.j.i.a(this.u, com.a.a.j.i.a(this.l, com.a.a.j.i.a(this.s, com.a.a.j.i.a(this.r, com.a.a.j.i.a(this.q, com.a.a.j.i.a(this.d, com.a.a.j.i.a(this.c, com.a.a.j.i.a(this.x, com.a.a.j.i.a(this.w, com.a.a.j.i.a(this.n, com.a.a.j.i.a(this.m, com.a.a.j.i.b(this.k, com.a.a.j.i.b(this.j, com.a.a.j.i.a(this.i, com.a.a.j.i.a(this.o, com.a.a.j.i.b(this.p, com.a.a.j.i.a(this.g, com.a.a.j.i.b(this.h, com.a.a.j.i.a(this.e, com.a.a.j.i.b(this.f, com.a.a.j.i.a(this.b)))))))))))))))))))));
    }

    public d g() {
        this.t = true;
        return this;
    }

    public d h() {
        if (!this.t || this.v) {
            this.v = true;
            return g();
        }
        throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
    }

    private d F() {
        if (!this.t) {
            return this;
        }
        throw new IllegalStateException("You cannot modify locked RequestOptions, consider clone()");
    }

    @NonNull
    public final Map<Class<?>, m<?>> i() {
        return this.r;
    }

    public final boolean j() {
        return this.m;
    }

    @NonNull
    public final j k() {
        return this.q;
    }

    @NonNull
    public final Class<?> l() {
        return this.s;
    }

    @NonNull
    public final h m() {
        return this.c;
    }

    @Nullable
    public final Drawable n() {
        return this.e;
    }

    public final int o() {
        return this.f;
    }

    public final int p() {
        return this.h;
    }

    @Nullable
    public final Drawable q() {
        return this.g;
    }

    public final int r() {
        return this.p;
    }

    @Nullable
    public final Drawable s() {
        return this.o;
    }

    @Nullable
    public final Resources.Theme t() {
        return this.u;
    }

    public final boolean u() {
        return this.i;
    }

    @NonNull
    public final com.a.a.d.h v() {
        return this.l;
    }

    public final boolean w() {
        return a(8);
    }

    @NonNull
    public final g x() {
        return this.d;
    }

    public final int y() {
        return this.k;
    }

    public final boolean z() {
        return com.a.a.j.i.a(this.k, this.j);
    }

    public final int A() {
        return this.j;
    }

    public final float B() {
        return this.b;
    }

    public boolean C() {
        return this.y;
    }

    private boolean a(int i2) {
        return b(this.f954a, i2);
    }

    public final boolean D() {
        return this.w;
    }

    public final boolean E() {
        return this.x;
    }
}
