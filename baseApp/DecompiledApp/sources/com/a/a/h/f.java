package com.a.a.h;

import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.util.Pools;
import android.util.Log;
import com.a.a.d.b.i;
import com.a.a.d.b.o;
import com.a.a.d.b.s;
import com.a.a.e;
import com.a.a.h.a.g;
import com.a.a.h.a.h;
import com.a.a.h.b.c;
import com.a.a.j.a.a;
import com.a.a.j.a.b;
import com.a.a.j.d;

public final class f<R> implements a, g, e, a.c {

    /* renamed from: a  reason: collision with root package name */
    private static final Pools.Pool<f<?>> f955a = com.a.a.j.a.a.a(150, new a.AbstractC0041a<f<?>>() {
        /* class com.a.a.h.f.AnonymousClass1 */

        /* renamed from: a */
        public f<?> b() {
            return new f<>();
        }
    });
    private static boolean z = true;
    private boolean b;
    private final String c = String.valueOf(super.hashCode());
    private final b d = b.a();
    private b e;
    private e f;
    @Nullable
    private Object g;
    private Class<R> h;
    private d i;
    private int j;
    private int k;
    private com.a.a.g l;
    private h<R> m;
    private c<R> n;
    private i o;
    private c<? super R> p;
    private s<R> q;
    private i.d r;
    private long s;
    private a t;
    private Drawable u;
    private Drawable v;
    private Drawable w;
    private int x;
    private int y;

    /* access modifiers changed from: private */
    public enum a {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CANCELLED,
        CLEARED,
        PAUSED
    }

    public static <R> f<R> a(e eVar, Object obj, Class<R> cls, d dVar, int i2, int i3, com.a.a.g gVar, h<R> hVar, c<R> cVar, b bVar, i iVar, c<? super R> cVar2) {
        f<R> fVar = (f<R>) f955a.acquire();
        if (fVar == null) {
            fVar = new f<>();
        }
        fVar.b(eVar, obj, cls, dVar, i2, i3, gVar, hVar, cVar, bVar, iVar, cVar2);
        return fVar;
    }

    f() {
    }

    private void b(e eVar, Object obj, Class<R> cls, d dVar, int i2, int i3, com.a.a.g gVar, h<R> hVar, c<R> cVar, b bVar, i iVar, c<? super R> cVar2) {
        this.f = eVar;
        this.g = obj;
        this.h = cls;
        this.i = dVar;
        this.j = i2;
        this.k = i3;
        this.l = gVar;
        this.m = hVar;
        this.n = cVar;
        this.e = bVar;
        this.o = iVar;
        this.p = cVar2;
        this.t = a.PENDING;
    }

    @Override // com.a.a.j.a.a.c
    public b a_() {
        return this.d;
    }

    @Override // com.a.a.h.a
    public void i() {
        k();
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = -1;
        this.k = -1;
        this.m = null;
        this.n = null;
        this.e = null;
        this.p = null;
        this.r = null;
        this.u = null;
        this.v = null;
        this.w = null;
        this.x = -1;
        this.y = -1;
        f955a.release(this);
    }

    @Override // com.a.a.h.a
    public void a() {
        k();
        this.d.b();
        this.s = d.a();
        if (this.g == null) {
            if (com.a.a.j.i.a(this.j, this.k)) {
                this.x = this.j;
                this.y = this.k;
            }
            a(new o("Received null model"), n() == null ? 5 : 3);
        } else if (this.t == a.RUNNING) {
            throw new IllegalArgumentException("Cannot restart a running request");
        } else if (this.t == a.COMPLETE) {
            a((s<?>) this.q, com.a.a.d.a.MEMORY_CACHE);
        } else {
            this.t = a.WAITING_FOR_SIZE;
            if (com.a.a.j.i.a(this.j, this.k)) {
                a(this.j, this.k);
            } else {
                this.m.a((g) this);
            }
            if ((this.t == a.RUNNING || this.t == a.WAITING_FOR_SIZE) && q()) {
                this.m.b(m());
            }
            if (Log.isLoggable("Request", 2)) {
                a("finished run method in " + d.a(this.s));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        k();
        this.d.b();
        this.m.b(this);
        this.t = a.CANCELLED;
        if (this.r != null) {
            this.r.a();
            this.r = null;
        }
    }

    private void k() {
        if (this.b) {
            throw new IllegalStateException("You can't start or clear loads in RequestListener or Target callbacks. If you must do so, consider posting your into() or clear() calls to the main thread using a Handler instead.");
        }
    }

    @Override // com.a.a.h.a
    public void c() {
        com.a.a.j.i.a();
        k();
        if (this.t != a.CLEARED) {
            j();
            if (this.q != null) {
                a((s<?>) this.q);
            }
            if (q()) {
                this.m.a(m());
            }
            this.t = a.CLEARED;
        }
    }

    @Override // com.a.a.h.a
    public void b() {
        c();
        this.t = a.PAUSED;
    }

    private void a(s<?> sVar) {
        this.o.a(sVar);
        this.q = null;
    }

    @Override // com.a.a.h.a
    public boolean e() {
        return this.t == a.RUNNING || this.t == a.WAITING_FOR_SIZE;
    }

    @Override // com.a.a.h.a
    public boolean f() {
        return this.t == a.COMPLETE;
    }

    @Override // com.a.a.h.a
    public boolean g() {
        return f();
    }

    @Override // com.a.a.h.a
    public boolean h() {
        return this.t == a.CANCELLED || this.t == a.CLEARED;
    }

    private Drawable l() {
        if (this.u == null) {
            this.u = this.i.n();
            if (this.u == null && this.i.o() > 0) {
                this.u = a(this.i.o());
            }
        }
        return this.u;
    }

    private Drawable m() {
        if (this.v == null) {
            this.v = this.i.q();
            if (this.v == null && this.i.p() > 0) {
                this.v = a(this.i.p());
            }
        }
        return this.v;
    }

    private Drawable n() {
        if (this.w == null) {
            this.w = this.i.s();
            if (this.w == null && this.i.r() > 0) {
                this.w = a(this.i.r());
            }
        }
        return this.w;
    }

    private Drawable a(@DrawableRes int i2) {
        if (z) {
            return b(i2);
        }
        return c(i2);
    }

    private Drawable b(@DrawableRes int i2) {
        try {
            return android.support.v7.a.a.a.b(this.f, i2);
        } catch (NoClassDefFoundError e2) {
            z = false;
            return c(i2);
        }
    }

    private Drawable c(@DrawableRes int i2) {
        return ResourcesCompat.getDrawable(this.f.getResources(), i2, this.i.t());
    }

    private void o() {
        if (q()) {
            Drawable drawable = null;
            if (this.g == null) {
                drawable = n();
            }
            if (drawable == null) {
                drawable = l();
            }
            if (drawable == null) {
                drawable = m();
            }
            this.m.c(drawable);
        }
    }

    @Override // com.a.a.h.a.g
    public void a(int i2, int i3) {
        this.d.b();
        if (Log.isLoggable("Request", 2)) {
            a("Got onSizeReady in " + d.a(this.s));
        }
        if (this.t == a.WAITING_FOR_SIZE) {
            this.t = a.RUNNING;
            float B = this.i.B();
            this.x = a(i2, B);
            this.y = a(i3, B);
            if (Log.isLoggable("Request", 2)) {
                a("finished setup for calling load in " + d.a(this.s));
            }
            this.r = this.o.a(this.f, this.g, this.i.v(), this.x, this.y, this.i.l(), this.h, this.l, this.i.m(), this.i.i(), this.i.j(), this.i.C(), this.i.k(), this.i.u(), this.i.D(), this.i.E(), this);
            if (Log.isLoggable("Request", 2)) {
                a("finished onSizeReady in " + d.a(this.s));
            }
        }
    }

    private static int a(int i2, float f2) {
        return i2 == Integer.MIN_VALUE ? i2 : Math.round(((float) i2) * f2);
    }

    private boolean p() {
        return this.e == null || this.e.b(this);
    }

    private boolean q() {
        return this.e == null || this.e.c(this);
    }

    private boolean r() {
        return this.e == null || !this.e.d();
    }

    private void s() {
        if (this.e != null) {
            this.e.d(this);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.a.a.h.f<R> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.a.a.h.e
    public void a(s<?> sVar, com.a.a.d.a aVar) {
        this.d.b();
        this.r = null;
        if (sVar == null) {
            a(new o("Expected to receive a Resource<R> with an object of " + this.h + " inside, but instead got null."));
            return;
        }
        Object c2 = sVar.c();
        if (c2 == null || !this.h.isAssignableFrom(c2.getClass())) {
            a(sVar);
            a(new o("Expected to receive an object of " + this.h + " but instead got " + (c2 != null ? c2.getClass() : "") + "{" + c2 + "} inside Resource{" + sVar + "}." + (c2 != null ? "" : " To indicate failure return a null Resource object, rather than a Resource object containing null data.")));
        } else if (!p()) {
            a(sVar);
            this.t = a.COMPLETE;
        } else {
            a(sVar, c2, aVar);
        }
    }

    /* JADX INFO: finally extract failed */
    private void a(s<R> sVar, R r2, com.a.a.d.a aVar) {
        boolean r3 = r();
        this.t = a.COMPLETE;
        this.q = sVar;
        if (this.f.d() <= 3) {
            Log.d("Glide", "Finished loading " + r2.getClass().getSimpleName() + " from " + aVar + " for " + this.g + " with size [" + this.x + "x" + this.y + "] in " + d.a(this.s) + " ms");
        }
        this.b = true;
        try {
            if (this.n == null || !this.n.a(r2, this.g, this.m, aVar, r3)) {
                this.m.a(r2, this.p.a(aVar, r3));
            }
            this.b = false;
            s();
        } catch (Throwable th) {
            this.b = false;
            throw th;
        }
    }

    @Override // com.a.a.h.e
    public void a(o oVar) {
        a(oVar, 5);
    }

    private void a(o oVar, int i2) {
        this.d.b();
        int d2 = this.f.d();
        if (d2 <= i2) {
            Log.w("Glide", "Load failed for " + this.g + " with size [" + this.x + "x" + this.y + "]", oVar);
            if (d2 <= 4) {
                oVar.a("Glide");
            }
        }
        this.r = null;
        this.t = a.FAILED;
        this.b = true;
        try {
            if (this.n == null || !this.n.a(oVar, this.g, this.m, r())) {
                o();
            }
        } finally {
            this.b = false;
        }
    }

    @Override // com.a.a.h.a
    public boolean a(a aVar) {
        if (!(aVar instanceof f)) {
            return false;
        }
        f fVar = (f) aVar;
        if (this.j != fVar.j || this.k != fVar.k || !com.a.a.j.i.b(this.g, fVar.g) || !this.h.equals(fVar.h) || !this.i.equals(fVar.i) || this.l != fVar.l) {
            return false;
        }
        return true;
    }

    private void a(String str) {
        Log.v("Request", str + " this: " + this.c);
    }
}
