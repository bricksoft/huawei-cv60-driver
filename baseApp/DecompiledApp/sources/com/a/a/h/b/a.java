package com.a.a.h.b;

import com.a.a.h.b.b;

public class a<R> implements b<R> {

    /* renamed from: a  reason: collision with root package name */
    static final a<?> f953a = new a<>();
    private static final c<?> b = new C0039a();

    /* renamed from: com.a.a.h.b.a$a  reason: collision with other inner class name */
    public static class C0039a<R> implements c<R> {
        @Override // com.a.a.h.b.c
        public b<R> a(com.a.a.d.a aVar, boolean z) {
            return a.f953a;
        }
    }

    public static <R> c<R> a() {
        return (c<R>) b;
    }

    @Override // com.a.a.h.b.b
    public boolean a(Object obj, b.a aVar) {
        return false;
    }
}
