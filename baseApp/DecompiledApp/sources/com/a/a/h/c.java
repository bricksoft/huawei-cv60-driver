package com.a.a.h;

import android.support.annotation.Nullable;
import com.a.a.d.a;
import com.a.a.d.b.o;
import com.a.a.h.a.h;

public interface c<R> {
    boolean a(@Nullable o oVar, Object obj, h<R> hVar, boolean z);

    boolean a(R r, Object obj, h<R> hVar, a aVar, boolean z);
}
