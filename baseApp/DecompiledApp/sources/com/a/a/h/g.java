package com.a.a.h;

import android.support.annotation.Nullable;

public class g implements a, b {

    /* renamed from: a  reason: collision with root package name */
    private a f957a;
    private a b;
    @Nullable
    private b c;
    private boolean d;

    public g() {
        this(null);
    }

    public g(b bVar) {
        this.c = bVar;
    }

    public void a(a aVar, a aVar2) {
        this.f957a = aVar;
        this.b = aVar2;
    }

    @Override // com.a.a.h.b
    public boolean b(a aVar) {
        return j() && (aVar.equals(this.f957a) || !this.f957a.g());
    }

    private boolean j() {
        return this.c == null || this.c.b(this);
    }

    @Override // com.a.a.h.b
    public boolean c(a aVar) {
        return k() && aVar.equals(this.f957a) && !d();
    }

    private boolean k() {
        return this.c == null || this.c.c(this);
    }

    @Override // com.a.a.h.b
    public boolean d() {
        return l() || g();
    }

    @Override // com.a.a.h.b
    public void d(a aVar) {
        if (!aVar.equals(this.b)) {
            if (this.c != null) {
                this.c.d(this);
            }
            if (!this.b.f()) {
                this.b.c();
            }
        }
    }

    private boolean l() {
        return this.c != null && this.c.d();
    }

    @Override // com.a.a.h.a
    public void a() {
        this.d = true;
        if (!this.b.e()) {
            this.b.a();
        }
        if (this.d && !this.f957a.e()) {
            this.f957a.a();
        }
    }

    @Override // com.a.a.h.a
    public void b() {
        this.d = false;
        this.f957a.b();
        this.b.b();
    }

    @Override // com.a.a.h.a
    public void c() {
        this.d = false;
        this.b.c();
        this.f957a.c();
    }

    @Override // com.a.a.h.a
    public boolean e() {
        return this.f957a.e();
    }

    @Override // com.a.a.h.a
    public boolean f() {
        return this.f957a.f() || this.b.f();
    }

    @Override // com.a.a.h.a
    public boolean g() {
        return this.f957a.g() || this.b.g();
    }

    @Override // com.a.a.h.a
    public boolean h() {
        return this.f957a.h();
    }

    @Override // com.a.a.h.a
    public void i() {
        this.f957a.i();
        this.b.i();
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0017 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @Override // com.a.a.h.a
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.a.a.h.a r4) {
        /*
            r3 = this;
            r0 = 0
            boolean r1 = r4 instanceof com.a.a.h.g
            if (r1 == 0) goto L_0x0018
            com.a.a.h.g r4 = (com.a.a.h.g) r4
            com.a.a.h.a r1 = r3.f957a
            if (r1 != 0) goto L_0x0019
            com.a.a.h.a r1 = r4.f957a
            if (r1 != 0) goto L_0x0018
        L_0x000f:
            com.a.a.h.a r1 = r3.b
            if (r1 != 0) goto L_0x0024
            com.a.a.h.a r1 = r4.b
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            return r0
        L_0x0019:
            com.a.a.h.a r1 = r3.f957a
            com.a.a.h.a r2 = r4.f957a
            boolean r1 = r1.a(r2)
            if (r1 == 0) goto L_0x0018
            goto L_0x000f
        L_0x0024:
            com.a.a.h.a r1 = r3.b
            com.a.a.h.a r2 = r4.b
            boolean r1 = r1.a(r2)
            if (r1 == 0) goto L_0x0018
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.h.g.a(com.a.a.h.a):boolean");
    }
}
