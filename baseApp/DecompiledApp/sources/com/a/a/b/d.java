package com.a.a.b;

import android.support.v4.view.ViewCompat;
import android.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f746a = new byte[256];
    private ByteBuffer b;
    private c c;
    private int d = 0;

    public d a(ByteBuffer byteBuffer) {
        c();
        this.b = byteBuffer.asReadOnlyBuffer();
        this.b.position(0);
        this.b.order(ByteOrder.LITTLE_ENDIAN);
        return this;
    }

    public void a() {
        this.b = null;
        this.c = null;
    }

    private void c() {
        this.b = null;
        Arrays.fill(this.f746a, (byte) 0);
        this.c = new c();
        this.d = 0;
    }

    public c b() {
        if (this.b == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        } else if (o()) {
            return this.c;
        } else {
            h();
            if (!o()) {
                d();
                if (this.c.c < 0) {
                    this.c.b = 1;
                }
            }
            return this.c;
        }
    }

    private void d() {
        a(Integer.MAX_VALUE);
    }

    private void a(int i) {
        boolean z = false;
        while (!z && !o() && this.c.c <= i) {
            switch (m()) {
                case 33:
                    switch (m()) {
                        case 1:
                            k();
                            continue;
                        case 249:
                            this.c.d = new b();
                            e();
                            continue;
                        case 254:
                            k();
                            continue;
                        case 255:
                            l();
                            String str = "";
                            for (int i2 = 0; i2 < 11; i2++) {
                                str = str + ((char) this.f746a[i2]);
                            }
                            if (!str.equals("NETSCAPE2.0")) {
                                k();
                                break;
                            } else {
                                g();
                                continue;
                            }
                        default:
                            k();
                            continue;
                    }
                case 44:
                    if (this.c.d == null) {
                        this.c.d = new b();
                    }
                    f();
                    break;
                case 59:
                    z = true;
                    break;
                default:
                    this.c.b = 1;
                    break;
            }
        }
    }

    private void e() {
        boolean z = true;
        m();
        int m = m();
        this.c.d.g = (m & 28) >> 2;
        if (this.c.d.g == 0) {
            this.c.d.g = 1;
        }
        b bVar = this.c.d;
        if ((m & 1) == 0) {
            z = false;
        }
        bVar.f = z;
        int n = n();
        if (n < 2) {
            n = 10;
        }
        this.c.d.i = n * 10;
        this.c.d.h = m();
        m();
    }

    private void f() {
        boolean z = true;
        this.c.d.f744a = n();
        this.c.d.b = n();
        this.c.d.c = n();
        this.c.d.d = n();
        int m = m();
        boolean z2 = (m & 128) != 0;
        int pow = (int) Math.pow(2.0d, (double) ((m & 7) + 1));
        b bVar = this.c.d;
        if ((m & 64) == 0) {
            z = false;
        }
        bVar.e = z;
        if (z2) {
            this.c.d.k = b(pow);
        } else {
            this.c.d.k = null;
        }
        this.c.d.j = this.b.position();
        j();
        if (!o()) {
            this.c.c++;
            this.c.e.add(this.c.d);
        }
    }

    private void g() {
        do {
            l();
            if (this.f746a[0] == 1) {
                this.c.m = (this.f746a[1] & 255) | ((this.f746a[2] & 255) << 8);
            }
            if (this.d <= 0) {
                return;
            }
        } while (!o());
    }

    private void h() {
        String str = "";
        for (int i = 0; i < 6; i++) {
            str = str + ((char) m());
        }
        if (!str.startsWith("GIF")) {
            this.c.b = 1;
            return;
        }
        i();
        if (this.c.h && !o()) {
            this.c.f745a = b(this.c.i);
            this.c.l = this.c.f745a[this.c.j];
        }
    }

    private void i() {
        this.c.f = n();
        this.c.g = n();
        int m = m();
        this.c.h = (m & 128) != 0;
        this.c.i = (int) Math.pow(2.0d, (double) ((m & 7) + 1));
        this.c.j = m();
        this.c.k = m();
    }

    private int[] b(int i) {
        int[] iArr = null;
        byte[] bArr = new byte[(i * 3)];
        try {
            this.b.get(bArr);
            iArr = new int[256];
            int i2 = 0;
            int i3 = 0;
            while (i3 < i) {
                int i4 = i2 + 1;
                int i5 = bArr[i2] & 255;
                int i6 = i4 + 1;
                int i7 = bArr[i4] & 255;
                i2 = i6 + 1;
                int i8 = i3 + 1;
                iArr[i3] = (i5 << 16) | ViewCompat.MEASURED_STATE_MASK | (i7 << 8) | (bArr[i6] & 255);
                i3 = i8;
            }
        } catch (BufferUnderflowException e) {
            if (Log.isLoggable("GifHeaderParser", 3)) {
                Log.d("GifHeaderParser", "Format Error Reading Color Table", e);
            }
            this.c.b = 1;
        }
        return iArr;
    }

    private void j() {
        m();
        k();
    }

    private void k() {
        int m;
        do {
            m = m();
            this.b.position(Math.min(this.b.position() + m, this.b.limit()));
        } while (m > 0);
    }

    private int l() {
        this.d = m();
        if (this.d <= 0) {
            return 0;
        }
        int i = 0;
        int i2 = 0;
        while (i2 < this.d) {
            try {
                i = this.d - i2;
                this.b.get(this.f746a, i2, i);
                i2 += i;
            } catch (Exception e) {
                if (Log.isLoggable("GifHeaderParser", 3)) {
                    Log.d("GifHeaderParser", "Error Reading Block n: " + i2 + " count: " + i + " blockSize: " + this.d, e);
                }
                this.c.b = 1;
                return i2;
            }
        }
        return i2;
    }

    private int m() {
        try {
            return this.b.get() & 255;
        } catch (Exception e) {
            this.c.b = 1;
            return 0;
        }
    }

    private int n() {
        return this.b.getShort();
    }

    private boolean o() {
        return this.c.b != 0;
    }
}
