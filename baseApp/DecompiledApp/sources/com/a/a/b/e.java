package com.a.a.b;

import android.graphics.Bitmap;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.util.Log;
import com.a.a.b.a;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;

public class e implements a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f747a = e.class.getSimpleName();
    @ColorInt
    private int[] b;
    @ColorInt
    private final int[] c;
    private ByteBuffer d;
    private byte[] e;
    @Nullable
    private byte[] f;
    private int g;
    private int h;
    private short[] i;
    private byte[] j;
    private byte[] k;
    private byte[] l;
    @ColorInt
    private int[] m;
    private int n;
    private c o;
    private a.AbstractC0027a p;
    private Bitmap q;
    private boolean r;
    private int s;
    private int t;
    private int u;
    private int v;
    private boolean w;

    public e(a.AbstractC0027a aVar, c cVar, ByteBuffer byteBuffer, int i2) {
        this(aVar);
        a(cVar, byteBuffer, i2);
    }

    public e(a.AbstractC0027a aVar) {
        this.c = new int[256];
        this.g = 0;
        this.h = 0;
        this.p = aVar;
        this.o = new c();
    }

    @Override // com.a.a.b.a
    public ByteBuffer a() {
        return this.d;
    }

    @Override // com.a.a.b.a
    public void b() {
        this.n = (this.n + 1) % this.o.c;
    }

    public int a(int i2) {
        if (i2 < 0 || i2 >= this.o.c) {
            return -1;
        }
        return this.o.e.get(i2).i;
    }

    @Override // com.a.a.b.a
    public int c() {
        if (this.o.c <= 0 || this.n < 0) {
            return 0;
        }
        return a(this.n);
    }

    @Override // com.a.a.b.a
    public int d() {
        return this.o.c;
    }

    @Override // com.a.a.b.a
    public int e() {
        return this.n;
    }

    @Override // com.a.a.b.a
    public void f() {
        this.n = -1;
    }

    @Override // com.a.a.b.a
    public int g() {
        return this.d.limit() + this.l.length + (this.m.length * 4);
    }

    @Override // com.a.a.b.a
    public synchronized Bitmap h() {
        Bitmap bitmap;
        b bVar;
        int[] iArr;
        if (this.o.c <= 0 || this.n < 0) {
            if (Log.isLoggable(f747a, 3)) {
                Log.d(f747a, "Unable to decode frame, frameCount=" + this.o.c + ", framePointer=" + this.n);
            }
            this.s = 1;
        }
        if (this.s == 1 || this.s == 2) {
            if (Log.isLoggable(f747a, 3)) {
                Log.d(f747a, "Unable to decode frame, status=" + this.s);
            }
            bitmap = null;
        } else {
            this.s = 0;
            b bVar2 = this.o.e.get(this.n);
            int i2 = this.n - 1;
            if (i2 >= 0) {
                bVar = this.o.e.get(i2);
            } else {
                bVar = null;
            }
            if (bVar2.k != null) {
                iArr = bVar2.k;
            } else {
                iArr = this.o.f745a;
            }
            this.b = iArr;
            if (this.b == null) {
                if (Log.isLoggable(f747a, 3)) {
                    Log.d(f747a, "No valid color table found for frame #" + this.n);
                }
                this.s = 1;
                bitmap = null;
            } else {
                if (bVar2.f) {
                    System.arraycopy(this.b, 0, this.c, 0, this.b.length);
                    this.b = this.c;
                    this.b[bVar2.h] = 0;
                }
                bitmap = a(bVar2, bVar);
            }
        }
        return bitmap;
    }

    @Override // com.a.a.b.a
    public void i() {
        this.o = null;
        if (this.l != null) {
            this.p.a(this.l);
        }
        if (this.m != null) {
            this.p.a(this.m);
        }
        if (this.q != null) {
            this.p.a(this.q);
        }
        this.q = null;
        this.d = null;
        this.w = false;
        if (this.e != null) {
            this.p.a(this.e);
        }
        if (this.f != null) {
            this.p.a(this.f);
        }
    }

    public synchronized void a(c cVar, ByteBuffer byteBuffer, int i2) {
        if (i2 <= 0) {
            throw new IllegalArgumentException("Sample size must be >=0, not: " + i2);
        }
        int highestOneBit = Integer.highestOneBit(i2);
        this.s = 0;
        this.o = cVar;
        this.w = false;
        this.n = -1;
        this.d = byteBuffer.asReadOnlyBuffer();
        this.d.position(0);
        this.d.order(ByteOrder.LITTLE_ENDIAN);
        this.r = false;
        Iterator<b> it = cVar.e.iterator();
        while (true) {
            if (it.hasNext()) {
                if (it.next().g == 3) {
                    this.r = true;
                    break;
                }
            } else {
                break;
            }
        }
        this.t = highestOneBit;
        this.v = cVar.f / highestOneBit;
        this.u = cVar.g / highestOneBit;
        this.l = this.p.a(cVar.f * cVar.g);
        this.m = this.p.b(this.v * this.u);
    }

    private Bitmap a(b bVar, b bVar2) {
        int i2;
        int i3;
        int a2;
        int[] iArr = this.m;
        if (bVar2 == null) {
            Arrays.fill(iArr, 0);
        }
        if (bVar2 != null && bVar2.g > 0) {
            if (bVar2.g == 2) {
                int i4 = 0;
                if (!bVar.f) {
                    i4 = this.o.l;
                    if (bVar.k != null && this.o.j == bVar.h) {
                        i4 = 0;
                    }
                } else if (this.n == 0) {
                    this.w = true;
                }
                int i5 = bVar2.d / this.t;
                int i6 = bVar2.b / this.t;
                int i7 = bVar2.c / this.t;
                int i8 = (i6 * this.v) + (bVar2.f744a / this.t);
                int i9 = i8 + (i5 * this.v);
                while (i8 < i9) {
                    int i10 = i8 + i7;
                    for (int i11 = i8; i11 < i10; i11++) {
                        iArr[i11] = i4;
                    }
                    i8 += this.v;
                }
            } else if (bVar2.g == 3 && this.q != null) {
                this.q.getPixels(iArr, 0, this.v, 0, 0, this.v, this.u);
            }
        }
        a(bVar);
        int i12 = bVar.d / this.t;
        int i13 = bVar.b / this.t;
        int i14 = bVar.c / this.t;
        int i15 = bVar.f744a / this.t;
        int i16 = 1;
        int i17 = 8;
        int i18 = 0;
        boolean z = this.n == 0;
        int i19 = 0;
        while (i19 < i12) {
            if (bVar.e) {
                if (i18 >= i12) {
                    i16++;
                    switch (i16) {
                        case 2:
                            i18 = 4;
                            break;
                        case 3:
                            i18 = 2;
                            i17 = 4;
                            break;
                        case 4:
                            i18 = 1;
                            i17 = 2;
                            break;
                    }
                }
                i3 = i18 + i17;
                i2 = i18;
            } else {
                i2 = i19;
                i3 = i18;
            }
            int i20 = i2 + i13;
            if (i20 < this.u) {
                int i21 = this.v * i20;
                int i22 = i21 + i15;
                int i23 = i22 + i14;
                if (this.v + i21 < i23) {
                    i23 = this.v + i21;
                }
                int i24 = this.t * i19 * bVar.c;
                int i25 = i24 + ((i23 - i22) * this.t);
                int i26 = i24;
                while (i22 < i23) {
                    if (this.t == 1) {
                        a2 = this.b[this.l[i26] & 255];
                    } else {
                        a2 = a(i26, i25, bVar.c);
                    }
                    if (a2 != 0) {
                        iArr[i22] = a2;
                    } else if (!this.w && z) {
                        this.w = true;
                    }
                    i22++;
                    i26 = this.t + i26;
                }
            }
            i19++;
            i18 = i3;
        }
        if (this.r && (bVar.g == 0 || bVar.g == 1)) {
            if (this.q == null) {
                this.q = m();
            }
            this.q.setPixels(iArr, 0, this.v, 0, 0, this.v, this.u);
        }
        Bitmap m2 = m();
        m2.setPixels(iArr, 0, this.v, 0, 0, this.v, this.u);
        return m2;
    }

    @ColorInt
    private int a(int i2, int i3, int i4) {
        int i5 = i2;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        while (i5 < this.t + i2 && i5 < this.l.length && i5 < i3) {
            int i11 = this.b[this.l[i5] & 255];
            if (i11 != 0) {
                i10 += (i11 >> 24) & 255;
                i9 += (i11 >> 16) & 255;
                i8 += (i11 >> 8) & 255;
                i7 += i11 & 255;
                i6++;
            }
            i5++;
        }
        int i12 = i2 + i4;
        while (i12 < i2 + i4 + this.t && i12 < this.l.length && i12 < i3) {
            int i13 = this.b[this.l[i12] & 255];
            if (i13 != 0) {
                i10 += (i13 >> 24) & 255;
                i9 += (i13 >> 16) & 255;
                i8 += (i13 >> 8) & 255;
                i7 += i13 & 255;
                i6++;
            }
            i12++;
        }
        if (i6 == 0) {
            return 0;
        }
        return ((i10 / i6) << 24) | ((i9 / i6) << 16) | ((i8 / i6) << 8) | (i7 / i6);
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x016f A[LOOP:5: B:59:0x016d->B:60:0x016f, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.a.a.b.b r22) {
        /*
        // Method dump skipped, instructions count: 408
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.b.e.a(com.a.a.b.b):void");
    }

    private void j() {
        if (this.g <= this.h) {
            if (this.f == null) {
                this.f = this.p.a(16384);
            }
            this.h = 0;
            this.g = Math.min(this.d.remaining(), 16384);
            this.d.get(this.f, 0, this.g);
        }
    }

    private int k() {
        try {
            j();
            byte[] bArr = this.f;
            int i2 = this.h;
            this.h = i2 + 1;
            return bArr[i2] & 255;
        } catch (Exception e2) {
            this.s = 1;
            return 0;
        }
    }

    private int l() {
        int k2 = k();
        if (k2 > 0) {
            try {
                if (this.e == null) {
                    this.e = this.p.a(255);
                }
                int i2 = this.g - this.h;
                if (i2 >= k2) {
                    System.arraycopy(this.f, this.h, this.e, 0, k2);
                    this.h += k2;
                } else if (this.d.remaining() + i2 >= k2) {
                    System.arraycopy(this.f, this.h, this.e, 0, i2);
                    this.h = this.g;
                    j();
                    int i3 = k2 - i2;
                    System.arraycopy(this.f, 0, this.e, i2, i3);
                    this.h += i3;
                } else {
                    this.s = 1;
                }
            } catch (Exception e2) {
                Log.w(f747a, "Error Reading Block", e2);
                this.s = 1;
            }
        }
        return k2;
    }

    private Bitmap m() {
        Bitmap a2 = this.p.a(this.v, this.u, this.w ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        a2.setHasAlpha(true);
        return a2;
    }
}
