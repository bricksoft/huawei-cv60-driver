package com.a.a.b;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import java.nio.ByteBuffer;

public interface a {

    /* renamed from: com.a.a.b.a$a  reason: collision with other inner class name */
    public interface AbstractC0027a {
        @NonNull
        Bitmap a(int i, int i2, Bitmap.Config config);

        void a(Bitmap bitmap);

        void a(byte[] bArr);

        void a(int[] iArr);

        byte[] a(int i);

        int[] b(int i);
    }

    ByteBuffer a();

    void b();

    int c();

    int d();

    int e();

    void f();

    int g();

    Bitmap h();

    void i();
}
