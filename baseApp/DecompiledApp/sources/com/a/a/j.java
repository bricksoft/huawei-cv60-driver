package com.a.a;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.a.a.d.b.h;
import com.a.a.d.d.e.c;
import com.a.a.e.c;
import com.a.a.e.i;
import com.a.a.e.m;
import com.a.a.e.n;
import com.a.a.e.p;
import com.a.a.h.d;

public class j implements i {
    private static final d c = d.a(Bitmap.class).g();
    private static final d d = d.a(c.class).g();
    private static final d e = d.a(h.c).a(g.LOW).a(true);

    /* renamed from: a  reason: collision with root package name */
    protected final c f960a;
    final com.a.a.e.h b;
    private final n f;
    private final m g;
    private final p h;
    private final Runnable i;
    private final Handler j;
    private final com.a.a.e.c k;
    @NonNull
    private d l;

    public j(c cVar, com.a.a.e.h hVar, m mVar) {
        this(cVar, hVar, mVar, new n(), cVar.d());
    }

    j(c cVar, com.a.a.e.h hVar, m mVar, n nVar, com.a.a.e.d dVar) {
        this.h = new p();
        this.i = new Runnable() {
            /* class com.a.a.j.AnonymousClass1 */

            public void run() {
                j.this.b.a(j.this);
            }
        };
        this.j = new Handler(Looper.getMainLooper());
        this.f960a = cVar;
        this.b = hVar;
        this.g = mVar;
        this.f = nVar;
        this.k = dVar.a(cVar.e().getBaseContext(), new b(nVar));
        if (com.a.a.j.i.c()) {
            this.j.post(this.i);
        } else {
            hVar.a(this);
        }
        hVar.a(this.k);
        a(cVar.e().a());
        cVar.a(this);
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull d dVar) {
        this.l = dVar.clone().h();
    }

    public void a() {
        com.a.a.j.i.a();
        this.f.a();
    }

    public void b() {
        com.a.a.j.i.a();
        this.f.b();
    }

    @Override // com.a.a.e.i
    public void c() {
        b();
        this.h.c();
    }

    @Override // com.a.a.e.i
    public void d() {
        a();
        this.h.d();
    }

    @Override // com.a.a.e.i
    public void e() {
        this.h.e();
        for (com.a.a.h.a.h<?> hVar : this.h.a()) {
            a(hVar);
        }
        this.h.b();
        this.f.c();
        this.b.b(this);
        this.b.b(this.k);
        this.j.removeCallbacks(this.i);
        this.f960a.b(this);
    }

    public i<Bitmap> f() {
        return a(Bitmap.class).a(c);
    }

    public i<Drawable> g() {
        return a(Drawable.class);
    }

    public i<Drawable> a(@Nullable Object obj) {
        return g().a(obj);
    }

    public <ResourceType> i<ResourceType> a(Class<ResourceType> cls) {
        return new i<>(this.f960a, this, cls);
    }

    public void a(View view) {
        a((com.a.a.h.a.h<?>) new a(view));
    }

    public void a(@Nullable final com.a.a.h.a.h<?> hVar) {
        if (hVar != null) {
            if (com.a.a.j.i.b()) {
                c(hVar);
            } else {
                this.j.post(new Runnable() {
                    /* class com.a.a.j.AnonymousClass2 */

                    public void run() {
                        j.this.a(hVar);
                    }
                });
            }
        }
    }

    private void c(com.a.a.h.a.h<?> hVar) {
        if (!b(hVar)) {
            this.f960a.a(hVar);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(com.a.a.h.a.h<?> hVar) {
        com.a.a.h.a b2 = hVar.b();
        if (b2 == null) {
            return true;
        }
        if (!this.f.b(b2)) {
            return false;
        }
        this.h.b(hVar);
        hVar.a((com.a.a.h.a) null);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(com.a.a.h.a.h<?> hVar, com.a.a.h.a aVar) {
        this.h.a(hVar);
        this.f.a(aVar);
    }

    /* access modifiers changed from: package-private */
    public d h() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public <T> k<?, T> b(Class<T> cls) {
        return this.f960a.e().a(cls);
    }

    public String toString() {
        return super.toString() + "{tracker=" + this.f + ", treeNode=" + this.g + "}";
    }

    private static class b implements c.a {

        /* renamed from: a  reason: collision with root package name */
        private final n f969a;

        public b(n nVar) {
            this.f969a = nVar;
        }

        @Override // com.a.a.e.c.a
        public void a(boolean z) {
            if (z) {
                this.f969a.d();
            }
        }
    }

    private static class a extends com.a.a.h.a.i<View, Object> {
        public a(View view) {
            super(view);
        }

        @Override // com.a.a.h.a.h
        public void a(Object obj, com.a.a.h.b.b<? super Object> bVar) {
        }
    }
}
