package com.a.a.c;

/* access modifiers changed from: package-private */
public class c {

    /* renamed from: a  reason: collision with root package name */
    protected static int f751a = 6;
    protected static final int b = (1 << f751a);
    protected static final int c = (b * 32);
    protected int d;
    protected byte[] e;
    protected int f;
    protected int g;
    protected int[][] h;
    protected int[] i = new int[256];
    protected int[] j = new int[256];
    protected int[] k = new int[256];
    protected int[] l = new int[32];
    private int m = 0;
    private d[] n = null;
    private int o = 0;
    private int p = 0;

    public c(byte[] bArr, int i2, int i3, int i4, int i5, int[] iArr, d[] dVarArr) {
        this.e = bArr;
        this.f = i2;
        this.g = i3;
        this.n = dVarArr;
        this.o = i4;
        this.p = i5;
        this.h = new int[256][];
        for (int i6 = 0; i6 < 256; i6++) {
            this.h[i6] = new int[4];
            int[] iArr2 = this.h[i6];
            int i7 = (i6 << 12) / 256;
            iArr2[2] = i7;
            iArr2[1] = i7;
            iArr2[0] = i7;
            this.k[i6] = 256;
            this.j[i6] = 0;
        }
        if (iArr != null) {
            this.m = iArr.length / 3;
            for (int i8 = 0; i8 < this.m; i8++) {
                this.h[255 - i8][0] = (iArr[i8 * 3] & 255) << 4;
                this.h[255 - i8][1] = (iArr[(i8 * 3) + 1] & 255) << 4;
                this.h[255 - i8][2] = (iArr[(i8 * 3) + 2] & 255) << 4;
            }
        }
    }

    public byte[] a() {
        byte[] bArr = new byte[768];
        int[] iArr = new int[256];
        for (int i2 = 0; i2 < 256; i2++) {
            iArr[this.h[i2][3]] = i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < 256; i4++) {
            int i5 = iArr[i4];
            int i6 = i3 + 1;
            bArr[i3] = (byte) this.h[i5][0];
            int i7 = i6 + 1;
            bArr[i6] = (byte) this.h[i5][1];
            i3 = i7 + 1;
            bArr[i7] = (byte) this.h[i5][2];
        }
        return bArr;
    }

    public void b() {
        int i2;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i5 < 256) {
            int[] iArr = this.h[i5];
            int i6 = iArr[1];
            int i7 = i5;
            for (int i8 = i5 + 1; i8 < 256; i8++) {
                int[] iArr2 = this.h[i8];
                if (iArr2[1] < i6) {
                    i6 = iArr2[1];
                    i7 = i8;
                }
            }
            int[] iArr3 = this.h[i7];
            if (i5 != i7) {
                int i9 = iArr3[0];
                iArr3[0] = iArr[0];
                iArr[0] = i9;
                int i10 = iArr3[1];
                iArr3[1] = iArr[1];
                iArr[1] = i10;
                int i11 = iArr3[2];
                iArr3[2] = iArr[2];
                iArr[2] = i11;
                int i12 = iArr3[3];
                iArr3[3] = iArr[3];
                iArr[3] = i12;
            }
            if (i6 != i4) {
                this.i[i4] = (i3 + i5) >> 1;
                for (int i13 = i4 + 1; i13 < i6; i13++) {
                    this.i[i13] = i5;
                }
                i2 = i5;
            } else {
                i2 = i3;
                i6 = i4;
            }
            i5++;
            i3 = i2;
            i4 = i6;
        }
        this.i[i4] = (i3 + 255) >> 1;
        for (int i14 = i4 + 1; i14 < 256; i14++) {
            this.i[i14] = 255;
        }
    }

    public void c() {
        int i2;
        int[] iArr;
        int i3;
        if (this.f < 1509) {
            this.g = 1;
        }
        this.d = ((this.g - 1) / 3) + 30;
        byte[] bArr = this.e;
        int i4 = this.f;
        int i5 = this.f / (this.g * 3);
        int i6 = i5 / 100;
        int i7 = c;
        int i8 = i7 >> f751a;
        if (i8 <= 1) {
            i8 = 0;
        }
        for (int i9 = 0; i9 < i8; i9++) {
            this.l[i9] = ((((i8 * i8) - (i9 * i9)) * 256) / (i8 * i8)) * 1024;
        }
        if (this.f < 1509) {
            i2 = 3;
        } else if (this.f % 499 != 0) {
            i2 = 1497;
        } else if (this.f % 491 != 0) {
            i2 = 1473;
        } else if (this.f % 487 != 0) {
            i2 = 1461;
        } else {
            i2 = 1509;
        }
        if (this.n != null) {
            int[] iArr2 = new int[(this.n.length + 1)];
            float f2 = 1.0f;
            iArr2[this.n.length] = i5;
            for (int length = this.n.length - 1; length >= 0; length--) {
                f2 -= (((float) (this.n[length].d * this.n[length].e)) / ((float) (this.o * this.p))) * this.n[length].f752a;
                iArr2[length] = (int) (((float) i5) * f2);
            }
            iArr = iArr2;
        } else {
            iArr = new int[]{i5};
        }
        int i10 = 0;
        int i11 = i6;
        int i12 = 1024;
        int i13 = i8;
        int i14 = i7;
        int i15 = 0;
        while (i15 < iArr[0]) {
            int i16 = (bArr[i10 + 0] & 255) << 4;
            int i17 = (bArr[i10 + 1] & 255) << 4;
            int i18 = (bArr[i10 + 2] & 255) << 4;
            int b2 = b(i16, i17, i18);
            b(i12, b2, i16, i17, i18);
            if (i13 != 0) {
                a(i13, b2, i16, i17, i18);
            }
            int i19 = i10 + i2;
            if (i19 >= i4) {
                i19 -= this.f;
            }
            int i20 = i15 + 1;
            if (i11 == 0) {
                i3 = 1;
            } else {
                i3 = i11;
            }
            if (i20 % i3 == 0) {
                i12 -= i12 / this.d;
                int i21 = i14 - (i14 / 30);
                i13 = i21 >> f751a;
                if (i13 <= 1) {
                    i13 = 0;
                }
                for (int i22 = 0; i22 < i13; i22++) {
                    this.l[i22] = ((((i13 * i13) - (i22 * i22)) * 256) / (i13 * i13)) * i12;
                }
                i10 = i19;
                i11 = i3;
                i14 = i21;
                i15 = i20;
            } else {
                i10 = i19;
                i11 = i3;
                i15 = i20;
            }
        }
        if (this.n != null) {
            for (int i23 = 1; i23 < iArr.length; i23++) {
                int i24 = ((this.o * this.n[i23 - 1].c) + this.n[i23 - 1].b) * 3;
                int i25 = this.n[i23 - 1].d;
                int i26 = i25 * this.n[i23 - 1].e;
                int i27 = 0;
                int i28 = i24;
                while (i15 < iArr[i23]) {
                    int i29 = (bArr[i28 + 0] & 255) << 4;
                    int i30 = (bArr[i28 + 1] & 255) << 4;
                    int i31 = (bArr[i28 + 2] & 255) << 4;
                    int b3 = b(i29, i30, i31);
                    b(i12, b3, i29, i30, i31);
                    if (i13 != 0) {
                        a(i13, b3, i29, i30, i31);
                    }
                    int i32 = (i2 / 3) + i27;
                    while (i32 >= i26) {
                        i32 -= i26;
                    }
                    i28 = ((((i32 / i25) * this.o) + (i32 % i25)) * 3) + i24;
                    i15++;
                    if (i11 == 0) {
                        i11 = 1;
                    }
                    if (i15 % i11 == 0) {
                        i12 -= i12 / this.d;
                        i14 -= i14 / 30;
                        i13 = i14 >> f751a;
                        if (i13 <= 1) {
                            i13 = 0;
                        }
                        for (int i33 = 0; i33 < i13; i33++) {
                            this.l[i33] = ((((i13 * i13) - (i33 * i33)) * 256) / (i13 * i13)) * i12;
                        }
                    }
                    i27 = i32;
                }
            }
        }
    }

    public int a(int i2, int i3, int i4) {
        int i5;
        int i6 = 1000;
        int i7 = this.i[i3];
        int i8 = -1;
        int i9 = i7 - 1;
        while (true) {
            if (i7 >= 256 && i9 < 0) {
                return i8;
            }
            if (i7 < 256) {
                int[] iArr = this.h[i7];
                int i10 = iArr[1] - i3;
                if (i10 >= i6) {
                    i5 = i8;
                    i7 = 256;
                } else {
                    i7++;
                    if (i10 < 0) {
                        i10 = -i10;
                    }
                    int i11 = iArr[0] - i2;
                    if (i11 < 0) {
                        i11 = -i11;
                    }
                    int i12 = i11 + i10;
                    if (i12 < i6) {
                        int i13 = iArr[2] - i4;
                        if (i13 < 0) {
                            i13 = -i13;
                        }
                        int i14 = i12 + i13;
                        if (i14 < i6) {
                            i5 = iArr[3];
                            i6 = i14;
                        }
                    }
                    i5 = i8;
                }
            } else {
                i5 = i8;
                i7 = i7;
            }
            if (i9 >= 0) {
                int[] iArr2 = this.h[i9];
                int i15 = i3 - iArr2[1];
                if (i15 >= i6) {
                    i8 = i5;
                    i9 = -1;
                } else {
                    i9--;
                    if (i15 < 0) {
                        i15 = -i15;
                    }
                    int i16 = iArr2[0] - i2;
                    if (i16 < 0) {
                        i16 = -i16;
                    }
                    int i17 = i16 + i15;
                    if (i17 < i6) {
                        int i18 = iArr2[2] - i4;
                        if (i18 < 0) {
                            i18 = -i18;
                        }
                        int i19 = i18 + i17;
                        if (i19 < i6) {
                            i8 = iArr2[3];
                            i6 = i19;
                        }
                    }
                }
            }
            i8 = i5;
        }
    }

    public byte[] d() {
        c();
        e();
        b();
        return a();
    }

    public void e() {
        for (int i2 = 0; i2 < 256; i2++) {
            int[] iArr = this.h[i2];
            iArr[0] = iArr[0] >> 4;
            int[] iArr2 = this.h[i2];
            iArr2[1] = iArr2[1] >> 4;
            int[] iArr3 = this.h[i2];
            iArr3[2] = iArr3[2] >> 4;
            this.h[i2][3] = i2;
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3, int i4, int i5, int i6) {
        int i7;
        int i8 = i3 - i2;
        if (i8 < -1) {
            i8 = -1;
        }
        int i9 = i3 + i2;
        if (i9 > 256 - this.m) {
            i9 = 256 - this.m;
        }
        int i10 = i3 + 1;
        int i11 = 1;
        int i12 = i3 - 1;
        while (true) {
            if (i10 < i9 || i12 > i8) {
                int i13 = i11 + 1;
                int i14 = this.l[i11];
                if (i10 < i9) {
                    i7 = i10 + 1;
                    int[] iArr = this.h[i10];
                    try {
                        iArr[0] = iArr[0] - (((iArr[0] - i4) * i14) / 262144);
                        iArr[1] = iArr[1] - (((iArr[1] - i5) * i14) / 262144);
                        iArr[2] = iArr[2] - (((iArr[2] - i6) * i14) / 262144);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                } else {
                    i7 = i10;
                }
                if (i12 > i8) {
                    int i15 = i12 - 1;
                    int[] iArr2 = this.h[i12];
                    try {
                        iArr2[0] = iArr2[0] - (((iArr2[0] - i4) * i14) / 262144);
                        iArr2[1] = iArr2[1] - (((iArr2[1] - i5) * i14) / 262144);
                        iArr2[2] = iArr2[2] - ((i14 * (iArr2[2] - i6)) / 262144);
                        i11 = i13;
                        i12 = i15;
                        i10 = i7;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        i11 = i13;
                        i12 = i15;
                        i10 = i7;
                    }
                } else {
                    i11 = i13;
                    i10 = i7;
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i2, int i3, int i4, int i5, int i6) {
        int[] iArr = this.h[i3];
        iArr[0] = iArr[0] - (((iArr[0] - i4) * i2) / 1024);
        iArr[1] = iArr[1] - (((iArr[1] - i5) * i2) / 1024);
        iArr[2] = iArr[2] - (((iArr[2] - i6) * i2) / 1024);
    }

    /* access modifiers changed from: protected */
    public int b(int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7 = Integer.MAX_VALUE;
        int i8 = Integer.MAX_VALUE;
        int i9 = -1;
        int i10 = -1;
        int i11 = 0;
        while (i11 < 256 - this.m) {
            int[] iArr = this.h[i11];
            int i12 = iArr[0] - i2;
            if (i12 < 0) {
                i12 = -i12;
            }
            int i13 = iArr[1] - i3;
            if (i13 < 0) {
                i13 = -i13;
            }
            int i14 = i13 + i12;
            int i15 = iArr[2] - i4;
            if (i15 < 0) {
                i15 = -i15;
            }
            int i16 = i14 + i15;
            if (i16 < i8) {
                i5 = i16;
                i6 = i11;
            } else {
                i5 = i8;
                i6 = i10;
            }
            int i17 = i16 - (this.j[i11] >> 12);
            if (i17 < i7) {
                i7 = i17;
                i9 = i11;
            }
            int i18 = this.k[i11] >> 10;
            int[] iArr2 = this.k;
            iArr2[i11] = iArr2[i11] - i18;
            int[] iArr3 = this.j;
            iArr3[i11] = (i18 << 10) + iArr3[i11];
            i11++;
            i8 = i5;
            i10 = i6;
        }
        int[] iArr4 = this.k;
        iArr4[i10] = iArr4[i10] + 64;
        int[] iArr5 = this.j;
        iArr5[i10] = iArr5[i10] - 65536;
        return i9;
    }
}
