package com.a.a.c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import com.google.android.exoplayer.extractor.ts.PsExtractor;
import java.io.IOException;
import java.io.OutputStream;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private int f749a;
    private int b;
    private Integer c = null;
    private int d;
    private int e = -1;
    private int f = 0;
    private boolean g = false;
    private OutputStream h;
    private Bitmap i;
    private byte[] j;
    private byte[] k;
    private int l;
    private byte[] m;
    private boolean[] n = new boolean[256];
    private int o = 7;
    private int p = -1;
    private boolean q = false;
    private boolean r = true;
    private boolean s = false;
    private int t = 10;
    private boolean u;
    private int[] v = null;
    private d[] w = null;

    public void a(int[] iArr) {
        this.v = iArr;
    }

    public void a(d[] dVarArr) {
        this.w = dVarArr;
    }

    public void a(int i2) {
        this.f = Math.round(((float) i2) / 10.0f);
    }

    public void b(int i2) {
        if (i2 >= 0) {
            this.e = i2;
        }
    }

    public boolean a(Bitmap bitmap) {
        if (bitmap == null || !this.g) {
            return false;
        }
        try {
            if (!this.s) {
                a(bitmap.getWidth(), bitmap.getHeight());
            }
            this.i = bitmap;
            c();
            b();
            if (this.r) {
                f();
                h();
                if (this.e >= 0) {
                    g();
                }
            }
            d();
            e();
            if (!this.r) {
                h();
            }
            i();
            this.r = false;
            return true;
        } catch (IOException e2) {
            return false;
        }
    }

    public boolean a() {
        boolean z;
        if (!this.g) {
            return false;
        }
        this.g = false;
        try {
            this.h.write(59);
            this.h.flush();
            if (this.q) {
                this.h.close();
            }
            z = true;
        } catch (IOException e2) {
            z = false;
        }
        this.d = 0;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.m = null;
        this.q = false;
        this.r = true;
        return z;
    }

    public void c(int i2) {
        if (i2 < 1) {
            i2 = 1;
        }
        this.t = i2;
    }

    public void a(int i2, int i3) {
        if (!this.g || this.r) {
            this.f749a = i2;
            this.b = i3;
            if (this.f749a < 1) {
                this.f749a = 320;
            }
            if (this.b < 1) {
                this.b = PsExtractor.VIDEO_STREAM_MASK;
            }
            this.s = true;
        }
    }

    public boolean a(OutputStream outputStream) {
        if (outputStream == null) {
            return false;
        }
        boolean z = true;
        this.q = false;
        this.h = outputStream;
        try {
            a("GIF89a");
        } catch (IOException e2) {
            z = false;
        }
        this.g = z;
        return z;
    }

    private void b() {
        int length = this.j.length;
        int i2 = length / 3;
        this.k = new byte[i2];
        c cVar = new c(this.j, length, this.t, this.f749a, this.b, this.v, this.w);
        this.m = cVar.d();
        for (int i3 = 0; i3 < this.m.length; i3 += 3) {
            byte b2 = this.m[i3];
            this.m[i3] = this.m[i3 + 2];
            this.m[i3 + 2] = b2;
            this.n[i3 / 3] = false;
        }
        int i4 = 0;
        for (int i5 = 0; i5 < i2; i5++) {
            int i6 = i4 + 1;
            int i7 = i6 + 1;
            i4 = i7 + 1;
            int a2 = cVar.a(this.j[i4] & 255, this.j[i6] & 255, this.j[i7] & 255);
            this.n[a2] = true;
            this.k[i5] = (byte) a2;
        }
        this.j = null;
        this.l = 8;
        this.o = 7;
        if (this.c != null) {
            this.d = d(this.c.intValue());
        } else if (this.u) {
            this.d = d(0);
        }
    }

    private int d(int i2) {
        if (this.m == null) {
            return -1;
        }
        int red = Color.red(i2);
        int green = Color.green(i2);
        int blue = Color.blue(i2);
        int i3 = 16777216;
        int length = this.m.length;
        int i4 = 0;
        int i5 = 0;
        while (i4 < length) {
            int i6 = i4 + 1;
            int i7 = red - (this.m[i4] & 255);
            int i8 = i6 + 1;
            int i9 = green - (this.m[i6] & 255);
            int i10 = blue - (this.m[i8] & 255);
            int i11 = (i7 * i7) + (i9 * i9) + (i10 * i10);
            int i12 = i8 / 3;
            if (!this.n[i12] || i11 >= i3) {
                i11 = i3;
                i12 = i5;
            }
            i4 = i8 + 1;
            i3 = i11;
            i5 = i12;
        }
        return i5;
    }

    private void c() {
        boolean z = false;
        int width = this.i.getWidth();
        int height = this.i.getHeight();
        if (!(width == this.f749a && height == this.b)) {
            Bitmap createBitmap = Bitmap.createBitmap(this.f749a, this.b, Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(createBitmap, 0.0f, 0.0f, (Paint) null);
            this.i = createBitmap;
        }
        int[] iArr = new int[(width * height)];
        this.i.getPixels(iArr, 0, width, 0, 0, width, height);
        this.j = new byte[(iArr.length * 3)];
        this.u = false;
        int i2 = 0;
        int i3 = 0;
        for (int i4 : iArr) {
            if (i4 == 0) {
                i2++;
            }
            int i5 = i3 + 1;
            this.j[i3] = (byte) (i4 & 255);
            int i6 = i5 + 1;
            this.j[i5] = (byte) ((i4 >> 8) & 255);
            i3 = i6 + 1;
            this.j[i6] = (byte) ((i4 >> 16) & 255);
        }
        double length = ((double) (i2 * 100)) / ((double) iArr.length);
        if (length > 4.0d) {
            z = true;
        }
        this.u = z;
        if (Log.isLoggable("AnimatedGifEncoderEx", 3)) {
            Log.d("AnimatedGifEncoderEx", "got pixels for frame with " + length + "% transparent pixels");
        }
    }

    private void d() {
        int i2;
        int i3;
        this.h.write(33);
        this.h.write(249);
        this.h.write(4);
        if (this.c != null || this.u) {
            i2 = 1;
            i3 = 2;
        } else {
            i3 = 0;
            i2 = 0;
        }
        if (this.p >= 0) {
            i3 = this.p & 7;
        }
        this.h.write((i3 << 2) | 0 | 0 | i2);
        e(this.f);
        this.h.write(this.d);
        this.h.write(0);
    }

    private void e() {
        this.h.write(44);
        e(0);
        e(0);
        e(this.f749a);
        e(this.b);
        if (this.r) {
            this.h.write(0);
        } else {
            this.h.write(this.o | 128);
        }
    }

    private void f() {
        e(this.f749a);
        e(this.b);
        this.h.write(this.o | PsExtractor.VIDEO_STREAM_MASK);
        this.h.write(0);
        this.h.write(0);
    }

    private void g() {
        this.h.write(33);
        this.h.write(255);
        this.h.write(11);
        a("NETSCAPE2.0");
        this.h.write(3);
        this.h.write(1);
        e(this.e);
        this.h.write(0);
    }

    private void h() {
        this.h.write(this.m, 0, this.m.length);
        int length = 768 - this.m.length;
        for (int i2 = 0; i2 < length; i2++) {
            this.h.write(0);
        }
    }

    private void i() {
        new b(this.f749a, this.b, this.k, this.l).b(this.h);
    }

    private void e(int i2) {
        this.h.write(i2 & 255);
        this.h.write((i2 >> 8) & 255);
    }

    private void a(String str) {
        for (int i2 = 0; i2 < str.length(); i2++) {
            this.h.write((byte) str.charAt(i2));
        }
    }
}
