package com.a.a;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.a.a.d.b.a.b;
import com.a.a.d.b.a.e;
import com.a.a.d.b.a.j;
import com.a.a.d.b.a.k;
import com.a.a.d.b.b.a;
import com.a.a.d.b.b.g;
import com.a.a.d.b.b.h;
import com.a.a.d.b.b.i;
import com.a.a.d.b.c.a;
import com.a.a.d.b.i;
import com.a.a.e.f;
import com.a.a.e.l;
import java.util.Map;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final Map<Class<?>, k<?, ?>> f753a = new ArrayMap();
    private i b;
    private e c;
    private b d;
    private h e;
    private a f;
    private a g;
    private a.AbstractC0028a h;
    private com.a.a.d.b.b.i i;
    private com.a.a.e.d j;
    private int k = 4;
    private com.a.a.h.d l = new com.a.a.h.d();
    @Nullable
    private l.a m;

    /* access modifiers changed from: package-private */
    public d a(@Nullable l.a aVar) {
        this.m = aVar;
        return this;
    }

    public c a(Context context) {
        if (this.f == null) {
            this.f = com.a.a.d.b.c.a.b();
        }
        if (this.g == null) {
            this.g = com.a.a.d.b.c.a.a();
        }
        if (this.i == null) {
            this.i = new i.a(context).a();
        }
        if (this.j == null) {
            this.j = new f();
        }
        if (this.c == null) {
            int b2 = this.i.b();
            if (b2 > 0) {
                this.c = new k(b2);
            } else {
                this.c = new com.a.a.d.b.a.f();
            }
        }
        if (this.d == null) {
            this.d = new j(this.i.c());
        }
        if (this.e == null) {
            this.e = new g(this.i.a());
        }
        if (this.h == null) {
            this.h = new com.a.a.d.b.b.f(context);
        }
        if (this.b == null) {
            this.b = new com.a.a.d.b.i(this.e, this.h, this.g, this.f, com.a.a.d.b.c.a.c());
        }
        return new c(context, this.b, this.e, this.c, this.d, new l(this.m), this.j, this.k, this.l.g(), this.f753a);
    }
}
