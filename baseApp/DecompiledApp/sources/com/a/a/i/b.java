package com.a.a.i;

import com.a.a.d.h;
import java.security.MessageDigest;

public final class b implements h {
    private final Object b;

    public b(Object obj) {
        this.b = com.a.a.j.h.a(obj);
    }

    public String toString() {
        return "ObjectKey{object=" + this.b + '}';
    }

    @Override // com.a.a.d.h
    public boolean equals(Object obj) {
        if (obj instanceof b) {
            return this.b.equals(((b) obj).b);
        }
        return false;
    }

    @Override // com.a.a.d.h
    public int hashCode() {
        return this.b.hashCode();
    }

    @Override // com.a.a.d.h
    public void a(MessageDigest messageDigest) {
        messageDigest.update(this.b.toString().getBytes(f922a));
    }
}
