package com.c.a;

import java.io.Closeable;
import java.nio.ByteBuffer;

public interface e extends Closeable {
    long a();

    ByteBuffer a(long j, long j2);

    void a(long j);

    @Override // java.io.Closeable, java.lang.AutoCloseable
    void close();
}
