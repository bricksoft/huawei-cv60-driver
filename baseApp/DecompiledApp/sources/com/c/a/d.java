package com.c.a;

import com.b.a.a.a;
import com.b.a.a.b;
import com.c.a.a.e;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class d implements b, Closeable, Iterator<a> {

    /* renamed from: a  reason: collision with root package name */
    private static final a f992a = new a("eof ") {
        /* class com.c.a.d.AnonymousClass1 */

        /* access modifiers changed from: protected */
        @Override // com.c.a.a
        public long d() {
            return 0;
        }

        /* access modifiers changed from: protected */
        @Override // com.c.a.a
        public void b(ByteBuffer byteBuffer) {
        }

        /* access modifiers changed from: protected */
        @Override // com.c.a.a
        public void a(ByteBuffer byteBuffer) {
        }
    };
    private static e b = e.a(d.class);
    private List<a> c = new ArrayList();
    protected com.b.a.a d;
    protected e e;
    a f = null;
    long g = 0;
    long h = 0;
    long i = 0;

    public List<a> c() {
        if (this.e == null || this.f == f992a) {
            return this.c;
        }
        return new com.c.a.a.d(this.c, this);
    }

    /* access modifiers changed from: protected */
    public long d() {
        long j = 0;
        for (int i2 = 0; i2 < c().size(); i2++) {
            j += this.c.get(i2).a();
        }
        return j;
    }

    public void a(a aVar) {
        if (aVar != null) {
            this.c = new ArrayList(c());
            aVar.a(this);
            this.c.add(aVar);
        }
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public boolean hasNext() {
        if (this.f == f992a) {
            return false;
        }
        if (this.f != null) {
            return true;
        }
        try {
            this.f = next();
            return true;
        } catch (NoSuchElementException e2) {
            this.f = f992a;
            return false;
        }
    }

    /* renamed from: e */
    public a next() {
        a a2;
        if (this.f != null && this.f != f992a) {
            a aVar = this.f;
            this.f = null;
            return aVar;
        } else if (this.e == null || this.g >= this.i) {
            this.f = f992a;
            throw new NoSuchElementException();
        } else {
            try {
                synchronized (this.e) {
                    this.e.a(this.g);
                    a2 = this.d.a(this.e, this);
                    this.g = this.e.a();
                }
                return a2;
            } catch (EOFException e2) {
                throw new NoSuchElementException();
            } catch (IOException e3) {
                throw new NoSuchElementException();
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName()).append("[");
        for (int i2 = 0; i2 < this.c.size(); i2++) {
            if (i2 > 0) {
                sb.append(";");
            }
            sb.append(this.c.get(i2).toString());
        }
        sb.append("]");
        return sb.toString();
    }

    public final void b(WritableByteChannel writableByteChannel) {
        for (a aVar : c()) {
            aVar.a(writableByteChannel);
        }
    }

    public ByteBuffer a(long j, long j2) {
        ByteBuffer a2;
        if (this.e != null) {
            synchronized (this.e) {
                a2 = this.e.a(this.h + j, j2);
            }
            return a2;
        }
        ByteBuffer allocate = ByteBuffer.allocate(com.c.a.a.b.a(j2));
        long j3 = j + j2;
        long j4 = 0;
        for (a aVar : this.c) {
            long a3 = aVar.a() + j4;
            if (a3 > j && j4 < j3) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                WritableByteChannel newChannel = Channels.newChannel(byteArrayOutputStream);
                aVar.a(newChannel);
                newChannel.close();
                if (j4 >= j && a3 <= j3) {
                    allocate.put(byteArrayOutputStream.toByteArray());
                    j4 = a3;
                } else if (j4 < j && a3 > j3) {
                    allocate.put(byteArrayOutputStream.toByteArray(), com.c.a.a.b.a(j - j4), com.c.a.a.b.a((aVar.a() - (j - j4)) - (a3 - j3)));
                    j4 = a3;
                } else if (j4 < j && a3 <= j3) {
                    allocate.put(byteArrayOutputStream.toByteArray(), com.c.a.a.b.a(j - j4), com.c.a.a.b.a(aVar.a() - (j - j4)));
                    j4 = a3;
                } else if (j4 >= j && a3 > j3) {
                    allocate.put(byteArrayOutputStream.toByteArray(), 0, com.c.a.a.b.a(aVar.a() - (a3 - j3)));
                }
            }
            j4 = a3;
        }
        return (ByteBuffer) allocate.rewind();
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.e.close();
    }
}
