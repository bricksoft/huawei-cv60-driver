package com.c.a;

import com.b.a.a.b;
import com.b.a.c;
import com.c.a.a.e;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import tv.danmaku.ijk.media.player.IjkMediaMeta;

public abstract class a implements com.b.a.a.a {

    /* renamed from: a  reason: collision with root package name */
    private static e f985a = e.a(a.class);
    static final /* synthetic */ boolean d = (!a.class.desiredAssertionStatus());
    protected String b;
    boolean c;
    private byte[] e;
    private b f;
    private ByteBuffer g;
    private ByteBuffer h = null;

    /* access modifiers changed from: protected */
    public abstract void a(ByteBuffer byteBuffer);

    /* access modifiers changed from: protected */
    public abstract void b(ByteBuffer byteBuffer);

    /* access modifiers changed from: protected */
    public abstract long d();

    protected a(String str) {
        this.b = str;
        this.c = true;
    }

    protected a(String str, byte[] bArr) {
        this.b = str;
        this.e = bArr;
        this.c = true;
    }

    @Override // com.b.a.a.a
    public void a(WritableByteChannel writableByteChannel) {
        int i;
        int i2 = 16;
        if (this.c) {
            ByteBuffer allocate = ByteBuffer.allocate(com.c.a.a.b.a(a()));
            d(allocate);
            b(allocate);
            if (this.h != null) {
                this.h.rewind();
                while (this.h.remaining() > 0) {
                    allocate.put(this.h);
                }
            }
            writableByteChannel.write((ByteBuffer) allocate.rewind());
            return;
        }
        if (b()) {
            i = 8;
        } else {
            i = 16;
        }
        if (!"uuid".equals(f())) {
            i2 = 0;
        }
        ByteBuffer allocate2 = ByteBuffer.allocate(i + i2);
        d(allocate2);
        writableByteChannel.write((ByteBuffer) allocate2.rewind());
        writableByteChannel.write((ByteBuffer) this.g.position(0));
    }

    public final synchronized void e() {
        f985a.a("parsing details of " + f());
        if (this.g != null) {
            ByteBuffer byteBuffer = this.g;
            this.c = true;
            byteBuffer.rewind();
            a(byteBuffer);
            if (byteBuffer.remaining() > 0) {
                this.h = byteBuffer.slice();
            }
            this.g = null;
            if (!d && !c(byteBuffer)) {
                throw new AssertionError();
            }
        }
    }

    @Override // com.b.a.a.a
    public long a() {
        long limit;
        int i;
        int i2;
        int i3 = 0;
        if (this.c) {
            limit = d();
        } else {
            limit = (long) (this.g != null ? this.g.limit() : 0);
        }
        if (limit >= 4294967288L) {
            i = 8;
        } else {
            i = 0;
        }
        int i4 = i + 8;
        if ("uuid".equals(f())) {
            i2 = 16;
        } else {
            i2 = 0;
        }
        long j = limit + ((long) (i2 + i4));
        if (this.h != null) {
            i3 = this.h.limit();
        }
        return ((long) i3) + j;
    }

    public String f() {
        return this.b;
    }

    public byte[] g() {
        return this.e;
    }

    @Override // com.b.a.a.a
    public void a(b bVar) {
        this.f = bVar;
    }

    public boolean h() {
        return this.c;
    }

    private boolean c(ByteBuffer byteBuffer) {
        ByteBuffer allocate = ByteBuffer.allocate(com.c.a.a.b.a(d() + ((long) (this.h != null ? this.h.limit() : 0))));
        b(allocate);
        if (this.h != null) {
            this.h.rewind();
            while (this.h.remaining() > 0) {
                allocate.put(this.h);
            }
        }
        byteBuffer.rewind();
        allocate.rewind();
        if (byteBuffer.remaining() != allocate.remaining()) {
            System.err.print(String.valueOf(f()) + ": remaining differs " + byteBuffer.remaining() + " vs. " + allocate.remaining());
            f985a.b(String.valueOf(f()) + ": remaining differs " + byteBuffer.remaining() + " vs. " + allocate.remaining());
            return false;
        }
        int position = byteBuffer.position();
        int limit = byteBuffer.limit() - 1;
        int limit2 = allocate.limit() - 1;
        while (limit >= position) {
            byte b2 = byteBuffer.get(limit);
            byte b3 = allocate.get(limit2);
            if (b2 != b3) {
                f985a.b(String.format("%s: buffers differ at %d: %2X/%2X", f(), Integer.valueOf(limit), Byte.valueOf(b2), Byte.valueOf(b3)));
                byte[] bArr = new byte[byteBuffer.remaining()];
                byte[] bArr2 = new byte[allocate.remaining()];
                byteBuffer.get(bArr);
                allocate.get(bArr2);
                System.err.println("original      : " + com.b.a.b.a(bArr, 4));
                System.err.println("reconstructed : " + com.b.a.b.a(bArr2, 4));
                return false;
            }
            limit--;
            limit2--;
        }
        return true;
    }

    private boolean b() {
        int i;
        int i2 = 8;
        if ("uuid".equals(f())) {
            i2 = 24;
        }
        if (!this.c) {
            return ((long) (i2 + this.g.limit())) < IjkMediaMeta.AV_CH_WIDE_RIGHT;
        }
        long d2 = d();
        if (this.h != null) {
            i = this.h.limit();
        } else {
            i = 0;
        }
        return (d2 + ((long) i)) + ((long) i2) < IjkMediaMeta.AV_CH_WIDE_RIGHT;
    }

    private void d(ByteBuffer byteBuffer) {
        if (b()) {
            com.b.a.e.b(byteBuffer, a());
            byteBuffer.put(c.a(f()));
        } else {
            com.b.a.e.b(byteBuffer, 1L);
            byteBuffer.put(c.a(f()));
            com.b.a.e.a(byteBuffer, a());
        }
        if ("uuid".equals(f())) {
            byteBuffer.put(g());
        }
    }
}
