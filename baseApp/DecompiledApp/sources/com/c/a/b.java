package com.c.a;

import com.b.a.a.a;
import com.b.a.e;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import tv.danmaku.ijk.media.player.IjkMediaMeta;

public class b extends d implements a {

    /* renamed from: a  reason: collision with root package name */
    com.b.a.a.b f990a;
    protected String b;
    protected boolean c;

    public b(String str) {
        this.b = str;
    }

    @Override // com.b.a.a.a
    public void a(com.b.a.a.b bVar) {
        this.f990a = bVar;
    }

    @Override // com.b.a.a.a
    public long a() {
        long d = d();
        return ((long) ((this.c || 8 + d >= IjkMediaMeta.AV_CH_WIDE_RIGHT) ? 16 : 8)) + d;
    }

    /* access modifiers changed from: protected */
    public ByteBuffer b() {
        ByteBuffer byteBuffer;
        if (this.c || a() >= IjkMediaMeta.AV_CH_WIDE_RIGHT) {
            byte[] bArr = new byte[16];
            bArr[3] = 1;
            bArr[4] = this.b.getBytes()[0];
            bArr[5] = this.b.getBytes()[1];
            bArr[6] = this.b.getBytes()[2];
            bArr[7] = this.b.getBytes()[3];
            byteBuffer = ByteBuffer.wrap(bArr);
            byteBuffer.position(8);
            e.a(byteBuffer, a());
        } else {
            byte[] bArr2 = new byte[8];
            bArr2[4] = this.b.getBytes()[0];
            bArr2[5] = this.b.getBytes()[1];
            bArr2[6] = this.b.getBytes()[2];
            bArr2[7] = this.b.getBytes()[3];
            byteBuffer = ByteBuffer.wrap(bArr2);
            e.b(byteBuffer, a());
        }
        byteBuffer.rewind();
        return byteBuffer;
    }

    @Override // com.b.a.a.a
    public void a(WritableByteChannel writableByteChannel) {
        writableByteChannel.write(b());
        b(writableByteChannel);
    }
}
