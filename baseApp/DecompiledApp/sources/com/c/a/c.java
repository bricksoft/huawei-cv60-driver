package com.c.a;

import com.b.a.d;
import com.b.a.e;
import java.nio.ByteBuffer;
import org.a.a.a.a;
import org.a.a.b.a.b;

public abstract class c extends a implements com.b.a.a.c {
    private static final a.AbstractC0099a f = null;
    private static final a.AbstractC0099a g = null;

    /* renamed from: a  reason: collision with root package name */
    private int f991a;
    private int e;

    static {
        b();
    }

    private static void b() {
        b bVar = new b("AbstractFullBox.java", c.class);
        f = bVar.a("method-execution", bVar.a("1", "setVersion", "com.googlecode.mp4parser.AbstractFullBox", "int", "version", "", "void"), 51);
        g = bVar.a("method-execution", bVar.a("1", "setFlags", "com.googlecode.mp4parser.AbstractFullBox", "int", "flags", "", "void"), 64);
    }

    protected c(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    public final long c(ByteBuffer byteBuffer) {
        this.f991a = d.d(byteBuffer);
        this.e = d.b(byteBuffer);
        return 4;
    }

    /* access modifiers changed from: protected */
    public final void d(ByteBuffer byteBuffer) {
        e.c(byteBuffer, this.f991a);
        e.a(byteBuffer, this.e);
    }
}
