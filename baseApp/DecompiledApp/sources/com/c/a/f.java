package com.c.a;

import org.a.a.a.a;
import org.a.a.a.b;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public static final f f993a = null;
    private static Throwable b;

    static {
        try {
            b();
        } catch (Throwable th) {
            b = th;
        }
    }

    public static f a() {
        if (f993a != null) {
            return f993a;
        }
        throw new b("com.googlecode.mp4parser.RequiresParseDetailAspect", b);
    }

    private static void b() {
        f993a = new f();
    }

    public void a(a aVar) {
        if (!(aVar.a() instanceof a)) {
            throw new RuntimeException("Only methods in subclasses of " + a.class.getName() + " can  be annotated with ParseDetail");
        } else if (!((a) aVar.a()).h()) {
            ((a) aVar.a()).e();
        }
    }
}
