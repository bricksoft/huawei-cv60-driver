package com.c.a.a;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class d<E> extends AbstractList<E> {
    private static final e c = e.a(d.class);

    /* renamed from: a  reason: collision with root package name */
    List<E> f988a;
    Iterator<E> b;

    public d(List<E> list, Iterator<E> it) {
        this.f988a = list;
        this.b = it;
    }

    private void a() {
        c.a("blowup running");
        while (this.b.hasNext()) {
            this.f988a.add(this.b.next());
        }
    }

    @Override // java.util.List, java.util.AbstractList
    public E get(int i) {
        if (this.f988a.size() > i) {
            return this.f988a.get(i);
        }
        if (this.b.hasNext()) {
            this.f988a.add(this.b.next());
            return get(i);
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, java.lang.Iterable
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            /* class com.c.a.a.d.AnonymousClass1 */

            /* renamed from: a  reason: collision with root package name */
            int f989a = 0;

            public boolean hasNext() {
                return this.f989a < d.this.f988a.size() || d.this.b.hasNext();
            }

            @Override // java.util.Iterator
            public E next() {
                if (this.f989a < d.this.f988a.size()) {
                    List<E> list = d.this.f988a;
                    int i = this.f989a;
                    this.f989a = i + 1;
                    return list.get(i);
                }
                d.this.f988a.add(d.this.b.next());
                return (E) next();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    public int size() {
        c.a("potentially expensive size() call");
        a();
        return this.f988a.size();
    }
}
