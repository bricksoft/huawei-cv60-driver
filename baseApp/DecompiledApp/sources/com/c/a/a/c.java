package com.c.a.a;

import java.util.logging.Level;
import java.util.logging.Logger;

public class c extends e {

    /* renamed from: a  reason: collision with root package name */
    Logger f987a;

    public c(String str) {
        this.f987a = Logger.getLogger(str);
    }

    @Override // com.c.a.a.e
    public void a(String str) {
        this.f987a.log(Level.FINE, str);
    }

    @Override // com.c.a.a.e
    public void b(String str) {
        this.f987a.log(Level.SEVERE, str);
    }
}
