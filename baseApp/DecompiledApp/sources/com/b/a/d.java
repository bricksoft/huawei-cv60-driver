package com.b.a;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import tv.danmaku.ijk.media.player.IjkMediaMeta;

public final class d {
    public static long a(ByteBuffer byteBuffer) {
        long j = (long) byteBuffer.getInt();
        if (j < 0) {
            return j + IjkMediaMeta.AV_CH_WIDE_RIGHT;
        }
        return j;
    }

    public static int b(ByteBuffer byteBuffer) {
        return 0 + (c(byteBuffer) << 8) + a(byteBuffer.get());
    }

    public static int c(ByteBuffer byteBuffer) {
        return 0 + (a(byteBuffer.get()) << 8) + a(byteBuffer.get());
    }

    public static int d(ByteBuffer byteBuffer) {
        return a(byteBuffer.get());
    }

    public static int a(byte b) {
        return b < 0 ? b + 256 : b;
    }

    public static String a(ByteBuffer byteBuffer, int i) {
        byte[] bArr = new byte[i];
        byteBuffer.get(bArr);
        return f.a(bArr);
    }

    public static String e(ByteBuffer byteBuffer) {
        byte[] bArr = new byte[4];
        byteBuffer.get(bArr);
        try {
            return new String(bArr, "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
