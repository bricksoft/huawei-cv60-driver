package com.b.a;

import java.nio.ByteBuffer;
import tv.danmaku.ijk.media.player.IjkMediaMeta;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f984a = (!e.class.desiredAssertionStatus());

    public static void a(ByteBuffer byteBuffer, long j) {
        if (f984a || j >= 0) {
            byteBuffer.putLong(j);
            return;
        }
        throw new AssertionError("The given long is negative");
    }

    public static void b(ByteBuffer byteBuffer, long j) {
        if (f984a || (j >= 0 && j <= IjkMediaMeta.AV_CH_WIDE_RIGHT)) {
            byteBuffer.putInt((int) j);
            return;
        }
        throw new AssertionError("The given long is not in the range of uint32 (" + j + ")");
    }

    public static void a(ByteBuffer byteBuffer, int i) {
        int i2 = 16777215 & i;
        b(byteBuffer, i2 >> 8);
        c(byteBuffer, i2);
    }

    public static void b(ByteBuffer byteBuffer, int i) {
        int i2 = 65535 & i;
        c(byteBuffer, i2 >> 8);
        c(byteBuffer, i2 & 255);
    }

    public static void c(ByteBuffer byteBuffer, int i) {
        byteBuffer.put((byte) (i & 255));
    }
}
