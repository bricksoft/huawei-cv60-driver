package com.b.a;

import com.c.a.a.e;
import com.c.a.d;
import java.io.Closeable;

public class c extends d implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    private static e f983a = e.a(c.class);

    public static byte[] a(String str) {
        byte[] bArr = new byte[4];
        if (str != null) {
            for (int i = 0; i < Math.min(4, str.length()); i++) {
                bArr[i] = (byte) str.charAt(i);
            }
        }
        return bArr;
    }

    @Override // com.c.a.d, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.e.close();
    }

    @Override // com.c.a.d
    public String toString() {
        return "model(" + this.e.toString() + ")";
    }
}
