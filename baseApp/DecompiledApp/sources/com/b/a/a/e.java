package com.b.a.a;

import com.c.a.b;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import tv.danmaku.ijk.media.player.IjkMediaMeta;

public class e extends b {
    private boolean j = true;
    private int k;
    private int l;

    public e() {
        super("meta");
    }

    /* access modifiers changed from: protected */
    public final void a(ByteBuffer byteBuffer) {
        com.b.a.e.c(byteBuffer, this.k);
        com.b.a.e.a(byteBuffer, this.l);
    }

    @Override // com.c.a.b, com.b.a.a.a
    public void a(WritableByteChannel writableByteChannel) {
        writableByteChannel.write(b());
        if (this.j) {
            ByteBuffer allocate = ByteBuffer.allocate(4);
            a(allocate);
            writableByteChannel.write((ByteBuffer) allocate.rewind());
        }
        b(writableByteChannel);
    }

    @Override // com.c.a.b, com.b.a.a.a
    public long a() {
        long d = d();
        long j2 = 0;
        if (this.j) {
            j2 = 0 + 4;
        }
        return ((long) ((this.c || j2 + d >= IjkMediaMeta.AV_CH_WIDE_RIGHT) ? 16 : 8)) + d + j2;
    }
}
