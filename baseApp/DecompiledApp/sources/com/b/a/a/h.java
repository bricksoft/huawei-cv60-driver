package com.b.a.a;

import com.b.a.d;
import com.c.a.c;
import com.c.a.f;
import java.nio.ByteBuffer;
import org.a.a.a.a;
import org.a.a.b.a.b;

public class h extends c {
    private static final a.AbstractC0099a e = null;
    private static final a.AbstractC0099a f = null;
    private static final a.AbstractC0099a g = null;

    /* renamed from: a  reason: collision with root package name */
    String f981a = "";

    static {
        b();
    }

    private static void b() {
        b bVar = new b("XmlBox.java", h.class);
        e = bVar.a("method-execution", bVar.a("1", "getXml", "com.coremedia.iso.boxes.XmlBox", "", "", "", "java.lang.String"), 20);
        f = bVar.a("method-execution", bVar.a("1", "setXml", "com.coremedia.iso.boxes.XmlBox", "java.lang.String", "xml", "", "void"), 24);
        g = bVar.a("method-execution", bVar.a("1", "toString", "com.coremedia.iso.boxes.XmlBox", "", "", "", "java.lang.String"), 46);
    }

    public h() {
        super("xml ");
    }

    public void a(String str) {
        f.a().a(b.a(f, this, this, str));
        this.f981a = str;
    }

    /* access modifiers changed from: protected */
    @Override // com.c.a.a
    public long d() {
        return (long) (com.b.a.f.b(this.f981a) + 4);
    }

    @Override // com.c.a.a
    public void a(ByteBuffer byteBuffer) {
        c(byteBuffer);
        this.f981a = d.a(byteBuffer, byteBuffer.remaining());
    }

    /* access modifiers changed from: protected */
    @Override // com.c.a.a
    public void b(ByteBuffer byteBuffer) {
        d(byteBuffer);
        byteBuffer.put(com.b.a.f.a(this.f981a));
    }

    public String toString() {
        f.a().a(b.a(g, this, this));
        return "XmlBox{xml='" + this.f981a + '\'' + '}';
    }
}
