package com.b.a.a;

import com.b.a.e;
import com.c.a.c;
import com.c.a.f;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.a.a.a.a;
import org.a.a.b.a.b;

public class d extends c {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<String, String> f979a;
    private static final a.AbstractC0099a l = null;
    private static final a.AbstractC0099a m = null;
    private static final a.AbstractC0099a n = null;
    private static final a.AbstractC0099a o = null;
    private static final a.AbstractC0099a p = null;
    private static final a.AbstractC0099a q = null;
    private String e;
    private String f = null;
    private long g;
    private long h;
    private long i;
    private boolean j = true;
    private long k;

    private static void i() {
        b bVar = new b("HandlerBox.java", d.class);
        l = bVar.a("method-execution", bVar.a("1", "getHandlerType", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 78);
        m = bVar.a("method-execution", bVar.a("1", "setName", "com.coremedia.iso.boxes.HandlerBox", "java.lang.String", "name", "", "void"), 87);
        n = bVar.a("method-execution", bVar.a("1", "setHandlerType", "com.coremedia.iso.boxes.HandlerBox", "java.lang.String", "handlerType", "", "void"), 91);
        o = bVar.a("method-execution", bVar.a("1", "getName", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 95);
        p = bVar.a("method-execution", bVar.a("1", "getHumanReadableTrackType", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 99);
        q = bVar.a("method-execution", bVar.a("1", "toString", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 149);
    }

    static {
        i();
        HashMap hashMap = new HashMap();
        hashMap.put("odsm", "ObjectDescriptorStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hashMap.put("crsm", "ClockReferenceStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hashMap.put("sdsm", "SceneDescriptionStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hashMap.put("m7sm", "MPEG7Stream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hashMap.put("ocsm", "ObjectContentInfoStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hashMap.put("ipsm", "IPMP Stream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hashMap.put("mjsm", "MPEG-J Stream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hashMap.put("mdir", "Apple Meta Data iTunes Reader");
        hashMap.put("mp7b", "MPEG-7 binary XML");
        hashMap.put("mp7t", "MPEG-7 XML");
        hashMap.put("vide", "Video Track");
        hashMap.put("soun", "Sound Track");
        hashMap.put("hint", "Hint Track");
        hashMap.put("appl", "Apple specific");
        hashMap.put("meta", "Timed Metadata track - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        f979a = Collections.unmodifiableMap(hashMap);
    }

    public d() {
        super("hdlr");
    }

    public String b() {
        f.a().a(b.a(l, this, this));
        return this.e;
    }

    public String c() {
        f.a().a(b.a(o, this, this));
        return this.f;
    }

    /* access modifiers changed from: protected */
    @Override // com.c.a.a
    public long d() {
        if (this.j) {
            return (long) (com.b.a.f.b(this.f) + 25);
        }
        return (long) (com.b.a.f.b(this.f) + 24);
    }

    @Override // com.c.a.a
    public void a(ByteBuffer byteBuffer) {
        c(byteBuffer);
        this.k = com.b.a.d.a(byteBuffer);
        this.e = com.b.a.d.e(byteBuffer);
        this.g = com.b.a.d.a(byteBuffer);
        this.h = com.b.a.d.a(byteBuffer);
        this.i = com.b.a.d.a(byteBuffer);
        if (byteBuffer.remaining() > 0) {
            this.f = com.b.a.d.a(byteBuffer, byteBuffer.remaining());
            if (this.f.endsWith("\u0000")) {
                this.f = this.f.substring(0, this.f.length() - 1);
                this.j = true;
                return;
            }
            this.j = false;
            return;
        }
        this.j = false;
    }

    /* access modifiers changed from: protected */
    @Override // com.c.a.a
    public void b(ByteBuffer byteBuffer) {
        d(byteBuffer);
        e.b(byteBuffer, this.k);
        byteBuffer.put(com.b.a.c.a(this.e));
        e.b(byteBuffer, this.g);
        e.b(byteBuffer, this.h);
        e.b(byteBuffer, this.i);
        if (this.f != null) {
            byteBuffer.put(com.b.a.f.a(this.f));
        }
        if (this.j) {
            byteBuffer.put((byte) 0);
        }
    }

    public String toString() {
        f.a().a(b.a(q, this, this));
        return "HandlerBox[handlerType=" + b() + ";name=" + c() + "]";
    }
}
