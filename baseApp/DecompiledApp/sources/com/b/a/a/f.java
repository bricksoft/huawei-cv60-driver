package com.b.a.a;

import com.c.a.a;
import java.nio.ByteBuffer;
import org.a.a.a.a;
import org.a.a.b.a.b;

public class f extends a {
    private static final a.AbstractC0099a e = null;
    private static final a.AbstractC0099a f = null;
    private static final a.AbstractC0099a g = null;

    /* renamed from: a  reason: collision with root package name */
    byte[] f980a;

    static {
        c();
    }

    private static void c() {
        b bVar = new b("UserBox.java", f.class);
        e = bVar.a("method-execution", bVar.a("1", "toString", "com.coremedia.iso.boxes.UserBox", "", "", "", "java.lang.String"), 40);
        f = bVar.a("method-execution", bVar.a("1", "getData", "com.coremedia.iso.boxes.UserBox", "", "", "", "[B"), 47);
        g = bVar.a("method-execution", bVar.a("1", "setData", "com.coremedia.iso.boxes.UserBox", "[B", "data", "", "void"), 51);
    }

    public f(byte[] bArr) {
        super("uuid", bArr);
    }

    /* access modifiers changed from: protected */
    @Override // com.c.a.a
    public long d() {
        return (long) this.f980a.length;
    }

    public String toString() {
        com.c.a.f.a().a(b.a(e, this, this));
        return "UserBox[type=" + f() + ";userType=" + new String(g()) + ";contentLength=" + this.f980a.length + "]";
    }

    public byte[] b() {
        com.c.a.f.a().a(b.a(f, this, this));
        return this.f980a;
    }

    public void a(byte[] bArr) {
        com.c.a.f.a().a(b.a(g, this, this, bArr));
        this.f980a = bArr;
    }

    @Override // com.c.a.a
    public void a(ByteBuffer byteBuffer) {
        this.f980a = new byte[byteBuffer.remaining()];
        byteBuffer.get(this.f980a);
    }

    /* access modifiers changed from: protected */
    @Override // com.c.a.a
    public void b(ByteBuffer byteBuffer) {
        byteBuffer.put(this.f980a);
    }
}
