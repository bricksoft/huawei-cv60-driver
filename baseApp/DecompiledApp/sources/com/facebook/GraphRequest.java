package com.facebook;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.internal.m;
import com.facebook.internal.o;
import com.facebook.internal.s;
import com.facebook.internal.u;
import com.facebook.internal.v;
import com.facebook.o;
import com.google.android.exoplayer.C;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import com.google.api.client.http.UrlEncodedParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tv.danmaku.ijk.media.player.IjkMediaMeta;

public class GraphRequest {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1003a = GraphRequest.class.getSimpleName();
    private static final String b;
    private static String c;
    private static Pattern d = Pattern.compile("^/?v\\d+\\.\\d+/(.*)");
    private static volatile String r;
    private AccessToken e;
    private q f;
    private String g;
    private JSONObject h;
    private String i;
    private String j;
    private boolean k;
    private Bundle l;
    private b m;
    private String n;
    private Object o;
    private String p;
    private boolean q;

    public interface b {
        void a(p pVar);
    }

    public interface c {
        void a(JSONObject jSONObject, p pVar);
    }

    /* access modifiers changed from: private */
    public interface d {
        void a(String str, String str2);
    }

    public interface e extends b {
        void a(long j, long j2);
    }

    static {
        char[] charArray = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        StringBuilder sb = new StringBuilder();
        SecureRandom secureRandom = new SecureRandom();
        int nextInt = secureRandom.nextInt(11) + 30;
        for (int i2 = 0; i2 < nextInt; i2++) {
            sb.append(charArray[secureRandom.nextInt(charArray.length)]);
        }
        b = sb.toString();
    }

    public GraphRequest() {
        this(null, null, null, null, null);
    }

    public GraphRequest(AccessToken accessToken, String str, Bundle bundle, q qVar, b bVar) {
        this(accessToken, str, bundle, qVar, bVar, null);
    }

    public GraphRequest(AccessToken accessToken, String str, Bundle bundle, q qVar, b bVar, String str2) {
        this.k = true;
        this.q = false;
        this.e = accessToken;
        this.g = str;
        this.p = str2;
        a(bVar);
        a(qVar);
        if (bundle != null) {
            this.l = new Bundle(bundle);
        } else {
            this.l = new Bundle();
        }
        if (this.p == null) {
            this.p = k.g();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.facebook.GraphRequest$1  reason: invalid class name */
    public static class AnonymousClass1 implements b {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ c f1004a;

        @Override // com.facebook.GraphRequest.b
        public void a(p pVar) {
            if (this.f1004a != null) {
                this.f1004a.a(pVar.b(), pVar);
            }
        }
    }

    public static GraphRequest a(AccessToken accessToken, String str, JSONObject jSONObject, b bVar) {
        GraphRequest graphRequest = new GraphRequest(accessToken, str, null, q.POST, bVar);
        graphRequest.a(jSONObject);
        return graphRequest;
    }

    public static GraphRequest a(AccessToken accessToken, String str, b bVar) {
        return new GraphRequest(accessToken, str, null, null, bVar);
    }

    public final JSONObject a() {
        return this.h;
    }

    public final void a(JSONObject jSONObject) {
        this.h = jSONObject;
    }

    public final String b() {
        return this.g;
    }

    public final q c() {
        return this.f;
    }

    public final void a(q qVar) {
        if (this.n == null || qVar == q.GET) {
            if (qVar == null) {
                qVar = q.GET;
            }
            this.f = qVar;
            return;
        }
        throw new h("Can't change HTTP method on request with overridden URL.");
    }

    public final String d() {
        return this.p;
    }

    public final void a(boolean z) {
        this.q = z;
    }

    public final Bundle e() {
        return this.l;
    }

    public final void a(Bundle bundle) {
        this.l = bundle;
    }

    public final AccessToken f() {
        return this.e;
    }

    public final b g() {
        return this.m;
    }

    public final void a(final b bVar) {
        if (k.a(t.GRAPH_API_DEBUG_INFO) || k.a(t.GRAPH_API_DEBUG_WARNING)) {
            this.m = new b() {
                /* class com.facebook.GraphRequest.AnonymousClass2 */

                @Override // com.facebook.GraphRequest.b
                public void a(p pVar) {
                    JSONObject b2 = pVar.b();
                    JSONObject optJSONObject = b2 != null ? b2.optJSONObject("__debug__") : null;
                    JSONArray optJSONArray = optJSONObject != null ? optJSONObject.optJSONArray("messages") : null;
                    if (optJSONArray != null) {
                        for (int i = 0; i < optJSONArray.length(); i++) {
                            JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                            String optString = optJSONObject2 != null ? optJSONObject2.optString("message") : null;
                            String optString2 = optJSONObject2 != null ? optJSONObject2.optString(IjkMediaMeta.IJKM_KEY_TYPE) : null;
                            String optString3 = optJSONObject2 != null ? optJSONObject2.optString("link") : null;
                            if (!(optString == null || optString2 == null)) {
                                t tVar = t.GRAPH_API_DEBUG_INFO;
                                if (optString2.equals("warning")) {
                                    tVar = t.GRAPH_API_DEBUG_WARNING;
                                }
                                if (!u.a(optString3)) {
                                    optString = optString + " Link: " + optString3;
                                }
                                o.a(tVar, GraphRequest.f1003a, optString);
                            }
                        }
                    }
                    if (bVar != null) {
                        bVar.a(pVar);
                    }
                }
            };
        } else {
            this.m = bVar;
        }
    }

    public final void a(Object obj) {
        this.o = obj;
    }

    public final Object h() {
        return this.o;
    }

    public final p i() {
        return a(this);
    }

    public final n j() {
        return b(this);
    }

    public static HttpURLConnection a(o oVar) {
        URL url;
        d(oVar);
        try {
            if (oVar.size() == 1) {
                url = new URL(oVar.get(0).l());
            } else {
                url = new URL(s.b());
            }
            HttpURLConnection httpURLConnection = null;
            try {
                httpURLConnection = a(url);
                a(oVar, httpURLConnection);
                return httpURLConnection;
            } catch (IOException | JSONException e2) {
                u.a(httpURLConnection);
                throw new h("could not construct request body", e2);
            }
        } catch (MalformedURLException e3) {
            throw new h("could not construct URL for request", e3);
        }
    }

    public static p a(GraphRequest graphRequest) {
        List<p> a2 = a(graphRequest);
        if (a2 != null && a2.size() == 1) {
            return a2.get(0);
        }
        throw new h("invalid state: expected a single response");
    }

    public static List<p> a(GraphRequest... graphRequestArr) {
        v.a(graphRequestArr, "requests");
        return a((Collection<GraphRequest>) Arrays.asList(graphRequestArr));
    }

    public static List<p> a(Collection<GraphRequest> collection) {
        return b(new o(collection));
    }

    public static List<p> b(o oVar) {
        v.c(oVar, "requests");
        try {
            HttpURLConnection a2 = a(oVar);
            try {
                return a(a2, oVar);
            } finally {
                u.a(a2);
            }
        } catch (Exception e2) {
            List<p> a3 = p.a(oVar.d(), (HttpURLConnection) null, new h(e2));
            a(oVar, a3);
            u.a((URLConnection) null);
            return a3;
        }
    }

    public static n b(GraphRequest... graphRequestArr) {
        v.a(graphRequestArr, "requests");
        return b((Collection<GraphRequest>) Arrays.asList(graphRequestArr));
    }

    public static n b(Collection<GraphRequest> collection) {
        return c(new o(collection));
    }

    public static n c(o oVar) {
        v.c(oVar, "requests");
        n nVar = new n(oVar);
        nVar.executeOnExecutor(k.d(), new Void[0]);
        return nVar;
    }

    public static List<p> a(HttpURLConnection httpURLConnection, o oVar) {
        List<p> a2 = p.a(httpURLConnection, oVar);
        u.a(httpURLConnection);
        int size = oVar.size();
        if (size != a2.size()) {
            throw new h(String.format(Locale.US, "Received %d responses while expecting %d", Integer.valueOf(a2.size()), Integer.valueOf(size)));
        }
        a(oVar, a2);
        b.a().e();
        return a2;
    }

    public String toString() {
        return "{Request: " + " accessToken: " + (this.e == null ? "null" : this.e) + ", graphPath: " + this.g + ", graphObject: " + this.h + ", httpMethod: " + this.f + ", parameters: " + this.l + "}";
    }

    static void a(final o oVar, List<p> list) {
        int size = oVar.size();
        final ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < size; i2++) {
            GraphRequest a2 = oVar.get(i2);
            if (a2.m != null) {
                arrayList.add(new Pair(a2.m, list.get(i2)));
            }
        }
        if (arrayList.size() > 0) {
            AnonymousClass3 r0 = new Runnable() {
                /* class com.facebook.GraphRequest.AnonymousClass3 */

                public void run() {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        Pair pair = (Pair) it.next();
                        ((b) pair.first).a((p) pair.second);
                    }
                    for (o.a aVar : oVar.e()) {
                        aVar.a(oVar);
                    }
                }
            };
            Handler c2 = oVar.c();
            if (c2 == null) {
                r0.run();
            } else {
                c2.post(r0);
            }
        }
    }

    private static HttpURLConnection a(URL url) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestProperty("User-Agent", q());
        httpURLConnection.setRequestProperty("Accept-Language", Locale.getDefault().toString());
        httpURLConnection.setChunkedStreamingMode(0);
        return httpURLConnection;
    }

    private void n() {
        if (this.e != null) {
            if (!this.l.containsKey("access_token")) {
                String d2 = this.e.d();
                com.facebook.internal.o.a(d2);
                this.l.putString("access_token", d2);
            }
        } else if (!this.q && !this.l.containsKey("access_token")) {
            String j2 = k.j();
            String k2 = k.k();
            if (u.a(j2) || u.a(k2)) {
                u.a(f1003a, "Warning: Request without access token missing application ID or client token.");
            } else {
                this.l.putString("access_token", j2 + "|" + k2);
            }
        }
        this.l.putString("sdk", "android");
        this.l.putString(IjkMediaMeta.IJKM_KEY_FORMAT, "json");
        if (k.a(t.GRAPH_API_DEBUG_INFO)) {
            this.l.putString("debug", "info");
        } else if (k.a(t.GRAPH_API_DEBUG_WARNING)) {
            this.l.putString("debug", "warning");
        }
    }

    private String a(String str, Boolean bool) {
        if (!bool.booleanValue() && this.f == q.POST) {
            return str;
        }
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        for (String str2 : this.l.keySet()) {
            Object obj = this.l.get(str2);
            if (obj == null) {
                obj = "";
            }
            if (e(obj)) {
                buildUpon.appendQueryParameter(str2, f(obj).toString());
            } else if (this.f == q.GET) {
                throw new IllegalArgumentException(String.format(Locale.US, "Unsupported parameter type for GET request: %s", obj.getClass().getSimpleName()));
            }
        }
        return buildUpon.toString();
    }

    /* access modifiers changed from: package-private */
    public final String k() {
        if (this.n != null) {
            throw new h("Can't override URL for a batch request");
        }
        String format = String.format("%s/%s", s.b(), o());
        n();
        Uri parse = Uri.parse(a(format, (Boolean) true));
        return String.format("%s?%s", parse.getPath(), parse.getQuery());
    }

    /* access modifiers changed from: package-private */
    public final String l() {
        String b2;
        if (this.n != null) {
            return this.n.toString();
        }
        if (c() != q.POST || this.g == null || !this.g.endsWith("/videos")) {
            b2 = s.b();
        } else {
            b2 = s.c();
        }
        String format = String.format("%s/%s", b2, o());
        n();
        return a(format, (Boolean) false);
    }

    private String o() {
        if (d.matcher(this.g).matches()) {
            return this.g;
        }
        return String.format("%s/%s", this.p, this.g);
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final GraphRequest f1009a;
        private final Object b;

        public a(GraphRequest graphRequest, Object obj) {
            this.f1009a = graphRequest;
            this.b = obj;
        }

        public GraphRequest a() {
            return this.f1009a;
        }

        public Object b() {
            return this.b;
        }
    }

    private void a(JSONArray jSONArray, Map<String, a> map) {
        JSONObject jSONObject = new JSONObject();
        if (this.i != null) {
            jSONObject.put("name", this.i);
            jSONObject.put("omit_response_on_success", this.k);
        }
        if (this.j != null) {
            jSONObject.put("depends_on", this.j);
        }
        String k2 = k();
        jSONObject.put("relative_url", k2);
        jSONObject.put("method", this.f);
        if (this.e != null) {
            com.facebook.internal.o.a(this.e.d());
        }
        ArrayList arrayList = new ArrayList();
        for (String str : this.l.keySet()) {
            Object obj = this.l.get(str);
            if (d(obj)) {
                String format = String.format(Locale.ROOT, "%s%d", "file", Integer.valueOf(map.size()));
                arrayList.add(format);
                map.put(format, new a(this, obj));
            }
        }
        if (!arrayList.isEmpty()) {
            jSONObject.put("attached_files", TextUtils.join(",", arrayList));
        }
        if (this.h != null) {
            final ArrayList arrayList2 = new ArrayList();
            a(this.h, k2, new d() {
                /* class com.facebook.GraphRequest.AnonymousClass4 */

                @Override // com.facebook.GraphRequest.d
                public void a(String str, String str2) {
                    arrayList2.add(String.format(Locale.US, "%s=%s", str, URLEncoder.encode(str2, C.UTF8_NAME)));
                }
            });
            jSONObject.put(TtmlNode.TAG_BODY, TextUtils.join("&", arrayList2));
        }
        jSONArray.put(jSONObject);
    }

    private static boolean e(o oVar) {
        for (o.a aVar : oVar.e()) {
            if (aVar instanceof o.b) {
                return true;
            }
        }
        Iterator it = oVar.iterator();
        while (it.hasNext()) {
            if (((GraphRequest) it.next()).g() instanceof e) {
                return true;
            }
        }
        return false;
    }

    private static void a(HttpURLConnection httpURLConnection, boolean z) {
        if (z) {
            httpURLConnection.setRequestProperty("Content-Type", UrlEncodedParser.CONTENT_TYPE);
            httpURLConnection.setRequestProperty("Content-Encoding", "gzip");
            return;
        }
        httpURLConnection.setRequestProperty("Content-Type", p());
    }

    private static boolean f(o oVar) {
        Iterator it = oVar.iterator();
        while (it.hasNext()) {
            GraphRequest graphRequest = (GraphRequest) it.next();
            Iterator<String> it2 = graphRequest.l.keySet().iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (d(graphRequest.l.get(it2.next()))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    static final boolean b(GraphRequest graphRequest) {
        boolean z;
        String d2 = graphRequest.d();
        if (u.a(d2)) {
            return true;
        }
        if (d2.startsWith("v")) {
            d2 = d2.substring(1);
        }
        String[] split = d2.split("\\.");
        if ((split.length < 2 || Integer.parseInt(split[0]) <= 2) && (Integer.parseInt(split[0]) < 2 || Integer.parseInt(split[1]) < 4)) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    static final void d(o oVar) {
        Iterator it = oVar.iterator();
        while (it.hasNext()) {
            GraphRequest graphRequest = (GraphRequest) it.next();
            if (q.GET.equals(graphRequest.c()) && b(graphRequest)) {
                Bundle e2 = graphRequest.e();
                if (!e2.containsKey("fields") || u.a(e2.getString("fields"))) {
                    com.facebook.internal.o.a(t.DEVELOPER_ERRORS, 5, "Request", "starting with Graph API v2.4, GET requests for /%s should contain an explicit \"fields\" parameter.", graphRequest.b());
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static final void a(com.facebook.o r13, java.net.HttpURLConnection r14) {
        /*
        // Method dump skipped, instructions count: 207
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.GraphRequest.a(com.facebook.o, java.net.HttpURLConnection):void");
    }

    private static void a(o oVar, com.facebook.internal.o oVar2, int i2, URL url, OutputStream outputStream, boolean z) {
        f fVar = new f(outputStream, oVar2, z);
        if (i2 == 1) {
            GraphRequest a2 = oVar.get(0);
            HashMap hashMap = new HashMap();
            for (String str : a2.l.keySet()) {
                Object obj = a2.l.get(str);
                if (d(obj)) {
                    hashMap.put(str, new a(a2, obj));
                }
            }
            if (oVar2 != null) {
                oVar2.c("  Parameters:\n");
            }
            a(a2.l, fVar, a2);
            if (oVar2 != null) {
                oVar2.c("  Attachments:\n");
            }
            a(hashMap, fVar);
            if (a2.h != null) {
                a(a2.h, url.getPath(), fVar);
                return;
            }
            return;
        }
        String g2 = g(oVar);
        if (u.a(g2)) {
            throw new h("App ID was not specified at the request or Settings.");
        }
        fVar.a("batch_app_id", g2);
        HashMap hashMap2 = new HashMap();
        a(fVar, oVar, hashMap2);
        if (oVar2 != null) {
            oVar2.c("  Attachments:\n");
        }
        a(hashMap2, fVar);
    }

    private static boolean a(String str) {
        Matcher matcher = d.matcher(str);
        if (matcher.matches()) {
            str = matcher.group(1);
        }
        if (str.startsWith("me/") || str.startsWith("/me/")) {
            return true;
        }
        return false;
    }

    private static void a(JSONObject jSONObject, String str, d dVar) {
        boolean z;
        boolean z2;
        if (a(str)) {
            int indexOf = str.indexOf(":");
            int indexOf2 = str.indexOf("?");
            z = indexOf > 3 && (indexOf2 == -1 || indexOf < indexOf2);
        } else {
            z = false;
        }
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            Object opt = jSONObject.opt(next);
            if (!z || !next.equalsIgnoreCase("image")) {
                z2 = false;
            } else {
                z2 = true;
            }
            a(next, opt, dVar, z2);
        }
    }

    private static void a(String str, Object obj, d dVar, boolean z) {
        Class<?> cls = obj.getClass();
        if (JSONObject.class.isAssignableFrom(cls)) {
            JSONObject jSONObject = (JSONObject) obj;
            if (z) {
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    a(String.format("%s[%s]", str, next), jSONObject.opt(next), dVar, z);
                }
            } else if (jSONObject.has(TtmlNode.ATTR_ID)) {
                a(str, jSONObject.optString(TtmlNode.ATTR_ID), dVar, z);
            } else if (jSONObject.has("url")) {
                a(str, jSONObject.optString("url"), dVar, z);
            } else if (jSONObject.has("fbsdk:create_object")) {
                a(str, jSONObject.toString(), dVar, z);
            }
        } else if (JSONArray.class.isAssignableFrom(cls)) {
            JSONArray jSONArray = (JSONArray) obj;
            int length = jSONArray.length();
            for (int i2 = 0; i2 < length; i2++) {
                a(String.format(Locale.ROOT, "%s[%d]", str, Integer.valueOf(i2)), jSONArray.opt(i2), dVar, z);
            }
        } else if (String.class.isAssignableFrom(cls) || Number.class.isAssignableFrom(cls) || Boolean.class.isAssignableFrom(cls)) {
            dVar.a(str, obj.toString());
        } else if (Date.class.isAssignableFrom(cls)) {
            dVar.a(str, new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format((Date) obj));
        }
    }

    private static void a(Bundle bundle, f fVar, GraphRequest graphRequest) {
        for (String str : bundle.keySet()) {
            Object obj = bundle.get(str);
            if (e(obj)) {
                fVar.a(str, obj, graphRequest);
            }
        }
    }

    private static void a(Map<String, a> map, f fVar) {
        for (String str : map.keySet()) {
            a aVar = map.get(str);
            if (d(aVar.b())) {
                fVar.a(str, aVar.b(), aVar.a());
            }
        }
    }

    private static void a(f fVar, Collection<GraphRequest> collection, Map<String, a> map) {
        JSONArray jSONArray = new JSONArray();
        for (GraphRequest graphRequest : collection) {
            graphRequest.a(jSONArray, map);
        }
        fVar.a("batch", jSONArray, collection);
    }

    private static String p() {
        return String.format("multipart/form-data; boundary=%s", b);
    }

    private static String q() {
        if (r == null) {
            r = String.format("%s.%s", "FBAndroidSDK", "4.42.0");
            String a2 = m.a();
            if (!u.a(a2)) {
                r = String.format(Locale.ROOT, "%s/%s", r, a2);
            }
        }
        return r;
    }

    private static String g(o oVar) {
        String k2;
        if (!u.a(oVar.f())) {
            return oVar.f();
        }
        Iterator it = oVar.iterator();
        while (it.hasNext()) {
            AccessToken accessToken = ((GraphRequest) it.next()).e;
            if (accessToken != null && (k2 = accessToken.k()) != null) {
                return k2;
            }
        }
        if (!u.a(c)) {
            return c;
        }
        return k.j();
    }

    private static boolean d(Object obj) {
        return (obj instanceof Bitmap) || (obj instanceof byte[]) || (obj instanceof Uri) || (obj instanceof ParcelFileDescriptor) || (obj instanceof ParcelableResourceWithMimeType);
    }

    /* access modifiers changed from: private */
    public static boolean e(Object obj) {
        return (obj instanceof String) || (obj instanceof Boolean) || (obj instanceof Number) || (obj instanceof Date);
    }

    /* access modifiers changed from: private */
    public static String f(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if ((obj instanceof Boolean) || (obj instanceof Number)) {
            return obj.toString();
        }
        if (obj instanceof Date) {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format(obj);
        }
        throw new IllegalArgumentException("Unsupported parameter type.");
    }

    /* access modifiers changed from: private */
    public static class f implements d {

        /* renamed from: a  reason: collision with root package name */
        private final OutputStream f1010a;
        private final com.facebook.internal.o b;
        private boolean c = true;
        private boolean d = false;

        public f(OutputStream outputStream, com.facebook.internal.o oVar, boolean z) {
            this.f1010a = outputStream;
            this.b = oVar;
            this.d = z;
        }

        public void a(String str, Object obj, GraphRequest graphRequest) {
            if (this.f1010a instanceof z) {
                ((z) this.f1010a).a(graphRequest);
            }
            if (GraphRequest.e(obj)) {
                a(str, GraphRequest.f(obj));
            } else if (obj instanceof Bitmap) {
                a(str, (Bitmap) obj);
            } else if (obj instanceof byte[]) {
                a(str, (byte[]) obj);
            } else if (obj instanceof Uri) {
                a(str, (Uri) obj, (String) null);
            } else if (obj instanceof ParcelFileDescriptor) {
                a(str, (ParcelFileDescriptor) obj, (String) null);
            } else if (obj instanceof ParcelableResourceWithMimeType) {
                ParcelableResourceWithMimeType parcelableResourceWithMimeType = (ParcelableResourceWithMimeType) obj;
                Parcelable b2 = parcelableResourceWithMimeType.b();
                String a2 = parcelableResourceWithMimeType.a();
                if (b2 instanceof ParcelFileDescriptor) {
                    a(str, (ParcelFileDescriptor) b2, a2);
                } else if (b2 instanceof Uri) {
                    a(str, (Uri) b2, a2);
                } else {
                    throw b();
                }
            } else {
                throw b();
            }
        }

        private RuntimeException b() {
            return new IllegalArgumentException("value is not a supported type.");
        }

        public void a(String str, JSONArray jSONArray, Collection<GraphRequest> collection) {
            if (!(this.f1010a instanceof z)) {
                a(str, jSONArray.toString());
                return;
            }
            z zVar = (z) this.f1010a;
            a(str, (String) null, (String) null);
            a("[", new Object[0]);
            int i = 0;
            for (GraphRequest graphRequest : collection) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                zVar.a(graphRequest);
                if (i > 0) {
                    a(",%s", jSONObject.toString());
                } else {
                    a("%s", jSONObject.toString());
                }
                i++;
            }
            a("]", new Object[0]);
            if (this.b != null) {
                this.b.a("    " + str, (Object) jSONArray.toString());
            }
        }

        @Override // com.facebook.GraphRequest.d
        public void a(String str, String str2) {
            a(str, (String) null, (String) null);
            b("%s", str2);
            a();
            if (this.b != null) {
                this.b.a("    " + str, (Object) str2);
            }
        }

        public void a(String str, Bitmap bitmap) {
            a(str, str, "image/png");
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, this.f1010a);
            b("", new Object[0]);
            a();
            if (this.b != null) {
                this.b.a("    " + str, (Object) "<Image>");
            }
        }

        public void a(String str, byte[] bArr) {
            a(str, str, "content/unknown");
            this.f1010a.write(bArr);
            b("", new Object[0]);
            a();
            if (this.b != null) {
                this.b.a("    " + str, (Object) String.format(Locale.ROOT, "<Data: %d>", Integer.valueOf(bArr.length)));
            }
        }

        public void a(String str, Uri uri, String str2) {
            int a2;
            if (str2 == null) {
                str2 = "content/unknown";
            }
            a(str, str, str2);
            if (this.f1010a instanceof x) {
                ((x) this.f1010a).a(u.d(uri));
                a2 = 0;
            } else {
                a2 = u.a(k.f().getContentResolver().openInputStream(uri), this.f1010a) + 0;
            }
            b("", new Object[0]);
            a();
            if (this.b != null) {
                this.b.a("    " + str, (Object) String.format(Locale.ROOT, "<Data: %d>", Integer.valueOf(a2)));
            }
        }

        public void a(String str, ParcelFileDescriptor parcelFileDescriptor, String str2) {
            int a2;
            if (str2 == null) {
                str2 = "content/unknown";
            }
            a(str, str, str2);
            if (this.f1010a instanceof x) {
                ((x) this.f1010a).a(parcelFileDescriptor.getStatSize());
                a2 = 0;
            } else {
                a2 = u.a((InputStream) new ParcelFileDescriptor.AutoCloseInputStream(parcelFileDescriptor), this.f1010a) + 0;
            }
            b("", new Object[0]);
            a();
            if (this.b != null) {
                this.b.a("    " + str, (Object) String.format(Locale.ROOT, "<Data: %d>", Integer.valueOf(a2)));
            }
        }

        public void a() {
            if (!this.d) {
                b("--%s", GraphRequest.b);
                return;
            }
            this.f1010a.write("&".getBytes());
        }

        public void a(String str, String str2, String str3) {
            if (!this.d) {
                a("Content-Disposition: form-data; name=\"%s\"", str);
                if (str2 != null) {
                    a("; filename=\"%s\"", str2);
                }
                b("", new Object[0]);
                if (str3 != null) {
                    b("%s: %s", "Content-Type", str3);
                }
                b("", new Object[0]);
                return;
            }
            this.f1010a.write(String.format("%s=", str).getBytes());
        }

        public void a(String str, Object... objArr) {
            if (!this.d) {
                if (this.c) {
                    this.f1010a.write("--".getBytes());
                    this.f1010a.write(GraphRequest.b.getBytes());
                    this.f1010a.write("\r\n".getBytes());
                    this.c = false;
                }
                this.f1010a.write(String.format(str, objArr).getBytes());
                return;
            }
            this.f1010a.write(URLEncoder.encode(String.format(Locale.US, str, objArr), C.UTF8_NAME).getBytes());
        }

        public void b(String str, Object... objArr) {
            a(str, objArr);
            if (!this.d) {
                a("\r\n", new Object[0]);
            }
        }
    }

    public static class ParcelableResourceWithMimeType<RESOURCE extends Parcelable> implements Parcelable {
        public static final Parcelable.Creator<ParcelableResourceWithMimeType> CREATOR = new Parcelable.Creator<ParcelableResourceWithMimeType>() {
            /* class com.facebook.GraphRequest.ParcelableResourceWithMimeType.AnonymousClass1 */

            /* renamed from: a */
            public ParcelableResourceWithMimeType createFromParcel(Parcel parcel) {
                return new ParcelableResourceWithMimeType(parcel, (AnonymousClass1) null);
            }

            /* renamed from: a */
            public ParcelableResourceWithMimeType[] newArray(int i) {
                return new ParcelableResourceWithMimeType[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private final String f1008a;
        private final RESOURCE b;

        /* synthetic */ ParcelableResourceWithMimeType(Parcel parcel, AnonymousClass1 r2) {
            this(parcel);
        }

        public String a() {
            return this.f1008a;
        }

        public RESOURCE b() {
            return this.b;
        }

        public int describeContents() {
            return 1;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f1008a);
            parcel.writeParcelable(this.b, i);
        }

        public ParcelableResourceWithMimeType(RESOURCE resource, String str) {
            this.f1008a = str;
            this.b = resource;
        }

        private ParcelableResourceWithMimeType(Parcel parcel) {
            this.f1008a = parcel.readString();
            this.b = (RESOURCE) parcel.readParcelable(k.f().getClassLoader());
        }
    }
}
