package com.facebook;

import android.content.SharedPreferences;
import com.facebook.internal.v;
import com.google.android.gms.common.Scopes;
import org.json.JSONException;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public final class u {

    /* renamed from: a  reason: collision with root package name */
    private final SharedPreferences f1232a = k.f().getSharedPreferences("com.facebook.AccessTokenManager.SharedPreferences", 0);

    u() {
    }

    /* access modifiers changed from: package-private */
    public Profile a() {
        String string = this.f1232a.getString("com.facebook.ProfileManager.CachedProfile", null);
        if (string != null) {
            try {
                return new Profile(new JSONObject(string));
            } catch (JSONException e) {
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(Profile profile) {
        v.a(profile, Scopes.PROFILE);
        JSONObject e = profile.e();
        if (e != null) {
            this.f1232a.edit().putString("com.facebook.ProfileManager.CachedProfile", e.toString()).apply();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f1232a.edit().remove("com.facebook.ProfileManager.CachedProfile").apply();
    }
}
