package com.facebook;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.facebook.common.R;
import com.facebook.internal.f;
import com.facebook.internal.p;
import com.facebook.internal.u;
import com.facebook.login.d;
import com.facebook.share.internal.DeviceShareDialogFragment;
import com.facebook.share.model.ShareContent;

public class FacebookActivity extends FragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    public static String f999a = "PassThrough";
    private static String b = "SingleFragment";
    private static final String c = FacebookActivity.class.getName();
    private Fragment d;

    @Override // android.support.v4.app.SupportActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (!k.a()) {
            u.a(c, "Facebook SDK not initialized. Make sure you call sdkInitialize inside your Application's onCreate method.");
            k.a(getApplicationContext());
        }
        setContentView(R.layout.com_facebook_activity_layout);
        if (f999a.equals(intent.getAction())) {
            c();
        } else {
            this.d = a();
        }
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        Intent intent = getIntent();
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        Fragment findFragmentByTag = supportFragmentManager.findFragmentByTag(b);
        if (findFragmentByTag != null) {
            return findFragmentByTag;
        }
        if ("FacebookDialogFragment".equals(intent.getAction())) {
            f fVar = new f();
            fVar.setRetainInstance(true);
            fVar.show(supportFragmentManager, b);
            return fVar;
        } else if ("DeviceShareDialogFragment".equals(intent.getAction())) {
            DeviceShareDialogFragment deviceShareDialogFragment = new DeviceShareDialogFragment();
            deviceShareDialogFragment.setRetainInstance(true);
            deviceShareDialogFragment.a((ShareContent) intent.getParcelableExtra("content"));
            deviceShareDialogFragment.show(supportFragmentManager, b);
            return deviceShareDialogFragment;
        } else {
            d dVar = new d();
            dVar.setRetainInstance(true);
            supportFragmentManager.beginTransaction().add(R.id.com_facebook_fragment_container, dVar, b).commit();
            return dVar;
        }
    }

    @Override // android.support.v4.app.FragmentActivity
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.d != null) {
            this.d.onConfigurationChanged(configuration);
        }
    }

    public Fragment b() {
        return this.d;
    }

    private void c() {
        setResult(0, p.a(getIntent(), (Bundle) null, p.a(p.c(getIntent()))));
        finish();
    }
}
