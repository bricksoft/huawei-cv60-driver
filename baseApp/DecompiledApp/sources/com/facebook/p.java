package com.facebook;

import android.util.Log;
import com.facebook.internal.o;
import com.facebook.internal.u;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class p {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1183a = p.class.getSimpleName();
    private final HttpURLConnection b;
    private final JSONObject c;
    private final JSONArray d;
    private final FacebookRequestError e;
    private final String f;
    private final GraphRequest g;

    p(GraphRequest graphRequest, HttpURLConnection httpURLConnection, String str, JSONObject jSONObject) {
        this(graphRequest, httpURLConnection, str, jSONObject, null, null);
    }

    p(GraphRequest graphRequest, HttpURLConnection httpURLConnection, String str, JSONArray jSONArray) {
        this(graphRequest, httpURLConnection, str, null, jSONArray, null);
    }

    p(GraphRequest graphRequest, HttpURLConnection httpURLConnection, FacebookRequestError facebookRequestError) {
        this(graphRequest, httpURLConnection, null, null, null, facebookRequestError);
    }

    p(GraphRequest graphRequest, HttpURLConnection httpURLConnection, String str, JSONObject jSONObject, JSONArray jSONArray, FacebookRequestError facebookRequestError) {
        this.g = graphRequest;
        this.b = httpURLConnection;
        this.f = str;
        this.c = jSONObject;
        this.d = jSONArray;
        this.e = facebookRequestError;
    }

    public final FacebookRequestError a() {
        return this.e;
    }

    public final JSONObject b() {
        return this.c;
    }

    public String toString() {
        String str;
        try {
            Locale locale = Locale.US;
            Object[] objArr = new Object[1];
            objArr[0] = Integer.valueOf(this.b != null ? this.b.getResponseCode() : 200);
            str = String.format(locale, "%d", objArr);
        } catch (IOException e2) {
            str = "unknown";
        }
        return "{Response: " + " responseCode: " + str + ", graphObject: " + this.c + ", error: " + this.e + "}";
    }

    static List<p> a(HttpURLConnection httpURLConnection, o oVar) {
        InputStream inputStream;
        Closeable closeable = null;
        try {
            if (httpURLConnection.getResponseCode() >= 400) {
                inputStream = httpURLConnection.getErrorStream();
            } else {
                inputStream = httpURLConnection.getInputStream();
            }
            return a(closeable, httpURLConnection, oVar);
        } catch (h e2) {
            o.a(t.REQUESTS, "Response", "Response <Error>: %s", e2);
            return a(oVar, httpURLConnection, e2);
        } catch (Exception e3) {
            o.a(t.REQUESTS, "Response", "Response <Error>: %s", e3);
            return a(oVar, httpURLConnection, new h(e3));
        } finally {
            u.a(closeable);
        }
    }

    static List<p> a(InputStream inputStream, HttpURLConnection httpURLConnection, o oVar) {
        String a2 = u.a(inputStream);
        o.a(t.INCLUDE_RAW_RESPONSES, "Response", "Response (raw)\n  Size: %d\n  Response:\n%s\n", Integer.valueOf(a2.length()), a2);
        return a(a2, httpURLConnection, oVar);
    }

    static List<p> a(String str, HttpURLConnection httpURLConnection, o oVar) {
        List<p> a2 = a(httpURLConnection, oVar, new JSONTokener(str).nextValue());
        o.a(t.REQUESTS, "Response", "Response\n  Id: %s\n  Size: %d\n  Responses:\n%s\n", oVar.b(), Integer.valueOf(str.length()), a2);
        return a2;
    }

    private static List<p> a(HttpURLConnection httpURLConnection, List<GraphRequest> list, Object obj) {
        JSONArray jSONArray;
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        if (size == 1) {
            GraphRequest graphRequest = list.get(0);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(TtmlNode.TAG_BODY, obj);
                jSONObject.put("code", httpURLConnection != null ? httpURLConnection.getResponseCode() : 200);
                JSONArray jSONArray2 = new JSONArray();
                jSONArray2.put(jSONObject);
                jSONArray = jSONArray2;
            } catch (JSONException e2) {
                arrayList.add(new p(graphRequest, httpURLConnection, new FacebookRequestError(httpURLConnection, e2)));
                jSONArray = obj;
            } catch (IOException e3) {
                arrayList.add(new p(graphRequest, httpURLConnection, new FacebookRequestError(httpURLConnection, e3)));
            }
            if ((jSONArray instanceof JSONArray) || ((JSONArray) jSONArray).length() != size) {
                throw new h("Unexpected number of results");
            }
            JSONArray jSONArray3 = (JSONArray) jSONArray;
            for (int i = 0; i < jSONArray3.length(); i++) {
                GraphRequest graphRequest2 = list.get(i);
                try {
                    arrayList.add(a(graphRequest2, httpURLConnection, jSONArray3.get(i), obj));
                } catch (JSONException e4) {
                    arrayList.add(new p(graphRequest2, httpURLConnection, new FacebookRequestError(httpURLConnection, e4)));
                } catch (h e5) {
                    arrayList.add(new p(graphRequest2, httpURLConnection, new FacebookRequestError(httpURLConnection, e5)));
                }
            }
            return arrayList;
        }
        jSONArray = obj;
        if (jSONArray instanceof JSONArray) {
        }
        throw new h("Unexpected number of results");
    }

    private static p a(GraphRequest graphRequest, HttpURLConnection httpURLConnection, Object obj, Object obj2) {
        if (obj instanceof JSONObject) {
            JSONObject jSONObject = (JSONObject) obj;
            FacebookRequestError a2 = FacebookRequestError.a(jSONObject, obj2, httpURLConnection);
            if (a2 != null) {
                Log.e(f1183a, a2.toString());
                if (a2.b() == 190 && u.a(graphRequest.f())) {
                    if (a2.c() != 493) {
                        AccessToken.a((AccessToken) null);
                    } else if (!AccessToken.a().m()) {
                        AccessToken.c();
                    }
                }
                return new p(graphRequest, httpURLConnection, a2);
            }
            Object a3 = u.a(jSONObject, TtmlNode.TAG_BODY, "FACEBOOK_NON_JSON_RESULT");
            if (a3 instanceof JSONObject) {
                return new p(graphRequest, httpURLConnection, a3.toString(), (JSONObject) a3);
            }
            if (a3 instanceof JSONArray) {
                return new p(graphRequest, httpURLConnection, a3.toString(), (JSONArray) a3);
            }
            obj = JSONObject.NULL;
        }
        if (obj == JSONObject.NULL) {
            return new p(graphRequest, httpURLConnection, obj.toString(), (JSONObject) null);
        }
        throw new h("Got unexpected object type in response, class: " + obj.getClass().getSimpleName());
    }

    static List<p> a(List<GraphRequest> list, HttpURLConnection httpURLConnection, h hVar) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            arrayList.add(new p(list.get(i), httpURLConnection, new FacebookRequestError(httpURLConnection, hVar)));
        }
        return arrayList;
    }
}
