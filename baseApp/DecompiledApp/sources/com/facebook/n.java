package com.facebook;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.facebook.internal.u;
import java.net.HttpURLConnection;
import java.util.List;

public class n extends AsyncTask<Void, Void, List<p>> {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1181a = n.class.getCanonicalName();
    private final HttpURLConnection b;
    private final o c;
    private Exception d;

    public n(o oVar) {
        this(null, oVar);
    }

    public n(HttpURLConnection httpURLConnection, o oVar) {
        this.c = oVar;
        this.b = httpURLConnection;
    }

    public String toString() {
        return "{RequestAsyncTask: " + " connection: " + this.b + ", requests: " + this.c + "}";
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        Handler handler;
        super.onPreExecute();
        if (k.b()) {
            u.a(f1181a, String.format("execute async task: %s", this));
        }
        if (this.c.c() == null) {
            if (Thread.currentThread() instanceof HandlerThread) {
                handler = new Handler();
            } else {
                handler = new Handler(Looper.getMainLooper());
            }
            this.c.a(handler);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(List<p> list) {
        super.onPostExecute(list);
        if (this.d != null) {
            u.a(f1181a, String.format("onPostExecute: exception encountered during request: %s", this.d.getMessage()));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public List<p> doInBackground(Void... voidArr) {
        try {
            if (this.b == null) {
                return this.c.g();
            }
            return GraphRequest.a(this.b, this.c);
        } catch (Exception e) {
            this.d = e;
            return null;
        }
    }
}
