package com.facebook;

import android.content.SharedPreferences;
import android.os.Bundle;
import com.facebook.internal.v;
import org.json.JSONException;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private final SharedPreferences f1012a;
    private final C0042a b;
    private s c;

    a(SharedPreferences sharedPreferences, C0042a aVar) {
        this.f1012a = sharedPreferences;
        this.b = aVar;
    }

    public a() {
        this(k.f().getSharedPreferences("com.facebook.AccessTokenManager.SharedPreferences", 0), new C0042a());
    }

    public AccessToken a() {
        if (c()) {
            return d();
        }
        if (!e()) {
            return null;
        }
        AccessToken f = f();
        if (f == null) {
            return f;
        }
        a(f);
        g().b();
        return f;
    }

    public void a(AccessToken accessToken) {
        v.a(accessToken, "accessToken");
        try {
            this.f1012a.edit().putString("com.facebook.AccessTokenManager.CachedAccessToken", accessToken.n().toString()).apply();
        } catch (JSONException e) {
        }
    }

    public void b() {
        this.f1012a.edit().remove("com.facebook.AccessTokenManager.CachedAccessToken").apply();
        if (e()) {
            g().b();
        }
    }

    private boolean c() {
        return this.f1012a.contains("com.facebook.AccessTokenManager.CachedAccessToken");
    }

    private AccessToken d() {
        String string = this.f1012a.getString("com.facebook.AccessTokenManager.CachedAccessToken", null);
        if (string == null) {
            return null;
        }
        try {
            return AccessToken.a(new JSONObject(string));
        } catch (JSONException e) {
            return null;
        }
    }

    private boolean e() {
        return k.c();
    }

    private AccessToken f() {
        Bundle a2 = g().a();
        if (a2 == null || !s.a(a2)) {
            return null;
        }
        return AccessToken.a(a2);
    }

    private s g() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = this.b.a();
                }
            }
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.facebook.a$a  reason: collision with other inner class name */
    public static class C0042a {
        C0042a() {
        }

        public s a() {
            return new s(k.f());
        }
    }
}
