package com.facebook;

import android.os.Handler;
import com.facebook.o;
import java.io.FilterOutputStream;
import java.io.OutputStream;
import java.util.Map;

/* access modifiers changed from: package-private */
public class y extends FilterOutputStream implements z {

    /* renamed from: a  reason: collision with root package name */
    private final Map<GraphRequest, aa> f1237a;
    private final o b;
    private final long c = k.i();
    private long d;
    private long e;
    private long f;
    private aa g;

    y(OutputStream outputStream, o oVar, Map<GraphRequest, aa> map, long j) {
        super(outputStream);
        this.b = oVar;
        this.f1237a = map;
        this.f = j;
    }

    private void a(long j) {
        if (this.g != null) {
            this.g.a(j);
        }
        this.d += j;
        if (this.d >= this.e + this.c || this.d >= this.f) {
            a();
        }
    }

    private void a() {
        if (this.d > this.e) {
            for (o.a aVar : this.b.e()) {
                if (aVar instanceof o.b) {
                    Handler c2 = this.b.c();
                    final o.b bVar = (o.b) aVar;
                    if (c2 == null) {
                        bVar.a(this.b, this.d, this.f);
                    } else {
                        c2.post(new Runnable() {
                            /* class com.facebook.y.AnonymousClass1 */

                            public void run() {
                                bVar.a(y.this.b, y.this.d, y.this.f);
                            }
                        });
                    }
                }
            }
            this.e = this.d;
        }
    }

    @Override // com.facebook.z
    public void a(GraphRequest graphRequest) {
        this.g = graphRequest != null ? this.f1237a.get(graphRequest) : null;
    }

    @Override // java.io.OutputStream, java.io.FilterOutputStream
    public void write(byte[] bArr) {
        this.out.write(bArr);
        a((long) bArr.length);
    }

    @Override // java.io.OutputStream, java.io.FilterOutputStream
    public void write(byte[] bArr, int i, int i2) {
        this.out.write(bArr, i, i2);
        a((long) i2);
    }

    @Override // java.io.OutputStream, java.io.FilterOutputStream
    public void write(int i) {
        this.out.write(i);
        a(1);
    }

    @Override // java.io.OutputStream, java.io.Closeable, java.io.FilterOutputStream, java.lang.AutoCloseable
    public void close() {
        super.close();
        for (aa aaVar : this.f1237a.values()) {
            aaVar.a();
        }
        a();
    }
}
