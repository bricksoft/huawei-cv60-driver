package com.facebook;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.internal.g;
import com.facebook.internal.k;
import com.facebook.internal.l;
import com.facebook.internal.u;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import java.net.HttpURLConnection;
import org.json.JSONException;
import org.json.JSONObject;
import tv.danmaku.ijk.media.player.IjkMediaMeta;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public final class FacebookRequestError implements Parcelable {
    public static final Parcelable.Creator<FacebookRequestError> CREATOR = new Parcelable.Creator<FacebookRequestError>() {
        /* class com.facebook.FacebookRequestError.AnonymousClass1 */

        /* renamed from: a */
        public FacebookRequestError createFromParcel(Parcel parcel) {
            return new FacebookRequestError(parcel);
        }

        /* renamed from: a */
        public FacebookRequestError[] newArray(int i) {
            return new FacebookRequestError[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    static final b f1000a = new b(200, 299);
    private final a b;
    private final int c;
    private final int d;
    private final int e;
    private final String f;
    private final String g;
    private final String h;
    private final String i;
    private final String j;
    private final JSONObject k;
    private final JSONObject l;
    private final Object m;
    private final HttpURLConnection n;
    private final h o;

    public enum a {
        LOGIN_RECOVERABLE,
        OTHER,
        TRANSIENT
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        private final int f1002a;
        private final int b;

        private b(int i, int i2) {
            this.f1002a = i;
            this.b = i2;
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i) {
            return this.f1002a <= i && i <= this.b;
        }
    }

    private FacebookRequestError(int i2, int i3, int i4, String str, String str2, String str3, String str4, boolean z, JSONObject jSONObject, JSONObject jSONObject2, Object obj, HttpURLConnection httpURLConnection, h hVar) {
        a a2;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = str;
        this.g = str2;
        this.l = jSONObject;
        this.k = jSONObject2;
        this.m = obj;
        this.n = httpURLConnection;
        this.h = str3;
        this.i = str4;
        boolean z2 = false;
        if (hVar != null) {
            this.o = hVar;
            z2 = true;
        } else {
            this.o = new m(this, str2);
        }
        g g2 = g();
        if (z2) {
            a2 = a.OTHER;
        } else {
            a2 = g2.a(i3, i4, z);
        }
        this.b = a2;
        this.j = g2.a(this.b);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    FacebookRequestError(java.net.HttpURLConnection r16, java.lang.Exception r17) {
        /*
            r15 = this;
            r2 = -1
            r3 = -1
            r4 = -1
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r0 = r17
            boolean r1 = r0 instanceof com.facebook.h
            if (r1 == 0) goto L_0x001c
            com.facebook.h r17 = (com.facebook.h) r17
            r14 = r17
        L_0x0015:
            r1 = r15
            r13 = r16
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            return
        L_0x001c:
            com.facebook.h r14 = new com.facebook.h
            r0 = r17
            r14.<init>(r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.FacebookRequestError.<init>(java.net.HttpURLConnection, java.lang.Exception):void");
    }

    public FacebookRequestError(int i2, String str, String str2) {
        this(-1, i2, -1, str, str2, null, null, false, null, null, null, null, null);
    }

    public int a() {
        return this.c;
    }

    public int b() {
        return this.d;
    }

    public int c() {
        return this.e;
    }

    public String d() {
        return this.f;
    }

    public String e() {
        if (this.g != null) {
            return this.g;
        }
        return this.o.getLocalizedMessage();
    }

    public h f() {
        return this.o;
    }

    public String toString() {
        return "{HttpStatus: " + this.c + ", errorCode: " + this.d + ", subErrorCode: " + this.e + ", errorType: " + this.f + ", errorMessage: " + e() + "}";
    }

    static FacebookRequestError a(JSONObject jSONObject, Object obj, HttpURLConnection httpURLConnection) {
        JSONObject jSONObject2;
        try {
            if (jSONObject.has("code")) {
                int i2 = jSONObject.getInt("code");
                Object a2 = u.a(jSONObject, TtmlNode.TAG_BODY, "FACEBOOK_NON_JSON_RESULT");
                if (a2 != null && (a2 instanceof JSONObject)) {
                    JSONObject jSONObject3 = (JSONObject) a2;
                    String str = null;
                    String str2 = null;
                    String str3 = null;
                    String str4 = null;
                    boolean z = false;
                    int i3 = -1;
                    int i4 = -1;
                    boolean z2 = false;
                    if (jSONObject3.has(IjkMediaPlayer.OnNativeInvokeListener.ARG_ERROR)) {
                        JSONObject jSONObject4 = (JSONObject) u.a(jSONObject3, IjkMediaPlayer.OnNativeInvokeListener.ARG_ERROR, (String) null);
                        str = jSONObject4.optString(IjkMediaMeta.IJKM_KEY_TYPE, null);
                        str2 = jSONObject4.optString("message", null);
                        i3 = jSONObject4.optInt("code", -1);
                        i4 = jSONObject4.optInt("error_subcode", -1);
                        str3 = jSONObject4.optString("error_user_msg", null);
                        str4 = jSONObject4.optString("error_user_title", null);
                        z = jSONObject4.optBoolean("is_transient", false);
                        z2 = true;
                    } else if (jSONObject3.has("error_code") || jSONObject3.has("error_msg") || jSONObject3.has("error_reason")) {
                        str = jSONObject3.optString("error_reason", null);
                        str2 = jSONObject3.optString("error_msg", null);
                        i3 = jSONObject3.optInt("error_code", -1);
                        i4 = jSONObject3.optInt("error_subcode", -1);
                        z2 = true;
                    }
                    if (z2) {
                        return new FacebookRequestError(i2, i3, i4, str, str2, str4, str3, z, jSONObject3, jSONObject, obj, httpURLConnection, null);
                    }
                }
                if (!f1000a.a(i2)) {
                    if (jSONObject.has(TtmlNode.TAG_BODY)) {
                        jSONObject2 = (JSONObject) u.a(jSONObject, TtmlNode.TAG_BODY, "FACEBOOK_NON_JSON_RESULT");
                    } else {
                        jSONObject2 = null;
                    }
                    return new FacebookRequestError(i2, -1, -1, null, null, null, null, false, jSONObject2, jSONObject, obj, httpURLConnection, null);
                }
            }
        } catch (JSONException e2) {
        }
        return null;
    }

    static synchronized g g() {
        g f2;
        synchronized (FacebookRequestError.class) {
            k a2 = l.a(k.j());
            if (a2 == null) {
                f2 = g.a();
            } else {
                f2 = a2.f();
            }
        }
        return f2;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
    }

    private FacebookRequestError(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), false, null, null, null, null, null);
    }

    public int describeContents() {
        return 0;
    }
}
