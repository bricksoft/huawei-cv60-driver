package com.facebook;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

public class CustomTabActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static final String f995a = (CustomTabActivity.class.getSimpleName() + ".action_customTabRedirect");
    public static final String b = (CustomTabActivity.class.getSimpleName() + ".action_destroy");
    private BroadcastReceiver c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = new Intent(this, CustomTabMainActivity.class);
        intent.setAction(f995a);
        intent.putExtra(CustomTabMainActivity.c, getIntent().getDataString());
        intent.addFlags(603979776);
        startActivityForResult(intent, 2);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == 0) {
            Intent intent2 = new Intent(f995a);
            intent2.putExtra(CustomTabMainActivity.c, getIntent().getDataString());
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
            this.c = new BroadcastReceiver() {
                /* class com.facebook.CustomTabActivity.AnonymousClass1 */

                public void onReceive(Context context, Intent intent) {
                    CustomTabActivity.this.finish();
                }
            };
            LocalBroadcastManager.getInstance(this).registerReceiver(this.c, new IntentFilter(b));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.c);
        super.onDestroy();
    }
}
