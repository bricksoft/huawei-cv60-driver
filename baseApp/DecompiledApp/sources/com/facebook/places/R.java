package com.facebook.places;

public final class R {

    public static final class attr {
        public static final int font = 2130772297;
        public static final int fontProviderAuthority = 2130772290;
        public static final int fontProviderCerts = 2130772293;
        public static final int fontProviderFetchStrategy = 2130772294;
        public static final int fontProviderFetchTimeout = 2130772295;
        public static final int fontProviderPackage = 2130772291;
        public static final int fontProviderQuery = 2130772292;
        public static final int fontStyle = 2130772296;
        public static final int fontWeight = 2130772298;
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2131558400;
    }

    public static final class color {
        public static final int notification_action_color_filter = 2131689472;
        public static final int notification_icon_bg_color = 2131689703;
        public static final int ripple_material_light = 2131689732;
        public static final int secondary_text_default_material_light = 2131689741;
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131427436;
        public static final int compat_button_inset_vertical_material = 2131427437;
        public static final int compat_button_padding_horizontal_material = 2131427438;
        public static final int compat_button_padding_vertical_material = 2131427439;
        public static final int compat_control_corner_material = 2131427440;
        public static final int notification_action_icon_size = 2131427542;
        public static final int notification_action_text_size = 2131427543;
        public static final int notification_big_circle_margin = 2131427544;
        public static final int notification_content_margin_start = 2131427353;
        public static final int notification_large_icon_height = 2131427545;
        public static final int notification_large_icon_width = 2131427546;
        public static final int notification_main_column_padding_top = 2131427354;
        public static final int notification_media_narrow_margin = 2131427355;
        public static final int notification_right_icon_size = 2131427547;
        public static final int notification_right_side_padding_top = 2131427351;
        public static final int notification_small_icon_background_padding = 2131427548;
        public static final int notification_small_icon_size_as_large = 2131427549;
        public static final int notification_subtext_size = 2131427550;
        public static final int notification_top_pad = 2131427551;
        public static final int notification_top_pad_large_text = 2131427552;
    }

    public static final class drawable {
        public static final int notification_action_background = 2130838147;
        public static final int notification_bg = 2130838148;
        public static final int notification_bg_low = 2130838149;
        public static final int notification_bg_low_normal = 2130838150;
        public static final int notification_bg_low_pressed = 2130838151;
        public static final int notification_bg_normal = 2130838152;
        public static final int notification_bg_normal_pressed = 2130838153;
        public static final int notification_icon_background = 2130838154;
        public static final int notification_template_icon_bg = 2130838374;
        public static final int notification_template_icon_low_bg = 2130838375;
        public static final int notification_tile_bg = 2130838155;
        public static final int notify_panel_notification_icon_bg = 2130838156;
    }

    public static final class id {
        public static final int action_container = 2131755236;
        public static final int action_divider = 2131755243;
        public static final int action_image = 2131755237;
        public static final int action_text = 2131755238;
        public static final int actions = 2131755251;
        public static final int async = 2131755090;
        public static final int blocking = 2131755091;
        public static final int chronometer = 2131755248;
        public static final int forever = 2131755092;
        public static final int icon = 2131755139;
        public static final int icon_group = 2131755252;
        public static final int info = 2131755249;
        public static final int italic = 2131755093;
        public static final int line1 = 2131755015;
        public static final int line3 = 2131755016;
        public static final int normal = 2131755045;
        public static final int notification_background = 2131755250;
        public static final int notification_main_column = 2131755245;
        public static final int notification_main_column_container = 2131755244;
        public static final int right_icon = 2131755253;
        public static final int right_side = 2131755246;
        public static final int tag_transition_group = 2131755028;
        public static final int text = 2131755031;
        public static final int text2 = 2131755032;
        public static final int time = 2131755247;
        public static final int title = 2131755036;
    }

    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 2131623951;
    }

    public static final class layout {
        public static final int notification_action = 2130968639;
        public static final int notification_action_tombstone = 2130968640;
        public static final int notification_template_custom_big = 2130968647;
        public static final int notification_template_icon_group = 2130968648;
        public static final int notification_template_part_chronometer = 2130968652;
        public static final int notification_template_part_time = 2130968653;
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131361860;
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131492986;
        public static final int TextAppearance_Compat_Notification_Info = 2131492987;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131493186;
        public static final int TextAppearance_Compat_Notification_Time = 2131492990;
        public static final int TextAppearance_Compat_Notification_Title = 2131492992;
        public static final int Widget_Compat_NotificationActionContainer = 2131492998;
        public static final int Widget_Compat_NotificationActionText = 2131492999;
    }

    public static final class styleable {
        public static final int[] FontFamily = {com.huawei.cvIntl60.R.attr.fontProviderAuthority, com.huawei.cvIntl60.R.attr.fontProviderPackage, com.huawei.cvIntl60.R.attr.fontProviderQuery, com.huawei.cvIntl60.R.attr.fontProviderCerts, com.huawei.cvIntl60.R.attr.fontProviderFetchStrategy, com.huawei.cvIntl60.R.attr.fontProviderFetchTimeout};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.huawei.cvIntl60.R.attr.fontStyle, com.huawei.cvIntl60.R.attr.font, com.huawei.cvIntl60.R.attr.fontWeight, com.huawei.cvIntl60.R.attr.fontVariationSettings, com.huawei.cvIntl60.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_font = 6;
        public static final int FontFamilyFont_fontStyle = 5;
        public static final int FontFamilyFont_fontWeight = 7;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 3;
        public static final int FontFamily_fontProviderFetchStrategy = 4;
        public static final int FontFamily_fontProviderFetchTimeout = 5;
        public static final int FontFamily_fontProviderPackage = 1;
        public static final int FontFamily_fontProviderQuery = 2;
    }
}
