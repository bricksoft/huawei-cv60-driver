package com.facebook;

public class g extends h {
    static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    private int f1094a;
    private String b;

    public g(String str, int i, String str2) {
        super(str);
        this.f1094a = i;
        this.b = str2;
    }

    public int a() {
        return this.f1094a;
    }

    public String b() {
        return this.b;
    }

    @Override // com.facebook.h
    public final String toString() {
        return "{FacebookDialogException: " + "errorCode: " + a() + ", message: " + getMessage() + ", url: " + b() + "}";
    }
}
