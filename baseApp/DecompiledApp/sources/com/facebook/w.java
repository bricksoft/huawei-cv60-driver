package com.facebook;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import com.facebook.internal.v;

public abstract class w {

    /* renamed from: a  reason: collision with root package name */
    private final BroadcastReceiver f1234a;
    private final LocalBroadcastManager b;
    private boolean c = false;

    /* access modifiers changed from: protected */
    public abstract void a(Profile profile, Profile profile2);

    public w() {
        v.a();
        this.f1234a = new a();
        this.b = LocalBroadcastManager.getInstance(k.f());
        a();
    }

    public void a() {
        if (!this.c) {
            c();
            this.c = true;
        }
    }

    public void b() {
        if (this.c) {
            this.b.unregisterReceiver(this.f1234a);
            this.c = false;
        }
    }

    private class a extends BroadcastReceiver {
        private a() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("com.facebook.sdk.ACTION_CURRENT_PROFILE_CHANGED".equals(intent.getAction())) {
                w.this.a((Profile) intent.getParcelableExtra("com.facebook.sdk.EXTRA_OLD_PROFILE"), (Profile) intent.getParcelableExtra("com.facebook.sdk.EXTRA_NEW_PROFILE"));
            }
        }
    }

    private void c() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.facebook.sdk.ACTION_CURRENT_PROFILE_CHANGED");
        this.b.registerReceiver(this.f1234a, intentFilter);
    }
}
