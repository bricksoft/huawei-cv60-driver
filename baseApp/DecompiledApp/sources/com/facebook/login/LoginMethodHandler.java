package com.facebook.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.a.g;
import com.facebook.c;
import com.facebook.h;
import com.facebook.internal.u;
import com.facebook.login.LoginClient;
import com.google.android.exoplayer.C;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public abstract class LoginMethodHandler implements Parcelable {

    /* renamed from: a  reason: collision with root package name */
    Map<String, String> f1165a;
    protected LoginClient b;

    /* access modifiers changed from: package-private */
    public abstract String a();

    /* access modifiers changed from: package-private */
    public abstract boolean a(LoginClient.Request request);

    LoginMethodHandler(LoginClient loginClient) {
        this.b = loginClient;
    }

    LoginMethodHandler(Parcel parcel) {
        this.f1165a = u.a(parcel);
    }

    /* access modifiers changed from: package-private */
    public void a(LoginClient loginClient) {
        if (this.b != null) {
            throw new h("Can't set LoginClient if it is already set.");
        }
        this.b = loginClient;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, Intent intent) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void b() {
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("0_auth_logger_id", str);
            jSONObject.put("3_method", a());
            a(jSONObject);
        } catch (JSONException e) {
            Log.w("LoginMethodHandler", "Error creating client state json: " + e.getMessage());
        }
        return jSONObject.toString();
    }

    /* access modifiers changed from: protected */
    public void a(String str, Object obj) {
        if (this.f1165a == null) {
            this.f1165a = new HashMap();
        }
        this.f1165a.put(str, obj == null ? null : obj.toString());
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        String d = this.b.c().d();
        g b2 = g.b(this.b.b(), d);
        Bundle bundle = new Bundle();
        bundle.putString("fb_web_login_e2e", str);
        bundle.putLong("fb_web_login_switchback_time", System.currentTimeMillis());
        bundle.putString("app_id", d);
        b2.a("fb_dialogs_web_login_dialog_complete", (Double) null, bundle);
    }

    static AccessToken a(Bundle bundle, c cVar, String str) {
        Date a2 = u.a(bundle, "com.facebook.platform.extra.EXPIRES_SECONDS_SINCE_EPOCH", new Date(0));
        ArrayList<String> stringArrayList = bundle.getStringArrayList("com.facebook.platform.extra.PERMISSIONS");
        String string = bundle.getString("com.facebook.platform.extra.ACCESS_TOKEN");
        Date a3 = u.a(bundle, "com.facebook.platform.extra.EXTRA_DATA_ACCESS_EXPIRATION_TIME", new Date(0));
        if (u.a(string)) {
            return null;
        }
        return new AccessToken(string, str, bundle.getString("com.facebook.platform.extra.USER_ID"), stringArrayList, null, cVar, a2, new Date(), a3);
    }

    public static AccessToken a(Collection<String> collection, Bundle bundle, c cVar, String str) {
        Collection<String> collection2;
        ArrayList arrayList;
        Date a2 = u.a(bundle, "expires_in", new Date());
        String string = bundle.getString("access_token");
        Date a3 = u.a(bundle, "data_access_expiration_time", new Date(0));
        String string2 = bundle.getString("granted_scopes");
        if (!u.a(string2)) {
            collection2 = new ArrayList<>(Arrays.asList(string2.split(",")));
        } else {
            collection2 = collection;
        }
        String string3 = bundle.getString("denied_scopes");
        if (!u.a(string3)) {
            arrayList = new ArrayList(Arrays.asList(string3.split(",")));
        } else {
            arrayList = null;
        }
        if (u.a(string)) {
            return null;
        }
        return new AccessToken(string, str, c(bundle.getString("signed_request")), collection2, arrayList, cVar, a2, new Date(), a3);
    }

    static String c(String str) {
        if (str == null || str.isEmpty()) {
            throw new h("Authorization response does not contain the signed_request");
        }
        try {
            String[] split = str.split("\\.");
            if (split.length == 2) {
                return new JSONObject(new String(Base64.decode(split[1], 0), C.UTF8_NAME)).getString("user_id");
            }
        } catch (UnsupportedEncodingException | JSONException e) {
        }
        throw new h("Failed to retrieve user_id from signed_request");
    }

    public void writeToParcel(Parcel parcel, int i) {
        u.a(parcel, this.f1165a);
    }
}
