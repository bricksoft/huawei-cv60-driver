package com.facebook.login;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.facebook.AccessToken;
import com.facebook.FacebookActivity;
import com.facebook.Profile;
import com.facebook.d;
import com.facebook.e;
import com.facebook.h;
import com.facebook.internal.d;
import com.facebook.internal.v;
import com.facebook.k;
import com.facebook.login.LoginClient;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static final Set<String> f1174a = c();
    private static volatile f b;
    private c c = c.NATIVE_WITH_FALLBACK;
    private a d = a.FRIENDS;
    private final SharedPreferences e;
    private String f = "rerequest";

    f() {
        v.a();
        this.e = k.f().getSharedPreferences("com.facebook.loginManager", 0);
    }

    public static f a() {
        if (b == null) {
            synchronized (f.class) {
                if (b == null) {
                    b = new f();
                }
            }
        }
        return b;
    }

    public void a(d dVar, final com.facebook.f<g> fVar) {
        if (!(dVar instanceof com.facebook.internal.d)) {
            throw new h("Unexpected CallbackManager, please use the provided Factory.");
        }
        ((com.facebook.internal.d) dVar).b(d.b.Login.a(), new d.a() {
            /* class com.facebook.login.f.AnonymousClass1 */

            @Override // com.facebook.internal.d.a
            public boolean a(int i, Intent intent) {
                return f.this.a(i, intent, fVar);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, Intent intent) {
        return a(i, intent, null);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, Intent intent, com.facebook.f<g> fVar) {
        boolean z;
        AccessToken accessToken;
        h hVar;
        LoginClient.Request request;
        Map<String, String> map;
        LoginClient.Result.a aVar;
        AccessToken accessToken2;
        AccessToken accessToken3;
        h hVar2;
        h hVar3 = null;
        LoginClient.Result.a aVar2 = LoginClient.Result.a.ERROR;
        Map<String, String> map2 = null;
        LoginClient.Request request2 = null;
        boolean z2 = false;
        if (intent != null) {
            LoginClient.Result result = (LoginClient.Result) intent.getParcelableExtra("com.facebook.LoginFragment:Result");
            if (result != null) {
                LoginClient.Request request3 = result.e;
                LoginClient.Result.a aVar3 = result.f1163a;
                if (i == -1) {
                    if (result.f1163a == LoginClient.Result.a.SUCCESS) {
                        accessToken3 = result.b;
                        hVar2 = null;
                    } else {
                        hVar2 = new e(result.c);
                        accessToken3 = null;
                    }
                } else if (i == 0) {
                    z2 = true;
                    accessToken3 = null;
                    hVar2 = null;
                } else {
                    accessToken3 = null;
                    hVar2 = null;
                }
                Map<String, String> map3 = result.f;
                request = request3;
                map = map3;
                aVar = aVar3;
                accessToken2 = accessToken3;
                hVar3 = hVar2;
            } else {
                request = null;
                map = null;
                aVar = aVar2;
                accessToken2 = null;
            }
            z = z2;
            request2 = request;
            map2 = map;
            aVar2 = aVar;
            accessToken = accessToken2;
        } else if (i == 0) {
            aVar2 = LoginClient.Result.a.CANCEL;
            z = true;
            accessToken = null;
        } else {
            z = false;
            accessToken = null;
        }
        if (hVar3 == null && accessToken == null && !z) {
            hVar = new h("Unexpected call to LoginManager.onActivityResult");
        } else {
            hVar = hVar3;
        }
        a(null, aVar2, map2, hVar, true, request2);
        a(accessToken, request2, hVar, z, fVar);
        return true;
    }

    public f a(a aVar) {
        this.d = aVar;
        return this;
    }

    public void b() {
        AccessToken.a((AccessToken) null);
        Profile.a(null);
        a(false);
    }

    public void a(Activity activity, Collection<String> collection) {
        b(collection);
        a(new a(activity), a(collection));
    }

    private void b(Collection<String> collection) {
        if (collection != null) {
            for (String str : collection) {
                if (!a(str)) {
                    throw new h(String.format("Cannot pass a read permission (%s) to a request for publish authorization", str));
                }
            }
        }
    }

    static boolean a(String str) {
        return str != null && (str.startsWith("publish") || str.startsWith("manage") || f1174a.contains(str));
    }

    private static Set<String> c() {
        return Collections.unmodifiableSet(new HashSet<String>() {
            /* class com.facebook.login.f.AnonymousClass2 */

            {
                add("ads_management");
                add("create_event");
                add("rsvp_event");
            }
        });
    }

    /* access modifiers changed from: protected */
    public LoginClient.Request a(Collection<String> collection) {
        LoginClient.Request request = new LoginClient.Request(this.c, Collections.unmodifiableSet(collection != null ? new HashSet(collection) : new HashSet()), this.d, this.f, k.j(), UUID.randomUUID().toString());
        request.a(AccessToken.b());
        return request;
    }

    private void a(h hVar, LoginClient.Request request) {
        a(hVar.a(), request);
        com.facebook.internal.d.a(d.b.Login.a(), new d.a() {
            /* class com.facebook.login.f.AnonymousClass3 */

            @Override // com.facebook.internal.d.a
            public boolean a(int i, Intent intent) {
                return f.this.a(i, intent);
            }
        });
        if (!b(hVar, request)) {
            h hVar2 = new h("Log in attempt failed: FacebookActivity could not be started. Please make sure you added FacebookActivity to the AndroidManifest.");
            a(hVar.a(), LoginClient.Result.a.ERROR, null, hVar2, false, request);
            throw hVar2;
        }
    }

    private void a(Context context, LoginClient.Request request) {
        e b2 = b.b(context);
        if (b2 != null && request != null) {
            b2.a(request);
        }
    }

    private void a(Context context, LoginClient.Result.a aVar, Map<String, String> map, Exception exc, boolean z, LoginClient.Request request) {
        e b2 = b.b(context);
        if (b2 != null) {
            if (request == null) {
                b2.c("fb_mobile_login_complete", "Unexpected call to logCompleteLogin with null pendingAuthorizationRequest.");
                return;
            }
            HashMap hashMap = new HashMap();
            hashMap.put("try_login_activity", z ? "1" : "0");
            b2.a(request.e(), hashMap, aVar, map, exc);
        }
    }

    private boolean b(h hVar, LoginClient.Request request) {
        Intent a2 = a(request);
        if (!a(a2)) {
            return false;
        }
        try {
            hVar.a(a2, LoginClient.d());
            return true;
        } catch (ActivityNotFoundException e2) {
            return false;
        }
    }

    private boolean a(Intent intent) {
        if (k.f().getPackageManager().resolveActivity(intent, 0) != null) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public Intent a(LoginClient.Request request) {
        Intent intent = new Intent();
        intent.setClass(k.f(), FacebookActivity.class);
        intent.setAction(request.b().toString());
        Bundle bundle = new Bundle();
        bundle.putParcelable("request", request);
        intent.putExtra("com.facebook.LoginFragment:Request", bundle);
        return intent;
    }

    static g a(LoginClient.Request request, AccessToken accessToken) {
        Set<String> a2 = request.a();
        HashSet hashSet = new HashSet(accessToken.g());
        if (request.f()) {
            hashSet.retainAll(a2);
        }
        HashSet hashSet2 = new HashSet(a2);
        hashSet2.removeAll(hashSet);
        return new g(accessToken, hashSet, hashSet2);
    }

    private void a(AccessToken accessToken, LoginClient.Request request, h hVar, boolean z, com.facebook.f<g> fVar) {
        if (accessToken != null) {
            AccessToken.a(accessToken);
            Profile.b();
        }
        if (fVar != null) {
            g a2 = accessToken != null ? a(request, accessToken) : null;
            if (z || (a2 != null && a2.a().size() == 0)) {
                fVar.a();
            } else if (hVar != null) {
                fVar.a(hVar);
            } else if (accessToken != null) {
                a(true);
                fVar.a(a2);
            }
        }
    }

    private void a(boolean z) {
        SharedPreferences.Editor edit = this.e.edit();
        edit.putBoolean("express_login_allowed", z);
        edit.apply();
    }

    private static class a implements h {

        /* renamed from: a  reason: collision with root package name */
        private final Activity f1177a;

        a(Activity activity) {
            v.a(activity, "activity");
            this.f1177a = activity;
        }

        @Override // com.facebook.login.h
        public void a(Intent intent, int i) {
            this.f1177a.startActivityForResult(intent, i);
        }

        @Override // com.facebook.login.h
        public Activity a() {
            return this.f1177a;
        }
    }

    /* access modifiers changed from: private */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private static e f1178a;

        /* access modifiers changed from: private */
        public static synchronized e b(Context context) {
            e eVar;
            synchronized (b.class) {
                if (context == null) {
                    context = k.f();
                }
                if (context == null) {
                    eVar = null;
                } else {
                    if (f1178a == null) {
                        f1178a = new e(context, k.j());
                    }
                    eVar = f1178a;
                }
            }
            return eVar;
        }
    }
}
