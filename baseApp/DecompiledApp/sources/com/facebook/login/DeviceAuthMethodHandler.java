package com.facebook.login;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import com.facebook.AccessToken;
import com.facebook.c;
import com.facebook.login.LoginClient;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/* access modifiers changed from: package-private */
public class DeviceAuthMethodHandler extends LoginMethodHandler {
    public static final Parcelable.Creator<DeviceAuthMethodHandler> CREATOR = new Parcelable.Creator() {
        /* class com.facebook.login.DeviceAuthMethodHandler.AnonymousClass1 */

        /* renamed from: a */
        public DeviceAuthMethodHandler createFromParcel(Parcel parcel) {
            return new DeviceAuthMethodHandler(parcel);
        }

        /* renamed from: a */
        public DeviceAuthMethodHandler[] newArray(int i) {
            return new DeviceAuthMethodHandler[i];
        }
    };
    private static ScheduledThreadPoolExecutor c;

    DeviceAuthMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public boolean a(LoginClient.Request request) {
        b(request);
        return true;
    }

    private void b(LoginClient.Request request) {
        FragmentActivity b = this.b.b();
        if (b != null && !b.isFinishing()) {
            DeviceAuthDialog c_ = c_();
            c_.show(b.getSupportFragmentManager(), "login_with_facebook");
            c_.a(request);
        }
    }

    /* access modifiers changed from: protected */
    public DeviceAuthDialog c_() {
        return new DeviceAuthDialog();
    }

    public void c() {
        this.b.a(LoginClient.Result.a(this.b.c(), "User canceled log in."));
    }

    public void a(Exception exc) {
        this.b.a(LoginClient.Result.a(this.b.c(), null, exc.getMessage()));
    }

    public void a(String str, String str2, String str3, Collection<String> collection, Collection<String> collection2, c cVar, Date date, Date date2, Date date3) {
        this.b.a(LoginClient.Result.a(this.b.c(), new AccessToken(str, str2, str3, collection, collection2, cVar, date, date2, date3)));
    }

    public static synchronized ScheduledThreadPoolExecutor d() {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
        synchronized (DeviceAuthMethodHandler.class) {
            if (c == null) {
                c = new ScheduledThreadPoolExecutor(1);
            }
            scheduledThreadPoolExecutor = c;
        }
        return scheduledThreadPoolExecutor;
    }

    protected DeviceAuthMethodHandler(Parcel parcel) {
        super(parcel);
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public String a() {
        return "device_auth";
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.facebook.login.LoginMethodHandler
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
    }
}
