package com.facebook.login;

public enum c {
    NATIVE_WITH_FALLBACK(true, true, true, false, true, true),
    NATIVE_ONLY(true, true, false, false, false, true),
    KATANA_ONLY(false, true, false, false, false, false),
    WEB_ONLY(false, false, true, false, true, false),
    WEB_VIEW_ONLY(false, false, true, false, false, false),
    DIALOG_ONLY(false, true, true, false, true, true),
    DEVICE_AUTH(false, false, false, true, false, false);
    
    private final boolean h;
    private final boolean i;
    private final boolean j;
    private final boolean k;
    private final boolean l;
    private final boolean m;

    private c(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.h = z;
        this.i = z2;
        this.j = z3;
        this.k = z4;
        this.l = z5;
        this.m = z6;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.m;
    }
}
