package com.facebook.login;

import com.facebook.AccessToken;
import java.util.Set;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private final AccessToken f1179a;
    private final Set<String> b;
    private final Set<String> c;

    public g(AccessToken accessToken, Set<String> set, Set<String> set2) {
        this.f1179a = accessToken;
        this.b = set;
        this.c = set2;
    }

    public Set<String> a() {
        return this.b;
    }
}
