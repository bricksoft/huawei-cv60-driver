package com.facebook.login;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.c;
import com.facebook.h;
import com.facebook.internal.q;
import com.facebook.internal.u;
import com.facebook.login.LoginClient;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public class GetTokenLoginMethodHandler extends LoginMethodHandler {
    public static final Parcelable.Creator<GetTokenLoginMethodHandler> CREATOR = new Parcelable.Creator() {
        /* class com.facebook.login.GetTokenLoginMethodHandler.AnonymousClass3 */

        /* renamed from: a */
        public GetTokenLoginMethodHandler createFromParcel(Parcel parcel) {
            return new GetTokenLoginMethodHandler(parcel);
        }

        /* renamed from: a */
        public GetTokenLoginMethodHandler[] newArray(int i) {
            return new GetTokenLoginMethodHandler[i];
        }
    };
    private b c;

    GetTokenLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public String a() {
        return "get_token";
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public void b() {
        if (this.c != null) {
            this.c.b();
            this.c.a((q.a) null);
            this.c = null;
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public boolean a(final LoginClient.Request request) {
        this.c = new b(this.b.b(), request.d());
        if (!this.c.a()) {
            return false;
        }
        this.b.k();
        this.c.a(new q.a() {
            /* class com.facebook.login.GetTokenLoginMethodHandler.AnonymousClass1 */

            @Override // com.facebook.internal.q.a
            public void a(Bundle bundle) {
                GetTokenLoginMethodHandler.this.a(request, bundle);
            }
        });
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(LoginClient.Request request, Bundle bundle) {
        if (this.c != null) {
            this.c.a((q.a) null);
        }
        this.c = null;
        this.b.l();
        if (bundle != null) {
            ArrayList<String> stringArrayList = bundle.getStringArrayList("com.facebook.platform.extra.PERMISSIONS");
            Set<String> a2 = request.a();
            if (stringArrayList == null || (a2 != null && !stringArrayList.containsAll(a2))) {
                HashSet hashSet = new HashSet();
                for (String str : a2) {
                    if (!stringArrayList.contains(str)) {
                        hashSet.add(str);
                    }
                }
                if (!hashSet.isEmpty()) {
                    a("new_permissions", TextUtils.join(",", hashSet));
                }
                request.a(hashSet);
            } else {
                c(request, bundle);
                return;
            }
        }
        this.b.i();
    }

    /* access modifiers changed from: package-private */
    public void b(LoginClient.Request request, Bundle bundle) {
        this.b.a(LoginClient.Result.a(this.b.c(), a(bundle, c.FACEBOOK_APPLICATION_SERVICE, request.d())));
    }

    /* access modifiers changed from: package-private */
    public void c(final LoginClient.Request request, final Bundle bundle) {
        String string = bundle.getString("com.facebook.platform.extra.USER_ID");
        if (string == null || string.isEmpty()) {
            this.b.k();
            u.a(bundle.getString("com.facebook.platform.extra.ACCESS_TOKEN"), (u.a) new u.a() {
                /* class com.facebook.login.GetTokenLoginMethodHandler.AnonymousClass2 */

                @Override // com.facebook.internal.u.a
                public void a(JSONObject jSONObject) {
                    try {
                        bundle.putString("com.facebook.platform.extra.USER_ID", jSONObject.getString(TtmlNode.ATTR_ID));
                        GetTokenLoginMethodHandler.this.b(request, bundle);
                    } catch (JSONException e) {
                        GetTokenLoginMethodHandler.this.b.b(LoginClient.Result.a(GetTokenLoginMethodHandler.this.b.c(), "Caught exception", e.getMessage()));
                    }
                }

                @Override // com.facebook.internal.u.a
                public void a(h hVar) {
                    GetTokenLoginMethodHandler.this.b.b(LoginClient.Result.a(GetTokenLoginMethodHandler.this.b.c(), "Caught exception", hVar.getMessage()));
                }
            });
            return;
        }
        b(request, bundle);
    }

    GetTokenLoginMethodHandler(Parcel parcel) {
        super(parcel);
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.facebook.login.LoginMethodHandler
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
    }
}
