package com.facebook.login;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import com.facebook.c;
import com.facebook.h;
import com.facebook.internal.f;
import com.facebook.internal.u;
import com.facebook.internal.w;
import com.facebook.login.LoginClient;

/* access modifiers changed from: package-private */
public class WebViewLoginMethodHandler extends WebLoginMethodHandler {
    public static final Parcelable.Creator<WebViewLoginMethodHandler> CREATOR = new Parcelable.Creator<WebViewLoginMethodHandler>() {
        /* class com.facebook.login.WebViewLoginMethodHandler.AnonymousClass2 */

        /* renamed from: a */
        public WebViewLoginMethodHandler createFromParcel(Parcel parcel) {
            return new WebViewLoginMethodHandler(parcel);
        }

        /* renamed from: a */
        public WebViewLoginMethodHandler[] newArray(int i) {
            return new WebViewLoginMethodHandler[i];
        }
    };
    private w c;
    private String d;

    WebViewLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public String a() {
        return "web_view";
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.WebLoginMethodHandler
    public c b_() {
        return c.WEB_VIEW;
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public boolean e() {
        return true;
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public void b() {
        if (this.c != null) {
            this.c.cancel();
            this.c = null;
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public boolean a(final LoginClient.Request request) {
        Bundle b = b(request);
        AnonymousClass1 r1 = new w.c() {
            /* class com.facebook.login.WebViewLoginMethodHandler.AnonymousClass1 */

            @Override // com.facebook.internal.w.c
            public void a(Bundle bundle, h hVar) {
                WebViewLoginMethodHandler.this.b(request, bundle, hVar);
            }
        };
        this.d = LoginClient.m();
        a("e2e", this.d);
        FragmentActivity b2 = this.b.b();
        this.c = new a(b2, request.d(), b).a(this.d).a(u.f(b2)).b(request.i()).a(r1).a();
        f fVar = new f();
        fVar.setRetainInstance(true);
        fVar.a(this.c);
        fVar.show(b2.getSupportFragmentManager(), "FacebookDialogFragment");
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(LoginClient.Request request, Bundle bundle, h hVar) {
        super.a(request, bundle, hVar);
    }

    static class a extends w.a {

        /* renamed from: a  reason: collision with root package name */
        private String f1167a;
        private String b;
        private String c = "fbconnect://success";

        public a(Context context, String str, Bundle bundle) {
            super(context, str, "oauth", bundle);
        }

        public a a(String str) {
            this.f1167a = str;
            return this;
        }

        public a a(boolean z) {
            this.c = z ? "fbconnect://chrome_os_success" : "fbconnect://success";
            return this;
        }

        public a b(String str) {
            this.b = str;
            return this;
        }

        @Override // com.facebook.internal.w.a
        public w a() {
            Bundle e = e();
            e.putString("redirect_uri", this.c);
            e.putString("client_id", b());
            e.putString("e2e", this.f1167a);
            e.putString("response_type", "token,signed_request");
            e.putString("return_scopes", "true");
            e.putString("auth_type", this.b);
            return w.a(c(), "oauth", e, d(), f());
        }
    }

    WebViewLoginMethodHandler(Parcel parcel) {
        super(parcel);
        this.d = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.facebook.login.LoginMethodHandler
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.d);
    }
}
