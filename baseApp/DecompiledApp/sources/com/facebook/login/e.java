package com.facebook.login;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.a.g;
import com.facebook.login.LoginClient;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private final g f1173a;
    private String b;
    private String c;

    e(Context context, String str) {
        PackageInfo packageInfo;
        this.b = str;
        this.f1173a = g.b(context, str);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager != null && (packageInfo = packageManager.getPackageInfo("com.facebook.katana", 0)) != null) {
                this.c = packageInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
    }

    public String a() {
        return this.b;
    }

    static Bundle a(String str) {
        Bundle bundle = new Bundle();
        bundle.putLong("1_timestamp_ms", System.currentTimeMillis());
        bundle.putString("0_auth_logger_id", str);
        bundle.putString("3_method", "");
        bundle.putString("2_result", "");
        bundle.putString("5_error_message", "");
        bundle.putString("4_error_code", "");
        bundle.putString("6_extras", "");
        return bundle;
    }

    public void a(LoginClient.Request request) {
        Bundle a2 = a(request.e());
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("login_behavior", request.b().toString());
            jSONObject.put("request_code", LoginClient.d());
            jSONObject.put("permissions", TextUtils.join(",", request.a()));
            jSONObject.put("default_audience", request.c().toString());
            jSONObject.put("isReauthorize", request.f());
            if (this.c != null) {
                jSONObject.put("facebookVersion", this.c);
            }
            a2.putString("6_extras", jSONObject.toString());
        } catch (JSONException e) {
        }
        this.f1173a.a("fb_mobile_login_start", (Double) null, a2);
    }

    public void a(String str, Map<String, String> map, LoginClient.Result.a aVar, Map<String, String> map2, Exception exc) {
        JSONObject jSONObject;
        Bundle a2 = a(str);
        if (aVar != null) {
            a2.putString("2_result", aVar.a());
        }
        if (!(exc == null || exc.getMessage() == null)) {
            a2.putString("5_error_message", exc.getMessage());
        }
        JSONObject jSONObject2 = !map.isEmpty() ? new JSONObject(map) : null;
        if (map2 != null) {
            if (jSONObject2 == null) {
                jSONObject = new JSONObject();
            } else {
                jSONObject = jSONObject2;
            }
            try {
                for (Map.Entry<String, String> entry : map2.entrySet()) {
                    jSONObject.put(entry.getKey(), entry.getValue());
                }
                jSONObject2 = jSONObject;
            } catch (JSONException e) {
                jSONObject2 = jSONObject;
            }
        }
        if (jSONObject2 != null) {
            a2.putString("6_extras", jSONObject2.toString());
        }
        this.f1173a.a("fb_mobile_login_complete", (Double) null, a2);
    }

    public void a(String str, String str2) {
        Bundle a2 = a(str);
        a2.putString("3_method", str2);
        this.f1173a.a("fb_mobile_login_method_start", (Double) null, a2);
    }

    public void a(String str, String str2, String str3, String str4, String str5, Map<String, String> map) {
        Bundle a2 = a(str);
        if (str3 != null) {
            a2.putString("2_result", str3);
        }
        if (str4 != null) {
            a2.putString("5_error_message", str4);
        }
        if (str5 != null) {
            a2.putString("4_error_code", str5);
        }
        if (map != null && !map.isEmpty()) {
            a2.putString("6_extras", new JSONObject(map).toString());
        }
        a2.putString("3_method", str2);
        this.f1173a.a("fb_mobile_login_method_complete", (Double) null, a2);
    }

    public void b(String str, String str2) {
        Bundle a2 = a(str);
        a2.putString("3_method", str2);
        this.f1173a.a("fb_mobile_login_method_not_tried", (Double) null, a2);
    }

    public void c(String str, String str2) {
        a(str, str2, "");
    }

    public void a(String str, String str2, String str3) {
        Bundle a2 = a("");
        a2.putString("2_result", LoginClient.Result.a.ERROR.a());
        a2.putString("5_error_message", str2);
        a2.putString("3_method", str3);
        this.f1173a.a(str, (Double) null, a2);
    }
}
