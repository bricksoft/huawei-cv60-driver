package com.facebook.login;

import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import com.facebook.AccessToken;
import com.facebook.FacebookRequestError;
import com.facebook.c;
import com.facebook.h;
import com.facebook.internal.u;
import com.facebook.j;
import com.facebook.k;
import com.facebook.login.LoginClient;
import com.facebook.m;
import java.util.Locale;

abstract class WebLoginMethodHandler extends LoginMethodHandler {
    private String c;

    /* access modifiers changed from: package-private */
    public abstract c b_();

    private static final String d() {
        return "fb" + k.j() + "://authorize";
    }

    WebLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
    }

    WebLoginMethodHandler(Parcel parcel) {
        super(parcel);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public Bundle b(LoginClient.Request request) {
        Bundle bundle = new Bundle();
        if (!u.a(request.a())) {
            String join = TextUtils.join(",", request.a());
            bundle.putString("scope", join);
            a("scope", join);
        }
        bundle.putString("default_audience", request.c().a());
        bundle.putString("state", a(request.e()));
        AccessToken a2 = AccessToken.a();
        String d = a2 != null ? a2.d() : null;
        if (d == null || !d.equals(f())) {
            u.b(this.b.b());
            a("access_token", "0");
        } else {
            bundle.putString("access_token", d);
            a("access_token", "1");
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    public Bundle a(Bundle bundle, LoginClient.Request request) {
        bundle.putString("redirect_uri", d());
        bundle.putString("client_id", request.d());
        LoginClient loginClient = this.b;
        bundle.putString("e2e", LoginClient.m());
        bundle.putString("response_type", "token,signed_request");
        bundle.putString("return_scopes", "true");
        bundle.putString("auth_type", request.i());
        if (c() != null) {
            bundle.putString("sso", c());
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void a(LoginClient.Request request, Bundle bundle, h hVar) {
        String str;
        LoginClient.Result a2;
        this.c = null;
        if (bundle != null) {
            if (bundle.containsKey("e2e")) {
                this.c = bundle.getString("e2e");
            }
            try {
                AccessToken a3 = a(request.a(), bundle, b_(), request.d());
                a2 = LoginClient.Result.a(this.b.c(), a3);
                CookieSyncManager.createInstance(this.b.b()).sync();
                d(a3.d());
            } catch (h e) {
                a2 = LoginClient.Result.a(this.b.c(), null, e.getMessage());
            }
        } else if (hVar instanceof j) {
            a2 = LoginClient.Result.a(this.b.c(), "User canceled log in.");
        } else {
            this.c = null;
            String message = hVar.getMessage();
            if (hVar instanceof m) {
                FacebookRequestError a4 = ((m) hVar).a();
                str = String.format(Locale.ROOT, "%d", Integer.valueOf(a4.b()));
                message = a4.toString();
            } else {
                str = null;
            }
            a2 = LoginClient.Result.a(this.b.c(), null, message, str);
        }
        if (!u.a(this.c)) {
            b(this.c);
        }
        this.b.a(a2);
    }

    private String f() {
        return this.b.b().getSharedPreferences("com.facebook.login.AuthorizationClient.WebViewAuthHandler.TOKEN_STORE_KEY", 0).getString("TOKEN", "");
    }

    private void d(String str) {
        this.b.b().getSharedPreferences("com.facebook.login.AuthorizationClient.WebViewAuthHandler.TOKEN_STORE_KEY", 0).edit().putString("TOKEN", str).apply();
    }
}
