package com.facebook.login;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import com.facebook.CustomTabMainActivity;
import com.facebook.FacebookRequestError;
import com.facebook.c;
import com.facebook.h;
import com.facebook.internal.l;
import com.facebook.internal.u;
import com.facebook.internal.v;
import com.facebook.j;
import com.facebook.k;
import com.facebook.login.LoginClient;
import com.facebook.m;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public class CustomTabLoginMethodHandler extends WebLoginMethodHandler {
    public static final Parcelable.Creator<CustomTabLoginMethodHandler> CREATOR = new Parcelable.Creator() {
        /* class com.facebook.login.CustomTabLoginMethodHandler.AnonymousClass1 */

        /* renamed from: a */
        public CustomTabLoginMethodHandler createFromParcel(Parcel parcel) {
            return new CustomTabLoginMethodHandler(parcel);
        }

        /* renamed from: a */
        public CustomTabLoginMethodHandler[] newArray(int i) {
            return new CustomTabLoginMethodHandler[i];
        }
    };
    private static final String[] c = {"com.android.chrome", "com.chrome.beta", "com.chrome.dev"};
    private String d;
    private String e;

    CustomTabLoginMethodHandler(LoginClient loginClient) {
        super(loginClient);
        this.e = u.a(20);
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public String a() {
        return "custom_tab";
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.WebLoginMethodHandler
    public c b_() {
        return c.CHROME_CUSTOM_TAB;
    }

    /* access modifiers changed from: protected */
    @Override // com.facebook.login.WebLoginMethodHandler
    public String c() {
        return "chrome_custom_tab";
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public boolean a(LoginClient.Request request) {
        if (!d()) {
            return false;
        }
        Bundle a2 = a(b(request), request);
        Intent intent = new Intent(this.b.b(), CustomTabMainActivity.class);
        intent.putExtra(CustomTabMainActivity.f997a, a2);
        intent.putExtra(CustomTabMainActivity.b, g());
        this.b.a().startActivityForResult(intent, 1);
        return true;
    }

    private boolean d() {
        return f() && g() != null && h() && v.a(k.f());
    }

    private boolean f() {
        com.facebook.internal.k a2 = l.a(u.a(this.b.b()));
        return a2 != null && a2.b();
    }

    private String g() {
        if (this.d != null) {
            return this.d;
        }
        FragmentActivity b = this.b.b();
        List<ResolveInfo> queryIntentServices = b.getPackageManager().queryIntentServices(new Intent("android.support.customtabs.action.CustomTabsService"), 0);
        if (queryIntentServices != null) {
            HashSet hashSet = new HashSet(Arrays.asList(c));
            for (ResolveInfo resolveInfo : queryIntentServices) {
                ServiceInfo serviceInfo = resolveInfo.serviceInfo;
                if (serviceInfo != null && hashSet.contains(serviceInfo.packageName)) {
                    this.d = serviceInfo.packageName;
                    return this.d;
                }
            }
        }
        return null;
    }

    private boolean h() {
        if (!u.e(this.b.b())) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    @Override // com.facebook.login.LoginMethodHandler
    public boolean a(int i, int i2, Intent intent) {
        if (i != 1) {
            return super.a(i, i2, intent);
        }
        LoginClient.Request c2 = this.b.c();
        if (i2 == -1) {
            a(intent.getStringExtra(CustomTabMainActivity.c), c2);
            return true;
        }
        super.a(c2, (Bundle) null, new j());
        return false;
    }

    private void a(String str, LoginClient.Request request) {
        int i;
        if (str != null && str.startsWith(CustomTabMainActivity.a())) {
            Uri parse = Uri.parse(str);
            Bundle c2 = u.c(parse.getQuery());
            c2.putAll(u.c(parse.getFragment()));
            if (!a(c2)) {
                super.a(request, (Bundle) null, new h("Invalid state parameter"));
                return;
            }
            String string = c2.getString(IjkMediaPlayer.OnNativeInvokeListener.ARG_ERROR);
            if (string == null) {
                string = c2.getString("error_type");
            }
            String string2 = c2.getString("error_msg");
            if (string2 == null) {
                string2 = c2.getString("error_message");
            }
            if (string2 == null) {
                string2 = c2.getString("error_description");
            }
            String string3 = c2.getString("error_code");
            if (!u.a(string3)) {
                try {
                    i = Integer.parseInt(string3);
                } catch (NumberFormatException e2) {
                    i = -1;
                }
            } else {
                i = -1;
            }
            if (u.a(string) && u.a(string2) && i == -1) {
                super.a(request, c2, (h) null);
            } else if (string != null && (string.equals("access_denied") || string.equals("OAuthAccessDeniedException"))) {
                super.a(request, (Bundle) null, new j());
            } else if (i == 4201) {
                super.a(request, (Bundle) null, new j());
            } else {
                super.a(request, (Bundle) null, new m(new FacebookRequestError(i, string, string2), string2));
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.facebook.login.LoginMethodHandler
    public void a(JSONObject jSONObject) {
        jSONObject.put("7_challenge", this.e);
    }

    private boolean a(Bundle bundle) {
        try {
            String string = bundle.getString("state");
            if (string == null) {
                return false;
            }
            return new JSONObject(string).getString("7_challenge").equals(this.e);
        } catch (JSONException e2) {
            return false;
        }
    }

    public int describeContents() {
        return 0;
    }

    CustomTabLoginMethodHandler(Parcel parcel) {
        super(parcel);
        this.e = parcel.readString();
    }

    @Override // com.facebook.login.LoginMethodHandler
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.e);
    }
}
