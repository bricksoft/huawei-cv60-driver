package com.facebook.login;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.common.R;
import com.facebook.h;
import com.facebook.internal.d;
import com.facebook.internal.u;
import com.facebook.internal.v;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

/* access modifiers changed from: package-private */
public class LoginClient implements Parcelable {
    public static final Parcelable.Creator<LoginClient> CREATOR = new Parcelable.Creator<LoginClient>() {
        /* class com.facebook.login.LoginClient.AnonymousClass1 */

        /* renamed from: a */
        public LoginClient createFromParcel(Parcel parcel) {
            return new LoginClient(parcel);
        }

        /* renamed from: a */
        public LoginClient[] newArray(int i) {
            return new LoginClient[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    LoginMethodHandler[] f1161a;
    int b = -1;
    Fragment c;
    b d;
    a e;
    boolean f;
    Request g;
    Map<String, String> h;
    Map<String, String> i;
    private e j;

    /* access modifiers changed from: package-private */
    public interface a {
        void a();

        void b();
    }

    public interface b {
        void a(Result result);
    }

    public LoginClient(Fragment fragment) {
        this.c = fragment;
    }

    public Fragment a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
        if (this.c != null) {
            throw new h("Can't set fragment once it is already set.");
        }
        this.c = fragment;
    }

    /* access modifiers changed from: package-private */
    public FragmentActivity b() {
        return this.c.getActivity();
    }

    public Request c() {
        return this.g;
    }

    public static int d() {
        return d.b.Login.a();
    }

    /* access modifiers changed from: package-private */
    public void a(Request request) {
        if (!e()) {
            b(request);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Request request) {
        if (request != null) {
            if (this.g != null) {
                throw new h("Attempted to authorize while a request is pending.");
            } else if (!AccessToken.b() || h()) {
                this.g = request;
                this.f1161a = c(request);
                i();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.g != null && this.b >= 0;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.b >= 0) {
            g().b();
        }
    }

    /* access modifiers changed from: package-private */
    public LoginMethodHandler g() {
        if (this.b >= 0) {
            return this.f1161a[this.b];
        }
        return null;
    }

    public boolean a(int i2, int i3, Intent intent) {
        if (this.g != null) {
            return g().a(i2, i3, intent);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public LoginMethodHandler[] c(Request request) {
        ArrayList arrayList = new ArrayList();
        c b2 = request.b();
        if (b2.a()) {
            arrayList.add(new GetTokenLoginMethodHandler(this));
        }
        if (b2.b()) {
            arrayList.add(new KatanaProxyLoginMethodHandler(this));
        }
        if (b2.f()) {
            arrayList.add(new FacebookLiteLoginMethodHandler(this));
        }
        if (b2.e()) {
            arrayList.add(new CustomTabLoginMethodHandler(this));
        }
        if (b2.c()) {
            arrayList.add(new WebViewLoginMethodHandler(this));
        }
        if (b2.d()) {
            arrayList.add(new DeviceAuthMethodHandler(this));
        }
        LoginMethodHandler[] loginMethodHandlerArr = new LoginMethodHandler[arrayList.size()];
        arrayList.toArray(loginMethodHandlerArr);
        return loginMethodHandlerArr;
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        if (this.f) {
            return true;
        }
        if (a("android.permission.INTERNET") != 0) {
            FragmentActivity b2 = b();
            b(Result.a(this.g, b2.getString(R.string.com_facebook_internet_permission_error_title), b2.getString(R.string.com_facebook_internet_permission_error_message)));
            return false;
        }
        this.f = true;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.b >= 0) {
            a(g().a(), "skipped", null, null, g().f1165a);
        }
        while (this.f1161a != null && this.b < this.f1161a.length - 1) {
            this.b++;
            if (j()) {
                return;
            }
        }
        if (this.g != null) {
            n();
        }
    }

    private void n() {
        b(Result.a(this.g, "Login attempt failed.", null));
    }

    private void a(String str, String str2, boolean z) {
        if (this.h == null) {
            this.h = new HashMap();
        }
        if (this.h.containsKey(str) && z) {
            str2 = this.h.get(str) + "," + str2;
        }
        this.h.put(str, str2);
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        boolean z = false;
        LoginMethodHandler g2 = g();
        if (!g2.e() || h()) {
            z = g2.a(this.g);
            if (z) {
                o().a(this.g.e(), g2.a());
            } else {
                o().b(this.g.e(), g2.a());
                a("not_tried", g2.a(), true);
            }
        } else {
            a("no_internet_permission", "1", false);
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void a(Result result) {
        if (result.b == null || !AccessToken.b()) {
            b(result);
        } else {
            c(result);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Result result) {
        LoginMethodHandler g2 = g();
        if (g2 != null) {
            a(g2.a(), result, g2.f1165a);
        }
        if (this.h != null) {
            result.f = this.h;
        }
        if (this.i != null) {
            result.g = this.i;
        }
        this.f1161a = null;
        this.b = -1;
        this.g = null;
        this.h = null;
        d(result);
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar) {
        this.d = bVar;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.e = aVar;
    }

    /* access modifiers changed from: package-private */
    public int a(String str) {
        return b().checkCallingOrSelfPermission(str);
    }

    /* access modifiers changed from: package-private */
    public void c(Result result) {
        Result a2;
        if (result.b == null) {
            throw new h("Can't validate without a token");
        }
        AccessToken a3 = AccessToken.a();
        AccessToken accessToken = result.b;
        if (!(a3 == null || accessToken == null)) {
            try {
                if (a3.l().equals(accessToken.l())) {
                    a2 = Result.a(this.g, result.b);
                    b(a2);
                }
            } catch (Exception e2) {
                b(Result.a(this.g, "Caught exception", e2.getMessage()));
                return;
            }
        }
        a2 = Result.a(this.g, "User logged in as different Facebook user.", null);
        b(a2);
    }

    private e o() {
        if (this.j == null || !this.j.a().equals(this.g.d())) {
            this.j = new e(b(), this.g.d());
        }
        return this.j;
    }

    private void d(Result result) {
        if (this.d != null) {
            this.d.a(result);
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        if (this.e != null) {
            this.e.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        if (this.e != null) {
            this.e.b();
        }
    }

    private void a(String str, Result result, Map<String, String> map) {
        a(str, result.f1163a.a(), result.c, result.d, map);
    }

    private void a(String str, String str2, String str3, String str4, Map<String, String> map) {
        if (this.g == null) {
            o().a("fb_mobile_login_method_complete", "Unexpected call to logCompleteLogin with null pendingAuthorizationRequest.", str);
        } else {
            o().a(this.g.e(), str, str2, str3, str4, map);
        }
    }

    static String m() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("init", System.currentTimeMillis());
        } catch (JSONException e2) {
        }
        return jSONObject.toString();
    }

    public static class Request implements Parcelable {
        public static final Parcelable.Creator<Request> CREATOR = new Parcelable.Creator<Request>() {
            /* class com.facebook.login.LoginClient.Request.AnonymousClass1 */

            /* renamed from: a */
            public Request createFromParcel(Parcel parcel) {
                return new Request(parcel);
            }

            /* renamed from: a */
            public Request[] newArray(int i) {
                return new Request[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private final c f1162a;
        private Set<String> b;
        private final a c;
        private final String d;
        private final String e;
        private boolean f;
        private String g;
        private String h;
        private String i;

        Request(c cVar, Set<String> set, a aVar, String str, String str2, String str3) {
            this.f = false;
            this.f1162a = cVar;
            this.b = set == null ? new HashSet<>() : set;
            this.c = aVar;
            this.h = str;
            this.d = str2;
            this.e = str3;
        }

        /* access modifiers changed from: package-private */
        public Set<String> a() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public void a(Set<String> set) {
            v.a((Object) set, "permissions");
            this.b = set;
        }

        /* access modifiers changed from: package-private */
        public c b() {
            return this.f1162a;
        }

        /* access modifiers changed from: package-private */
        public a c() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public String d() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public String e() {
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public boolean f() {
            return this.f;
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            this.f = z;
        }

        /* access modifiers changed from: package-private */
        public String g() {
            return this.g;
        }

        /* access modifiers changed from: package-private */
        public String h() {
            return this.i;
        }

        /* access modifiers changed from: package-private */
        public String i() {
            return this.h;
        }

        /* access modifiers changed from: package-private */
        public boolean j() {
            for (String str : this.b) {
                if (f.a(str)) {
                    return true;
                }
            }
            return false;
        }

        private Request(Parcel parcel) {
            boolean z;
            a aVar = null;
            this.f = false;
            String readString = parcel.readString();
            this.f1162a = readString != null ? c.valueOf(readString) : null;
            ArrayList arrayList = new ArrayList();
            parcel.readStringList(arrayList);
            this.b = new HashSet(arrayList);
            String readString2 = parcel.readString();
            this.c = readString2 != null ? a.valueOf(readString2) : aVar;
            this.d = parcel.readString();
            this.e = parcel.readString();
            if (parcel.readByte() != 0) {
                z = true;
            } else {
                z = false;
            }
            this.f = z;
            this.g = parcel.readString();
            this.h = parcel.readString();
            this.i = parcel.readString();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            String str = null;
            parcel.writeString(this.f1162a != null ? this.f1162a.name() : null);
            parcel.writeStringList(new ArrayList(this.b));
            if (this.c != null) {
                str = this.c.name();
            }
            parcel.writeString(str);
            parcel.writeString(this.d);
            parcel.writeString(this.e);
            parcel.writeByte((byte) (this.f ? 1 : 0));
            parcel.writeString(this.g);
            parcel.writeString(this.h);
            parcel.writeString(this.i);
        }
    }

    public static class Result implements Parcelable {
        public static final Parcelable.Creator<Result> CREATOR = new Parcelable.Creator<Result>() {
            /* class com.facebook.login.LoginClient.Result.AnonymousClass1 */

            /* renamed from: a */
            public Result createFromParcel(Parcel parcel) {
                return new Result(parcel);
            }

            /* renamed from: a */
            public Result[] newArray(int i) {
                return new Result[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        final a f1163a;
        final AccessToken b;
        final String c;
        final String d;
        final Request e;
        public Map<String, String> f;
        public Map<String, String> g;

        /* access modifiers changed from: package-private */
        public enum a {
            SUCCESS("success"),
            CANCEL("cancel"),
            ERROR(IjkMediaPlayer.OnNativeInvokeListener.ARG_ERROR);
            
            private final String d;

            private a(String str) {
                this.d = str;
            }

            /* access modifiers changed from: package-private */
            public String a() {
                return this.d;
            }
        }

        Result(Request request, a aVar, AccessToken accessToken, String str, String str2) {
            v.a(aVar, "code");
            this.e = request;
            this.b = accessToken;
            this.c = str;
            this.f1163a = aVar;
            this.d = str2;
        }

        static Result a(Request request, AccessToken accessToken) {
            return new Result(request, a.SUCCESS, accessToken, null, null);
        }

        static Result a(Request request, String str) {
            return new Result(request, a.CANCEL, null, str, null);
        }

        static Result a(Request request, String str, String str2) {
            return a(request, str, str2, null);
        }

        static Result a(Request request, String str, String str2, String str3) {
            return new Result(request, a.ERROR, null, TextUtils.join(": ", u.b(str, str2)), str3);
        }

        private Result(Parcel parcel) {
            this.f1163a = a.valueOf(parcel.readString());
            this.b = (AccessToken) parcel.readParcelable(AccessToken.class.getClassLoader());
            this.c = parcel.readString();
            this.d = parcel.readString();
            this.e = (Request) parcel.readParcelable(Request.class.getClassLoader());
            this.f = u.a(parcel);
            this.g = u.a(parcel);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f1163a.name());
            parcel.writeParcelable(this.b, i);
            parcel.writeString(this.c);
            parcel.writeString(this.d);
            parcel.writeParcelable(this.e, i);
            u.a(parcel, this.f);
            u.a(parcel, this.g);
        }
    }

    public LoginClient(Parcel parcel) {
        Parcelable[] readParcelableArray = parcel.readParcelableArray(LoginMethodHandler.class.getClassLoader());
        this.f1161a = new LoginMethodHandler[readParcelableArray.length];
        for (int i2 = 0; i2 < readParcelableArray.length; i2++) {
            this.f1161a[i2] = (LoginMethodHandler) readParcelableArray[i2];
            this.f1161a[i2].a(this);
        }
        this.b = parcel.readInt();
        this.g = (Request) parcel.readParcelable(Request.class.getClassLoader());
        this.h = u.a(parcel);
        this.i = u.a(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeParcelableArray(this.f1161a, i2);
        parcel.writeInt(this.b);
        parcel.writeParcelable(this.g, i2);
        u.a(parcel, this.h);
        u.a(parcel, this.i);
    }
}
