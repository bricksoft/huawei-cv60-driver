package com.facebook.login;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.AccessToken;
import com.facebook.FacebookActivity;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.a.g;
import com.facebook.b.a.a;
import com.facebook.c;
import com.facebook.common.R;
import com.facebook.h;
import com.facebook.internal.l;
import com.facebook.internal.t;
import com.facebook.internal.u;
import com.facebook.internal.v;
import com.facebook.k;
import com.facebook.login.LoginClient;
import com.facebook.n;
import com.facebook.p;
import com.facebook.q;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class DeviceAuthDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private View f1150a;
    private TextView b;
    private TextView c;
    private DeviceAuthMethodHandler d;
    private AtomicBoolean e = new AtomicBoolean();
    private volatile n f;
    private volatile ScheduledFuture g;
    private volatile RequestState h;
    private Dialog i;
    private boolean j = false;
    private boolean k = false;
    private LoginClient.Request l = null;

    @Override // android.support.v4.app.Fragment
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        RequestState requestState;
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        this.d = (DeviceAuthMethodHandler) ((d) ((FacebookActivity) getActivity()).b()).c().g();
        if (!(bundle == null || (requestState = (RequestState) bundle.getParcelable("request_state")) == null)) {
            a(requestState);
        }
        return onCreateView;
    }

    @Override // android.support.v4.app.DialogFragment
    @NonNull
    public Dialog onCreateDialog(Bundle bundle) {
        this.i = new Dialog(getActivity(), R.style.com_facebook_auth_dialog);
        this.i.setContentView(a(a.b() && !this.k));
        return this.i;
    }

    @Override // android.support.v4.app.DialogFragment
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (!this.j) {
            a();
        }
    }

    @Override // android.support.v4.app.Fragment, android.support.v4.app.DialogFragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.h != null) {
            bundle.putParcelable("request_state", this.h);
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        this.j = true;
        this.e.set(true);
        super.onDestroy();
        if (this.f != null) {
            this.f.cancel(true);
        }
        if (this.g != null) {
            this.g.cancel(true);
        }
    }

    public void a(LoginClient.Request request) {
        this.l = request;
        Bundle bundle = new Bundle();
        bundle.putString("scope", TextUtils.join(",", request.a()));
        String g2 = request.g();
        if (g2 != null) {
            bundle.putString("redirect_uri", g2);
        }
        String h2 = request.h();
        if (h2 != null) {
            bundle.putString("target_user_id", h2);
        }
        bundle.putString("access_token", v.b() + "|" + v.c());
        bundle.putString("device_info", a.a());
        new GraphRequest(null, "device/login", bundle, q.POST, new GraphRequest.b() {
            /* class com.facebook.login.DeviceAuthDialog.AnonymousClass1 */

            @Override // com.facebook.GraphRequest.b
            public void a(p pVar) {
                if (!DeviceAuthDialog.this.j) {
                    if (pVar.a() != null) {
                        DeviceAuthDialog.this.a(pVar.a().f());
                        return;
                    }
                    JSONObject b = pVar.b();
                    RequestState requestState = new RequestState();
                    try {
                        requestState.a(b.getString("user_code"));
                        requestState.b(b.getString("code"));
                        requestState.a(b.getLong("interval"));
                        DeviceAuthDialog.this.a((DeviceAuthDialog) requestState);
                    } catch (JSONException e) {
                        DeviceAuthDialog.this.a(new h(e));
                    }
                }
            }
        }).j();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(RequestState requestState) {
        this.h = requestState;
        this.b.setText(requestState.b());
        this.c.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, new BitmapDrawable(getResources(), a.b(requestState.a())), (Drawable) null, (Drawable) null);
        this.b.setVisibility(0);
        this.f1150a.setVisibility(8);
        if (!this.k && a.a(requestState.b())) {
            g.a(getContext()).a("fb_smart_login_service", (Double) null, (Bundle) null);
        }
        if (requestState.e()) {
            c();
        } else {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public View a(boolean z) {
        View inflate = getActivity().getLayoutInflater().inflate(b(z), (ViewGroup) null);
        this.f1150a = inflate.findViewById(R.id.progress_bar);
        this.b = (TextView) inflate.findViewById(R.id.confirmation_code);
        ((Button) inflate.findViewById(R.id.cancel_button)).setOnClickListener(new View.OnClickListener() {
            /* class com.facebook.login.DeviceAuthDialog.AnonymousClass2 */

            public void onClick(View view) {
                DeviceAuthDialog.this.a();
            }
        });
        this.c = (TextView) inflate.findViewById(R.id.com_facebook_device_auth_instructions);
        this.c.setText(Html.fromHtml(getString(R.string.com_facebook_device_auth_instructions)));
        return inflate;
    }

    /* access modifiers changed from: protected */
    @LayoutRes
    public int b(boolean z) {
        return z ? R.layout.com_facebook_smart_device_dialog_fragment : R.layout.com_facebook_device_auth_dialog_fragment;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void b() {
        this.h.b(new Date().getTime());
        this.f = d().j();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void c() {
        this.g = DeviceAuthMethodHandler.d().schedule(new Runnable() {
            /* class com.facebook.login.DeviceAuthDialog.AnonymousClass3 */

            public void run() {
                DeviceAuthDialog.this.b();
            }
        }, this.h.d(), TimeUnit.SECONDS);
    }

    private GraphRequest d() {
        Bundle bundle = new Bundle();
        bundle.putString("code", this.h.c());
        return new GraphRequest(null, "device/login_status", bundle, q.POST, new GraphRequest.b() {
            /* class com.facebook.login.DeviceAuthDialog.AnonymousClass4 */

            @Override // com.facebook.GraphRequest.b
            public void a(p pVar) {
                if (!DeviceAuthDialog.this.e.get()) {
                    FacebookRequestError a2 = pVar.a();
                    if (a2 != null) {
                        switch (a2.c()) {
                            case 1349152:
                                if (DeviceAuthDialog.this.h != null) {
                                    a.c(DeviceAuthDialog.this.h.b());
                                }
                                if (DeviceAuthDialog.this.l != null) {
                                    DeviceAuthDialog.this.a(DeviceAuthDialog.this.l);
                                    return;
                                } else {
                                    DeviceAuthDialog.this.a();
                                    return;
                                }
                            case 1349172:
                            case 1349174:
                                DeviceAuthDialog.this.c();
                                return;
                            case 1349173:
                                DeviceAuthDialog.this.a();
                                return;
                            default:
                                DeviceAuthDialog.this.a(pVar.a().f());
                                return;
                        }
                    } else {
                        try {
                            JSONObject b = pVar.b();
                            DeviceAuthDialog.this.a(b.getString("access_token"), Long.valueOf(b.getLong("expires_in")), Long.valueOf(b.optLong("data_access_expiration_time")));
                        } catch (JSONException e) {
                            DeviceAuthDialog.this.a(new h(e));
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(final String str, final u.b bVar, final String str2, String str3, final Date date, final Date date2) {
        String string = getResources().getString(R.string.com_facebook_smart_login_confirmation_title);
        String string2 = getResources().getString(R.string.com_facebook_smart_login_confirmation_continue_as);
        String string3 = getResources().getString(R.string.com_facebook_smart_login_confirmation_cancel);
        String format = String.format(string2, str3);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(string).setCancelable(true).setNegativeButton(format, new DialogInterface.OnClickListener() {
            /* class com.facebook.login.DeviceAuthDialog.AnonymousClass6 */

            public void onClick(DialogInterface dialogInterface, int i) {
                DeviceAuthDialog.this.a(str, bVar, str2, date, date2);
            }
        }).setPositiveButton(string3, new DialogInterface.OnClickListener() {
            /* class com.facebook.login.DeviceAuthDialog.AnonymousClass5 */

            public void onClick(DialogInterface dialogInterface, int i) {
                DeviceAuthDialog.this.i.setContentView(DeviceAuthDialog.this.a(false));
                DeviceAuthDialog.this.a(DeviceAuthDialog.this.l);
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(final String str, Long l2, Long l3) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id,permissions,name");
        final Date date = l2.longValue() != 0 ? new Date(new Date().getTime() + (l2.longValue() * 1000)) : null;
        final Date date2 = (l3.longValue() == 0 || l3 == null) ? null : new Date(l3.longValue() * 1000);
        new GraphRequest(new AccessToken(str, k.j(), "0", null, null, null, date, null, date2), "me", bundle, q.GET, new GraphRequest.b() {
            /* class com.facebook.login.DeviceAuthDialog.AnonymousClass7 */

            @Override // com.facebook.GraphRequest.b
            public void a(p pVar) {
                if (!DeviceAuthDialog.this.e.get()) {
                    if (pVar.a() != null) {
                        DeviceAuthDialog.this.a(pVar.a().f());
                        return;
                    }
                    try {
                        JSONObject b2 = pVar.b();
                        String string = b2.getString(TtmlNode.ATTR_ID);
                        u.b a2 = u.a(b2);
                        String string2 = b2.getString("name");
                        a.c(DeviceAuthDialog.this.h.b());
                        if (!l.a(k.j()).e().contains(t.RequireConfirm) || DeviceAuthDialog.this.k) {
                            DeviceAuthDialog.this.a(string, a2, str, date, date2);
                            return;
                        }
                        DeviceAuthDialog.this.k = true;
                        DeviceAuthDialog.this.a((DeviceAuthDialog) string, (String) a2, (u.b) str, string2, (String) date, date2);
                    } catch (JSONException e) {
                        DeviceAuthDialog.this.a(new h(e));
                    }
                }
            }
        }).j();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, u.b bVar, String str2, Date date, Date date2) {
        this.d.a(str2, k.j(), str, bVar.a(), bVar.b(), c.DEVICE_AUTH, date, null, date2);
        this.i.dismiss();
    }

    /* access modifiers changed from: protected */
    public void a(h hVar) {
        if (this.e.compareAndSet(false, true)) {
            if (this.h != null) {
                a.c(this.h.b());
            }
            this.d.a(hVar);
            this.i.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.e.compareAndSet(false, true)) {
            if (this.h != null) {
                a.c(this.h.b());
            }
            if (this.d != null) {
                this.d.c();
            }
            this.i.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public static class RequestState implements Parcelable {
        public static final Parcelable.Creator<RequestState> CREATOR = new Parcelable.Creator<RequestState>() {
            /* class com.facebook.login.DeviceAuthDialog.RequestState.AnonymousClass1 */

            /* renamed from: a */
            public RequestState createFromParcel(Parcel parcel) {
                return new RequestState(parcel);
            }

            /* renamed from: a */
            public RequestState[] newArray(int i) {
                return new RequestState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private String f1158a;
        private String b;
        private String c;
        private long d;
        private long e;

        RequestState() {
        }

        public String a() {
            return this.f1158a;
        }

        public String b() {
            return this.b;
        }

        public void a(String str) {
            this.b = str;
            this.f1158a = String.format(Locale.ENGLISH, "https://facebook.com/device?user_code=%1$s&qr=1", str);
        }

        public String c() {
            return this.c;
        }

        public void b(String str) {
            this.c = str;
        }

        public long d() {
            return this.d;
        }

        public void a(long j) {
            this.d = j;
        }

        public void b(long j) {
            this.e = j;
        }

        protected RequestState(Parcel parcel) {
            this.f1158a = parcel.readString();
            this.b = parcel.readString();
            this.c = parcel.readString();
            this.d = parcel.readLong();
            this.e = parcel.readLong();
        }

        public boolean e() {
            if (this.e != 0 && (new Date().getTime() - this.e) - (this.d * 1000) < 0) {
                return true;
            }
            return false;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f1158a);
            parcel.writeString(this.b);
            parcel.writeString(this.c);
            parcel.writeLong(this.d);
            parcel.writeLong(this.e);
        }
    }
}
