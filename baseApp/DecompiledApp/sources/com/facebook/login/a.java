package com.facebook.login;

public enum a {
    NONE(null),
    ONLY_ME("only_me"),
    FRIENDS("friends"),
    EVERYONE("everyone");
    
    private final String e;

    private a(String str) {
        this.e = str;
    }

    public String a() {
        return this.e;
    }
}
