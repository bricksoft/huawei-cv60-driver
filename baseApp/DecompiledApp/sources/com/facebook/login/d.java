package com.facebook.login;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.common.R;
import com.facebook.login.LoginClient;

public class d extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private String f1170a;
    private LoginClient b;
    private LoginClient.Request c;

    @Override // android.support.v4.app.Fragment
    public void onCreate(Bundle bundle) {
        Bundle bundleExtra;
        super.onCreate(bundle);
        if (bundle != null) {
            this.b = (LoginClient) bundle.getParcelable("loginClient");
            this.b.a(this);
        } else {
            this.b = a();
        }
        this.b.a(new LoginClient.b() {
            /* class com.facebook.login.d.AnonymousClass1 */

            @Override // com.facebook.login.LoginClient.b
            public void a(LoginClient.Result result) {
                d.this.a((d) result);
            }
        });
        FragmentActivity activity = getActivity();
        if (activity != null) {
            a(activity);
            Intent intent = activity.getIntent();
            if (intent != null && (bundleExtra = intent.getBundleExtra("com.facebook.LoginFragment:Request")) != null) {
                this.c = (LoginClient.Request) bundleExtra.getParcelable("request");
            }
        }
    }

    /* access modifiers changed from: protected */
    public LoginClient a() {
        return new LoginClient(this);
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        this.b.f();
        super.onDestroy();
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(b(), viewGroup, false);
        final View findViewById = inflate.findViewById(R.id.com_facebook_login_fragment_progress_bar);
        this.b.a(new LoginClient.a() {
            /* class com.facebook.login.d.AnonymousClass2 */

            @Override // com.facebook.login.LoginClient.a
            public void a() {
                findViewById.setVisibility(0);
            }

            @Override // com.facebook.login.LoginClient.a
            public void b() {
                findViewById.setVisibility(8);
            }
        });
        return inflate;
    }

    /* access modifiers changed from: protected */
    @LayoutRes
    public int b() {
        return R.layout.com_facebook_login_fragment;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(LoginClient.Result result) {
        this.c = null;
        int i = result.f1163a == LoginClient.Result.a.CANCEL ? 0 : -1;
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.facebook.LoginFragment:Result", result);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        if (isAdded()) {
            getActivity().setResult(i, intent);
            getActivity().finish();
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onResume() {
        super.onResume();
        if (this.f1170a == null) {
            Log.e("LoginFragment", "Cannot call LoginFragment with a null calling package. This can occur if the launchMode of the caller is singleInstance.");
            getActivity().finish();
            return;
        }
        this.b.a(this.c);
    }

    @Override // android.support.v4.app.Fragment
    public void onPause() {
        View findViewById;
        super.onPause();
        if (getView() == null) {
            findViewById = null;
        } else {
            findViewById = getView().findViewById(R.id.com_facebook_login_fragment_progress_bar);
        }
        if (findViewById != null) {
            findViewById.setVisibility(8);
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        this.b.a(i, i2, intent);
    }

    @Override // android.support.v4.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("loginClient", this.b);
    }

    private void a(Activity activity) {
        ComponentName callingActivity = activity.getCallingActivity();
        if (callingActivity != null) {
            this.f1170a = callingActivity.getPackageName();
        }
    }

    /* access modifiers changed from: package-private */
    public LoginClient c() {
        return this.b;
    }
}
