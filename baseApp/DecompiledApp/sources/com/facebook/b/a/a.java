package com.facebook.b.a;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import com.facebook.internal.l;
import com.facebook.internal.t;
import com.facebook.internal.u;
import com.facebook.k;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.EnumMap;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1091a = a.class.getCanonicalName();
    private static HashMap<String, NsdManager.RegistrationListener> b = new HashMap<>();

    public static String a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("device", Build.DEVICE);
            jSONObject.put("model", Build.MODEL);
        } catch (JSONException e) {
        }
        return jSONObject.toString();
    }

    public static boolean a(String str) {
        if (b()) {
            return d(str);
        }
        return false;
    }

    public static boolean b() {
        return Build.VERSION.SDK_INT >= 16 && l.a(k.j()).e().contains(t.Enabled);
    }

    public static Bitmap b(String str) {
        EnumMap enumMap = new EnumMap(EncodeHintType.class);
        enumMap.put((Object) EncodeHintType.MARGIN, (Object) 2);
        try {
            BitMatrix encode = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, 200, 200, enumMap);
            int height = encode.getHeight();
            int width = encode.getWidth();
            int[] iArr = new int[(height * width)];
            for (int i = 0; i < height; i++) {
                int i2 = i * width;
                for (int i3 = 0; i3 < width; i3++) {
                    iArr[i2 + i3] = encode.get(i3, i) ? ViewCompat.MEASURED_STATE_MASK : -1;
                }
            }
            Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            try {
                createBitmap.setPixels(iArr, 0, width, 0, 0, width, height);
                return createBitmap;
            } catch (WriterException e) {
                return createBitmap;
            }
        } catch (WriterException e2) {
            return null;
        }
    }

    public static void c(String str) {
        e(str);
    }

    @TargetApi(16)
    private static boolean d(final String str) {
        if (!b.containsKey(str)) {
            final String format = String.format("%s_%s_%s", "fbsdk", String.format("%s-%s", "android", k.h().replace('.', '|')), str);
            NsdServiceInfo nsdServiceInfo = new NsdServiceInfo();
            nsdServiceInfo.setServiceType("_fb._tcp.");
            nsdServiceInfo.setServiceName(format);
            nsdServiceInfo.setPort(80);
            AnonymousClass1 r3 = new NsdManager.RegistrationListener() {
                /* class com.facebook.b.a.a.AnonymousClass1 */

                public void onServiceRegistered(NsdServiceInfo nsdServiceInfo) {
                    if (!format.equals(nsdServiceInfo.getServiceName())) {
                        a.c(str);
                    }
                }

                public void onServiceUnregistered(NsdServiceInfo nsdServiceInfo) {
                }

                public void onRegistrationFailed(NsdServiceInfo nsdServiceInfo, int i) {
                    a.c(str);
                }

                public void onUnregistrationFailed(NsdServiceInfo nsdServiceInfo, int i) {
                }
            };
            b.put(str, r3);
            ((NsdManager) k.f().getSystemService("servicediscovery")).registerService(nsdServiceInfo, 1, r3);
        }
        return true;
    }

    @TargetApi(16)
    private static void e(String str) {
        NsdManager.RegistrationListener registrationListener = b.get(str);
        if (registrationListener != null) {
            try {
                ((NsdManager) k.f().getSystemService("servicediscovery")).unregisterService(registrationListener);
            } catch (IllegalArgumentException e) {
                u.a(f1091a, (Exception) e);
            }
            b.remove(str);
        }
    }
}
