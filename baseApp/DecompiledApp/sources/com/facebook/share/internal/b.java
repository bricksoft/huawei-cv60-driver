package com.facebook.share.internal;

import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Pair;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.h;
import com.facebook.internal.u;
import com.facebook.q;
import com.facebook.share.internal.a;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.SharePhoto;
import java.io.File;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class b {
    public static JSONObject a(ShareOpenGraphContent shareOpenGraphContent) {
        return a.a(shareOpenGraphContent.c(), (a.AbstractC0048a) new a.AbstractC0048a() {
            /* class com.facebook.share.internal.b.AnonymousClass1 */

            @Override // com.facebook.share.internal.a.AbstractC0048a
            public JSONObject a(SharePhoto sharePhoto) {
                Uri c = sharePhoto.c();
                if (!u.a(c)) {
                    throw new h("Only web images may be used in OG objects shared via the web dialog");
                }
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("url", c.toString());
                    return jSONObject;
                } catch (JSONException e) {
                    throw new h("Unable to attach images", e);
                }
            }
        });
    }

    public static JSONArray a(JSONArray jSONArray, boolean z) {
        JSONArray jSONArray2 = new JSONArray();
        for (int i = 0; i < jSONArray.length(); i++) {
            Object obj = jSONArray.get(i);
            if (obj instanceof JSONArray) {
                obj = a((JSONArray) obj, z);
            } else if (obj instanceof JSONObject) {
                obj = a((JSONObject) obj, z);
            }
            jSONArray2.put(obj);
        }
        return jSONArray2;
    }

    public static JSONObject a(JSONObject jSONObject, boolean z) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            return null;
        }
        try {
            JSONObject jSONObject3 = new JSONObject();
            JSONObject jSONObject4 = new JSONObject();
            JSONArray names = jSONObject.names();
            for (int i = 0; i < names.length(); i++) {
                String string = names.getString(i);
                Object obj = jSONObject.get(string);
                if (obj instanceof JSONObject) {
                    jSONObject2 = a((JSONObject) obj, true);
                } else if (obj instanceof JSONArray) {
                    jSONObject2 = a((JSONArray) obj, true);
                } else {
                    jSONObject2 = obj;
                }
                Pair<String, String> a2 = a(string);
                String str = (String) a2.first;
                String str2 = (String) a2.second;
                if (z) {
                    if (str != null && str.equals("fbsdk")) {
                        jSONObject3.put(string, jSONObject2);
                    } else if (str == null || str.equals("og")) {
                        jSONObject3.put(str2, jSONObject2);
                    } else {
                        jSONObject4.put(str2, jSONObject2);
                    }
                } else if (str == null || !str.equals("fb")) {
                    jSONObject3.put(str2, jSONObject2);
                } else {
                    jSONObject3.put(string, jSONObject2);
                }
            }
            if (jSONObject4.length() > 0) {
                jSONObject3.put("data", jSONObject4);
            }
            return jSONObject3;
        } catch (JSONException e) {
            throw new h("Failed to create json object from share content");
        }
    }

    public static Pair<String, String> a(String str) {
        String str2 = null;
        int indexOf = str.indexOf(58);
        if (indexOf != -1 && str.length() > indexOf + 1) {
            str2 = str.substring(0, indexOf);
            str = str.substring(indexOf + 1);
        }
        return new Pair<>(str2, str);
    }

    public static GraphRequest a(AccessToken accessToken, File file, GraphRequest.b bVar) {
        GraphRequest.ParcelableResourceWithMimeType parcelableResourceWithMimeType = new GraphRequest.ParcelableResourceWithMimeType(ParcelFileDescriptor.open(file, 268435456), "image/png");
        Bundle bundle = new Bundle(1);
        bundle.putParcelable("file", parcelableResourceWithMimeType);
        return new GraphRequest(accessToken, "me/staging_resources", bundle, q.POST, bVar);
    }

    public static GraphRequest a(AccessToken accessToken, Uri uri, GraphRequest.b bVar) {
        if (u.c(uri)) {
            return a(accessToken, new File(uri.getPath()), bVar);
        }
        if (!u.b(uri)) {
            throw new h("The image Uri must be either a file:// or content:// Uri");
        }
        GraphRequest.ParcelableResourceWithMimeType parcelableResourceWithMimeType = new GraphRequest.ParcelableResourceWithMimeType(uri, "image/png");
        Bundle bundle = new Bundle(1);
        bundle.putParcelable("file", parcelableResourceWithMimeType);
        return new GraphRequest(accessToken, "me/staging_resources", bundle, q.POST, bVar);
    }
}
