package com.facebook.share.internal;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.b.a.a;
import com.facebook.common.R;
import com.facebook.internal.v;
import com.facebook.p;
import com.facebook.q;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphContent;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public class DeviceShareDialogFragment extends DialogFragment {
    private static ScheduledThreadPoolExecutor f;

    /* renamed from: a  reason: collision with root package name */
    private ProgressBar f1186a;
    private TextView b;
    private Dialog c;
    private volatile RequestState d;
    private volatile ScheduledFuture e;
    private ShareContent g;

    @Override // android.support.v4.app.Fragment
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        RequestState requestState;
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        if (!(bundle == null || (requestState = (RequestState) bundle.getParcelable("request_state")) == null)) {
            a(requestState);
        }
        return onCreateView;
    }

    @Override // android.support.v4.app.DialogFragment
    @NonNull
    public Dialog onCreateDialog(Bundle bundle) {
        this.c = new Dialog(getActivity(), R.style.com_facebook_auth_dialog);
        View inflate = getActivity().getLayoutInflater().inflate(R.layout.com_facebook_device_auth_dialog_fragment, (ViewGroup) null);
        this.f1186a = (ProgressBar) inflate.findViewById(R.id.progress_bar);
        this.b = (TextView) inflate.findViewById(R.id.confirmation_code);
        ((Button) inflate.findViewById(R.id.cancel_button)).setOnClickListener(new View.OnClickListener() {
            /* class com.facebook.share.internal.DeviceShareDialogFragment.AnonymousClass1 */

            public void onClick(View view) {
                DeviceShareDialogFragment.this.c.dismiss();
            }
        });
        ((TextView) inflate.findViewById(R.id.com_facebook_device_auth_instructions)).setText(Html.fromHtml(getString(R.string.com_facebook_device_auth_instructions)));
        this.c.setContentView(inflate);
        c();
        return this.c;
    }

    @Override // android.support.v4.app.DialogFragment
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (this.e != null) {
            this.e.cancel(true);
        }
        a(-1, new Intent());
    }

    @Override // android.support.v4.app.Fragment, android.support.v4.app.DialogFragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.d != null) {
            bundle.putParcelable("request_state", this.d);
        }
    }

    private void a(int i, Intent intent) {
        if (this.d != null) {
            a.c(this.d.a());
        }
        FacebookRequestError facebookRequestError = (FacebookRequestError) intent.getParcelableExtra(IjkMediaPlayer.OnNativeInvokeListener.ARG_ERROR);
        if (facebookRequestError != null) {
            Toast.makeText(getContext(), facebookRequestError.e(), 0).show();
        }
        if (isAdded()) {
            FragmentActivity activity = getActivity();
            activity.setResult(i, intent);
            activity.finish();
        }
    }

    private void a() {
        if (isAdded()) {
            getFragmentManager().beginTransaction().remove(this).commit();
        }
    }

    public void a(ShareContent shareContent) {
        this.g = shareContent;
    }

    private Bundle b() {
        ShareContent shareContent = this.g;
        if (shareContent == null) {
            return null;
        }
        if (shareContent instanceof ShareLinkContent) {
            return c.a((ShareLinkContent) shareContent);
        }
        if (shareContent instanceof ShareOpenGraphContent) {
            return c.a((ShareOpenGraphContent) shareContent);
        }
        return null;
    }

    private void c() {
        Bundle b2 = b();
        if (b2 == null || b2.size() == 0) {
            a(new FacebookRequestError(0, "", "Failed to get share content"));
        }
        b2.putString("access_token", v.b() + "|" + v.c());
        b2.putString("device_info", a.a());
        new GraphRequest(null, "device/share", b2, q.POST, new GraphRequest.b() {
            /* class com.facebook.share.internal.DeviceShareDialogFragment.AnonymousClass2 */

            @Override // com.facebook.GraphRequest.b
            public void a(p pVar) {
                FacebookRequestError a2 = pVar.a();
                if (a2 != null) {
                    DeviceShareDialogFragment.this.a((DeviceShareDialogFragment) a2);
                    return;
                }
                JSONObject b = pVar.b();
                RequestState requestState = new RequestState();
                try {
                    requestState.a(b.getString("user_code"));
                    requestState.a(b.getLong("expires_in"));
                    DeviceShareDialogFragment.this.a((DeviceShareDialogFragment) requestState);
                } catch (JSONException e) {
                    DeviceShareDialogFragment.this.a((DeviceShareDialogFragment) new FacebookRequestError(0, "", "Malformed server response"));
                }
            }
        }).j();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(FacebookRequestError facebookRequestError) {
        a();
        Intent intent = new Intent();
        intent.putExtra(IjkMediaPlayer.OnNativeInvokeListener.ARG_ERROR, facebookRequestError);
        a(-1, intent);
    }

    private static synchronized ScheduledThreadPoolExecutor d() {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
        synchronized (DeviceShareDialogFragment.class) {
            if (f == null) {
                f = new ScheduledThreadPoolExecutor(1);
            }
            scheduledThreadPoolExecutor = f;
        }
        return scheduledThreadPoolExecutor;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(RequestState requestState) {
        this.d = requestState;
        this.b.setText(requestState.a());
        this.b.setVisibility(0);
        this.f1186a.setVisibility(8);
        this.e = d().schedule(new Runnable() {
            /* class com.facebook.share.internal.DeviceShareDialogFragment.AnonymousClass3 */

            public void run() {
                DeviceShareDialogFragment.this.c.dismiss();
            }
        }, requestState.b(), TimeUnit.SECONDS);
    }

    /* access modifiers changed from: private */
    public static class RequestState implements Parcelable {
        public static final Parcelable.Creator<RequestState> CREATOR = new Parcelable.Creator<RequestState>() {
            /* class com.facebook.share.internal.DeviceShareDialogFragment.RequestState.AnonymousClass1 */

            /* renamed from: a */
            public RequestState createFromParcel(Parcel parcel) {
                return new RequestState(parcel);
            }

            /* renamed from: a */
            public RequestState[] newArray(int i) {
                return new RequestState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private String f1190a;
        private long b;

        RequestState() {
        }

        public String a() {
            return this.f1190a;
        }

        public void a(String str) {
            this.f1190a = str;
        }

        public long b() {
            return this.b;
        }

        public void a(long j) {
            this.b = j;
        }

        protected RequestState(Parcel parcel) {
            this.f1190a = parcel.readString();
            this.b = parcel.readLong();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f1190a);
            parcel.writeLong(this.b);
        }
    }
}
