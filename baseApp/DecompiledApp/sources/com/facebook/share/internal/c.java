package com.facebook.share.internal;

import android.os.Bundle;
import com.facebook.h;
import com.facebook.internal.u;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphContent;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    public static Bundle a(ShareLinkContent shareLinkContent) {
        Bundle a2 = a((ShareContent) shareLinkContent);
        u.a(a2, "href", shareLinkContent.a());
        u.a(a2, "quote", shareLinkContent.c());
        return a2;
    }

    public static Bundle a(ShareOpenGraphContent shareOpenGraphContent) {
        Bundle a2 = a((ShareContent) shareOpenGraphContent);
        u.a(a2, "action_type", shareOpenGraphContent.c().a());
        try {
            JSONObject a3 = b.a(b.a(shareOpenGraphContent), false);
            if (a3 != null) {
                u.a(a2, "action_properties", a3.toString());
            }
            return a2;
        } catch (JSONException e) {
            throw new h("Unable to serialize the ShareOpenGraphContent to JSON", e);
        }
    }

    public static Bundle a(ShareContent shareContent) {
        Bundle bundle = new Bundle();
        ShareHashtag b = shareContent.b();
        if (b != null) {
            u.a(bundle, "hashtag", b.a());
        }
        return bundle;
    }
}
