package com.facebook.share.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class CameraEffectTextures implements ShareModel {
    public static final Parcelable.Creator<CameraEffectTextures> CREATOR = new Parcelable.Creator<CameraEffectTextures>() {
        /* class com.facebook.share.model.CameraEffectTextures.AnonymousClass1 */

        /* renamed from: a */
        public CameraEffectTextures createFromParcel(Parcel parcel) {
            return new CameraEffectTextures(parcel);
        }

        /* renamed from: a */
        public CameraEffectTextures[] newArray(int i) {
            return new CameraEffectTextures[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f1199a;

    private CameraEffectTextures(a aVar) {
        this.f1199a = aVar.f1200a;
    }

    CameraEffectTextures(Parcel parcel) {
        this.f1199a = parcel.readBundle(getClass().getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.f1199a);
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Bundle f1200a = new Bundle();

        public a a(CameraEffectTextures cameraEffectTextures) {
            if (cameraEffectTextures != null) {
                this.f1200a.putAll(cameraEffectTextures.f1199a);
            }
            return this;
        }

        public a a(Parcel parcel) {
            return a((CameraEffectTextures) parcel.readParcelable(CameraEffectTextures.class.getClassLoader()));
        }

        public CameraEffectTextures a() {
            return new CameraEffectTextures(this);
        }
    }
}
