package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.ShareVideo;

public final class ShareVideoContent extends ShareContent<ShareVideoContent, Object> implements ShareModel {
    public static final Parcelable.Creator<ShareVideoContent> CREATOR = new Parcelable.Creator<ShareVideoContent>() {
        /* class com.facebook.share.model.ShareVideoContent.AnonymousClass1 */

        /* renamed from: a */
        public ShareVideoContent createFromParcel(Parcel parcel) {
            return new ShareVideoContent(parcel);
        }

        /* renamed from: a */
        public ShareVideoContent[] newArray(int i) {
            return new ShareVideoContent[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final String f1230a;
    private final String b;
    private final SharePhoto c;
    private final ShareVideo d;

    ShareVideoContent(Parcel parcel) {
        super(parcel);
        this.f1230a = parcel.readString();
        this.b = parcel.readString();
        SharePhoto.a b2 = new SharePhoto.a().b(parcel);
        if (b2.a() == null && b2.b() == null) {
            this.c = null;
        } else {
            this.c = b2.c();
        }
        this.d = new ShareVideo.a().b(parcel).a();
    }

    @Override // com.facebook.share.model.ShareContent
    public int describeContents() {
        return 0;
    }

    @Override // com.facebook.share.model.ShareContent
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f1230a);
        parcel.writeString(this.b);
        parcel.writeParcelable(this.c, 0);
        parcel.writeParcelable(this.d, 0);
    }
}
