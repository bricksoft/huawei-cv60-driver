package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable;

public final class ShareMessengerGenericTemplateContent extends ShareContent<ShareMessengerGenericTemplateContent, Object> {
    public static final Parcelable.Creator<ShareMessengerGenericTemplateContent> CREATOR = new Parcelable.Creator<ShareMessengerGenericTemplateContent>() {
        /* class com.facebook.share.model.ShareMessengerGenericTemplateContent.AnonymousClass1 */

        /* renamed from: a */
        public ShareMessengerGenericTemplateContent createFromParcel(Parcel parcel) {
            return new ShareMessengerGenericTemplateContent(parcel);
        }

        /* renamed from: a */
        public ShareMessengerGenericTemplateContent[] newArray(int i) {
            return new ShareMessengerGenericTemplateContent[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final boolean f1213a;
    private final a b;
    private final ShareMessengerGenericTemplateElement c;

    public enum a {
        HORIZONTAL,
        SQUARE
    }

    ShareMessengerGenericTemplateContent(Parcel parcel) {
        super(parcel);
        this.f1213a = parcel.readByte() != 0;
        this.b = (a) parcel.readSerializable();
        this.c = (ShareMessengerGenericTemplateElement) parcel.readParcelable(ShareMessengerGenericTemplateElement.class.getClassLoader());
    }

    @Override // com.facebook.share.model.ShareContent
    public int describeContents() {
        return 0;
    }

    @Override // com.facebook.share.model.ShareContent
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeByte((byte) (this.f1213a ? 1 : 0));
        parcel.writeSerializable(this.b);
        parcel.writeParcelable(this.c, i);
    }
}
