package com.facebook.share.model;

import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.Nullable;
import com.facebook.share.model.ShareOpenGraphValueContainer;
import com.facebook.share.model.ShareOpenGraphValueContainer.a;
import java.util.Set;

public abstract class ShareOpenGraphValueContainer<P extends ShareOpenGraphValueContainer, E extends a> implements ShareModel {

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f1222a;

    protected ShareOpenGraphValueContainer(a<P, E> aVar) {
        this.f1222a = (Bundle) ((a) aVar).f1223a.clone();
    }

    ShareOpenGraphValueContainer(Parcel parcel) {
        this.f1222a = parcel.readBundle(a.class.getClassLoader());
    }

    @Nullable
    public Object a(String str) {
        return this.f1222a.get(str);
    }

    @Nullable
    public String b(String str) {
        return this.f1222a.getString(str);
    }

    public Bundle b() {
        return (Bundle) this.f1222a.clone();
    }

    public Set<String> c() {
        return this.f1222a.keySet();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.f1222a);
    }

    public static abstract class a<P extends ShareOpenGraphValueContainer, E extends a> {

        /* renamed from: a  reason: collision with root package name */
        private Bundle f1223a = new Bundle();

        public E a(String str, @Nullable String str2) {
            this.f1223a.putString(str, str2);
            return this;
        }

        public E a(P p) {
            if (p != null) {
                this.f1223a.putAll(p.b());
            }
            return this;
        }
    }
}
