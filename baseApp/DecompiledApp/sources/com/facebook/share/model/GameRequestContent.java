package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public final class GameRequestContent implements ShareModel {
    public static final Parcelable.Creator<GameRequestContent> CREATOR = new Parcelable.Creator<GameRequestContent>() {
        /* class com.facebook.share.model.GameRequestContent.AnonymousClass1 */

        /* renamed from: a */
        public GameRequestContent createFromParcel(Parcel parcel) {
            return new GameRequestContent(parcel);
        }

        /* renamed from: a */
        public GameRequestContent[] newArray(int i) {
            return new GameRequestContent[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final String f1201a;
    private final List<String> b;
    private final String c;
    private final String d;
    private final a e;
    private final String f;
    private final b g;
    private final List<String> h;

    public enum a {
        SEND,
        ASKFOR,
        TURN
    }

    public enum b {
        APP_USERS,
        APP_NON_USERS
    }

    GameRequestContent(Parcel parcel) {
        this.f1201a = parcel.readString();
        this.b = parcel.createStringArrayList();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = (a) parcel.readSerializable();
        this.f = parcel.readString();
        this.g = (b) parcel.readSerializable();
        this.h = parcel.createStringArrayList();
        parcel.readStringList(this.h);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f1201a);
        parcel.writeStringList(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeSerializable(this.e);
        parcel.writeString(this.f);
        parcel.writeSerializable(this.g);
        parcel.writeStringList(this.h);
    }
}
