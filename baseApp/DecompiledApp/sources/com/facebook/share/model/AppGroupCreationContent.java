package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable;

public final class AppGroupCreationContent implements ShareModel {
    public static final Parcelable.Creator<AppGroupCreationContent> CREATOR = new Parcelable.Creator<AppGroupCreationContent>() {
        /* class com.facebook.share.model.AppGroupCreationContent.AnonymousClass1 */

        /* renamed from: a */
        public AppGroupCreationContent createFromParcel(Parcel parcel) {
            return new AppGroupCreationContent(parcel);
        }

        /* renamed from: a */
        public AppGroupCreationContent[] newArray(int i) {
            return new AppGroupCreationContent[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final String f1193a;
    private final String b;
    private a c;

    public enum a {
        Open,
        Closed
    }

    AppGroupCreationContent(Parcel parcel) {
        this.f1193a = parcel.readString();
        this.b = parcel.readString();
        this.c = (a) parcel.readSerializable();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f1193a);
        parcel.writeString(this.b);
        parcel.writeSerializable(this.c);
    }
}
