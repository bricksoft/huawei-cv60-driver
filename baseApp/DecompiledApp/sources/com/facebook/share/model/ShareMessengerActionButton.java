package com.facebook.share.model;

import android.os.Parcel;

public abstract class ShareMessengerActionButton implements ShareModel {

    /* renamed from: a  reason: collision with root package name */
    private final String f1212a;

    ShareMessengerActionButton(Parcel parcel) {
        this.f1212a = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f1212a);
    }
}
