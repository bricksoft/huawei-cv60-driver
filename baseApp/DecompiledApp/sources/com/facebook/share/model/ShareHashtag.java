package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ShareHashtag implements ShareModel {
    public static final Parcelable.Creator<ShareHashtag> CREATOR = new Parcelable.Creator<ShareHashtag>() {
        /* class com.facebook.share.model.ShareHashtag.AnonymousClass1 */

        /* renamed from: a */
        public ShareHashtag createFromParcel(Parcel parcel) {
            return new ShareHashtag(parcel);
        }

        /* renamed from: a */
        public ShareHashtag[] newArray(int i) {
            return new ShareHashtag[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final String f1206a;

    private ShareHashtag(a aVar) {
        this.f1206a = aVar.f1207a;
    }

    ShareHashtag(Parcel parcel) {
        this.f1206a = parcel.readString();
    }

    public String a() {
        return this.f1206a;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f1206a);
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private String f1207a;

        public a a(String str) {
            this.f1207a = str;
            return this;
        }

        public a a(ShareHashtag shareHashtag) {
            return shareHashtag == null ? this : a(shareHashtag.a());
        }

        /* access modifiers changed from: package-private */
        public a a(Parcel parcel) {
            return a((ShareHashtag) parcel.readParcelable(ShareHashtag.class.getClassLoader()));
        }

        public ShareHashtag a() {
            return new ShareHashtag(this);
        }
    }
}
