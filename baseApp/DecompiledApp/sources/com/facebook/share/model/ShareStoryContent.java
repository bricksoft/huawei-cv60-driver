package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ShareStoryContent extends ShareContent<ShareStoryContent, Object> {
    public static final Parcelable.Creator<ShareStoryContent> CREATOR = new Parcelable.Creator<ShareStoryContent>() {
        /* class com.facebook.share.model.ShareStoryContent.AnonymousClass1 */

        /* renamed from: a */
        public ShareStoryContent createFromParcel(Parcel parcel) {
            return new ShareStoryContent(parcel);
        }

        /* renamed from: a */
        public ShareStoryContent[] newArray(int i) {
            return new ShareStoryContent[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final ShareMedia f1227a;
    private final SharePhoto b;
    @Nullable
    private final List<String> c;
    private final String d;

    ShareStoryContent(Parcel parcel) {
        super(parcel);
        this.f1227a = (ShareMedia) parcel.readParcelable(ShareMedia.class.getClassLoader());
        this.b = (SharePhoto) parcel.readParcelable(SharePhoto.class.getClassLoader());
        this.c = a(parcel);
        this.d = parcel.readString();
    }

    @Override // com.facebook.share.model.ShareContent
    public int describeContents() {
        return 0;
    }

    @Override // com.facebook.share.model.ShareContent
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.f1227a, 0);
        parcel.writeParcelable(this.b, 0);
        parcel.writeStringList(this.c);
        parcel.writeString(this.d);
    }

    @Nullable
    private List<String> a(Parcel parcel) {
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        if (arrayList.isEmpty()) {
            return null;
        }
        return Collections.unmodifiableList(arrayList);
    }
}
