package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.model.CameraEffectArguments;
import com.facebook.share.model.CameraEffectTextures;

public class ShareCameraEffectContent extends ShareContent<ShareCameraEffectContent, Object> {
    public static final Parcelable.Creator<ShareCameraEffectContent> CREATOR = new Parcelable.Creator<ShareCameraEffectContent>() {
        /* class com.facebook.share.model.ShareCameraEffectContent.AnonymousClass1 */

        /* renamed from: a */
        public ShareCameraEffectContent createFromParcel(Parcel parcel) {
            return new ShareCameraEffectContent(parcel);
        }

        /* renamed from: a */
        public ShareCameraEffectContent[] newArray(int i) {
            return new ShareCameraEffectContent[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private String f1204a;
    private CameraEffectArguments b;
    private CameraEffectTextures c;

    ShareCameraEffectContent(Parcel parcel) {
        super(parcel);
        this.f1204a = parcel.readString();
        this.b = new CameraEffectArguments.a().a(parcel).a();
        this.c = new CameraEffectTextures.a().a(parcel).a();
    }

    @Override // com.facebook.share.model.ShareContent
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f1204a);
        parcel.writeParcelable(this.b, 0);
        parcel.writeParcelable(this.c, 0);
    }
}
