package com.facebook.share.model;

import android.net.Uri;
import android.os.Parcel;
import android.support.annotation.Nullable;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareHashtag;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ShareContent<P extends ShareContent, E> implements ShareModel {

    /* renamed from: a  reason: collision with root package name */
    private final Uri f1205a;
    private final List<String> b;
    private final String c;
    private final String d;
    private final String e;
    private final ShareHashtag f;

    protected ShareContent(Parcel parcel) {
        this.f1205a = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.b = a(parcel);
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = new ShareHashtag.a().a(parcel).a();
    }

    @Nullable
    public Uri a() {
        return this.f1205a;
    }

    @Nullable
    public ShareHashtag b() {
        return this.f;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f1205a, 0);
        parcel.writeStringList(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeParcelable(this.f, 0);
    }

    private List<String> a(Parcel parcel) {
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        if (arrayList.size() == 0) {
            return null;
        }
        return Collections.unmodifiableList(arrayList);
    }
}
