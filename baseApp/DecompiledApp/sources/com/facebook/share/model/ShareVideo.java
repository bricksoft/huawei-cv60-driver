package com.facebook.share.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.facebook.share.model.ShareMedia;

public final class ShareVideo extends ShareMedia {
    public static final Parcelable.Creator<ShareVideo> CREATOR = new Parcelable.Creator<ShareVideo>() {
        /* class com.facebook.share.model.ShareVideo.AnonymousClass1 */

        /* renamed from: a */
        public ShareVideo createFromParcel(Parcel parcel) {
            return new ShareVideo(parcel);
        }

        /* renamed from: a */
        public ShareVideo[] newArray(int i) {
            return new ShareVideo[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Uri f1228a;

    private ShareVideo(a aVar) {
        super(aVar);
        this.f1228a = aVar.f1229a;
    }

    ShareVideo(Parcel parcel) {
        super(parcel);
        this.f1228a = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
    }

    @Nullable
    public Uri b() {
        return this.f1228a;
    }

    @Override // com.facebook.share.model.ShareMedia
    public int describeContents() {
        return 0;
    }

    @Override // com.facebook.share.model.ShareMedia
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.f1228a, 0);
    }

    public static final class a extends ShareMedia.a<ShareVideo, a> {

        /* renamed from: a  reason: collision with root package name */
        private Uri f1229a;

        public a a(@Nullable Uri uri) {
            this.f1229a = uri;
            return this;
        }

        public ShareVideo a() {
            return new ShareVideo(this);
        }

        public a a(ShareVideo shareVideo) {
            if (shareVideo == null) {
                return this;
            }
            return ((a) super.a((ShareMedia) shareVideo)).a(shareVideo.b());
        }

        /* access modifiers changed from: package-private */
        public a b(Parcel parcel) {
            return a((ShareVideo) parcel.readParcelable(ShareVideo.class.getClassLoader()));
        }
    }
}
