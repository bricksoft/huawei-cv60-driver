package com.facebook.share.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class CameraEffectArguments implements ShareModel {
    public static final Parcelable.Creator<CameraEffectArguments> CREATOR = new Parcelable.Creator<CameraEffectArguments>() {
        /* class com.facebook.share.model.CameraEffectArguments.AnonymousClass1 */

        /* renamed from: a */
        public CameraEffectArguments createFromParcel(Parcel parcel) {
            return new CameraEffectArguments(parcel);
        }

        /* renamed from: a */
        public CameraEffectArguments[] newArray(int i) {
            return new CameraEffectArguments[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f1197a;

    private CameraEffectArguments(a aVar) {
        this.f1197a = aVar.f1198a;
    }

    CameraEffectArguments(Parcel parcel) {
        this.f1197a = parcel.readBundle(getClass().getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.f1197a);
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Bundle f1198a = new Bundle();

        public a a(CameraEffectArguments cameraEffectArguments) {
            if (cameraEffectArguments != null) {
                this.f1198a.putAll(cameraEffectArguments.f1197a);
            }
            return this;
        }

        public a a(Parcel parcel) {
            return a((CameraEffectArguments) parcel.readParcelable(CameraEffectArguments.class.getClassLoader()));
        }

        public CameraEffectArguments a() {
            return new CameraEffectArguments(this);
        }
    }
}
