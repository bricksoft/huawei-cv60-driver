package com.facebook.share.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public abstract class ShareMedia implements ShareModel {

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f1209a;

    protected ShareMedia(a aVar) {
        this.f1209a = new Bundle(aVar.f1210a);
    }

    ShareMedia(Parcel parcel) {
        this.f1209a = parcel.readBundle();
    }

    @Deprecated
    public Bundle a() {
        return new Bundle(this.f1209a);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.f1209a);
    }

    public static abstract class a<M extends ShareMedia, B extends a> {

        /* renamed from: a  reason: collision with root package name */
        private Bundle f1210a = new Bundle();

        @Deprecated
        public B a(Bundle bundle) {
            this.f1210a.putAll(bundle);
            return this;
        }

        public B a(M m) {
            return m == null ? this : a(m.a());
        }

        static List<ShareMedia> a(Parcel parcel) {
            Parcelable[] readParcelableArray = parcel.readParcelableArray(ShareMedia.class.getClassLoader());
            ArrayList arrayList = new ArrayList(readParcelableArray.length);
            for (Parcelable parcelable : readParcelableArray) {
                arrayList.add((ShareMedia) parcelable);
            }
            return arrayList;
        }
    }
}
