package com.facebook.share.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public final class ShareMessengerURLActionButton extends ShareMessengerActionButton {
    public static final Parcelable.Creator<ShareMessengerURLActionButton> CREATOR = new Parcelable.Creator<ShareMessengerURLActionButton>() {
        /* class com.facebook.share.model.ShareMessengerURLActionButton.AnonymousClass1 */

        /* renamed from: a */
        public ShareMessengerURLActionButton createFromParcel(Parcel parcel) {
            return new ShareMessengerURLActionButton(parcel);
        }

        /* renamed from: a */
        public ShareMessengerURLActionButton[] newArray(int i) {
            return new ShareMessengerURLActionButton[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Uri f1219a;
    private final Uri b;
    private final boolean c;
    private final boolean d;
    private final a e;

    public enum a {
        WebviewHeightRatioFull,
        WebviewHeightRatioTall,
        WebviewHeightRatioCompact
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ShareMessengerURLActionButton(Parcel parcel) {
        super(parcel);
        boolean z;
        boolean z2 = true;
        this.f1219a = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        if (parcel.readByte() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.c = z;
        this.b = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.e = (a) parcel.readSerializable();
        this.d = parcel.readByte() == 0 ? false : z2;
    }
}
