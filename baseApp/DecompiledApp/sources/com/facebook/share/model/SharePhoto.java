package com.facebook.share.model;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.facebook.share.model.ShareMedia;
import java.util.ArrayList;
import java.util.List;

public final class SharePhoto extends ShareMedia {
    public static final Parcelable.Creator<SharePhoto> CREATOR = new Parcelable.Creator<SharePhoto>() {
        /* class com.facebook.share.model.SharePhoto.AnonymousClass1 */

        /* renamed from: a */
        public SharePhoto createFromParcel(Parcel parcel) {
            return new SharePhoto(parcel);
        }

        /* renamed from: a */
        public SharePhoto[] newArray(int i) {
            return new SharePhoto[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Bitmap f1224a;
    private final Uri b;
    private final boolean c;
    private final String d;

    private SharePhoto(a aVar) {
        super(aVar);
        this.f1224a = aVar.f1225a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
    }

    SharePhoto(Parcel parcel) {
        super(parcel);
        this.f1224a = (Bitmap) parcel.readParcelable(Bitmap.class.getClassLoader());
        this.b = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.c = parcel.readByte() != 0;
        this.d = parcel.readString();
    }

    @Nullable
    public Bitmap b() {
        return this.f1224a;
    }

    @Nullable
    public Uri c() {
        return this.b;
    }

    public boolean d() {
        return this.c;
    }

    public String e() {
        return this.d;
    }

    @Override // com.facebook.share.model.ShareMedia
    public int describeContents() {
        return 0;
    }

    @Override // com.facebook.share.model.ShareMedia
    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 0;
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.f1224a, 0);
        parcel.writeParcelable(this.b, 0);
        if (this.c) {
            i2 = 1;
        }
        parcel.writeByte((byte) i2);
        parcel.writeString(this.d);
    }

    public static final class a extends ShareMedia.a<SharePhoto, a> {

        /* renamed from: a  reason: collision with root package name */
        private Bitmap f1225a;
        private Uri b;
        private boolean c;
        private String d;

        public a a(@Nullable Bitmap bitmap) {
            this.f1225a = bitmap;
            return this;
        }

        public a a(@Nullable Uri uri) {
            this.b = uri;
            return this;
        }

        public a a(boolean z) {
            this.c = z;
            return this;
        }

        public a a(@Nullable String str) {
            this.d = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Uri a() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public Bitmap b() {
            return this.f1225a;
        }

        public SharePhoto c() {
            return new SharePhoto(this);
        }

        public a a(SharePhoto sharePhoto) {
            if (sharePhoto == null) {
                return this;
            }
            return ((a) super.a((ShareMedia) sharePhoto)).a(sharePhoto.b()).a(sharePhoto.c()).a(sharePhoto.d()).a(sharePhoto.e());
        }

        /* access modifiers changed from: package-private */
        public a b(Parcel parcel) {
            return a((SharePhoto) parcel.readParcelable(SharePhoto.class.getClassLoader()));
        }

        static void a(Parcel parcel, int i, List<SharePhoto> list) {
            ShareMedia[] shareMediaArr = new ShareMedia[list.size()];
            for (int i2 = 0; i2 < list.size(); i2++) {
                shareMediaArr[i2] = list.get(i2);
            }
            parcel.writeParcelableArray(shareMediaArr, i);
        }

        static List<SharePhoto> c(Parcel parcel) {
            List<ShareMedia> a2 = a(parcel);
            ArrayList arrayList = new ArrayList();
            for (ShareMedia shareMedia : a2) {
                if (shareMedia instanceof SharePhoto) {
                    arrayList.add((SharePhoto) shareMedia);
                }
            }
            return arrayList;
        }
    }
}
