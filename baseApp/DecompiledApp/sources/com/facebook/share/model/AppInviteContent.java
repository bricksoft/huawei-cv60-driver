package com.facebook.share.model;

import android.os.Parcel;
import android.os.Parcelable;

@Deprecated
public final class AppInviteContent implements ShareModel {
    @Deprecated
    public static final Parcelable.Creator<AppInviteContent> CREATOR = new Parcelable.Creator<AppInviteContent>() {
        /* class com.facebook.share.model.AppInviteContent.AnonymousClass1 */

        /* renamed from: a */
        public AppInviteContent createFromParcel(Parcel parcel) {
            return new AppInviteContent(parcel);
        }

        /* renamed from: a */
        public AppInviteContent[] newArray(int i) {
            return new AppInviteContent[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final String f1195a;
    private final String b;
    private final String c;
    private final String d;
    private final a.EnumC0049a e;

    @Deprecated
    AppInviteContent(Parcel parcel) {
        this.f1195a = parcel.readString();
        this.b = parcel.readString();
        this.d = parcel.readString();
        this.c = parcel.readString();
        String readString = parcel.readString();
        if (readString.length() > 0) {
            this.e = a.EnumC0049a.valueOf(readString);
        } else {
            this.e = a.EnumC0049a.FACEBOOK;
        }
    }

    @Deprecated
    public int describeContents() {
        return 0;
    }

    @Deprecated
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f1195a);
        parcel.writeString(this.b);
        parcel.writeString(this.d);
        parcel.writeString(this.c);
        parcel.writeString(this.e.toString());
    }

    @Deprecated
    public static class a {

        @Deprecated
        /* renamed from: com.facebook.share.model.AppInviteContent$a$a  reason: collision with other inner class name */
        public enum EnumC0049a {
            FACEBOOK("facebook"),
            MESSENGER("messenger");
            
            private final String c;

            private EnumC0049a(String str) {
                this.c = str;
            }

            public String toString() {
                return this.c;
            }
        }
    }
}
