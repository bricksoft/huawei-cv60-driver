package com.facebook.a;

import android.preference.PreferenceManager;
import android.util.Log;
import com.facebook.k;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* access modifiers changed from: package-private */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1040a = b.class.getSimpleName();
    private static ReentrantReadWriteLock b = new ReentrantReadWriteLock();
    private static String c;
    private static volatile boolean d = false;

    b() {
    }

    public static void a() {
        if (!d) {
            g.h().execute(new Runnable() {
                /* class com.facebook.a.b.AnonymousClass1 */

                public void run() {
                    b.d();
                }
            });
        }
    }

    public static String b() {
        if (!d) {
            Log.w(f1040a, "initStore should have been called before calling setUserID");
            d();
        }
        b.readLock().lock();
        try {
            return c;
        } finally {
            b.readLock().unlock();
        }
    }

    /* access modifiers changed from: private */
    public static void d() {
        if (!d) {
            b.writeLock().lock();
            try {
                if (!d) {
                    c = PreferenceManager.getDefaultSharedPreferences(k.f()).getString("com.facebook.appevents.AnalyticsUserIDStore.userID", null);
                    d = true;
                    b.writeLock().unlock();
                }
            } finally {
                b.writeLock().unlock();
            }
        }
    }
}
