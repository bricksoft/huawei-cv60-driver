package com.facebook.a;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.a.g;
import com.facebook.internal.l;
import com.facebook.internal.o;
import com.facebook.k;
import com.facebook.p;
import com.facebook.t;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1064a = e.class.getName();
    private static volatile d b = new d();
    private static final ScheduledExecutorService c = Executors.newSingleThreadScheduledExecutor();
    private static ScheduledFuture d;
    private static final Runnable e = new Runnable() {
        /* class com.facebook.a.e.AnonymousClass1 */

        public void run() {
            ScheduledFuture unused = e.d = null;
            if (g.a() != g.a.EXPLICIT_ONLY) {
                e.b(h.TIMER);
            }
        }
    };

    e() {
    }

    public static void a() {
        c.execute(new Runnable() {
            /* class com.facebook.a.e.AnonymousClass2 */

            public void run() {
                f.a(e.b);
                d unused = e.b = new d();
            }
        });
    }

    public static void a(final h hVar) {
        c.execute(new Runnable() {
            /* class com.facebook.a.e.AnonymousClass3 */

            public void run() {
                e.b(hVar);
            }
        });
    }

    public static void a(final a aVar, final c cVar) {
        c.execute(new Runnable() {
            /* class com.facebook.a.e.AnonymousClass4 */

            public void run() {
                e.b.a(aVar, cVar);
                if (g.a() != g.a.EXPLICIT_ONLY && e.b.b() > 100) {
                    e.b(h.EVENT_THRESHOLD);
                } else if (e.d == null) {
                    ScheduledFuture unused = e.d = e.c.schedule(e.e, 15, TimeUnit.SECONDS);
                }
            }
        });
    }

    public static Set<a> b() {
        return b.a();
    }

    static void b(h hVar) {
        b.a(f.a());
        try {
            j a2 = a(hVar, b);
            if (a2 != null) {
                Intent intent = new Intent("com.facebook.sdk.APP_EVENTS_FLUSHED");
                intent.putExtra("com.facebook.sdk.APP_EVENTS_NUM_EVENTS_FLUSHED", a2.f1075a);
                intent.putExtra("com.facebook.sdk.APP_EVENTS_FLUSH_RESULT", a2.b);
                LocalBroadcastManager.getInstance(k.f()).sendBroadcast(intent);
            }
        } catch (Exception e2) {
            Log.w(f1064a, "Caught unexpected exception while flushing app events: ", e2);
        }
    }

    private static j a(h hVar, d dVar) {
        j jVar = new j();
        boolean b2 = k.b(k.f());
        ArrayList<GraphRequest> arrayList = new ArrayList();
        for (a aVar : dVar.a()) {
            GraphRequest a2 = a(aVar, dVar.a(aVar), b2, jVar);
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        if (arrayList.size() <= 0) {
            return null;
        }
        o.a(t.APP_EVENTS, f1064a, "Flushing %d events due to %s.", Integer.valueOf(jVar.f1075a), hVar.toString());
        for (GraphRequest graphRequest : arrayList) {
            graphRequest.i();
        }
        return jVar;
    }

    private static GraphRequest a(final a aVar, final l lVar, boolean z, final j jVar) {
        boolean z2;
        String b2 = aVar.b();
        com.facebook.internal.k a2 = l.a(b2, false);
        final GraphRequest a3 = GraphRequest.a((AccessToken) null, String.format("%s/activities", b2), (JSONObject) null, (GraphRequest.b) null);
        Bundle e2 = a3.e();
        if (e2 == null) {
            e2 = new Bundle();
        }
        e2.putString("access_token", aVar.a());
        String d2 = g.d();
        if (d2 != null) {
            e2.putString("device_token", d2);
        }
        a3.a(e2);
        if (a2 != null) {
            z2 = a2.a();
        } else {
            z2 = false;
        }
        int a4 = lVar.a(a3, k.f(), z2, z);
        if (a4 == 0) {
            return null;
        }
        jVar.f1075a = a4 + jVar.f1075a;
        a3.a((GraphRequest.b) new GraphRequest.b() {
            /* class com.facebook.a.e.AnonymousClass5 */

            @Override // com.facebook.GraphRequest.b
            public void a(p pVar) {
                e.b(aVar, a3, pVar, lVar, jVar);
            }
        });
        return a3;
    }

    /* access modifiers changed from: private */
    public static void b(final a aVar, GraphRequest graphRequest, p pVar, final l lVar, j jVar) {
        i iVar;
        String str;
        FacebookRequestError a2 = pVar.a();
        String str2 = "Success";
        i iVar2 = i.SUCCESS;
        if (a2 == null) {
            iVar = iVar2;
        } else if (a2.b() == -1) {
            str2 = "Failed: No Connectivity";
            iVar = i.NO_CONNECTIVITY;
        } else {
            str2 = String.format("Failed:\n  Response: %s\n  Error %s", pVar.toString(), a2.toString());
            iVar = i.SERVER_ERROR;
        }
        if (k.a(t.APP_EVENTS)) {
            try {
                str = new JSONArray((String) graphRequest.h()).toString(2);
            } catch (JSONException e2) {
                str = "<Can't encode events for debug logging>";
            }
            o.a(t.APP_EVENTS, f1064a, "Flush completed\nParams: %s\n  Result: %s\n  Events JSON: %s", graphRequest.a().toString(), str2, str);
        }
        lVar.a(a2 != null);
        if (iVar == i.NO_CONNECTIVITY) {
            k.d().execute(new Runnable() {
                /* class com.facebook.a.e.AnonymousClass6 */

                public void run() {
                    f.a(aVar, lVar);
                }
            });
        }
        if (iVar != i.SUCCESS && jVar.b != i.NO_CONNECTIVITY) {
            jVar.b = iVar;
        }
    }
}
