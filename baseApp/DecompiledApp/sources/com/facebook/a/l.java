package com.facebook.a;

import android.content.Context;
import android.os.Bundle;
import com.facebook.GraphRequest;
import com.facebook.a.b.c;
import com.facebook.internal.a;
import com.facebook.internal.u;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private List<c> f1078a = new ArrayList();
    private List<c> b = new ArrayList();
    private int c;
    private a d;
    private String e;
    private final int f = 1000;

    public l(a aVar, String str) {
        this.d = aVar;
        this.e = str;
    }

    public synchronized void a(c cVar) {
        if (this.f1078a.size() + this.b.size() >= 1000) {
            this.c++;
        } else {
            this.f1078a.add(cVar);
        }
    }

    public synchronized int a() {
        return this.f1078a.size();
    }

    public synchronized void a(boolean z) {
        if (z) {
            this.f1078a.addAll(this.b);
        }
        this.b.clear();
        this.c = 0;
    }

    public int a(GraphRequest graphRequest, Context context, boolean z, boolean z2) {
        synchronized (this) {
            int i = this.c;
            this.b.addAll(this.f1078a);
            this.f1078a.clear();
            JSONArray jSONArray = new JSONArray();
            for (c cVar : this.b) {
                if (!cVar.d()) {
                    u.a("Event with invalid checksum: %s", cVar.toString());
                } else if (z || !cVar.b()) {
                    jSONArray.put(cVar.c());
                }
            }
            if (jSONArray.length() == 0) {
                return 0;
            }
            a(graphRequest, context, i, jSONArray, z2);
            return jSONArray.length();
        }
    }

    public synchronized List<c> b() {
        List<c> list;
        list = this.f1078a;
        this.f1078a = new ArrayList();
        return list;
    }

    private void a(GraphRequest graphRequest, Context context, int i, JSONArray jSONArray, boolean z) {
        JSONObject jSONObject;
        try {
            jSONObject = c.a(c.a.CUSTOM_APP_EVENTS, this.d, this.e, z, context);
            if (this.c > 0) {
                jSONObject.put("num_skipped_events", i);
            }
        } catch (JSONException e2) {
            jSONObject = new JSONObject();
        }
        graphRequest.a(jSONObject);
        Bundle e3 = graphRequest.e();
        if (e3 == null) {
            e3 = new Bundle();
        }
        String jSONArray2 = jSONArray.toString();
        if (jSONArray2 != null) {
            e3.putString("custom_events", jSONArray2);
            graphRequest.a((Object) jSONArray2);
        }
        graphRequest.a(e3);
    }
}
