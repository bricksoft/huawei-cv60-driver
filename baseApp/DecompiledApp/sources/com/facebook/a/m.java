package com.facebook.a;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.facebook.k;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class m {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1079a = m.class.getSimpleName();
    private static ConcurrentHashMap<String, String> b;
    private static SharedPreferences c;
    private static AtomicBoolean d = new AtomicBoolean(false);

    static void a() {
        if (!d.get()) {
            c();
        }
    }

    static String b() {
        if (!d.get()) {
            Log.w(f1079a, "initStore should have been called before calling setUserID");
            c();
        }
        return a(b);
    }

    private static synchronized void c() {
        synchronized (m.class) {
            if (!d.get()) {
                c = PreferenceManager.getDefaultSharedPreferences(k.f());
                b = new ConcurrentHashMap<>(a(c.getString("com.facebook.appevents.UserDataStore.userData", "")));
                d.set(true);
            }
        }
    }

    private static String a(Map<String, String> map) {
        if (map.isEmpty()) {
            return "";
        }
        try {
            JSONObject jSONObject = new JSONObject();
            for (String str : map.keySet()) {
                jSONObject.put(str, map.get(str));
            }
            return jSONObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    private static Map<String, String> a(String str) {
        if (str.isEmpty()) {
            return new HashMap();
        }
        try {
            HashMap hashMap = new HashMap();
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.getString(next));
            }
            return hashMap;
        } catch (JSONException e) {
            return new HashMap();
        }
    }
}
