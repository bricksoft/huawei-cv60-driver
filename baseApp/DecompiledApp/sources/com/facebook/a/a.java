package com.facebook.a;

import com.facebook.AccessToken;
import com.facebook.internal.u;
import com.facebook.k;
import java.io.Serializable;

/* access modifiers changed from: package-private */
public class a implements Serializable {
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    private final String f1013a;
    private final String b;

    public a(AccessToken accessToken) {
        this(accessToken.d(), k.j());
    }

    public a(String str, String str2) {
        this.f1013a = u.a(str) ? null : str;
        this.b = str2;
    }

    public String a() {
        return this.f1013a;
    }

    public String b() {
        return this.b;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = this.f1013a == null ? 0 : this.f1013a.hashCode();
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode ^ i;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        if (!u.a((Object) aVar.f1013a, (Object) this.f1013a) || !u.a((Object) aVar.b, (Object) this.b)) {
            return false;
        }
        return true;
    }

    /* renamed from: com.facebook.a.a$a  reason: collision with other inner class name */
    static class C0043a implements Serializable {
        private static final long serialVersionUID = -2488473066578201069L;

        /* renamed from: a  reason: collision with root package name */
        private final String f1014a;
        private final String b;

        private C0043a(String str, String str2) {
            this.f1014a = str;
            this.b = str2;
        }

        private Object readResolve() {
            return new a(this.f1014a, this.b);
        }
    }

    private Object writeReplace() {
        return new C0043a(this.f1013a, this.b);
    }
}
