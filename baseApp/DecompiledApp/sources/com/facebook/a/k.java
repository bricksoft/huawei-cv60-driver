package com.facebook.a;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/* access modifiers changed from: package-private */
public class k implements Serializable {
    private static final long serialVersionUID = 20160629001L;

    /* renamed from: a  reason: collision with root package name */
    private HashMap<a, List<c>> f1076a = new HashMap<>();

    public k() {
    }

    public k(HashMap<a, List<c>> hashMap) {
        this.f1076a.putAll(hashMap);
    }

    public Set<a> a() {
        return this.f1076a.keySet();
    }

    public List<c> a(a aVar) {
        return this.f1076a.get(aVar);
    }

    public boolean b(a aVar) {
        return this.f1076a.containsKey(aVar);
    }

    public void a(a aVar, List<c> list) {
        if (!this.f1076a.containsKey(aVar)) {
            this.f1076a.put(aVar, list);
        } else {
            this.f1076a.get(aVar).addAll(list);
        }
    }

    static class a implements Serializable {
        private static final long serialVersionUID = 20160629001L;

        /* renamed from: a  reason: collision with root package name */
        private final HashMap<a, List<c>> f1077a;

        private a(HashMap<a, List<c>> hashMap) {
            this.f1077a = hashMap;
        }

        private Object readResolve() {
            return new k(this.f1077a);
        }
    }

    private Object writeReplace() {
        return new a(this.f1076a);
    }
}
