package com.facebook.a.b;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.a.a.b;
import com.facebook.a.a.d;
import com.facebook.a.a.e;
import com.facebook.a.g;
import com.facebook.internal.l;
import com.facebook.internal.o;
import com.facebook.internal.u;
import com.facebook.k;
import com.facebook.t;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1041a = a.class.getCanonicalName();
    private static final ScheduledExecutorService b = Executors.newSingleThreadScheduledExecutor();
    private static volatile ScheduledFuture c;
    private static final Object d = new Object();
    private static AtomicInteger e = new AtomicInteger(0);
    private static volatile i f;
    private static AtomicBoolean g = new AtomicBoolean(false);
    private static String h;
    private static long i;
    private static final b j = new b();
    private static final e k = new e();
    private static SensorManager l;
    private static d m;
    @Nullable
    private static String n = null;
    private static Boolean o = false;
    private static volatile Boolean p = false;
    private static int q = 0;

    static /* synthetic */ int f() {
        int i2 = q;
        q = i2 + 1;
        return i2;
    }

    static /* synthetic */ int g() {
        int i2 = q;
        q = i2 - 1;
        return i2;
    }

    public static void a(Application application, String str) {
        if (g.compareAndSet(false, true)) {
            h = str;
            application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
                /* class com.facebook.a.b.a.AnonymousClass1 */

                public void onActivityCreated(Activity activity, Bundle bundle) {
                    o.a(t.APP_EVENTS, a.f1041a, "onActivityCreated");
                    b.b();
                    a.a(activity);
                }

                public void onActivityStarted(Activity activity) {
                    a.f();
                    o.a(t.APP_EVENTS, a.f1041a, "onActivityStarted");
                }

                public void onActivityResumed(Activity activity) {
                    o.a(t.APP_EVENTS, a.f1041a, "onActivityResumed");
                    b.b();
                    a.b(activity);
                }

                public void onActivityPaused(Activity activity) {
                    o.a(t.APP_EVENTS, a.f1041a, "onActivityPaused");
                    b.b();
                    a.d(activity);
                }

                public void onActivityStopped(Activity activity) {
                    o.a(t.APP_EVENTS, a.f1041a, "onActivityStopped");
                    g.c();
                    a.g();
                }

                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                    o.a(t.APP_EVENTS, a.f1041a, "onActivitySaveInstanceState");
                }

                public void onActivityDestroyed(Activity activity) {
                    o.a(t.APP_EVENTS, a.f1041a, "onActivityDestroyed");
                }
            });
        }
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public static boolean a() {
        return q == 0;
    }

    public static UUID b() {
        if (f != null) {
            return f.g();
        }
        return null;
    }

    public static void a(Activity activity) {
        b.execute(new Runnable() {
            /* class com.facebook.a.b.a.AnonymousClass2 */

            public void run() {
                if (a.f == null) {
                    i unused = a.f = i.a();
                }
            }
        });
    }

    public static void b(Activity activity) {
        e.incrementAndGet();
        r();
        final long currentTimeMillis = System.currentTimeMillis();
        i = currentTimeMillis;
        final String c2 = u.c(activity);
        j.a(activity);
        b.execute(new Runnable() {
            /* class com.facebook.a.b.a.AnonymousClass3 */

            public void run() {
                if (a.f == null) {
                    i unused = a.f = new i(Long.valueOf(currentTimeMillis), null);
                    j.a(c2, (k) null, a.h);
                } else if (a.f.c() != null) {
                    long longValue = currentTimeMillis - a.f.c().longValue();
                    if (longValue > ((long) (a.q() * 1000))) {
                        j.a(c2, a.f, a.h);
                        j.a(c2, (k) null, a.h);
                        i unused2 = a.f = new i(Long.valueOf(currentTimeMillis), null);
                    } else if (longValue > 1000) {
                        a.f.e();
                    }
                }
                a.f.a(Long.valueOf(currentTimeMillis));
                a.f.j();
            }
        });
        Context applicationContext = activity.getApplicationContext();
        final String j2 = k.j();
        final com.facebook.internal.k a2 = l.a(j2);
        if (a2 != null && a2.h()) {
            l = (SensorManager) applicationContext.getSystemService("sensor");
            if (l != null) {
                Sensor defaultSensor = l.getDefaultSensor(1);
                m = new d(activity);
                k.a(new e.a() {
                    /* class com.facebook.a.b.a.AnonymousClass4 */

                    @Override // com.facebook.a.a.e.a
                    public void a() {
                        boolean z = false;
                        boolean z2 = a2 != null && a2.h();
                        if (k.m()) {
                            z = true;
                        }
                        if (z2 && z) {
                            a.a(j2);
                        }
                    }
                });
                l.registerListener(k, defaultSensor, 2);
                if (a2 != null && a2.h()) {
                    m.a();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void d(Activity activity) {
        if (e.decrementAndGet() < 0) {
            e.set(0);
            Log.w(f1041a, "Unexpected activity pause without a matching activity resume. Logging data may be incorrect. Make sure you call activateApp from your Application's onCreate method");
        }
        r();
        final long currentTimeMillis = System.currentTimeMillis();
        final String c2 = u.c(activity);
        j.b(activity);
        b.execute(new Runnable() {
            /* class com.facebook.a.b.a.AnonymousClass5 */

            public void run() {
                long j = 0;
                if (a.f == null) {
                    i unused = a.f = new i(Long.valueOf(currentTimeMillis), null);
                }
                a.f.a(Long.valueOf(currentTimeMillis));
                if (a.e.get() <= 0) {
                    AnonymousClass1 r2 = new Runnable() {
                        /* class com.facebook.a.b.a.AnonymousClass5.AnonymousClass1 */

                        public void run() {
                            if (a.e.get() <= 0) {
                                j.a(c2, a.f, a.h);
                                i.b();
                                i unused = a.f = null;
                            }
                            synchronized (a.d) {
                                ScheduledFuture unused2 = a.c = null;
                            }
                        }
                    };
                    synchronized (a.d) {
                        ScheduledFuture unused2 = a.c = a.b.schedule(r2, (long) a.q(), TimeUnit.SECONDS);
                    }
                }
                long j2 = a.i;
                if (j2 > 0) {
                    j = (currentTimeMillis - j2) / 1000;
                }
                d.a(c2, j);
                a.f.j();
            }
        });
        if (m != null) {
            m.b();
        }
        if (l != null) {
            l.unregisterListener(k);
        }
    }

    /* access modifiers changed from: private */
    public static int q() {
        com.facebook.internal.k a2 = l.a(k.j());
        if (a2 == null) {
            return e.a();
        }
        return a2.c();
    }

    private static void r() {
        synchronized (d) {
            if (c != null) {
                c.cancel(false);
            }
            c = null;
        }
    }

    public static void a(final String str) {
        if (!p.booleanValue()) {
            p = true;
            k.d().execute(new Runnable() {
                /* class com.facebook.a.b.a.AnonymousClass6 */

                public void run() {
                    GraphRequest a2 = GraphRequest.a((AccessToken) null, String.format(Locale.US, "%s/app_indexing_session", str), (JSONObject) null, (GraphRequest.b) null);
                    Bundle e = a2.e();
                    if (e == null) {
                        e = new Bundle();
                    }
                    com.facebook.internal.a a3 = com.facebook.internal.a.a(k.f());
                    JSONArray jSONArray = new JSONArray();
                    jSONArray.put(Build.MODEL != null ? Build.MODEL : "");
                    if (a3 == null || a3.b() == null) {
                        jSONArray.put("");
                    } else {
                        jSONArray.put(a3.b());
                    }
                    jSONArray.put("0");
                    jSONArray.put(b.c() ? "1" : "0");
                    Locale a4 = u.a();
                    jSONArray.put(a4.getLanguage() + "_" + a4.getCountry());
                    String jSONArray2 = jSONArray.toString();
                    e.putString("device_session_id", a.c());
                    e.putString("extinfo", jSONArray2);
                    a2.a(e);
                    if (a2 != null) {
                        JSONObject b = a2.i().b();
                        Boolean unused = a.o = Boolean.valueOf(b != null && b.optBoolean("is_app_indexing_enabled", false));
                        if (!a.o.booleanValue()) {
                            String unused2 = a.n = null;
                        } else {
                            a.m.a();
                        }
                    }
                    Boolean unused3 = a.p = false;
                }
            });
        }
    }

    public static String c() {
        if (n == null) {
            n = UUID.randomUUID().toString();
        }
        return n;
    }

    public static boolean d() {
        return o.booleanValue();
    }

    public static void a(Boolean bool) {
        o = bool;
    }
}
