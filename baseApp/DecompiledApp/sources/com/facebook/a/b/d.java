package com.facebook.a.b;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import com.facebook.a.g;
import com.facebook.internal.l;
import com.facebook.internal.v;
import com.facebook.k;
import com.google.android.gms.plus.PlusShare;
import java.math.BigDecimal;
import java.util.Currency;
import org.json.JSONException;
import org.json.JSONObject;
import tv.danmaku.ijk.media.player.IjkMediaMeta;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1049a = d.class.getCanonicalName();
    private static final h b = new h(k.f());

    public static void a() {
        Context f = k.f();
        String j = k.j();
        boolean l = k.l();
        v.a(f, "context");
        if (!l) {
            return;
        }
        if (f instanceof Application) {
            g.a((Application) f, j);
        } else {
            Log.w(f1049a, "Automatic logging of basic events will not happen, because FacebookSdk.getApplicationContext() returns object that is not instance of android.app.Application. Make sure you call FacebookSdk.sdkInitialize() from Application class and pass application context.");
        }
    }

    public static void a(String str, long j) {
        Context f = k.f();
        String j2 = k.j();
        v.a(f, "context");
        com.facebook.internal.k a2 = l.a(j2, false);
        if (a2 != null && a2.d() && j > 0) {
            g a3 = g.a(f);
            Bundle bundle = new Bundle(1);
            bundle.putCharSequence("fb_aa_time_spent_view_name", str);
            a3.a("fb_aa_time_spent_on_view", (double) j, bundle);
        }
    }

    public static void a(String str, String str2) {
        a b2;
        if (b() && (b2 = b(str, str2)) != null) {
            b.a(b2.f1051a, b2.b, b2.c);
        }
    }

    public static void a(l lVar, String str, String str2) {
        String str3;
        if (b()) {
            switch (lVar) {
                case RESTORE:
                    str3 = "SubscriptionRestore";
                    break;
                case CANCEL:
                    str3 = "SubscriptionCancel";
                    break;
                case HEARTBEAT:
                    str3 = "SubscriptionHeartbeat";
                    break;
                case EXPIRE:
                    str3 = "SubscriptionExpire";
                    break;
                case NEW:
                    a(str, str2);
                    return;
                default:
                    return;
            }
            a b2 = b(str, str2);
            if (b2 != null) {
                b.a(str3, b2.f1051a, b2.b, b2.c);
            }
        }
    }

    public static boolean b() {
        com.facebook.internal.k a2 = l.a(k.j());
        return a2 != null && k.l() && a2.g();
    }

    @Nullable
    private static a b(String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            JSONObject jSONObject2 = new JSONObject(str2);
            Bundle bundle = new Bundle(1);
            bundle.putCharSequence("fb_iap_product_id", jSONObject.getString("productId"));
            bundle.putCharSequence("fb_iap_purchase_time", jSONObject.getString("purchaseTime"));
            bundle.putCharSequence("fb_iap_purchase_token", jSONObject.getString("purchaseToken"));
            bundle.putCharSequence("fb_iap_package_name", jSONObject.optString("packageName"));
            bundle.putCharSequence("fb_iap_product_title", jSONObject2.optString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
            bundle.putCharSequence("fb_iap_product_description", jSONObject2.optString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION));
            String optString = jSONObject2.optString(IjkMediaMeta.IJKM_KEY_TYPE);
            bundle.putCharSequence("fb_iap_product_type", optString);
            if (optString.equals("subs")) {
                bundle.putCharSequence("fb_iap_subs_auto_renewing", Boolean.toString(jSONObject.optBoolean("autoRenewing", false)));
                bundle.putCharSequence("fb_iap_subs_period", jSONObject2.optString("subscriptionPeriod"));
                bundle.putCharSequence("fb_free_trial_period", jSONObject2.optString("freeTrialPeriod"));
                String optString2 = jSONObject2.optString("introductoryPriceCycles");
                if (!optString2.isEmpty()) {
                    bundle.putCharSequence("fb_intro_price_amount_micros", jSONObject2.optString("introductoryPriceAmountMicros"));
                    bundle.putCharSequence("fb_intro_price_cycles", optString2);
                }
            }
            return new a(new BigDecimal(((double) jSONObject2.getLong("price_amount_micros")) / 1000000.0d), Currency.getInstance(jSONObject2.getString("price_currency_code")), bundle);
        } catch (JSONException e) {
            Log.e(f1049a, "Error parsing in-app subscription data.", e);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        BigDecimal f1051a;
        Currency b;
        Bundle c;

        a(BigDecimal bigDecimal, Currency currency, Bundle bundle) {
            this.f1051a = bigDecimal;
            this.b = currency;
            this.c = bundle;
        }
    }
}
