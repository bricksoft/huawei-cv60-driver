package com.facebook.a.b;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import com.facebook.k;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1052a = f.class.getCanonicalName();
    private static final AtomicBoolean b = new AtomicBoolean(false);
    private static Boolean c = null;
    private static Boolean d = null;
    private static ServiceConnection e;
    private static Application.ActivityLifecycleCallbacks f;
    private static Intent g;
    private static Object h;

    public static void a() {
        d();
        if (c.booleanValue() && d.b()) {
            e();
        }
    }

    private static void d() {
        if (c == null) {
            try {
                Class.forName("com.android.vending.billing.IInAppBillingService$Stub");
                c = true;
                try {
                    Class.forName("com.android.billingclient.api.ProxyBillingActivity");
                    d = true;
                } catch (ClassNotFoundException e2) {
                    d = false;
                }
                g.a();
                g = new Intent("com.android.vending.billing.InAppBillingService.BIND").setPackage("com.android.vending");
                e = new ServiceConnection() {
                    /* class com.facebook.a.b.f.AnonymousClass1 */

                    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                        Object unused = f.h = g.a(k.f(), iBinder);
                    }

                    public void onServiceDisconnected(ComponentName componentName) {
                    }
                };
                f = new Application.ActivityLifecycleCallbacks() {
                    /* class com.facebook.a.b.f.AnonymousClass2 */

                    public void onActivityResumed(Activity activity) {
                        k.d().execute(new Runnable() {
                            /* class com.facebook.a.b.f.AnonymousClass2.AnonymousClass1 */

                            public void run() {
                                Context f = k.f();
                                f.b(f, g.a(f, f.h));
                                Map<String, l> c = g.c(f, f.h);
                                Iterator<String> it = g.b(f, f.h).iterator();
                                while (it.hasNext()) {
                                    c.put(it.next(), l.EXPIRE);
                                }
                                f.b(f, c);
                            }
                        });
                    }

                    public void onActivityCreated(Activity activity, Bundle bundle) {
                    }

                    public void onActivityStarted(Activity activity) {
                    }

                    public void onActivityPaused(Activity activity) {
                    }

                    public void onActivityStopped(Activity activity) {
                        if (f.d.booleanValue() && activity.getLocalClassName().equals("com.android.billingclient.api.ProxyBillingActivity")) {
                            k.d().execute(new Runnable() {
                                /* class com.facebook.a.b.f.AnonymousClass2.AnonymousClass2 */

                                public void run() {
                                    Context f = k.f();
                                    ArrayList<String> a2 = g.a(f, f.h);
                                    if (a2.isEmpty()) {
                                        a2 = g.d(f, f.h);
                                    }
                                    f.b(f, a2);
                                }
                            });
                        }
                    }

                    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                    }

                    public void onActivityDestroyed(Activity activity) {
                    }
                };
            } catch (ClassNotFoundException e3) {
                c = false;
            }
        }
    }

    private static void e() {
        if (b.compareAndSet(false, true)) {
            Context f2 = k.f();
            if (f2 instanceof Application) {
                ((Application) f2).registerActivityLifecycleCallbacks(f);
                f2.bindService(g, e, 1);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(Context context, ArrayList<String> arrayList) {
        if (!arrayList.isEmpty()) {
            HashMap hashMap = new HashMap();
            ArrayList arrayList2 = new ArrayList();
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                String next = it.next();
                try {
                    String string = new JSONObject(next).getString("productId");
                    hashMap.put(string, next);
                    arrayList2.add(string);
                } catch (JSONException e2) {
                    Log.e(f1052a, "Error parsing in-app purchase data.", e2);
                }
            }
            for (Map.Entry<String, String> entry : g.a(context, arrayList2, h, false).entrySet()) {
                d.a((String) hashMap.get(entry.getKey()), entry.getValue());
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(Context context, Map<String, l> map) {
        if (!map.isEmpty()) {
            HashMap hashMap = new HashMap();
            ArrayList arrayList = new ArrayList();
            for (String str : map.keySet()) {
                try {
                    String string = new JSONObject(str).getString("productId");
                    arrayList.add(string);
                    hashMap.put(string, str);
                } catch (JSONException e2) {
                    Log.e(f1052a, "Error parsing in-app purchase data.", e2);
                }
            }
            Map<String, String> a2 = g.a(context, arrayList, h, true);
            for (String str2 : a2.keySet()) {
                String str3 = (String) hashMap.get(str2);
                d.a(map.get(str3), str3, a2.get(str2));
            }
        }
    }
}
