package com.facebook.a.b;

import android.content.Context;
import android.os.Bundle;
import com.facebook.AccessToken;
import com.facebook.a.g;
import com.facebook.internal.u;
import java.math.BigDecimal;
import java.util.Currency;

/* access modifiers changed from: package-private */
public class h extends g {
    h(Context context) {
        this(u.c(context), null, null);
    }

    h(String str, String str2, AccessToken accessToken) {
        super(str, str2, accessToken);
    }

    /* access modifiers changed from: protected */
    @Override // com.facebook.a.g
    public void a(BigDecimal bigDecimal, Currency currency, Bundle bundle) {
        super.a(bigDecimal, currency, bundle);
    }

    /* access modifiers changed from: protected */
    @Override // com.facebook.a.g
    public void a(String str, BigDecimal bigDecimal, Currency currency, Bundle bundle) {
        super.a(str, bigDecimal, currency, bundle);
    }
}
