package com.facebook.a.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import com.facebook.k;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1055a = g.class.getCanonicalName();
    private static final HashMap<String, Method> b = new HashMap<>();
    private static final HashMap<String, Class<?>> c = new HashMap<>();
    private static final String d = k.f().getPackageName();
    private static final SharedPreferences e = k.f().getSharedPreferences("com.facebook.internal.SKU_DETAILS", 0);
    private static final SharedPreferences f = k.f().getSharedPreferences("com.facebook.internal.PURCHASE", 0);
    private static final SharedPreferences g = k.f().getSharedPreferences("com.facebook.internal.PURCHASE_SUBS", 0);

    g() {
    }

    @Nullable
    public static Object a(Context context, IBinder iBinder) {
        return a(context, "com.android.vending.billing.IInAppBillingService$Stub", "asInterface", null, new Object[]{iBinder});
    }

    public static Map<String, String> a(Context context, ArrayList<String> arrayList, Object obj, boolean z) {
        Map<String, String> a2 = a(arrayList);
        ArrayList arrayList2 = new ArrayList();
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (!a2.containsKey(next)) {
                arrayList2.add(next);
            }
        }
        a2.putAll(b(context, arrayList2, obj, z));
        return a2;
    }

    private static Map<String, String> b(Context context, ArrayList<String> arrayList, Object obj, boolean z) {
        HashMap hashMap = new HashMap();
        if (obj == null || arrayList.isEmpty()) {
            return hashMap;
        }
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("ITEM_ID_LIST", arrayList);
        Object[] objArr = new Object[4];
        objArr[0] = 3;
        objArr[1] = d;
        objArr[2] = z ? "subs" : "inapp";
        objArr[3] = bundle;
        Object a2 = a(context, "com.android.vending.billing.IInAppBillingService", "getSkuDetails", obj, objArr);
        if (a2 != null) {
            Bundle bundle2 = (Bundle) a2;
            if (bundle2.getInt("RESPONSE_CODE") == 0) {
                ArrayList<String> stringArrayList = bundle2.getStringArrayList("DETAILS_LIST");
                if (stringArrayList != null && arrayList.size() == stringArrayList.size()) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        hashMap.put(arrayList.get(i), stringArrayList.get(i));
                    }
                }
                a(hashMap);
            }
        }
        return hashMap;
    }

    private static Map<String, String> a(ArrayList<String> arrayList) {
        HashMap hashMap = new HashMap();
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            String next = it.next();
            String string = e.getString(next, null);
            if (string != null) {
                String[] split = string.split(";", 2);
                if (currentTimeMillis - Long.parseLong(split[0]) < 43200) {
                    hashMap.put(next, split[1]);
                }
            }
        }
        return hashMap;
    }

    private static void a(Map<String, String> map) {
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        SharedPreferences.Editor edit = e.edit();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            edit.putString(entry.getKey(), currentTimeMillis + ";" + entry.getValue());
        }
        edit.apply();
    }

    private static Boolean a(Context context, Object obj, String str) {
        if (obj == null) {
            return false;
        }
        Object a2 = a(context, "com.android.vending.billing.IInAppBillingService", "isBillingSupported", obj, new Object[]{3, d, str});
        return Boolean.valueOf(a2 != null && ((Integer) a2).intValue() == 0);
    }

    public static ArrayList<String> a(Context context, Object obj) {
        return b(b(context, obj, "inapp"));
    }

    public static ArrayList<String> b(Context context, Object obj) {
        ArrayList<String> arrayList = new ArrayList<>();
        Map<String, ?> all = g.getAll();
        if (all.isEmpty()) {
            return arrayList;
        }
        ArrayList<String> b2 = b(context, obj, "subs");
        HashSet hashSet = new HashSet();
        Iterator<String> it = b2.iterator();
        while (it.hasNext()) {
            try {
                hashSet.add(new JSONObject(it.next()).getString("productId"));
            } catch (JSONException e2) {
                Log.e(f1055a, "Error parsing purchase json", e2);
            }
        }
        HashSet<String> hashSet2 = new HashSet();
        for (Map.Entry<String, ?> entry : all.entrySet()) {
            String key = entry.getKey();
            if (!hashSet.contains(key)) {
                hashSet2.add(key);
            }
        }
        SharedPreferences.Editor edit = g.edit();
        for (String str : hashSet2) {
            String string = g.getString(str, "");
            edit.remove(str);
            if (!string.isEmpty()) {
                arrayList.add(g.getString(str, ""));
            }
        }
        edit.apply();
        return arrayList;
    }

    public static Map<String, l> c(Context context, Object obj) {
        HashMap hashMap = new HashMap();
        Iterator<String> it = b(context, obj, "subs").iterator();
        while (it.hasNext()) {
            String next = it.next();
            l a2 = a(next);
            if (!(a2 == l.DUPLICATED || a2 == l.UNKNOWN)) {
                hashMap.put(next, a2);
            }
        }
        return hashMap;
    }

    private static l a(String str) {
        JSONObject jSONObject;
        l lVar = null;
        try {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            JSONObject jSONObject2 = new JSONObject(str);
            String string = jSONObject2.getString("productId");
            String string2 = g.getString(string, "");
            if (string2.isEmpty()) {
                jSONObject = new JSONObject();
            } else {
                jSONObject = new JSONObject(string2);
            }
            if (!jSONObject.optString("purchaseToken").equals(jSONObject2.get("purchaseToken"))) {
                lVar = currentTimeMillis - (jSONObject2.getLong("purchaseTime") / 1000) < 43200 ? l.NEW : l.HEARTBEAT;
            }
            if (lVar == null && !string2.isEmpty()) {
                boolean z = jSONObject.getBoolean("autoRenewing");
                boolean z2 = jSONObject2.getBoolean("autoRenewing");
                if (!z2 && z) {
                    lVar = l.CANCEL;
                } else if (!z && z2) {
                    lVar = l.RESTORE;
                }
            }
            if (lVar == null && !string2.isEmpty()) {
                if (currentTimeMillis - jSONObject.getLong("LAST_LOGGED_TIME_SEC") > 86400) {
                    lVar = l.HEARTBEAT;
                } else {
                    lVar = l.DUPLICATED;
                }
            }
            if (lVar == l.DUPLICATED) {
                return lVar;
            }
            jSONObject2.put("LAST_LOGGED_TIME_SEC", currentTimeMillis);
            g.edit().putString(string, jSONObject2.toString()).apply();
            return lVar;
        } catch (JSONException e2) {
            Log.e(f1055a, "parsing purchase failure: ", e2);
            return l.UNKNOWN;
        }
    }

    private static ArrayList<String> b(Context context, Object obj, String str) {
        int i;
        ArrayList<String> arrayList = new ArrayList<>();
        if (obj == null) {
            return arrayList;
        }
        if (a(context, obj, str).booleanValue()) {
            int i2 = 0;
            String str2 = null;
            while (true) {
                Object a2 = a(context, "com.android.vending.billing.IInAppBillingService", "getPurchases", obj, new Object[]{3, d, str, str2});
                if (a2 != null) {
                    Bundle bundle = (Bundle) a2;
                    if (bundle.getInt("RESPONSE_CODE") == 0) {
                        ArrayList<String> stringArrayList = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                        if (stringArrayList == null) {
                            break;
                        }
                        arrayList.addAll(stringArrayList);
                        str2 = bundle.getString("INAPP_CONTINUATION_TOKEN");
                        i = i2 + stringArrayList.size();
                        if (i >= 30 || str2 == null) {
                            break;
                        }
                        i2 = i;
                    }
                }
                i = i2;
                str2 = null;
                i2 = i;
            }
        }
        return arrayList;
    }

    public static ArrayList<String> d(Context context, Object obj) {
        Class<?> a2;
        ArrayList<String> arrayList = new ArrayList<>();
        return (obj == null || (a2 = a(context, "com.android.vending.billing.IInAppBillingService")) == null || a(a2, "getPurchaseHistory") == null) ? arrayList : b(c(context, obj, "inapp"));
    }

    private static ArrayList<String> c(Context context, Object obj, String str) {
        int i;
        boolean z;
        ArrayList<String> arrayList = new ArrayList<>();
        if (a(context, obj, str).booleanValue()) {
            int i2 = 0;
            Boolean bool = false;
            String str2 = null;
            while (true) {
                String str3 = null;
                Object a2 = a(context, "com.android.vending.billing.IInAppBillingService", "getPurchaseHistory", obj, new Object[]{6, d, str, str2, new Bundle()});
                if (a2 != null) {
                    long currentTimeMillis = System.currentTimeMillis() / 1000;
                    Bundle bundle = (Bundle) a2;
                    if (bundle.getInt("RESPONSE_CODE") == 0) {
                        Iterator<String> it = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST").iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                z = bool;
                                break;
                            }
                            String next = it.next();
                            try {
                                if (currentTimeMillis - (new JSONObject(next).getLong("purchaseTime") / 1000) > 1200) {
                                    z = true;
                                    break;
                                }
                                arrayList.add(next);
                                i2++;
                            } catch (JSONException e2) {
                                Log.e(f1055a, "parsing purchase failure: ", e2);
                                i2 = i2;
                            }
                        }
                        str3 = bundle.getString("INAPP_CONTINUATION_TOKEN");
                        bool = z;
                        i = i2;
                        if (i >= 30 || str3 == null || bool.booleanValue()) {
                            break;
                        }
                        i2 = i;
                        str2 = str3;
                    }
                }
                i = i2;
                i2 = i;
                str2 = str3;
            }
        }
        return arrayList;
    }

    private static ArrayList<String> b(ArrayList<String> arrayList) {
        ArrayList<String> arrayList2 = new ArrayList<>();
        SharedPreferences.Editor edit = f.edit();
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            String next = it.next();
            try {
                JSONObject jSONObject = new JSONObject(next);
                String string = jSONObject.getString("productId");
                long j = jSONObject.getLong("purchaseTime");
                String string2 = jSONObject.getString("purchaseToken");
                if (currentTimeMillis - (j / 1000) <= 43200 && !f.getString(string, "").equals(string2)) {
                    edit.putString(string, string2);
                    arrayList2.add(next);
                }
            } catch (JSONException e2) {
                Log.e(f1055a, "parsing purchase failure: ", e2);
            }
        }
        edit.apply();
        return arrayList2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0055, code lost:
        if (r10.equals("asInterface") != false) goto L_0x001a;
     */
    @android.support.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.reflect.Method a(java.lang.Class<?> r9, java.lang.String r10) {
        /*
        // Method dump skipped, instructions count: 274
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.a.b.g.a(java.lang.Class, java.lang.String):java.lang.reflect.Method");
    }

    @Nullable
    private static Class<?> a(Context context, String str) {
        Class<?> cls = c.get(str);
        if (cls != null) {
            return cls;
        }
        try {
            cls = context.getClassLoader().loadClass(str);
            c.put(str, cls);
            return cls;
        } catch (ClassNotFoundException e2) {
            Log.e(f1055a, str + " is not available, please add " + str + " to the project.", e2);
            return cls;
        }
    }

    @Nullable
    private static Object a(Context context, String str, String str2, Object obj, Object[] objArr) {
        Method a2;
        Class<?> a3 = a(context, str);
        if (a3 == null || (a2 = a(a3, str2)) == null) {
            return null;
        }
        if (obj != null) {
            obj = a3.cast(obj);
        }
        try {
            return a2.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            Log.e(f1055a, "Illegal access to method " + a3.getName() + "." + a2.getName(), e2);
            return null;
        } catch (InvocationTargetException e3) {
            Log.e(f1055a, "Invocation target exception in " + a3.getName() + "." + a2.getName(), e3);
            return null;
        }
    }

    public static void a() {
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        long j = e.getLong("LAST_CLEARED_TIME", 0);
        if (j == 0) {
            e.edit().putLong("LAST_CLEARED_TIME", currentTimeMillis).apply();
        } else if (currentTimeMillis - j > 604800) {
            e.edit().clear().putLong("LAST_CLEARED_TIME", currentTimeMillis).apply();
        }
    }
}
