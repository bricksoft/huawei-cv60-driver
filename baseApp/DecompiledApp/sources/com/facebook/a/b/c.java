package com.facebook.a.b;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import com.facebook.a.g;
import com.facebook.internal.o;
import com.facebook.internal.u;
import com.facebook.t;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<a, String> f1047a = new HashMap<a, String>() {
        /* class com.facebook.a.b.c.AnonymousClass1 */

        {
            put(a.MOBILE_INSTALL_EVENT, "MOBILE_APP_INSTALL");
            put(a.CUSTOM_APP_EVENTS, "CUSTOM_APP_EVENTS");
        }
    };

    public enum a {
        MOBILE_INSTALL_EVENT,
        CUSTOM_APP_EVENTS
    }

    public static JSONObject a(a aVar, com.facebook.internal.a aVar2, String str, boolean z, Context context) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(NotificationCompat.CATEGORY_EVENT, f1047a.get(aVar));
        String e = g.e();
        if (e != null) {
            jSONObject.put("app_user_id", e);
        }
        String f = g.f();
        if (!f.isEmpty()) {
            jSONObject.put("ud", f);
        }
        u.a(jSONObject, aVar2, str, z);
        try {
            u.a(jSONObject, context);
        } catch (Exception e2) {
            o.a(t.APP_EVENTS, "AppEvents", "Fetching extended device info parameters failed: '%s'", e2.toString());
        }
        jSONObject.put("application_package_name", context.getPackageName());
        return jSONObject;
    }
}
