package com.facebook.a.b;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.facebook.k;
import java.util.UUID;

/* access modifiers changed from: package-private */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private Long f1056a;
    private Long b;
    private int c;
    private Long d;
    private k e;
    private UUID f;

    public i(Long l, Long l2) {
        this(l, l2, UUID.randomUUID());
    }

    public i(Long l, Long l2, UUID uuid) {
        this.f1056a = l;
        this.b = l2;
        this.f = uuid;
    }

    public static i a() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(k.f());
        long j = defaultSharedPreferences.getLong("com.facebook.appevents.SessionInfo.sessionStartTime", 0);
        long j2 = defaultSharedPreferences.getLong("com.facebook.appevents.SessionInfo.sessionEndTime", 0);
        String string = defaultSharedPreferences.getString("com.facebook.appevents.SessionInfo.sessionId", null);
        if (j == 0 || j2 == 0 || string == null) {
            return null;
        }
        i iVar = new i(Long.valueOf(j), Long.valueOf(j2));
        iVar.c = defaultSharedPreferences.getInt("com.facebook.appevents.SessionInfo.interruptionCount", 0);
        iVar.e = k.a();
        iVar.d = Long.valueOf(System.currentTimeMillis());
        iVar.f = UUID.fromString(string);
        return iVar;
    }

    public static void b() {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(k.f()).edit();
        edit.remove("com.facebook.appevents.SessionInfo.sessionStartTime");
        edit.remove("com.facebook.appevents.SessionInfo.sessionEndTime");
        edit.remove("com.facebook.appevents.SessionInfo.interruptionCount");
        edit.remove("com.facebook.appevents.SessionInfo.sessionId");
        edit.apply();
        k.b();
    }

    public Long c() {
        return this.b;
    }

    public void a(Long l) {
        this.b = l;
    }

    public int d() {
        return this.c;
    }

    public void e() {
        this.c++;
    }

    public long f() {
        if (this.d == null) {
            return 0;
        }
        return this.d.longValue();
    }

    public UUID g() {
        return this.f;
    }

    public long h() {
        if (this.f1056a == null || this.b == null) {
            return 0;
        }
        return this.b.longValue() - this.f1056a.longValue();
    }

    public k i() {
        return this.e;
    }

    public void j() {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(k.f()).edit();
        edit.putLong("com.facebook.appevents.SessionInfo.sessionStartTime", this.f1056a.longValue());
        edit.putLong("com.facebook.appevents.SessionInfo.sessionEndTime", this.b.longValue());
        edit.putInt("com.facebook.appevents.SessionInfo.interruptionCount", this.c);
        edit.putString("com.facebook.appevents.SessionInfo.sessionId", this.f.toString());
        edit.apply();
        if (this.e != null) {
            this.e.c();
        }
    }
}
