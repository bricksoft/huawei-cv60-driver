package com.facebook.a.b;

import android.os.Bundle;
import com.facebook.a.g;
import com.facebook.internal.o;
import com.facebook.t;
import java.util.Locale;

class j {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1057a = j.class.getCanonicalName();
    private static final long[] b = {300000, 900000, 1800000, 3600000, 21600000, 43200000, 86400000, 172800000, 259200000, 604800000, 1209600000, 1814400000, 2419200000L, 5184000000L, 7776000000L, 10368000000L, 12960000000L, 15552000000L, 31536000000L};

    j() {
    }

    public static void a(String str, k kVar, String str2) {
        String kVar2 = kVar != null ? kVar.toString() : "Unclassified";
        Bundle bundle = new Bundle();
        bundle.putString("fb_mobile_launch_source", kVar2);
        h hVar = new h(str, str2, null);
        hVar.a("fb_mobile_activate_app", bundle);
        if (g.a() != g.a.EXPLICIT_ONLY) {
            hVar.b();
        }
    }

    public static void a(String str, i iVar, String str2) {
        Long valueOf = Long.valueOf(iVar.f() - iVar.c().longValue());
        if (valueOf.longValue() < 0) {
            valueOf = 0L;
            a();
        }
        Long valueOf2 = Long.valueOf(iVar.h());
        if (valueOf2.longValue() < 0) {
            a();
            valueOf2 = 0L;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("fb_mobile_app_interruptions", iVar.d());
        bundle.putString("fb_mobile_time_between_sessions", String.format(Locale.ROOT, "session_quanta_%d", Integer.valueOf(a(valueOf.longValue()))));
        k i = iVar.i();
        bundle.putString("fb_mobile_launch_source", i != null ? i.toString() : "Unclassified");
        bundle.putLong("_logTime", iVar.c().longValue() / 1000);
        new h(str, str2, null).a("fb_mobile_deactivate_app", (double) (valueOf2.longValue() / 1000), bundle);
    }

    private static void a() {
        o.a(t.APP_EVENTS, f1057a, "Clock skew detected");
    }

    private static int a(long j) {
        int i = 0;
        while (i < b.length && b[i] < j) {
            i++;
        }
        return i;
    }
}
