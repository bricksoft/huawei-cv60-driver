package com.facebook.a;

import android.content.Context;
import com.facebook.internal.a;
import com.facebook.k;
import java.util.HashMap;
import java.util.Set;

/* access modifiers changed from: package-private */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap<a, l> f1063a = new HashMap<>();

    public synchronized void a(k kVar) {
        if (kVar != null) {
            for (a aVar : kVar.a()) {
                l b = b(aVar);
                for (c cVar : kVar.a(aVar)) {
                    b.a(cVar);
                }
            }
        }
    }

    public synchronized void a(a aVar, c cVar) {
        b(aVar).a(cVar);
    }

    public synchronized Set<a> a() {
        return this.f1063a.keySet();
    }

    public synchronized l a(a aVar) {
        return this.f1063a.get(aVar);
    }

    public synchronized int b() {
        int i;
        i = 0;
        for (l lVar : this.f1063a.values()) {
            i = lVar.a() + i;
        }
        return i;
    }

    private synchronized l b(a aVar) {
        l lVar;
        lVar = this.f1063a.get(aVar);
        if (lVar == null) {
            Context f = k.f();
            lVar = new l(a.a(f), g.b(f));
        }
        this.f1063a.put(aVar, lVar);
        return lVar;
    }
}
