package com.facebook.a;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.facebook.AccessToken;
import com.facebook.h;
import com.facebook.internal.l;
import com.facebook.internal.o;
import com.facebook.internal.u;
import com.facebook.internal.v;
import com.facebook.k;
import com.facebook.t;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1070a = g.class.getCanonicalName();
    private static ScheduledThreadPoolExecutor d;
    private static a e = a.AUTO;
    private static Object f = new Object();
    private static String g;
    private static boolean h;
    private static String i;
    private final String b;
    private final a c;

    public enum a {
        AUTO,
        EXPLICIT_ONLY
    }

    public static void a(Application application, String str) {
        if (!k.a()) {
            throw new h("The Facebook sdk must be initialized before calling activateApp");
        }
        b.a();
        m.a();
        if (str == null) {
            str = k.j();
        }
        k.a(application, str);
        com.facebook.a.b.a.a(application, str);
    }

    public static void a(Context context, String str) {
        if (k.l()) {
            d.execute(new Runnable(new g(context, str, (AccessToken) null)) {
                /* class com.facebook.a.g.AnonymousClass1 */

                /* renamed from: a  reason: collision with root package name */
                final /* synthetic */ g f1071a;

                {
                    this.f1071a = r1;
                }

                public void run() {
                    Bundle bundle = new Bundle();
                    try {
                        Class.forName("com.facebook.core.Core");
                        bundle.putInt("core_lib_included", 1);
                    } catch (ClassNotFoundException e) {
                    }
                    try {
                        Class.forName("com.facebook.login.Login");
                        bundle.putInt("login_lib_included", 1);
                    } catch (ClassNotFoundException e2) {
                    }
                    try {
                        Class.forName("com.facebook.share.Share");
                        bundle.putInt("share_lib_included", 1);
                    } catch (ClassNotFoundException e3) {
                    }
                    try {
                        Class.forName("com.facebook.places.Places");
                        bundle.putInt("places_lib_included", 1);
                    } catch (ClassNotFoundException e4) {
                    }
                    try {
                        Class.forName("com.facebook.messenger.Messenger");
                        bundle.putInt("messenger_lib_included", 1);
                    } catch (ClassNotFoundException e5) {
                    }
                    try {
                        Class.forName("com.facebook.applinks.AppLinks");
                        bundle.putInt("applinks_lib_included", 1);
                    } catch (ClassNotFoundException e6) {
                    }
                    try {
                        Class.forName("com.facebook.marketing.Marketing");
                        bundle.putInt("marketing_lib_included", 1);
                    } catch (ClassNotFoundException e7) {
                    }
                    try {
                        Class.forName("com.facebook.all.All");
                        bundle.putInt("all_lib_included", 1);
                    } catch (ClassNotFoundException e8) {
                    }
                    try {
                        Class.forName("com.android.billingclient.api.BillingClient");
                        bundle.putInt("billing_client_lib_included", 1);
                    } catch (ClassNotFoundException e9) {
                    }
                    try {
                        Class.forName("com.android.vending.billing.IInAppBillingService");
                        bundle.putInt("billing_service_lib_included", 1);
                    } catch (ClassNotFoundException e10) {
                    }
                    this.f1071a.a("fb_sdk_initialize", (Double) null, bundle);
                }
            });
        }
    }

    public static g a(Context context) {
        return new g(context, (String) null, (AccessToken) null);
    }

    public static g b(Context context, String str) {
        return new g(context, str, (AccessToken) null);
    }

    public static a a() {
        a aVar;
        synchronized (f) {
            aVar = e;
        }
        return aVar;
    }

    public void a(String str, Bundle bundle) {
        a(str, null, bundle, false, com.facebook.a.b.a.b());
    }

    public void a(String str, double d2, Bundle bundle) {
        a(str, Double.valueOf(d2), bundle, false, com.facebook.a.b.a.b());
    }

    /* access modifiers changed from: protected */
    public void a(BigDecimal bigDecimal, Currency currency, Bundle bundle) {
        a(bigDecimal, currency, bundle, true);
    }

    private void a(BigDecimal bigDecimal, Currency currency, Bundle bundle, boolean z) {
        Bundle bundle2;
        if (bigDecimal == null) {
            a("purchaseAmount cannot be null");
        } else if (currency == null) {
            a("currency cannot be null");
        } else {
            if (bundle == null) {
                bundle2 = new Bundle();
            } else {
                bundle2 = bundle;
            }
            bundle2.putString("fb_currency", currency.getCurrencyCode());
            a("fb_mobile_purchase", Double.valueOf(bigDecimal.doubleValue()), bundle2, z, com.facebook.a.b.a.b());
            g();
        }
    }

    public void b() {
        e.a(h.EXPLICIT);
    }

    public static void c() {
        e.a();
    }

    static String d() {
        String str;
        synchronized (f) {
            str = i;
        }
        return str;
    }

    public static String e() {
        return b.b();
    }

    public static String f() {
        return m.b();
    }

    public void a(String str, Double d2, Bundle bundle) {
        a(str, d2, bundle, true, com.facebook.a.b.a.b());
    }

    private g(Context context, String str, AccessToken accessToken) {
        this(u.c(context), str, accessToken);
    }

    protected g(String str, String str2, AccessToken accessToken) {
        v.a();
        this.b = str;
        accessToken = accessToken == null ? AccessToken.a() : accessToken;
        if (!AccessToken.b() || (str2 != null && !str2.equals(accessToken.k()))) {
            this.c = new a(null, str2 == null ? u.a(k.f()) : str2);
        } else {
            this.c = new a(accessToken);
        }
        i();
    }

    private static void i() {
        synchronized (f) {
            if (d == null) {
                d = new ScheduledThreadPoolExecutor(1);
                d.scheduleAtFixedRate(new Runnable() {
                    /* class com.facebook.a.g.AnonymousClass2 */

                    public void run() {
                        HashSet<String> hashSet = new HashSet();
                        for (a aVar : e.b()) {
                            hashSet.add(aVar.b());
                        }
                        for (String str : hashSet) {
                            l.a(str, true);
                        }
                    }
                }, 0, 86400, TimeUnit.SECONDS);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, BigDecimal bigDecimal, Currency currency, Bundle bundle) {
        a(str, Double.valueOf(bigDecimal.doubleValue()), bundle, true, com.facebook.a.b.a.b());
    }

    private void a(String str, Double d2, Bundle bundle, boolean z, @Nullable UUID uuid) {
        try {
            a(new c(this.b, str, d2, bundle, z, com.facebook.a.b.a.a(), uuid), this.c);
        } catch (JSONException e2) {
            o.a(t.APP_EVENTS, "AppEvents", "JSON encoding for app event failed: '%s'", e2.toString());
        } catch (h e3) {
            o.a(t.APP_EVENTS, "AppEvents", "Invalid app event: %s", e3.toString());
        }
    }

    private static void a(c cVar, a aVar) {
        e.a(aVar, cVar);
        if (!cVar.b() && !h) {
            if (cVar.a().equals("fb_mobile_activate_app")) {
                h = true;
            } else {
                o.a(t.APP_EVENTS, "AppEvents", "Warning: Please call AppEventsLogger.activateApp(...)from the long-lived activity's onResume() methodbefore logging other app events.");
            }
        }
    }

    static void g() {
        if (a() != a.EXPLICIT_ONLY) {
            e.a(h.EAGER_FLUSHING_EVENT);
        }
    }

    private static void a(String str) {
        o.a(t.DEVELOPER_ERRORS, "AppEvents", str);
    }

    static Executor h() {
        if (d == null) {
            i();
        }
        return d;
    }

    public static String b(Context context) {
        if (g == null) {
            synchronized (f) {
                if (g == null) {
                    g = context.getSharedPreferences("com.facebook.sdk.appEventPreferences", 0).getString("anonymousAppDeviceGUID", null);
                    if (g == null) {
                        g = "XZ" + UUID.randomUUID().toString();
                        context.getSharedPreferences("com.facebook.sdk.appEventPreferences", 0).edit().putString("anonymousAppDeviceGUID", g).apply();
                    }
                }
            }
        }
        return g;
    }
}
