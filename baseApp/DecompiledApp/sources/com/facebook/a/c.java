package com.facebook.a;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.facebook.h;
import com.facebook.internal.o;
import com.facebook.internal.u;
import com.facebook.t;
import com.google.android.exoplayer.C;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.UUID;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final HashSet<String> f1060a = new HashSet<>();
    private static final long serialVersionUID = 1;
    private final JSONObject b;
    private final boolean c;
    private final boolean d;
    private final String e;
    private final String f;

    public c(String str, String str2, Double d2, Bundle bundle, boolean z, boolean z2, @Nullable UUID uuid) {
        this.b = a(str, str2, d2, bundle, z, z2, uuid);
        this.c = z;
        this.d = z2;
        this.e = str2;
        this.f = e();
    }

    public String a() {
        return this.e;
    }

    private c(String str, boolean z, boolean z2, String str2) {
        this.b = new JSONObject(str);
        this.c = z;
        this.e = this.b.optString("_eventName");
        this.f = str2;
        this.d = z2;
    }

    public boolean b() {
        return this.c;
    }

    public JSONObject c() {
        return this.b;
    }

    public boolean d() {
        if (this.f == null) {
            return true;
        }
        return e().equals(this.f);
    }

    private static void a(String str) {
        boolean contains;
        if (str == null || str.length() == 0 || str.length() > 40) {
            if (str == null) {
                str = "<None Provided>";
            }
            throw new h(String.format(Locale.ROOT, "Identifier '%s' must be less than %d characters", str, 40));
        }
        synchronized (f1060a) {
            contains = f1060a.contains(str);
        }
        if (contains) {
            return;
        }
        if (str.matches("^[0-9a-zA-Z_]+[0-9a-zA-Z _-]*$")) {
            synchronized (f1060a) {
                f1060a.add(str);
            }
            return;
        }
        throw new h(String.format("Skipping event named '%s' due to illegal name - must be under 40 chars and alphanumeric, _, - or space, and not start with a space or hyphen.", str));
    }

    private static JSONObject a(String str, String str2, Double d2, Bundle bundle, boolean z, boolean z2, @Nullable UUID uuid) {
        a(str2);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("_eventName", str2);
        jSONObject.put("_eventName_md5", b(str2));
        jSONObject.put("_logTime", System.currentTimeMillis() / 1000);
        jSONObject.put("_ui", str);
        if (uuid != null) {
            jSONObject.put("_session_id", uuid);
        }
        if (d2 != null) {
            jSONObject.put("_valueToSum", d2.doubleValue());
        }
        if (z) {
            jSONObject.put("_implicitlyLogged", "1");
        }
        if (z2) {
            jSONObject.put("_inBackground", "1");
        }
        if (bundle != null) {
            for (String str3 : bundle.keySet()) {
                a(str3);
                Object obj = bundle.get(str3);
                if ((obj instanceof String) || (obj instanceof Number)) {
                    jSONObject.put(str3, obj.toString());
                } else {
                    throw new h(String.format("Parameter value '%s' for key '%s' should be a string or a numeric type.", obj, str3));
                }
            }
        }
        if (!z) {
            o.a(t.APP_EVENTS, "AppEvents", "Created app event '%s'", jSONObject.toString());
        }
        return jSONObject;
    }

    static class a implements Serializable {
        private static final long serialVersionUID = -2488473066578201069L;

        /* renamed from: a  reason: collision with root package name */
        private final String f1061a;
        private final boolean b;
        private final boolean c;

        private Object readResolve() {
            return new c(this.f1061a, this.b, this.c, null);
        }
    }

    static class b implements Serializable {
        private static final long serialVersionUID = 20160803001L;

        /* renamed from: a  reason: collision with root package name */
        private final String f1062a;
        private final boolean b;
        private final boolean c;
        private final String d;

        private b(String str, boolean z, boolean z2, String str2) {
            this.f1062a = str;
            this.b = z;
            this.c = z2;
            this.d = str2;
        }

        private Object readResolve() {
            return new c(this.f1062a, this.b, this.c, this.d);
        }
    }

    private Object writeReplace() {
        return new b(this.b.toString(), this.c, this.d, this.f);
    }

    public String toString() {
        return String.format("\"%s\", implicit: %b, json: %s", this.b.optString("_eventName"), Boolean.valueOf(this.c), this.b.toString());
    }

    private String e() {
        if (Build.VERSION.SDK_INT > 19) {
            return b(this.b.toString());
        }
        ArrayList arrayList = new ArrayList();
        Iterator<String> keys = this.b.keys();
        while (keys.hasNext()) {
            arrayList.add(keys.next());
        }
        Collections.sort(arrayList);
        StringBuilder sb = new StringBuilder();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            sb.append(str).append(" = ").append(this.b.optString(str)).append('\n');
        }
        return b(sb.toString());
    }

    private static String b(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            byte[] bytes = str.getBytes(C.UTF8_NAME);
            instance.update(bytes, 0, bytes.length);
            return com.facebook.a.b.b.a(instance.digest());
        } catch (NoSuchAlgorithmException e2) {
            u.a("Failed to generate checksum: ", (Exception) e2);
            return "0";
        } catch (UnsupportedEncodingException e3) {
            u.a("Failed to generate checksum: ", (Exception) e3);
            return "1";
        }
    }
}
