package com.facebook.a.a;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class e implements SensorEventListener {

    /* renamed from: a  reason: collision with root package name */
    private a f1039a;

    public interface a {
        void a();
    }

    public void a(a aVar) {
        this.f1039a = aVar;
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (this.f1039a != null) {
            float f = sensorEvent.values[0];
            double d = (double) (f / 9.80665f);
            double d2 = (double) (sensorEvent.values[1] / 9.80665f);
            double d3 = (double) (sensorEvent.values[2] / 9.80665f);
            if (Math.sqrt((d2 * d2) + (d * d) + (d3 * d3)) > 2.299999952316284d) {
                this.f1039a.a();
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }
}
