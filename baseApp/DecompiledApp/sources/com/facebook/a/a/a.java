package com.facebook.a.a;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.facebook.a.a.a.a;
import com.facebook.a.a.a.f;
import com.facebook.a.b.b;
import com.facebook.a.g;
import com.facebook.h;
import com.facebook.k;
import java.lang.ref.WeakReference;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1015a = a.class.getCanonicalName();

    public static C0044a a(com.facebook.a.a.a.a aVar, View view, View view2) {
        return new C0044a(aVar, view, view2);
    }

    /* renamed from: com.facebook.a.a.a$a  reason: collision with other inner class name */
    public static class C0044a extends View.AccessibilityDelegate {

        /* renamed from: a  reason: collision with root package name */
        protected boolean f1017a = false;
        private com.facebook.a.a.a.a b;
        private WeakReference<View> c;
        private WeakReference<View> d;
        private int e;
        private View.AccessibilityDelegate f;
        private boolean g = false;

        public C0044a() {
        }

        public C0044a(com.facebook.a.a.a.a aVar, View view, View view2) {
            if (aVar != null && view != null && view2 != null) {
                this.f = f.f(view2);
                this.b = aVar;
                this.c = new WeakReference<>(view2);
                this.d = new WeakReference<>(view);
                a.EnumC0045a d2 = aVar.d();
                switch (aVar.d()) {
                    case CLICK:
                        this.e = 1;
                        break;
                    case SELECTED:
                        this.e = 4;
                        break;
                    case TEXT_CHANGED:
                        this.e = 16;
                        break;
                    default:
                        throw new h("Unsupported action type: " + d2.toString());
                }
                this.g = true;
            }
        }

        public void sendAccessibilityEvent(View view, int i) {
            if (i == -1) {
                Log.e(a.f1015a, "Unsupported action type");
            }
            if (i == this.e) {
                if (this.f != null && !(this.f instanceof C0044a)) {
                    this.f.sendAccessibilityEvent(view, i);
                }
                b();
            }
        }

        private void b() {
            final String c2 = this.b.c();
            final Bundle a2 = b.a(this.b, this.d.get(), this.c.get());
            if (a2.containsKey("_valueToSum")) {
                a2.putDouble("_valueToSum", b.a(a2.getString("_valueToSum")));
            }
            a2.putString("_is_fb_codeless", "1");
            k.d().execute(new Runnable() {
                /* class com.facebook.a.a.a.C0044a.AnonymousClass1 */

                public void run() {
                    g.a(k.f()).a(c2, a2);
                }
            });
        }

        public boolean a() {
            return this.g;
        }
    }
}
