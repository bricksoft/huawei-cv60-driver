package com.facebook.a.a.a;

import android.support.annotation.Nullable;
import android.support.v4.view.NestedScrollingChild;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import com.facebook.internal.u;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import com.google.android.exoplayer.util.MimeTypes;
import com.google.android.gms.plus.PlusShare;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1026a = f.class.getCanonicalName();
    private static WeakReference<View> b = new WeakReference<>(null);
    @Nullable
    private static Method c = null;

    public static List<View> a(View view) {
        ArrayList arrayList = new ArrayList();
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                arrayList.add(viewGroup.getChildAt(i));
            }
        }
        return arrayList;
    }

    public static JSONObject a(View view, JSONObject jSONObject) {
        try {
            String d = d(view);
            String e = e(view);
            Object tag = view.getTag();
            CharSequence contentDescription = view.getContentDescription();
            jSONObject.put("classname", view.getClass().getCanonicalName());
            jSONObject.put("classtypebitmask", j(view));
            jSONObject.put(TtmlNode.ATTR_ID, view.getId());
            if (!d.a(view)) {
                jSONObject.put(MimeTypes.BASE_TYPE_TEXT, d);
            } else {
                jSONObject.put(MimeTypes.BASE_TYPE_TEXT, "");
                jSONObject.put("is_user_input", true);
            }
            jSONObject.put("hint", e);
            if (tag != null) {
                jSONObject.put("tag", tag.toString());
            }
            if (contentDescription != null) {
                jSONObject.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, contentDescription.toString());
            }
            jSONObject.put("dimension", l(view));
        } catch (JSONException e2) {
            u.a(f1026a, (Exception) e2);
        }
        return jSONObject;
    }

    public static JSONObject b(View view) {
        JSONException e;
        if (view.getClass().getName().equals("com.facebook.react.ReactRootView")) {
            b = new WeakReference<>(view);
        }
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject a2 = a(view, jSONObject);
            try {
                JSONArray jSONArray = new JSONArray();
                List<View> a3 = a(view);
                for (int i = 0; i < a3.size(); i++) {
                    jSONArray.put(b(a3.get(i)));
                }
                a2.put("childviews", jSONArray);
                return a2;
            } catch (JSONException e2) {
                e = e2;
                jSONObject = a2;
                Log.e(f1026a, "Failed to create JSONObject for view.", e);
                return jSONObject;
            }
        } catch (JSONException e3) {
            e = e3;
            Log.e(f1026a, "Failed to create JSONObject for view.", e);
            return jSONObject;
        }
    }

    private static int j(View view) {
        int i;
        int i2 = 0;
        if (view instanceof ImageView) {
            i2 = 2;
        }
        if (c(view)) {
            i2 |= 32;
        }
        if (k(view)) {
            i = i2 | 512;
        } else {
            i = i2;
        }
        if (view instanceof TextView) {
            int i3 = i | 1024 | 1;
            if (view instanceof Button) {
                i3 |= 4;
                if (view instanceof Switch) {
                    i3 |= 8192;
                } else if (view instanceof CheckBox) {
                    i3 |= 32768;
                }
            }
            if (view instanceof EditText) {
                return i3 | 2048;
            }
            return i3;
        } else if ((view instanceof Spinner) || (view instanceof DatePicker)) {
            return i | 4096;
        } else {
            if (view instanceof RatingBar) {
                return i | 65536;
            }
            if (view instanceof RadioGroup) {
                return i | 16384;
            }
            if (!(view instanceof ViewGroup) || !a(view, b.get())) {
                return i;
            }
            return i | 64;
        }
    }

    public static boolean c(View view) {
        Field declaredField;
        try {
            Field declaredField2 = Class.forName("android.view.View").getDeclaredField("mListenerInfo");
            if (declaredField2 == null) {
                return false;
            }
            declaredField2.setAccessible(true);
            Object obj = declaredField2.get(view);
            if (obj == null || (declaredField = Class.forName("android.view.View$ListenerInfo").getDeclaredField("mOnClickListener")) == null) {
                return false;
            }
            return ((View.OnClickListener) declaredField.get(obj)) != null;
        } catch (Exception e) {
            Log.e(f1026a, "Failed to check if the view is clickable.", e);
            return false;
        }
    }

    private static boolean k(View view) {
        ViewParent parent = view.getParent();
        return (parent instanceof AdapterView) || (parent instanceof NestedScrollingChild);
    }

    public static String d(View view) {
        String valueOf;
        Object selectedItem;
        CharSequence charSequence = null;
        if (view instanceof TextView) {
            valueOf = ((TextView) view).getText();
            if (view instanceof Switch) {
                valueOf = ((Switch) view).isChecked() ? "1" : "0";
            }
        } else if (view instanceof Spinner) {
            valueOf = (((Spinner) view).getCount() <= 0 || (selectedItem = ((Spinner) view).getSelectedItem()) == null) ? null : selectedItem.toString();
        } else if (view instanceof DatePicker) {
            DatePicker datePicker = (DatePicker) view;
            valueOf = String.format("%04d-%02d-%02d", Integer.valueOf(datePicker.getYear()), Integer.valueOf(datePicker.getMonth()), Integer.valueOf(datePicker.getDayOfMonth()));
        } else if (view instanceof TimePicker) {
            TimePicker timePicker = (TimePicker) view;
            valueOf = String.format("%02d:%02d", Integer.valueOf(timePicker.getCurrentHour().intValue()), Integer.valueOf(timePicker.getCurrentMinute().intValue()));
        } else if (view instanceof RadioGroup) {
            RadioGroup radioGroup = (RadioGroup) view;
            int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
            int childCount = radioGroup.getChildCount();
            int i = 0;
            while (true) {
                if (i >= childCount) {
                    break;
                }
                View childAt = radioGroup.getChildAt(i);
                if (childAt.getId() == checkedRadioButtonId && (childAt instanceof RadioButton)) {
                    charSequence = ((RadioButton) childAt).getText();
                    break;
                }
                i++;
            }
            valueOf = charSequence;
        } else {
            valueOf = view instanceof RatingBar ? String.valueOf(((RatingBar) view).getRating()) : null;
        }
        if (valueOf == null) {
            return "";
        }
        return valueOf.toString();
    }

    public static String e(View view) {
        CharSequence charSequence = null;
        if (view instanceof EditText) {
            charSequence = ((EditText) view).getHint();
        } else if (view instanceof TextView) {
            charSequence = ((TextView) view).getHint();
        }
        return charSequence == null ? "" : charSequence.toString();
    }

    private static JSONObject l(View view) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("top", view.getTop());
            jSONObject.put(TtmlNode.LEFT, view.getLeft());
            jSONObject.put("width", view.getWidth());
            jSONObject.put("height", view.getHeight());
            jSONObject.put("scrollx", view.getScrollX());
            jSONObject.put("scrolly", view.getScrollY());
            jSONObject.put("visibility", view.getVisibility());
        } catch (JSONException e) {
            Log.e(f1026a, "Failed to create JSONObject for dimension.", e);
        }
        return jSONObject;
    }

    @Nullable
    public static View.AccessibilityDelegate f(View view) {
        try {
            return (View.AccessibilityDelegate) view.getClass().getMethod("getAccessibilityDelegate", new Class[0]).invoke(view, new Object[0]);
        } catch (IllegalAccessException | NoSuchMethodException | NullPointerException | SecurityException | InvocationTargetException e) {
            return null;
        }
    }

    @Nullable
    public static View.OnTouchListener g(View view) {
        View.OnTouchListener onTouchListener;
        try {
            Field declaredField = Class.forName("android.view.View").getDeclaredField("mListenerInfo");
            if (declaredField != null) {
                declaredField.setAccessible(true);
            }
            Object obj = declaredField.get(view);
            if (obj == null) {
                return null;
            }
            Field declaredField2 = Class.forName("android.view.View$ListenerInfo").getDeclaredField("mOnTouchListener");
            if (declaredField2 != null) {
                declaredField2.setAccessible(true);
                onTouchListener = (View.OnTouchListener) declaredField2.get(obj);
            } else {
                onTouchListener = null;
            }
            return onTouchListener;
        } catch (NoSuchFieldException e) {
            u.a(f1026a, (Exception) e);
            return null;
        } catch (ClassNotFoundException e2) {
            u.a(f1026a, (Exception) e2);
            return null;
        } catch (IllegalAccessException e3) {
            u.a(f1026a, (Exception) e3);
            return null;
        }
    }

    @Nullable
    public static View a(float[] fArr, @Nullable View view) {
        a();
        if (c == null || view == null) {
            return null;
        }
        try {
            View view2 = (View) c.invoke(null, fArr, view);
            if (view2 == null || view2.getId() <= 0) {
                return null;
            }
            View view3 = (View) view2.getParent();
            if (view3 == null) {
                view3 = null;
            }
            return view3;
        } catch (IllegalAccessException e) {
            u.a(f1026a, (Exception) e);
            return null;
        } catch (InvocationTargetException e2) {
            u.a(f1026a, (Exception) e2);
            return null;
        }
    }

    public static boolean a(View view, @Nullable View view2) {
        View a2;
        if (!view.getClass().getName().equals("com.facebook.react.views.view.ReactViewGroup") || (a2 = a(m(view), view2)) == null || a2.getId() != view.getId()) {
            return false;
        }
        return true;
    }

    public static boolean h(View view) {
        return view.getClass().getName().equals("com.facebook.react.ReactRootView");
    }

    @Nullable
    public static View i(View view) {
        View view2 = view;
        while (view2 != null) {
            if (!h(view2)) {
                ViewParent parent = view2.getParent();
                if (!(parent instanceof View)) {
                    break;
                }
                view2 = (View) parent;
            } else {
                return view2;
            }
        }
        return null;
    }

    private static float[] m(View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        return new float[]{(float) iArr[0], (float) iArr[1]};
    }

    private static void a() {
        if (c == null) {
            try {
                c = Class.forName("com.facebook.react.uimanager.TouchTargetHelper").getDeclaredMethod("findTouchTargetView", float[].class, ViewGroup.class);
                c.setAccessible(true);
            } catch (ClassNotFoundException e) {
                u.a(f1026a, (Exception) e);
            } catch (NoSuchMethodException e2) {
                u.a(f1026a, (Exception) e2);
            }
        }
    }
}
