package com.facebook.a.a.a;

import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;

public class d {
    public static boolean a(View view) {
        if (!(view instanceof TextView)) {
            return false;
        }
        TextView textView = (TextView) view;
        if (a(textView) || f(textView) || c(textView) || d(textView) || e(textView) || b(textView)) {
            return true;
        }
        return false;
    }

    private static boolean a(TextView textView) {
        if (textView.getInputType() == 128) {
            return true;
        }
        return textView.getTransformationMethod() instanceof PasswordTransformationMethod;
    }

    private static boolean b(TextView textView) {
        if (textView.getInputType() == 32) {
            return true;
        }
        String d = f.d(textView);
        if (d == null || d.length() == 0) {
            return false;
        }
        return Patterns.EMAIL_ADDRESS.matcher(d).matches();
    }

    private static boolean c(TextView textView) {
        return textView.getInputType() == 96;
    }

    private static boolean d(TextView textView) {
        return textView.getInputType() == 112;
    }

    private static boolean e(TextView textView) {
        return textView.getInputType() == 3;
    }

    private static boolean f(TextView textView) {
        boolean z;
        boolean z2 = true;
        String replaceAll = f.d(textView).replaceAll("\\s", "");
        int length = replaceAll.length();
        if (length < 12 || length > 19) {
            return false;
        }
        int i = length - 1;
        boolean z3 = false;
        int i2 = 0;
        while (i >= 0) {
            char charAt = replaceAll.charAt(i);
            if (charAt < '0' || charAt > '9') {
                return false;
            }
            int i3 = charAt - '0';
            if (z3 && (i3 = i3 * 2) > 9) {
                i3 = (i3 % 10) + 1;
            }
            i2 += i3;
            if (!z3) {
                z = true;
            } else {
                z = false;
            }
            i--;
            z3 = z;
        }
        if (i2 % 10 != 0) {
            z2 = false;
        }
        return z2;
    }
}
