package com.facebook.a.a.a;

import com.google.android.exoplayer.text.ttml.TtmlNode;
import com.google.android.exoplayer.util.MimeTypes;
import com.google.android.gms.plus.PlusShare;
import org.json.JSONObject;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public final String f1023a;
    public final int b;
    public final int c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    public final int h;

    public enum a {
        ID(1),
        TEXT(2),
        TAG(4),
        DESCRIPTION(8),
        HINT(16);
        
        private final int f;

        private a(int i) {
            this.f = i;
        }

        public int a() {
            return this.f;
        }
    }

    c(JSONObject jSONObject) {
        this.f1023a = jSONObject.getString("class_name");
        this.b = jSONObject.optInt("index", -1);
        this.c = jSONObject.optInt(TtmlNode.ATTR_ID);
        this.d = jSONObject.optString(MimeTypes.BASE_TYPE_TEXT);
        this.e = jSONObject.optString("tag");
        this.f = jSONObject.optString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION);
        this.g = jSONObject.optString("hint");
        this.h = jSONObject.optInt("match_bitmask");
    }
}
