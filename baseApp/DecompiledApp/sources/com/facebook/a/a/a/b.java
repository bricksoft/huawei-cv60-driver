package com.facebook.a.a.a;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public final String f1022a;
    public final String b;
    public final List<c> c;
    public final String d;

    public b(JSONObject jSONObject) {
        this.f1022a = jSONObject.getString("name");
        this.b = jSONObject.optString("value");
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("path");
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                arrayList.add(new c(optJSONArray.getJSONObject(i)));
            }
        }
        this.c = arrayList;
        this.d = jSONObject.optString("path_type", "absolute");
    }
}
