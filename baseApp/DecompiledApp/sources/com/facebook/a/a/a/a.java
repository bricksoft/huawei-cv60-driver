package com.facebook.a.a.a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f1018a;
    private final b b;
    private final EnumC0045a c;
    private final String d;
    private final List<c> e;
    private final List<b> f;
    private final String g;
    private final String h;
    private final String i;

    /* renamed from: com.facebook.a.a.a.a$a  reason: collision with other inner class name */
    public enum EnumC0045a {
        CLICK,
        SELECTED,
        TEXT_CHANGED
    }

    public enum b {
        MANUAL,
        INFERENCE
    }

    public a(String str, b bVar, EnumC0045a aVar, String str2, List<c> list, List<b> list2, String str3, String str4, String str5) {
        this.f1018a = str;
        this.b = bVar;
        this.c = aVar;
        this.d = str2;
        this.e = list;
        this.f = list2;
        this.g = str3;
        this.h = str4;
        this.i = str5;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x000e A[Catch:{ IllegalArgumentException | JSONException -> 0x001e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.facebook.a.a.a.a> a(org.json.JSONArray r4) {
        /*
            r1 = 0
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            if (r4 == 0) goto L_0x001c
            int r0 = r4.length()     // Catch:{ JSONException -> 0x0020, IllegalArgumentException -> 0x001e }
        L_0x000c:
            if (r1 >= r0) goto L_0x001f
            org.json.JSONObject r3 = r4.getJSONObject(r1)     // Catch:{ JSONException -> 0x0020, IllegalArgumentException -> 0x001e }
            com.facebook.a.a.a.a r3 = a(r3)     // Catch:{ JSONException -> 0x0020, IllegalArgumentException -> 0x001e }
            r2.add(r3)     // Catch:{ JSONException -> 0x0020, IllegalArgumentException -> 0x001e }
            int r1 = r1 + 1
            goto L_0x000c
        L_0x001c:
            r0 = r1
            goto L_0x000c
        L_0x001e:
            r0 = move-exception
        L_0x001f:
            return r2
        L_0x0020:
            r0 = move-exception
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.a.a.a.a.a(org.json.JSONArray):java.util.List");
    }

    public static a a(JSONObject jSONObject) {
        String string = jSONObject.getString("event_name");
        b valueOf = b.valueOf(jSONObject.getString("method").toUpperCase(Locale.ENGLISH));
        EnumC0045a valueOf2 = EnumC0045a.valueOf(jSONObject.getString("event_type").toUpperCase(Locale.ENGLISH));
        String string2 = jSONObject.getString("app_version");
        JSONArray jSONArray = jSONObject.getJSONArray("path");
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            arrayList.add(new c(jSONArray.getJSONObject(i2)));
        }
        String optString = jSONObject.optString("path_type", "absolute");
        JSONArray optJSONArray = jSONObject.optJSONArray("parameters");
        ArrayList arrayList2 = new ArrayList();
        if (optJSONArray != null) {
            for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                arrayList2.add(new b(optJSONArray.getJSONObject(i3)));
            }
        }
        return new a(string, valueOf, valueOf2, string2, arrayList, arrayList2, jSONObject.optString("component_id"), optString, jSONObject.optString("activity_name"));
    }

    public List<c> a() {
        return Collections.unmodifiableList(this.e);
    }

    public List<b> b() {
        return Collections.unmodifiableList(this.f);
    }

    public String c() {
        return this.f1018a;
    }

    public EnumC0045a d() {
        return this.c;
    }

    public String e() {
        return this.i;
    }
}
