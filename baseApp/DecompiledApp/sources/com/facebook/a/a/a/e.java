package com.facebook.a.a.a;

import android.util.Log;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1025a = e.class.getCanonicalName();
    private static Class<?> b;

    public static void a(String str, String str2, String str3) {
        try {
            if (b == null) {
                b = Class.forName("com.unity3d.player.UnityPlayer");
            }
            b.getMethod("UnitySendMessage", String.class, String.class, String.class).invoke(b, str, str2, str3);
        } catch (Exception e) {
            Log.e(f1025a, "Failed to send message to Unity", e);
        }
    }

    public static void a() {
        a("UnityFacebookSDKPlugin", "CaptureViewHierarchy", "");
    }

    public static void a(String str) {
        a("UnityFacebookSDKPlugin", "OnReceiveMapping", str);
    }
}
