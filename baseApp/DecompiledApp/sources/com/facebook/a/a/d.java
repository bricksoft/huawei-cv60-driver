package com.facebook.a.a;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.a.a.a.e;
import com.facebook.a.a.a.f;
import com.facebook.a.b.b;
import com.facebook.internal.m;
import com.facebook.internal.o;
import com.facebook.internal.u;
import com.facebook.k;
import com.facebook.p;
import com.facebook.t;
import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1034a = d.class.getCanonicalName();
    private static d f;
    private final Handler b = new Handler(Looper.getMainLooper());
    private WeakReference<Activity> c;
    private Timer d;
    private String e = null;

    public d(Activity activity) {
        this.c = new WeakReference<>(activity);
        f = this;
    }

    public void a() {
        final Activity activity = this.c.get();
        if (activity != null) {
            final String simpleName = activity.getClass().getSimpleName();
            final AnonymousClass1 r2 = new TimerTask() {
                /* class com.facebook.a.a.d.AnonymousClass1 */

                public void run() {
                    String str;
                    try {
                        View rootView = activity.getWindow().getDecorView().getRootView();
                        if (com.facebook.a.b.a.d()) {
                            if (m.b()) {
                                e.a();
                                return;
                            }
                            FutureTask futureTask = new FutureTask(new a(rootView));
                            d.this.b.post(futureTask);
                            try {
                                str = (String) futureTask.get(1, TimeUnit.SECONDS);
                            } catch (Exception e) {
                                Log.e(d.f1034a, "Failed to take screenshot.", e);
                                str = "";
                            }
                            JSONObject jSONObject = new JSONObject();
                            try {
                                jSONObject.put("screenname", simpleName);
                                jSONObject.put("screenshot", str);
                                JSONArray jSONArray = new JSONArray();
                                jSONArray.put(f.b(rootView));
                                jSONObject.put("view", jSONArray);
                            } catch (JSONException e2) {
                                Log.e(d.f1034a, "Failed to create JSONObject");
                            }
                            d.this.a((d) jSONObject.toString());
                        }
                    } catch (Exception e3) {
                        Log.e(d.f1034a, "UI Component tree indexing failure!", e3);
                    }
                }
            };
            k.d().execute(new Runnable() {
                /* class com.facebook.a.a.d.AnonymousClass2 */

                public void run() {
                    try {
                        if (d.this.d != null) {
                            d.this.d.cancel();
                        }
                        d.this.e = null;
                        d.this.d = new Timer();
                        d.this.d.scheduleAtFixedRate(r2, 0, 1000);
                    } catch (Exception e) {
                        Log.e(d.f1034a, "Error scheduling indexing job", e);
                    }
                }
            });
        }
    }

    public void b() {
        if (this.c.get() != null && this.d != null) {
            try {
                this.d.cancel();
                this.d = null;
            } catch (Exception e2) {
                Log.e(f1034a, "Error unscheduling indexing job", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(final String str) {
        k.d().execute(new Runnable() {
            /* class com.facebook.a.a.d.AnonymousClass3 */

            public void run() {
                GraphRequest a2;
                String b2 = u.b(str);
                AccessToken a3 = AccessToken.a();
                if ((b2 == null || !b2.equals(d.this.e)) && (a2 = d.a(str, a3, k.j(), "app_indexing")) != null) {
                    p i = a2.i();
                    try {
                        JSONObject b3 = i.b();
                        if (b3 != null) {
                            if ("true".equals(b3.optString("success"))) {
                                o.a(t.APP_EVENTS, d.f1034a, "Successfully send UI component tree to server");
                                d.this.e = b2;
                            }
                            if (b3.has("is_app_indexing_enabled")) {
                                com.facebook.a.b.a.a(Boolean.valueOf(b3.getBoolean("is_app_indexing_enabled")));
                                return;
                            }
                            return;
                        }
                        Log.e(d.f1034a, "Error sending UI component tree to Facebook: " + i.a());
                    } catch (JSONException e) {
                        Log.e(d.f1034a, "Error decoding server response.", e);
                    }
                }
            }
        });
    }

    @Nullable
    public static GraphRequest a(String str, AccessToken accessToken, String str2, String str3) {
        if (str == null) {
            return null;
        }
        GraphRequest a2 = GraphRequest.a(accessToken, String.format(Locale.US, "%s/app_indexing", str2), (JSONObject) null, (GraphRequest.b) null);
        Bundle e2 = a2.e();
        if (e2 == null) {
            e2 = new Bundle();
        }
        e2.putString("tree", str);
        e2.putString("app_version", b.d());
        e2.putString("platform", "android");
        e2.putString("request_type", str3);
        if (str3.equals("app_indexing")) {
            e2.putString("device_session_id", com.facebook.a.b.a.c());
        }
        a2.a(e2);
        a2.a((GraphRequest.b) new GraphRequest.b() {
            /* class com.facebook.a.a.d.AnonymousClass4 */

            @Override // com.facebook.GraphRequest.b
            public void a(p pVar) {
                o.a(t.APP_EVENTS, d.f1034a, "App index sent to FB!");
            }
        });
        return a2;
    }

    private static class a implements Callable<String> {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<View> f1038a;

        a(View view) {
            this.f1038a = new WeakReference<>(view);
        }

        /* renamed from: a */
        public String call() {
            View view = this.f1038a.get();
            if (view == null || view.getWidth() == 0 || view.getHeight() == 0) {
                return "";
            }
            Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.RGB_565);
            view.draw(new Canvas(createBitmap));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            createBitmap.compress(Bitmap.CompressFormat.JPEG, 10, byteArrayOutputStream);
            return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 2);
        }
    }
}
