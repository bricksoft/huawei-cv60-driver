package com.facebook.a.a;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.a.a.a.f;
import com.facebook.a.b.b;
import com.facebook.a.g;
import com.facebook.k;
import java.lang.ref.WeakReference;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1031a = c.class.getCanonicalName();

    public static a a(com.facebook.a.a.a.a aVar, View view, View view2) {
        return new a(aVar, view, view2);
    }

    public static class a implements View.OnTouchListener {

        /* renamed from: a  reason: collision with root package name */
        private com.facebook.a.a.a.a f1032a;
        private WeakReference<View> b;
        private WeakReference<View> c;
        @Nullable
        private View.OnTouchListener d;
        private boolean e = false;

        public a(com.facebook.a.a.a.a aVar, View view, View view2) {
            if (aVar != null && view != null && view2 != null) {
                this.d = f.g(view2);
                this.f1032a = aVar;
                this.b = new WeakReference<>(view2);
                this.c = new WeakReference<>(view);
                this.e = true;
            }
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                b();
            }
            if (this.d == null || !this.d.onTouch(view, motionEvent)) {
                return false;
            }
            return true;
        }

        private void b() {
            if (this.f1032a != null) {
                final String c2 = this.f1032a.c();
                final Bundle a2 = b.a(this.f1032a, this.c.get(), this.b.get());
                if (a2.containsKey("_valueToSum")) {
                    a2.putDouble("_valueToSum", b.a(a2.getString("_valueToSum")));
                }
                a2.putString("_is_fb_codeless", "1");
                k.d().execute(new Runnable() {
                    /* class com.facebook.a.a.c.a.AnonymousClass1 */

                    public void run() {
                        g.a(k.f()).a(c2, a2);
                    }
                });
            }
        }

        public boolean a() {
            return this.e;
        }
    }
}
