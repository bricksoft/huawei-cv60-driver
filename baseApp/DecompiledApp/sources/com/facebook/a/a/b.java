package com.facebook.a.a;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import com.facebook.a.a.a;
import com.facebook.a.a.a.c;
import com.facebook.a.a.a.f;
import com.facebook.a.a.c;
import com.facebook.h;
import com.facebook.internal.k;
import com.facebook.internal.l;
import com.facebook.internal.m;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1027a = b.class.getCanonicalName();
    private final Handler b = new Handler(Looper.getMainLooper());
    private Set<Activity> c = new HashSet();
    private Set<ViewTreeObserver$OnGlobalLayoutListenerC0046b> d = new HashSet();
    private HashMap<String, String> e = new HashMap<>();

    public void a(Activity activity) {
        if (!m.b()) {
            if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
                throw new h("Can't add activity to CodelessMatcher on non-UI thread");
            }
            this.c.add(activity);
            this.e.clear();
            b();
        }
    }

    public void b(Activity activity) {
        if (!m.b()) {
            if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
                throw new h("Can't remove activity from CodelessMatcher on non-UI thread");
            }
            this.c.remove(activity);
            this.d.clear();
            this.e.clear();
        }
    }

    public static Bundle a(com.facebook.a.a.a.a aVar, View view, View view2) {
        List<a> a2;
        Bundle bundle = new Bundle();
        if (aVar == null) {
            return bundle;
        }
        List<com.facebook.a.a.a.b> b2 = aVar.b();
        if (b2 != null) {
            for (com.facebook.a.a.a.b bVar : b2) {
                if (bVar.b != null && bVar.b.length() > 0) {
                    bundle.putString(bVar.f1022a, bVar.b);
                } else if (bVar.c.size() > 0) {
                    if (bVar.d.equals("relative")) {
                        a2 = ViewTreeObserver$OnGlobalLayoutListenerC0046b.a(aVar, view2, bVar.c, 0, -1, view2.getClass().getSimpleName());
                    } else {
                        a2 = ViewTreeObserver$OnGlobalLayoutListenerC0046b.a(aVar, view, bVar.c, 0, -1, view.getClass().getSimpleName());
                    }
                    Iterator<a> it = a2.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        a next = it.next();
                        if (next.a() != null) {
                            String d2 = f.d(next.a());
                            if (d2.length() > 0) {
                                bundle.putString(bVar.f1022a, d2);
                                break;
                            }
                        }
                    }
                }
            }
        }
        return bundle;
    }

    private void b() {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            c();
        } else {
            this.b.post(new Runnable() {
                /* class com.facebook.a.a.b.AnonymousClass1 */

                public void run() {
                    b.this.c();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void c() {
        for (Activity activity : this.c) {
            this.d.add(new ViewTreeObserver$OnGlobalLayoutListenerC0046b(activity.getWindow().getDecorView().getRootView(), this.b, this.e, activity.getClass().getSimpleName()));
        }
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<View> f1029a;
        private String b;

        public a(View view, String str) {
            this.f1029a = new WeakReference<>(view);
            this.b = str;
        }

        @Nullable
        public View a() {
            if (this.f1029a == null) {
                return null;
            }
            return this.f1029a.get();
        }

        public String b() {
            return this.b;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: com.facebook.a.a.b$b  reason: collision with other inner class name */
    public static class ViewTreeObserver$OnGlobalLayoutListenerC0046b implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener, Runnable {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<View> f1030a;
        @Nullable
        private List<com.facebook.a.a.a.a> b;
        private final Handler c;
        private HashMap<String, String> d;
        private final String e;

        public ViewTreeObserver$OnGlobalLayoutListenerC0046b(View view, Handler handler, HashMap<String, String> hashMap, String str) {
            this.f1030a = new WeakReference<>(view);
            this.c = handler;
            this.d = hashMap;
            this.e = str;
            this.c.postDelayed(this, 200);
        }

        public void run() {
            View view;
            k a2 = l.a(com.facebook.k.j());
            if (a2 != null && a2.h()) {
                this.b = com.facebook.a.a.a.a.a(a2.i());
                if (this.b != null && (view = this.f1030a.get()) != null) {
                    ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
                    if (viewTreeObserver.isAlive()) {
                        viewTreeObserver.addOnGlobalLayoutListener(this);
                        viewTreeObserver.addOnScrollChangedListener(this);
                    }
                    a();
                }
            }
        }

        public void onGlobalLayout() {
            a();
        }

        public void onScrollChanged() {
            a();
        }

        private void a() {
            if (!(this.b == null || this.f1030a.get() == null)) {
                for (int i = 0; i < this.b.size(); i++) {
                    a(this.b.get(i), this.f1030a.get());
                }
            }
        }

        public void a(com.facebook.a.a.a.a aVar, View view) {
            if (!(aVar == null || view == null)) {
                if (TextUtils.isEmpty(aVar.e()) || aVar.e().equals(this.e)) {
                    List<c> a2 = aVar.a();
                    if (a2.size() <= 25) {
                        for (a aVar2 : a(aVar, view, a2, 0, -1, this.e)) {
                            a(aVar2, view, aVar);
                        }
                    }
                }
            }
        }

        public static List<a> a(com.facebook.a.a.a.a aVar, View view, List<c> list, int i, int i2, String str) {
            int i3 = 0;
            String str2 = str + "." + String.valueOf(i2);
            ArrayList arrayList = new ArrayList();
            if (view == null) {
                return arrayList;
            }
            if (i >= list.size()) {
                arrayList.add(new a(view, str2));
            } else {
                c cVar = list.get(i);
                if (cVar.f1023a.equals("..")) {
                    ViewParent parent = view.getParent();
                    if (parent instanceof ViewGroup) {
                        List<View> a2 = a((ViewGroup) parent);
                        int size = a2.size();
                        while (i3 < size) {
                            arrayList.addAll(a(aVar, a2.get(i3), list, i + 1, i3, str2));
                            i3++;
                        }
                    }
                    return arrayList;
                } else if (cVar.f1023a.equals(".")) {
                    arrayList.add(new a(view, str2));
                    return arrayList;
                } else if (!a(view, cVar, i2)) {
                    return arrayList;
                } else {
                    if (i == list.size() - 1) {
                        arrayList.add(new a(view, str2));
                    }
                }
            }
            if (view instanceof ViewGroup) {
                List<View> a3 = a((ViewGroup) view);
                int size2 = a3.size();
                while (i3 < size2) {
                    arrayList.addAll(a(aVar, a3.get(i3), list, i + 1, i3, str2));
                    i3++;
                }
            }
            return arrayList;
        }

        private static boolean a(View view, c cVar, int i) {
            String valueOf;
            String valueOf2;
            if (cVar.b != -1 && i != cVar.b) {
                return false;
            }
            if (!view.getClass().getCanonicalName().equals(cVar.f1023a)) {
                if (!cVar.f1023a.matches(".*android\\..*")) {
                    return false;
                }
                String[] split = cVar.f1023a.split("\\.");
                if (split.length <= 0) {
                    return false;
                }
                if (!view.getClass().getSimpleName().equals(split[split.length - 1])) {
                    return false;
                }
            }
            if ((cVar.h & c.a.ID.a()) > 0 && cVar.c != view.getId()) {
                return false;
            }
            if ((cVar.h & c.a.TEXT.a()) > 0 && !cVar.d.equals(f.d(view))) {
                return false;
            }
            if ((cVar.h & c.a.DESCRIPTION.a()) > 0) {
                String str = cVar.f;
                if (view.getContentDescription() == null) {
                    valueOf2 = "";
                } else {
                    valueOf2 = String.valueOf(view.getContentDescription());
                }
                if (!str.equals(valueOf2)) {
                    return false;
                }
            }
            if ((cVar.h & c.a.HINT.a()) > 0 && !cVar.g.equals(f.e(view))) {
                return false;
            }
            if ((cVar.h & c.a.TAG.a()) > 0) {
                String str2 = cVar.e;
                if (view.getTag() == null) {
                    valueOf = "";
                } else {
                    valueOf = String.valueOf(view.getTag());
                }
                if (!str2.equals(valueOf)) {
                    return false;
                }
            }
            return true;
        }

        private static List<View> a(ViewGroup viewGroup) {
            ArrayList arrayList = new ArrayList();
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup.getChildAt(i);
                if (childAt.getVisibility() == 0) {
                    arrayList.add(childAt);
                }
            }
            return arrayList;
        }

        private void a(a aVar, View view, com.facebook.a.a.a.a aVar2) {
            boolean z;
            boolean z2;
            if (aVar2 != null) {
                try {
                    View a2 = aVar.a();
                    if (a2 != null) {
                        View i = f.i(a2);
                        if (i != null && f.a(a2, i)) {
                            a(aVar, view, i, aVar2);
                        } else if (!a2.getClass().getName().startsWith("com.facebook.react")) {
                            String b2 = aVar.b();
                            View.AccessibilityDelegate f = f.f(a2);
                            boolean z3 = f != null;
                            if (!z3 || !(f instanceof a.C0044a)) {
                                z = false;
                            } else {
                                z = true;
                            }
                            if (!z || !((a.C0044a) f).a()) {
                                z2 = false;
                            } else {
                                z2 = true;
                            }
                            if (this.d.containsKey(b2)) {
                                return;
                            }
                            if (!z3 || !z || !z2) {
                                a2.setAccessibilityDelegate(a.a(aVar2, view, a2));
                                this.d.put(b2, aVar2.c());
                            }
                        }
                    }
                } catch (h e2) {
                    Log.e(b.f1027a, "Failed to attach auto logging event listener.", e2);
                }
            }
        }

        private void a(a aVar, View view, View view2, com.facebook.a.a.a.a aVar2) {
            View a2;
            boolean z;
            boolean z2;
            if (aVar2 != null && (a2 = aVar.a()) != null && f.a(a2, view2)) {
                String b2 = aVar.b();
                View.OnTouchListener g = f.g(a2);
                boolean z3 = g != null;
                if (!z3 || !(g instanceof c.a)) {
                    z = false;
                } else {
                    z = true;
                }
                if (!z || !((c.a) g).a()) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                if (this.d.containsKey(b2)) {
                    return;
                }
                if (!z3 || !z || !z2) {
                    a2.setOnTouchListener(c.a(aVar2, view, a2));
                    this.d.put(b2, aVar2.c());
                }
            }
        }
    }
}
