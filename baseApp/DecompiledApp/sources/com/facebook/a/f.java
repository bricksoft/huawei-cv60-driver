package com.facebook.a;

import android.content.Context;
import android.util.Log;
import com.facebook.a.a;
import com.facebook.a.b.b;
import com.facebook.a.c;
import com.facebook.internal.u;
import com.facebook.k;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;

class f {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1069a = f.class.getName();

    f() {
    }

    public static synchronized void a(a aVar, l lVar) {
        synchronized (f.class) {
            b.a();
            k a2 = a();
            if (a2.b(aVar)) {
                a2.a(aVar).addAll(lVar.b());
            } else {
                a2.a(aVar, lVar.b());
            }
            a(a2);
        }
    }

    public static synchronized void a(d dVar) {
        synchronized (f.class) {
            b.a();
            k a2 = a();
            for (a aVar : dVar.a()) {
                a2.a(aVar, dVar.a(aVar).b());
            }
            a(a2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized com.facebook.a.k a() {
        /*
        // Method dump skipped, instructions count: 156
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.a.f.a():com.facebook.a.k");
    }

    private static void a(k kVar) {
        ObjectOutputStream objectOutputStream;
        Throwable th;
        Exception e;
        Context f = k.f();
        try {
            objectOutputStream = new ObjectOutputStream(new BufferedOutputStream(f.openFileOutput("AppEventsLogger.persistedevents", 0)));
            try {
                objectOutputStream.writeObject(kVar);
                u.a(objectOutputStream);
            } catch (Exception e2) {
                e = e2;
                try {
                    Log.w(f1069a, "Got unexpected exception while persisting events: ", e);
                    try {
                        f.getFileStreamPath("AppEventsLogger.persistedevents").delete();
                    } catch (Exception e3) {
                    }
                    u.a(objectOutputStream);
                } catch (Throwable th2) {
                    th = th2;
                    u.a(objectOutputStream);
                    throw th;
                }
            }
        } catch (Exception e4) {
            e = e4;
            objectOutputStream = null;
            Log.w(f1069a, "Got unexpected exception while persisting events: ", e);
            f.getFileStreamPath("AppEventsLogger.persistedevents").delete();
            u.a(objectOutputStream);
        } catch (Throwable th3) {
            th = th3;
            objectOutputStream = null;
            u.a(objectOutputStream);
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public static class a extends ObjectInputStream {
        public a(InputStream inputStream) {
            super(inputStream);
        }

        /* access modifiers changed from: protected */
        @Override // java.io.ObjectInputStream
        public ObjectStreamClass readClassDescriptor() {
            ObjectStreamClass readClassDescriptor = super.readClassDescriptor();
            if (readClassDescriptor.getName().equals("com.facebook.appevents.AppEventsLogger$AccessTokenAppIdPair$SerializationProxyV1")) {
                return ObjectStreamClass.lookup(a.C0043a.class);
            }
            if (readClassDescriptor.getName().equals("com.facebook.appevents.AppEventsLogger$AppEvent$SerializationProxyV1")) {
                return ObjectStreamClass.lookup(c.a.class);
            }
            return readClassDescriptor;
        }
    }
}
