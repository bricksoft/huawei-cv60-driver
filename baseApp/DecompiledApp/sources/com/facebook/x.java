package com.facebook;

import android.os.Handler;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/* access modifiers changed from: package-private */
public class x extends OutputStream implements z {

    /* renamed from: a  reason: collision with root package name */
    private final Map<GraphRequest, aa> f1236a = new HashMap();
    private final Handler b;
    private GraphRequest c;
    private aa d;
    private int e;

    x(Handler handler) {
        this.b = handler;
    }

    @Override // com.facebook.z
    public void a(GraphRequest graphRequest) {
        this.c = graphRequest;
        this.d = graphRequest != null ? this.f1236a.get(graphRequest) : null;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public Map<GraphRequest, aa> b() {
        return this.f1236a;
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        if (this.d == null) {
            this.d = new aa(this.b, this.c);
            this.f1236a.put(this.c, this.d);
        }
        this.d.b(j);
        this.e = (int) (((long) this.e) + j);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr) {
        a((long) bArr.length);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        a((long) i2);
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        a(1);
    }
}
