package com.facebook.internal;

import android.net.Uri;
import java.util.EnumSet;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1114a;
    private String b;
    private boolean c;
    private boolean d;
    private int e;
    private EnumSet<t> f;
    private Map<String, Map<String, a>> g;
    private boolean h;
    private g i;
    private String j;
    private String k;
    private boolean l;
    private boolean m;
    private String n;
    private JSONArray o;
    private boolean p;

    public k(boolean z, String str, boolean z2, boolean z3, int i2, EnumSet<t> enumSet, Map<String, Map<String, a>> map, boolean z4, g gVar, String str2, String str3, boolean z5, boolean z6, JSONArray jSONArray, String str4, boolean z7) {
        this.f1114a = z;
        this.b = str;
        this.c = z2;
        this.d = z3;
        this.g = map;
        this.i = gVar;
        this.e = i2;
        this.h = z4;
        this.f = enumSet;
        this.j = str2;
        this.k = str3;
        this.l = z5;
        this.m = z6;
        this.o = jSONArray;
        this.n = str4;
        this.p = z7;
    }

    public boolean a() {
        return this.f1114a;
    }

    public boolean b() {
        return this.d;
    }

    public int c() {
        return this.e;
    }

    public boolean d() {
        return this.h;
    }

    public EnumSet<t> e() {
        return this.f;
    }

    public g f() {
        return this.i;
    }

    public boolean g() {
        return this.l;
    }

    public boolean h() {
        return this.m;
    }

    public JSONArray i() {
        return this.o;
    }

    public String j() {
        return this.n;
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private String f1115a;
        private String b;
        private Uri c;
        private int[] d;

        public static a a(JSONObject jSONObject) {
            Uri uri = null;
            String optString = jSONObject.optString("name");
            if (u.a(optString)) {
                return null;
            }
            String[] split = optString.split("\\|");
            if (split.length != 2) {
                return null;
            }
            String str = split[0];
            String str2 = split[1];
            if (u.a(str) || u.a(str2)) {
                return null;
            }
            String optString2 = jSONObject.optString("url");
            if (!u.a(optString2)) {
                uri = Uri.parse(optString2);
            }
            return new a(str, str2, uri, a(jSONObject.optJSONArray("versions")));
        }

        private static int[] a(JSONArray jSONArray) {
            if (jSONArray == null) {
                return null;
            }
            int length = jSONArray.length();
            int[] iArr = new int[length];
            for (int i = 0; i < length; i++) {
                int optInt = jSONArray.optInt(i, -1);
                if (optInt == -1) {
                    String optString = jSONArray.optString(i);
                    if (!u.a(optString)) {
                        try {
                            optInt = Integer.parseInt(optString);
                        } catch (NumberFormatException e) {
                            u.a("FacebookSDK", (Exception) e);
                            optInt = -1;
                        }
                    }
                }
                iArr[i] = optInt;
            }
            return iArr;
        }

        private a(String str, String str2, Uri uri, int[] iArr) {
            this.f1115a = str;
            this.b = str2;
            this.c = uri;
            this.d = iArr;
        }

        public String a() {
            return this.f1115a;
        }

        public String b() {
            return this.b;
        }
    }
}
