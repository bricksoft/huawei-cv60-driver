package com.facebook.internal;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.k;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1113a = j.class.getCanonicalName();
    private static final Map<String, JSONObject> b = new ConcurrentHashMap();
    @Nullable
    private static Long c = null;

    @Nullable
    public static JSONObject a(String str, boolean z) {
        if (!z && b.containsKey(str)) {
            return b.get(str);
        }
        JSONObject a2 = a(str);
        if (a2 == null) {
            return null;
        }
        Context f = k.f();
        f.getSharedPreferences("com.facebook.internal.preferences.APP_GATEKEEPERS", 0).edit().putString(String.format("com.facebook.internal.APP_GATEKEEPERS.%s", str), a2.toString()).apply();
        return a(str, a2);
    }

    @Nullable
    private static JSONObject a(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("platform", "android");
        bundle.putString("sdk_version", k.h());
        bundle.putString("fields", "gatekeepers");
        GraphRequest a2 = GraphRequest.a((AccessToken) null, String.format("%s/%s", str, "mobile_sdk_gk"), (GraphRequest.b) null);
        a2.a(true);
        a2.a(bundle);
        return a2.i().b();
    }

    private static synchronized JSONObject a(String str, JSONObject jSONObject) {
        JSONObject jSONObject2;
        synchronized (j.class) {
            if (b.containsKey(str)) {
                jSONObject2 = b.get(str);
            } else {
                jSONObject2 = new JSONObject();
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("data");
            JSONObject jSONObject3 = null;
            if (optJSONArray != null) {
                jSONObject3 = optJSONArray.optJSONObject(0);
            }
            if (!(jSONObject3 == null || jSONObject3.optJSONArray("gatekeepers") == null)) {
                JSONArray optJSONArray2 = jSONObject3.optJSONArray("gatekeepers");
                for (int i = 0; i < optJSONArray2.length(); i++) {
                    try {
                        JSONObject jSONObject4 = optJSONArray2.getJSONObject(i);
                        jSONObject2.put(jSONObject4.getString("key"), jSONObject4.getBoolean("value"));
                    } catch (JSONException e) {
                        u.a("FacebookSDK", (Exception) e);
                    }
                }
            }
            b.put(str, jSONObject2);
        }
        return jSONObject2;
    }
}
