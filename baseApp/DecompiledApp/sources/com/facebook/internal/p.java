package com.facebook.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.h;
import com.facebook.j;
import com.facebook.k;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1125a = p.class.getName();
    private static final List<e> b = e();
    private static final List<e> c = f();
    private static final Map<String, List<e>> d = g();
    private static final AtomicBoolean e = new AtomicBoolean(false);
    private static final List<Integer> f = Arrays.asList(20170417, 20160327, 20141218, 20141107, 20141028, 20141001, 20140701, 20140324, 20140204, 20131107, 20130618, 20130502, 20121101);

    /* access modifiers changed from: private */
    public static abstract class e {

        /* renamed from: a  reason: collision with root package name */
        private TreeSet<Integer> f1126a;

        /* access modifiers changed from: protected */
        public abstract String a();

        /* access modifiers changed from: protected */
        public abstract String b();

        private e() {
        }

        public TreeSet<Integer> c() {
            if (this.f1126a == null || this.f1126a.isEmpty()) {
                a(false);
            }
            return this.f1126a;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x000d, code lost:
            if (r1.f1126a.isEmpty() == false) goto L_0x0015;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private synchronized void a(boolean r2) {
            /*
                r1 = this;
                monitor-enter(r1)
                if (r2 != 0) goto L_0x000f
                java.util.TreeSet<java.lang.Integer> r0 = r1.f1126a     // Catch:{ all -> 0x0017 }
                if (r0 == 0) goto L_0x000f
                java.util.TreeSet<java.lang.Integer> r0 = r1.f1126a     // Catch:{ all -> 0x0017 }
                boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0017 }
                if (r0 == 0) goto L_0x0015
            L_0x000f:
                java.util.TreeSet r0 = com.facebook.internal.p.a(r1)     // Catch:{ all -> 0x0017 }
                r1.f1126a = r0     // Catch:{ all -> 0x0017 }
            L_0x0015:
                monitor-exit(r1)
                return
            L_0x0017:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.internal.p.e.a(boolean):void");
        }
    }

    /* access modifiers changed from: private */
    public static class c extends e {
        private c() {
            super();
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String a() {
            return "com.facebook.katana";
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String b() {
            return "com.facebook.katana.ProxyAuth";
        }
    }

    /* access modifiers changed from: private */
    public static class d extends e {
        private d() {
            super();
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String a() {
            return "com.facebook.orca";
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String b() {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static class g extends e {
        private g() {
            super();
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String a() {
            return "com.facebook.wakizashi";
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String b() {
            return "com.facebook.katana.ProxyAuth";
        }
    }

    private static class b extends e {
        private b() {
            super();
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String a() {
            return "com.facebook.lite";
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String b() {
            return "com.facebook.lite.platform.LoginGDPDialogActivity";
        }
    }

    /* access modifiers changed from: private */
    public static class a extends e {
        private a() {
            super();
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String a() {
            return "com.facebook.arstudio.player";
        }

        /* access modifiers changed from: protected */
        @Override // com.facebook.internal.p.e
        public String b() {
            return null;
        }
    }

    private static List<e> e() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new c());
        arrayList.add(new g());
        return arrayList;
    }

    private static List<e> f() {
        ArrayList arrayList = new ArrayList(e());
        arrayList.add(0, new a());
        return arrayList;
    }

    private static Map<String, List<e>> g() {
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new d());
        hashMap.put("com.facebook.platform.action.request.OGACTIONPUBLISH_DIALOG", b);
        hashMap.put("com.facebook.platform.action.request.FEED_DIALOG", b);
        hashMap.put("com.facebook.platform.action.request.LIKE_DIALOG", b);
        hashMap.put("com.facebook.platform.action.request.APPINVITES_DIALOG", b);
        hashMap.put("com.facebook.platform.action.request.MESSAGE_DIALOG", arrayList);
        hashMap.put("com.facebook.platform.action.request.OGMESSAGEPUBLISH_DIALOG", arrayList);
        hashMap.put("com.facebook.platform.action.request.CAMERA_EFFECT", c);
        hashMap.put("com.facebook.platform.action.request.SHARE_STORY", b);
        return hashMap;
    }

    static Intent a(Context context, Intent intent, e eVar) {
        if (intent == null) {
            return null;
        }
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity == null) {
            return null;
        }
        if (!h.a(context, resolveActivity.activityInfo.packageName)) {
            return null;
        }
        return intent;
    }

    static Intent b(Context context, Intent intent, e eVar) {
        if (intent == null) {
            return null;
        }
        ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
        if (resolveService == null) {
            return null;
        }
        if (!h.a(context, resolveService.serviceInfo.packageName)) {
            return null;
        }
        return intent;
    }

    public static Intent a(Context context, String str, Collection<String> collection, String str2, boolean z, boolean z2, com.facebook.login.a aVar, String str3, String str4) {
        b bVar = new b();
        return a(context, a(bVar, str, collection, str2, z, z2, aVar, str3, str4), bVar);
    }

    private static Intent a(e eVar, String str, Collection<String> collection, String str2, boolean z, boolean z2, com.facebook.login.a aVar, String str3, String str4) {
        String b2 = eVar.b();
        if (b2 == null) {
            return null;
        }
        Intent putExtra = new Intent().setClassName(eVar.a(), b2).putExtra("client_id", str);
        putExtra.putExtra("facebook_sdk_version", k.h());
        if (!u.a(collection)) {
            putExtra.putExtra("scope", TextUtils.join(",", collection));
        }
        if (!u.a(str2)) {
            putExtra.putExtra("e2e", str2);
        }
        putExtra.putExtra("state", str3);
        putExtra.putExtra("response_type", "token,signed_request");
        putExtra.putExtra("return_scopes", "true");
        if (z2) {
            putExtra.putExtra("default_audience", aVar.a());
        }
        putExtra.putExtra("legacy_override", k.g());
        putExtra.putExtra("auth_type", str4);
        return putExtra;
    }

    public static Intent b(Context context, String str, Collection<String> collection, String str2, boolean z, boolean z2, com.facebook.login.a aVar, String str3, String str4) {
        for (e eVar : b) {
            Intent a2 = a(context, a(eVar, str, collection, str2, z, z2, aVar, str3, str4), eVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    public static final int a() {
        return f.get(0).intValue();
    }

    public static boolean a(int i) {
        return f.contains(Integer.valueOf(i)) && i >= 20140701;
    }

    public static Intent a(Intent intent, Bundle bundle, h hVar) {
        UUID b2 = b(intent);
        if (b2 == null) {
            return null;
        }
        Intent intent2 = new Intent();
        intent2.putExtra("com.facebook.platform.protocol.PROTOCOL_VERSION", a(intent));
        Bundle bundle2 = new Bundle();
        bundle2.putString("action_id", b2.toString());
        if (hVar != null) {
            bundle2.putBundle(IjkMediaPlayer.OnNativeInvokeListener.ARG_ERROR, a(hVar));
        }
        intent2.putExtra("com.facebook.platform.protocol.BRIDGE_ARGS", bundle2);
        if (bundle == null) {
            return intent2;
        }
        intent2.putExtra("com.facebook.platform.protocol.RESULT_ARGS", bundle);
        return intent2;
    }

    public static Intent a(Context context) {
        for (e eVar : b) {
            Intent b2 = b(context, new Intent("com.facebook.platform.PLATFORM_SERVICE").setPackage(eVar.a()).addCategory("android.intent.category.DEFAULT"), eVar);
            if (b2 != null) {
                return b2;
            }
        }
        return null;
    }

    public static int a(Intent intent) {
        return intent.getIntExtra("com.facebook.platform.protocol.PROTOCOL_VERSION", 0);
    }

    public static UUID b(Intent intent) {
        String stringExtra;
        if (intent == null) {
            return null;
        }
        if (a(a(intent))) {
            Bundle bundleExtra = intent.getBundleExtra("com.facebook.platform.protocol.BRIDGE_ARGS");
            if (bundleExtra != null) {
                stringExtra = bundleExtra.getString("action_id");
            } else {
                stringExtra = null;
            }
        } else {
            stringExtra = intent.getStringExtra("com.facebook.platform.protocol.CALL_ID");
        }
        if (stringExtra == null) {
            return null;
        }
        try {
            return UUID.fromString(stringExtra);
        } catch (IllegalArgumentException e2) {
            return null;
        }
    }

    public static Bundle c(Intent intent) {
        if (!a(a(intent))) {
            return intent.getExtras();
        }
        return intent.getBundleExtra("com.facebook.platform.protocol.METHOD_ARGS");
    }

    public static h a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        String string = bundle.getString("error_type");
        if (string == null) {
            string = bundle.getString("com.facebook.platform.status.ERROR_TYPE");
        }
        String string2 = bundle.getString("error_description");
        if (string2 == null) {
            string2 = bundle.getString("com.facebook.platform.status.ERROR_DESCRIPTION");
        }
        if (string == null || !string.equalsIgnoreCase("UserCanceled")) {
            return new h(string2);
        }
        return new j(string2);
    }

    public static Bundle a(h hVar) {
        if (hVar == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("error_description", hVar.toString());
        if (!(hVar instanceof j)) {
            return bundle;
        }
        bundle.putString("error_type", "UserCanceled");
        return bundle;
    }

    public static int b(int i) {
        return a(b, new int[]{i}).b();
    }

    private static f a(List<e> list, int[] iArr) {
        b();
        if (list == null) {
            return f.a();
        }
        for (e eVar : list) {
            int a2 = a(eVar.c(), a(), iArr);
            if (a2 != -1) {
                return f.a(eVar, a2);
            }
        }
        return f.a();
    }

    public static void b() {
        if (e.compareAndSet(false, true)) {
            k.d().execute(new Runnable() {
                /* class com.facebook.internal.p.AnonymousClass1 */

                public void run() {
                    try {
                        for (e eVar : p.b) {
                            eVar.a(true);
                        }
                    } finally {
                        p.e.set(false);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.TreeSet<java.lang.Integer> b(com.facebook.internal.p.e r8) {
        /*
        // Method dump skipped, instructions count: 138
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.internal.p.b(com.facebook.internal.p$e):java.util.TreeSet");
    }

    public static int a(TreeSet<Integer> treeSet, int i, int[] iArr) {
        Iterator<Integer> descendingIterator = treeSet.descendingIterator();
        int i2 = -1;
        int length = iArr.length - 1;
        while (descendingIterator.hasNext()) {
            int intValue = descendingIterator.next().intValue();
            int max = Math.max(i2, intValue);
            while (length >= 0 && iArr[length] > intValue) {
                length--;
            }
            if (length < 0) {
                return -1;
            }
            if (iArr[length] != intValue) {
                i2 = max;
            } else if (length % 2 == 0) {
                return Math.min(max, i);
            } else {
                return -1;
            }
        }
        return -1;
    }

    private static Uri c(e eVar) {
        return Uri.parse("content://" + eVar.a() + ".provider.PlatformProvider/versions");
    }

    public static class f {

        /* renamed from: a  reason: collision with root package name */
        private e f1127a;
        private int b;

        public static f a(e eVar, int i) {
            f fVar = new f();
            fVar.f1127a = eVar;
            fVar.b = i;
            return fVar;
        }

        public static f a() {
            f fVar = new f();
            fVar.b = -1;
            return fVar;
        }

        private f() {
        }

        public int b() {
            return this.b;
        }
    }
}
