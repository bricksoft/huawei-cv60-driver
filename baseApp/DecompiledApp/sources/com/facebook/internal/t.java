package com.facebook.internal;

import java.util.EnumSet;
import java.util.Iterator;

public enum t {
    None(0),
    Enabled(1),
    RequireConfirm(2);
    
    public static final EnumSet<t> d = EnumSet.allOf(t.class);
    private final long e;

    public static EnumSet<t> a(long j) {
        EnumSet<t> noneOf = EnumSet.noneOf(t.class);
        Iterator it = d.iterator();
        while (it.hasNext()) {
            t tVar = (t) it.next();
            if ((tVar.a() & j) != 0) {
                noneOf.add(tVar);
            }
        }
        return noneOf;
    }

    private t(long j) {
        this.e = j;
    }

    public long a() {
        return this.e;
    }
}
