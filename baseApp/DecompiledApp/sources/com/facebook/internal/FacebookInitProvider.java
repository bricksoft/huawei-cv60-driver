package com.facebook.internal;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.facebook.k;

public final class FacebookInitProvider extends ContentProvider {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1096a = FacebookInitProvider.class.getSimpleName();

    public boolean onCreate() {
        try {
            k.a(getContext());
            return false;
        } catch (Exception e) {
            Log.i(f1096a, "Failed to auto initialize the Facebook SDK", e);
            return false;
        }
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }
}
