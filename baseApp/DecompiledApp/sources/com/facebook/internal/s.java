package com.facebook.internal;

import com.facebook.k;
import java.util.Collection;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    public static final Collection<String> f1131a = u.a("service_disabled", "AndroidAuthKillSwitchException");
    public static final Collection<String> b = u.a("access_denied", "OAuthAccessDeniedException");
    private static final String c = s.class.getName();

    public static final String a() {
        return String.format("m.%s", k.e());
    }

    public static final String b() {
        return String.format("https://graph.%s", k.e());
    }

    public static final String c() {
        return String.format("https://graph-video.%s", k.e());
    }

    public static final String d() {
        return "v3.2";
    }
}
