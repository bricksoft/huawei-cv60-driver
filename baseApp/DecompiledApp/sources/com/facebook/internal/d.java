package com.facebook.internal;

import android.content.Intent;
import com.facebook.k;
import java.util.HashMap;
import java.util.Map;

public final class d implements com.facebook.d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1102a = d.class.getSimpleName();
    private static Map<Integer, a> b = new HashMap();
    private Map<Integer, a> c = new HashMap();

    public interface a {
        boolean a(int i, Intent intent);
    }

    public static synchronized void a(int i, a aVar) {
        synchronized (d.class) {
            v.a(aVar, "callback");
            if (!b.containsKey(Integer.valueOf(i))) {
                b.put(Integer.valueOf(i), aVar);
            }
        }
    }

    private static synchronized a a(Integer num) {
        a aVar;
        synchronized (d.class) {
            aVar = b.get(num);
        }
        return aVar;
    }

    private static boolean b(int i, int i2, Intent intent) {
        a a2 = a(Integer.valueOf(i));
        if (a2 != null) {
            return a2.a(i2, intent);
        }
        return false;
    }

    public void b(int i, a aVar) {
        v.a(aVar, "callback");
        this.c.put(Integer.valueOf(i), aVar);
    }

    @Override // com.facebook.d
    public boolean a(int i, int i2, Intent intent) {
        a aVar = this.c.get(Integer.valueOf(i));
        if (aVar != null) {
            return aVar.a(i2, intent);
        }
        return b(i, i2, intent);
    }

    public enum b {
        Login(0),
        Share(1),
        Message(2),
        Like(3),
        GameRequest(4),
        AppGroupCreate(5),
        AppGroupJoin(6),
        AppInvite(7),
        DeviceShare(8);
        
        private final int j;

        private b(int i) {
            this.j = i;
        }

        public int a() {
            return k.o() + this.j;
        }
    }
}
