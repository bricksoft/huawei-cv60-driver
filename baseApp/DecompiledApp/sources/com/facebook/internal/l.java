package com.facebook.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.a.a.a.e;
import com.facebook.a.b.d;
import com.facebook.a.b.f;
import com.facebook.internal.k;
import com.facebook.k;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1116a = l.class.getSimpleName();
    private static final String[] b = {"supports_implicit_sdk_logging", "gdpv4_nux_content", "gdpv4_nux_enabled", "gdpv4_chrome_custom_tabs_enabled", "android_dialog_configs", "android_sdk_error_categories", "app_events_session_timeout", "app_events_feature_bitmask", "auto_event_mapping_android", "seamless_login", "smart_login_bookmark_icon_url", "smart_login_menu_icon_url"};
    private static final Map<String, k> c = new ConcurrentHashMap();
    private static final AtomicReference<a> d = new AtomicReference<>(a.NOT_LOADED);
    private static final ConcurrentLinkedQueue<b> e = new ConcurrentLinkedQueue<>();
    private static boolean f = false;
    private static boolean g = false;
    @Nullable
    private static JSONArray h = null;

    /* access modifiers changed from: package-private */
    public enum a {
        NOT_LOADED,
        LOADING,
        SUCCESS,
        ERROR
    }

    public interface b {
        void a();

        void a(k kVar);
    }

    public static void a() {
        boolean z;
        final Context f2 = k.f();
        final String j = k.j();
        if (u.a(j)) {
            d.set(a.ERROR);
            g();
        } else if (c.containsKey(j)) {
            d.set(a.SUCCESS);
            g();
        } else {
            if (d.compareAndSet(a.NOT_LOADED, a.LOADING) || d.compareAndSet(a.ERROR, a.LOADING)) {
                z = true;
            } else {
                z = false;
            }
            if (!z) {
                g();
                return;
            }
            final String format = String.format("com.facebook.internal.APP_SETTINGS.%s", j);
            k.d().execute(new Runnable() {
                /* class com.facebook.internal.l.AnonymousClass1 */

                public void run() {
                    JSONObject jSONObject;
                    k kVar = null;
                    SharedPreferences sharedPreferences = f2.getSharedPreferences("com.facebook.internal.preferences.APP_SETTINGS", 0);
                    String string = sharedPreferences.getString(format, null);
                    if (!u.a(string)) {
                        try {
                            jSONObject = new JSONObject(string);
                        } catch (JSONException e) {
                            u.a("FacebookSDK", (Exception) e);
                            jSONObject = null;
                        }
                        if (jSONObject != null) {
                            kVar = l.b(j, jSONObject);
                        }
                    }
                    JSONObject c2 = l.c(j);
                    if (c2 != null) {
                        l.b(j, c2);
                        sharedPreferences.edit().putString(format, c2.toString()).apply();
                    }
                    if (kVar != null) {
                        String j = kVar.j();
                        if (!l.f && j != null && j.length() > 0) {
                            boolean unused = l.f = true;
                            Log.w(l.f1116a, j);
                        }
                    }
                    j.a(j, true);
                    d.a();
                    f.a();
                    l.d.set(l.c.containsKey(j) ? a.SUCCESS : a.ERROR);
                    l.g();
                }
            });
        }
    }

    @Nullable
    public static k a(String str) {
        if (str != null) {
            return c.get(str);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static synchronized void g() {
        synchronized (l.class) {
            a aVar = d.get();
            if (!a.NOT_LOADED.equals(aVar) && !a.LOADING.equals(aVar)) {
                final k kVar = c.get(k.j());
                Handler handler = new Handler(Looper.getMainLooper());
                if (a.ERROR.equals(aVar)) {
                    while (!e.isEmpty()) {
                        final b poll = e.poll();
                        handler.post(new Runnable() {
                            /* class com.facebook.internal.l.AnonymousClass2 */

                            public void run() {
                                poll.a();
                            }
                        });
                    }
                } else {
                    while (!e.isEmpty()) {
                        final b poll2 = e.poll();
                        handler.post(new Runnable() {
                            /* class com.facebook.internal.l.AnonymousClass3 */

                            public void run() {
                                poll2.a(kVar);
                            }
                        });
                    }
                }
            }
        }
    }

    @Nullable
    public static k a(String str, boolean z) {
        if (!z && c.containsKey(str)) {
            return c.get(str);
        }
        JSONObject c2 = c(str);
        if (c2 == null) {
            return null;
        }
        k b2 = b(str, c2);
        if (!str.equals(k.j())) {
            return b2;
        }
        d.set(a.SUCCESS);
        g();
        return b2;
    }

    /* access modifiers changed from: private */
    public static k b(String str, JSONObject jSONObject) {
        g a2;
        JSONArray optJSONArray = jSONObject.optJSONArray("android_sdk_error_categories");
        if (optJSONArray == null) {
            a2 = g.a();
        } else {
            a2 = g.a(optJSONArray);
        }
        int optInt = jSONObject.optInt("app_events_feature_bitmask", 0);
        boolean z = (optInt & 8) != 0;
        boolean z2 = (optInt & 16) != 0;
        boolean z3 = (optInt & 32) != 0;
        boolean z4 = (optInt & 256) != 0;
        JSONArray optJSONArray2 = jSONObject.optJSONArray("auto_event_mapping_android");
        h = optJSONArray2;
        if (h != null && m.b()) {
            e.a(optJSONArray2.toString());
        }
        k kVar = new k(jSONObject.optBoolean("supports_implicit_sdk_logging", false), jSONObject.optString("gdpv4_nux_content", ""), jSONObject.optBoolean("gdpv4_nux_enabled", false), jSONObject.optBoolean("gdpv4_chrome_custom_tabs_enabled", false), jSONObject.optInt("app_events_session_timeout", com.facebook.a.b.e.a()), t.a(jSONObject.optLong("seamless_login")), a(jSONObject.optJSONObject("android_dialog_configs")), z, a2, jSONObject.optString("smart_login_bookmark_icon_url"), jSONObject.optString("smart_login_menu_icon_url"), z2, z3, optJSONArray2, jSONObject.optString("sdk_update_message"), z4);
        c.put(str, kVar);
        return kVar;
    }

    /* access modifiers changed from: private */
    public static JSONObject c(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", TextUtils.join(",", new ArrayList(Arrays.asList(b))));
        GraphRequest a2 = GraphRequest.a((AccessToken) null, str, (GraphRequest.b) null);
        a2.a(true);
        a2.a(bundle);
        return a2.i().b();
    }

    private static Map<String, Map<String, k.a>> a(JSONObject jSONObject) {
        JSONArray optJSONArray;
        HashMap hashMap = new HashMap();
        if (!(jSONObject == null || (optJSONArray = jSONObject.optJSONArray("data")) == null)) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                k.a a2 = k.a.a(optJSONArray.optJSONObject(i));
                if (a2 != null) {
                    String a3 = a2.a();
                    Map map = (Map) hashMap.get(a3);
                    if (map == null) {
                        map = new HashMap();
                        hashMap.put(a3, map);
                    }
                    map.put(a2.b(), a2);
                }
            }
        }
        return hashMap;
    }
}
