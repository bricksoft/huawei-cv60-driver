package com.facebook.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import com.facebook.a.g;

public class b extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static b f1100a;
    private Context b;

    private b(Context context) {
        this.b = context.getApplicationContext();
    }

    private void a() {
        LocalBroadcastManager.getInstance(this.b).registerReceiver(this, new IntentFilter("com.parse.bolts.measurement_event"));
    }

    private void b() {
        LocalBroadcastManager.getInstance(this.b).unregisterReceiver(this);
    }

    public static b a(Context context) {
        if (f1100a != null) {
            return f1100a;
        }
        f1100a = new b(context);
        f1100a.a();
        return f1100a;
    }

    /* access modifiers changed from: protected */
    @Override // java.lang.Object
    public void finalize() {
        try {
            b();
        } finally {
            super.finalize();
        }
    }

    public void onReceive(Context context, Intent intent) {
        g a2 = g.a(context);
        String str = "bf_" + intent.getStringExtra("event_name");
        Bundle bundleExtra = intent.getBundleExtra("event_args");
        Bundle bundle = new Bundle();
        for (String str2 : bundleExtra.keySet()) {
            bundle.putString(str2.replaceAll("[^0-9a-zA-Z _-]", "-").replaceAll("^[ -]*", "").replaceAll("[ -]*$", ""), (String) bundleExtra.get(str2));
        }
        a2.a(str, bundle);
    }
}
