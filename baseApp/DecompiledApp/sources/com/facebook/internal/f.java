package com.facebook.internal;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import com.facebook.h;
import com.facebook.internal.w;
import com.facebook.k;

public class f extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private Dialog f1105a;

    public void a(Dialog dialog) {
        this.f1105a = dialog;
    }

    @Override // android.support.v4.app.Fragment, android.support.v4.app.DialogFragment
    public void onCreate(Bundle bundle) {
        w a2;
        super.onCreate(bundle);
        if (this.f1105a == null) {
            FragmentActivity activity = getActivity();
            Bundle c = p.c(activity.getIntent());
            if (!c.getBoolean("is_fallback", false)) {
                String string = c.getString("action");
                Bundle bundle2 = c.getBundle("params");
                if (u.a(string)) {
                    u.a("FacebookDialogFragment", "Cannot start a WebDialog with an empty/missing 'actionName'");
                    activity.finish();
                    return;
                }
                a2 = new w.a(activity, string, bundle2).a(new w.c() {
                    /* class com.facebook.internal.f.AnonymousClass1 */

                    @Override // com.facebook.internal.w.c
                    public void a(Bundle bundle, h hVar) {
                        f.this.a((f) bundle, (Bundle) hVar);
                    }
                }).a();
            } else {
                String string2 = c.getString("url");
                if (u.a(string2)) {
                    u.a("FacebookDialogFragment", "Cannot start a fallback WebDialog with an empty/missing 'url'");
                    activity.finish();
                    return;
                }
                a2 = i.a(activity, string2, String.format("fb%s://bridge/", k.j()));
                a2.a(new w.c() {
                    /* class com.facebook.internal.f.AnonymousClass2 */

                    @Override // com.facebook.internal.w.c
                    public void a(Bundle bundle, h hVar) {
                        f.this.a((f) bundle);
                    }
                });
            }
            this.f1105a = a2;
        }
    }

    @Override // android.support.v4.app.DialogFragment
    @NonNull
    public Dialog onCreateDialog(Bundle bundle) {
        if (this.f1105a == null) {
            a((Bundle) null, (h) null);
            setShowsDialog(false);
        }
        return this.f1105a;
    }

    @Override // android.support.v4.app.Fragment
    public void onResume() {
        super.onResume();
        if (this.f1105a instanceof w) {
            ((w) this.f1105a).e();
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if ((this.f1105a instanceof w) && isResumed()) {
            ((w) this.f1105a).e();
        }
    }

    @Override // android.support.v4.app.Fragment, android.support.v4.app.DialogFragment
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(Bundle bundle, h hVar) {
        FragmentActivity activity = getActivity();
        activity.setResult(hVar == null ? -1 : 0, p.a(activity.getIntent(), bundle, hVar));
        activity.finish();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(Bundle bundle) {
        FragmentActivity activity = getActivity();
        Intent intent = new Intent();
        if (bundle == null) {
            bundle = new Bundle();
        }
        intent.putExtras(bundle);
        activity.setResult(-1, intent);
        activity.finish();
    }
}
