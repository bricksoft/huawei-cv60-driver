package com.facebook.internal;

import com.facebook.k;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.FutureTask;

public class n<T> {

    /* renamed from: a  reason: collision with root package name */
    private T f1122a;
    private CountDownLatch b = new CountDownLatch(1);

    public n(final Callable<T> callable) {
        k.d().execute(new FutureTask(new Callable<Void>() {
            /* class com.facebook.internal.n.AnonymousClass1 */

            /* JADX INFO: finally extract failed */
            /* renamed from: a */
            public Void call() {
                try {
                    n.this.f1122a = callable.call();
                    n.this.b.countDown();
                    return null;
                } catch (Throwable th) {
                    n.this.b.countDown();
                    throw th;
                }
            }
        }));
    }
}
