package com.facebook.internal;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.a;
import com.facebook.k;
import com.google.android.exoplayer.C;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private Uri f1104a;

    public e(String str, Bundle bundle) {
        this.f1104a = u.a(s.a(), k.g() + "/" + "dialog/" + str, bundle == null ? new Bundle() : bundle);
    }

    public void a(Activity activity, String str) {
        a a2 = new a.C0013a().a();
        a2.f217a.setPackage(str);
        a2.f217a.addFlags(C.ENCODING_PCM_32BIT);
        a2.a(activity, this.f1104a);
    }
}
