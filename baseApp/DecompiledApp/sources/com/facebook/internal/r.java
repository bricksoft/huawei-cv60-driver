package com.facebook.internal;

import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public class r {

    /* renamed from: a  reason: collision with root package name */
    private static final ConcurrentHashMap<String, JSONObject> f1130a = new ConcurrentHashMap<>();

    public static JSONObject a(String str) {
        return f1130a.get(str);
    }

    public static void a(String str, JSONObject jSONObject) {
        f1130a.put(str, jSONObject);
    }
}
