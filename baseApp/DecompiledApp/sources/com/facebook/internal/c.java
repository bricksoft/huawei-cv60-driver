package com.facebook.internal;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<Class<?>, a> f1101a = new HashMap();

    public interface a {
        void a(Bundle bundle, String str, Object obj);
    }

    static {
        f1101a.put(Boolean.class, new a() {
            /* class com.facebook.internal.c.AnonymousClass1 */

            @Override // com.facebook.internal.c.a
            public void a(Bundle bundle, String str, Object obj) {
                bundle.putBoolean(str, ((Boolean) obj).booleanValue());
            }
        });
        f1101a.put(Integer.class, new a() {
            /* class com.facebook.internal.c.AnonymousClass2 */

            @Override // com.facebook.internal.c.a
            public void a(Bundle bundle, String str, Object obj) {
                bundle.putInt(str, ((Integer) obj).intValue());
            }
        });
        f1101a.put(Long.class, new a() {
            /* class com.facebook.internal.c.AnonymousClass3 */

            @Override // com.facebook.internal.c.a
            public void a(Bundle bundle, String str, Object obj) {
                bundle.putLong(str, ((Long) obj).longValue());
            }
        });
        f1101a.put(Double.class, new a() {
            /* class com.facebook.internal.c.AnonymousClass4 */

            @Override // com.facebook.internal.c.a
            public void a(Bundle bundle, String str, Object obj) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            }
        });
        f1101a.put(String.class, new a() {
            /* class com.facebook.internal.c.AnonymousClass5 */

            @Override // com.facebook.internal.c.a
            public void a(Bundle bundle, String str, Object obj) {
                bundle.putString(str, (String) obj);
            }
        });
        f1101a.put(String[].class, new a() {
            /* class com.facebook.internal.c.AnonymousClass6 */

            @Override // com.facebook.internal.c.a
            public void a(Bundle bundle, String str, Object obj) {
                throw new IllegalArgumentException("Unexpected type from JSON");
            }
        });
        f1101a.put(JSONArray.class, new a() {
            /* class com.facebook.internal.c.AnonymousClass7 */

            @Override // com.facebook.internal.c.a
            public void a(Bundle bundle, String str, Object obj) {
                JSONArray jSONArray = (JSONArray) obj;
                ArrayList<String> arrayList = new ArrayList<>();
                if (jSONArray.length() == 0) {
                    bundle.putStringArrayList(str, arrayList);
                    return;
                }
                for (int i = 0; i < jSONArray.length(); i++) {
                    Object obj2 = jSONArray.get(i);
                    if (obj2 instanceof String) {
                        arrayList.add((String) obj2);
                    } else {
                        throw new IllegalArgumentException("Unexpected type in an array: " + obj2.getClass());
                    }
                }
                bundle.putStringArrayList(str, arrayList);
            }
        });
    }

    public static Bundle a(JSONObject jSONObject) {
        Bundle bundle = new Bundle();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            Object obj = jSONObject.get(next);
            if (!(obj == null || obj == JSONObject.NULL)) {
                if (obj instanceof JSONObject) {
                    bundle.putBundle(next, a((JSONObject) obj));
                } else {
                    a aVar = f1101a.get(obj.getClass());
                    if (aVar == null) {
                        throw new IllegalArgumentException("Unsupported type: " + obj.getClass());
                    }
                    aVar.a(bundle, next, obj);
                }
            }
        }
        return bundle;
    }
}
