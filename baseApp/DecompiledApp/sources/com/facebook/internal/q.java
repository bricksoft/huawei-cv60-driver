package com.facebook.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public abstract class q implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private final Context f1128a;
    private final Handler b;
    private a c;
    private boolean d;
    private Messenger e;
    private int f;
    private int g;
    private final String h;
    private final int i;

    public interface a {
        void a(Bundle bundle);
    }

    /* access modifiers changed from: protected */
    public abstract void a(Bundle bundle);

    public q(Context context, int i2, int i3, int i4, String str) {
        Context applicationContext = context.getApplicationContext();
        this.f1128a = applicationContext != null ? applicationContext : context;
        this.f = i2;
        this.g = i3;
        this.h = str;
        this.i = i4;
        this.b = new Handler() {
            /* class com.facebook.internal.q.AnonymousClass1 */

            public void handleMessage(Message message) {
                q.this.a(message);
            }
        };
    }

    public void a(a aVar) {
        this.c = aVar;
    }

    public boolean a() {
        Intent a2;
        if (this.d || p.b(this.i) == -1 || (a2 = p.a(this.f1128a)) == null) {
            return false;
        }
        this.d = true;
        this.f1128a.bindService(a2, this, 1);
        return true;
    }

    public void b() {
        this.d = false;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.e = new Messenger(iBinder);
        c();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.e = null;
        try {
            this.f1128a.unbindService(this);
        } catch (IllegalArgumentException e2) {
        }
        b(null);
    }

    private void c() {
        Bundle bundle = new Bundle();
        bundle.putString("com.facebook.platform.extra.APPLICATION_ID", this.h);
        a(bundle);
        Message obtain = Message.obtain((Handler) null, this.f);
        obtain.arg1 = this.i;
        obtain.setData(bundle);
        obtain.replyTo = new Messenger(this.b);
        try {
            this.e.send(obtain);
        } catch (RemoteException e2) {
            b(null);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Message message) {
        if (message.what == this.g) {
            Bundle data = message.getData();
            if (data.getString("com.facebook.platform.status.ERROR_TYPE") != null) {
                b(null);
            } else {
                b(data);
            }
            try {
                this.f1128a.unbindService(this);
            } catch (IllegalArgumentException e2) {
            }
        }
    }

    private void b(Bundle bundle) {
        if (this.d) {
            this.d = false;
            a aVar = this.c;
            if (aVar != null) {
                aVar.a(bundle);
            }
        }
    }
}
