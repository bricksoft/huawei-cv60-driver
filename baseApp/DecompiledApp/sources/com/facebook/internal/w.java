package com.facebook.internal;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.facebook.AccessToken;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.common.R;
import com.facebook.g;
import com.facebook.h;
import com.facebook.i;
import com.facebook.j;
import com.facebook.k;
import com.facebook.m;
import com.facebook.p;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import org.json.JSONArray;
import org.json.JSONObject;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public class w extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private static final int f1137a = R.style.com_facebook_activity_theme;
    private static volatile int m;
    private String b;
    private String c;
    private c d;
    private WebView e;
    private ProgressDialog f;
    private ImageView g;
    private FrameLayout h;
    private d i;
    private boolean j;
    private boolean k;
    private boolean l;
    private WindowManager.LayoutParams n;

    public interface c {
        void a(Bundle bundle, h hVar);
    }

    protected static void a(Context context) {
        if (context != null) {
            try {
                ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo != null && applicationInfo.metaData != null && m == 0) {
                    a(applicationInfo.metaData.getInt("com.facebook.sdk.WebDialogTheme"));
                }
            } catch (PackageManager.NameNotFoundException e2) {
            }
        }
    }

    public static w a(Context context, String str, Bundle bundle, int i2, c cVar) {
        a(context);
        return new w(context, str, bundle, i2, cVar);
    }

    public static int a() {
        v.a();
        return m;
    }

    public static void a(int i2) {
        if (i2 == 0) {
            i2 = f1137a;
        }
        m = i2;
    }

    protected w(Context context, String str) {
        this(context, str, a());
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private w(Context context, String str, int i2) {
        super(context, i2 == 0 ? a() : i2);
        this.c = "fbconnect://success";
        this.j = false;
        this.k = false;
        this.l = false;
        this.b = str;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private w(Context context, String str, Bundle bundle, int i2, c cVar) {
        super(context, i2 == 0 ? a() : i2);
        this.c = "fbconnect://success";
        this.j = false;
        this.k = false;
        this.l = false;
        bundle = bundle == null ? new Bundle() : bundle;
        this.c = u.f(context) ? "fbconnect://chrome_os_success" : "fbconnect://success";
        bundle.putString("redirect_uri", this.c);
        bundle.putString("display", "touch");
        bundle.putString("client_id", k.j());
        bundle.putString("sdk", String.format(Locale.ROOT, "android-%s", k.h()));
        this.d = cVar;
        if (!str.equals("share") || !bundle.containsKey("media")) {
            this.b = u.a(s.a(), k.g() + "/" + "dialog/" + str, bundle).toString();
        } else {
            this.i = new d(str, bundle);
        }
    }

    public void a(c cVar) {
        this.d = cVar;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            cancel();
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public void dismiss() {
        if (this.e != null) {
            this.e.stopLoading();
        }
        if (!this.k && this.f != null && this.f.isShowing()) {
            this.f.dismiss();
        }
        super.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.i == null || this.i.getStatus() != AsyncTask.Status.PENDING) {
            e();
            return;
        }
        this.i.execute(new Void[0]);
        this.f.show();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.i != null) {
            this.i.cancel(true);
            this.f.dismiss();
        }
        super.onStop();
    }

    public void onDetachedFromWindow() {
        this.k = true;
        super.onDetachedFromWindow();
    }

    public void onAttachedToWindow() {
        this.k = false;
        if (u.d(getContext()) && this.n != null && this.n.token == null) {
            this.n.token = getOwnerActivity().getWindow().getAttributes().token;
            u.a("FacebookSDK.WebDialog", "Set token on onAttachedToWindow(): " + this.n.token);
        }
        super.onAttachedToWindow();
    }

    public void onWindowAttributesChanged(WindowManager.LayoutParams layoutParams) {
        if (layoutParams.token == null) {
            this.n = layoutParams;
        }
        super.onWindowAttributesChanged(layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f = new ProgressDialog(getContext());
        this.f.requestWindowFeature(1);
        this.f.setMessage(getContext().getString(R.string.com_facebook_loading));
        this.f.setCanceledOnTouchOutside(false);
        this.f.setOnCancelListener(new DialogInterface.OnCancelListener() {
            /* class com.facebook.internal.w.AnonymousClass1 */

            public void onCancel(DialogInterface dialogInterface) {
                w.this.cancel();
            }
        });
        requestWindowFeature(1);
        this.h = new FrameLayout(getContext());
        e();
        getWindow().setGravity(17);
        getWindow().setSoftInputMode(16);
        f();
        if (this.b != null) {
            b((this.g.getDrawable().getIntrinsicWidth() / 2) + 1);
        }
        this.h.addView(this.g, new ViewGroup.LayoutParams(-2, -2));
        setContentView(this.h);
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public Bundle a(String str) {
        Uri parse = Uri.parse(str);
        Bundle c2 = u.c(parse.getQuery());
        c2.putAll(u.c(parse.getFragment()));
        return c2;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public WebView d() {
        return this.e;
    }

    public void e() {
        Display defaultDisplay = ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        getWindow().setLayout(Math.min(a(displayMetrics.widthPixels < displayMetrics.heightPixels ? displayMetrics.widthPixels : displayMetrics.heightPixels, displayMetrics.density, 480, 800), displayMetrics.widthPixels), Math.min(a(displayMetrics.widthPixels < displayMetrics.heightPixels ? displayMetrics.heightPixels : displayMetrics.widthPixels, displayMetrics.density, 800, 1280), displayMetrics.heightPixels));
    }

    private int a(int i2, float f2, int i3, int i4) {
        double d2 = 0.5d;
        int i5 = (int) (((float) i2) / f2);
        if (i5 <= i3) {
            d2 = 1.0d;
        } else if (i5 < i4) {
            d2 = 0.5d + ((((double) (i4 - i5)) / ((double) (i4 - i3))) * 0.5d);
        }
        return (int) (d2 * ((double) i2));
    }

    /* access modifiers changed from: protected */
    public void a(Bundle bundle) {
        if (this.d != null && !this.j) {
            this.j = true;
            this.d.a(bundle, null);
            dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void a(Throwable th) {
        h hVar;
        if (this.d != null && !this.j) {
            this.j = true;
            if (th instanceof h) {
                hVar = (h) th;
            } else {
                hVar = new h(th);
            }
            this.d.a(null, hVar);
            dismiss();
        }
    }

    public void cancel() {
        if (this.d != null && !this.j) {
            a(new j());
        }
    }

    private void f() {
        this.g = new ImageView(getContext());
        this.g.setOnClickListener(new View.OnClickListener() {
            /* class com.facebook.internal.w.AnonymousClass2 */

            public void onClick(View view) {
                w.this.cancel();
            }
        });
        this.g.setImageDrawable(getContext().getResources().getDrawable(R.drawable.com_facebook_close));
        this.g.setVisibility(4);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    @SuppressLint({"SetJavaScriptEnabled"})
    private void b(int i2) {
        LinearLayout linearLayout = new LinearLayout(getContext());
        this.e = new WebView(getContext()) {
            /* class com.facebook.internal.w.AnonymousClass3 */

            public void onWindowFocusChanged(boolean z) {
                try {
                    super.onWindowFocusChanged(z);
                } catch (NullPointerException e) {
                }
            }
        };
        this.e.setVerticalScrollBarEnabled(false);
        this.e.setHorizontalScrollBarEnabled(false);
        this.e.setWebViewClient(new b());
        this.e.getSettings().setJavaScriptEnabled(true);
        this.e.loadUrl(this.b);
        this.e.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.e.setVisibility(4);
        this.e.getSettings().setSavePassword(false);
        this.e.getSettings().setSaveFormData(false);
        this.e.setFocusable(true);
        this.e.setFocusableInTouchMode(true);
        this.e.setOnTouchListener(new View.OnTouchListener() {
            /* class com.facebook.internal.w.AnonymousClass4 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.hasFocus()) {
                    return false;
                }
                view.requestFocus();
                return false;
            }
        });
        linearLayout.setPadding(i2, i2, i2, i2);
        linearLayout.addView(this.e);
        linearLayout.setBackgroundColor(-872415232);
        this.h.addView(linearLayout);
    }

    /* access modifiers changed from: private */
    public class b extends WebViewClient {
        private b() {
        }

        @Override // android.webkit.WebViewClient
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            int i;
            u.a("FacebookSDK.WebDialog", "Redirect URL: " + str);
            if (str.startsWith(w.this.c)) {
                Bundle a2 = w.this.a(str);
                String string = a2.getString(IjkMediaPlayer.OnNativeInvokeListener.ARG_ERROR);
                if (string == null) {
                    string = a2.getString("error_type");
                }
                String string2 = a2.getString("error_msg");
                if (string2 == null) {
                    string2 = a2.getString("error_message");
                }
                if (string2 == null) {
                    string2 = a2.getString("error_description");
                }
                String string3 = a2.getString("error_code");
                if (!u.a(string3)) {
                    try {
                        i = Integer.parseInt(string3);
                    } catch (NumberFormatException e) {
                        i = -1;
                    }
                } else {
                    i = -1;
                }
                if (u.a(string) && u.a(string2) && i == -1) {
                    w.this.a(a2);
                } else if (string != null && (string.equals("access_denied") || string.equals("OAuthAccessDeniedException"))) {
                    w.this.cancel();
                } else if (i == 4201) {
                    w.this.cancel();
                } else {
                    w.this.a(new m(new FacebookRequestError(i, string, string2), string2));
                }
                return true;
            } else if (str.startsWith("fbconnect://cancel")) {
                w.this.cancel();
                return true;
            } else if (str.contains("touch")) {
                return false;
            } else {
                try {
                    w.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                    return true;
                } catch (ActivityNotFoundException e2) {
                    return false;
                }
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            w.this.a(new g(str, i, str2));
        }

        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            super.onReceivedSslError(webView, sslErrorHandler, sslError);
            sslErrorHandler.cancel();
            w.this.a(new g(null, -11, null));
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            u.a("FacebookSDK.WebDialog", "Webview loading URL: " + str);
            super.onPageStarted(webView, str, bitmap);
            if (!w.this.k) {
                w.this.f.show();
            }
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (!w.this.k) {
                w.this.f.dismiss();
            }
            w.this.h.setBackgroundColor(0);
            w.this.e.setVisibility(0);
            w.this.g.setVisibility(0);
            w.this.l = true;
        }
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Context f1142a;
        private String b;
        private String c;
        private int d;
        private c e;
        private Bundle f;
        private AccessToken g;

        public a(Context context, String str, Bundle bundle) {
            this.g = AccessToken.a();
            if (!AccessToken.b()) {
                String a2 = u.a(context);
                if (a2 != null) {
                    this.b = a2;
                } else {
                    throw new h("Attempted to create a builder without a valid access token or a valid default Application ID.");
                }
            }
            a(context, str, bundle);
        }

        public a(Context context, String str, String str2, Bundle bundle) {
            str = str == null ? u.a(context) : str;
            v.a(str, "applicationId");
            this.b = str;
            a(context, str2, bundle);
        }

        public a a(c cVar) {
            this.e = cVar;
            return this;
        }

        public w a() {
            if (this.g != null) {
                this.f.putString("app_id", this.g.k());
                this.f.putString("access_token", this.g.d());
            } else {
                this.f.putString("app_id", this.b);
            }
            return w.a(this.f1142a, this.c, this.f, this.d, this.e);
        }

        public String b() {
            return this.b;
        }

        public Context c() {
            return this.f1142a;
        }

        public int d() {
            return this.d;
        }

        public Bundle e() {
            return this.f;
        }

        public c f() {
            return this.e;
        }

        private void a(Context context, String str, Bundle bundle) {
            this.f1142a = context;
            this.c = str;
            if (bundle != null) {
                this.f = bundle;
            } else {
                this.f = new Bundle();
            }
        }
    }

    private class d extends AsyncTask<Void, Void, String[]> {
        private String b;
        private Bundle c;
        private Exception[] d;

        d(String str, Bundle bundle) {
            this.b = str;
            this.c = bundle;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String[] doInBackground(Void... voidArr) {
            String[] stringArray = this.c.getStringArray("media");
            final String[] strArr = new String[stringArray.length];
            this.d = new Exception[stringArray.length];
            final CountDownLatch countDownLatch = new CountDownLatch(stringArray.length);
            ConcurrentLinkedQueue concurrentLinkedQueue = new ConcurrentLinkedQueue();
            AccessToken a2 = AccessToken.a();
            for (final int i = 0; i < stringArray.length; i++) {
                try {
                    if (isCancelled()) {
                        Iterator it = concurrentLinkedQueue.iterator();
                        while (it.hasNext()) {
                            ((AsyncTask) it.next()).cancel(true);
                        }
                        return null;
                    }
                    Uri parse = Uri.parse(stringArray[i]);
                    if (u.a(parse)) {
                        strArr[i] = parse.toString();
                        countDownLatch.countDown();
                    } else {
                        concurrentLinkedQueue.add(com.facebook.share.internal.b.a(a2, parse, new GraphRequest.b() {
                            /* class com.facebook.internal.w.d.AnonymousClass1 */

                            @Override // com.facebook.GraphRequest.b
                            public void a(p pVar) {
                                try {
                                    FacebookRequestError a2 = pVar.a();
                                    if (a2 != null) {
                                        String e = a2.e();
                                        if (e == null) {
                                            e = "Error staging photo.";
                                        }
                                        throw new i(pVar, e);
                                    }
                                    JSONObject b2 = pVar.b();
                                    if (b2 == null) {
                                        throw new h("Error staging photo.");
                                    }
                                    String optString = b2.optString("uri");
                                    if (optString == null) {
                                        throw new h("Error staging photo.");
                                    }
                                    strArr[i] = optString;
                                    countDownLatch.countDown();
                                } catch (Exception e2) {
                                    d.this.d[i] = e2;
                                }
                            }
                        }).j());
                    }
                } catch (Exception e) {
                    Iterator it2 = concurrentLinkedQueue.iterator();
                    while (it2.hasNext()) {
                        ((AsyncTask) it2.next()).cancel(true);
                    }
                    return null;
                }
            }
            countDownLatch.await();
            return strArr;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(String[] strArr) {
            w.this.f.dismiss();
            Exception[] excArr = this.d;
            for (Exception exc : excArr) {
                if (exc != null) {
                    w.this.a(exc);
                    return;
                }
            }
            if (strArr == null) {
                w.this.a(new h("Failed to stage photos for web dialog"));
                return;
            }
            List asList = Arrays.asList(strArr);
            if (asList.contains(null)) {
                w.this.a(new h("Failed to stage photos for web dialog"));
                return;
            }
            u.a(this.c, "media", new JSONArray((Collection) asList));
            w.this.b = u.a(s.a(), k.g() + "/" + "dialog/" + this.b, this.c).toString();
            w.this.b((w) ((w.this.g.getDrawable().getIntrinsicWidth() / 2) + 1));
        }
    }
}
