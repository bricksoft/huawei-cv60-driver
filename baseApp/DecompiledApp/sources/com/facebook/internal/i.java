package com.facebook.internal;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebView;
import org.json.JSONException;
import org.json.JSONObject;

public class i extends w {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1111a = i.class.getName();
    private boolean b;

    public static i a(Context context, String str, String str2) {
        w.a(context);
        return new i(context, str, str2);
    }

    private i(Context context, String str, String str2) {
        super(context, str);
        b(str2);
    }

    /* access modifiers changed from: protected */
    @Override // com.facebook.internal.w
    public Bundle a(String str) {
        Bundle c = u.c(Uri.parse(str).getQuery());
        String string = c.getString("bridge_args");
        c.remove("bridge_args");
        if (!u.a(string)) {
            try {
                c.putBundle("com.facebook.platform.protocol.BRIDGE_ARGS", c.a(new JSONObject(string)));
            } catch (JSONException e) {
                u.a(f1111a, "Unable to parse bridge_args JSON", e);
            }
        }
        String string2 = c.getString("method_results");
        c.remove("method_results");
        if (!u.a(string2)) {
            if (u.a(string2)) {
                string2 = "{}";
            }
            try {
                c.putBundle("com.facebook.platform.protocol.RESULT_ARGS", c.a(new JSONObject(string2)));
            } catch (JSONException e2) {
                u.a(f1111a, "Unable to parse bridge_args JSON", e2);
            }
        }
        c.remove("version");
        c.putInt("com.facebook.platform.protocol.PROTOCOL_VERSION", p.a());
        return c;
    }

    @Override // com.facebook.internal.w
    public void cancel() {
        WebView d = d();
        if (!c() || b() || d == null || !d.isShown()) {
            super.cancel();
        } else if (!this.b) {
            this.b = true;
            d.loadUrl("javascript:" + "(function() {  var event = document.createEvent('Event');  event.initEvent('fbPlatformDialogMustClose',true,true);  document.dispatchEvent(event);})();");
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                /* class com.facebook.internal.i.AnonymousClass1 */

                public void run() {
                    i.super.cancel();
                }
            }, 1500);
        }
    }
}
