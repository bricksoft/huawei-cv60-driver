package com.facebook.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.support.annotation.Nullable;
import com.facebook.h;
import com.facebook.k;
import java.lang.reflect.Method;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1097a = a.class.getCanonicalName();
    private static a g;
    private String b;
    private String c;
    private String d;
    private boolean e;
    private long f;

    private static a b(Context context) {
        a c2 = c(context);
        if (c2 != null) {
            return c2;
        }
        a d2 = d(context);
        if (d2 == null) {
            return new a();
        }
        return d2;
    }

    private static a c(Context context) {
        try {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                throw new h("getAndroidId cannot be called on the main thread.");
            }
            Method a2 = u.a("com.google.android.gms.common.GooglePlayServicesUtil", "isGooglePlayServicesAvailable", Context.class);
            if (a2 == null) {
                return null;
            }
            Object a3 = u.a((Object) null, a2, context);
            if (!(a3 instanceof Integer) || ((Integer) a3).intValue() != 0) {
                return null;
            }
            Method a4 = u.a("com.google.android.gms.ads.identifier.AdvertisingIdClient", "getAdvertisingIdInfo", Context.class);
            if (a4 == null) {
                return null;
            }
            Object a5 = u.a((Object) null, a4, context);
            if (a5 == null) {
                return null;
            }
            Method a6 = u.a(a5.getClass(), "getId", new Class[0]);
            Method a7 = u.a(a5.getClass(), "isLimitAdTrackingEnabled", new Class[0]);
            if (a6 == null || a7 == null) {
                return null;
            }
            a aVar = new a();
            aVar.c = (String) u.a(a5, a6, new Object[0]);
            aVar.e = ((Boolean) u.a(a5, a7, new Object[0])).booleanValue();
            return aVar;
        } catch (Exception e2) {
            u.a("android_id", e2);
            return null;
        }
    }

    private static a d(Context context) {
        b bVar = new b();
        Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
        intent.setPackage("com.google.android.gms");
        if (context.bindService(intent, bVar, 1)) {
            try {
                C0047a aVar = new C0047a(bVar.a());
                a aVar2 = new a();
                aVar2.c = aVar.a();
                aVar2.e = aVar.b();
                return aVar2;
            } catch (Exception e2) {
                u.a("android_id", e2);
            } finally {
                context.unbindService(bVar);
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00fe  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.internal.a a(android.content.Context r8) {
        /*
        // Method dump skipped, instructions count: 265
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.internal.a.a(android.content.Context):com.facebook.internal.a");
    }

    private static a a(a aVar) {
        aVar.f = System.currentTimeMillis();
        g = aVar;
        return aVar;
    }

    public String a() {
        return this.b;
    }

    public String b() {
        if (!k.a() || !k.n()) {
            return null;
        }
        return this.c;
    }

    public String c() {
        return this.d;
    }

    public boolean d() {
        return this.e;
    }

    @Nullable
    private static String e(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager != null) {
            return packageManager.getInstallerPackageName(context.getPackageName());
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static final class b implements ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        private AtomicBoolean f1099a;
        private final BlockingQueue<IBinder> b;

        private b() {
            this.f1099a = new AtomicBoolean(false);
            this.b = new LinkedBlockingDeque();
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder != null) {
                try {
                    this.b.put(iBinder);
                } catch (InterruptedException e) {
                }
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }

        public IBinder a() {
            if (!this.f1099a.compareAndSet(true, true)) {
                return this.b.take();
            }
            throw new IllegalStateException("Binder already consumed");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: com.facebook.internal.a$a  reason: collision with other inner class name */
    public static final class C0047a implements IInterface {

        /* renamed from: a  reason: collision with root package name */
        private IBinder f1098a;

        C0047a(IBinder iBinder) {
            this.f1098a = iBinder;
        }

        public IBinder asBinder() {
            return this.f1098a;
        }

        public String a() {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.f1098a.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readString();
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        public boolean b() {
            boolean z = true;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(1);
                this.f1098a.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z = false;
                }
                return z;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
    }
}
