package com.facebook.internal;

import android.util.Log;
import com.facebook.k;
import com.facebook.t;
import java.util.HashMap;
import java.util.Map;

public class o {

    /* renamed from: a  reason: collision with root package name */
    private static final HashMap<String, String> f1124a = new HashMap<>();
    private final t b;
    private final String c;
    private StringBuilder d;
    private int e = 3;

    public static synchronized void a(String str, String str2) {
        synchronized (o.class) {
            f1124a.put(str, str2);
        }
    }

    public static synchronized void a(String str) {
        synchronized (o.class) {
            if (!k.a(t.INCLUDE_ACCESS_TOKENS)) {
                a(str, "ACCESS_TOKEN_REMOVED");
            }
        }
    }

    public static void a(t tVar, String str, String str2) {
        a(tVar, 3, str, str2);
    }

    public static void a(t tVar, String str, String str2, Object... objArr) {
        if (k.a(tVar)) {
            a(tVar, 3, str, String.format(str2, objArr));
        }
    }

    public static void a(t tVar, int i, String str, String str2, Object... objArr) {
        if (k.a(tVar)) {
            a(tVar, i, str, String.format(str2, objArr));
        }
    }

    public static void a(t tVar, int i, String str, String str2) {
        if (k.a(tVar)) {
            String d2 = d(str2);
            if (!str.startsWith("FacebookSDK.")) {
                str = "FacebookSDK." + str;
            }
            Log.println(i, str, d2);
            if (tVar == t.DEVELOPER_ERRORS) {
                new Exception().printStackTrace();
            }
        }
    }

    private static synchronized String d(String str) {
        synchronized (o.class) {
            for (Map.Entry<String, String> entry : f1124a.entrySet()) {
                str = str.replace(entry.getKey(), entry.getValue());
            }
        }
        return str;
    }

    public o(t tVar, String str) {
        v.a(str, "tag");
        this.b = tVar;
        this.c = "FacebookSDK." + str;
        this.d = new StringBuilder();
    }

    public void a() {
        b(this.d.toString());
        this.d = new StringBuilder();
    }

    public void b(String str) {
        a(this.b, this.e, this.c, str);
    }

    public void c(String str) {
        if (b()) {
            this.d.append(str);
        }
    }

    public void a(String str, Object... objArr) {
        if (b()) {
            this.d.append(String.format(str, objArr));
        }
    }

    public void a(String str, Object obj) {
        a("  %s:\t%s\n", str, obj);
    }

    private boolean b() {
        return k.a(this.b);
    }
}
