package com.facebook.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcel;
import android.os.StatFs;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.view.autofill.AutofillManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.h;
import com.facebook.k;
import com.facebook.p;
import com.facebook.q;
import com.google.android.exoplayer.C;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public final class u {

    /* renamed from: a  reason: collision with root package name */
    private static int f1133a = 0;
    private static long b = -1;
    private static long c = -1;
    private static long d = -1;
    private static String e = "";
    private static String f = "";
    private static String g = "NoCarrier";

    public interface a {
        void a(h hVar);

        void a(JSONObject jSONObject);
    }

    public static <T> boolean a(Collection<T> collection) {
        return collection == null || collection.size() == 0;
    }

    public static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    public static <T> Collection<T> a(T... tArr) {
        return Collections.unmodifiableCollection(Arrays.asList(tArr));
    }

    public static String b(String str) {
        return b("MD5", str);
    }

    public static String a(byte[] bArr) {
        return a("SHA-1", bArr);
    }

    private static String b(String str, String str2) {
        return a(str, str2.getBytes());
    }

    private static String a(String str, byte[] bArr) {
        try {
            return a(MessageDigest.getInstance(str), bArr);
        } catch (NoSuchAlgorithmException e2) {
            return null;
        }
    }

    private static String a(MessageDigest messageDigest, byte[] bArr) {
        messageDigest.update(bArr);
        byte[] digest = messageDigest.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b2 : digest) {
            sb.append(Integer.toHexString((b2 >> 4) & 15));
            sb.append(Integer.toHexString((b2 >> 0) & 15));
        }
        return sb.toString();
    }

    public static Uri a(String str, String str2, Bundle bundle) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https");
        builder.authority(str);
        builder.path(str2);
        if (bundle != null) {
            for (String str3 : bundle.keySet()) {
                Object obj = bundle.get(str3);
                if (obj instanceof String) {
                    builder.appendQueryParameter(str3, (String) obj);
                }
            }
        }
        return builder.build();
    }

    public static Bundle c(String str) {
        Bundle bundle = new Bundle();
        if (!a(str)) {
            for (String str2 : str.split("&")) {
                String[] split = str2.split("=");
                try {
                    if (split.length == 2) {
                        bundle.putString(URLDecoder.decode(split[0], C.UTF8_NAME), URLDecoder.decode(split[1], C.UTF8_NAME));
                    } else if (split.length == 1) {
                        bundle.putString(URLDecoder.decode(split[0], C.UTF8_NAME), "");
                    }
                } catch (UnsupportedEncodingException e2) {
                    a("FacebookSDK", (Exception) e2);
                }
            }
        }
        return bundle;
    }

    public static void a(Bundle bundle, String str, String str2) {
        if (!a(str2)) {
            bundle.putString(str, str2);
        }
    }

    public static void a(Bundle bundle, String str, Uri uri) {
        if (uri != null) {
            a(bundle, str, uri.toString());
        }
    }

    public static boolean a(Bundle bundle, String str, Object obj) {
        if (obj == null) {
            bundle.remove(str);
        } else if (obj instanceof Boolean) {
            bundle.putBoolean(str, ((Boolean) obj).booleanValue());
        } else if (obj instanceof boolean[]) {
            bundle.putBooleanArray(str, (boolean[]) obj);
        } else if (obj instanceof Double) {
            bundle.putDouble(str, ((Double) obj).doubleValue());
        } else if (obj instanceof double[]) {
            bundle.putDoubleArray(str, (double[]) obj);
        } else if (obj instanceof Integer) {
            bundle.putInt(str, ((Integer) obj).intValue());
        } else if (obj instanceof int[]) {
            bundle.putIntArray(str, (int[]) obj);
        } else if (obj instanceof Long) {
            bundle.putLong(str, ((Long) obj).longValue());
        } else if (obj instanceof long[]) {
            bundle.putLongArray(str, (long[]) obj);
        } else if (obj instanceof String) {
            bundle.putString(str, (String) obj);
        } else if (obj instanceof JSONArray) {
            bundle.putString(str, obj.toString());
        } else if (!(obj instanceof JSONObject)) {
            return false;
        } else {
            bundle.putString(str, obj.toString());
        }
        return true;
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e2) {
            }
        }
    }

    public static void a(URLConnection uRLConnection) {
        if (uRLConnection != null && (uRLConnection instanceof HttpURLConnection)) {
            ((HttpURLConnection) uRLConnection).disconnect();
        }
    }

    public static String a(Context context) {
        v.a(context, "context");
        k.a(context);
        return k.j();
    }

    public static Object a(JSONObject jSONObject, String str, String str2) {
        Object obj;
        Object opt = jSONObject.opt(str);
        if (opt == null || !(opt instanceof String)) {
            obj = opt;
        } else {
            obj = new JSONTokener((String) opt).nextValue();
        }
        if (obj == null || (obj instanceof JSONObject) || (obj instanceof JSONArray)) {
            return obj;
        }
        if (str2 != null) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.putOpt(str2, obj);
            return jSONObject2;
        }
        throw new h("Got an unexpected non-JSON object.");
    }

    public static String a(InputStream inputStream) {
        InputStreamReader inputStreamReader;
        BufferedInputStream bufferedInputStream;
        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            try {
                inputStreamReader = new InputStreamReader(bufferedInputStream);
                try {
                    StringBuilder sb = new StringBuilder();
                    char[] cArr = new char[2048];
                    while (true) {
                        int read = inputStreamReader.read(cArr);
                        if (read != -1) {
                            sb.append(cArr, 0, read);
                        } else {
                            String sb2 = sb.toString();
                            a((Closeable) bufferedInputStream);
                            a(inputStreamReader);
                            return sb2;
                        }
                    }
                } catch (Throwable th) {
                    th = th;
                    a((Closeable) bufferedInputStream);
                    a(inputStreamReader);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                inputStreamReader = null;
                a((Closeable) bufferedInputStream);
                a(inputStreamReader);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            inputStreamReader = null;
            bufferedInputStream = null;
            a((Closeable) bufferedInputStream);
            a(inputStreamReader);
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.io.InputStream r5, java.io.OutputStream r6) {
        /*
            r0 = 0
            r2 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0023 }
            r1.<init>(r5)     // Catch:{ all -> 0x0023 }
            r2 = 8192(0x2000, float:1.14794E-41)
            byte[] r2 = new byte[r2]     // Catch:{ all -> 0x0030 }
        L_0x000b:
            int r3 = r1.read(r2)     // Catch:{ all -> 0x0030 }
            r4 = -1
            if (r3 == r4) goto L_0x0018
            r4 = 0
            r6.write(r2, r4, r3)     // Catch:{ all -> 0x0030 }
            int r0 = r0 + r3
            goto L_0x000b
        L_0x0018:
            if (r1 == 0) goto L_0x001d
            r1.close()
        L_0x001d:
            if (r5 == 0) goto L_0x0022
            r5.close()
        L_0x0022:
            return r0
        L_0x0023:
            r0 = move-exception
            r1 = r2
        L_0x0025:
            if (r1 == 0) goto L_0x002a
            r1.close()
        L_0x002a:
            if (r5 == 0) goto L_0x002f
            r5.close()
        L_0x002f:
            throw r0
        L_0x0030:
            r0 = move-exception
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.internal.u.a(java.io.InputStream, java.io.OutputStream):int");
    }

    private static void a(Context context, String str) {
        CookieSyncManager.createInstance(context).sync();
        CookieManager instance = CookieManager.getInstance();
        String cookie = instance.getCookie(str);
        if (cookie != null) {
            String[] split = cookie.split(";");
            for (String str2 : split) {
                String[] split2 = str2.split("=");
                if (split2.length > 0) {
                    instance.setCookie(str, split2[0].trim() + "=;expires=Sat, 1 Jan 2000 00:00:01 UTC;");
                }
            }
            instance.removeExpiredCookie();
        }
    }

    public static void b(Context context) {
        a(context, "facebook.com");
        a(context, ".facebook.com");
        a(context, "https://facebook.com");
        a(context, "https://.facebook.com");
    }

    public static void a(String str, Exception exc) {
        if (k.b() && str != null && exc != null) {
            Log.d(str, exc.getClass().getSimpleName() + ": " + exc.getMessage());
        }
    }

    public static void a(String str, String str2) {
        if (k.b() && str != null && str2 != null) {
            Log.d(str, str2);
        }
    }

    public static void a(String str, String str2, Throwable th) {
        if (k.b() && !a(str)) {
            Log.d(str, str2, th);
        }
    }

    public static <T> boolean a(T t, T t2) {
        if (t == null) {
            return t2 == null;
        }
        return t.equals(t2);
    }

    public static <T> List<T> b(T... tArr) {
        ArrayList arrayList = new ArrayList();
        for (T t : tArr) {
            if (t != null) {
                arrayList.add(t);
            }
        }
        return arrayList;
    }

    public static List<String> a(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(jSONArray.getString(i));
        }
        return arrayList;
    }

    public static void a(JSONObject jSONObject, a aVar, String str, boolean z) {
        boolean z2 = true;
        if (!(aVar == null || aVar.a() == null)) {
            jSONObject.put("attribution", aVar.a());
        }
        if (!(aVar == null || aVar.b() == null)) {
            jSONObject.put("advertiser_id", aVar.b());
            jSONObject.put("advertiser_tracking_enabled", !aVar.d());
        }
        if (!(aVar == null || aVar.c() == null)) {
            jSONObject.put("installer_package", aVar.c());
        }
        jSONObject.put("anon_id", str);
        if (z) {
            z2 = false;
        }
        jSONObject.put("application_tracking_enabled", z2);
    }

    public static void a(JSONObject jSONObject, Context context) {
        Locale locale;
        int i;
        int i2;
        double d2;
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("a2");
        g(context);
        String packageName = context.getPackageName();
        int i3 = -1;
        String str = "";
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            i3 = packageInfo.versionCode;
            str = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e2) {
        }
        jSONArray.put(packageName);
        jSONArray.put(i3);
        jSONArray.put(str);
        jSONArray.put(Build.VERSION.RELEASE);
        jSONArray.put(Build.MODEL);
        try {
            locale = context.getResources().getConfiguration().locale;
        } catch (Exception e3) {
            locale = Locale.getDefault();
        }
        jSONArray.put(locale.getLanguage() + "_" + locale.getCountry());
        jSONArray.put(e);
        jSONArray.put(g);
        try {
            WindowManager windowManager = (WindowManager) context.getSystemService("window");
            if (windowManager != null) {
                Display defaultDisplay = windowManager.getDefaultDisplay();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                defaultDisplay.getMetrics(displayMetrics);
                i = displayMetrics.widthPixels;
                try {
                    i2 = displayMetrics.heightPixels;
                    try {
                        d2 = (double) displayMetrics.density;
                    } catch (Exception e4) {
                        d2 = 0.0d;
                        jSONArray.put(i);
                        jSONArray.put(i2);
                        jSONArray.put(new DecimalFormat("#.##").format(d2));
                        jSONArray.put(b());
                        jSONArray.put(c);
                        jSONArray.put(d);
                        jSONArray.put(f);
                        jSONObject.put("extinfo", jSONArray.toString());
                    }
                } catch (Exception e5) {
                    i2 = 0;
                    d2 = 0.0d;
                    jSONArray.put(i);
                    jSONArray.put(i2);
                    jSONArray.put(new DecimalFormat("#.##").format(d2));
                    jSONArray.put(b());
                    jSONArray.put(c);
                    jSONArray.put(d);
                    jSONArray.put(f);
                    jSONObject.put("extinfo", jSONArray.toString());
                }
            } else {
                d2 = 0.0d;
                i2 = 0;
                i = 0;
            }
        } catch (Exception e6) {
            i2 = 0;
            i = 0;
            d2 = 0.0d;
            jSONArray.put(i);
            jSONArray.put(i2);
            jSONArray.put(new DecimalFormat("#.##").format(d2));
            jSONArray.put(b());
            jSONArray.put(c);
            jSONArray.put(d);
            jSONArray.put(f);
            jSONObject.put("extinfo", jSONArray.toString());
        }
        jSONArray.put(i);
        jSONArray.put(i2);
        jSONArray.put(new DecimalFormat("#.##").format(d2));
        jSONArray.put(b());
        jSONArray.put(c);
        jSONArray.put(d);
        jSONArray.put(f);
        jSONObject.put("extinfo", jSONArray.toString());
    }

    public static Method a(Class<?> cls, String str, Class<?>... clsArr) {
        try {
            return cls.getMethod(str, clsArr);
        } catch (NoSuchMethodException e2) {
            return null;
        }
    }

    public static Method a(String str, String str2, Class<?>... clsArr) {
        try {
            return a(Class.forName(str), str2, clsArr);
        } catch (ClassNotFoundException e2) {
            return null;
        }
    }

    public static Object a(Object obj, Method method, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException | InvocationTargetException e2) {
            return null;
        }
    }

    public static String c(Context context) {
        if (context == null) {
            return "null";
        }
        if (context == context.getApplicationContext()) {
            return "unknown";
        }
        return context.getClass().getSimpleName();
    }

    public static boolean a(Uri uri) {
        return uri != null && ("http".equalsIgnoreCase(uri.getScheme()) || "https".equalsIgnoreCase(uri.getScheme()) || "fbstaging".equalsIgnoreCase(uri.getScheme()));
    }

    public static boolean b(Uri uri) {
        return uri != null && "content".equalsIgnoreCase(uri.getScheme());
    }

    public static boolean c(Uri uri) {
        return uri != null && "file".equalsIgnoreCase(uri.getScheme());
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long d(android.net.Uri r7) {
        /*
            r6 = 0
            android.content.Context r0 = com.facebook.k.f()     // Catch:{ all -> 0x0025 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ all -> 0x0025 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r1 = r7
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0025 }
            java.lang.String r0 = "_size"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ all -> 0x002d }
            r1.moveToFirst()     // Catch:{ all -> 0x002d }
            long r2 = r1.getLong(r0)     // Catch:{ all -> 0x002d }
            if (r1 == 0) goto L_0x0024
            r1.close()
        L_0x0024:
            return r2
        L_0x0025:
            r0 = move-exception
            r1 = r6
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()
        L_0x002c:
            throw r0
        L_0x002d:
            r0 = move-exception
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.internal.u.d(android.net.Uri):long");
    }

    public static Date a(Bundle bundle, String str, Date date) {
        long parseLong;
        if (bundle == null) {
            return null;
        }
        Object obj = bundle.get(str);
        if (obj instanceof Long) {
            parseLong = ((Long) obj).longValue();
        } else if (!(obj instanceof String)) {
            return null;
        } else {
            try {
                parseLong = Long.parseLong((String) obj);
            } catch (NumberFormatException e2) {
                return null;
            }
        }
        if (parseLong == 0) {
            return new Date(Long.MAX_VALUE);
        }
        return new Date((parseLong * 1000) + date.getTime());
    }

    public static void a(Parcel parcel, Map<String, String> map) {
        if (map == null) {
            parcel.writeInt(-1);
            return;
        }
        parcel.writeInt(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            parcel.writeString(entry.getKey());
            parcel.writeString(entry.getValue());
        }
    }

    public static Map<String, String> a(Parcel parcel) {
        int readInt = parcel.readInt();
        if (readInt < 0) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (int i = 0; i < readInt; i++) {
            hashMap.put(parcel.readString(), parcel.readString());
        }
        return hashMap;
    }

    public static boolean a(AccessToken accessToken) {
        return accessToken != null && accessToken.equals(AccessToken.a());
    }

    public static void a(final String str, final a aVar) {
        JSONObject a2 = r.a(str);
        if (a2 != null) {
            aVar.a(a2);
            return;
        }
        AnonymousClass1 r0 = new GraphRequest.b() {
            /* class com.facebook.internal.u.AnonymousClass1 */

            @Override // com.facebook.GraphRequest.b
            public void a(p pVar) {
                if (pVar.a() != null) {
                    aVar.a(pVar.a().f());
                    return;
                }
                r.a(str, pVar.b());
                aVar.a(pVar.b());
            }
        };
        GraphRequest e2 = e(str);
        e2.a((GraphRequest.b) r0);
        e2.j();
    }

    public static JSONObject d(String str) {
        JSONObject a2 = r.a(str);
        if (a2 != null) {
            return a2;
        }
        p i = e(str).i();
        if (i.a() != null) {
            return null;
        }
        return i.b();
    }

    private static GraphRequest e(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id,name,first_name,middle_name,last_name,link");
        bundle.putString("access_token", str);
        return new GraphRequest(null, "me", bundle, q.GET, null);
    }

    private static int b() {
        if (f1133a > 0) {
            return f1133a;
        }
        try {
            File[] listFiles = new File("/sys/devices/system/cpu/").listFiles(new FilenameFilter() {
                /* class com.facebook.internal.u.AnonymousClass2 */

                public boolean accept(File file, String str) {
                    return Pattern.matches("cpu[0-9]+", str);
                }
            });
            if (listFiles != null) {
                f1133a = listFiles.length;
            }
        } catch (Exception e2) {
        }
        if (f1133a <= 0) {
            f1133a = Math.max(Runtime.getRuntime().availableProcessors(), 1);
        }
        return f1133a;
    }

    private static void g(Context context) {
        if (b == -1 || System.currentTimeMillis() - b >= 1800000) {
            b = System.currentTimeMillis();
            c();
            h(context);
            f();
            e();
        }
    }

    private static void c() {
        try {
            TimeZone timeZone = TimeZone.getDefault();
            e = timeZone.getDisplayName(timeZone.inDaylightTime(new Date()), 0);
            f = timeZone.getID();
        } catch (AssertionError | Exception e2) {
        }
    }

    private static void h(Context context) {
        if (g.equals("NoCarrier")) {
            try {
                g = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
            } catch (Exception e2) {
            }
        }
    }

    private static boolean d() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    private static void e() {
        try {
            if (d()) {
                StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
                d = ((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks());
            }
            d = a((double) d);
        } catch (Exception e2) {
        }
    }

    private static void f() {
        try {
            if (d()) {
                StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
                c = ((long) statFs.getBlockSize()) * ((long) statFs.getBlockCount());
            }
            c = a((double) c);
        } catch (Exception e2) {
        }
    }

    private static long a(double d2) {
        return Math.round(d2 / 1.073741824E9d);
    }

    public static class b {

        /* renamed from: a  reason: collision with root package name */
        List<String> f1135a;
        List<String> b;

        public b(List<String> list, List<String> list2) {
            this.f1135a = list;
            this.b = list2;
        }

        public List<String> a() {
            return this.f1135a;
        }

        public List<String> b() {
            return this.b;
        }
    }

    public static b a(JSONObject jSONObject) {
        String optString;
        JSONArray jSONArray = jSONObject.getJSONObject("permissions").getJSONArray("data");
        ArrayList arrayList = new ArrayList(jSONArray.length());
        ArrayList arrayList2 = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            String optString2 = optJSONObject.optString("permission");
            if (!(optString2 == null || optString2.equals("installed") || (optString = optJSONObject.optString(NotificationCompat.CATEGORY_STATUS)) == null)) {
                if (optString.equals("granted")) {
                    arrayList.add(optString2);
                } else if (optString.equals("declined")) {
                    arrayList2.add(optString2);
                }
            }
        }
        return new b(arrayList, arrayList2);
    }

    public static String a(int i) {
        return new BigInteger(i * 5, new Random()).toString(32);
    }

    public static boolean d(Context context) {
        return e(context);
    }

    public static boolean e(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return false;
        }
        AutofillManager autofillManager = (AutofillManager) context.getSystemService(AutofillManager.class);
        return autofillManager != null && autofillManager.isAutofillSupported() && autofillManager.isEnabled();
    }

    public static boolean f(Context context) {
        if (Build.VERSION.SDK_INT >= 27) {
            return context.getPackageManager().hasSystemFeature("android.hardware.type.pc");
        }
        return Build.DEVICE != null && Build.DEVICE.matches(".+_cheets|cheets_.+");
    }

    public static Locale a() {
        try {
            return k.f().getResources().getConfiguration().locale;
        } catch (Exception e2) {
            return Locale.getDefault();
        }
    }
}
