package com.facebook;

public class i extends h {

    /* renamed from: a  reason: collision with root package name */
    private final p f1095a;

    public i(p pVar, String str) {
        super(str);
        this.f1095a = pVar;
    }

    @Override // com.facebook.h
    public final String toString() {
        FacebookRequestError a2 = this.f1095a != null ? this.f1095a.a() : null;
        StringBuilder append = new StringBuilder().append("{FacebookGraphResponseException: ");
        String message = getMessage();
        if (message != null) {
            append.append(message);
            append.append(" ");
        }
        if (a2 != null) {
            append.append("httpResponseCode: ").append(a2.a()).append(", facebookErrorCode: ").append(a2.b()).append(", facebookErrorType: ").append(a2.d()).append(", message: ").append(a2.e()).append("}");
        }
        return append.toString();
    }
}
