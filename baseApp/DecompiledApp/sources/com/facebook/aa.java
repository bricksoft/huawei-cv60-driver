package com.facebook;

import android.os.Handler;
import com.facebook.GraphRequest;

/* access modifiers changed from: package-private */
public class aa {

    /* renamed from: a  reason: collision with root package name */
    private final GraphRequest f1080a;
    private final Handler b;
    private final long c = k.i();
    private long d;
    private long e;
    private long f;

    aa(Handler handler, GraphRequest graphRequest) {
        this.f1080a = graphRequest;
        this.b = handler;
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        this.d += j;
        if (this.d >= this.e + this.c || this.d >= this.f) {
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(long j) {
        this.f += j;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.d > this.e) {
            GraphRequest.b g = this.f1080a.g();
            if (this.f > 0 && (g instanceof GraphRequest.e)) {
                final long j = this.d;
                final long j2 = this.f;
                final GraphRequest.e eVar = (GraphRequest.e) g;
                if (this.b == null) {
                    eVar.a(j, j2);
                } else {
                    this.b.post(new Runnable() {
                        /* class com.facebook.aa.AnonymousClass1 */

                        public void run() {
                            eVar.a(j, j2);
                        }
                    });
                }
                this.e = this.d;
            }
        }
    }
}
