package com.facebook;

public class h extends RuntimeException {
    static final long serialVersionUID = 1;

    public h() {
    }

    public h(String str) {
        super(str);
    }

    public h(String str, Throwable th) {
        super(str, th);
    }

    public h(Throwable th) {
        super(th);
    }

    public String toString() {
        return getMessage();
    }
}
