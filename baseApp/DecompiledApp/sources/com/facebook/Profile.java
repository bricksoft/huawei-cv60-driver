package com.facebook;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;
import com.facebook.internal.u;
import com.facebook.internal.v;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import org.json.JSONException;
import org.json.JSONObject;

public final class Profile implements Parcelable {
    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        /* class com.facebook.Profile.AnonymousClass2 */

        /* renamed from: a */
        public Profile createFromParcel(Parcel parcel) {
            return new Profile(parcel);
        }

        /* renamed from: a */
        public Profile[] newArray(int i) {
            return new Profile[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private static final String f1011a = Profile.class.getSimpleName();
    @Nullable
    private final String b;
    @Nullable
    private final String c;
    @Nullable
    private final String d;
    @Nullable
    private final String e;
    @Nullable
    private final String f;
    @Nullable
    private final Uri g;

    public static Profile a() {
        return v.a().b();
    }

    public static void a(@Nullable Profile profile) {
        v.a().a(profile);
    }

    public static void b() {
        AccessToken a2 = AccessToken.a();
        if (!AccessToken.b()) {
            a(null);
        } else {
            u.a(a2.d(), (u.a) new u.a() {
                /* class com.facebook.Profile.AnonymousClass1 */

                @Override // com.facebook.internal.u.a
                public void a(JSONObject jSONObject) {
                    String optString = jSONObject.optString(TtmlNode.ATTR_ID);
                    if (optString != null) {
                        String optString2 = jSONObject.optString("link");
                        Profile.a(new Profile(optString, jSONObject.optString("first_name"), jSONObject.optString("middle_name"), jSONObject.optString("last_name"), jSONObject.optString("name"), optString2 != null ? Uri.parse(optString2) : null));
                    }
                }

                @Override // com.facebook.internal.u.a
                public void a(h hVar) {
                    Log.e(Profile.f1011a, "Got unexpected exception: " + hVar);
                }
            });
        }
    }

    public Profile(String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable Uri uri) {
        v.a(str, TtmlNode.ATTR_ID);
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = uri;
    }

    public String c() {
        return this.b;
    }

    public String d() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Profile)) {
            return false;
        }
        Profile profile = (Profile) obj;
        if (this.b.equals(profile.b) && this.c == null) {
            return profile.c == null;
        }
        if (this.c.equals(profile.c) && this.d == null) {
            return profile.d == null;
        }
        if (this.d.equals(profile.d) && this.e == null) {
            return profile.e == null;
        }
        if (this.e.equals(profile.e) && this.f == null) {
            return profile.f == null;
        }
        if (!this.f.equals(profile.f) || this.g != null) {
            return this.g.equals(profile.g);
        }
        return profile.g == null;
    }

    public int hashCode() {
        int hashCode = this.b.hashCode() + 527;
        if (this.c != null) {
            hashCode = (hashCode * 31) + this.c.hashCode();
        }
        if (this.d != null) {
            hashCode = (hashCode * 31) + this.d.hashCode();
        }
        if (this.e != null) {
            hashCode = (hashCode * 31) + this.e.hashCode();
        }
        if (this.f != null) {
            hashCode = (hashCode * 31) + this.f.hashCode();
        }
        if (this.g != null) {
            return (hashCode * 31) + this.g.hashCode();
        }
        return hashCode;
    }

    /* access modifiers changed from: package-private */
    public JSONObject e() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(TtmlNode.ATTR_ID, this.b);
            jSONObject.put("first_name", this.c);
            jSONObject.put("middle_name", this.d);
            jSONObject.put("last_name", this.e);
            jSONObject.put("name", this.f);
            if (this.g == null) {
                return jSONObject;
            }
            jSONObject.put("link_uri", this.g.toString());
            return jSONObject;
        } catch (JSONException e2) {
            return null;
        }
    }

    Profile(JSONObject jSONObject) {
        Uri uri = null;
        this.b = jSONObject.optString(TtmlNode.ATTR_ID, null);
        this.c = jSONObject.optString("first_name", null);
        this.d = jSONObject.optString("middle_name", null);
        this.e = jSONObject.optString("last_name", null);
        this.f = jSONObject.optString("name", null);
        String optString = jSONObject.optString("link_uri", null);
        this.g = optString != null ? Uri.parse(optString) : uri;
    }

    private Profile(Parcel parcel) {
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readString();
        String readString = parcel.readString();
        this.g = readString == null ? null : Uri.parse(readString);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g == null ? null : this.g.toString());
    }
}
