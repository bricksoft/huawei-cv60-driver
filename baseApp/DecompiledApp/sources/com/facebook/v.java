package com.facebook;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import com.facebook.internal.u;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    private static volatile v f1233a;
    private final LocalBroadcastManager b;
    private final u c;
    private Profile d;

    v(LocalBroadcastManager localBroadcastManager, u uVar) {
        com.facebook.internal.v.a(localBroadcastManager, "localBroadcastManager");
        com.facebook.internal.v.a(uVar, "profileCache");
        this.b = localBroadcastManager;
        this.c = uVar;
    }

    static v a() {
        if (f1233a == null) {
            synchronized (v.class) {
                if (f1233a == null) {
                    f1233a = new v(LocalBroadcastManager.getInstance(k.f()), new u());
                }
            }
        }
        return f1233a;
    }

    /* access modifiers changed from: package-private */
    public Profile b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        Profile a2 = this.c.a();
        if (a2 == null) {
            return false;
        }
        a(a2, false);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable Profile profile) {
        a(profile, true);
    }

    private void a(@Nullable Profile profile, boolean z) {
        Profile profile2 = this.d;
        this.d = profile;
        if (z) {
            if (profile != null) {
                this.c.a(profile);
            } else {
                this.c.b();
            }
        }
        if (!u.a(profile2, profile)) {
            a(profile2, profile);
        }
    }

    private void a(Profile profile, Profile profile2) {
        Intent intent = new Intent("com.facebook.sdk.ACTION_CURRENT_PROFILE_CHANGED");
        intent.putExtra("com.facebook.sdk.EXTRA_OLD_PROFILE", profile);
        intent.putExtra("com.facebook.sdk.EXTRA_NEW_PROFILE", profile2);
        this.b.sendBroadcast(intent);
    }
}
