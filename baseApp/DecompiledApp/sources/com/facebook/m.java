package com.facebook;

public class m extends h {
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    private final FacebookRequestError f1180a;

    public m(FacebookRequestError facebookRequestError, String str) {
        super(str);
        this.f1180a = facebookRequestError;
    }

    public final FacebookRequestError a() {
        return this.f1180a;
    }

    @Override // com.facebook.h
    public final String toString() {
        return "{FacebookServiceException: " + "httpResponseCode: " + this.f1180a.a() + ", facebookErrorCode: " + this.f1180a.b() + ", facebookErrorType: " + this.f1180a.d() + ", message: " + this.f1180a.e() + "}";
    }
}
