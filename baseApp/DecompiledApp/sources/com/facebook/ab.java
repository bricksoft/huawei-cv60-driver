package com.facebook;

import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.facebook.GraphRequest;
import com.facebook.internal.k;
import com.facebook.internal.l;
import com.facebook.internal.u;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

/* access modifiers changed from: package-private */
public final class ab {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1082a = ab.class.getName();
    private static AtomicBoolean b = new AtomicBoolean(false);
    private static a c = new a(true, "com.facebook.sdk.AutoLogAppEventsEnabled", "com.facebook.sdk.AutoLogAppEventsEnabled");
    private static a d = new a(true, "com.facebook.sdk.AdvertiserIDCollectionEnabled", "com.facebook.sdk.AdvertiserIDCollectionEnabled");
    private static a e = new a(false, "auto_event_setup_enabled", null);
    private static SharedPreferences f;
    private static SharedPreferences.Editor g;

    ab() {
    }

    public static void a() {
        if (k.a() && b.compareAndSet(false, true)) {
            f = k.f().getSharedPreferences("com.facebook.sdk.USER_SETTINGS", 0);
            g = f.edit();
            b(c);
            b(d);
            g();
        }
    }

    private static void b(a aVar) {
        if (aVar == e) {
            g();
        } else if (aVar.c == null) {
            d(aVar);
            if (aVar.c == null && aVar.b != null) {
                e(aVar);
            }
        } else {
            c(aVar);
        }
    }

    private static void g() {
        d(e);
        final long currentTimeMillis = System.currentTimeMillis();
        if (e.c == null || currentTimeMillis - e.e >= 604800000) {
            e.c = null;
            e.e = 0;
            k.d().execute(new Runnable() {
                /* class com.facebook.ab.AnonymousClass1 */

                public void run() {
                    k a2;
                    String str;
                    if (ab.d.a() && (a2 = l.a(k.j(), false)) != null && a2.h()) {
                        com.facebook.internal.a a3 = com.facebook.internal.a.a(k.f());
                        if (a3 == null || a3.b() == null) {
                            str = null;
                        } else {
                            str = a3.b();
                        }
                        if (str != null) {
                            Bundle bundle = new Bundle();
                            bundle.putString("advertiser_id", a3.b());
                            bundle.putString("fields", "auto_event_setup_enabled");
                            GraphRequest a4 = GraphRequest.a((AccessToken) null, k.j(), (GraphRequest.b) null);
                            a4.a(true);
                            a4.a(bundle);
                            JSONObject b = a4.i().b();
                            if (b != null) {
                                ab.e.c = Boolean.valueOf(b.optBoolean("auto_event_setup_enabled", false));
                                ab.e.e = currentTimeMillis;
                                ab.c(ab.e);
                            }
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static void c(a aVar) {
        h();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("value", aVar.c);
            jSONObject.put("last_timestamp", aVar.e);
            g.putString(aVar.f1084a, jSONObject.toString()).commit();
        } catch (JSONException e2) {
            u.a(f1082a, (Exception) e2);
        }
    }

    private static void d(a aVar) {
        h();
        try {
            String string = f.getString(aVar.f1084a, "");
            if (!string.isEmpty()) {
                JSONObject jSONObject = new JSONObject(string);
                aVar.c = Boolean.valueOf(jSONObject.getBoolean("value"));
                aVar.e = jSONObject.getLong("last_timestamp");
            }
        } catch (JSONException e2) {
            u.a(f1082a, (Exception) e2);
        }
    }

    private static void e(a aVar) {
        h();
        try {
            ApplicationInfo applicationInfo = k.f().getPackageManager().getApplicationInfo(k.f().getPackageName(), 128);
            if (applicationInfo != null && applicationInfo.metaData != null && applicationInfo.metaData.containsKey(aVar.b)) {
                aVar.c = Boolean.valueOf(applicationInfo.metaData.getBoolean(aVar.b, aVar.d));
            }
        } catch (PackageManager.NameNotFoundException e2) {
            u.a(f1082a, (Exception) e2);
        }
    }

    private static void h() {
        if (!b.get()) {
            throw new l("The UserSettingManager has not been initialized successfully");
        }
    }

    public static boolean b() {
        a();
        return c.a();
    }

    public static boolean c() {
        a();
        return d.a();
    }

    public static boolean d() {
        a();
        return e.a();
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        String f1084a;
        String b;
        Boolean c;
        boolean d;
        long e;

        a(boolean z, String str, String str2) {
            this.d = z;
            this.f1084a = str;
            this.b = str2;
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.c == null ? this.d : this.c.booleanValue();
        }
    }
}
