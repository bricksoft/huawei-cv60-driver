package com.facebook;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import com.facebook.GraphRequest;
import com.facebook.a.b.c;
import com.facebook.a.g;
import com.facebook.internal.b;
import com.facebook.internal.l;
import com.facebook.internal.n;
import com.facebook.internal.p;
import com.facebook.internal.s;
import com.facebook.internal.u;
import com.facebook.internal.v;
import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONException;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1146a = k.class.getCanonicalName();
    private static final HashSet<t> b = new HashSet<>(Arrays.asList(t.DEVELOPER_ERRORS));
    private static Executor c;
    private static volatile String d;
    private static volatile String e;
    private static volatile String f;
    private static volatile Boolean g;
    private static volatile String h = "facebook.com";
    private static AtomicLong i = new AtomicLong(65536);
    private static volatile boolean j = false;
    private static boolean k = false;
    private static n<File> l;
    private static Context m;
    private static int n = 64206;
    private static final Object o = new Object();
    private static String p = s.d();
    private static final BlockingQueue<Runnable> q = new LinkedBlockingQueue(10);
    private static final ThreadFactory r = new ThreadFactory() {
        /* class com.facebook.k.AnonymousClass1 */

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f1147a = new AtomicInteger(0);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "FacebookSdk #" + this.f1147a.incrementAndGet());
        }
    };
    private static Boolean s = false;

    public interface a {
        void a();
    }

    @Deprecated
    public static synchronized void a(Context context) {
        synchronized (k.class) {
            a(context, (a) null);
        }
    }

    @Deprecated
    public static synchronized void a(final Context context, final a aVar) {
        synchronized (k.class) {
            if (!s.booleanValue()) {
                v.a(context, "applicationContext");
                v.b(context, false);
                v.a(context, false);
                m = context.getApplicationContext();
                c(m);
                if (u.a(d)) {
                    throw new h("A valid Facebook app id must be set in the AndroidManifest.xml or set by calling FacebookSdk.setApplicationId before initializing the sdk.");
                }
                s = true;
                if ((m instanceof Application) && ab.b()) {
                    com.facebook.a.b.a.a((Application) m, d);
                }
                l.a();
                p.b();
                b.a(m);
                l = new n<>(new Callable<File>() {
                    /* class com.facebook.k.AnonymousClass2 */

                    /* renamed from: a */
                    public File call() {
                        return k.m.getCacheDir();
                    }
                });
                d().execute(new FutureTask(new Callable<Void>() {
                    /* class com.facebook.k.AnonymousClass3 */

                    /* renamed from: a */
                    public Void call() {
                        b.a().c();
                        v.a().c();
                        if (AccessToken.b() && Profile.a() == null) {
                            Profile.b();
                        }
                        if (aVar != null) {
                            aVar.a();
                        }
                        g.a(k.m, k.d);
                        g.a(context.getApplicationContext()).b();
                        return null;
                    }
                }));
            } else if (aVar != null) {
                aVar.a();
            }
        }
    }

    public static synchronized boolean a() {
        boolean booleanValue;
        synchronized (k.class) {
            booleanValue = s.booleanValue();
        }
        return booleanValue;
    }

    public static boolean a(t tVar) {
        boolean z;
        synchronized (b) {
            z = b() && b.contains(tVar);
        }
        return z;
    }

    public static boolean b() {
        return j;
    }

    public static boolean c() {
        return k;
    }

    public static Executor d() {
        synchronized (o) {
            if (c == null) {
                c = AsyncTask.THREAD_POOL_EXECUTOR;
            }
        }
        return c;
    }

    public static String e() {
        return h;
    }

    public static Context f() {
        v.a();
        return m;
    }

    public static String g() {
        u.a(f1146a, String.format("getGraphApiVersion: %s", p));
        return p;
    }

    public static void a(Context context, final String str) {
        final Context applicationContext = context.getApplicationContext();
        d().execute(new Runnable() {
            /* class com.facebook.k.AnonymousClass4 */

            public void run() {
                k.b(applicationContext, str);
            }
        });
    }

    static void b(Context context, String str) {
        if (context == null || str == null) {
            try {
                throw new IllegalArgumentException("Both context and applicationId must be non-null");
            } catch (Exception e2) {
                u.a("Facebook-publish", e2);
            }
        } else {
            com.facebook.internal.a a2 = com.facebook.internal.a.a(context);
            SharedPreferences sharedPreferences = context.getSharedPreferences("com.facebook.sdk.attributionTracking", 0);
            String str2 = str + "ping";
            long j2 = sharedPreferences.getLong(str2, 0);
            try {
                GraphRequest a3 = GraphRequest.a((AccessToken) null, String.format("%s/activities", str), c.a(c.a.MOBILE_INSTALL_EVENT, a2, g.b(context), b(context), context), (GraphRequest.b) null);
                if (j2 == 0) {
                    a3.i();
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putLong(str2, System.currentTimeMillis());
                    edit.apply();
                }
            } catch (JSONException e3) {
                throw new h("An error occurred while publishing install.", e3);
            }
        }
    }

    public static String h() {
        return "4.42.0";
    }

    public static boolean b(Context context) {
        v.a();
        return context.getSharedPreferences("com.facebook.sdk.appEventPreferences", 0).getBoolean("limitEventUsage", false);
    }

    public static long i() {
        v.a();
        return i.get();
    }

    static void c(Context context) {
        if (context != null) {
            try {
                ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo != null && applicationInfo.metaData != null) {
                    if (d == null) {
                        Object obj = applicationInfo.metaData.get("com.facebook.sdk.ApplicationId");
                        if (obj instanceof String) {
                            String str = (String) obj;
                            if (str.toLowerCase(Locale.ROOT).startsWith("fb")) {
                                d = str.substring(2);
                            } else {
                                d = str;
                            }
                        } else if (obj instanceof Integer) {
                            throw new h("App Ids cannot be directly placed in the manifest.They must be prefixed by 'fb' or be placed in the string resource file.");
                        }
                    }
                    if (e == null) {
                        e = applicationInfo.metaData.getString("com.facebook.sdk.ApplicationName");
                    }
                    if (f == null) {
                        f = applicationInfo.metaData.getString("com.facebook.sdk.ClientToken");
                    }
                    if (n == 64206) {
                        n = applicationInfo.metaData.getInt("com.facebook.sdk.CallbackOffset", 64206);
                    }
                    if (g == null) {
                        g = Boolean.valueOf(applicationInfo.metaData.getBoolean("com.facebook.sdk.CodelessDebugLogEnabled", false));
                    }
                }
            } catch (PackageManager.NameNotFoundException e2) {
            }
        }
    }

    public static String j() {
        v.a();
        return d;
    }

    public static String k() {
        v.a();
        return f;
    }

    public static boolean l() {
        return ab.b();
    }

    public static boolean m() {
        return ab.d();
    }

    public static boolean n() {
        return ab.c();
    }

    public static int o() {
        v.a();
        return n;
    }
}
