package com.facebook;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.internal.u;
import com.facebook.internal.v;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class AccessToken implements Parcelable {
    public static final Parcelable.Creator<AccessToken> CREATOR = new Parcelable.Creator() {
        /* class com.facebook.AccessToken.AnonymousClass1 */

        /* renamed from: a */
        public AccessToken createFromParcel(Parcel parcel) {
            return new AccessToken(parcel);
        }

        /* renamed from: a */
        public AccessToken[] newArray(int i) {
            return new AccessToken[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private static final Date f994a = new Date(Long.MAX_VALUE);
    private static final Date b = f994a;
    private static final Date c = new Date();
    private static final c d = c.FACEBOOK_APPLICATION_WEB;
    private final Date e;
    private final Set<String> f;
    private final Set<String> g;
    private final String h;
    private final c i;
    private final Date j;
    private final String k;
    private final String l;
    private final Date m;

    public interface a {
        void a(AccessToken accessToken);

        void a(h hVar);
    }

    public AccessToken(String str, String str2, String str3, @Nullable Collection<String> collection, @Nullable Collection<String> collection2, @Nullable c cVar, @Nullable Date date, @Nullable Date date2, @Nullable Date date3) {
        v.a(str, "accessToken");
        v.a(str2, "applicationId");
        v.a(str3, "userId");
        this.e = date == null ? b : date;
        this.f = Collections.unmodifiableSet(collection != null ? new HashSet(collection) : new HashSet());
        this.g = Collections.unmodifiableSet(collection2 != null ? new HashSet(collection2) : new HashSet());
        this.h = str;
        this.i = cVar == null ? d : cVar;
        this.j = date2 == null ? c : date2;
        this.k = str2;
        this.l = str3;
        this.m = (date3 == null || date3.getTime() == 0) ? b : date3;
    }

    public static AccessToken a() {
        return b.a().b();
    }

    public static boolean b() {
        AccessToken b2 = b.a().b();
        return b2 != null && !b2.m();
    }

    static void c() {
        AccessToken b2 = b.a().b();
        if (b2 != null) {
            a(b(b2));
        }
    }

    public static void a(AccessToken accessToken) {
        b.a().a(accessToken);
    }

    public String d() {
        return this.h;
    }

    public Date e() {
        return this.e;
    }

    public Date f() {
        return this.m;
    }

    public Set<String> g() {
        return this.f;
    }

    public Set<String> h() {
        return this.g;
    }

    public c i() {
        return this.i;
    }

    public Date j() {
        return this.j;
    }

    public String k() {
        return this.k;
    }

    public String l() {
        return this.l;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{AccessToken");
        sb.append(" token:").append(o());
        a(sb);
        sb.append("}");
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AccessToken)) {
            return false;
        }
        AccessToken accessToken = (AccessToken) obj;
        return this.e.equals(accessToken.e) && this.f.equals(accessToken.f) && this.g.equals(accessToken.g) && this.h.equals(accessToken.h) && this.i == accessToken.i && this.j.equals(accessToken.j) && (this.k != null ? this.k.equals(accessToken.k) : accessToken.k == null) && this.l.equals(accessToken.l) && this.m.equals(accessToken.m);
    }

    public int hashCode() {
        return (((((this.k == null ? 0 : this.k.hashCode()) + ((((((((((((this.e.hashCode() + 527) * 31) + this.f.hashCode()) * 31) + this.g.hashCode()) * 31) + this.h.hashCode()) * 31) + this.i.hashCode()) * 31) + this.j.hashCode()) * 31)) * 31) + this.l.hashCode()) * 31) + this.m.hashCode();
    }

    static AccessToken b(AccessToken accessToken) {
        return new AccessToken(accessToken.h, accessToken.k, accessToken.l(), accessToken.g(), accessToken.h(), accessToken.i, new Date(), new Date(), accessToken.m);
    }

    static AccessToken a(Bundle bundle) {
        List<String> a2 = a(bundle, "com.facebook.TokenCachingStrategy.Permissions");
        List<String> a3 = a(bundle, "com.facebook.TokenCachingStrategy.DeclinedPermissions");
        String d2 = s.d(bundle);
        if (u.a(d2)) {
            d2 = k.j();
        }
        String b2 = s.b(bundle);
        try {
            return new AccessToken(b2, d2, u.d(b2).getString(TtmlNode.ATTR_ID), a2, a3, s.c(bundle), s.a(bundle, "com.facebook.TokenCachingStrategy.ExpirationDate"), s.a(bundle, "com.facebook.TokenCachingStrategy.LastRefreshDate"), null);
        } catch (JSONException e2) {
            return null;
        }
    }

    static List<String> a(Bundle bundle, String str) {
        ArrayList<String> stringArrayList = bundle.getStringArrayList(str);
        if (stringArrayList == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(new ArrayList(stringArrayList));
    }

    public boolean m() {
        return new Date().after(this.e);
    }

    /* access modifiers changed from: package-private */
    public JSONObject n() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("version", 1);
        jSONObject.put("token", this.h);
        jSONObject.put("expires_at", this.e.getTime());
        jSONObject.put("permissions", new JSONArray((Collection) this.f));
        jSONObject.put("declined_permissions", new JSONArray((Collection) this.g));
        jSONObject.put("last_refresh", this.j.getTime());
        jSONObject.put("source", this.i.name());
        jSONObject.put("application_id", this.k);
        jSONObject.put("user_id", this.l);
        jSONObject.put("data_access_expiration_time", this.m.getTime());
        return jSONObject;
    }

    static AccessToken a(JSONObject jSONObject) {
        if (jSONObject.getInt("version") > 1) {
            throw new h("Unknown AccessToken serialization format.");
        }
        String string = jSONObject.getString("token");
        Date date = new Date(jSONObject.getLong("expires_at"));
        JSONArray jSONArray = jSONObject.getJSONArray("permissions");
        JSONArray jSONArray2 = jSONObject.getJSONArray("declined_permissions");
        Date date2 = new Date(jSONObject.getLong("last_refresh"));
        c valueOf = c.valueOf(jSONObject.getString("source"));
        return new AccessToken(string, jSONObject.getString("application_id"), jSONObject.getString("user_id"), u.a(jSONArray), u.a(jSONArray2), valueOf, date, date2, new Date(jSONObject.optLong("data_access_expiration_time", 0)));
    }

    private String o() {
        if (this.h == null) {
            return "null";
        }
        if (k.a(t.INCLUDE_ACCESS_TOKENS)) {
            return this.h;
        }
        return "ACCESS_TOKEN_REMOVED";
    }

    private void a(StringBuilder sb) {
        sb.append(" permissions:");
        if (this.f == null) {
            sb.append("null");
            return;
        }
        sb.append("[");
        sb.append(TextUtils.join(", ", this.f));
        sb.append("]");
    }

    AccessToken(Parcel parcel) {
        this.e = new Date(parcel.readLong());
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        this.f = Collections.unmodifiableSet(new HashSet(arrayList));
        arrayList.clear();
        parcel.readStringList(arrayList);
        this.g = Collections.unmodifiableSet(new HashSet(arrayList));
        this.h = parcel.readString();
        this.i = c.valueOf(parcel.readString());
        this.j = new Date(parcel.readLong());
        this.k = parcel.readString();
        this.l = parcel.readString();
        this.m = new Date(parcel.readLong());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeLong(this.e.getTime());
        parcel.writeStringList(new ArrayList(this.f));
        parcel.writeStringList(new ArrayList(this.g));
        parcel.writeString(this.h);
        parcel.writeString(this.i.name());
        parcel.writeLong(this.j.getTime());
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeLong(this.m.getTime());
    }
}
