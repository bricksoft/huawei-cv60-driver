package com.facebook;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.internal.u;
import com.facebook.internal.v;
import com.facebook.o;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static volatile b f1085a;
    private final LocalBroadcastManager b;
    private final a c;
    private AccessToken d;
    private AtomicBoolean e = new AtomicBoolean(false);
    private Date f = new Date(0);

    b(LocalBroadcastManager localBroadcastManager, a aVar) {
        v.a(localBroadcastManager, "localBroadcastManager");
        v.a(aVar, "accessTokenCache");
        this.b = localBroadcastManager;
        this.c = aVar;
    }

    static b a() {
        if (f1085a == null) {
            synchronized (b.class) {
                if (f1085a == null) {
                    f1085a = new b(LocalBroadcastManager.getInstance(k.f()), new a());
                }
            }
        }
        return f1085a;
    }

    /* access modifiers changed from: package-private */
    public AccessToken b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        AccessToken a2 = this.c.a();
        if (a2 == null) {
            return false;
        }
        a(a2, false);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(AccessToken accessToken) {
        a(accessToken, true);
    }

    private void a(AccessToken accessToken, boolean z) {
        AccessToken accessToken2 = this.d;
        this.d = accessToken;
        this.e.set(false);
        this.f = new Date(0);
        if (z) {
            if (accessToken != null) {
                this.c.a(accessToken);
            } else {
                this.c.b();
                u.b(k.f());
            }
        }
        if (!u.a(accessToken2, accessToken)) {
            a(accessToken2, accessToken);
            f();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        a(this.d, this.d);
    }

    private void a(AccessToken accessToken, AccessToken accessToken2) {
        Intent intent = new Intent(k.f(), CurrentAccessTokenExpirationBroadcastReceiver.class);
        intent.setAction("com.facebook.sdk.ACTION_CURRENT_ACCESS_TOKEN_CHANGED");
        intent.putExtra("com.facebook.sdk.EXTRA_OLD_ACCESS_TOKEN", accessToken);
        intent.putExtra("com.facebook.sdk.EXTRA_NEW_ACCESS_TOKEN", accessToken2);
        this.b.sendBroadcast(intent);
    }

    private void f() {
        Context f2 = k.f();
        AccessToken a2 = AccessToken.a();
        AlarmManager alarmManager = (AlarmManager) f2.getSystemService(NotificationCompat.CATEGORY_ALARM);
        if (AccessToken.b() && a2.e() != null && alarmManager != null) {
            Intent intent = new Intent(f2, CurrentAccessTokenExpirationBroadcastReceiver.class);
            intent.setAction("com.facebook.sdk.ACTION_CURRENT_ACCESS_TOKEN_CHANGED");
            alarmManager.set(1, a2.e().getTime(), PendingIntent.getBroadcast(f2, 0, intent, 0));
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (g()) {
            a((AccessToken.a) null);
        }
    }

    private boolean g() {
        if (this.d == null) {
            return false;
        }
        Long valueOf = Long.valueOf(new Date().getTime());
        if (!this.d.i().a() || valueOf.longValue() - this.f.getTime() <= 3600000 || valueOf.longValue() - this.d.j().getTime() <= 86400000) {
            return false;
        }
        return true;
    }

    private static GraphRequest a(AccessToken accessToken, GraphRequest.b bVar) {
        return new GraphRequest(accessToken, "me/permissions", new Bundle(), q.GET, bVar);
    }

    private static GraphRequest b(AccessToken accessToken, GraphRequest.b bVar) {
        Bundle bundle = new Bundle();
        bundle.putString("grant_type", "fb_extend_sso_token");
        return new GraphRequest(accessToken, "oauth/access_token", bundle, q.GET, bVar);
    }

    /* access modifiers changed from: private */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f1090a;
        public int b;
        public Long c;

        private a() {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final AccessToken.a aVar) {
        if (Looper.getMainLooper().equals(Looper.myLooper())) {
            b(aVar);
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                /* class com.facebook.b.AnonymousClass1 */

                public void run() {
                    b.this.b(aVar);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void b(final AccessToken.a aVar) {
        final AccessToken accessToken = this.d;
        if (accessToken == null) {
            if (aVar != null) {
                aVar.a(new h("No current access token to refresh"));
            }
        } else if (this.e.compareAndSet(false, true)) {
            this.f = new Date();
            final HashSet hashSet = new HashSet();
            final HashSet hashSet2 = new HashSet();
            final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
            final a aVar2 = new a();
            o oVar = new o(a(accessToken, new GraphRequest.b() {
                /* class com.facebook.b.AnonymousClass2 */

                @Override // com.facebook.GraphRequest.b
                public void a(p pVar) {
                    JSONArray optJSONArray;
                    JSONObject b2 = pVar.b();
                    if (!(b2 == null || (optJSONArray = b2.optJSONArray("data")) == null)) {
                        atomicBoolean.set(true);
                        for (int i = 0; i < optJSONArray.length(); i++) {
                            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                            if (optJSONObject != null) {
                                String optString = optJSONObject.optString("permission");
                                String optString2 = optJSONObject.optString(NotificationCompat.CATEGORY_STATUS);
                                if (!u.a(optString) && !u.a(optString2)) {
                                    String lowerCase = optString2.toLowerCase(Locale.US);
                                    if (lowerCase.equals("granted")) {
                                        hashSet.add(optString);
                                    } else if (lowerCase.equals("declined")) {
                                        hashSet2.add(optString);
                                    } else {
                                        Log.w("AccessTokenManager", "Unexpected status: " + lowerCase);
                                    }
                                }
                            }
                        }
                    }
                }
            }), b(accessToken, new GraphRequest.b() {
                /* class com.facebook.b.AnonymousClass3 */

                @Override // com.facebook.GraphRequest.b
                public void a(p pVar) {
                    JSONObject b2 = pVar.b();
                    if (b2 != null) {
                        aVar2.f1090a = b2.optString("access_token");
                        aVar2.b = b2.optInt("expires_at");
                        aVar2.c = Long.valueOf(b2.optLong("data_access_expiration_time"));
                    }
                }
            }));
            oVar.a(new o.a() {
                /* class com.facebook.b.AnonymousClass4 */

                @Override // com.facebook.o.a
                public void a(o oVar) {
                    AccessToken accessToken;
                    Throwable th;
                    String d2;
                    Set<String> g2;
                    Set<String> h;
                    Date e2;
                    Date f2;
                    try {
                        if (b.a().b() == null || b.a().b().l() != accessToken.l()) {
                            if (aVar != null) {
                                aVar.a(new h("No current access token to refresh"));
                            }
                            b.this.e.set(false);
                            if (aVar != null && 0 != 0) {
                                aVar.a((AccessToken) null);
                            }
                        } else if (!atomicBoolean.get() && aVar2.f1090a == null && aVar2.b == 0) {
                            if (aVar != null) {
                                aVar.a(new h("Failed to refresh access token"));
                            }
                            b.this.e.set(false);
                            if (aVar != null && 0 != 0) {
                                aVar.a((AccessToken) null);
                            }
                        } else {
                            if (aVar2.f1090a != null) {
                                d2 = aVar2.f1090a;
                            } else {
                                d2 = accessToken.d();
                            }
                            String k = accessToken.k();
                            String l = accessToken.l();
                            if (atomicBoolean.get()) {
                                g2 = hashSet;
                            } else {
                                g2 = accessToken.g();
                            }
                            if (atomicBoolean.get()) {
                                h = hashSet2;
                            } else {
                                h = accessToken.h();
                            }
                            c i = accessToken.i();
                            if (aVar2.b != 0) {
                                e2 = new Date(((long) aVar2.b) * 1000);
                            } else {
                                e2 = accessToken.e();
                            }
                            Date date = new Date();
                            if (aVar2.c != null) {
                                f2 = new Date(aVar2.c.longValue() * 1000);
                            } else {
                                f2 = accessToken.f();
                            }
                            AccessToken accessToken2 = new AccessToken(d2, k, l, g2, h, i, e2, date, f2);
                            try {
                                b.a().a(accessToken2);
                                b.this.e.set(false);
                                if (aVar != null && accessToken2 != null) {
                                    aVar.a(accessToken2);
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                accessToken = accessToken2;
                                b.this.e.set(false);
                                if (!(aVar == null || accessToken == null)) {
                                    aVar.a(accessToken);
                                }
                                throw th;
                            }
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        accessToken = null;
                        b.this.e.set(false);
                        aVar.a(accessToken);
                        throw th;
                    }
                }
            });
            oVar.h();
        } else if (aVar != null) {
            aVar.a(new h("Refresh already in progress"));
        }
    }
}
