package com.google.android.exoplayer.util.extensions;

import com.google.android.exoplayer.util.Assertions;
import com.google.android.exoplayer.util.extensions.InputBuffer;
import com.google.android.exoplayer.util.extensions.OutputBuffer;
import java.lang.Exception;
import java.util.LinkedList;

public abstract class SimpleDecoder<I extends InputBuffer, O extends OutputBuffer, E extends Exception> extends Thread implements Decoder<I, O, E> {
    private int availableInputBufferCount;
    private final I[] availableInputBuffers;
    private int availableOutputBufferCount;
    private final O[] availableOutputBuffers;
    private I dequeuedInputBuffer;
    private E exception;
    private boolean flushed;
    private final Object lock = new Object();
    private final LinkedList<I> queuedInputBuffers = new LinkedList<>();
    private final LinkedList<O> queuedOutputBuffers = new LinkedList<>();
    private boolean released;

    public interface EventListener<E> {
        void onDecoderError(E e);
    }

    /* access modifiers changed from: protected */
    public abstract I createInputBuffer();

    /* access modifiers changed from: protected */
    public abstract O createOutputBuffer();

    /* access modifiers changed from: protected */
    public abstract E decode(I i, O o, boolean z);

    protected SimpleDecoder(I[] iArr, O[] oArr) {
        this.availableInputBuffers = iArr;
        this.availableInputBufferCount = iArr.length;
        for (int i = 0; i < this.availableInputBufferCount; i++) {
            this.availableInputBuffers[i] = createInputBuffer();
        }
        this.availableOutputBuffers = oArr;
        this.availableOutputBufferCount = oArr.length;
        for (int i2 = 0; i2 < this.availableOutputBufferCount; i2++) {
            this.availableOutputBuffers[i2] = createOutputBuffer();
        }
    }

    /* access modifiers changed from: protected */
    public final void setInitialInputBufferSize(int i) {
        Assertions.checkState(this.availableInputBufferCount == this.availableInputBuffers.length);
        for (int i2 = 0; i2 < this.availableInputBuffers.length; i2++) {
            this.availableInputBuffers[i2].sampleHolder.ensureSpaceForWrite(i);
        }
    }

    @Override // com.google.android.exoplayer.util.extensions.Decoder
    public final I dequeueInputBuffer() {
        I i;
        synchronized (this.lock) {
            maybeThrowException();
            Assertions.checkState(this.dequeuedInputBuffer == null);
            if (this.availableInputBufferCount == 0) {
                i = null;
            } else {
                I[] iArr = this.availableInputBuffers;
                int i2 = this.availableInputBufferCount - 1;
                this.availableInputBufferCount = i2;
                i = iArr[i2];
                i.reset();
                this.dequeuedInputBuffer = i;
            }
        }
        return i;
    }

    public final void queueInputBuffer(I i) {
        synchronized (this.lock) {
            maybeThrowException();
            Assertions.checkArgument(i == this.dequeuedInputBuffer);
            this.queuedInputBuffers.addLast(i);
            maybeNotifyDecodeLoop();
            this.dequeuedInputBuffer = null;
        }
    }

    @Override // com.google.android.exoplayer.util.extensions.Decoder
    public final O dequeueOutputBuffer() {
        O removeFirst;
        synchronized (this.lock) {
            maybeThrowException();
            if (this.queuedOutputBuffers.isEmpty()) {
                removeFirst = null;
            } else {
                removeFirst = this.queuedOutputBuffers.removeFirst();
            }
        }
        return removeFirst;
    }

    /* access modifiers changed from: protected */
    public void releaseOutputBuffer(O o) {
        synchronized (this.lock) {
            O[] oArr = this.availableOutputBuffers;
            int i = this.availableOutputBufferCount;
            this.availableOutputBufferCount = i + 1;
            oArr[i] = o;
            maybeNotifyDecodeLoop();
        }
    }

    @Override // com.google.android.exoplayer.util.extensions.Decoder
    public final void flush() {
        synchronized (this.lock) {
            this.flushed = true;
            if (this.dequeuedInputBuffer != null) {
                I[] iArr = this.availableInputBuffers;
                int i = this.availableInputBufferCount;
                this.availableInputBufferCount = i + 1;
                iArr[i] = this.dequeuedInputBuffer;
                this.dequeuedInputBuffer = null;
            }
            while (!this.queuedInputBuffers.isEmpty()) {
                int i2 = this.availableInputBufferCount;
                this.availableInputBufferCount = i2 + 1;
                this.availableInputBuffers[i2] = this.queuedInputBuffers.removeFirst();
            }
            while (!this.queuedOutputBuffers.isEmpty()) {
                int i3 = this.availableOutputBufferCount;
                this.availableOutputBufferCount = i3 + 1;
                this.availableOutputBuffers[i3] = this.queuedOutputBuffers.removeFirst();
            }
        }
    }

    @Override // com.google.android.exoplayer.util.extensions.Decoder
    public void release() {
        synchronized (this.lock) {
            this.released = true;
            this.lock.notify();
        }
        try {
            join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void maybeThrowException() {
        if (this.exception != null) {
            throw this.exception;
        }
    }

    private void maybeNotifyDecodeLoop() {
        if (canDecodeBuffer()) {
            this.lock.notify();
        }
    }

    public final void run() {
        do {
            try {
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        } while (decode());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        r4.reset();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003f, code lost:
        if (r0.getFlag(1) == false) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        r4.setFlag(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0044, code lost:
        r1 = r8.lock;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0046, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0049, code lost:
        if (r8.flushed != false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0050, code lost:
        if (r4.getFlag(2) == false) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0052, code lost:
        r3 = r8.availableOutputBuffers;
        r5 = r8.availableOutputBufferCount;
        r8.availableOutputBufferCount = r5 + 1;
        r3[r5] = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005c, code lost:
        r3 = r8.availableInputBuffers;
        r4 = r8.availableInputBufferCount;
        r8.availableInputBufferCount = r4 + 1;
        r3[r4] = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0066, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006d, code lost:
        if (r0.getFlag(2) == false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006f, code lost:
        r4.setFlag(2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0072, code lost:
        r8.exception = decode(r0, r4, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007a, code lost:
        if (r8.exception == null) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x007c, code lost:
        r2 = r8.lock;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x007e, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0085, code lost:
        r8.queuedOutputBuffers.addLast(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean decode() {
        /*
        // Method dump skipped, instructions count: 142
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer.util.extensions.SimpleDecoder.decode():boolean");
    }

    private boolean canDecodeBuffer() {
        return !this.queuedInputBuffers.isEmpty() && this.availableOutputBufferCount > 0;
    }
}
