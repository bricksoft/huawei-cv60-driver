package com.google.android.exoplayer.util;

import com.google.android.exoplayer.C;
import com.google.android.exoplayer.extractor.SeekMap;

public final class FlacSeekTable {
    private static final int METADATA_LENGTH_OFFSET = 1;
    private static final int SEEK_POINT_SIZE = 18;
    private final long[] offsets;
    private final long[] sampleNumbers;

    public static FlacSeekTable parseSeekTable(ParsableByteArray parsableByteArray) {
        parsableByteArray.skipBytes(1);
        int readUnsignedInt24 = parsableByteArray.readUnsignedInt24() / 18;
        long[] jArr = new long[readUnsignedInt24];
        long[] jArr2 = new long[readUnsignedInt24];
        for (int i = 0; i < readUnsignedInt24; i++) {
            jArr[i] = parsableByteArray.readLong();
            jArr2[i] = parsableByteArray.readLong();
            parsableByteArray.skipBytes(2);
        }
        return new FlacSeekTable(jArr, jArr2);
    }

    private FlacSeekTable(long[] jArr, long[] jArr2) {
        this.sampleNumbers = jArr;
        this.offsets = jArr2;
    }

    public SeekMap createSeekMap(final long j, final long j2) {
        return new SeekMap() {
            /* class com.google.android.exoplayer.util.FlacSeekTable.AnonymousClass1 */

            @Override // com.google.android.exoplayer.extractor.SeekMap
            public boolean isSeekable() {
                return true;
            }

            @Override // com.google.android.exoplayer.extractor.SeekMap
            public long getPosition(long j) {
                int binarySearchFloor = Util.binarySearchFloor(FlacSeekTable.this.sampleNumbers, (j2 * j) / C.MICROS_PER_SECOND, true, true);
                return FlacSeekTable.this.offsets[binarySearchFloor] + j;
            }
        };
    }
}
