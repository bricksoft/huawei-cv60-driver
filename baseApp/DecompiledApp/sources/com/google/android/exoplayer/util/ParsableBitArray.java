package com.google.android.exoplayer.util;

public final class ParsableBitArray {
    private int bitOffset;
    private int byteLimit;
    private int byteOffset;
    public byte[] data;

    public ParsableBitArray() {
    }

    public ParsableBitArray(byte[] bArr) {
        this(bArr, bArr.length);
    }

    public ParsableBitArray(byte[] bArr, int i) {
        this.data = bArr;
        this.byteLimit = i;
    }

    public void reset(byte[] bArr) {
        reset(bArr, bArr.length);
    }

    public void reset(byte[] bArr, int i) {
        this.data = bArr;
        this.byteOffset = 0;
        this.bitOffset = 0;
        this.byteLimit = i;
    }

    public int bitsLeft() {
        return ((this.byteLimit - this.byteOffset) * 8) - this.bitOffset;
    }

    public int getPosition() {
        return (this.byteOffset * 8) + this.bitOffset;
    }

    public void setPosition(int i) {
        this.byteOffset = i / 8;
        this.bitOffset = i - (this.byteOffset * 8);
        assertValidOffset();
    }

    public void skipBits(int i) {
        this.byteOffset += i / 8;
        this.bitOffset += i % 8;
        if (this.bitOffset > 7) {
            this.byteOffset++;
            this.bitOffset -= 8;
        }
        assertValidOffset();
    }

    public boolean readBit() {
        return readBits(1) == 1;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:22:0x002b */
    /* JADX DEBUG: Multi-variable search result rejected for r0v13, resolved type: byte[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v14, resolved type: byte */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v15 */
    public int readBits(int i) {
        byte b;
        int i2 = 0;
        if (i != 0) {
            int i3 = i / 8;
            int i4 = 0;
            for (int i5 = 0; i5 < i3; i5++) {
                if (this.bitOffset != 0) {
                    b = ((this.data[this.byteOffset] & 255) << this.bitOffset) | ((this.data[this.byteOffset + 1] & 255) >>> (8 - this.bitOffset));
                } else {
                    b = this.data[this.byteOffset];
                }
                i -= 8;
                i4 |= ((b == true ? 1 : 0) & 255) << i;
                this.byteOffset++;
            }
            if (i > 0) {
                int i6 = this.bitOffset + i;
                byte b2 = (byte) (255 >> (8 - i));
                if (i6 > 8) {
                    i2 = (b2 & (((this.data[this.byteOffset] & 255) << (i6 - 8)) | ((this.data[this.byteOffset + 1] & 255) >> (16 - i6)))) | i4;
                    this.byteOffset++;
                } else {
                    i2 = (b2 & ((this.data[this.byteOffset] & 255) >> (8 - i6))) | i4;
                    if (i6 == 8) {
                        this.byteOffset++;
                    }
                }
                this.bitOffset = i6 % 8;
            } else {
                i2 = i4;
            }
            assertValidOffset();
        }
        return i2;
    }

    public boolean canReadExpGolombCodedNum() {
        boolean z;
        int i = this.byteOffset;
        int i2 = this.bitOffset;
        int i3 = 0;
        while (this.byteOffset < this.byteLimit && !readBit()) {
            i3++;
        }
        if (this.byteOffset == this.byteLimit) {
            z = true;
        } else {
            z = false;
        }
        this.byteOffset = i;
        this.bitOffset = i2;
        return !z && bitsLeft() >= (i3 * 2) + 1;
    }

    public int readUnsignedExpGolombCodedInt() {
        return readExpGolombCodeNum();
    }

    public int readSignedExpGolombCodedInt() {
        int readExpGolombCodeNum = readExpGolombCodeNum();
        return (readExpGolombCodeNum % 2 == 0 ? -1 : 1) * ((readExpGolombCodeNum + 1) / 2);
    }

    private int readExpGolombCodeNum() {
        int i = 0;
        int i2 = 0;
        while (!readBit()) {
            i2++;
        }
        int i3 = (1 << i2) - 1;
        if (i2 > 0) {
            i = readBits(i2);
        }
        return i3 + i;
    }

    private void assertValidOffset() {
        Assertions.checkState(this.byteOffset >= 0 && this.bitOffset >= 0 && this.bitOffset < 8 && (this.byteOffset < this.byteLimit || (this.byteOffset == this.byteLimit && this.bitOffset == 0)));
    }
}
