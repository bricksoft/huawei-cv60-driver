package com.google.android.exoplayer;

import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.util.Assertions;

public abstract class TrackRenderer implements ExoPlayer.ExoPlayerComponent {
    public static final long END_OF_TRACK_US = -3;
    public static final long MATCH_LONGEST_US = -2;
    protected static final int STATE_ENABLED = 2;
    protected static final int STATE_PREPARED = 1;
    protected static final int STATE_RELEASED = -1;
    protected static final int STATE_STARTED = 3;
    protected static final int STATE_UNPREPARED = 0;
    public static final long UNKNOWN_TIME_US = -1;
    private int state;

    /* access modifiers changed from: protected */
    public abstract boolean doPrepare(long j);

    /* access modifiers changed from: protected */
    public abstract void doSomeWork(long j, long j2);

    /* access modifiers changed from: protected */
    public abstract long getBufferedPositionUs();

    /* access modifiers changed from: protected */
    public abstract long getDurationUs();

    /* access modifiers changed from: protected */
    public abstract MediaFormat getFormat(int i);

    /* access modifiers changed from: protected */
    public abstract int getTrackCount();

    /* access modifiers changed from: protected */
    public abstract boolean isEnded();

    /* access modifiers changed from: protected */
    public abstract boolean isReady();

    /* access modifiers changed from: protected */
    public abstract void maybeThrowError();

    /* access modifiers changed from: protected */
    public abstract void seekTo(long j);

    /* access modifiers changed from: protected */
    public MediaClock getMediaClock() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final int getState() {
        return this.state;
    }

    /* access modifiers changed from: package-private */
    public final int prepare(long j) {
        boolean z;
        int i = 1;
        if (this.state == 0) {
            z = true;
        } else {
            z = false;
        }
        Assertions.checkState(z);
        if (!doPrepare(j)) {
            i = 0;
        }
        this.state = i;
        return this.state;
    }

    /* access modifiers changed from: package-private */
    public final void enable(int i, long j, boolean z) {
        boolean z2 = true;
        if (this.state != 1) {
            z2 = false;
        }
        Assertions.checkState(z2);
        this.state = 2;
        onEnabled(i, j, z);
    }

    /* access modifiers changed from: protected */
    public void onEnabled(int i, long j, boolean z) {
    }

    /* access modifiers changed from: package-private */
    public final void start() {
        Assertions.checkState(this.state == 2);
        this.state = 3;
        onStarted();
    }

    /* access modifiers changed from: protected */
    public void onStarted() {
    }

    /* access modifiers changed from: package-private */
    public final void stop() {
        Assertions.checkState(this.state == 3);
        this.state = 2;
        onStopped();
    }

    /* access modifiers changed from: protected */
    public void onStopped() {
    }

    /* access modifiers changed from: package-private */
    public final void disable() {
        Assertions.checkState(this.state == 2);
        this.state = 1;
        onDisabled();
    }

    /* access modifiers changed from: protected */
    public void onDisabled() {
    }

    /* access modifiers changed from: package-private */
    public final void release() {
        Assertions.checkState((this.state == 2 || this.state == 3 || this.state == -1) ? false : true);
        this.state = -1;
        onReleased();
    }

    /* access modifiers changed from: protected */
    public void onReleased() {
    }

    @Override // com.google.android.exoplayer.ExoPlayer.ExoPlayerComponent
    public void handleMessage(int i, Object obj) {
    }
}
