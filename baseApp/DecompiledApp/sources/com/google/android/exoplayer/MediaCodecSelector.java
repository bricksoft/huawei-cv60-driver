package com.google.android.exoplayer;

public interface MediaCodecSelector {
    public static final MediaCodecSelector DEFAULT = new MediaCodecSelector() {
        /* class com.google.android.exoplayer.MediaCodecSelector.AnonymousClass1 */

        @Override // com.google.android.exoplayer.MediaCodecSelector
        public DecoderInfo getDecoderInfo(String str, boolean z) {
            return MediaCodecUtil.getDecoderInfo(str, z);
        }

        @Override // com.google.android.exoplayer.MediaCodecSelector
        public DecoderInfo getPassthroughDecoderInfo() {
            return MediaCodecUtil.getPassthroughDecoderInfo();
        }
    };

    DecoderInfo getDecoderInfo(String str, boolean z);

    DecoderInfo getPassthroughDecoderInfo();
}
