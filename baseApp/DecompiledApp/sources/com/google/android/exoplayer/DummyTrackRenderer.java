package com.google.android.exoplayer;

public final class DummyTrackRenderer extends TrackRenderer {
    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public boolean doPrepare(long j) {
        return true;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public int getTrackCount() {
        return 0;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public MediaFormat getFormat(int i) {
        throw new IllegalStateException();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public boolean isEnded() {
        throw new IllegalStateException();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public boolean isReady() {
        throw new IllegalStateException();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public void seekTo(long j) {
        throw new IllegalStateException();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public void doSomeWork(long j, long j2) {
        throw new IllegalStateException();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public void maybeThrowError() {
        throw new IllegalStateException();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public long getDurationUs() {
        throw new IllegalStateException();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public long getBufferedPositionUs() {
        throw new IllegalStateException();
    }
}
