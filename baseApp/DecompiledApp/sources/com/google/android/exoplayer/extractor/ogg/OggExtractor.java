package com.google.android.exoplayer.extractor.ogg;

import com.google.android.exoplayer.extractor.Extractor;
import com.google.android.exoplayer.extractor.ExtractorInput;
import com.google.android.exoplayer.extractor.ExtractorOutput;
import com.google.android.exoplayer.extractor.PositionHolder;
import com.google.android.exoplayer.extractor.TrackOutput;

public class OggExtractor implements Extractor {
    private StreamReader streamReader;

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    @Override // com.google.android.exoplayer.extractor.Extractor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean sniff(com.google.android.exoplayer.extractor.ExtractorInput r8) {
        /*
            r7 = this;
            r6 = 7
            r1 = 1
            r0 = 0
            com.google.android.exoplayer.util.ParsableByteArray r2 = new com.google.android.exoplayer.util.ParsableByteArray     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r3 = 27
            byte[] r3 = new byte[r3]     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r4 = 0
            r2.<init>(r3, r4)     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            com.google.android.exoplayer.extractor.ogg.OggUtil$PageHeader r3 = new com.google.android.exoplayer.extractor.ogg.OggUtil$PageHeader     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r3.<init>()     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r4 = 1
            boolean r4 = com.google.android.exoplayer.extractor.ogg.OggUtil.populatePageHeader(r8, r3, r2, r4)     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            if (r4 == 0) goto L_0x0024
            int r4 = r3.type     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r4 = r4 & 2
            r5 = 2
            if (r4 != r5) goto L_0x0024
            int r3 = r3.bodySize     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            if (r3 >= r6) goto L_0x0025
        L_0x0024:
            return r0
        L_0x0025:
            r2.reset()     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            byte[] r3 = r2.data     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r4 = 0
            r5 = 7
            r8.peekFully(r3, r4, r5)     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            boolean r3 = com.google.android.exoplayer.extractor.ogg.FlacReader.verifyBitstreamType(r2)     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            if (r3 == 0) goto L_0x003e
            com.google.android.exoplayer.extractor.ogg.FlacReader r2 = new com.google.android.exoplayer.extractor.ogg.FlacReader     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r2.<init>()     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r7.streamReader = r2     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
        L_0x003c:
            r0 = r1
            goto L_0x0024
        L_0x003e:
            r2.reset()     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            boolean r2 = com.google.android.exoplayer.extractor.ogg.VorbisReader.verifyBitstreamType(r2)     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            if (r2 == 0) goto L_0x0024
            com.google.android.exoplayer.extractor.ogg.VorbisReader r2 = new com.google.android.exoplayer.extractor.ogg.VorbisReader     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r2.<init>()     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            r7.streamReader = r2     // Catch:{ ParserException -> 0x004f, all -> 0x0051 }
            goto L_0x003c
        L_0x004f:
            r1 = move-exception
            goto L_0x0024
        L_0x0051:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer.extractor.ogg.OggExtractor.sniff(com.google.android.exoplayer.extractor.ExtractorInput):boolean");
    }

    @Override // com.google.android.exoplayer.extractor.Extractor
    public void init(ExtractorOutput extractorOutput) {
        TrackOutput track = extractorOutput.track(0);
        extractorOutput.endTracks();
        this.streamReader.init(extractorOutput, track);
    }

    @Override // com.google.android.exoplayer.extractor.Extractor
    public void seek() {
        this.streamReader.seek();
    }

    @Override // com.google.android.exoplayer.extractor.Extractor
    public void release() {
    }

    @Override // com.google.android.exoplayer.extractor.Extractor
    public int read(ExtractorInput extractorInput, PositionHolder positionHolder) {
        return this.streamReader.read(extractorInput, positionHolder);
    }
}
