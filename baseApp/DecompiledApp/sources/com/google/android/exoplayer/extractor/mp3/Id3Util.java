package com.google.android.exoplayer.extractor.mp3;

import android.util.Pair;
import com.google.android.exoplayer.C;
import com.google.android.exoplayer.extractor.ExtractorInput;
import com.google.android.exoplayer.extractor.GaplessInfo;
import com.google.android.exoplayer.extractor.ts.PsExtractor;
import com.google.android.exoplayer.util.ParsableByteArray;
import com.google.android.exoplayer.util.Util;
import java.nio.charset.Charset;

/* access modifiers changed from: package-private */
public final class Id3Util {
    private static final Charset[] CHARSET_BY_ENCODING = {Charset.forName("ISO-8859-1"), Charset.forName("UTF-16LE"), Charset.forName("UTF-16BE"), Charset.forName(C.UTF8_NAME)};
    private static final int ID3_TAG = Util.getIntegerCodeForString("ID3");
    private static final int MAXIMUM_METADATA_SIZE = 3145728;

    public static GaplessInfo parseId3(ExtractorInput extractorInput) {
        ParsableByteArray parsableByteArray = new ParsableByteArray(10);
        GaplessInfo gaplessInfo = null;
        int i = 0;
        while (true) {
            extractorInput.peekFully(parsableByteArray.data, 0, 10);
            parsableByteArray.setPosition(0);
            if (parsableByteArray.readUnsignedInt24() != ID3_TAG) {
                extractorInput.resetPeekPosition();
                extractorInput.advancePeekPosition(i);
                return gaplessInfo;
            }
            int readUnsignedByte = parsableByteArray.readUnsignedByte();
            int readUnsignedByte2 = parsableByteArray.readUnsignedByte();
            int readUnsignedByte3 = parsableByteArray.readUnsignedByte();
            int readSynchSafeInt = parsableByteArray.readSynchSafeInt();
            if (gaplessInfo != null || !canParseMetadata(readUnsignedByte, readUnsignedByte2, readUnsignedByte3, readSynchSafeInt)) {
                extractorInput.advancePeekPosition(readSynchSafeInt);
            } else {
                byte[] bArr = new byte[readSynchSafeInt];
                extractorInput.peekFully(bArr, 0, readSynchSafeInt);
                gaplessInfo = parseGaplessInfo(new ParsableByteArray(bArr), readUnsignedByte, readUnsignedByte3);
            }
            i += readSynchSafeInt + 10;
        }
    }

    private static boolean canParseMetadata(int i, int i2, int i3, int i4) {
        return i2 != 255 && i >= 2 && i <= 4 && i4 <= MAXIMUM_METADATA_SIZE && (i != 2 || ((i3 & 63) == 0 && (i3 & 64) == 0)) && ((i != 3 || (i3 & 31) == 0) && (i != 4 || (i3 & 15) == 0));
    }

    private static GaplessInfo parseGaplessInfo(ParsableByteArray parsableByteArray, int i, int i2) {
        GaplessInfo createFromComment;
        unescape(parsableByteArray, i, i2);
        parsableByteArray.setPosition(0);
        if (i != 3 || (i2 & 64) == 0) {
            if (i == 4 && (i2 & 64) != 0) {
                if (parsableByteArray.bytesLeft() < 4) {
                    return null;
                }
                int readSynchSafeInt = parsableByteArray.readSynchSafeInt();
                if (readSynchSafeInt < 6 || readSynchSafeInt > parsableByteArray.bytesLeft() + 4) {
                    return null;
                }
                parsableByteArray.setPosition(readSynchSafeInt);
            }
        } else if (parsableByteArray.bytesLeft() < 4) {
            return null;
        } else {
            int readUnsignedIntToInt = parsableByteArray.readUnsignedIntToInt();
            if (readUnsignedIntToInt > parsableByteArray.bytesLeft()) {
                return null;
            }
            if (readUnsignedIntToInt >= 6) {
                parsableByteArray.skipBytes(2);
                int readUnsignedIntToInt2 = parsableByteArray.readUnsignedIntToInt();
                parsableByteArray.setPosition(4);
                parsableByteArray.setLimit(parsableByteArray.limit() - readUnsignedIntToInt2);
                if (parsableByteArray.bytesLeft() < readUnsignedIntToInt) {
                    return null;
                }
            }
            parsableByteArray.skipBytes(readUnsignedIntToInt);
        }
        while (true) {
            Pair<String, String> findNextComment = findNextComment(i, parsableByteArray);
            if (findNextComment == null) {
                return null;
            }
            if (((String) findNextComment.first).length() > 3 && (createFromComment = GaplessInfo.createFromComment(((String) findNextComment.first).substring(3), (String) findNextComment.second)) != null) {
                return createFromComment;
            }
        }
    }

    private static Pair<String, String> findNextComment(int i, ParsableByteArray parsableByteArray) {
        int i2;
        boolean z;
        Pair<String, String> pair;
        while (true) {
            if (i == 2) {
                if (parsableByteArray.bytesLeft() < 6) {
                    return null;
                }
                String readString = parsableByteArray.readString(3, Charset.forName("US-ASCII"));
                if (readString.equals("\u0000\u0000\u0000") || (i2 = parsableByteArray.readUnsignedInt24()) == 0 || i2 > parsableByteArray.bytesLeft()) {
                    return null;
                }
                if (readString.equals("COM")) {
                    break;
                }
                parsableByteArray.skipBytes(i2);
            } else if (parsableByteArray.bytesLeft() < 10) {
                return null;
            } else {
                String readString2 = parsableByteArray.readString(4, Charset.forName("US-ASCII"));
                if (readString2.equals("\u0000\u0000\u0000\u0000")) {
                    return null;
                }
                i2 = i == 4 ? parsableByteArray.readSynchSafeInt() : parsableByteArray.readUnsignedIntToInt();
                if (i2 == 0 || i2 > parsableByteArray.bytesLeft() - 2) {
                    return null;
                }
                int readUnsignedShort = parsableByteArray.readUnsignedShort();
                if ((i != 4 || (readUnsignedShort & 12) == 0) && (i != 3 || (readUnsignedShort & PsExtractor.AUDIO_STREAM) == 0)) {
                    z = false;
                } else {
                    z = true;
                }
                if (!z && readString2.equals("COMM")) {
                    break;
                }
                parsableByteArray.skipBytes(i2);
            }
        }
        int readUnsignedByte = parsableByteArray.readUnsignedByte();
        if (readUnsignedByte < 0 || readUnsignedByte >= CHARSET_BY_ENCODING.length) {
            return null;
        }
        String[] split = parsableByteArray.readString(i2 - 1, CHARSET_BY_ENCODING[readUnsignedByte]).split("\u0000");
        if (split.length == 2) {
            pair = Pair.create(split[0], split[1]);
        } else {
            pair = null;
        }
        return pair;
    }

    private static boolean unescape(ParsableByteArray parsableByteArray, int i, int i2) {
        if (i != 4) {
            if ((i2 & 128) != 0) {
                byte[] bArr = parsableByteArray.data;
                int length = bArr.length;
                for (int i3 = 0; i3 + 1 < length; i3++) {
                    if ((bArr[i3] & 255) == 255 && bArr[i3 + 1] == 0) {
                        System.arraycopy(bArr, i3 + 2, bArr, i3 + 1, (length - i3) - 2);
                        length--;
                    }
                }
                parsableByteArray.setLimit(length);
            }
        } else if (canUnescapeVersion4(parsableByteArray, false)) {
            unescapeVersion4(parsableByteArray, false);
        } else if (!canUnescapeVersion4(parsableByteArray, true)) {
            return false;
        } else {
            unescapeVersion4(parsableByteArray, true);
        }
        return true;
    }

    private static boolean canUnescapeVersion4(ParsableByteArray parsableByteArray, boolean z) {
        parsableByteArray.setPosition(0);
        while (parsableByteArray.bytesLeft() >= 10) {
            if (parsableByteArray.readInt() == 0) {
                return true;
            }
            long readUnsignedInt = parsableByteArray.readUnsignedInt();
            if (!z) {
                if ((8421504 & readUnsignedInt) != 0) {
                    return false;
                }
                readUnsignedInt = (((readUnsignedInt >> 24) & 127) << 21) | (readUnsignedInt & 127) | (((readUnsignedInt >> 8) & 127) << 7) | (((readUnsignedInt >> 16) & 127) << 14);
            }
            if (readUnsignedInt > ((long) (parsableByteArray.bytesLeft() - 2))) {
                return false;
            }
            if ((parsableByteArray.readUnsignedShort() & 1) != 0 && parsableByteArray.bytesLeft() < 4) {
                return false;
            }
            parsableByteArray.skipBytes((int) readUnsignedInt);
        }
        return true;
    }

    private static void unescapeVersion4(ParsableByteArray parsableByteArray, boolean z) {
        int i;
        int i2;
        int i3;
        parsableByteArray.setPosition(0);
        byte[] bArr = parsableByteArray.data;
        while (parsableByteArray.bytesLeft() >= 10 && parsableByteArray.readInt() != 0) {
            int readUnsignedIntToInt = z ? parsableByteArray.readUnsignedIntToInt() : parsableByteArray.readSynchSafeInt();
            int readUnsignedShort = parsableByteArray.readUnsignedShort();
            if ((readUnsignedShort & 1) != 0) {
                int position = parsableByteArray.getPosition();
                System.arraycopy(bArr, position + 4, bArr, position, parsableByteArray.bytesLeft() - 4);
                i2 = readUnsignedIntToInt - 4;
                i = readUnsignedShort & -2;
                parsableByteArray.setLimit(parsableByteArray.limit() - 4);
            } else {
                i = readUnsignedShort;
                i2 = readUnsignedIntToInt;
            }
            if ((i & 2) != 0) {
                int position2 = parsableByteArray.getPosition() + 1;
                int i4 = 0;
                int i5 = position2;
                int i6 = position2;
                while (i4 + 1 < i2) {
                    if ((bArr[i6 - 1] & 255) == 255 && bArr[i6] == 0) {
                        i6++;
                        i2--;
                    }
                    bArr[i5] = bArr[i6];
                    i4++;
                    i5++;
                    i6++;
                }
                parsableByteArray.setLimit(parsableByteArray.limit() - (i6 - i5));
                System.arraycopy(bArr, i6, bArr, i5, parsableByteArray.bytesLeft() - i6);
                i3 = i & -3;
            } else {
                i3 = i;
            }
            if (i3 != readUnsignedShort || z) {
                int position3 = parsableByteArray.getPosition() - 6;
                writeSyncSafeInteger(bArr, position3, i2);
                bArr[position3 + 4] = (byte) (i3 >> 8);
                bArr[position3 + 5] = (byte) (i3 & 255);
            }
            parsableByteArray.skipBytes(i2);
        }
    }

    private static void writeSyncSafeInteger(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) ((i2 >> 21) & 127);
        bArr[i + 1] = (byte) ((i2 >> 14) & 127);
        bArr[i + 2] = (byte) ((i2 >> 7) & 127);
        bArr[i + 3] = (byte) (i2 & 127);
    }

    private Id3Util() {
    }
}
