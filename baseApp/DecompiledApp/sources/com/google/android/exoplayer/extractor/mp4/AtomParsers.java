package com.google.android.exoplayer.extractor.mp4;

import android.util.Pair;
import com.google.android.exoplayer.C;
import com.google.android.exoplayer.MediaFormat;
import com.google.android.exoplayer.ParserException;
import com.google.android.exoplayer.extractor.GaplessInfo;
import com.google.android.exoplayer.extractor.mp4.Atom;
import com.google.android.exoplayer.extractor.mp4.FixedSampleSizeRechunker;
import com.google.android.exoplayer.extractor.ts.PsExtractor;
import com.google.android.exoplayer.util.Ac3Util;
import com.google.android.exoplayer.util.Assertions;
import com.google.android.exoplayer.util.CodecSpecificDataUtil;
import com.google.android.exoplayer.util.MimeTypes;
import com.google.android.exoplayer.util.NalUnitUtil;
import com.google.android.exoplayer.util.ParsableBitArray;
import com.google.android.exoplayer.util.ParsableByteArray;
import com.google.android.exoplayer.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* access modifiers changed from: package-private */
public final class AtomParsers {

    private interface SampleSizeBox {
        int getSampleCount();

        boolean isFixedSampleSize();

        int readNextSampleSize();
    }

    public static Track parseTrak(Atom.ContainerAtom containerAtom, Atom.LeafAtom leafAtom, long j, boolean z) {
        long j2;
        long scaleLargeTimestamp;
        Atom.ContainerAtom containerAtomOfType = containerAtom.getContainerAtomOfType(Atom.TYPE_mdia);
        int parseHdlr = parseHdlr(containerAtomOfType.getLeafAtomOfType(Atom.TYPE_hdlr).data);
        if (parseHdlr != Track.TYPE_soun && parseHdlr != Track.TYPE_vide && parseHdlr != Track.TYPE_text && parseHdlr != Track.TYPE_sbtl && parseHdlr != Track.TYPE_subt) {
            return null;
        }
        TkhdData parseTkhd = parseTkhd(containerAtom.getLeafAtomOfType(Atom.TYPE_tkhd).data);
        if (j == -1) {
            j2 = parseTkhd.duration;
        } else {
            j2 = j;
        }
        long parseMvhd = parseMvhd(leafAtom.data);
        if (j2 == -1) {
            scaleLargeTimestamp = -1;
        } else {
            scaleLargeTimestamp = Util.scaleLargeTimestamp(j2, C.MICROS_PER_SECOND, parseMvhd);
        }
        Atom.ContainerAtom containerAtomOfType2 = containerAtomOfType.getContainerAtomOfType(Atom.TYPE_minf).getContainerAtomOfType(Atom.TYPE_stbl);
        Pair<Long, String> parseMdhd = parseMdhd(containerAtomOfType.getLeafAtomOfType(Atom.TYPE_mdhd).data);
        StsdData parseStsd = parseStsd(containerAtomOfType2.getLeafAtomOfType(Atom.TYPE_stsd).data, parseTkhd.id, scaleLargeTimestamp, parseTkhd.rotationDegrees, (String) parseMdhd.second, z);
        Pair<long[], long[]> parseEdts = parseEdts(containerAtom.getContainerAtomOfType(Atom.TYPE_edts));
        if (parseStsd.mediaFormat == null) {
            return null;
        }
        return new Track(parseTkhd.id, parseHdlr, ((Long) parseMdhd.first).longValue(), parseMvhd, scaleLargeTimestamp, parseStsd.mediaFormat, parseStsd.trackEncryptionBoxes, parseStsd.nalUnitLengthFieldLength, (long[]) parseEdts.first, (long[]) parseEdts.second);
    }

    public static TrackSampleTable parseStbl(Track track, Atom.ContainerAtom containerAtom) {
        SampleSizeBox stz2SampleSizeBox;
        long[] jArr;
        int[] iArr;
        int i;
        long[] jArr2;
        int[] iArr2;
        long[] jArr3;
        int[] iArr3;
        int[] iArr4;
        int i2;
        boolean z;
        boolean z2;
        int i3;
        int i4;
        int i5;
        Atom.LeafAtom leafAtomOfType = containerAtom.getLeafAtomOfType(Atom.TYPE_stsz);
        if (leafAtomOfType != null) {
            stz2SampleSizeBox = new StszSampleSizeBox(leafAtomOfType);
        } else {
            Atom.LeafAtom leafAtomOfType2 = containerAtom.getLeafAtomOfType(Atom.TYPE_stz2);
            if (leafAtomOfType2 == null) {
                throw new ParserException("Track has no sample table size information");
            }
            stz2SampleSizeBox = new Stz2SampleSizeBox(leafAtomOfType2);
        }
        int sampleCount = stz2SampleSizeBox.getSampleCount();
        if (sampleCount == 0) {
            return new TrackSampleTable(new long[0], new int[0], 0, new long[0], new int[0]);
        }
        boolean z3 = false;
        Atom.LeafAtom leafAtomOfType3 = containerAtom.getLeafAtomOfType(Atom.TYPE_stco);
        if (leafAtomOfType3 == null) {
            z3 = true;
            leafAtomOfType3 = containerAtom.getLeafAtomOfType(Atom.TYPE_co64);
        }
        ParsableByteArray parsableByteArray = leafAtomOfType3.data;
        ParsableByteArray parsableByteArray2 = containerAtom.getLeafAtomOfType(Atom.TYPE_stsc).data;
        ParsableByteArray parsableByteArray3 = containerAtom.getLeafAtomOfType(Atom.TYPE_stts).data;
        Atom.LeafAtom leafAtomOfType4 = containerAtom.getLeafAtomOfType(Atom.TYPE_stss);
        ParsableByteArray parsableByteArray4 = leafAtomOfType4 != null ? leafAtomOfType4.data : null;
        Atom.LeafAtom leafAtomOfType5 = containerAtom.getLeafAtomOfType(Atom.TYPE_ctts);
        ParsableByteArray parsableByteArray5 = leafAtomOfType5 != null ? leafAtomOfType5.data : null;
        ChunkIterator chunkIterator = new ChunkIterator(parsableByteArray2, parsableByteArray, z3);
        parsableByteArray3.setPosition(12);
        int readUnsignedIntToInt = parsableByteArray3.readUnsignedIntToInt() - 1;
        int readUnsignedIntToInt2 = parsableByteArray3.readUnsignedIntToInt();
        int readUnsignedIntToInt3 = parsableByteArray3.readUnsignedIntToInt();
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        if (parsableByteArray5 != null) {
            parsableByteArray5.setPosition(12);
            i7 = parsableByteArray5.readUnsignedIntToInt();
        }
        int i9 = -1;
        int i10 = 0;
        if (parsableByteArray4 != null) {
            parsableByteArray4.setPosition(12);
            i10 = parsableByteArray4.readUnsignedIntToInt();
            if (i10 > 0) {
                i9 = parsableByteArray4.readUnsignedIntToInt() - 1;
            } else {
                parsableByteArray4 = null;
            }
        }
        int i11 = 0;
        if (!(stz2SampleSizeBox.isFixedSampleSize() && MimeTypes.AUDIO_RAW.equals(track.mediaFormat.mimeType) && readUnsignedIntToInt == 0 && i7 == 0 && i10 == 0)) {
            long[] jArr4 = new long[sampleCount];
            int[] iArr5 = new int[sampleCount];
            long[] jArr5 = new long[sampleCount];
            int[] iArr6 = new int[sampleCount];
            long j = 0;
            long j2 = 0;
            int i12 = 0;
            int i13 = 0;
            int i14 = i10;
            int i15 = i9;
            while (i13 < sampleCount) {
                while (i12 == 0) {
                    Assertions.checkState(chunkIterator.moveNext());
                    j2 = chunkIterator.offset;
                    i12 = chunkIterator.numSamples;
                }
                if (parsableByteArray5 != null) {
                    i3 = i8;
                    i4 = i7;
                    while (i6 == 0 && i4 > 0) {
                        i6 = parsableByteArray5.readUnsignedIntToInt();
                        i3 = parsableByteArray5.readInt();
                        i4--;
                    }
                    i6--;
                } else {
                    i3 = i8;
                    i4 = i7;
                }
                jArr4[i13] = j2;
                iArr5[i13] = stz2SampleSizeBox.readNextSampleSize();
                if (iArr5[i13] > i11) {
                    i11 = iArr5[i13];
                }
                jArr5[i13] = ((long) i3) + j;
                iArr6[i13] = parsableByteArray4 == null ? 1 : 0;
                if (i13 == i15) {
                    iArr6[i13] = 1;
                    int i16 = i14 - 1;
                    if (i16 > 0) {
                        i14 = i16;
                        i15 = parsableByteArray4.readUnsignedIntToInt() - 1;
                    } else {
                        i14 = i16;
                    }
                }
                j += (long) readUnsignedIntToInt3;
                int i17 = readUnsignedIntToInt2 - 1;
                if (i17 != 0 || readUnsignedIntToInt <= 0) {
                    i5 = i17;
                } else {
                    int readUnsignedIntToInt4 = parsableByteArray3.readUnsignedIntToInt();
                    readUnsignedIntToInt--;
                    readUnsignedIntToInt3 = parsableByteArray3.readUnsignedIntToInt();
                    i5 = readUnsignedIntToInt4;
                }
                j2 += (long) iArr5[i13];
                i12--;
                i13++;
                i8 = i3;
                i7 = i4;
                readUnsignedIntToInt2 = i5;
            }
            Assertions.checkArgument(i6 == 0);
            while (i7 > 0) {
                Assertions.checkArgument(parsableByteArray5.readUnsignedIntToInt() == 0);
                parsableByteArray5.readInt();
                i7--;
            }
            Assertions.checkArgument(i14 == 0);
            Assertions.checkArgument(readUnsignedIntToInt2 == 0);
            Assertions.checkArgument(i12 == 0);
            if (readUnsignedIntToInt == 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            Assertions.checkArgument(z2);
            iArr2 = iArr6;
            jArr2 = jArr5;
            i = i11;
            iArr = iArr5;
            jArr = jArr4;
        } else {
            long[] jArr6 = new long[chunkIterator.length];
            int[] iArr7 = new int[chunkIterator.length];
            while (chunkIterator.moveNext()) {
                jArr6[chunkIterator.index] = chunkIterator.offset;
                iArr7[chunkIterator.index] = chunkIterator.numSamples;
            }
            FixedSampleSizeRechunker.Results rechunk = FixedSampleSizeRechunker.rechunk(stz2SampleSizeBox.readNextSampleSize(), jArr6, iArr7, (long) readUnsignedIntToInt3);
            jArr = rechunk.offsets;
            iArr = rechunk.sizes;
            i = rechunk.maximumSize;
            jArr2 = rechunk.timestamps;
            iArr2 = rechunk.flags;
        }
        if (track.editListDurations == null) {
            Util.scaleLargeTimestampsInPlace(jArr2, C.MICROS_PER_SECOND, track.timescale);
            return new TrackSampleTable(jArr, iArr, i, jArr2, iArr2);
        } else if (track.editListDurations.length == 1 && track.editListDurations[0] == 0) {
            for (int i18 = 0; i18 < jArr2.length; i18++) {
                jArr2[i18] = Util.scaleLargeTimestamp(jArr2[i18] - track.editListMediaTimes[0], C.MICROS_PER_SECOND, track.timescale);
            }
            return new TrackSampleTable(jArr, iArr, i, jArr2, iArr2);
        } else {
            int i19 = 0;
            int i20 = 0;
            boolean z4 = false;
            for (int i21 = 0; i21 < track.editListDurations.length; i21++) {
                long j3 = track.editListMediaTimes[i21];
                if (j3 != -1) {
                    long scaleLargeTimestamp = Util.scaleLargeTimestamp(track.editListDurations[i21], track.timescale, track.movieTimescale);
                    int binarySearchCeil = Util.binarySearchCeil(jArr2, j3, true, true);
                    i20 = Util.binarySearchCeil(jArr2, scaleLargeTimestamp + j3, true, false);
                    i19 += i20 - binarySearchCeil;
                    if (i20 != binarySearchCeil) {
                        z = true;
                    } else {
                        z = false;
                    }
                    z4 = z | z4;
                } else {
                    z4 = z4;
                    i20 = i20;
                    i19 = i19;
                }
            }
            boolean z5 = z4 | (i19 != sampleCount);
            if (z5) {
                jArr3 = new long[i19];
            } else {
                jArr3 = jArr;
            }
            if (z5) {
                iArr3 = new int[i19];
            } else {
                iArr3 = iArr;
            }
            if (z5) {
                i = 0;
            }
            if (z5) {
                iArr4 = new int[i19];
            } else {
                iArr4 = iArr2;
            }
            long[] jArr7 = new long[i19];
            int i22 = 0;
            int i23 = 0;
            long j4 = 0;
            while (i22 < track.editListDurations.length) {
                long j5 = track.editListMediaTimes[i22];
                long j6 = track.editListDurations[i22];
                if (j5 != -1) {
                    long scaleLargeTimestamp2 = j5 + Util.scaleLargeTimestamp(j6, track.timescale, track.movieTimescale);
                    int binarySearchCeil2 = Util.binarySearchCeil(jArr2, j5, true, true);
                    int binarySearchCeil3 = Util.binarySearchCeil(jArr2, scaleLargeTimestamp2, true, false);
                    if (z5) {
                        int i24 = binarySearchCeil3 - binarySearchCeil2;
                        System.arraycopy(jArr, binarySearchCeil2, jArr3, i23, i24);
                        System.arraycopy(iArr, binarySearchCeil2, iArr3, i23, i24);
                        System.arraycopy(iArr2, binarySearchCeil2, iArr4, i23, i24);
                    }
                    int i25 = i23;
                    for (int i26 = binarySearchCeil2; i26 < binarySearchCeil3; i26++) {
                        jArr7[i25] = Util.scaleLargeTimestamp(jArr2[i26] - j5, C.MICROS_PER_SECOND, track.timescale) + Util.scaleLargeTimestamp(j4, C.MICROS_PER_SECOND, track.movieTimescale);
                        if (z5 && iArr3[i25] > i) {
                            i = iArr[i26];
                        }
                        i25++;
                    }
                    i2 = i25;
                } else {
                    i2 = i23;
                }
                i22++;
                i23 = i2;
                j4 = j6 + j4;
            }
            boolean z6 = false;
            for (int i27 = 0; i27 < iArr4.length && !z6; i27++) {
                z6 |= (iArr4[i27] & 1) != 0;
            }
            if (z6) {
                return new TrackSampleTable(jArr3, iArr3, i, jArr7, iArr4);
            }
            throw new ParserException("The edited sample sequence does not contain a sync sample.");
        }
    }

    public static GaplessInfo parseUdta(Atom.LeafAtom leafAtom, boolean z) {
        if (z) {
            return null;
        }
        ParsableByteArray parsableByteArray = leafAtom.data;
        parsableByteArray.setPosition(8);
        while (parsableByteArray.bytesLeft() >= 8) {
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_meta) {
                parsableByteArray.setPosition(parsableByteArray.getPosition() - 8);
                parsableByteArray.setLimit(parsableByteArray.getPosition() + readInt);
                return parseMetaAtom(parsableByteArray);
            }
            parsableByteArray.skipBytes(readInt - 8);
        }
        return null;
    }

    private static GaplessInfo parseMetaAtom(ParsableByteArray parsableByteArray) {
        parsableByteArray.skipBytes(12);
        ParsableByteArray parsableByteArray2 = new ParsableByteArray();
        while (parsableByteArray.bytesLeft() >= 8) {
            int readInt = parsableByteArray.readInt() - 8;
            if (parsableByteArray.readInt() == Atom.TYPE_ilst) {
                parsableByteArray2.reset(parsableByteArray.data, parsableByteArray.getPosition() + readInt);
                parsableByteArray2.setPosition(parsableByteArray.getPosition());
                GaplessInfo parseIlst = parseIlst(parsableByteArray2);
                if (parseIlst != null) {
                    return parseIlst;
                }
            }
            parsableByteArray.skipBytes(readInt);
        }
        return null;
    }

    private static GaplessInfo parseIlst(ParsableByteArray parsableByteArray) {
        while (parsableByteArray.bytesLeft() > 0) {
            int position = parsableByteArray.getPosition() + parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_DASHES) {
                String str = null;
                String str2 = null;
                String str3 = null;
                while (parsableByteArray.getPosition() < position) {
                    int readInt = parsableByteArray.readInt() - 12;
                    int readInt2 = parsableByteArray.readInt();
                    parsableByteArray.skipBytes(4);
                    if (readInt2 == Atom.TYPE_mean) {
                        str3 = parsableByteArray.readString(readInt);
                    } else if (readInt2 == Atom.TYPE_name) {
                        str2 = parsableByteArray.readString(readInt);
                    } else if (readInt2 == Atom.TYPE_data) {
                        parsableByteArray.skipBytes(4);
                        str = parsableByteArray.readString(readInt - 4);
                    } else {
                        parsableByteArray.skipBytes(readInt);
                    }
                }
                if (!(str2 == null || str == null || !"com.apple.iTunes".equals(str3))) {
                    return GaplessInfo.createFromComment(str2, str);
                }
            } else {
                parsableByteArray.setPosition(position);
            }
        }
        return null;
    }

    private static long parseMvhd(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.setPosition(8);
        if (Atom.parseFullAtomVersion(parsableByteArray.readInt()) != 0) {
            i = 16;
        }
        parsableByteArray.skipBytes(i);
        return parsableByteArray.readUnsignedInt();
    }

    private static TkhdData parseTkhd(ParsableByteArray parsableByteArray) {
        long readUnsignedInt;
        int i;
        int i2 = 8;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        parsableByteArray.skipBytes(parseFullAtomVersion == 0 ? 8 : 16);
        int readInt = parsableByteArray.readInt();
        parsableByteArray.skipBytes(4);
        boolean z = true;
        int position = parsableByteArray.getPosition();
        if (parseFullAtomVersion == 0) {
            i2 = 4;
        }
        int i3 = 0;
        while (true) {
            if (i3 >= i2) {
                break;
            } else if (parsableByteArray.data[position + i3] != -1) {
                z = false;
                break;
            } else {
                i3++;
            }
        }
        if (z) {
            parsableByteArray.skipBytes(i2);
            readUnsignedInt = -1;
        } else {
            readUnsignedInt = parseFullAtomVersion == 0 ? parsableByteArray.readUnsignedInt() : parsableByteArray.readUnsignedLongToLong();
            if (readUnsignedInt == 0) {
                readUnsignedInt = -1;
            }
        }
        parsableByteArray.skipBytes(16);
        int readInt2 = parsableByteArray.readInt();
        int readInt3 = parsableByteArray.readInt();
        parsableByteArray.skipBytes(4);
        int readInt4 = parsableByteArray.readInt();
        int readInt5 = parsableByteArray.readInt();
        if (readInt2 == 0 && readInt3 == 65536 && readInt4 == (-65536) && readInt5 == 0) {
            i = 90;
        } else if (readInt2 == 0 && readInt3 == (-65536) && readInt4 == 65536 && readInt5 == 0) {
            i = 270;
        } else {
            i = (readInt2 == (-65536) && readInt3 == 0 && readInt4 == 0 && readInt5 == (-65536)) ? 180 : 0;
        }
        return new TkhdData(readInt, readUnsignedInt, i);
    }

    private static int parseHdlr(ParsableByteArray parsableByteArray) {
        parsableByteArray.setPosition(16);
        return parsableByteArray.readInt();
    }

    private static Pair<Long, String> parseMdhd(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        parsableByteArray.skipBytes(parseFullAtomVersion == 0 ? 8 : 16);
        long readUnsignedInt = parsableByteArray.readUnsignedInt();
        if (parseFullAtomVersion == 0) {
            i = 4;
        }
        parsableByteArray.skipBytes(i);
        int readUnsignedShort = parsableByteArray.readUnsignedShort();
        return Pair.create(Long.valueOf(readUnsignedInt), "" + ((char) (((readUnsignedShort >> 10) & 31) + 96)) + ((char) (((readUnsignedShort >> 5) & 31) + 96)) + ((char) ((readUnsignedShort & 31) + 96)));
    }

    private static StsdData parseStsd(ParsableByteArray parsableByteArray, int i, long j, int i2, String str, boolean z) {
        parsableByteArray.setPosition(12);
        int readInt = parsableByteArray.readInt();
        StsdData stsdData = new StsdData(readInt);
        for (int i3 = 0; i3 < readInt; i3++) {
            int position = parsableByteArray.getPosition();
            int readInt2 = parsableByteArray.readInt();
            Assertions.checkArgument(readInt2 > 0, "childAtomSize should be positive");
            int readInt3 = parsableByteArray.readInt();
            if (readInt3 == Atom.TYPE_avc1 || readInt3 == Atom.TYPE_avc3 || readInt3 == Atom.TYPE_encv || readInt3 == Atom.TYPE_mp4v || readInt3 == Atom.TYPE_hvc1 || readInt3 == Atom.TYPE_hev1 || readInt3 == Atom.TYPE_s263 || readInt3 == Atom.TYPE_vp08 || readInt3 == Atom.TYPE_vp09) {
                parseVideoSampleEntry(parsableByteArray, readInt3, position, readInt2, i, j, i2, stsdData, i3);
            } else if (readInt3 == Atom.TYPE_mp4a || readInt3 == Atom.TYPE_enca || readInt3 == Atom.TYPE_ac_3 || readInt3 == Atom.TYPE_ec_3 || readInt3 == Atom.TYPE_dtsc || readInt3 == Atom.TYPE_dtse || readInt3 == Atom.TYPE_dtsh || readInt3 == Atom.TYPE_dtsl || readInt3 == Atom.TYPE_samr || readInt3 == Atom.TYPE_sawb || readInt3 == Atom.TYPE_lpcm || readInt3 == Atom.TYPE_sowt) {
                parseAudioSampleEntry(parsableByteArray, readInt3, position, readInt2, i, j, str, z, stsdData, i3);
            } else if (readInt3 == Atom.TYPE_TTML) {
                stsdData.mediaFormat = MediaFormat.createTextFormat(Integer.toString(i), MimeTypes.APPLICATION_TTML, -1, j, str);
            } else if (readInt3 == Atom.TYPE_tx3g) {
                stsdData.mediaFormat = MediaFormat.createTextFormat(Integer.toString(i), MimeTypes.APPLICATION_TX3G, -1, j, str);
            } else if (readInt3 == Atom.TYPE_wvtt) {
                stsdData.mediaFormat = MediaFormat.createTextFormat(Integer.toString(i), MimeTypes.APPLICATION_MP4VTT, -1, j, str);
            } else if (readInt3 == Atom.TYPE_stpp) {
                stsdData.mediaFormat = MediaFormat.createTextFormat(Integer.toString(i), MimeTypes.APPLICATION_TTML, -1, j, str, 0);
            }
            parsableByteArray.setPosition(position + readInt2);
        }
        return stsdData;
    }

    /* JADX WARNING: Removed duplicated region for block: B:65:0x0143  */
    /* JADX WARNING: Removed duplicated region for block: B:83:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void parseVideoSampleEntry(com.google.android.exoplayer.util.ParsableByteArray r19, int r20, int r21, int r22, int r23, long r24, int r26, com.google.android.exoplayer.extractor.mp4.AtomParsers.StsdData r27, int r28) {
        /*
        // Method dump skipped, instructions count: 354
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer.extractor.mp4.AtomParsers.parseVideoSampleEntry(com.google.android.exoplayer.util.ParsableByteArray, int, int, int, int, long, int, com.google.android.exoplayer.extractor.mp4.AtomParsers$StsdData, int):void");
    }

    private static AvcCData parseAvcCFromParent(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.setPosition(i + 8 + 4);
        int readUnsignedByte = (parsableByteArray.readUnsignedByte() & 3) + 1;
        if (readUnsignedByte == 3) {
            throw new IllegalStateException();
        }
        ArrayList arrayList = new ArrayList();
        float f = 1.0f;
        int readUnsignedByte2 = parsableByteArray.readUnsignedByte() & 31;
        for (int i2 = 0; i2 < readUnsignedByte2; i2++) {
            arrayList.add(NalUnitUtil.parseChildNalUnit(parsableByteArray));
        }
        int readUnsignedByte3 = parsableByteArray.readUnsignedByte();
        for (int i3 = 0; i3 < readUnsignedByte3; i3++) {
            arrayList.add(NalUnitUtil.parseChildNalUnit(parsableByteArray));
        }
        if (readUnsignedByte2 > 0) {
            ParsableBitArray parsableBitArray = new ParsableBitArray((byte[]) arrayList.get(0));
            parsableBitArray.setPosition((readUnsignedByte + 1) * 8);
            f = NalUnitUtil.parseSpsNalUnit(parsableBitArray).pixelWidthAspectRatio;
        }
        return new AvcCData(arrayList, readUnsignedByte, f);
    }

    private static Pair<List<byte[]>, Integer> parseHvcCFromParent(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.setPosition(i + 8 + 21);
        int readUnsignedByte = parsableByteArray.readUnsignedByte() & 3;
        int readUnsignedByte2 = parsableByteArray.readUnsignedByte();
        int position = parsableByteArray.getPosition();
        int i2 = 0;
        int i3 = 0;
        while (i2 < readUnsignedByte2) {
            parsableByteArray.skipBytes(1);
            int readUnsignedShort = parsableByteArray.readUnsignedShort();
            int i4 = i3;
            for (int i5 = 0; i5 < readUnsignedShort; i5++) {
                int readUnsignedShort2 = parsableByteArray.readUnsignedShort();
                i4 += readUnsignedShort2 + 4;
                parsableByteArray.skipBytes(readUnsignedShort2);
            }
            i2++;
            i3 = i4;
        }
        parsableByteArray.setPosition(position);
        byte[] bArr = new byte[i3];
        int i6 = 0;
        for (int i7 = 0; i7 < readUnsignedByte2; i7++) {
            parsableByteArray.skipBytes(1);
            int readUnsignedShort3 = parsableByteArray.readUnsignedShort();
            for (int i8 = 0; i8 < readUnsignedShort3; i8++) {
                int readUnsignedShort4 = parsableByteArray.readUnsignedShort();
                System.arraycopy(NalUnitUtil.NAL_START_CODE, 0, bArr, i6, NalUnitUtil.NAL_START_CODE.length);
                int length = i6 + NalUnitUtil.NAL_START_CODE.length;
                System.arraycopy(parsableByteArray.data, parsableByteArray.getPosition(), bArr, length, readUnsignedShort4);
                i6 = length + readUnsignedShort4;
                parsableByteArray.skipBytes(readUnsignedShort4);
            }
        }
        return Pair.create(i3 == 0 ? null : Collections.singletonList(bArr), Integer.valueOf(readUnsignedByte + 1));
    }

    private static Pair<long[], long[]> parseEdts(Atom.ContainerAtom containerAtom) {
        Atom.LeafAtom leafAtomOfType;
        if (containerAtom == null || (leafAtomOfType = containerAtom.getLeafAtomOfType(Atom.TYPE_elst)) == null) {
            return Pair.create(null, null);
        }
        ParsableByteArray parsableByteArray = leafAtomOfType.data;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        int readUnsignedIntToInt = parsableByteArray.readUnsignedIntToInt();
        long[] jArr = new long[readUnsignedIntToInt];
        long[] jArr2 = new long[readUnsignedIntToInt];
        for (int i = 0; i < readUnsignedIntToInt; i++) {
            jArr[i] = parseFullAtomVersion == 1 ? parsableByteArray.readUnsignedLongToLong() : parsableByteArray.readUnsignedInt();
            jArr2[i] = parseFullAtomVersion == 1 ? parsableByteArray.readLong() : (long) parsableByteArray.readInt();
            if (parsableByteArray.readShort() != 1) {
                throw new IllegalArgumentException("Unsupported media rate.");
            }
            parsableByteArray.skipBytes(2);
        }
        return Pair.create(jArr, jArr2);
    }

    private static float parsePaspFromParent(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.setPosition(i + 8);
        return ((float) parsableByteArray.readUnsignedIntToInt()) / ((float) parsableByteArray.readUnsignedIntToInt());
    }

    private static void parseAudioSampleEntry(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, long j, String str, boolean z, StsdData stsdData, int i5) {
        int i6;
        int i7;
        int i8;
        List singletonList;
        int findEsdsPosition;
        byte[] bArr;
        parsableByteArray.setPosition(i2 + 8);
        if (z) {
            parsableByteArray.skipBytes(8);
            int readUnsignedShort = parsableByteArray.readUnsignedShort();
            parsableByteArray.skipBytes(6);
            i6 = readUnsignedShort;
        } else {
            parsableByteArray.skipBytes(16);
            i6 = 0;
        }
        if (i6 == 0 || i6 == 1) {
            int readUnsignedShort2 = parsableByteArray.readUnsignedShort();
            parsableByteArray.skipBytes(6);
            i8 = parsableByteArray.readUnsignedFixedPoint1616();
            if (i6 == 1) {
                parsableByteArray.skipBytes(16);
                i7 = readUnsignedShort2;
            } else {
                i7 = readUnsignedShort2;
            }
        } else if (i6 == 2) {
            parsableByteArray.skipBytes(16);
            i8 = (int) Math.round(parsableByteArray.readDouble());
            int readUnsignedIntToInt = parsableByteArray.readUnsignedIntToInt();
            parsableByteArray.skipBytes(20);
            i7 = readUnsignedIntToInt;
        } else {
            return;
        }
        int position = parsableByteArray.getPosition();
        if (i == Atom.TYPE_enca) {
            i = parseSampleEntryEncryptionData(parsableByteArray, i2, i3, stsdData, i5);
            parsableByteArray.setPosition(position);
        }
        String str2 = null;
        if (i == Atom.TYPE_ac_3) {
            str2 = MimeTypes.AUDIO_AC3;
        } else if (i == Atom.TYPE_ec_3) {
            str2 = MimeTypes.AUDIO_E_AC3;
        } else if (i == Atom.TYPE_dtsc) {
            str2 = MimeTypes.AUDIO_DTS;
        } else if (i == Atom.TYPE_dtsh || i == Atom.TYPE_dtsl) {
            str2 = MimeTypes.AUDIO_DTS_HD;
        } else if (i == Atom.TYPE_dtse) {
            str2 = MimeTypes.AUDIO_DTS_EXPRESS;
        } else if (i == Atom.TYPE_samr) {
            str2 = MimeTypes.AUDIO_AMR_NB;
        } else if (i == Atom.TYPE_sawb) {
            str2 = MimeTypes.AUDIO_AMR_WB;
        } else if (i == Atom.TYPE_lpcm || i == Atom.TYPE_sowt) {
            str2 = MimeTypes.AUDIO_RAW;
        }
        byte[] bArr2 = null;
        int i9 = i8;
        int i10 = i7;
        while (position - i2 < i3) {
            parsableByteArray.setPosition(position);
            int readInt = parsableByteArray.readInt();
            Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
            int readInt2 = parsableByteArray.readInt();
            if (readInt2 == Atom.TYPE_esds || (z && readInt2 == Atom.TYPE_wave)) {
                if (readInt2 == Atom.TYPE_esds) {
                    findEsdsPosition = position;
                } else {
                    findEsdsPosition = findEsdsPosition(parsableByteArray, position, readInt);
                }
                if (findEsdsPosition != -1) {
                    Pair<String, byte[]> parseEsdsFromParent = parseEsdsFromParent(parsableByteArray, findEsdsPosition);
                    str2 = (String) parseEsdsFromParent.first;
                    bArr = (byte[]) parseEsdsFromParent.second;
                    if (MimeTypes.AUDIO_AAC.equals(str2)) {
                        Pair<Integer, Integer> parseAacAudioSpecificConfig = CodecSpecificDataUtil.parseAacAudioSpecificConfig(bArr);
                        i9 = ((Integer) parseAacAudioSpecificConfig.first).intValue();
                        i10 = ((Integer) parseAacAudioSpecificConfig.second).intValue();
                    }
                } else {
                    bArr = bArr2;
                }
                bArr2 = bArr;
            } else if (readInt2 == Atom.TYPE_dac3) {
                parsableByteArray.setPosition(position + 8);
                stsdData.mediaFormat = Ac3Util.parseAc3AnnexFFormat(parsableByteArray, Integer.toString(i4), j, str);
            } else if (readInt2 == Atom.TYPE_dec3) {
                parsableByteArray.setPosition(position + 8);
                stsdData.mediaFormat = Ac3Util.parseEAc3AnnexFFormat(parsableByteArray, Integer.toString(i4), j, str);
            } else if (readInt2 == Atom.TYPE_ddts) {
                stsdData.mediaFormat = MediaFormat.createAudioFormat(Integer.toString(i4), str2, -1, -1, j, i10, i9, null, str);
            }
            position += readInt;
        }
        if (stsdData.mediaFormat == null && str2 != null) {
            int i11 = MimeTypes.AUDIO_RAW.equals(str2) ? 2 : -1;
            String num = Integer.toString(i4);
            if (bArr2 == null) {
                singletonList = null;
            } else {
                singletonList = Collections.singletonList(bArr2);
            }
            stsdData.mediaFormat = MediaFormat.createAudioFormat(num, str2, -1, -1, j, i10, i9, singletonList, str, i11);
        }
    }

    private static int findEsdsPosition(ParsableByteArray parsableByteArray, int i, int i2) {
        int position = parsableByteArray.getPosition();
        while (position - i < i2) {
            parsableByteArray.setPosition(position);
            int readInt = parsableByteArray.readInt();
            Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (parsableByteArray.readInt() == Atom.TYPE_esds) {
                return position;
            }
            position += readInt;
        }
        return -1;
    }

    private static Pair<String, byte[]> parseEsdsFromParent(ParsableByteArray parsableByteArray, int i) {
        String str = null;
        parsableByteArray.setPosition(i + 8 + 4);
        parsableByteArray.skipBytes(1);
        parseExpandableClassSize(parsableByteArray);
        parsableByteArray.skipBytes(2);
        int readUnsignedByte = parsableByteArray.readUnsignedByte();
        if ((readUnsignedByte & 128) != 0) {
            parsableByteArray.skipBytes(2);
        }
        if ((readUnsignedByte & 64) != 0) {
            parsableByteArray.skipBytes(parsableByteArray.readUnsignedShort());
        }
        if ((readUnsignedByte & 32) != 0) {
            parsableByteArray.skipBytes(2);
        }
        parsableByteArray.skipBytes(1);
        parseExpandableClassSize(parsableByteArray);
        switch (parsableByteArray.readUnsignedByte()) {
            case 32:
                str = MimeTypes.VIDEO_MP4V;
                break;
            case 33:
                str = MimeTypes.VIDEO_H264;
                break;
            case 35:
                str = MimeTypes.VIDEO_H265;
                break;
            case 64:
            case 102:
            case 103:
            case 104:
                str = MimeTypes.AUDIO_AAC;
                break;
            case 107:
                return Pair.create(MimeTypes.AUDIO_MPEG, null);
            case 165:
                str = MimeTypes.AUDIO_AC3;
                break;
            case 166:
                str = MimeTypes.AUDIO_E_AC3;
                break;
            case 169:
            case 172:
                return Pair.create(MimeTypes.AUDIO_DTS, null);
            case 170:
            case 171:
                return Pair.create(MimeTypes.AUDIO_DTS_HD, null);
        }
        parsableByteArray.skipBytes(12);
        parsableByteArray.skipBytes(1);
        int parseExpandableClassSize = parseExpandableClassSize(parsableByteArray);
        byte[] bArr = new byte[parseExpandableClassSize];
        parsableByteArray.readBytes(bArr, 0, parseExpandableClassSize);
        return Pair.create(str, bArr);
    }

    private static int parseSampleEntryEncryptionData(ParsableByteArray parsableByteArray, int i, int i2, StsdData stsdData, int i3) {
        boolean z = true;
        int position = parsableByteArray.getPosition();
        while (position - i < i2) {
            parsableByteArray.setPosition(position);
            int readInt = parsableByteArray.readInt();
            Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (parsableByteArray.readInt() == Atom.TYPE_sinf) {
                Pair<Integer, TrackEncryptionBox> parseSinfFromParent = parseSinfFromParent(parsableByteArray, position, readInt);
                Integer num = (Integer) parseSinfFromParent.first;
                if (num == null) {
                    z = false;
                }
                Assertions.checkArgument(z, "frma atom is mandatory");
                stsdData.trackEncryptionBoxes[i3] = (TrackEncryptionBox) parseSinfFromParent.second;
                return num.intValue();
            }
            position += readInt;
        }
        return 0;
    }

    private static Pair<Integer, TrackEncryptionBox> parseSinfFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        Integer num = null;
        TrackEncryptionBox trackEncryptionBox = null;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            int readInt2 = parsableByteArray.readInt();
            if (readInt2 == Atom.TYPE_frma) {
                num = Integer.valueOf(parsableByteArray.readInt());
            } else if (readInt2 == Atom.TYPE_schm) {
                parsableByteArray.skipBytes(4);
                parsableByteArray.readInt();
                parsableByteArray.readInt();
            } else if (readInt2 == Atom.TYPE_schi) {
                trackEncryptionBox = parseSchiFromParent(parsableByteArray, i3, readInt);
            }
            i3 = readInt + i3;
        }
        return Pair.create(num, trackEncryptionBox);
    }

    private static TrackEncryptionBox parseSchiFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        boolean z = true;
        int i3 = i + 8;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_tenc) {
                parsableByteArray.skipBytes(6);
                if (parsableByteArray.readUnsignedByte() != 1) {
                    z = false;
                }
                int readUnsignedByte = parsableByteArray.readUnsignedByte();
                byte[] bArr = new byte[16];
                parsableByteArray.readBytes(bArr, 0, bArr.length);
                return new TrackEncryptionBox(z, readUnsignedByte, bArr);
            }
            i3 += readInt;
        }
        return null;
    }

    private static byte[] parseProjFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_proj) {
                return Arrays.copyOfRange(parsableByteArray.data, i3, readInt + i3);
            }
            i3 += readInt;
        }
        return null;
    }

    private static int parseExpandableClassSize(ParsableByteArray parsableByteArray) {
        int readUnsignedByte = parsableByteArray.readUnsignedByte();
        int i = readUnsignedByte & 127;
        while ((readUnsignedByte & 128) == 128) {
            readUnsignedByte = parsableByteArray.readUnsignedByte();
            i = (i << 7) | (readUnsignedByte & 127);
        }
        return i;
    }

    private AtomParsers() {
    }

    private static final class ChunkIterator {
        private final ParsableByteArray chunkOffsets;
        private final boolean chunkOffsetsAreLongs;
        public int index;
        public final int length;
        private int nextSamplesPerChunkChangeIndex;
        public int numSamples;
        public long offset;
        private int remainingSamplesPerChunkChanges;
        private final ParsableByteArray stsc;

        public ChunkIterator(ParsableByteArray parsableByteArray, ParsableByteArray parsableByteArray2, boolean z) {
            boolean z2 = true;
            this.stsc = parsableByteArray;
            this.chunkOffsets = parsableByteArray2;
            this.chunkOffsetsAreLongs = z;
            parsableByteArray2.setPosition(12);
            this.length = parsableByteArray2.readUnsignedIntToInt();
            parsableByteArray.setPosition(12);
            this.remainingSamplesPerChunkChanges = parsableByteArray.readUnsignedIntToInt();
            Assertions.checkState(parsableByteArray.readInt() != 1 ? false : z2, "first_chunk must be 1");
            this.index = -1;
        }

        public boolean moveNext() {
            long readUnsignedInt;
            int i = this.index + 1;
            this.index = i;
            if (i == this.length) {
                return false;
            }
            if (this.chunkOffsetsAreLongs) {
                readUnsignedInt = this.chunkOffsets.readUnsignedLongToLong();
            } else {
                readUnsignedInt = this.chunkOffsets.readUnsignedInt();
            }
            this.offset = readUnsignedInt;
            if (this.index == this.nextSamplesPerChunkChangeIndex) {
                this.numSamples = this.stsc.readUnsignedIntToInt();
                this.stsc.skipBytes(4);
                int i2 = this.remainingSamplesPerChunkChanges - 1;
                this.remainingSamplesPerChunkChanges = i2;
                this.nextSamplesPerChunkChangeIndex = i2 > 0 ? this.stsc.readUnsignedIntToInt() - 1 : -1;
            }
            return true;
        }
    }

    /* access modifiers changed from: private */
    public static final class TkhdData {
        private final long duration;
        private final int id;
        private final int rotationDegrees;

        public TkhdData(int i, long j, int i2) {
            this.id = i;
            this.duration = j;
            this.rotationDegrees = i2;
        }
    }

    /* access modifiers changed from: private */
    public static final class StsdData {
        public MediaFormat mediaFormat;
        public int nalUnitLengthFieldLength = -1;
        public final TrackEncryptionBox[] trackEncryptionBoxes;

        public StsdData(int i) {
            this.trackEncryptionBoxes = new TrackEncryptionBox[i];
        }
    }

    /* access modifiers changed from: private */
    public static final class AvcCData {
        public final List<byte[]> initializationData;
        public final int nalUnitLengthFieldLength;
        public final float pixelWidthAspectRatio;

        public AvcCData(List<byte[]> list, int i, float f) {
            this.initializationData = list;
            this.nalUnitLengthFieldLength = i;
            this.pixelWidthAspectRatio = f;
        }
    }

    static final class StszSampleSizeBox implements SampleSizeBox {
        private final ParsableByteArray data;
        private final int fixedSampleSize = this.data.readUnsignedIntToInt();
        private final int sampleCount = this.data.readUnsignedIntToInt();

        public StszSampleSizeBox(Atom.LeafAtom leafAtom) {
            this.data = leafAtom.data;
            this.data.setPosition(12);
        }

        @Override // com.google.android.exoplayer.extractor.mp4.AtomParsers.SampleSizeBox
        public int getSampleCount() {
            return this.sampleCount;
        }

        @Override // com.google.android.exoplayer.extractor.mp4.AtomParsers.SampleSizeBox
        public int readNextSampleSize() {
            return this.fixedSampleSize == 0 ? this.data.readUnsignedIntToInt() : this.fixedSampleSize;
        }

        @Override // com.google.android.exoplayer.extractor.mp4.AtomParsers.SampleSizeBox
        public boolean isFixedSampleSize() {
            return this.fixedSampleSize != 0;
        }
    }

    static final class Stz2SampleSizeBox implements SampleSizeBox {
        private int currentByte;
        private final ParsableByteArray data;
        private final int fieldSize = (this.data.readUnsignedIntToInt() & 255);
        private final int sampleCount = this.data.readUnsignedIntToInt();
        private int sampleIndex;

        public Stz2SampleSizeBox(Atom.LeafAtom leafAtom) {
            this.data = leafAtom.data;
            this.data.setPosition(12);
        }

        @Override // com.google.android.exoplayer.extractor.mp4.AtomParsers.SampleSizeBox
        public int getSampleCount() {
            return this.sampleCount;
        }

        @Override // com.google.android.exoplayer.extractor.mp4.AtomParsers.SampleSizeBox
        public int readNextSampleSize() {
            if (this.fieldSize == 8) {
                return this.data.readUnsignedByte();
            }
            if (this.fieldSize == 16) {
                return this.data.readUnsignedShort();
            }
            int i = this.sampleIndex;
            this.sampleIndex = i + 1;
            if (i % 2 != 0) {
                return this.currentByte & 15;
            }
            this.currentByte = this.data.readUnsignedByte();
            return (this.currentByte & PsExtractor.VIDEO_STREAM_MASK) >> 4;
        }

        @Override // com.google.android.exoplayer.extractor.mp4.AtomParsers.SampleSizeBox
        public boolean isFixedSampleSize() {
            return false;
        }
    }
}
