package com.google.android.exoplayer.extractor.ogg;

import com.google.android.exoplayer.extractor.ExtractorInput;
import com.google.android.exoplayer.extractor.ogg.OggUtil;
import com.google.android.exoplayer.util.Assertions;
import com.google.android.exoplayer.util.ParsableByteArray;

/* access modifiers changed from: package-private */
public final class OggSeeker {
    private static final int MATCH_RANGE = 72000;
    private long audioDataLength = -1;
    private final ParsableByteArray headerArray = new ParsableByteArray(282);
    private final OggUtil.PageHeader pageHeader = new OggUtil.PageHeader();
    private long totalSamples;

    OggSeeker() {
    }

    public void setup(long j, long j2) {
        Assertions.checkArgument(j > 0 && j2 > 0);
        this.audioDataLength = j;
        this.totalSamples = j2;
    }

    public long getNextSeekPosition(long j, ExtractorInput extractorInput) {
        int i = 1;
        Assertions.checkState((this.audioDataLength == -1 || this.totalSamples == 0) ? false : true);
        OggUtil.populatePageHeader(extractorInput, this.pageHeader, this.headerArray, false);
        long j2 = j - this.pageHeader.granulePosition;
        if (j2 <= 0 || j2 > 72000) {
            int i2 = this.pageHeader.bodySize + this.pageHeader.headerSize;
            if (j2 <= 0) {
                i = 2;
            }
            return (extractorInput.getPosition() - ((long) (i2 * i))) + ((j2 * this.audioDataLength) / this.totalSamples);
        }
        extractorInput.resetPeekPosition();
        return -1;
    }
}
