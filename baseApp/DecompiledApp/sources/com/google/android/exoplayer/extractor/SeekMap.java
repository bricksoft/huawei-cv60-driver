package com.google.android.exoplayer.extractor;

public interface SeekMap {
    public static final SeekMap UNSEEKABLE = new SeekMap() {
        /* class com.google.android.exoplayer.extractor.SeekMap.AnonymousClass1 */

        @Override // com.google.android.exoplayer.extractor.SeekMap
        public boolean isSeekable() {
            return false;
        }

        @Override // com.google.android.exoplayer.extractor.SeekMap
        public long getPosition(long j) {
            return 0;
        }
    };

    long getPosition(long j);

    boolean isSeekable();
}
