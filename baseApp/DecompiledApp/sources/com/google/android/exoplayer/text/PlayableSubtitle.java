package com.google.android.exoplayer.text;

import java.util.List;

final class PlayableSubtitle implements Subtitle {
    private final long offsetUs;
    public final long startTimeUs;
    private final Subtitle subtitle;

    public PlayableSubtitle(Subtitle subtitle2, boolean z, long j, long j2) {
        this.subtitle = subtitle2;
        this.startTimeUs = j;
        this.offsetUs = (!z ? 0 : j) + j2;
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public int getEventTimeCount() {
        return this.subtitle.getEventTimeCount();
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public long getEventTime(int i) {
        return this.subtitle.getEventTime(i) + this.offsetUs;
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public long getLastEventTime() {
        return this.subtitle.getLastEventTime() + this.offsetUs;
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public int getNextEventTimeIndex(long j) {
        return this.subtitle.getNextEventTimeIndex(j - this.offsetUs);
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public List<Cue> getCues(long j) {
        return this.subtitle.getCues(j - this.offsetUs);
    }
}
