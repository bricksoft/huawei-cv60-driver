package com.google.android.exoplayer.text.webvtt;

import android.text.SpannableStringBuilder;
import com.google.android.exoplayer.text.Cue;
import com.google.android.exoplayer.text.Subtitle;
import com.google.android.exoplayer.util.Assertions;
import com.google.android.exoplayer.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class WebvttSubtitle implements Subtitle {
    private final long[] cueTimesUs = new long[(this.numCues * 2)];
    private final List<WebvttCue> cues;
    private final int numCues;
    private final long[] sortedCueTimesUs;

    public WebvttSubtitle(List<WebvttCue> list) {
        this.cues = list;
        this.numCues = list.size();
        for (int i = 0; i < this.numCues; i++) {
            WebvttCue webvttCue = list.get(i);
            int i2 = i * 2;
            this.cueTimesUs[i2] = webvttCue.startTime;
            this.cueTimesUs[i2 + 1] = webvttCue.endTime;
        }
        this.sortedCueTimesUs = Arrays.copyOf(this.cueTimesUs, this.cueTimesUs.length);
        Arrays.sort(this.sortedCueTimesUs);
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public int getNextEventTimeIndex(long j) {
        int binarySearchCeil = Util.binarySearchCeil(this.sortedCueTimesUs, j, false, false);
        if (binarySearchCeil < this.sortedCueTimesUs.length) {
            return binarySearchCeil;
        }
        return -1;
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public int getEventTimeCount() {
        return this.sortedCueTimesUs.length;
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public long getEventTime(int i) {
        boolean z;
        boolean z2 = true;
        if (i >= 0) {
            z = true;
        } else {
            z = false;
        }
        Assertions.checkArgument(z);
        if (i >= this.sortedCueTimesUs.length) {
            z2 = false;
        }
        Assertions.checkArgument(z2);
        return this.sortedCueTimesUs[i];
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public long getLastEventTime() {
        if (getEventTimeCount() == 0) {
            return -1;
        }
        return this.sortedCueTimesUs[this.sortedCueTimesUs.length - 1];
    }

    @Override // com.google.android.exoplayer.text.Subtitle
    public List<Cue> getCues(long j) {
        WebvttCue webvttCue;
        int i = 0;
        SpannableStringBuilder spannableStringBuilder = null;
        WebvttCue webvttCue2 = null;
        ArrayList arrayList = null;
        while (i < this.numCues) {
            if (this.cueTimesUs[i * 2] <= j && j < this.cueTimesUs[(i * 2) + 1]) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                webvttCue = this.cues.get(i);
                if (webvttCue.isNormalCue()) {
                    if (webvttCue2 != null) {
                        if (spannableStringBuilder == null) {
                            spannableStringBuilder = new SpannableStringBuilder();
                            spannableStringBuilder.append(webvttCue2.text).append((CharSequence) "\n").append(webvttCue.text);
                            webvttCue = webvttCue2;
                        } else {
                            spannableStringBuilder.append((CharSequence) "\n").append(webvttCue.text);
                            webvttCue = webvttCue2;
                        }
                    }
                    i++;
                    webvttCue2 = webvttCue;
                } else {
                    arrayList.add(webvttCue);
                }
            }
            webvttCue = webvttCue2;
            i++;
            webvttCue2 = webvttCue;
        }
        if (spannableStringBuilder != null) {
            arrayList.add(new WebvttCue(spannableStringBuilder));
        } else if (webvttCue2 != null) {
            arrayList.add(webvttCue2);
        }
        return arrayList != null ? arrayList : Collections.emptyList();
    }
}
