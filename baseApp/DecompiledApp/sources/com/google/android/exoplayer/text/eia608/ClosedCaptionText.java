package com.google.android.exoplayer.text.eia608;

/* access modifiers changed from: package-private */
public final class ClosedCaptionText extends ClosedCaption {
    public final String text;

    public ClosedCaptionText(String str) {
        super(1);
        this.text = str;
    }
}
