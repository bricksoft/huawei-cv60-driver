package com.google.android.exoplayer.text;

public interface SubtitleParser {
    boolean canParse(String str);

    Subtitle parse(byte[] bArr, int i, int i2);
}
