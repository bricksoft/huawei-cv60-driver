package com.google.android.exoplayer.text.webvtt;

import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import com.google.android.exoplayer.text.ttml.TtmlNode;
import com.google.android.exoplayer.text.webvtt.WebvttCue;
import com.google.android.exoplayer.util.ParsableByteArray;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class WebvttCueParser {
    private static final char CHAR_AMPERSAND = '&';
    private static final char CHAR_GREATER_THAN = '>';
    private static final char CHAR_LESS_THAN = '<';
    private static final char CHAR_SEMI_COLON = ';';
    private static final char CHAR_SLASH = '/';
    private static final char CHAR_SPACE = ' ';
    private static final Pattern COMMENT = Pattern.compile("^NOTE(( |\t).*)?$");
    public static final Pattern CUE_HEADER_PATTERN = Pattern.compile("^(\\S+)\\s+-->\\s+(\\S+)(.*)?$");
    private static final Pattern CUE_SETTING_PATTERN = Pattern.compile("(\\S+?):(\\S+)");
    private static final String ENTITY_AMPERSAND = "amp";
    private static final String ENTITY_GREATER_THAN = "gt";
    private static final String ENTITY_LESS_THAN = "lt";
    private static final String ENTITY_NON_BREAK_SPACE = "nbsp";
    private static final String SPACE = " ";
    private static final int STYLE_BOLD = 1;
    private static final int STYLE_ITALIC = 2;
    private static final String TAG = "WebvttCueParser";
    private static final String TAG_BOLD = "b";
    private static final String TAG_CLASS = "c";
    private static final String TAG_ITALIC = "i";
    private static final String TAG_LANG = "lang";
    private static final String TAG_UNDERLINE = "u";
    private static final String TAG_VOICE = "v";
    private final StringBuilder textBuilder = new StringBuilder();

    /* access modifiers changed from: package-private */
    public boolean parseNextValidCue(ParsableByteArray parsableByteArray, WebvttCue.Builder builder) {
        Matcher findNextCueHeader;
        do {
            findNextCueHeader = findNextCueHeader(parsableByteArray);
            if (findNextCueHeader == null) {
                return false;
            }
        } while (!parseCue(findNextCueHeader, parsableByteArray, builder, this.textBuilder));
        return true;
    }

    static void parseCueSettingsList(String str, WebvttCue.Builder builder) {
        Matcher matcher = CUE_SETTING_PATTERN.matcher(str);
        while (matcher.find()) {
            String group = matcher.group(1);
            String group2 = matcher.group(2);
            try {
                if ("line".equals(group)) {
                    parseLineAttribute(group2, builder);
                } else if ("align".equals(group)) {
                    builder.setTextAlignment(parseTextAlignment(group2));
                } else if ("position".equals(group)) {
                    parsePositionAttribute(group2, builder);
                } else if ("size".equals(group)) {
                    builder.setWidth(WebvttParserUtil.parsePercentage(group2));
                } else {
                    Log.w(TAG, "Unknown cue setting " + group + ":" + group2);
                }
            } catch (NumberFormatException e) {
                Log.w(TAG, "Skipping bad cue setting: " + matcher.group());
            }
        }
    }

    public static Matcher findNextCueHeader(ParsableByteArray parsableByteArray) {
        String readLine;
        while (true) {
            String readLine2 = parsableByteArray.readLine();
            if (readLine2 == null) {
                return null;
            }
            if (COMMENT.matcher(readLine2).matches()) {
                do {
                    readLine = parsableByteArray.readLine();
                    if (readLine == null) {
                        break;
                    }
                } while (!readLine.isEmpty());
            } else {
                Matcher matcher = CUE_HEADER_PATTERN.matcher(readLine2);
                if (matcher.matches()) {
                    return matcher;
                }
            }
        }
    }

    static void parseCueText(String str, WebvttCue.Builder builder) {
        boolean z;
        int i;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        Stack stack = new Stack();
        int i2 = 0;
        while (i2 < str.length()) {
            char charAt = str.charAt(i2);
            switch (charAt) {
                case '&':
                    int indexOf = str.indexOf(59, i2 + 1);
                    int indexOf2 = str.indexOf(32, i2 + 1);
                    if (indexOf == -1) {
                        indexOf = indexOf2;
                    } else if (indexOf2 != -1) {
                        indexOf = Math.min(indexOf, indexOf2);
                    }
                    if (indexOf == -1) {
                        spannableStringBuilder.append(charAt);
                        i2++;
                        break;
                    } else {
                        applyEntity(str.substring(i2 + 1, indexOf), spannableStringBuilder);
                        if (indexOf == indexOf2) {
                            spannableStringBuilder.append((CharSequence) SPACE);
                        }
                        i2 = indexOf + 1;
                        break;
                    }
                case '<':
                    if (i2 + 1 < str.length()) {
                        boolean z2 = str.charAt(i2 + 1) == '/';
                        int findEndOfTag = findEndOfTag(str, i2 + 1);
                        if (str.charAt(findEndOfTag - 2) == '/') {
                            z = true;
                        } else {
                            z = false;
                        }
                        if (z2) {
                            i = 2;
                        } else {
                            i = 1;
                        }
                        String[] strArr = tokenizeTag(str.substring(i + i2, z ? findEndOfTag - 2 : findEndOfTag - 1));
                        if (strArr != null) {
                            if (isSupportedTag(strArr[0])) {
                                if (!z2) {
                                    if (!z) {
                                        stack.push(new StartTag(strArr[0], spannableStringBuilder.length()));
                                        i2 = findEndOfTag;
                                        break;
                                    }
                                } else {
                                    while (!stack.isEmpty()) {
                                        StartTag startTag = (StartTag) stack.pop();
                                        applySpansForTag(startTag, spannableStringBuilder);
                                        if (startTag.name.equals(strArr[0])) {
                                            i2 = findEndOfTag;
                                            break;
                                        }
                                    }
                                    i2 = findEndOfTag;
                                }
                            } else {
                                i2 = findEndOfTag;
                                break;
                            }
                        }
                        i2 = findEndOfTag;
                        break;
                    } else {
                        i2++;
                        break;
                    }
                default:
                    spannableStringBuilder.append(charAt);
                    i2++;
                    break;
            }
        }
        while (!stack.isEmpty()) {
            applySpansForTag((StartTag) stack.pop(), spannableStringBuilder);
        }
        builder.setText(spannableStringBuilder);
    }

    private static boolean parseCue(Matcher matcher, ParsableByteArray parsableByteArray, WebvttCue.Builder builder, StringBuilder sb) {
        try {
            builder.setStartTime(WebvttParserUtil.parseTimestampUs(matcher.group(1))).setEndTime(WebvttParserUtil.parseTimestampUs(matcher.group(2)));
            parseCueSettingsList(matcher.group(3), builder);
            sb.setLength(0);
            while (true) {
                String readLine = parsableByteArray.readLine();
                if (readLine == null || readLine.isEmpty()) {
                    parseCueText(sb.toString(), builder);
                } else {
                    if (sb.length() > 0) {
                        sb.append("\n");
                    }
                    sb.append(readLine.trim());
                }
            }
            parseCueText(sb.toString(), builder);
            return true;
        } catch (NumberFormatException e) {
            Log.w(TAG, "Skipping cue with bad header: " + matcher.group());
            return false;
        }
    }

    private static void parseLineAttribute(String str, WebvttCue.Builder builder) {
        int indexOf = str.indexOf(44);
        if (indexOf != -1) {
            builder.setLineAnchor(parsePositionAnchor(str.substring(indexOf + 1)));
            str = str.substring(0, indexOf);
        } else {
            builder.setLineAnchor(Integer.MIN_VALUE);
        }
        if (str.endsWith("%")) {
            builder.setLine(WebvttParserUtil.parsePercentage(str)).setLineType(0);
        } else {
            builder.setLine((float) Integer.parseInt(str)).setLineType(1);
        }
    }

    private static void parsePositionAttribute(String str, WebvttCue.Builder builder) {
        int indexOf = str.indexOf(44);
        if (indexOf != -1) {
            builder.setPositionAnchor(parsePositionAnchor(str.substring(indexOf + 1)));
            str = str.substring(0, indexOf);
        } else {
            builder.setPositionAnchor(Integer.MIN_VALUE);
        }
        builder.setPosition(WebvttParserUtil.parsePercentage(str));
    }

    private static int parsePositionAnchor(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case -1364013995:
                if (str.equals(TtmlNode.CENTER)) {
                    c = 1;
                    break;
                }
                break;
            case -1074341483:
                if (str.equals("middle")) {
                    c = 2;
                    break;
                }
                break;
            case 100571:
                if (str.equals(TtmlNode.END)) {
                    c = 3;
                    break;
                }
                break;
            case 109757538:
                if (str.equals(TtmlNode.START)) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return 0;
            case 1:
            case 2:
                return 1;
            case 3:
                return 2;
            default:
                Log.w(TAG, "Invalid anchor value: " + str);
                return Integer.MIN_VALUE;
        }
    }

    private static Layout.Alignment parseTextAlignment(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case -1364013995:
                if (str.equals(TtmlNode.CENTER)) {
                    c = 2;
                    break;
                }
                break;
            case -1074341483:
                if (str.equals("middle")) {
                    c = 3;
                    break;
                }
                break;
            case 100571:
                if (str.equals(TtmlNode.END)) {
                    c = 4;
                    break;
                }
                break;
            case 3317767:
                if (str.equals(TtmlNode.LEFT)) {
                    c = 1;
                    break;
                }
                break;
            case 108511772:
                if (str.equals(TtmlNode.RIGHT)) {
                    c = 5;
                    break;
                }
                break;
            case 109757538:
                if (str.equals(TtmlNode.START)) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
            case 1:
                return Layout.Alignment.ALIGN_NORMAL;
            case 2:
            case 3:
                return Layout.Alignment.ALIGN_CENTER;
            case 4:
            case 5:
                return Layout.Alignment.ALIGN_OPPOSITE;
            default:
                Log.w(TAG, "Invalid alignment value: " + str);
                return null;
        }
    }

    private static int findEndOfTag(String str, int i) {
        int indexOf = str.indexOf(62, i);
        return indexOf == -1 ? str.length() : indexOf + 1;
    }

    private static void applyEntity(String str, SpannableStringBuilder spannableStringBuilder) {
        char c = 65535;
        switch (str.hashCode()) {
            case 3309:
                if (str.equals(ENTITY_GREATER_THAN)) {
                    c = 1;
                    break;
                }
                break;
            case 3464:
                if (str.equals(ENTITY_LESS_THAN)) {
                    c = 0;
                    break;
                }
                break;
            case 96708:
                if (str.equals(ENTITY_AMPERSAND)) {
                    c = 3;
                    break;
                }
                break;
            case 3374865:
                if (str.equals(ENTITY_NON_BREAK_SPACE)) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                spannableStringBuilder.append(CHAR_LESS_THAN);
                return;
            case 1:
                spannableStringBuilder.append(CHAR_GREATER_THAN);
                return;
            case 2:
                spannableStringBuilder.append(CHAR_SPACE);
                return;
            case 3:
                spannableStringBuilder.append(CHAR_AMPERSAND);
                return;
            default:
                Log.w(TAG, "ignoring unsupported entity: '&" + str + ";'");
                return;
        }
    }

    private static boolean isSupportedTag(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case 98:
                if (str.equals(TAG_BOLD)) {
                    c = 0;
                    break;
                }
                break;
            case 99:
                if (str.equals(TAG_CLASS)) {
                    c = 1;
                    break;
                }
                break;
            case 105:
                if (str.equals(TAG_ITALIC)) {
                    c = 2;
                    break;
                }
                break;
            case 117:
                if (str.equals(TAG_UNDERLINE)) {
                    c = 4;
                    break;
                }
                break;
            case 118:
                if (str.equals(TAG_VOICE)) {
                    c = 5;
                    break;
                }
                break;
            case 3314158:
                if (str.equals(TAG_LANG)) {
                    c = 3;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return true;
            default:
                return false;
        }
    }

    private static void applySpansForTag(StartTag startTag, SpannableStringBuilder spannableStringBuilder) {
        String str = startTag.name;
        char c = 65535;
        switch (str.hashCode()) {
            case 98:
                if (str.equals(TAG_BOLD)) {
                    c = 0;
                    break;
                }
                break;
            case 105:
                if (str.equals(TAG_ITALIC)) {
                    c = 1;
                    break;
                }
                break;
            case 117:
                if (str.equals(TAG_UNDERLINE)) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                spannableStringBuilder.setSpan(new StyleSpan(1), startTag.position, spannableStringBuilder.length(), 33);
                return;
            case 1:
                spannableStringBuilder.setSpan(new StyleSpan(2), startTag.position, spannableStringBuilder.length(), 33);
                return;
            case 2:
                spannableStringBuilder.setSpan(new UnderlineSpan(), startTag.position, spannableStringBuilder.length(), 33);
                return;
            default:
                return;
        }
    }

    private static String[] tokenizeTag(String str) {
        String trim = str.replace("\\s+", SPACE).trim();
        if (trim.length() == 0) {
            return null;
        }
        if (trim.contains(SPACE)) {
            trim = trim.substring(0, trim.indexOf(SPACE));
        }
        return trim.split("\\.");
    }

    /* access modifiers changed from: private */
    public static final class StartTag {
        public final String name;
        public final int position;

        public StartTag(String str, int i) {
            this.position = i;
            this.name = str;
        }
    }
}
