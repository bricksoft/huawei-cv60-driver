package com.google.android.exoplayer;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.media.PlaybackParams;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Surface;
import com.google.android.exoplayer.MediaCodecTrackRenderer;
import com.google.android.exoplayer.audio.AudioCapabilities;
import com.google.android.exoplayer.audio.AudioTrack;
import com.google.android.exoplayer.drm.DrmSessionManager;
import com.google.android.exoplayer.util.MimeTypes;
import java.nio.ByteBuffer;
import tv.danmaku.ijk.media.player.misc.IMediaFormat;

@TargetApi(16)
public class MediaCodecAudioTrackRenderer extends MediaCodecTrackRenderer implements MediaClock {
    public static final int MSG_SET_PLAYBACK_PARAMS = 2;
    public static final int MSG_SET_VOLUME = 1;
    private boolean allowPositionDiscontinuity;
    private int audioSessionId;
    private final AudioTrack audioTrack;
    private boolean audioTrackHasData;
    private long currentPositionUs;
    private final EventListener eventListener;
    private long lastFeedElapsedRealtimeMs;
    private boolean passthroughEnabled;
    private MediaFormat passthroughMediaFormat;
    private int pcmEncoding;

    public interface EventListener extends MediaCodecTrackRenderer.EventListener {
        void onAudioTrackInitializationError(AudioTrack.InitializationException initializationException);

        void onAudioTrackUnderrun(int i, long j, long j2);

        void onAudioTrackWriteError(AudioTrack.WriteException writeException);
    }

    public MediaCodecAudioTrackRenderer(SampleSource sampleSource, MediaCodecSelector mediaCodecSelector) {
        this(sampleSource, mediaCodecSelector, (DrmSessionManager) null, true);
    }

    public MediaCodecAudioTrackRenderer(SampleSource sampleSource, MediaCodecSelector mediaCodecSelector, DrmSessionManager drmSessionManager, boolean z) {
        this(sampleSource, mediaCodecSelector, drmSessionManager, z, null, null);
    }

    public MediaCodecAudioTrackRenderer(SampleSource sampleSource, MediaCodecSelector mediaCodecSelector, Handler handler, EventListener eventListener2) {
        this(sampleSource, mediaCodecSelector, null, true, handler, eventListener2);
    }

    public MediaCodecAudioTrackRenderer(SampleSource sampleSource, MediaCodecSelector mediaCodecSelector, DrmSessionManager drmSessionManager, boolean z, Handler handler, EventListener eventListener2) {
        this(sampleSource, mediaCodecSelector, drmSessionManager, z, handler, eventListener2, (AudioCapabilities) null, 3);
    }

    public MediaCodecAudioTrackRenderer(SampleSource sampleSource, MediaCodecSelector mediaCodecSelector, DrmSessionManager drmSessionManager, boolean z, Handler handler, EventListener eventListener2, AudioCapabilities audioCapabilities, int i) {
        this(new SampleSource[]{sampleSource}, mediaCodecSelector, drmSessionManager, z, handler, eventListener2, audioCapabilities, i);
    }

    public MediaCodecAudioTrackRenderer(SampleSource[] sampleSourceArr, MediaCodecSelector mediaCodecSelector, DrmSessionManager drmSessionManager, boolean z, Handler handler, EventListener eventListener2, AudioCapabilities audioCapabilities, int i) {
        super(sampleSourceArr, mediaCodecSelector, drmSessionManager, z, handler, eventListener2);
        this.eventListener = eventListener2;
        this.audioSessionId = 0;
        this.audioTrack = new AudioTrack(audioCapabilities, i);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer
    public boolean handlesTrack(MediaCodecSelector mediaCodecSelector, MediaFormat mediaFormat) {
        String str = mediaFormat.mimeType;
        if (!MimeTypes.isAudio(str)) {
            return false;
        }
        if (MimeTypes.AUDIO_UNKNOWN.equals(str) || ((allowPassthrough(str) && mediaCodecSelector.getPassthroughDecoderInfo() != null) || mediaCodecSelector.getDecoderInfo(str, false) != null)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer
    public DecoderInfo getDecoderInfo(MediaCodecSelector mediaCodecSelector, String str, boolean z) {
        DecoderInfo passthroughDecoderInfo;
        if (!allowPassthrough(str) || (passthroughDecoderInfo = mediaCodecSelector.getPassthroughDecoderInfo()) == null) {
            this.passthroughEnabled = false;
            return super.getDecoderInfo(mediaCodecSelector, str, z);
        }
        this.passthroughEnabled = true;
        return passthroughDecoderInfo;
    }

    /* access modifiers changed from: protected */
    public boolean allowPassthrough(String str) {
        return this.audioTrack.isPassthroughSupported(str);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer
    public void configureCodec(MediaCodec mediaCodec, boolean z, MediaFormat mediaFormat, MediaCrypto mediaCrypto) {
        String string = mediaFormat.getString(IMediaFormat.KEY_MIME);
        if (this.passthroughEnabled) {
            mediaFormat.setString(IMediaFormat.KEY_MIME, MimeTypes.AUDIO_RAW);
            mediaCodec.configure(mediaFormat, (Surface) null, mediaCrypto, 0);
            mediaFormat.setString(IMediaFormat.KEY_MIME, string);
            this.passthroughMediaFormat = mediaFormat;
            return;
        }
        mediaCodec.configure(mediaFormat, (Surface) null, mediaCrypto, 0);
        this.passthroughMediaFormat = null;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public MediaClock getMediaClock() {
        return this;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer
    public void onInputFormatChanged(MediaFormatHolder mediaFormatHolder) {
        super.onInputFormatChanged(mediaFormatHolder);
        this.pcmEncoding = MimeTypes.AUDIO_RAW.equals(mediaFormatHolder.format.mimeType) ? mediaFormatHolder.format.pcmEncoding : 2;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer
    public void onOutputFormatChanged(MediaCodec mediaCodec, MediaFormat mediaFormat) {
        boolean z = this.passthroughMediaFormat != null;
        String string = z ? this.passthroughMediaFormat.getString(IMediaFormat.KEY_MIME) : MimeTypes.AUDIO_RAW;
        if (z) {
            mediaFormat = this.passthroughMediaFormat;
        }
        this.audioTrack.configure(string, mediaFormat.getInteger("channel-count"), mediaFormat.getInteger("sample-rate"), this.pcmEncoding);
    }

    /* access modifiers changed from: protected */
    public void onAudioSessionId(int i) {
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer, com.google.android.exoplayer.TrackRenderer
    public void onStarted() {
        super.onStarted();
        this.audioTrack.play();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer, com.google.android.exoplayer.TrackRenderer
    public void onStopped() {
        this.audioTrack.pause();
        super.onStopped();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer, com.google.android.exoplayer.TrackRenderer
    public boolean isEnded() {
        return super.isEnded() && !this.audioTrack.hasPendingData();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer, com.google.android.exoplayer.TrackRenderer
    public boolean isReady() {
        return this.audioTrack.hasPendingData() || super.isReady();
    }

    @Override // com.google.android.exoplayer.MediaClock
    public long getPositionUs() {
        long currentPositionUs2 = this.audioTrack.getCurrentPositionUs(isEnded());
        if (currentPositionUs2 != Long.MIN_VALUE) {
            if (!this.allowPositionDiscontinuity) {
                currentPositionUs2 = Math.max(this.currentPositionUs, currentPositionUs2);
            }
            this.currentPositionUs = currentPositionUs2;
            this.allowPositionDiscontinuity = false;
        }
        return this.currentPositionUs;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer, com.google.android.exoplayer.SampleSourceTrackRenderer, com.google.android.exoplayer.TrackRenderer
    public void onDisabled() {
        this.audioSessionId = 0;
        try {
            this.audioTrack.release();
        } finally {
            super.onDisabled();
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer, com.google.android.exoplayer.SampleSourceTrackRenderer
    public void onDiscontinuity(long j) {
        super.onDiscontinuity(j);
        this.audioTrack.reset();
        this.currentPositionUs = j;
        this.allowPositionDiscontinuity = true;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer
    public boolean processOutputBuffer(long j, long j2, MediaCodec mediaCodec, ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo, int i, boolean z) {
        if (this.passthroughEnabled && (bufferInfo.flags & 2) != 0) {
            mediaCodec.releaseOutputBuffer(i, false);
            return true;
        } else if (z) {
            mediaCodec.releaseOutputBuffer(i, false);
            this.codecCounters.skippedOutputBufferCount++;
            this.audioTrack.handleDiscontinuity();
            return true;
        } else {
            if (!this.audioTrack.isInitialized()) {
                try {
                    if (this.audioSessionId != 0) {
                        this.audioTrack.initialize(this.audioSessionId);
                    } else {
                        this.audioSessionId = this.audioTrack.initialize();
                        onAudioSessionId(this.audioSessionId);
                    }
                    this.audioTrackHasData = false;
                    if (getState() == 3) {
                        this.audioTrack.play();
                    }
                } catch (AudioTrack.InitializationException e) {
                    notifyAudioTrackInitializationError(e);
                    throw new ExoPlaybackException(e);
                }
            } else {
                boolean z2 = this.audioTrackHasData;
                this.audioTrackHasData = this.audioTrack.hasPendingData();
                if (z2 && !this.audioTrackHasData && getState() == 3) {
                    long elapsedRealtime = SystemClock.elapsedRealtime() - this.lastFeedElapsedRealtimeMs;
                    long bufferSizeUs = this.audioTrack.getBufferSizeUs();
                    notifyAudioTrackUnderrun(this.audioTrack.getBufferSize(), bufferSizeUs == -1 ? -1 : bufferSizeUs / 1000, elapsedRealtime);
                }
            }
            try {
                int handleBuffer = this.audioTrack.handleBuffer(byteBuffer, bufferInfo.offset, bufferInfo.size, bufferInfo.presentationTimeUs);
                this.lastFeedElapsedRealtimeMs = SystemClock.elapsedRealtime();
                if ((handleBuffer & 1) != 0) {
                    handleAudioTrackDiscontinuity();
                    this.allowPositionDiscontinuity = true;
                }
                if ((handleBuffer & 2) == 0) {
                    return false;
                }
                mediaCodec.releaseOutputBuffer(i, false);
                this.codecCounters.renderedOutputBufferCount++;
                return true;
            } catch (AudioTrack.WriteException e2) {
                notifyAudioTrackWriteError(e2);
                throw new ExoPlaybackException(e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.MediaCodecTrackRenderer
    public void onOutputStreamEnded() {
        this.audioTrack.handleEndOfStream();
    }

    /* access modifiers changed from: protected */
    public void handleAudioTrackDiscontinuity() {
    }

    @Override // com.google.android.exoplayer.ExoPlayer.ExoPlayerComponent, com.google.android.exoplayer.TrackRenderer
    public void handleMessage(int i, Object obj) {
        switch (i) {
            case 1:
                this.audioTrack.setVolume(((Float) obj).floatValue());
                return;
            case 2:
                this.audioTrack.setPlaybackParams((PlaybackParams) obj);
                return;
            default:
                super.handleMessage(i, obj);
                return;
        }
    }

    private void notifyAudioTrackInitializationError(final AudioTrack.InitializationException initializationException) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                /* class com.google.android.exoplayer.MediaCodecAudioTrackRenderer.AnonymousClass1 */

                public void run() {
                    MediaCodecAudioTrackRenderer.this.eventListener.onAudioTrackInitializationError(initializationException);
                }
            });
        }
    }

    private void notifyAudioTrackWriteError(final AudioTrack.WriteException writeException) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                /* class com.google.android.exoplayer.MediaCodecAudioTrackRenderer.AnonymousClass2 */

                public void run() {
                    MediaCodecAudioTrackRenderer.this.eventListener.onAudioTrackWriteError(writeException);
                }
            });
        }
    }

    private void notifyAudioTrackUnderrun(final int i, final long j, final long j2) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                /* class com.google.android.exoplayer.MediaCodecAudioTrackRenderer.AnonymousClass3 */

                public void run() {
                    MediaCodecAudioTrackRenderer.this.eventListener.onAudioTrackUnderrun(i, j, j2);
                }
            });
        }
    }
}
