package com.google.android.exoplayer.hls;

import android.os.Handler;
import android.os.SystemClock;
import com.google.android.exoplayer.C;
import com.google.android.exoplayer.LoadControl;
import com.google.android.exoplayer.MediaFormat;
import com.google.android.exoplayer.MediaFormatHolder;
import com.google.android.exoplayer.SampleHolder;
import com.google.android.exoplayer.SampleSource;
import com.google.android.exoplayer.chunk.BaseChunkSampleSourceEventListener;
import com.google.android.exoplayer.chunk.Chunk;
import com.google.android.exoplayer.chunk.ChunkOperationHolder;
import com.google.android.exoplayer.chunk.Format;
import com.google.android.exoplayer.upstream.Loader;
import com.google.android.exoplayer.util.Assertions;
import com.google.android.exoplayer.util.MimeTypes;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;

public final class HlsSampleSource implements SampleSource, SampleSource.SampleSourceReader, Loader.Callback {
    public static final int DEFAULT_MIN_LOADABLE_RETRY_COUNT = 3;
    private static final long NO_RESET_PENDING = Long.MIN_VALUE;
    private static final int PRIMARY_TYPE_AUDIO = 2;
    private static final int PRIMARY_TYPE_NONE = 0;
    private static final int PRIMARY_TYPE_TEXT = 1;
    private static final int PRIMARY_TYPE_VIDEO = 3;
    private final int bufferSizeContribution;
    private final ChunkOperationHolder chunkOperationHolder;
    private final HlsChunkSource chunkSource;
    private int[] chunkSourceTrackIndices;
    private long currentLoadStartTimeMs;
    private Chunk currentLoadable;
    private IOException currentLoadableException;
    private int currentLoadableExceptionCount;
    private long currentLoadableExceptionTimestamp;
    private TsChunk currentTsLoadable;
    private Format downstreamFormat;
    private MediaFormat[] downstreamMediaFormats;
    private long downstreamPositionUs;
    private int enabledTrackCount;
    private final Handler eventHandler;
    private final EventListener eventListener;
    private final int eventSourceId;
    private boolean[] extractorTrackEnabledStates;
    private int[] extractorTrackIndices;
    private final LinkedList<HlsExtractorWrapper> extractors;
    private long lastSeekPositionUs;
    private final LoadControl loadControl;
    private boolean loadControlRegistered;
    private Loader loader;
    private boolean loadingFinished;
    private final int minLoadableRetryCount;
    private boolean[] pendingDiscontinuities;
    private long pendingResetPositionUs;
    private boolean prepared;
    private TsChunk previousTsLoadable;
    private int remainingReleaseCount;
    private int trackCount;
    private boolean[] trackEnabledStates;
    private MediaFormat[] trackFormats;

    public interface EventListener extends BaseChunkSampleSourceEventListener {
    }

    public HlsSampleSource(HlsChunkSource hlsChunkSource, LoadControl loadControl2, int i) {
        this(hlsChunkSource, loadControl2, i, null, null, 0);
    }

    public HlsSampleSource(HlsChunkSource hlsChunkSource, LoadControl loadControl2, int i, Handler handler, EventListener eventListener2, int i2) {
        this(hlsChunkSource, loadControl2, i, handler, eventListener2, i2, 3);
    }

    public HlsSampleSource(HlsChunkSource hlsChunkSource, LoadControl loadControl2, int i, Handler handler, EventListener eventListener2, int i2, int i3) {
        this.chunkSource = hlsChunkSource;
        this.loadControl = loadControl2;
        this.bufferSizeContribution = i;
        this.minLoadableRetryCount = i3;
        this.eventHandler = handler;
        this.eventListener = eventListener2;
        this.eventSourceId = i2;
        this.pendingResetPositionUs = Long.MIN_VALUE;
        this.extractors = new LinkedList<>();
        this.chunkOperationHolder = new ChunkOperationHolder();
    }

    @Override // com.google.android.exoplayer.SampleSource
    public SampleSource.SampleSourceReader register() {
        this.remainingReleaseCount++;
        return this;
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public boolean prepare(long j) {
        if (this.prepared) {
            return true;
        }
        if (!this.chunkSource.prepare()) {
            return false;
        }
        if (!this.extractors.isEmpty()) {
            while (true) {
                HlsExtractorWrapper first = this.extractors.getFirst();
                if (!first.isPrepared()) {
                    if (this.extractors.size() <= 1) {
                        break;
                    }
                    this.extractors.removeFirst().clear();
                } else {
                    buildTracks(first);
                    this.prepared = true;
                    maybeStartLoading();
                    return true;
                }
            }
        }
        if (this.loader == null) {
            this.loader = new Loader("Loader:HLS");
            this.loadControl.register(this, this.bufferSizeContribution);
            this.loadControlRegistered = true;
        }
        if (!this.loader.isLoading()) {
            this.pendingResetPositionUs = j;
            this.downstreamPositionUs = j;
        }
        maybeStartLoading();
        return false;
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public int getTrackCount() {
        Assertions.checkState(this.prepared);
        return this.trackCount;
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public MediaFormat getFormat(int i) {
        Assertions.checkState(this.prepared);
        return this.trackFormats[i];
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public void enable(int i, long j) {
        Assertions.checkState(this.prepared);
        setTrackEnabledState(i, true);
        this.downstreamMediaFormats[i] = null;
        this.pendingDiscontinuities[i] = false;
        this.downstreamFormat = null;
        boolean z = this.loadControlRegistered;
        if (!this.loadControlRegistered) {
            this.loadControl.register(this, this.bufferSizeContribution);
            this.loadControlRegistered = true;
        }
        if (this.chunkSource.isLive()) {
            j = 0;
        }
        int i2 = this.chunkSourceTrackIndices[i];
        if (i2 != -1 && i2 != this.chunkSource.getSelectedTrackIndex()) {
            this.chunkSource.selectTrack(i2);
            seekToInternal(j);
        } else if (this.enabledTrackCount == 1) {
            this.lastSeekPositionUs = j;
            if (!z || this.downstreamPositionUs != j) {
                this.downstreamPositionUs = j;
                restartFrom(j);
                return;
            }
            maybeStartLoading();
        }
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public void disable(int i) {
        Assertions.checkState(this.prepared);
        setTrackEnabledState(i, false);
        if (this.enabledTrackCount == 0) {
            this.chunkSource.reset();
            this.downstreamPositionUs = Long.MIN_VALUE;
            if (this.loadControlRegistered) {
                this.loadControl.unregister(this);
                this.loadControlRegistered = false;
            }
            if (this.loader.isLoading()) {
                this.loader.cancelLoading();
                return;
            }
            clearState();
            this.loadControl.trimAllocator();
        }
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public boolean continueBuffering(int i, long j) {
        Assertions.checkState(this.prepared);
        Assertions.checkState(this.trackEnabledStates[i]);
        this.downstreamPositionUs = j;
        if (!this.extractors.isEmpty()) {
            discardSamplesForDisabledTracks(getCurrentExtractor(), this.downstreamPositionUs);
        }
        maybeStartLoading();
        if (this.loadingFinished) {
            return true;
        }
        if (isPendingReset() || this.extractors.isEmpty()) {
            return false;
        }
        for (int i2 = 0; i2 < this.extractors.size(); i2++) {
            HlsExtractorWrapper hlsExtractorWrapper = this.extractors.get(i2);
            if (!hlsExtractorWrapper.isPrepared()) {
                return false;
            }
            if (hlsExtractorWrapper.hasSamples(this.extractorTrackIndices[i])) {
                return true;
            }
        }
        return false;
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public long readDiscontinuity(int i) {
        if (!this.pendingDiscontinuities[i]) {
            return Long.MIN_VALUE;
        }
        this.pendingDiscontinuities[i] = false;
        return this.lastSeekPositionUs;
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public int readData(int i, long j, MediaFormatHolder mediaFormatHolder, SampleHolder sampleHolder) {
        boolean z;
        int i2 = 0;
        Assertions.checkState(this.prepared);
        this.downstreamPositionUs = j;
        if (this.pendingDiscontinuities[i] || isPendingReset()) {
            return -2;
        }
        HlsExtractorWrapper currentExtractor = getCurrentExtractor();
        if (!currentExtractor.isPrepared()) {
            return -2;
        }
        Format format = currentExtractor.format;
        if (!format.equals(this.downstreamFormat)) {
            notifyDownstreamFormatChanged(format, currentExtractor.trigger, currentExtractor.startTimeUs);
        }
        this.downstreamFormat = format;
        if (this.extractors.size() > 1) {
            currentExtractor.configureSpliceTo(this.extractors.get(1));
        }
        int i3 = this.extractorTrackIndices[i];
        int i4 = 0;
        HlsExtractorWrapper hlsExtractorWrapper = currentExtractor;
        while (this.extractors.size() > i4 + 1 && !hlsExtractorWrapper.hasSamples(i3)) {
            i4++;
            hlsExtractorWrapper = this.extractors.get(i4);
            if (!hlsExtractorWrapper.isPrepared()) {
                return -2;
            }
        }
        MediaFormat mediaFormat = hlsExtractorWrapper.getMediaFormat(i3);
        if (mediaFormat != null) {
            if (!mediaFormat.equals(this.downstreamMediaFormats[i])) {
                mediaFormatHolder.format = mediaFormat;
                this.downstreamMediaFormats[i] = mediaFormat;
                return -4;
            }
            this.downstreamMediaFormats[i] = mediaFormat;
        }
        if (hlsExtractorWrapper.getSample(i3, sampleHolder)) {
            if (sampleHolder.timeUs < this.lastSeekPositionUs) {
                z = true;
            } else {
                z = false;
            }
            int i5 = sampleHolder.flags;
            if (z) {
                i2 = C.SAMPLE_FLAG_DECODE_ONLY;
            }
            sampleHolder.flags = i5 | i2;
            return -3;
        } else if (this.loadingFinished) {
            return -1;
        } else {
            return -2;
        }
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public void maybeThrowError() {
        if (this.currentLoadableException != null && this.currentLoadableExceptionCount > this.minLoadableRetryCount) {
            throw this.currentLoadableException;
        } else if (this.currentLoadable == null) {
            this.chunkSource.maybeThrowError();
        }
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public void seekToUs(long j) {
        Assertions.checkState(this.prepared);
        Assertions.checkState(this.enabledTrackCount > 0);
        if (this.chunkSource.isLive()) {
            j = 0;
        }
        long j2 = isPendingReset() ? this.pendingResetPositionUs : this.downstreamPositionUs;
        this.downstreamPositionUs = j;
        this.lastSeekPositionUs = j;
        if (j2 != j) {
            seekToInternal(j);
        }
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public long getBufferedPositionUs() {
        long j;
        Assertions.checkState(this.prepared);
        Assertions.checkState(this.enabledTrackCount > 0);
        if (isPendingReset()) {
            return this.pendingResetPositionUs;
        }
        if (this.loadingFinished) {
            return -3;
        }
        long largestParsedTimestampUs = this.extractors.getLast().getLargestParsedTimestampUs();
        if (this.extractors.size() > 1) {
            j = Math.max(largestParsedTimestampUs, this.extractors.get(this.extractors.size() - 2).getLargestParsedTimestampUs());
        } else {
            j = largestParsedTimestampUs;
        }
        return j == Long.MIN_VALUE ? this.downstreamPositionUs : j;
    }

    @Override // com.google.android.exoplayer.SampleSource.SampleSourceReader
    public void release() {
        Assertions.checkState(this.remainingReleaseCount > 0);
        int i = this.remainingReleaseCount - 1;
        this.remainingReleaseCount = i;
        if (i == 0 && this.loader != null) {
            if (this.loadControlRegistered) {
                this.loadControl.unregister(this);
                this.loadControlRegistered = false;
            }
            this.loader.release();
            this.loader = null;
        }
    }

    @Override // com.google.android.exoplayer.upstream.Loader.Callback
    public void onLoadCompleted(Loader.Loadable loadable) {
        boolean z = true;
        Assertions.checkState(loadable == this.currentLoadable);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        long j = elapsedRealtime - this.currentLoadStartTimeMs;
        this.chunkSource.onChunkLoadCompleted(this.currentLoadable);
        if (isTsChunk(this.currentLoadable)) {
            if (this.currentLoadable != this.currentTsLoadable) {
                z = false;
            }
            Assertions.checkState(z);
            this.previousTsLoadable = this.currentTsLoadable;
            notifyLoadCompleted(this.currentLoadable.bytesLoaded(), this.currentTsLoadable.type, this.currentTsLoadable.trigger, this.currentTsLoadable.format, this.currentTsLoadable.startTimeUs, this.currentTsLoadable.endTimeUs, elapsedRealtime, j);
        } else {
            notifyLoadCompleted(this.currentLoadable.bytesLoaded(), this.currentLoadable.type, this.currentLoadable.trigger, this.currentLoadable.format, -1, -1, elapsedRealtime, j);
        }
        clearCurrentLoadable();
        maybeStartLoading();
    }

    @Override // com.google.android.exoplayer.upstream.Loader.Callback
    public void onLoadCanceled(Loader.Loadable loadable) {
        notifyLoadCanceled(this.currentLoadable.bytesLoaded());
        if (this.enabledTrackCount > 0) {
            restartFrom(this.pendingResetPositionUs);
            return;
        }
        clearState();
        this.loadControl.trimAllocator();
    }

    @Override // com.google.android.exoplayer.upstream.Loader.Callback
    public void onLoadError(Loader.Loadable loadable, IOException iOException) {
        if (this.chunkSource.onChunkLoadError(this.currentLoadable, iOException)) {
            if (this.previousTsLoadable == null && !isPendingReset()) {
                this.pendingResetPositionUs = this.lastSeekPositionUs;
            }
            clearCurrentLoadable();
        } else {
            this.currentLoadableException = iOException;
            this.currentLoadableExceptionCount++;
            this.currentLoadableExceptionTimestamp = SystemClock.elapsedRealtime();
        }
        notifyLoadError(iOException);
        maybeStartLoading();
    }

    private void buildTracks(HlsExtractorWrapper hlsExtractorWrapper) {
        String str;
        int i;
        MediaFormat copyWithFixedTrackInfo;
        char c;
        char c2 = 0;
        int i2 = -1;
        int trackCount2 = hlsExtractorWrapper.getTrackCount();
        int i3 = 0;
        while (i3 < trackCount2) {
            String str2 = hlsExtractorWrapper.getMediaFormat(i3).mimeType;
            if (MimeTypes.isVideo(str2)) {
                c = 3;
            } else if (MimeTypes.isAudio(str2)) {
                c = 2;
            } else if (MimeTypes.isText(str2)) {
                c = 1;
            } else {
                c = 0;
            }
            if (c > c2) {
                i2 = i3;
            } else if (c != c2 || i2 == -1) {
                c = c2;
            } else {
                i2 = -1;
                c = c2;
            }
            i3++;
            c2 = c;
        }
        int trackCount3 = this.chunkSource.getTrackCount();
        boolean z = i2 != -1;
        this.trackCount = trackCount2;
        if (z) {
            this.trackCount += trackCount3 - 1;
        }
        this.trackFormats = new MediaFormat[this.trackCount];
        this.trackEnabledStates = new boolean[this.trackCount];
        this.pendingDiscontinuities = new boolean[this.trackCount];
        this.downstreamMediaFormats = new MediaFormat[this.trackCount];
        this.chunkSourceTrackIndices = new int[this.trackCount];
        this.extractorTrackIndices = new int[this.trackCount];
        this.extractorTrackEnabledStates = new boolean[trackCount2];
        long durationUs = this.chunkSource.getDurationUs();
        int i4 = 0;
        int i5 = 0;
        while (i5 < trackCount2) {
            MediaFormat copyWithDurationUs = hlsExtractorWrapper.getMediaFormat(i5).copyWithDurationUs(durationUs);
            if (MimeTypes.isAudio(copyWithDurationUs.mimeType)) {
                str = this.chunkSource.getMuxedAudioLanguage();
            } else if (MimeTypes.APPLICATION_EIA608.equals(copyWithDurationUs.mimeType)) {
                str = this.chunkSource.getMuxedCaptionLanguage();
            } else {
                str = null;
            }
            if (i5 == i2) {
                int i6 = 0;
                while (i6 < trackCount3) {
                    this.extractorTrackIndices[i4] = i5;
                    this.chunkSourceTrackIndices[i4] = i6;
                    Variant fixedTrackVariant = this.chunkSource.getFixedTrackVariant(i6);
                    MediaFormat[] mediaFormatArr = this.trackFormats;
                    int i7 = i4 + 1;
                    if (fixedTrackVariant == null) {
                        copyWithFixedTrackInfo = copyWithDurationUs.copyAsAdaptive(null);
                    } else {
                        copyWithFixedTrackInfo = copyWithFixedTrackInfo(copyWithDurationUs, fixedTrackVariant.format, str);
                    }
                    mediaFormatArr[i4] = copyWithFixedTrackInfo;
                    i6++;
                    i4 = i7;
                }
                i = i4;
            } else {
                this.extractorTrackIndices[i4] = i5;
                this.chunkSourceTrackIndices[i4] = -1;
                i = i4 + 1;
                this.trackFormats[i4] = copyWithDurationUs.copyWithLanguage(str);
            }
            i5++;
            i4 = i;
        }
    }

    private void setTrackEnabledState(int i, boolean z) {
        boolean z2 = false;
        int i2 = 1;
        Assertions.checkState(this.trackEnabledStates[i] != z);
        int i3 = this.extractorTrackIndices[i];
        if (this.extractorTrackEnabledStates[i3] != z) {
            z2 = true;
        }
        Assertions.checkState(z2);
        this.trackEnabledStates[i] = z;
        this.extractorTrackEnabledStates[i3] = z;
        int i4 = this.enabledTrackCount;
        if (!z) {
            i2 = -1;
        }
        this.enabledTrackCount = i4 + i2;
    }

    private static MediaFormat copyWithFixedTrackInfo(MediaFormat mediaFormat, Format format, String str) {
        return mediaFormat.copyWithFixedTrackInfo(format.id, format.bitrate, format.width == -1 ? -1 : format.width, format.height == -1 ? -1 : format.height, format.language == null ? str : format.language);
    }

    private void seekToInternal(long j) {
        this.lastSeekPositionUs = j;
        this.downstreamPositionUs = j;
        Arrays.fill(this.pendingDiscontinuities, true);
        this.chunkSource.seek();
        restartFrom(j);
    }

    private HlsExtractorWrapper getCurrentExtractor() {
        HlsExtractorWrapper hlsExtractorWrapper;
        HlsExtractorWrapper first = this.extractors.getFirst();
        while (true) {
            hlsExtractorWrapper = first;
            if (this.extractors.size() <= 1 || haveSamplesForEnabledTracks(hlsExtractorWrapper)) {
                return hlsExtractorWrapper;
            }
            this.extractors.removeFirst().clear();
            first = this.extractors.getFirst();
        }
        return hlsExtractorWrapper;
    }

    private void discardSamplesForDisabledTracks(HlsExtractorWrapper hlsExtractorWrapper, long j) {
        if (hlsExtractorWrapper.isPrepared()) {
            for (int i = 0; i < this.extractorTrackEnabledStates.length; i++) {
                if (!this.extractorTrackEnabledStates[i]) {
                    hlsExtractorWrapper.discardUntil(i, j);
                }
            }
        }
    }

    private boolean haveSamplesForEnabledTracks(HlsExtractorWrapper hlsExtractorWrapper) {
        if (!hlsExtractorWrapper.isPrepared()) {
            return false;
        }
        for (int i = 0; i < this.extractorTrackEnabledStates.length; i++) {
            if (this.extractorTrackEnabledStates[i] && hlsExtractorWrapper.hasSamples(i)) {
                return true;
            }
        }
        return false;
    }

    private void restartFrom(long j) {
        this.pendingResetPositionUs = j;
        this.loadingFinished = false;
        if (this.loader.isLoading()) {
            this.loader.cancelLoading();
            return;
        }
        clearState();
        maybeStartLoading();
    }

    private void clearState() {
        for (int i = 0; i < this.extractors.size(); i++) {
            this.extractors.get(i).clear();
        }
        this.extractors.clear();
        clearCurrentLoadable();
        this.previousTsLoadable = null;
    }

    private void clearCurrentLoadable() {
        this.currentTsLoadable = null;
        this.currentLoadable = null;
        this.currentLoadableException = null;
        this.currentLoadableExceptionCount = 0;
    }

    private void maybeStartLoading() {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        long nextLoadPositionUs = getNextLoadPositionUs();
        boolean z = this.currentLoadableException != null;
        boolean update = this.loadControl.update(this, this.downstreamPositionUs, nextLoadPositionUs, this.loader.isLoading() || z);
        if (z) {
            if (elapsedRealtime - this.currentLoadableExceptionTimestamp >= getRetryDelayMillis((long) this.currentLoadableExceptionCount)) {
                this.currentLoadableException = null;
                this.loader.startLoading(this.currentLoadable, this);
            }
        } else if (!this.loader.isLoading() && update) {
            if (!this.prepared || this.enabledTrackCount != 0) {
                this.chunkSource.getChunkOperation(this.previousTsLoadable, this.pendingResetPositionUs != Long.MIN_VALUE ? this.pendingResetPositionUs : this.downstreamPositionUs, this.chunkOperationHolder);
                boolean z2 = this.chunkOperationHolder.endOfStream;
                Chunk chunk = this.chunkOperationHolder.chunk;
                this.chunkOperationHolder.clear();
                if (z2) {
                    this.loadingFinished = true;
                    this.loadControl.update(this, this.downstreamPositionUs, -1, false);
                } else if (chunk != null) {
                    this.currentLoadStartTimeMs = elapsedRealtime;
                    this.currentLoadable = chunk;
                    if (isTsChunk(this.currentLoadable)) {
                        TsChunk tsChunk = (TsChunk) this.currentLoadable;
                        if (isPendingReset()) {
                            this.pendingResetPositionUs = Long.MIN_VALUE;
                        }
                        HlsExtractorWrapper hlsExtractorWrapper = tsChunk.extractorWrapper;
                        if (this.extractors.isEmpty() || this.extractors.getLast() != hlsExtractorWrapper) {
                            hlsExtractorWrapper.init(this.loadControl.getAllocator());
                            this.extractors.addLast(hlsExtractorWrapper);
                        }
                        notifyLoadStarted(tsChunk.dataSpec.length, tsChunk.type, tsChunk.trigger, tsChunk.format, tsChunk.startTimeUs, tsChunk.endTimeUs);
                        this.currentTsLoadable = tsChunk;
                    } else {
                        notifyLoadStarted(this.currentLoadable.dataSpec.length, this.currentLoadable.type, this.currentLoadable.trigger, this.currentLoadable.format, -1, -1);
                    }
                    this.loader.startLoading(this.currentLoadable, this);
                }
            }
        }
    }

    private long getNextLoadPositionUs() {
        if (isPendingReset()) {
            return this.pendingResetPositionUs;
        }
        if (this.loadingFinished || (this.prepared && this.enabledTrackCount == 0)) {
            return -1;
        }
        return this.currentTsLoadable != null ? this.currentTsLoadable.endTimeUs : this.previousTsLoadable.endTimeUs;
    }

    private boolean isTsChunk(Chunk chunk) {
        return chunk instanceof TsChunk;
    }

    private boolean isPendingReset() {
        return this.pendingResetPositionUs != Long.MIN_VALUE;
    }

    private long getRetryDelayMillis(long j) {
        return Math.min((j - 1) * 1000, (long) HlsChunkSource.DEFAULT_MIN_BUFFER_TO_SWITCH_UP_MS);
    }

    /* access modifiers changed from: package-private */
    public long usToMs(long j) {
        return j / 1000;
    }

    private void notifyLoadStarted(final long j, final int i, final int i2, final Format format, final long j2, final long j3) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                /* class com.google.android.exoplayer.hls.HlsSampleSource.AnonymousClass1 */

                public void run() {
                    HlsSampleSource.this.eventListener.onLoadStarted(HlsSampleSource.this.eventSourceId, j, i, i2, format, HlsSampleSource.this.usToMs(j2), HlsSampleSource.this.usToMs(j3));
                }
            });
        }
    }

    private void notifyLoadCompleted(final long j, final int i, final int i2, final Format format, final long j2, final long j3, final long j4, final long j5) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                /* class com.google.android.exoplayer.hls.HlsSampleSource.AnonymousClass2 */

                public void run() {
                    HlsSampleSource.this.eventListener.onLoadCompleted(HlsSampleSource.this.eventSourceId, j, i, i2, format, HlsSampleSource.this.usToMs(j2), HlsSampleSource.this.usToMs(j3), j4, j5);
                }
            });
        }
    }

    private void notifyLoadCanceled(final long j) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                /* class com.google.android.exoplayer.hls.HlsSampleSource.AnonymousClass3 */

                public void run() {
                    HlsSampleSource.this.eventListener.onLoadCanceled(HlsSampleSource.this.eventSourceId, j);
                }
            });
        }
    }

    private void notifyLoadError(final IOException iOException) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                /* class com.google.android.exoplayer.hls.HlsSampleSource.AnonymousClass4 */

                public void run() {
                    HlsSampleSource.this.eventListener.onLoadError(HlsSampleSource.this.eventSourceId, iOException);
                }
            });
        }
    }

    private void notifyDownstreamFormatChanged(final Format format, final int i, final long j) {
        if (this.eventHandler != null && this.eventListener != null) {
            this.eventHandler.post(new Runnable() {
                /* class com.google.android.exoplayer.hls.HlsSampleSource.AnonymousClass5 */

                public void run() {
                    HlsSampleSource.this.eventListener.onDownstreamFormatChanged(HlsSampleSource.this.eventSourceId, format, i, HlsSampleSource.this.usToMs(j));
                }
            });
        }
    }
}
