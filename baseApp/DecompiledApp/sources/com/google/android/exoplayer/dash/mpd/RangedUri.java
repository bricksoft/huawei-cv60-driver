package com.google.android.exoplayer.dash.mpd;

import android.net.Uri;
import com.google.android.exoplayer.util.Assertions;
import com.google.android.exoplayer.util.UriUtil;

public final class RangedUri {
    private final String baseUri;
    private int hashCode;
    public final long length;
    private final String referenceUri;
    public final long start;

    public RangedUri(String str, String str2, long j, long j2) {
        Assertions.checkArgument((str == null && str2 == null) ? false : true);
        this.baseUri = str;
        this.referenceUri = str2;
        this.start = j;
        this.length = j2;
    }

    public Uri getUri() {
        return UriUtil.resolveToUri(this.baseUri, this.referenceUri);
    }

    public String getUriString() {
        return UriUtil.resolve(this.baseUri, this.referenceUri);
    }

    public RangedUri attemptMerge(RangedUri rangedUri) {
        RangedUri rangedUri2 = null;
        long j = -1;
        if (rangedUri != null && getUriString().equals(rangedUri.getUriString())) {
            if (this.length != -1 && this.start + this.length == rangedUri.start) {
                String str = this.baseUri;
                String str2 = this.referenceUri;
                long j2 = this.start;
                if (rangedUri.length != -1) {
                    j = this.length + rangedUri.length;
                }
                rangedUri2 = new RangedUri(str, str2, j2, j);
            } else if (rangedUri.length != -1 && rangedUri.start + rangedUri.length == this.start) {
                String str3 = this.baseUri;
                String str4 = this.referenceUri;
                long j3 = rangedUri.start;
                if (this.length != -1) {
                    j = rangedUri.length + this.length;
                }
                rangedUri2 = new RangedUri(str3, str4, j3, j);
            }
        }
        return rangedUri2;
    }

    public int hashCode() {
        if (this.hashCode == 0) {
            this.hashCode = ((((((int) this.start) + 527) * 31) + ((int) this.length)) * 31) + getUriString().hashCode();
        }
        return this.hashCode;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        RangedUri rangedUri = (RangedUri) obj;
        return this.start == rangedUri.start && this.length == rangedUri.length && getUriString().equals(rangedUri.getUriString());
    }
}
