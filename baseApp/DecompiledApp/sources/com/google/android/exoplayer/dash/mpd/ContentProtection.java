package com.google.android.exoplayer.dash.mpd;

import com.google.android.exoplayer.drm.DrmInitData;
import com.google.android.exoplayer.util.Assertions;
import com.google.android.exoplayer.util.Util;
import java.util.UUID;

public class ContentProtection {
    public final DrmInitData.SchemeInitData data;
    public final String schemeUriId;
    public final UUID uuid;

    public ContentProtection(String str, UUID uuid2, DrmInitData.SchemeInitData schemeInitData) {
        this.schemeUriId = (String) Assertions.checkNotNull(str);
        this.uuid = uuid2;
        this.data = schemeInitData;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ContentProtection)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        ContentProtection contentProtection = (ContentProtection) obj;
        return this.schemeUriId.equals(contentProtection.schemeUriId) && Util.areEqual(this.uuid, contentProtection.uuid) && Util.areEqual(this.data, contentProtection.data);
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int hashCode = this.schemeUriId.hashCode() * 37;
        if (this.uuid != null) {
            i = this.uuid.hashCode();
        } else {
            i = 0;
        }
        int i3 = (i + hashCode) * 37;
        if (this.data != null) {
            i2 = this.data.hashCode();
        }
        return i3 + i2;
    }
}
