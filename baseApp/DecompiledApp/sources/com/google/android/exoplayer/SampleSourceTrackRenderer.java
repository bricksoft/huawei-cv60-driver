package com.google.android.exoplayer;

import com.google.android.exoplayer.MediaCodecUtil;
import com.google.android.exoplayer.SampleSource;
import java.io.IOException;
import java.util.Arrays;

public abstract class SampleSourceTrackRenderer extends TrackRenderer {
    private long durationUs;
    private SampleSource.SampleSourceReader enabledSource;
    private int enabledSourceTrackIndex;
    private int[] handledSourceIndices;
    private int[] handledSourceTrackIndices;
    private final SampleSource.SampleSourceReader[] sources;

    /* access modifiers changed from: protected */
    public abstract void doSomeWork(long j, long j2, boolean z);

    /* access modifiers changed from: protected */
    public abstract boolean handlesTrack(MediaFormat mediaFormat);

    /* access modifiers changed from: protected */
    public abstract void onDiscontinuity(long j);

    public SampleSourceTrackRenderer(SampleSource... sampleSourceArr) {
        this.sources = new SampleSource.SampleSourceReader[sampleSourceArr.length];
        for (int i = 0; i < sampleSourceArr.length; i++) {
            this.sources[i] = sampleSourceArr[i].register();
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public final boolean doPrepare(long j) {
        boolean z = true;
        for (int i = 0; i < this.sources.length; i++) {
            z &= this.sources[i].prepare(j);
        }
        if (!z) {
            return false;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.sources.length; i3++) {
            i2 += this.sources[i3].getTrackCount();
        }
        long j2 = 0;
        int i4 = 0;
        int[] iArr = new int[i2];
        int[] iArr2 = new int[i2];
        int length = this.sources.length;
        for (int i5 = 0; i5 < length; i5++) {
            SampleSource.SampleSourceReader sampleSourceReader = this.sources[i5];
            int trackCount = sampleSourceReader.getTrackCount();
            for (int i6 = 0; i6 < trackCount; i6++) {
                MediaFormat format = sampleSourceReader.getFormat(i6);
                try {
                    if (handlesTrack(format)) {
                        iArr[i4] = i5;
                        iArr2[i4] = i6;
                        i4++;
                        if (j2 != -1) {
                            long j3 = format.durationUs;
                            if (j3 == -1) {
                                j2 = -1;
                            } else if (j3 != -2) {
                                j2 = Math.max(j2, j3);
                            }
                        }
                    }
                } catch (MediaCodecUtil.DecoderQueryException e) {
                    throw new ExoPlaybackException(e);
                }
            }
        }
        this.durationUs = j2;
        this.handledSourceIndices = Arrays.copyOf(iArr, i4);
        this.handledSourceTrackIndices = Arrays.copyOf(iArr2, i4);
        return true;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public void onEnabled(int i, long j, boolean z) {
        long shiftInputPosition = shiftInputPosition(j);
        this.enabledSource = this.sources[this.handledSourceIndices[i]];
        this.enabledSourceTrackIndex = this.handledSourceTrackIndices[i];
        this.enabledSource.enable(this.enabledSourceTrackIndex, shiftInputPosition);
        onDiscontinuity(shiftInputPosition);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public final void seekTo(long j) {
        long shiftInputPosition = shiftInputPosition(j);
        this.enabledSource.seekToUs(shiftInputPosition);
        checkForDiscontinuity(shiftInputPosition);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public final void doSomeWork(long j, long j2) {
        long shiftInputPosition = shiftInputPosition(j);
        doSomeWork(checkForDiscontinuity(shiftInputPosition), j2, this.enabledSource.continueBuffering(this.enabledSourceTrackIndex, shiftInputPosition));
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public long getBufferedPositionUs() {
        return this.enabledSource.getBufferedPositionUs();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public long getDurationUs() {
        return this.durationUs;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public void maybeThrowError() {
        if (this.enabledSource != null) {
            maybeThrowError(this.enabledSource);
            return;
        }
        int length = this.sources.length;
        for (int i = 0; i < length; i++) {
            maybeThrowError(this.sources[i]);
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public void onDisabled() {
        this.enabledSource.disable(this.enabledSourceTrackIndex);
        this.enabledSource = null;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public void onReleased() {
        int length = this.sources.length;
        for (int i = 0; i < length; i++) {
            this.sources[i].release();
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public final int getTrackCount() {
        return this.handledSourceTrackIndices.length;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public final MediaFormat getFormat(int i) {
        return this.sources[this.handledSourceIndices[i]].getFormat(this.handledSourceTrackIndices[i]);
    }

    /* access modifiers changed from: protected */
    public long shiftInputPosition(long j) {
        return j;
    }

    /* access modifiers changed from: protected */
    public final int readSource(long j, MediaFormatHolder mediaFormatHolder, SampleHolder sampleHolder) {
        return this.enabledSource.readData(this.enabledSourceTrackIndex, j, mediaFormatHolder, sampleHolder);
    }

    private long checkForDiscontinuity(long j) {
        long readDiscontinuity = this.enabledSource.readDiscontinuity(this.enabledSourceTrackIndex);
        if (readDiscontinuity == Long.MIN_VALUE) {
            return j;
        }
        onDiscontinuity(readDiscontinuity);
        return readDiscontinuity;
    }

    private void maybeThrowError(SampleSource.SampleSourceReader sampleSourceReader) {
        try {
            sampleSourceReader.maybeThrowError();
        } catch (IOException e) {
            throw new ExoPlaybackException(e);
        }
    }
}
