package com.google.android.exoplayer.metadata;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.MediaFormat;
import com.google.android.exoplayer.MediaFormatHolder;
import com.google.android.exoplayer.SampleHolder;
import com.google.android.exoplayer.SampleSource;
import com.google.android.exoplayer.SampleSourceTrackRenderer;
import com.google.android.exoplayer.util.Assertions;
import java.io.IOException;

public final class MetadataTrackRenderer<T> extends SampleSourceTrackRenderer implements Handler.Callback {
    private static final int MSG_INVOKE_RENDERER = 0;
    private final MediaFormatHolder formatHolder;
    private boolean inputStreamEnded;
    private final Handler metadataHandler;
    private final MetadataParser<T> metadataParser;
    private final MetadataRenderer<T> metadataRenderer;
    private T pendingMetadata;
    private long pendingMetadataTimestamp;
    private final SampleHolder sampleHolder;

    public interface MetadataRenderer<T> {
        void onMetadata(T t);
    }

    public MetadataTrackRenderer(SampleSource sampleSource, MetadataParser<T> metadataParser2, MetadataRenderer<T> metadataRenderer2, Looper looper) {
        super(sampleSource);
        this.metadataParser = (MetadataParser) Assertions.checkNotNull(metadataParser2);
        this.metadataRenderer = (MetadataRenderer) Assertions.checkNotNull(metadataRenderer2);
        this.metadataHandler = looper == null ? null : new Handler(looper, this);
        this.formatHolder = new MediaFormatHolder();
        this.sampleHolder = new SampleHolder(1);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.SampleSourceTrackRenderer
    public boolean handlesTrack(MediaFormat mediaFormat) {
        return this.metadataParser.canParse(mediaFormat.mimeType);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.SampleSourceTrackRenderer
    public void onDiscontinuity(long j) {
        this.pendingMetadata = null;
        this.inputStreamEnded = false;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.SampleSourceTrackRenderer
    public void doSomeWork(long j, long j2, boolean z) {
        if (!this.inputStreamEnded && this.pendingMetadata == null) {
            this.sampleHolder.clearData();
            int readSource = readSource(j, this.formatHolder, this.sampleHolder);
            if (readSource == -3) {
                this.pendingMetadataTimestamp = this.sampleHolder.timeUs;
                try {
                    this.pendingMetadata = this.metadataParser.parse(this.sampleHolder.data.array(), this.sampleHolder.size);
                } catch (IOException e) {
                    throw new ExoPlaybackException(e);
                }
            } else if (readSource == -1) {
                this.inputStreamEnded = true;
            }
        }
        if (this.pendingMetadata != null && this.pendingMetadataTimestamp <= j) {
            invokeRenderer(this.pendingMetadata);
            this.pendingMetadata = null;
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.SampleSourceTrackRenderer, com.google.android.exoplayer.TrackRenderer
    public void onDisabled() {
        this.pendingMetadata = null;
        super.onDisabled();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.SampleSourceTrackRenderer, com.google.android.exoplayer.TrackRenderer
    public long getBufferedPositionUs() {
        return -3;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public boolean isEnded() {
        return this.inputStreamEnded;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.exoplayer.TrackRenderer
    public boolean isReady() {
        return true;
    }

    private void invokeRenderer(T t) {
        if (this.metadataHandler != null) {
            this.metadataHandler.obtainMessage(0, t).sendToTarget();
        } else {
            invokeRendererInternal(t);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.google.android.exoplayer.metadata.MetadataTrackRenderer<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                invokeRendererInternal(message.obj);
                return true;
            default:
                return false;
        }
    }

    private void invokeRendererInternal(T t) {
        this.metadataRenderer.onMetadata(t);
    }
}
