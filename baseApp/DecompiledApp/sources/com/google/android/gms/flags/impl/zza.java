package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import com.google.android.gms.internal.zzvv;
import java.util.concurrent.Callable;

public abstract class zza<T> {

    /* renamed from: com.google.android.gms.flags.impl.zza$zza  reason: collision with other inner class name */
    public static class C0072zza extends zza<Boolean> {
        public static Boolean zza(final SharedPreferences sharedPreferences, final String str, final Boolean bool) {
            return (Boolean) zzvv.zzb(new Callable<Boolean>() {
                /* class com.google.android.gms.flags.impl.zza.C0072zza.AnonymousClass1 */

                /* renamed from: zzwa */
                public Boolean call() {
                    return Boolean.valueOf(sharedPreferences.getBoolean(str, bool.booleanValue()));
                }
            });
        }
    }

    public static class zzb extends zza<Integer> {
        public static Integer zza(final SharedPreferences sharedPreferences, final String str, final Integer num) {
            return (Integer) zzvv.zzb(new Callable<Integer>() {
                /* class com.google.android.gms.flags.impl.zza.zzb.AnonymousClass1 */

                /* renamed from: zzbhg */
                public Integer call() {
                    return Integer.valueOf(sharedPreferences.getInt(str, num.intValue()));
                }
            });
        }
    }

    public static class zzc extends zza<Long> {
        public static Long zza(final SharedPreferences sharedPreferences, final String str, final Long l) {
            return (Long) zzvv.zzb(new Callable<Long>() {
                /* class com.google.android.gms.flags.impl.zza.zzc.AnonymousClass1 */

                /* renamed from: zzbhh */
                public Long call() {
                    return Long.valueOf(sharedPreferences.getLong(str, l.longValue()));
                }
            });
        }
    }

    public static class zzd extends zza<String> {
        public static String zza(final SharedPreferences sharedPreferences, final String str, final String str2) {
            return (String) zzvv.zzb(new Callable<String>() {
                /* class com.google.android.gms.flags.impl.zza.zzd.AnonymousClass1 */

                /* renamed from: zzaed */
                public String call() {
                    return sharedPreferences.getString(str, str2);
                }
            });
        }
    }
}
