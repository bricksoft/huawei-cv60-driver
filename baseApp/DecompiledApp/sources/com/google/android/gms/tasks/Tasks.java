package com.google.android.gms.tasks;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzaa;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class Tasks {

    private static final class zza implements zzb {
        private final CountDownLatch zzank;

        private zza() {
            this.zzank = new CountDownLatch(1);
        }

        public void await() {
            this.zzank.await();
        }

        public boolean await(long j, TimeUnit timeUnit) {
            return this.zzank.await(j, timeUnit);
        }

        @Override // com.google.android.gms.tasks.OnFailureListener
        public void onFailure(@NonNull Exception exc) {
            this.zzank.countDown();
        }

        @Override // com.google.android.gms.tasks.OnSuccessListener
        public void onSuccess(Object obj) {
            this.zzank.countDown();
        }
    }

    /* access modifiers changed from: package-private */
    public interface zzb extends OnFailureListener, OnSuccessListener<Object> {
    }

    /* access modifiers changed from: private */
    public static final class zzc implements zzb {
        private final zzh<Void> aMS;
        private Exception aMX;
        private final int aMZ;
        private int aNa;
        private int aNb;
        private final Object zzako = new Object();

        public zzc(int i, zzh<Void> zzh) {
            this.aMZ = i;
            this.aMS = zzh;
        }

        private void zzclj() {
            if (this.aNa + this.aNb != this.aMZ) {
                return;
            }
            if (this.aMX == null) {
                this.aMS.setResult(null);
                return;
            }
            zzh<Void> zzh = this.aMS;
            int i = this.aNb;
            zzh.setException(new ExecutionException(new StringBuilder(54).append(i).append(" out of ").append(this.aMZ).append(" underlying tasks failed").toString(), this.aMX));
        }

        @Override // com.google.android.gms.tasks.OnFailureListener
        public void onFailure(@NonNull Exception exc) {
            synchronized (this.zzako) {
                this.aNb++;
                this.aMX = exc;
                zzclj();
            }
        }

        @Override // com.google.android.gms.tasks.OnSuccessListener
        public void onSuccess(Object obj) {
            synchronized (this.zzako) {
                this.aNa++;
                zzclj();
            }
        }
    }

    private Tasks() {
    }

    public static <TResult> TResult await(@NonNull Task<TResult> task) {
        zzaa.zzawk();
        zzaa.zzb(task, "Task must not be null");
        if (task.isComplete()) {
            return (TResult) zzb(task);
        }
        zza zza2 = new zza();
        zza(task, zza2);
        zza2.await();
        return (TResult) zzb(task);
    }

    public static <TResult> TResult await(@NonNull Task<TResult> task, long j, @NonNull TimeUnit timeUnit) {
        zzaa.zzawk();
        zzaa.zzb(task, "Task must not be null");
        zzaa.zzb(timeUnit, "TimeUnit must not be null");
        if (task.isComplete()) {
            return (TResult) zzb(task);
        }
        zza zza2 = new zza();
        zza(task, zza2);
        if (zza2.await(j, timeUnit)) {
            return (TResult) zzb(task);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    public static <TResult> Task<TResult> call(@NonNull Callable<TResult> callable) {
        return call(TaskExecutors.MAIN_THREAD, callable);
    }

    public static <TResult> Task<TResult> call(@NonNull Executor executor, @NonNull final Callable<TResult> callable) {
        zzaa.zzb(executor, "Executor must not be null");
        zzaa.zzb(callable, "Callback must not be null");
        final zzh zzh = new zzh();
        executor.execute(new Runnable() {
            /* class com.google.android.gms.tasks.Tasks.AnonymousClass1 */

            public void run() {
                try {
                    zzh.this.setResult(callable.call());
                } catch (Exception e) {
                    zzh.this.setException(e);
                }
            }
        });
        return zzh;
    }

    public static <TResult> Task<TResult> forException(@NonNull Exception exc) {
        zzh zzh = new zzh();
        zzh.setException(exc);
        return zzh;
    }

    public static <TResult> Task<TResult> forResult(TResult tresult) {
        zzh zzh = new zzh();
        zzh.setResult(tresult);
        return zzh;
    }

    public static Task<Void> whenAll(Collection<? extends Task<?>> collection) {
        if (collection.isEmpty()) {
            return forResult(null);
        }
        Iterator<? extends Task<?>> it = collection.iterator();
        while (it.hasNext()) {
            if (((Task) it.next()) == null) {
                throw new NullPointerException("null tasks are not accepted");
            }
        }
        zzh zzh = new zzh();
        zzc zzc2 = new zzc(collection.size(), zzh);
        Iterator<? extends Task<?>> it2 = collection.iterator();
        while (it2.hasNext()) {
            zza((Task) it2.next(), zzc2);
        }
        return zzh;
    }

    public static Task<Void> whenAll(Task<?>... taskArr) {
        return taskArr.length == 0 ? forResult(null) : whenAll(Arrays.asList(taskArr));
    }

    private static void zza(Task<?> task, zzb zzb2) {
        task.addOnSuccessListener(TaskExecutors.aMT, zzb2);
        task.addOnFailureListener(TaskExecutors.aMT, zzb2);
    }

    private static <TResult> TResult zzb(Task<TResult> task) {
        if (task.isSuccessful()) {
            return task.getResult();
        }
        throw new ExecutionException(task.getException());
    }
}
