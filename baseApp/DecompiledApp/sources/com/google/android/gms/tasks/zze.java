package com.google.android.gms.tasks;

import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

/* access modifiers changed from: package-private */
public class zze<TResult> implements zzf<TResult> {
    private final Executor aEQ;
    private OnSuccessListener<? super TResult> aMO;
    private final Object zzako = new Object();

    public zze(@NonNull Executor executor, @NonNull OnSuccessListener<? super TResult> onSuccessListener) {
        this.aEQ = executor;
        this.aMO = onSuccessListener;
    }

    @Override // com.google.android.gms.tasks.zzf
    public void cancel() {
        synchronized (this.zzako) {
            this.aMO = null;
        }
    }

    @Override // com.google.android.gms.tasks.zzf
    public void onComplete(@NonNull final Task<TResult> task) {
        if (task.isSuccessful()) {
            synchronized (this.zzako) {
                if (this.aMO != null) {
                    this.aEQ.execute(new Runnable() {
                        /* class com.google.android.gms.tasks.zze.AnonymousClass1 */

                        /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: com.google.android.gms.tasks.OnSuccessListener */
                        /* JADX WARN: Multi-variable type inference failed */
                        public void run() {
                            synchronized (zze.this.zzako) {
                                if (zze.this.aMO != null) {
                                    zze.this.aMO.onSuccess(task.getResult());
                                }
                            }
                        }
                    });
                }
            }
        }
    }
}
