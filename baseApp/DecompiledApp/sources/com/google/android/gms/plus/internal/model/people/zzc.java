package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import java.util.HashSet;
import java.util.Set;

public class zzc implements Parcelable.Creator<PersonEntity.CoverEntity> {
    static void zza(PersonEntity.CoverEntity coverEntity, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        Set<Integer> set = coverEntity.aBr;
        if (set.contains(1)) {
            zzb.zzc(parcel, 1, coverEntity.mVersionCode);
        }
        if (set.contains(2)) {
            zzb.zza(parcel, 2, (Parcelable) coverEntity.aBN, i, true);
        }
        if (set.contains(3)) {
            zzb.zza(parcel, 3, (Parcelable) coverEntity.aBO, i, true);
        }
        if (set.contains(4)) {
            zzb.zzc(parcel, 4, coverEntity.aBP);
        }
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzrm */
    public PersonEntity.CoverEntity createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        PersonEntity.CoverEntity.CoverPhotoEntity coverPhotoEntity = null;
        PersonEntity.CoverEntity.CoverInfoEntity coverInfoEntity = null;
        int i2 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i2 = zza.zzg(parcel, zzcq);
                    hashSet.add(1);
                    break;
                case 2:
                    hashSet.add(2);
                    coverInfoEntity = (PersonEntity.CoverEntity.CoverInfoEntity) zza.zza(parcel, zzcq, PersonEntity.CoverEntity.CoverInfoEntity.CREATOR);
                    break;
                case 3:
                    hashSet.add(3);
                    coverPhotoEntity = (PersonEntity.CoverEntity.CoverPhotoEntity) zza.zza(parcel, zzcq, PersonEntity.CoverEntity.CoverPhotoEntity.CREATOR);
                    break;
                case 4:
                    i = zza.zzg(parcel, zzcq);
                    hashSet.add(4);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new PersonEntity.CoverEntity(hashSet, i2, coverInfoEntity, coverPhotoEntity, i);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzzf */
    public PersonEntity.CoverEntity[] newArray(int i) {
        return new PersonEntity.CoverEntity[i];
    }
}
