package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import java.util.HashSet;
import java.util.Set;

public class zzj implements Parcelable.Creator<PersonEntity.UrlsEntity> {
    static void zza(PersonEntity.UrlsEntity urlsEntity, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        Set<Integer> set = urlsEntity.aBr;
        if (set.contains(1)) {
            zzb.zzc(parcel, 1, urlsEntity.mVersionCode);
        }
        if (set.contains(3)) {
            zzb.zzc(parcel, 3, urlsEntity.zzccn());
        }
        if (set.contains(4)) {
            zzb.zza(parcel, 4, urlsEntity.mValue, true);
        }
        if (set.contains(5)) {
            zzb.zza(parcel, 5, urlsEntity.ce, true);
        }
        if (set.contains(6)) {
            zzb.zzc(parcel, 6, urlsEntity.nV);
        }
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzrt */
    public PersonEntity.UrlsEntity createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str = null;
        int i2 = 0;
        String str2 = null;
        int i3 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i3 = zza.zzg(parcel, zzcq);
                    hashSet.add(1);
                    break;
                case 2:
                default:
                    zza.zzb(parcel, zzcq);
                    break;
                case 3:
                    i = zza.zzg(parcel, zzcq);
                    hashSet.add(3);
                    break;
                case 4:
                    str = zza.zzq(parcel, zzcq);
                    hashSet.add(4);
                    break;
                case 5:
                    str2 = zza.zzq(parcel, zzcq);
                    hashSet.add(5);
                    break;
                case 6:
                    i2 = zza.zzg(parcel, zzcq);
                    hashSet.add(6);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new PersonEntity.UrlsEntity(hashSet, i3, str2, i2, str, i);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzzm */
    public PersonEntity.UrlsEntity[] newArray(int i) {
        return new PersonEntity.UrlsEntity[i];
    }
}
