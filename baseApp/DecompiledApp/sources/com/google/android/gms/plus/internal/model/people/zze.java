package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import java.util.HashSet;
import java.util.Set;

public class zze implements Parcelable.Creator<PersonEntity.CoverEntity.CoverPhotoEntity> {
    static void zza(PersonEntity.CoverEntity.CoverPhotoEntity coverPhotoEntity, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        Set<Integer> set = coverPhotoEntity.aBr;
        if (set.contains(1)) {
            zzb.zzc(parcel, 1, coverPhotoEntity.mVersionCode);
        }
        if (set.contains(2)) {
            zzb.zzc(parcel, 2, coverPhotoEntity.zzaki);
        }
        if (set.contains(3)) {
            zzb.zza(parcel, 3, coverPhotoEntity.zzae, true);
        }
        if (set.contains(4)) {
            zzb.zzc(parcel, 4, coverPhotoEntity.zzakh);
        }
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzro */
    public PersonEntity.CoverEntity.CoverPhotoEntity createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i3 = zza.zzg(parcel, zzcq);
                    hashSet.add(1);
                    break;
                case 2:
                    i2 = zza.zzg(parcel, zzcq);
                    hashSet.add(2);
                    break;
                case 3:
                    str = zza.zzq(parcel, zzcq);
                    hashSet.add(3);
                    break;
                case 4:
                    i = zza.zzg(parcel, zzcq);
                    hashSet.add(4);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new PersonEntity.CoverEntity.CoverPhotoEntity(hashSet, i3, i2, str, i);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzzh */
    public PersonEntity.CoverEntity.CoverPhotoEntity[] newArray(int i) {
        return new PersonEntity.CoverEntity.CoverPhotoEntity[i];
    }
}
