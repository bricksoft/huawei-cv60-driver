package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import java.util.HashSet;
import java.util.Set;

public class zzi implements Parcelable.Creator<PersonEntity.PlacesLivedEntity> {
    static void zza(PersonEntity.PlacesLivedEntity placesLivedEntity, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        Set<Integer> set = placesLivedEntity.aBr;
        if (set.contains(1)) {
            zzb.zzc(parcel, 1, placesLivedEntity.mVersionCode);
        }
        if (set.contains(2)) {
            zzb.zza(parcel, 2, placesLivedEntity.aBZ);
        }
        if (set.contains(3)) {
            zzb.zza(parcel, 3, placesLivedEntity.mValue, true);
        }
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzrs */
    public PersonEntity.PlacesLivedEntity createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        boolean z = false;
        int i = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i = zza.zzg(parcel, zzcq);
                    hashSet.add(1);
                    break;
                case 2:
                    z = zza.zzc(parcel, zzcq);
                    hashSet.add(2);
                    break;
                case 3:
                    str = zza.zzq(parcel, zzcq);
                    hashSet.add(3);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new PersonEntity.PlacesLivedEntity(hashSet, i, z, str);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzzl */
    public PersonEntity.PlacesLivedEntity[] newArray(int i) {
        return new PersonEntity.PlacesLivedEntity[i];
    }
}
