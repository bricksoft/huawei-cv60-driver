package com.google.android.gms.plus.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh implements Parcelable.Creator<PlusSession> {
    static void zza(PlusSession plusSession, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zza(parcel, 1, plusSession.getAccountName(), false);
        zzb.zza(parcel, 2, plusSession.zzcbv(), false);
        zzb.zza(parcel, 3, plusSession.zzcbw(), false);
        zzb.zza(parcel, 4, plusSession.zzcbx(), false);
        zzb.zza(parcel, 5, plusSession.zzcby(), false);
        zzb.zza(parcel, 6, plusSession.zzcbz(), false);
        zzb.zza(parcel, 7, plusSession.zzapc(), false);
        zzb.zzc(parcel, 1000, plusSession.getVersionCode());
        zzb.zza(parcel, 8, plusSession.zzcca(), false);
        zzb.zza(parcel, 9, (Parcelable) plusSession.zzccb(), i, false);
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzrj */
    public PlusSession createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        PlusCommonExtras plusCommonExtras = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String[] strArr = null;
        String[] strArr2 = null;
        String[] strArr3 = null;
        String str5 = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    str5 = zza.zzq(parcel, zzcq);
                    break;
                case 2:
                    strArr3 = zza.zzac(parcel, zzcq);
                    break;
                case 3:
                    strArr2 = zza.zzac(parcel, zzcq);
                    break;
                case 4:
                    strArr = zza.zzac(parcel, zzcq);
                    break;
                case 5:
                    str4 = zza.zzq(parcel, zzcq);
                    break;
                case 6:
                    str3 = zza.zzq(parcel, zzcq);
                    break;
                case 7:
                    str2 = zza.zzq(parcel, zzcq);
                    break;
                case 8:
                    str = zza.zzq(parcel, zzcq);
                    break;
                case 9:
                    plusCommonExtras = (PlusCommonExtras) zza.zza(parcel, zzcq, PlusCommonExtras.CREATOR);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new PlusSession(i, str5, strArr3, strArr2, strArr, str4, str3, str2, str, plusCommonExtras);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzzc */
    public PlusSession[] newArray(int i) {
        return new PlusSession[i];
    }
}
