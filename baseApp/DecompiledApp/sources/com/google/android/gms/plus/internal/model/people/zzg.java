package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import java.util.HashSet;
import java.util.Set;

public class zzg implements Parcelable.Creator<PersonEntity.NameEntity> {
    static void zza(PersonEntity.NameEntity nameEntity, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        Set<Integer> set = nameEntity.aBr;
        if (set.contains(1)) {
            zzb.zzc(parcel, 1, nameEntity.mVersionCode);
        }
        if (set.contains(2)) {
            zzb.zza(parcel, 2, nameEntity.it, true);
        }
        if (set.contains(3)) {
            zzb.zza(parcel, 3, nameEntity.aBS, true);
        }
        if (set.contains(4)) {
            zzb.zza(parcel, 4, nameEntity.is, true);
        }
        if (set.contains(5)) {
            zzb.zza(parcel, 5, nameEntity.aBT, true);
        }
        if (set.contains(6)) {
            zzb.zza(parcel, 6, nameEntity.aBU, true);
        }
        if (set.contains(7)) {
            zzb.zza(parcel, 7, nameEntity.aBV, true);
        }
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzrq */
    public PersonEntity.NameEntity createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i = zza.zzg(parcel, zzcq);
                    hashSet.add(1);
                    break;
                case 2:
                    str6 = zza.zzq(parcel, zzcq);
                    hashSet.add(2);
                    break;
                case 3:
                    str5 = zza.zzq(parcel, zzcq);
                    hashSet.add(3);
                    break;
                case 4:
                    str4 = zza.zzq(parcel, zzcq);
                    hashSet.add(4);
                    break;
                case 5:
                    str3 = zza.zzq(parcel, zzcq);
                    hashSet.add(5);
                    break;
                case 6:
                    str2 = zza.zzq(parcel, zzcq);
                    hashSet.add(6);
                    break;
                case 7:
                    str = zza.zzq(parcel, zzcq);
                    hashSet.add(7);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new PersonEntity.NameEntity(hashSet, i, str6, str5, str4, str3, str2, str);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzzj */
    public PersonEntity.NameEntity[] newArray(int i) {
        return new PersonEntity.NameEntity[i];
    }
}
