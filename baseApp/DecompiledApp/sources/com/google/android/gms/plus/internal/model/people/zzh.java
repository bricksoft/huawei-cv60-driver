package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import java.util.HashSet;
import java.util.Set;

public class zzh implements Parcelable.Creator<PersonEntity.OrganizationsEntity> {
    static void zza(PersonEntity.OrganizationsEntity organizationsEntity, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        Set<Integer> set = organizationsEntity.aBr;
        if (set.contains(1)) {
            zzb.zzc(parcel, 1, organizationsEntity.mVersionCode);
        }
        if (set.contains(2)) {
            zzb.zza(parcel, 2, organizationsEntity.aBW, true);
        }
        if (set.contains(3)) {
            zzb.zza(parcel, 3, organizationsEntity.cg, true);
        }
        if (set.contains(4)) {
            zzb.zza(parcel, 4, organizationsEntity.aBX, true);
        }
        if (set.contains(5)) {
            zzb.zza(parcel, 5, organizationsEntity.aBY, true);
        }
        if (set.contains(6)) {
            zzb.zza(parcel, 6, organizationsEntity.mName, true);
        }
        if (set.contains(7)) {
            zzb.zza(parcel, 7, organizationsEntity.aBZ);
        }
        if (set.contains(8)) {
            zzb.zza(parcel, 8, organizationsEntity.aCa, true);
        }
        if (set.contains(9)) {
            zzb.zza(parcel, 9, organizationsEntity.JB, true);
        }
        if (set.contains(10)) {
            zzb.zzc(parcel, 10, organizationsEntity.nV);
        }
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzrr */
    public PersonEntity.OrganizationsEntity createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str = null;
        String str2 = null;
        boolean z = false;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        int i2 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i2 = zza.zzg(parcel, zzcq);
                    hashSet.add(1);
                    break;
                case 2:
                    str7 = zza.zzq(parcel, zzcq);
                    hashSet.add(2);
                    break;
                case 3:
                    str6 = zza.zzq(parcel, zzcq);
                    hashSet.add(3);
                    break;
                case 4:
                    str5 = zza.zzq(parcel, zzcq);
                    hashSet.add(4);
                    break;
                case 5:
                    str4 = zza.zzq(parcel, zzcq);
                    hashSet.add(5);
                    break;
                case 6:
                    str3 = zza.zzq(parcel, zzcq);
                    hashSet.add(6);
                    break;
                case 7:
                    z = zza.zzc(parcel, zzcq);
                    hashSet.add(7);
                    break;
                case 8:
                    str2 = zza.zzq(parcel, zzcq);
                    hashSet.add(8);
                    break;
                case 9:
                    str = zza.zzq(parcel, zzcq);
                    hashSet.add(9);
                    break;
                case 10:
                    i = zza.zzg(parcel, zzcq);
                    hashSet.add(10);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new PersonEntity.OrganizationsEntity(hashSet, i2, str7, str6, str5, str4, str3, z, str2, str, i);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzzk */
    public PersonEntity.OrganizationsEntity[] newArray(int i) {
        return new PersonEntity.OrganizationsEntity[i];
    }
}
