package com.google.android.gms.plus.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzq;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import com.google.android.gms.plus.internal.zzd;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

public class zze extends zzj<zzd> {
    private Person aAV;
    private final PlusSession aAW;

    /* access modifiers changed from: package-private */
    public static final class zza implements People.LoadPeopleResult {
        private final String aAX;
        private final PersonBuffer aAY;
        private final Status hv;

        public zza(Status status, DataHolder dataHolder, String str) {
            this.hv = status;
            this.aAX = str;
            this.aAY = dataHolder != null ? new PersonBuffer(dataHolder) : null;
        }

        @Override // com.google.android.gms.plus.People.LoadPeopleResult
        public String getNextPageToken() {
            return this.aAX;
        }

        @Override // com.google.android.gms.plus.People.LoadPeopleResult
        public PersonBuffer getPersonBuffer() {
            return this.aAY;
        }

        @Override // com.google.android.gms.common.api.Result
        public Status getStatus() {
            return this.hv;
        }

        @Override // com.google.android.gms.common.api.Releasable
        public void release() {
            if (this.aAY != null) {
                this.aAY.release();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public static final class zzb extends zza {
        private final zzqo.zzb<People.LoadPeopleResult> alb;

        public zzb(zzqo.zzb<People.LoadPeopleResult> zzb) {
            this.alb = zzb;
        }

        @Override // com.google.android.gms.plus.internal.zza, com.google.android.gms.plus.internal.zzb
        public void zza(DataHolder dataHolder, String str) {
            Status status = new Status(dataHolder.getStatusCode(), null, dataHolder.zzaui() != null ? (PendingIntent) dataHolder.zzaui().getParcelable("pendingIntent") : null);
            if (!status.isSuccess() && dataHolder != null) {
                if (!dataHolder.isClosed()) {
                    dataHolder.close();
                }
                dataHolder = null;
            }
            this.alb.setResult(new zza(status, dataHolder, str));
        }
    }

    static final class zzc extends zza {
        private final zzqo.zzb<Status> alb;

        public zzc(zzqo.zzb<Status> zzb) {
            this.alb = zzb;
        }

        @Override // com.google.android.gms.plus.internal.zza, com.google.android.gms.plus.internal.zzb
        public void zzk(int i, Bundle bundle) {
            this.alb.setResult(new Status(i, null, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null));
        }
    }

    public zze(Context context, Looper looper, zzf zzf, PlusSession plusSession, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 2, zzf, connectionCallbacks, onConnectionFailedListener);
        this.aAW = plusSession;
    }

    public static boolean zze(Set<Scope> set) {
        if (set == null || set.isEmpty()) {
            return false;
        }
        return set.size() != 1 || !set.contains(new Scope("plus_one_placeholder_scope"));
    }

    public String getAccountName() {
        zzavf();
        try {
            return ((zzd) zzavg()).getAccountName();
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public zzq zza(zzqo.zzb<People.LoadPeopleResult> zzb2, int i, String str) {
        zzavf();
        zzb zzb3 = new zzb(zzb2);
        try {
            return ((zzd) zzavg()).zza(zzb3, 1, i, -1, str);
        } catch (RemoteException e) {
            zzb3.zza(DataHolder.zzgb(8), (String) null);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public void zza(int i, IBinder iBinder, Bundle bundle, int i2) {
        if (i == 0 && bundle != null && bundle.containsKey("loaded_person")) {
            this.aAV = PersonEntity.zzaf(bundle.getByteArray("loaded_person"));
        }
        super.zza(i, iBinder, bundle, i2);
    }

    public void zza(zzqo.zzb<People.LoadPeopleResult> zzb2, Collection<String> collection) {
        zzavf();
        zzb zzb3 = new zzb(zzb2);
        try {
            ((zzd) zzavg()).zza(zzb3, new ArrayList(collection));
        } catch (RemoteException e) {
            zzb3.zza(DataHolder.zzgb(8), (String) null);
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public Bundle zzahv() {
        Bundle zzccc = this.aAW.zzccc();
        zzccc.putStringArray("request_visible_actions", this.aAW.zzcbw());
        zzccc.putString("auth_package", this.aAW.zzcby());
        return zzccc;
    }

    @Override // com.google.android.gms.common.api.Api.zze, com.google.android.gms.common.internal.zze
    public boolean zzain() {
        return zze(zzawb().zzc(Plus.API));
    }

    public void zzcbq() {
        zzavf();
        try {
            this.aAV = null;
            ((zzd) zzavg()).zzcbq();
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public Person zzcbs() {
        zzavf();
        return this.aAV;
    }

    public void zzd(zzqo.zzb<People.LoadPeopleResult> zzb2, String[] strArr) {
        zza(zzb2, Arrays.asList(strArr));
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjx() {
        return "com.google.android.gms.plus.service.START";
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjy() {
        return "com.google.android.gms.plus.internal.IPlusService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzkn */
    public zzd zzh(IBinder iBinder) {
        return zzd.zza.zzkm(iBinder);
    }

    public zzq zzu(zzqo.zzb<People.LoadPeopleResult> zzb2, String str) {
        return zza(zzb2, 0, str);
    }

    public void zzv(zzqo.zzb<People.LoadPeopleResult> zzb2) {
        zzavf();
        zzb zzb3 = new zzb(zzb2);
        try {
            ((zzd) zzavg()).zza(zzb3, 2, 1, -1, null);
        } catch (RemoteException e) {
            zzb3.zza(DataHolder.zzgb(8), (String) null);
        }
    }

    public void zzw(zzqo.zzb<Status> zzb2) {
        zzavf();
        zzcbq();
        zzc zzc2 = new zzc(zzb2);
        try {
            ((zzd) zzavg()).zzb(zzc2);
        } catch (RemoteException e) {
            zzc2.zzk(8, null);
        }
    }
}
