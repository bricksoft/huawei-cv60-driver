package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import java.util.HashSet;
import java.util.Set;

public class zzd implements Parcelable.Creator<PersonEntity.CoverEntity.CoverInfoEntity> {
    static void zza(PersonEntity.CoverEntity.CoverInfoEntity coverInfoEntity, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        Set<Integer> set = coverInfoEntity.aBr;
        if (set.contains(1)) {
            zzb.zzc(parcel, 1, coverInfoEntity.mVersionCode);
        }
        if (set.contains(2)) {
            zzb.zzc(parcel, 2, coverInfoEntity.aBQ);
        }
        if (set.contains(3)) {
            zzb.zzc(parcel, 3, coverInfoEntity.aBR);
        }
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzrn */
    public PersonEntity.CoverEntity.CoverInfoEntity createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i3 = zza.zzg(parcel, zzcq);
                    hashSet.add(1);
                    break;
                case 2:
                    i2 = zza.zzg(parcel, zzcq);
                    hashSet.add(2);
                    break;
                case 3:
                    i = zza.zzg(parcel, zzcq);
                    hashSet.add(3);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
            i2 = i2;
        }
        if (parcel.dataPosition() == zzcr) {
            return new PersonEntity.CoverEntity.CoverInfoEntity(hashSet, i3, i2, i);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzzg */
    public PersonEntity.CoverEntity.CoverInfoEntity[] newArray(int i) {
        return new PersonEntity.CoverEntity.CoverInfoEntity[i];
    }
}
