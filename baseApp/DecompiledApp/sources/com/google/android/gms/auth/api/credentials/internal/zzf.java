package com.google.android.gms.auth.api.credentials.internal;

import android.content.Context;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzqo;

/* access modifiers changed from: package-private */
public abstract class zzf<R extends Result> extends zzqo.zza<R, zzg> {
    zzf(GoogleApiClient googleApiClient) {
        super(Auth.CREDENTIALS_API, googleApiClient);
    }

    /* access modifiers changed from: protected */
    public abstract void zza(Context context, zzk zzk);

    /* access modifiers changed from: protected */
    public final void zza(zzg zzg) {
        zza(zzg.getContext(), (zzk) zzg.zzavg());
    }
}
