package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd implements Parcelable.Creator<HintRequest> {
    static void zza(HintRequest hintRequest, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zza(parcel, 1, (Parcelable) hintRequest.getHintPickerConfig(), i, false);
        zzb.zza(parcel, 2, hintRequest.isEmailAddressIdentifierSupported());
        zzb.zza(parcel, 3, hintRequest.zzaih());
        zzb.zza(parcel, 4, hintRequest.getAccountTypes(), false);
        zzb.zzc(parcel, 1000, hintRequest.mVersionCode);
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzam */
    public HintRequest createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        String[] strArr = null;
        boolean z = false;
        boolean z2 = false;
        CredentialPickerConfig credentialPickerConfig = null;
        int i = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    credentialPickerConfig = (CredentialPickerConfig) zza.zza(parcel, zzcq, CredentialPickerConfig.CREATOR);
                    break;
                case 2:
                    z2 = zza.zzc(parcel, zzcq);
                    break;
                case 3:
                    z = zza.zzc(parcel, zzcq);
                    break;
                case 4:
                    strArr = zza.zzac(parcel, zzcq);
                    break;
                case 1000:
                    i = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new HintRequest(i, credentialPickerConfig, z2, z, strArr);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzda */
    public HintRequest[] newArray(int i) {
        return new HintRequest[i];
    }
}
