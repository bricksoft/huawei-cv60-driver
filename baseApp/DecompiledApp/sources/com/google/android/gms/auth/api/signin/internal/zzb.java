package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.zzsa;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class zzb extends AsyncTaskLoader<Void> implements zzsa {
    private Set<GoogleApiClient> jA;
    private Semaphore jz = new Semaphore(0);

    public zzb(Context context, Set<GoogleApiClient> set) {
        super(context);
        this.jA = set;
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.content.Loader
    public void onStartLoading() {
        this.jz.drainPermits();
        forceLoad();
    }

    /* renamed from: zzaja */
    public Void loadInBackground() {
        int i = 0;
        for (GoogleApiClient googleApiClient : this.jA) {
            i = googleApiClient.zza(this) ? i + 1 : i;
        }
        try {
            this.jz.tryAcquire(i, 5, TimeUnit.SECONDS);
            return null;
        } catch (InterruptedException e) {
            Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
            Thread.currentThread().interrupt();
            return null;
        }
    }

    @Override // com.google.android.gms.internal.zzsa
    public void zzajb() {
        this.jz.release();
    }
}
