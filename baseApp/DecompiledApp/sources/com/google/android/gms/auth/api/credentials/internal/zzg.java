package com.google.android.gms.auth.api.credentials.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.internal.zzk;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public final class zzg extends zzj<zzk> {
    @Nullable
    private final Auth.AuthCredentialsOptions iV;

    public zzg(Context context, Looper looper, zzf zzf, Auth.AuthCredentialsOptions authCredentialsOptions, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 68, zzf, connectionCallbacks, onConnectionFailedListener);
        this.iV = authCredentialsOptions;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public Bundle zzahv() {
        return this.iV == null ? new Bundle() : this.iV.zzahv();
    }

    /* access modifiers changed from: package-private */
    public Auth.AuthCredentialsOptions zzaim() {
        return this.iV;
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzce */
    public zzk zzh(IBinder iBinder) {
        return zzk.zza.zzcg(iBinder);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjx() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjy() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }
}
