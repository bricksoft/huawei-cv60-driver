package com.google.android.gms.auth.api.credentials.internal;

import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzaa;

public class zzb {
    public static String zzfx(String str) {
        boolean z = false;
        zzaa.zzb(!TextUtils.isEmpty(str), "account type cannot be empty");
        String scheme = Uri.parse(str).getScheme();
        if ("http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)) {
            z = true;
        }
        zzaa.zzb(z, "Account type must be an http or https URI");
        return str;
    }
}
