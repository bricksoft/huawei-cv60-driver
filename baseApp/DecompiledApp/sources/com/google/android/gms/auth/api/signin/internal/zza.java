package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.internal.zzg;
import com.google.android.gms.common.api.Status;

public class zza extends zzg.zza {
    @Override // com.google.android.gms.auth.api.signin.internal.zzg
    public void zza(GoogleSignInAccount googleSignInAccount, Status status) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.auth.api.signin.internal.zzg
    public void zzl(Status status) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.auth.api.signin.internal.zzg
    public void zzm(Status status) {
        throw new UnsupportedOperationException();
    }
}
