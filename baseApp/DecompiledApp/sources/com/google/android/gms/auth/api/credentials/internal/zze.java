package com.google.android.gms.auth.api.credentials.internal;

import android.app.PendingIntent;
import android.content.Context;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzqo;

public final class zze implements CredentialsApi {

    private static class zza extends zza {
        private zzqo.zzb<Status> iU;

        zza(zzqo.zzb<Status> zzb) {
            this.iU = zzb;
        }

        @Override // com.google.android.gms.auth.api.credentials.internal.zza, com.google.android.gms.auth.api.credentials.internal.zzj
        public void zzh(Status status) {
            this.iU.setResult(status);
        }
    }

    private PasswordSpecification zza(GoogleApiClient googleApiClient) {
        Auth.AuthCredentialsOptions zzaim = ((zzg) googleApiClient.zza(Auth.hX)).zzaim();
        return (zzaim == null || zzaim.zzaid() == null) ? PasswordSpecification.iG : zzaim.zzaid();
    }

    @Override // com.google.android.gms.auth.api.credentials.CredentialsApi
    public PendingResult<Status> delete(GoogleApiClient googleApiClient, final Credential credential) {
        return googleApiClient.zzb(new zzf<Status>(googleApiClient) {
            /* class com.google.android.gms.auth.api.credentials.internal.zze.AnonymousClass3 */

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.auth.api.credentials.internal.zzf
            public void zza(Context context, zzk zzk) {
                zzk.zza(new zza(this), new DeleteRequest(credential));
            }

            /* access modifiers changed from: protected */
            /* renamed from: zzb */
            public Status zzc(Status status) {
                return status;
            }
        });
    }

    @Override // com.google.android.gms.auth.api.credentials.CredentialsApi
    public PendingResult<Status> disableAutoSignIn(GoogleApiClient googleApiClient) {
        return googleApiClient.zzb(new zzf<Status>(googleApiClient) {
            /* class com.google.android.gms.auth.api.credentials.internal.zze.AnonymousClass4 */

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.auth.api.credentials.internal.zzf
            public void zza(Context context, zzk zzk) {
                zzk.zza(new zza(this));
            }

            /* access modifiers changed from: protected */
            /* renamed from: zzb */
            public Status zzc(Status status) {
                return status;
            }
        });
    }

    @Override // com.google.android.gms.auth.api.credentials.CredentialsApi
    public PendingIntent getHintPickerIntent(GoogleApiClient googleApiClient, HintRequest hintRequest) {
        zzaa.zzb(googleApiClient, "client must not be null");
        zzaa.zzb(hintRequest, "request must not be null");
        zzaa.zzb(googleApiClient.zza(Auth.CREDENTIALS_API), "Auth.CREDENTIALS_API must be added to GoogleApiClient to use this API");
        return PendingIntent.getActivity(googleApiClient.getContext(), 2000, zzc.zza(googleApiClient.getContext(), hintRequest, zza(googleApiClient)), 268435456);
    }

    @Override // com.google.android.gms.auth.api.credentials.CredentialsApi
    public PendingResult<CredentialRequestResult> request(GoogleApiClient googleApiClient, final CredentialRequest credentialRequest) {
        return googleApiClient.zza(new zzf<CredentialRequestResult>(googleApiClient) {
            /* class com.google.android.gms.auth.api.credentials.internal.zze.AnonymousClass1 */

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.auth.api.credentials.internal.zzf
            public void zza(Context context, zzk zzk) {
                zzk.zza(new zza() {
                    /* class com.google.android.gms.auth.api.credentials.internal.zze.AnonymousClass1.AnonymousClass1 */

                    @Override // com.google.android.gms.auth.api.credentials.internal.zza, com.google.android.gms.auth.api.credentials.internal.zzj
                    public void zza(Status status, Credential credential) {
                        AnonymousClass1.this.zzc(new zzd(status, credential));
                    }

                    @Override // com.google.android.gms.auth.api.credentials.internal.zza, com.google.android.gms.auth.api.credentials.internal.zzj
                    public void zzh(Status status) {
                        AnonymousClass1.this.zzc(zzd.zzi(status));
                    }
                }, credentialRequest);
            }

            /* access modifiers changed from: protected */
            /* renamed from: zzj */
            public CredentialRequestResult zzc(Status status) {
                return zzd.zzi(status);
            }
        });
    }

    @Override // com.google.android.gms.auth.api.credentials.CredentialsApi
    public PendingResult<Status> save(GoogleApiClient googleApiClient, final Credential credential) {
        return googleApiClient.zzb(new zzf<Status>(googleApiClient) {
            /* class com.google.android.gms.auth.api.credentials.internal.zze.AnonymousClass2 */

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.auth.api.credentials.internal.zzf
            public void zza(Context context, zzk zzk) {
                zzk.zza(new zza(this), new SaveRequest(credential));
            }

            /* access modifiers changed from: protected */
            /* renamed from: zzb */
            public Status zzc(Status status) {
                return status;
            }
        });
    }
}
