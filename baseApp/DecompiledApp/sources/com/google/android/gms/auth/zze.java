package com.google.android.gms.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.annotation.RequiresPermission;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzl;
import com.google.android.gms.internal.zzbz;
import com.google.android.gms.internal.zzoe;
import com.google.android.gms.internal.zzsu;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class zze {
    public static final int CHANGE_TYPE_ACCOUNT_ADDED = 1;
    public static final int CHANGE_TYPE_ACCOUNT_REMOVED = 2;
    public static final int CHANGE_TYPE_ACCOUNT_RENAMED_FROM = 3;
    public static final int CHANGE_TYPE_ACCOUNT_RENAMED_TO = 4;
    public static final String GOOGLE_ACCOUNT_TYPE = "com.google";
    public static final String KEY_ANDROID_PACKAGE_NAME = (Build.VERSION.SDK_INT >= 14 ? "androidPackageName" : "androidPackageName");
    public static final String KEY_CALLER_UID = (Build.VERSION.SDK_INT >= 11 ? "callerUid" : "callerUid");
    public static final String KEY_SUPPRESS_PROGRESS_SCREEN = "suppressProgressScreen";
    public static final String WORK_ACCOUNT_TYPE = "com.google.work";
    private static final String[] hC = {"com.google", "com.google.work", "cn.google"};
    private static final ComponentName hD = new ComponentName("com.google.android.gms", "com.google.android.gms.auth.GetToken");
    private static final ComponentName hE = new ComponentName("com.google.android.gms", "com.google.android.gms.recovery.RecoveryService");
    private static final zzsu hF = zzd.zzb("GoogleAuthUtil");

    /* access modifiers changed from: private */
    public interface zza<T> {
        T zzbu(IBinder iBinder);
    }

    zze() {
    }

    public static void clearToken(Context context, final String str) {
        zzaa.zzht("Calling this from your main thread can lead to deadlock");
        zzaz(context);
        final Bundle bundle = new Bundle();
        String str2 = context.getApplicationInfo().packageName;
        bundle.putString("clientPackageName", str2);
        if (!bundle.containsKey(KEY_ANDROID_PACKAGE_NAME)) {
            bundle.putString(KEY_ANDROID_PACKAGE_NAME, str2);
        }
        zza(context, hD, new zza<Void>() {
            /* class com.google.android.gms.auth.zze.AnonymousClass2 */

            /* renamed from: zzbv */
            public Void zzbu(IBinder iBinder) {
                Bundle bundle = (Bundle) zze.zzn(zzbz.zza.zza(iBinder).zza(str, bundle));
                String string = bundle.getString("Error");
                if (bundle.getBoolean("booleanResult")) {
                    return null;
                }
                throw new GoogleAuthException(string);
            }
        });
    }

    public static List<AccountChangeEvent> getAccountChangeEvents(Context context, final int i, final String str) {
        zzaa.zzh(str, "accountName must be provided");
        zzaa.zzht("Calling this from your main thread can lead to deadlock");
        zzaz(context);
        return (List) zza(context, hD, new zza<List<AccountChangeEvent>>() {
            /* class com.google.android.gms.auth.zze.AnonymousClass3 */

            /* renamed from: zzbw */
            public List<AccountChangeEvent> zzbu(IBinder iBinder) {
                return ((AccountChangeEventsResponse) zze.zzn(zzbz.zza.zza(iBinder).zza(new AccountChangeEventsRequest().setAccountName(str).setEventIndex(i)))).getEvents();
            }
        });
    }

    public static String getAccountId(Context context, String str) {
        zzaa.zzh(str, "accountName must be provided");
        zzaa.zzht("Calling this from your main thread can lead to deadlock");
        zzaz(context);
        return getToken(context, str, "^^_account_id_^^", new Bundle());
    }

    public static String getToken(Context context, Account account, String str) {
        return getToken(context, account, str, new Bundle());
    }

    public static String getToken(Context context, Account account, String str, Bundle bundle) {
        zzc(account);
        return zzc(context, account, str, bundle).getToken();
    }

    @Deprecated
    public static String getToken(Context context, String str, String str2) {
        return getToken(context, new Account(str, "com.google"), str2);
    }

    @Deprecated
    public static String getToken(Context context, String str, String str2, Bundle bundle) {
        return getToken(context, new Account(str, "com.google"), str2, bundle);
    }

    @RequiresPermission("android.permission.MANAGE_ACCOUNTS")
    @Deprecated
    public static void invalidateToken(Context context, String str) {
        AccountManager.get(context).invalidateAuthToken("com.google", str);
    }

    @TargetApi(23)
    public static Bundle removeAccount(Context context, final Account account) {
        zzaa.zzy(context);
        zzc(account);
        zzaz(context);
        return (Bundle) zza(context, hD, new zza<Bundle>() {
            /* class com.google.android.gms.auth.zze.AnonymousClass4 */

            /* renamed from: zzbx */
            public Bundle zzbu(IBinder iBinder) {
                return (Bundle) zze.zzn(zzbz.zza.zza(iBinder).zza(account));
            }
        });
    }

    private static <T> T zza(Context context, ComponentName componentName, zza<T> zza2) {
        com.google.android.gms.common.zza zza3 = new com.google.android.gms.common.zza();
        zzl zzcc = zzl.zzcc(context);
        if (zzcc.zza(componentName, zza3, "GoogleAuthUtil")) {
            try {
                T zzbu = zza2.zzbu(zza3.zzaqk());
                zzcc.zzb(componentName, zza3, "GoogleAuthUtil");
                return zzbu;
            } catch (RemoteException | InterruptedException e) {
                hF.zze("GoogleAuthUtil", "Error on service connection.", e);
                throw new IOException("Error on service connection.", e);
            } catch (Throwable th) {
                zzcc.zzb(componentName, zza3, "GoogleAuthUtil");
                throw th;
            }
        } else {
            throw new IOException("Could not bind to service.");
        }
    }

    private static void zzaz(Context context) {
        try {
            com.google.android.gms.common.zze.zzaz(context.getApplicationContext());
        } catch (GooglePlayServicesRepairableException e) {
            throw new GooglePlayServicesAvailabilityException(e.getConnectionStatusCode(), e.getMessage(), e.getIntent());
        } catch (GooglePlayServicesNotAvailableException e2) {
            throw new GoogleAuthException(e2.getMessage());
        }
    }

    public static TokenData zzc(Context context, final Account account, final String str, Bundle bundle) {
        zzaa.zzht("Calling this from your main thread can lead to deadlock");
        zzaa.zzh(str, "Scope cannot be empty or null.");
        zzc(account);
        zzaz(context);
        final Bundle bundle2 = bundle == null ? new Bundle() : new Bundle(bundle);
        String str2 = context.getApplicationInfo().packageName;
        bundle2.putString("clientPackageName", str2);
        if (TextUtils.isEmpty(bundle2.getString(KEY_ANDROID_PACKAGE_NAME))) {
            bundle2.putString(KEY_ANDROID_PACKAGE_NAME, str2);
        }
        bundle2.putLong("service_connection_start_time_millis", SystemClock.elapsedRealtime());
        return (TokenData) zza(context, hD, new zza<TokenData>() {
            /* class com.google.android.gms.auth.zze.AnonymousClass1 */

            /* renamed from: zzbt */
            public TokenData zzbu(IBinder iBinder) {
                Bundle bundle = (Bundle) zze.zzn(zzbz.zza.zza(iBinder).zza(account, str, bundle2));
                TokenData zzd = TokenData.zzd(bundle, "tokenDetails");
                if (zzd != null) {
                    return zzd;
                }
                String string = bundle.getString("Error");
                Intent intent = (Intent) bundle.getParcelable("userRecoveryIntent");
                zzoe zzgi = zzoe.zzgi(string);
                if (zzoe.zza(zzgi)) {
                    zzsu zzsu = zze.hF;
                    String valueOf = String.valueOf(zzgi);
                    zzsu.zzf("GoogleAuthUtil", new StringBuilder(String.valueOf(valueOf).length() + 31).append("isUserRecoverableError status: ").append(valueOf).toString());
                    throw new UserRecoverableAuthException(string, intent);
                } else if (zzoe.zzb(zzgi)) {
                    throw new IOException(string);
                } else {
                    throw new GoogleAuthException(string);
                }
            }
        });
    }

    private static void zzc(Account account) {
        if (account == null) {
            throw new IllegalArgumentException("Account cannot be null");
        } else if (TextUtils.isEmpty(account.name)) {
            throw new IllegalArgumentException("Account name cannot be empty!");
        } else {
            for (String str : hC) {
                if (str.equals(account.type)) {
                    return;
                }
            }
            throw new IllegalArgumentException("Account type not supported");
        }
    }

    static void zzi(Intent intent) {
        if (intent == null) {
            throw new IllegalArgumentException("Callback cannot be null.");
        }
        try {
            Intent.parseUri(intent.toUri(1), 1);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Parameter callback contains invalid data. It must be serializable using toUri() and parseUri().");
        }
    }

    /* access modifiers changed from: private */
    public static <T> T zzn(T t) {
        if (t != null) {
            return t;
        }
        hF.zzf("GoogleAuthUtil", "Binder call returned null.");
        throw new IOException("Service unavailable.");
    }
}
