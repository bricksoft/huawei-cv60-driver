package com.google.android.gms.auth.api.credentials.internal;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.internal.zzj;
import com.google.android.gms.common.api.Status;

public class zza extends zzj.zza {
    @Override // com.google.android.gms.auth.api.credentials.internal.zzj
    public void zza(Status status, Credential credential) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.auth.api.credentials.internal.zzj
    public void zza(Status status, String str) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.auth.api.credentials.internal.zzj
    public void zzh(Status status) {
        throw new UnsupportedOperationException();
    }
}
