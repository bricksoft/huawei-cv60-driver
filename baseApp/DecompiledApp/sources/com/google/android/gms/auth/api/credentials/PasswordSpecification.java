package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

public final class PasswordSpecification extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<PasswordSpecification> CREATOR = new zzf();
    public static final PasswordSpecification iG = new zza().zzl(12, 16).zzfw("abcdefghijkmnopqrstxyzABCDEFGHJKLMNPQRSTXY3456789").zzf("abcdefghijkmnopqrstxyz", 1).zzf("ABCDEFGHJKLMNPQRSTXY", 1).zzf("3456789", 1).zzaij();
    public static final PasswordSpecification iH = new zza().zzl(12, 16).zzfw("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890").zzf("abcdefghijklmnopqrstuvwxyz", 1).zzf("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1).zzf("1234567890", 1).zzaij();
    final String iI;
    final List<String> iJ;
    final List<Integer> iK;
    final int iL;
    final int iM;
    private final int[] iN = zzaii();
    final int mVersionCode;
    private final Random zzbao = new SecureRandom();

    public static class zza {
        private final List<String> iJ = new ArrayList();
        private final List<Integer> iK = new ArrayList();
        private int iL = 12;
        private int iM = 16;
        private final TreeSet<Character> iO = new TreeSet<>();

        private void zzaik() {
            int i = 0;
            for (Integer num : this.iK) {
                i = num.intValue() + i;
            }
            if (i > this.iM) {
                throw new zzb("required character count cannot be greater than the max password size");
            }
        }

        private void zzail() {
            boolean[] zArr = new boolean[95];
            for (String str : this.iJ) {
                char[] charArray = str.toCharArray();
                int length = charArray.length;
                int i = 0;
                while (true) {
                    if (i < length) {
                        char c = charArray[i];
                        if (zArr[c - ' ']) {
                            throw new zzb(new StringBuilder(58).append("character ").append(c).append(" occurs in more than one required character set").toString());
                        }
                        zArr[c - ' '] = true;
                        i++;
                    }
                }
            }
        }

        private TreeSet<Character> zzv(String str, String str2) {
            if (TextUtils.isEmpty(str)) {
                throw new zzb(String.valueOf(str2).concat(" cannot be null or empty"));
            }
            TreeSet<Character> treeSet = new TreeSet<>();
            char[] charArray = str.toCharArray();
            for (char c : charArray) {
                if (PasswordSpecification.zzc(c, 32, 126)) {
                    throw new zzb(String.valueOf(str2).concat(" must only contain ASCII printable characters"));
                }
                treeSet.add(Character.valueOf(c));
            }
            return treeSet;
        }

        public PasswordSpecification zzaij() {
            if (this.iO.isEmpty()) {
                throw new zzb("no allowed characters specified");
            }
            zzaik();
            zzail();
            return new PasswordSpecification(1, PasswordSpecification.zzb(this.iO), this.iJ, this.iK, this.iL, this.iM);
        }

        public zza zzf(@NonNull String str, int i) {
            if (i < 1) {
                throw new zzb("count must be at least 1");
            }
            this.iJ.add(PasswordSpecification.zzb(zzv(str, "requiredChars")));
            this.iK.add(Integer.valueOf(i));
            return this;
        }

        public zza zzfw(@NonNull String str) {
            this.iO.addAll(zzv(str, "allowedChars"));
            return this;
        }

        public zza zzl(int i, int i2) {
            if (i < 1) {
                throw new zzb("minimumSize must be at least 1");
            } else if (i > i2) {
                throw new zzb("maximumSize must be greater than or equal to minimumSize");
            } else {
                this.iL = i;
                this.iM = i2;
                return this;
            }
        }
    }

    public static class zzb extends Error {
        public zzb(String str) {
            super(str);
        }
    }

    PasswordSpecification(int i, String str, List<String> list, List<Integer> list2, int i2, int i3) {
        this.mVersionCode = i;
        this.iI = str;
        this.iJ = Collections.unmodifiableList(list);
        this.iK = Collections.unmodifiableList(list2);
        this.iL = i2;
        this.iM = i3;
    }

    private int zza(char c) {
        return c - ' ';
    }

    private int[] zzaii() {
        int[] iArr = new int[95];
        Arrays.fill(iArr, -1);
        int i = 0;
        for (String str : this.iJ) {
            char[] charArray = str.toCharArray();
            for (char c : charArray) {
                iArr[zza(c)] = i;
            }
            i++;
        }
        return iArr;
    }

    /* access modifiers changed from: private */
    public static String zzb(Collection<Character> collection) {
        char[] cArr = new char[collection.size()];
        int i = 0;
        for (Character ch : collection) {
            i++;
            cArr[i] = ch.charValue();
        }
        return new String(cArr);
    }

    /* access modifiers changed from: private */
    public static boolean zzc(int i, int i2, int i3) {
        return i < i2 || i > i3;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzf.zza(this, parcel, i);
    }
}
