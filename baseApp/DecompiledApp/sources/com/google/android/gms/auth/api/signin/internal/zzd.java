package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.zzh;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public class zzd extends zzj<zzh> {
    private final GoogleSignInOptions jH;

    public zzd(Context context, Looper looper, zzf zzf, GoogleSignInOptions googleSignInOptions, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 91, zzf, connectionCallbacks, onConnectionFailedListener);
        googleSignInOptions = googleSignInOptions == null ? new GoogleSignInOptions.Builder().build() : googleSignInOptions;
        if (!zzf.zzavq().isEmpty()) {
            GoogleSignInOptions.Builder builder = new GoogleSignInOptions.Builder(googleSignInOptions);
            for (Scope scope : zzf.zzavq()) {
                builder.requestScopes(scope, new Scope[0]);
            }
            googleSignInOptions = builder.build();
        }
        this.jH = googleSignInOptions;
    }

    @Override // com.google.android.gms.common.api.Api.zze, com.google.android.gms.common.internal.zze
    public boolean zzajc() {
        return true;
    }

    @Override // com.google.android.gms.common.api.Api.zze, com.google.android.gms.common.internal.zze
    public Intent zzajd() {
        SignInConfiguration signInConfiguration = new SignInConfiguration(getContext().getPackageName(), this.jH);
        Intent intent = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        intent.setClass(getContext(), SignInHubActivity.class);
        intent.putExtra("config", signInConfiguration);
        return intent;
    }

    public GoogleSignInOptions zzaje() {
        return this.jH;
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzck */
    public zzh zzh(IBinder iBinder) {
        return zzh.zza.zzcm(iBinder);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjx() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjy() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }
}
