package com.google.android.gms.auth.api.signin.internal;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.internal.zzru;
import java.util.HashSet;

public class zzc implements GoogleSignInApi {

    private abstract class zza<R extends Result> extends zzqo.zza<R, zzd> {
        public zza(GoogleApiClient googleApiClient) {
            super(Auth.GOOGLE_SIGN_IN_API, googleApiClient);
        }
    }

    private OptionalPendingResult<GoogleSignInResult> zza(GoogleApiClient googleApiClient, final GoogleSignInOptions googleSignInOptions) {
        Log.d("GoogleSignInApiImpl", "trySilentSignIn");
        return new zzru(googleApiClient.zza(new zza<GoogleSignInResult>(googleApiClient) {
            /* class com.google.android.gms.auth.api.signin.internal.zzc.AnonymousClass1 */

            /* access modifiers changed from: protected */
            public void zza(zzd zzd) {
                final zzk zzba = zzk.zzba(zzd.getContext());
                ((zzh) zzd.zzavg()).zza(new zza() {
                    /* class com.google.android.gms.auth.api.signin.internal.zzc.AnonymousClass1.AnonymousClass1 */

                    @Override // com.google.android.gms.auth.api.signin.internal.zzg, com.google.android.gms.auth.api.signin.internal.zza
                    public void zza(GoogleSignInAccount googleSignInAccount, Status status) {
                        if (googleSignInAccount != null) {
                            zzba.zzb(googleSignInAccount, googleSignInOptions);
                        }
                        AnonymousClass1.this.zzc(new GoogleSignInResult(googleSignInAccount, status));
                    }
                }, googleSignInOptions);
            }

            /* access modifiers changed from: protected */
            /* renamed from: zzn */
            public GoogleSignInResult zzc(Status status) {
                return new GoogleSignInResult(null, status);
            }
        }));
    }

    private boolean zza(Account account, Account account2) {
        return account == null ? account2 == null : account.equals(account2);
    }

    private GoogleSignInOptions zzb(GoogleApiClient googleApiClient) {
        return ((zzd) googleApiClient.zza(Auth.hZ)).zzaje();
    }

    @Override // com.google.android.gms.auth.api.signin.GoogleSignInApi
    public Intent getSignInIntent(GoogleApiClient googleApiClient) {
        zzaa.zzy(googleApiClient);
        return ((zzd) googleApiClient.zza(Auth.hZ)).zzajd();
    }

    @Override // com.google.android.gms.auth.api.signin.GoogleSignInApi
    public GoogleSignInResult getSignInResultFromIntent(Intent intent) {
        if (intent == null || (!intent.hasExtra("googleSignInStatus") && !intent.hasExtra("googleSignInAccount"))) {
            return null;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) intent.getParcelableExtra("googleSignInAccount");
        Status status = (Status) intent.getParcelableExtra("googleSignInStatus");
        if (googleSignInAccount != null) {
            status = Status.xZ;
        }
        return new GoogleSignInResult(googleSignInAccount, status);
    }

    @Override // com.google.android.gms.auth.api.signin.GoogleSignInApi
    public PendingResult<Status> revokeAccess(GoogleApiClient googleApiClient) {
        zzk.zzba(googleApiClient.getContext()).zzajo();
        for (GoogleApiClient googleApiClient2 : GoogleApiClient.zzarc()) {
            googleApiClient2.zzard();
        }
        return googleApiClient.zzb(new zza<Status>(googleApiClient) {
            /* class com.google.android.gms.auth.api.signin.internal.zzc.AnonymousClass3 */

            /* access modifiers changed from: protected */
            public void zza(zzd zzd) {
                ((zzh) zzd.zzavg()).zzc(new zza() {
                    /* class com.google.android.gms.auth.api.signin.internal.zzc.AnonymousClass3.AnonymousClass1 */

                    @Override // com.google.android.gms.auth.api.signin.internal.zzg, com.google.android.gms.auth.api.signin.internal.zza
                    public void zzm(Status status) {
                        AnonymousClass3.this.zzc((Result) status);
                    }
                }, zzd.zzaje());
            }

            /* access modifiers changed from: protected */
            /* renamed from: zzb */
            public Status zzc(Status status) {
                return status;
            }
        });
    }

    @Override // com.google.android.gms.auth.api.signin.GoogleSignInApi
    public PendingResult<Status> signOut(GoogleApiClient googleApiClient) {
        zzk.zzba(googleApiClient.getContext()).zzajo();
        for (GoogleApiClient googleApiClient2 : GoogleApiClient.zzarc()) {
            googleApiClient2.zzard();
        }
        return googleApiClient.zzb(new zza<Status>(googleApiClient) {
            /* class com.google.android.gms.auth.api.signin.internal.zzc.AnonymousClass2 */

            /* access modifiers changed from: protected */
            public void zza(zzd zzd) {
                ((zzh) zzd.zzavg()).zzb(new zza() {
                    /* class com.google.android.gms.auth.api.signin.internal.zzc.AnonymousClass2.AnonymousClass1 */

                    @Override // com.google.android.gms.auth.api.signin.internal.zzg, com.google.android.gms.auth.api.signin.internal.zza
                    public void zzl(Status status) {
                        AnonymousClass2.this.zzc((Result) status);
                    }
                }, zzd.zzaje());
            }

            /* access modifiers changed from: protected */
            /* renamed from: zzb */
            public Status zzc(Status status) {
                return status;
            }
        });
    }

    @Override // com.google.android.gms.auth.api.signin.GoogleSignInApi
    public OptionalPendingResult<GoogleSignInResult> silentSignIn(GoogleApiClient googleApiClient) {
        GoogleSignInOptions zzb = zzb(googleApiClient);
        GoogleSignInResult zza2 = zza(googleApiClient.getContext(), zzb);
        return zza2 != null ? PendingResults.zzb(zza2, googleApiClient) : zza(googleApiClient, zzb);
    }

    public GoogleSignInResult zza(Context context, GoogleSignInOptions googleSignInOptions) {
        GoogleSignInAccount zzajm;
        Log.d("GoogleSignInApiImpl", "getSavedSignInResultIfEligible");
        zzaa.zzy(googleSignInOptions);
        zzk zzba = zzk.zzba(context);
        GoogleSignInOptions zzajn = zzba.zzajn();
        if (zzajn == null || !zza(zzajn.getAccount(), googleSignInOptions.getAccount()) || googleSignInOptions.zzaiv()) {
            return null;
        }
        if ((!googleSignInOptions.zzaiu() || (zzajn.zzaiu() && googleSignInOptions.zzaix().equals(zzajn.zzaix()))) && new HashSet(zzajn.zzait()).containsAll(new HashSet(googleSignInOptions.zzait())) && (zzajm = zzba.zzajm()) != null && !zzajm.zza()) {
            return new GoogleSignInResult(zzajm, Status.xZ);
        }
        return null;
    }
}
