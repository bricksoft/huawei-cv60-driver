package com.google.android.gms.auth.api;

import android.os.Bundle;
import com.google.android.gms.common.api.Api;

public class zzb implements Api.ApiOptions.Optional {
    private final Bundle ik;

    public Bundle zzaie() {
        return new Bundle(this.ik);
    }
}
