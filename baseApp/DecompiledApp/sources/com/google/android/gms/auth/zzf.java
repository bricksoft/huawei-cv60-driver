package com.google.android.gms.auth;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzf implements Parcelable.Creator<TokenData> {
    static void zza(TokenData tokenData, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, tokenData.mVersionCode);
        zzb.zza(parcel, 2, tokenData.getToken(), false);
        zzb.zza(parcel, 3, tokenData.zzahy(), false);
        zzb.zza(parcel, 4, tokenData.zzahz());
        zzb.zza(parcel, 5, tokenData.zzaia());
        zzb.zzb(parcel, 6, tokenData.zzaib(), false);
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzai */
    public TokenData createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        ArrayList<String> arrayList = null;
        boolean z = false;
        boolean z2 = false;
        Long l = null;
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    str = zza.zzq(parcel, zzcq);
                    break;
                case 3:
                    l = zza.zzj(parcel, zzcq);
                    break;
                case 4:
                    z2 = zza.zzc(parcel, zzcq);
                    break;
                case 5:
                    z = zza.zzc(parcel, zzcq);
                    break;
                case 6:
                    arrayList = zza.zzae(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new TokenData(i, str, l, z2, z, arrayList);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzcw */
    public TokenData[] newArray(int i) {
        return new TokenData[i];
    }
}
