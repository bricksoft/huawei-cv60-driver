package com.google.android.gms.auth.api.proxy;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;

public class zzb implements Parcelable.Creator<ProxyRequest> {
    static void zza(ProxyRequest proxyRequest, Parcel parcel, int i) {
        int zzcs = com.google.android.gms.common.internal.safeparcel.zzb.zzcs(parcel);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 1, proxyRequest.url, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zzc(parcel, 2, proxyRequest.httpMethod);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 3, proxyRequest.timeoutMillis);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 4, proxyRequest.body, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 5, proxyRequest.iW, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zzc(parcel, 1000, proxyRequest.versionCode);
        com.google.android.gms.common.internal.safeparcel.zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzat */
    public ProxyRequest createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        long j = 0;
        Bundle bundle = null;
        byte[] bArr = null;
        int i = 0;
        String str = null;
        int i2 = 0;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    str = zza.zzq(parcel, zzcq);
                    break;
                case 2:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 3:
                    j = zza.zzi(parcel, zzcq);
                    break;
                case 4:
                    bArr = zza.zzt(parcel, zzcq);
                    break;
                case 5:
                    bundle = zza.zzs(parcel, zzcq);
                    break;
                case 1000:
                    i2 = zza.zzg(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new ProxyRequest(i2, str, i, j, bArr, bundle);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzdh */
    public ProxyRequest[] newArray(int i) {
        return new ProxyRequest[i];
    }
}
