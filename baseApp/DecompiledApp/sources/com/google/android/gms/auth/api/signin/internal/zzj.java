package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj implements Parcelable.Creator<SignInConfiguration> {
    static void zza(SignInConfiguration signInConfiguration, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, signInConfiguration.versionCode);
        zzb.zza(parcel, 2, signInConfiguration.zzajj(), false);
        zzb.zza(parcel, 5, (Parcelable) signInConfiguration.zzajk(), i, false);
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzay */
    public SignInConfiguration createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        GoogleSignInOptions googleSignInOptions = null;
        String str = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    str = zza.zzq(parcel, zzcq);
                    break;
                case 3:
                case 4:
                default:
                    zza.zzb(parcel, zzcq);
                    break;
                case 5:
                    googleSignInOptions = (GoogleSignInOptions) zza.zza(parcel, zzcq, GoogleSignInOptions.CREATOR);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new SignInConfiguration(i, str, googleSignInOptions);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzdm */
    public SignInConfiguration[] newArray(int i) {
        return new SignInConfiguration[i];
    }
}
