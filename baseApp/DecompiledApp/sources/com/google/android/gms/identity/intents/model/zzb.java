package com.google.android.gms.identity.intents.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;

public class zzb implements Parcelable.Creator<UserAddress> {
    static void zza(UserAddress userAddress, Parcel parcel, int i) {
        int zzcs = com.google.android.gms.common.internal.safeparcel.zzb.zzcs(parcel);
        com.google.android.gms.common.internal.safeparcel.zzb.zzc(parcel, 1, userAddress.getVersionCode());
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 2, userAddress.name, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 3, userAddress.ahQ, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 4, userAddress.ahR, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 5, userAddress.ahS, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 6, userAddress.ahT, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 7, userAddress.ahU, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 8, userAddress.ahV, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 9, userAddress.ahW, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 10, userAddress.zzcpw, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 11, userAddress.ahX, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 12, userAddress.ahY, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 13, userAddress.phoneNumber, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 14, userAddress.ahZ);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 15, userAddress.aia, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 16, userAddress.aib, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzne */
    public UserAddress createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        boolean z = false;
        String str13 = null;
        String str14 = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    str = zza.zzq(parcel, zzcq);
                    break;
                case 3:
                    str2 = zza.zzq(parcel, zzcq);
                    break;
                case 4:
                    str3 = zza.zzq(parcel, zzcq);
                    break;
                case 5:
                    str4 = zza.zzq(parcel, zzcq);
                    break;
                case 6:
                    str5 = zza.zzq(parcel, zzcq);
                    break;
                case 7:
                    str6 = zza.zzq(parcel, zzcq);
                    break;
                case 8:
                    str7 = zza.zzq(parcel, zzcq);
                    break;
                case 9:
                    str8 = zza.zzq(parcel, zzcq);
                    break;
                case 10:
                    str9 = zza.zzq(parcel, zzcq);
                    break;
                case 11:
                    str10 = zza.zzq(parcel, zzcq);
                    break;
                case 12:
                    str11 = zza.zzq(parcel, zzcq);
                    break;
                case 13:
                    str12 = zza.zzq(parcel, zzcq);
                    break;
                case 14:
                    z = zza.zzc(parcel, zzcq);
                    break;
                case 15:
                    str13 = zza.zzq(parcel, zzcq);
                    break;
                case 16:
                    str14 = zza.zzq(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new UserAddress(i, str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, z, str13, str14);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zztt */
    public UserAddress[] newArray(int i) {
        return new UserAddress[i];
    }
}
