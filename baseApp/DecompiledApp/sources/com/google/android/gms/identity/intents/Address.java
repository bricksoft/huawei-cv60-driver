package com.google.android.gms.identity.intents;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.internal.zzvx;

public final class Address {
    public static final Api<AddressOptions> API = new Api<>("Address.API", hh, hg);
    static final Api.zzf<zzvx> hg = new Api.zzf<>();
    private static final Api.zza<zzvx, AddressOptions> hh = new Api.zza<zzvx, AddressOptions>() {
        /* class com.google.android.gms.identity.intents.Address.AnonymousClass1 */

        public zzvx zza(Context context, Looper looper, zzf zzf, AddressOptions addressOptions, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
            zzaa.zzb(context instanceof Activity, "An Activity must be used for Address APIs");
            if (addressOptions == null) {
                addressOptions = new AddressOptions();
            }
            return new zzvx((Activity) context, looper, zzf, addressOptions.theme, connectionCallbacks, onConnectionFailedListener);
        }
    };

    public static final class AddressOptions implements Api.ApiOptions.HasOptions {
        public final int theme;

        public AddressOptions() {
            this.theme = 0;
        }

        public AddressOptions(int i) {
            this.theme = i;
        }
    }

    private static abstract class zza extends zzqo.zza<Status, zzvx> {
        public zza(GoogleApiClient googleApiClient) {
            super(Address.API, googleApiClient);
        }

        /* renamed from: zzb */
        public Status zzc(Status status) {
            return status;
        }
    }

    public static void requestUserAddress(GoogleApiClient googleApiClient, final UserAddressRequest userAddressRequest, final int i) {
        googleApiClient.zza(new zza(googleApiClient) {
            /* class com.google.android.gms.identity.intents.Address.AnonymousClass2 */

            /* access modifiers changed from: protected */
            public void zza(zzvx zzvx) {
                zzvx.zza(userAddressRequest, i);
                zzc((Result) Status.xZ);
            }
        });
    }
}
