package com.google.android.gms.internal;

import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class zzaqf extends zzaqp {
    private static final Reader bpL = new Reader() {
        /* class com.google.android.gms.internal.zzaqf.AnonymousClass1 */

        @Override // java.io.Closeable, java.io.Reader, java.lang.AutoCloseable
        public void close() {
            throw new AssertionError();
        }

        @Override // java.io.Reader
        public int read(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    };
    private static final Object bpM = new Object();
    private final List<Object> bpN = new ArrayList();

    public zzaqf(zzaoy zzaoy) {
        super(bpL);
        this.bpN.add(zzaoy);
    }

    private Object br() {
        return this.bpN.get(this.bpN.size() - 1);
    }

    private Object bs() {
        return this.bpN.remove(this.bpN.size() - 1);
    }

    private void zza(zzaqq zzaqq) {
        if (bq() != zzaqq) {
            String valueOf = String.valueOf(zzaqq);
            String valueOf2 = String.valueOf(bq());
            throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 18 + String.valueOf(valueOf2).length()).append("Expected ").append(valueOf).append(" but was ").append(valueOf2).toString());
        }
    }

    @Override // com.google.android.gms.internal.zzaqp
    public void beginArray() {
        zza(zzaqq.BEGIN_ARRAY);
        this.bpN.add(((zzaov) br()).iterator());
    }

    @Override // com.google.android.gms.internal.zzaqp
    public void beginObject() {
        zza(zzaqq.BEGIN_OBJECT);
        this.bpN.add(((zzapb) br()).entrySet().iterator());
    }

    @Override // com.google.android.gms.internal.zzaqp
    public zzaqq bq() {
        if (this.bpN.isEmpty()) {
            return zzaqq.END_DOCUMENT;
        }
        Object br = br();
        if (br instanceof Iterator) {
            boolean z = this.bpN.get(this.bpN.size() - 2) instanceof zzapb;
            Iterator it = (Iterator) br;
            if (!it.hasNext()) {
                return z ? zzaqq.END_OBJECT : zzaqq.END_ARRAY;
            }
            if (z) {
                return zzaqq.NAME;
            }
            this.bpN.add(it.next());
            return bq();
        } else if (br instanceof zzapb) {
            return zzaqq.BEGIN_OBJECT;
        } else {
            if (br instanceof zzaov) {
                return zzaqq.BEGIN_ARRAY;
            }
            if (br instanceof zzape) {
                zzape zzape = (zzape) br;
                if (zzape.bf()) {
                    return zzaqq.STRING;
                }
                if (zzape.bd()) {
                    return zzaqq.BOOLEAN;
                }
                if (zzape.be()) {
                    return zzaqq.NUMBER;
                }
                throw new AssertionError();
            } else if (br instanceof zzapa) {
                return zzaqq.NULL;
            } else {
                if (br == bpM) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    public void bt() {
        zza(zzaqq.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) br()).next();
        this.bpN.add(entry.getValue());
        this.bpN.add(new zzape((String) entry.getKey()));
    }

    @Override // java.io.Closeable, com.google.android.gms.internal.zzaqp, java.lang.AutoCloseable
    public void close() {
        this.bpN.clear();
        this.bpN.add(bpM);
    }

    @Override // com.google.android.gms.internal.zzaqp
    public void endArray() {
        zza(zzaqq.END_ARRAY);
        bs();
        bs();
    }

    @Override // com.google.android.gms.internal.zzaqp
    public void endObject() {
        zza(zzaqq.END_OBJECT);
        bs();
        bs();
    }

    @Override // com.google.android.gms.internal.zzaqp
    public boolean hasNext() {
        zzaqq bq = bq();
        return (bq == zzaqq.END_OBJECT || bq == zzaqq.END_ARRAY) ? false : true;
    }

    @Override // com.google.android.gms.internal.zzaqp
    public boolean nextBoolean() {
        zza(zzaqq.BOOLEAN);
        return ((zzape) bs()).getAsBoolean();
    }

    @Override // com.google.android.gms.internal.zzaqp
    public double nextDouble() {
        zzaqq bq = bq();
        if (bq == zzaqq.NUMBER || bq == zzaqq.STRING) {
            double asDouble = ((zzape) br()).getAsDouble();
            if (isLenient() || (!Double.isNaN(asDouble) && !Double.isInfinite(asDouble))) {
                bs();
                return asDouble;
            }
            throw new NumberFormatException(new StringBuilder(57).append("JSON forbids NaN and infinities: ").append(asDouble).toString());
        }
        String valueOf = String.valueOf(zzaqq.NUMBER);
        String valueOf2 = String.valueOf(bq);
        throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 18 + String.valueOf(valueOf2).length()).append("Expected ").append(valueOf).append(" but was ").append(valueOf2).toString());
    }

    @Override // com.google.android.gms.internal.zzaqp
    public int nextInt() {
        zzaqq bq = bq();
        if (bq == zzaqq.NUMBER || bq == zzaqq.STRING) {
            int asInt = ((zzape) br()).getAsInt();
            bs();
            return asInt;
        }
        String valueOf = String.valueOf(zzaqq.NUMBER);
        String valueOf2 = String.valueOf(bq);
        throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 18 + String.valueOf(valueOf2).length()).append("Expected ").append(valueOf).append(" but was ").append(valueOf2).toString());
    }

    @Override // com.google.android.gms.internal.zzaqp
    public long nextLong() {
        zzaqq bq = bq();
        if (bq == zzaqq.NUMBER || bq == zzaqq.STRING) {
            long asLong = ((zzape) br()).getAsLong();
            bs();
            return asLong;
        }
        String valueOf = String.valueOf(zzaqq.NUMBER);
        String valueOf2 = String.valueOf(bq);
        throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 18 + String.valueOf(valueOf2).length()).append("Expected ").append(valueOf).append(" but was ").append(valueOf2).toString());
    }

    @Override // com.google.android.gms.internal.zzaqp
    public String nextName() {
        zza(zzaqq.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) br()).next();
        this.bpN.add(entry.getValue());
        return (String) entry.getKey();
    }

    @Override // com.google.android.gms.internal.zzaqp
    public void nextNull() {
        zza(zzaqq.NULL);
        bs();
    }

    @Override // com.google.android.gms.internal.zzaqp
    public String nextString() {
        zzaqq bq = bq();
        if (bq == zzaqq.STRING || bq == zzaqq.NUMBER) {
            return ((zzape) bs()).aU();
        }
        String valueOf = String.valueOf(zzaqq.STRING);
        String valueOf2 = String.valueOf(bq);
        throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 18 + String.valueOf(valueOf2).length()).append("Expected ").append(valueOf).append(" but was ").append(valueOf2).toString());
    }

    @Override // com.google.android.gms.internal.zzaqp
    public void skipValue() {
        if (bq() == zzaqq.NAME) {
            nextName();
        } else {
            bs();
        }
    }

    @Override // com.google.android.gms.internal.zzaqp
    public String toString() {
        return getClass().getSimpleName();
    }
}
