package com.google.android.gms.internal;

import com.google.android.gms.internal.zzaqj;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/* access modifiers changed from: package-private */
public final class zzaqm<T> extends zzapk<T> {
    private final zzapk<T> bol;
    private final zzaos bqh;
    private final Type bqi;

    zzaqm(zzaos zzaos, zzapk<T> zzapk, Type type) {
        this.bqh = zzaos;
        this.bol = zzapk;
        this.bqi = type;
    }

    private Type zzb(Type type, Object obj) {
        return obj != null ? (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) ? obj.getClass() : type : type;
    }

    @Override // com.google.android.gms.internal.zzapk
    public void zza(zzaqr zzaqr, T t) {
        zzapk<T> zzapk = this.bol;
        Type zzb = zzb(this.bqi, t);
        if (zzb != this.bqi) {
            zzapk = this.bqh.zza(zzaqo.zzl(zzb));
            if ((zzapk instanceof zzaqj.zza) && !(this.bol instanceof zzaqj.zza)) {
                zzapk = this.bol;
            }
        }
        zzapk.zza(zzaqr, t);
    }

    @Override // com.google.android.gms.internal.zzapk
    public T zzb(zzaqp zzaqp) {
        return this.bol.zzb(zzaqp);
    }
}
