package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzasa {
    protected volatile int btP = -1;

    public static final <T extends zzasa> T zza(T t, byte[] bArr) {
        return (T) zzb(t, bArr, 0, bArr.length);
    }

    public static final void zza(zzasa zzasa, byte[] bArr, int i, int i2) {
        try {
            zzart zzc = zzart.zzc(bArr, i, i2);
            zzasa.zza(zzc);
            zzc.cm();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public static final <T extends zzasa> T zzb(T t, byte[] bArr, int i, int i2) {
        try {
            zzars zzb = zzars.zzb(bArr, i, i2);
            t.zzb(zzb);
            zzb.zzagq(0);
            return t;
        } catch (zzarz e) {
            throw e;
        } catch (IOException e2) {
            throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).");
        }
    }

    public static final byte[] zzf(zzasa zzasa) {
        byte[] bArr = new byte[zzasa.cz()];
        zza(zzasa, bArr, 0, bArr.length);
        return bArr;
    }

    /* renamed from: co */
    public zzasa clone() {
        return (zzasa) super.clone();
    }

    public int cy() {
        if (this.btP < 0) {
            cz();
        }
        return this.btP;
    }

    public int cz() {
        int zzx = zzx();
        this.btP = zzx;
        return zzx;
    }

    public String toString() {
        return zzasb.zzg(this);
    }

    public void zza(zzart zzart) {
    }

    public abstract zzasa zzb(zzars zzars);

    /* access modifiers changed from: protected */
    public int zzx() {
        return 0;
    }
}
