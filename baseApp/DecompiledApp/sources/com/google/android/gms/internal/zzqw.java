package com.google.android.gms.internal;

import android.app.Activity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zza;

public class zzqw extends zzqp {
    private zzrh xy;
    private final zza<zzql<?>> zx = new zza<>();

    private zzqw(zzrp zzrp) {
        super(zzrp);
        this.Bf.zza("ConnectionlessLifecycleHelper", this);
    }

    public static void zza(Activity activity, zzrh zzrh, zzql<?> zzql) {
        zzrp zzs = zzs(activity);
        zzqw zzqw = (zzqw) zzs.zza("ConnectionlessLifecycleHelper", zzqw.class);
        if (zzqw == null) {
            zzqw = new zzqw(zzs);
        }
        zzqw.xy = zzrh;
        zzqw.zza(zzql);
        zzrh.zza(zzqw);
    }

    private void zza(zzql<?> zzql) {
        zzaa.zzb(zzql, "ApiKey cannot be null");
        this.zx.add(zzql);
    }

    @Override // com.google.android.gms.internal.zzqp, com.google.android.gms.internal.zzro
    public void onStart() {
        super.onStart();
        if (!this.zx.isEmpty()) {
            this.xy.zza(this);
        }
    }

    @Override // com.google.android.gms.internal.zzqp, com.google.android.gms.internal.zzro
    public void onStop() {
        super.onStop();
        this.xy.zzb(this);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.zzqp
    public void zza(ConnectionResult connectionResult, int i) {
        this.xy.zza(connectionResult, i);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.zzqp
    public void zzarm() {
        this.xy.zzarm();
    }

    /* access modifiers changed from: package-private */
    public zza<zzql<?>> zzasl() {
        return this.zx;
    }
}
