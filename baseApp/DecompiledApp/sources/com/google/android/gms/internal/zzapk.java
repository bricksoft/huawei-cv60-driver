package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzapk<T> {
    public abstract void zza(zzaqr zzaqr, T t);

    public abstract T zzb(zzaqp zzaqp);

    public final zzaoy zzcn(T t) {
        try {
            zzaqg zzaqg = new zzaqg();
            zza(zzaqg, t);
            return zzaqg.bu();
        } catch (IOException e) {
            throw new zzaoz(e);
        }
    }
}
