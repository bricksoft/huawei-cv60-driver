package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzaa;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class zzqm extends zzqp {
    private final SparseArray<zza> yq = new SparseArray<>();

    /* access modifiers changed from: private */
    public class zza implements GoogleApiClient.OnConnectionFailedListener {
        public final int yr;
        public final GoogleApiClient ys;
        public final GoogleApiClient.OnConnectionFailedListener yt;

        public zza(int i, GoogleApiClient googleApiClient, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
            this.yr = i;
            this.ys = googleApiClient;
            this.yt = onConnectionFailedListener;
            googleApiClient.registerConnectionFailedListener(this);
        }

        public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.append((CharSequence) str).append("GoogleApiClient #").print(this.yr);
            printWriter.println(":");
            this.ys.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        }

        @Override // com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            String valueOf = String.valueOf(connectionResult);
            Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf).length() + 27).append("beginFailureResolution for ").append(valueOf).toString());
            zzqm.this.zzb(connectionResult, this.yr);
        }

        public void zzarn() {
            this.ys.unregisterConnectionFailedListener(this);
            this.ys.disconnect();
        }
    }

    private zzqm(zzrp zzrp) {
        super(zzrp);
        this.Bf.zza("AutoManageHelper", this);
    }

    public static zzqm zza(zzrn zzrn) {
        zzrp zzc = zzc(zzrn);
        zzqm zzqm = (zzqm) zzc.zza("AutoManageHelper", zzqm.class);
        return zzqm != null ? zzqm : new zzqm(zzc);
    }

    @Override // com.google.android.gms.internal.zzro
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.yq.size(); i++) {
            this.yq.valueAt(i).dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    @Override // com.google.android.gms.internal.zzqp, com.google.android.gms.internal.zzro
    public void onStart() {
        super.onStart();
        boolean z = this.mStarted;
        String valueOf = String.valueOf(this.yq);
        Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf).length() + 14).append("onStart ").append(z).append(" ").append(valueOf).toString());
        if (!this.yz) {
            for (int i = 0; i < this.yq.size(); i++) {
                this.yq.valueAt(i).ys.connect();
            }
        }
    }

    @Override // com.google.android.gms.internal.zzqp, com.google.android.gms.internal.zzro
    public void onStop() {
        super.onStop();
        for (int i = 0; i < this.yq.size(); i++) {
            this.yq.valueAt(i).ys.disconnect();
        }
    }

    public void zza(int i, GoogleApiClient googleApiClient, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        zzaa.zzb(googleApiClient, "GoogleApiClient instance cannot be null");
        zzaa.zza(this.yq.indexOfKey(i) < 0, new StringBuilder(54).append("Already managing a GoogleApiClient with id ").append(i).toString());
        Log.d("AutoManageHelper", new StringBuilder(54).append("starting AutoManage for client ").append(i).append(" ").append(this.mStarted).append(" ").append(this.yz).toString());
        this.yq.put(i, new zza(i, googleApiClient, onConnectionFailedListener));
        if (this.mStarted && !this.yz) {
            String valueOf = String.valueOf(googleApiClient);
            Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf).length() + 11).append("connecting ").append(valueOf).toString());
            googleApiClient.connect();
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.zzqp
    public void zza(ConnectionResult connectionResult, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        zza zza2 = this.yq.get(i);
        if (zza2 != null) {
            zzfs(i);
            GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener = zza2.yt;
            if (onConnectionFailedListener != null) {
                onConnectionFailedListener.onConnectionFailed(connectionResult);
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.zzqp
    public void zzarm() {
        for (int i = 0; i < this.yq.size(); i++) {
            this.yq.valueAt(i).ys.connect();
        }
    }

    public void zzfs(int i) {
        zza zza2 = this.yq.get(i);
        this.yq.remove(i);
        if (zza2 != null) {
            zza2.zzarn();
        }
    }
}
