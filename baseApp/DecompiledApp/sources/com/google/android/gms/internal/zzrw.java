package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzrw<A extends Api.zzb> {
    private final zzrr<?> Bt;

    /* access modifiers changed from: protected */
    public abstract void zza(A a2, TaskCompletionSource<Void> taskCompletionSource);

    public zzrr.zzb<?> zzatz() {
        return this.Bt.zzatz();
    }

    public void zzaua() {
        this.Bt.clear();
    }
}
