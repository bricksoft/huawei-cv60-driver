package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.zza;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzqo;

/* access modifiers changed from: package-private */
public abstract class zzob extends zzqo.zza<ProxyApi.ProxyResult, zzny> {
    public zzob(GoogleApiClient googleApiClient) {
        super(zza.API, googleApiClient);
    }

    /* access modifiers changed from: protected */
    public abstract void zza(Context context, zzoa zzoa);

    /* access modifiers changed from: protected */
    public final void zza(zzny zzny) {
        zza(zzny.getContext(), (zzoa) zzny.zzavg());
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzk */
    public ProxyApi.ProxyResult zzc(Status status) {
        return new zzod(status);
    }
}
