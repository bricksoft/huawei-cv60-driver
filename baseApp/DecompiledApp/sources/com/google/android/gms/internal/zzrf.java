package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.zzc;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.internal.zzrm;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class zzrf implements zzqs, zzrm {
    private ConnectionResult AA = null;
    int AB;
    final zzrm.zza AC;
    final Map<Api.zzc<?>, Api.zze> Aj;
    private final Condition Aw;
    private final zzb Ax;
    final Map<Api.zzc<?>, ConnectionResult> Ay = new HashMap();
    private volatile zzre Az;
    private final Context mContext;
    final Api.zza<? extends zzxp, zzxq> xQ;
    final zzrd yW;
    final zzf zP;
    private final Lock zg;
    final Map<Api<?>, Integer> zk;
    private final zzc zm;

    static abstract class zza {
        private final zzre AD;

        protected zza(zzre zzre) {
            this.AD = zzre;
        }

        /* access modifiers changed from: protected */
        public abstract void zzaso();

        public final void zzc(zzrf zzrf) {
            zzrf.zg.lock();
            try {
                if (zzrf.Az == this.AD) {
                    zzaso();
                    zzrf.zg.unlock();
                }
            } finally {
                zzrf.zg.unlock();
            }
        }
    }

    final class zzb extends Handler {
        zzb(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    ((zza) message.obj).zzc(zzrf.this);
                    return;
                case 2:
                    throw ((RuntimeException) message.obj);
                default:
                    Log.w("GACStateManager", new StringBuilder(31).append("Unknown message id: ").append(message.what).toString());
                    return;
            }
        }
    }

    public zzrf(Context context, zzrd zzrd, Lock lock, Looper looper, zzc zzc, Map<Api.zzc<?>, Api.zze> map, zzf zzf, Map<Api<?>, Integer> map2, Api.zza<? extends zzxp, zzxq> zza2, ArrayList<zzqr> arrayList, zzrm.zza zza3) {
        this.mContext = context;
        this.zg = lock;
        this.zm = zzc;
        this.Aj = map;
        this.zP = zzf;
        this.zk = map2;
        this.xQ = zza2;
        this.yW = zzrd;
        this.AC = zza3;
        Iterator<zzqr> it = arrayList.iterator();
        while (it.hasNext()) {
            it.next().zza(this);
        }
        this.Ax = new zzb(looper);
        this.Aw = lock.newCondition();
        this.Az = new zzrc(this);
    }

    @Override // com.google.android.gms.internal.zzrm
    public ConnectionResult blockingConnect() {
        connect();
        while (isConnecting()) {
            try {
                this.Aw.await();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return new ConnectionResult(15, null);
            }
        }
        return isConnected() ? ConnectionResult.wO : this.AA != null ? this.AA : new ConnectionResult(13, null);
    }

    @Override // com.google.android.gms.internal.zzrm
    public ConnectionResult blockingConnect(long j, TimeUnit timeUnit) {
        connect();
        long nanos = timeUnit.toNanos(j);
        while (isConnecting()) {
            if (nanos <= 0) {
                try {
                    disconnect();
                    return new ConnectionResult(14, null);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    return new ConnectionResult(15, null);
                }
            } else {
                nanos = this.Aw.awaitNanos(nanos);
            }
        }
        return isConnected() ? ConnectionResult.wO : this.AA != null ? this.AA : new ConnectionResult(13, null);
    }

    @Override // com.google.android.gms.internal.zzrm
    public void connect() {
        this.Az.connect();
    }

    @Override // com.google.android.gms.internal.zzrm
    public void disconnect() {
        if (this.Az.disconnect()) {
            this.Ay.clear();
        }
    }

    @Override // com.google.android.gms.internal.zzrm
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append((CharSequence) str).append("mState=").println(this.Az);
        for (Api<?> api : this.zk.keySet()) {
            printWriter.append((CharSequence) str).append((CharSequence) api.getName()).println(":");
            this.Aj.get(api.zzaqv()).dump(concat, fileDescriptor, printWriter, strArr);
        }
    }

    @Override // com.google.android.gms.internal.zzrm
    @Nullable
    public ConnectionResult getConnectionResult(@NonNull Api<?> api) {
        Api.zzc<?> zzaqv = api.zzaqv();
        if (this.Aj.containsKey(zzaqv)) {
            if (this.Aj.get(zzaqv).isConnected()) {
                return ConnectionResult.wO;
            }
            if (this.Ay.containsKey(zzaqv)) {
                return this.Ay.get(zzaqv);
            }
        }
        return null;
    }

    @Override // com.google.android.gms.internal.zzrm
    public boolean isConnected() {
        return this.Az instanceof zzra;
    }

    @Override // com.google.android.gms.internal.zzrm
    public boolean isConnecting() {
        return this.Az instanceof zzrb;
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
    public void onConnected(@Nullable Bundle bundle) {
        this.zg.lock();
        try {
            this.Az.onConnected(bundle);
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
    public void onConnectionSuspended(int i) {
        this.zg.lock();
        try {
            this.Az.onConnectionSuspended(i);
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.internal.zzrm
    public <A extends Api.zzb, R extends Result, T extends zzqo.zza<R, A>> T zza(@NonNull T t) {
        t.zzarv();
        return (T) this.Az.zza(t);
    }

    @Override // com.google.android.gms.internal.zzqs
    public void zza(@NonNull ConnectionResult connectionResult, @NonNull Api<?> api, int i) {
        this.zg.lock();
        try {
            this.Az.zza(connectionResult, api, i);
        } finally {
            this.zg.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zza zza2) {
        this.Ax.sendMessage(this.Ax.obtainMessage(1, zza2));
    }

    /* access modifiers changed from: package-private */
    public void zza(RuntimeException runtimeException) {
        this.Ax.sendMessage(this.Ax.obtainMessage(2, runtimeException));
    }

    @Override // com.google.android.gms.internal.zzrm
    public boolean zza(zzsa zzsa) {
        return false;
    }

    @Override // com.google.android.gms.internal.zzrm
    public void zzard() {
    }

    @Override // com.google.android.gms.internal.zzrm
    public void zzarz() {
        if (isConnected()) {
            ((zzra) this.Az).zzasn();
        }
    }

    /* access modifiers changed from: package-private */
    public void zzatc() {
        this.zg.lock();
        try {
            this.Az = new zzrb(this, this.zP, this.zk, this.zm, this.xQ, this.zg, this.mContext);
            this.Az.begin();
            this.Aw.signalAll();
        } finally {
            this.zg.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public void zzatd() {
        this.zg.lock();
        try {
            this.yW.zzasz();
            this.Az = new zzra(this);
            this.Az.begin();
            this.Aw.signalAll();
        } finally {
            this.zg.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public void zzate() {
        for (Api.zze zze : this.Aj.values()) {
            zze.disconnect();
        }
    }

    @Override // com.google.android.gms.internal.zzrm
    public <A extends Api.zzb, T extends zzqo.zza<? extends Result, A>> T zzb(@NonNull T t) {
        t.zzarv();
        return (T) this.Az.zzb(t);
    }

    /* access modifiers changed from: package-private */
    public void zzh(ConnectionResult connectionResult) {
        this.zg.lock();
        try {
            this.AA = connectionResult;
            this.Az = new zzrc(this);
            this.Az.begin();
            this.Aw.signalAll();
        } finally {
            this.zg.unlock();
        }
    }
}
