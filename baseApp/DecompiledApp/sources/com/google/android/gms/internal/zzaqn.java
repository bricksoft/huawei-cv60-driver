package com.google.android.gms.internal;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

public final class zzaqn {
    public static final zzapl bqA = zza(Number.class, bqz);
    public static final zzapk<Character> bqB = new zzapk<Character>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass4 */

        public void zza(zzaqr zzaqr, Character ch) {
            zzaqr.zzut(ch == null ? null : String.valueOf(ch));
        }

        /* renamed from: zzp */
        public Character zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            String nextString = zzaqp.nextString();
            if (nextString.length() == 1) {
                return Character.valueOf(nextString.charAt(0));
            }
            String valueOf = String.valueOf(nextString);
            throw new zzaph(valueOf.length() != 0 ? "Expecting character, got: ".concat(valueOf) : new String("Expecting character, got: "));
        }
    };
    public static final zzapl bqC = zza(Character.TYPE, Character.class, bqB);
    public static final zzapk<String> bqD = new zzapk<String>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass5 */

        public void zza(zzaqr zzaqr, String str) {
            zzaqr.zzut(str);
        }

        /* renamed from: zzq */
        public String zzb(zzaqp zzaqp) {
            zzaqq bq = zzaqp.bq();
            if (bq != zzaqq.NULL) {
                return bq == zzaqq.BOOLEAN ? Boolean.toString(zzaqp.nextBoolean()) : zzaqp.nextString();
            }
            zzaqp.nextNull();
            return null;
        }
    };
    public static final zzapk<BigDecimal> bqE = new zzapk<BigDecimal>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass6 */

        public void zza(zzaqr zzaqr, BigDecimal bigDecimal) {
            zzaqr.zza(bigDecimal);
        }

        /* renamed from: zzr */
        public BigDecimal zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            try {
                return new BigDecimal(zzaqp.nextString());
            } catch (NumberFormatException e) {
                throw new zzaph(e);
            }
        }
    };
    public static final zzapk<BigInteger> bqF = new zzapk<BigInteger>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass7 */

        public void zza(zzaqr zzaqr, BigInteger bigInteger) {
            zzaqr.zza(bigInteger);
        }

        /* renamed from: zzs */
        public BigInteger zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            try {
                return new BigInteger(zzaqp.nextString());
            } catch (NumberFormatException e) {
                throw new zzaph(e);
            }
        }
    };
    public static final zzapl bqG = zza(String.class, bqD);
    public static final zzapk<StringBuilder> bqH = new zzapk<StringBuilder>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass8 */

        public void zza(zzaqr zzaqr, StringBuilder sb) {
            zzaqr.zzut(sb == null ? null : sb.toString());
        }

        /* renamed from: zzt */
        public StringBuilder zzb(zzaqp zzaqp) {
            if (zzaqp.bq() != zzaqq.NULL) {
                return new StringBuilder(zzaqp.nextString());
            }
            zzaqp.nextNull();
            return null;
        }
    };
    public static final zzapl bqI = zza(StringBuilder.class, bqH);
    public static final zzapk<StringBuffer> bqJ = new zzapk<StringBuffer>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass9 */

        public void zza(zzaqr zzaqr, StringBuffer stringBuffer) {
            zzaqr.zzut(stringBuffer == null ? null : stringBuffer.toString());
        }

        /* renamed from: zzu */
        public StringBuffer zzb(zzaqp zzaqp) {
            if (zzaqp.bq() != zzaqq.NULL) {
                return new StringBuffer(zzaqp.nextString());
            }
            zzaqp.nextNull();
            return null;
        }
    };
    public static final zzapl bqK = zza(StringBuffer.class, bqJ);
    public static final zzapk<URL> bqL = new zzapk<URL>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass10 */

        public void zza(zzaqr zzaqr, URL url) {
            zzaqr.zzut(url == null ? null : url.toExternalForm());
        }

        /* renamed from: zzv */
        public URL zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            String nextString = zzaqp.nextString();
            if (!"null".equals(nextString)) {
                return new URL(nextString);
            }
            return null;
        }
    };
    public static final zzapl bqM = zza(URL.class, bqL);
    public static final zzapk<URI> bqN = new zzapk<URI>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass11 */

        public void zza(zzaqr zzaqr, URI uri) {
            zzaqr.zzut(uri == null ? null : uri.toASCIIString());
        }

        /* renamed from: zzw */
        public URI zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            try {
                String nextString = zzaqp.nextString();
                if (!"null".equals(nextString)) {
                    return new URI(nextString);
                }
                return null;
            } catch (URISyntaxException e) {
                throw new zzaoz(e);
            }
        }
    };
    public static final zzapl bqO = zza(URI.class, bqN);
    public static final zzapk<InetAddress> bqP = new zzapk<InetAddress>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass13 */

        public void zza(zzaqr zzaqr, InetAddress inetAddress) {
            zzaqr.zzut(inetAddress == null ? null : inetAddress.getHostAddress());
        }

        /* renamed from: zzy */
        public InetAddress zzb(zzaqp zzaqp) {
            if (zzaqp.bq() != zzaqq.NULL) {
                return InetAddress.getByName(zzaqp.nextString());
            }
            zzaqp.nextNull();
            return null;
        }
    };
    public static final zzapl bqQ = zzb(InetAddress.class, bqP);
    public static final zzapk<UUID> bqR = new zzapk<UUID>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass14 */

        public void zza(zzaqr zzaqr, UUID uuid) {
            zzaqr.zzut(uuid == null ? null : uuid.toString());
        }

        /* renamed from: zzz */
        public UUID zzb(zzaqp zzaqp) {
            if (zzaqp.bq() != zzaqq.NULL) {
                return UUID.fromString(zzaqp.nextString());
            }
            zzaqp.nextNull();
            return null;
        }
    };
    public static final zzapl bqS = zza(UUID.class, bqR);
    public static final zzapl bqT = new zzapl() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass15 */

        @Override // com.google.android.gms.internal.zzapl
        public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
            if (zzaqo.bB() != Timestamp.class) {
                return null;
            }
            final zzapk<T> zzk = zzaos.zzk(Date.class);
            return new zzapk<Timestamp>() {
                /* class com.google.android.gms.internal.zzaqn.AnonymousClass15.AnonymousClass1 */

                public void zza(zzaqr zzaqr, Timestamp timestamp) {
                    zzk.zza(zzaqr, timestamp);
                }

                /* renamed from: zzaa */
                public Timestamp zzb(zzaqp zzaqp) {
                    Date date = (Date) zzk.zzb(zzaqp);
                    if (date != null) {
                        return new Timestamp(date.getTime());
                    }
                    return null;
                }
            };
        }
    };
    public static final zzapk<Calendar> bqU = new zzapk<Calendar>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass16 */

        public void zza(zzaqr zzaqr, Calendar calendar) {
            if (calendar == null) {
                zzaqr.bA();
                return;
            }
            zzaqr.by();
            zzaqr.zzus("year");
            zzaqr.zzcs((long) calendar.get(1));
            zzaqr.zzus("month");
            zzaqr.zzcs((long) calendar.get(2));
            zzaqr.zzus("dayOfMonth");
            zzaqr.zzcs((long) calendar.get(5));
            zzaqr.zzus("hourOfDay");
            zzaqr.zzcs((long) calendar.get(11));
            zzaqr.zzus("minute");
            zzaqr.zzcs((long) calendar.get(12));
            zzaqr.zzus("second");
            zzaqr.zzcs((long) calendar.get(13));
            zzaqr.bz();
        }

        /* renamed from: zzab */
        public Calendar zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            zzaqp.beginObject();
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (zzaqp.bq() != zzaqq.END_OBJECT) {
                String nextName = zzaqp.nextName();
                int nextInt = zzaqp.nextInt();
                if ("year".equals(nextName)) {
                    i6 = nextInt;
                } else if ("month".equals(nextName)) {
                    i5 = nextInt;
                } else if ("dayOfMonth".equals(nextName)) {
                    i4 = nextInt;
                } else if ("hourOfDay".equals(nextName)) {
                    i3 = nextInt;
                } else if ("minute".equals(nextName)) {
                    i2 = nextInt;
                } else if ("second".equals(nextName)) {
                    i = nextInt;
                }
            }
            zzaqp.endObject();
            return new GregorianCalendar(i6, i5, i4, i3, i2, i);
        }
    };
    public static final zzapl bqV = zzb(Calendar.class, GregorianCalendar.class, bqU);
    public static final zzapk<Locale> bqW = new zzapk<Locale>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass17 */

        public void zza(zzaqr zzaqr, Locale locale) {
            zzaqr.zzut(locale == null ? null : locale.toString());
        }

        /* renamed from: zzac */
        public Locale zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(zzaqp.nextString(), "_");
            String nextToken = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String nextToken2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String nextToken3 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            return (nextToken2 == null && nextToken3 == null) ? new Locale(nextToken) : nextToken3 == null ? new Locale(nextToken, nextToken2) : new Locale(nextToken, nextToken2, nextToken3);
        }
    };
    public static final zzapl bqX = zza(Locale.class, bqW);
    public static final zzapk<zzaoy> bqY = new zzapk<zzaoy>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass18 */

        public void zza(zzaqr zzaqr, zzaoy zzaoy) {
            if (zzaoy == null || zzaoy.aY()) {
                zzaqr.bA();
            } else if (zzaoy.aX()) {
                zzape bb = zzaoy.bb();
                if (bb.be()) {
                    zzaqr.zza(bb.aT());
                } else if (bb.bd()) {
                    zzaqr.zzdh(bb.getAsBoolean());
                } else {
                    zzaqr.zzut(bb.aU());
                }
            } else if (zzaoy.aV()) {
                zzaqr.bw();
                Iterator<zzaoy> it = zzaoy.ba().iterator();
                while (it.hasNext()) {
                    zza(zzaqr, it.next());
                }
                zzaqr.bx();
            } else if (zzaoy.aW()) {
                zzaqr.by();
                for (Map.Entry<String, zzaoy> entry : zzaoy.aZ().entrySet()) {
                    zzaqr.zzus(entry.getKey());
                    zza(zzaqr, entry.getValue());
                }
                zzaqr.bz();
            } else {
                String valueOf = String.valueOf(zzaoy.getClass());
                throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 15).append("Couldn't write ").append(valueOf).toString());
            }
        }

        /* renamed from: zzad */
        public zzaoy zzb(zzaqp zzaqp) {
            switch (AnonymousClass26.bpW[zzaqp.bq().ordinal()]) {
                case 1:
                    return new zzape((Number) new zzapv(zzaqp.nextString()));
                case 2:
                    return new zzape(Boolean.valueOf(zzaqp.nextBoolean()));
                case 3:
                    return new zzape(zzaqp.nextString());
                case 4:
                    zzaqp.nextNull();
                    return zzapa.bou;
                case 5:
                    zzaov zzaov = new zzaov();
                    zzaqp.beginArray();
                    while (zzaqp.hasNext()) {
                        zzaov.zzc((zzaoy) zzb(zzaqp));
                    }
                    zzaqp.endArray();
                    return zzaov;
                case 6:
                    zzapb zzapb = new zzapb();
                    zzaqp.beginObject();
                    while (zzaqp.hasNext()) {
                        zzapb.zza(zzaqp.nextName(), (zzaoy) zzb(zzaqp));
                    }
                    zzaqp.endObject();
                    return zzapb;
                default:
                    throw new IllegalArgumentException();
            }
        }
    };
    public static final zzapl bqZ = zzb(zzaoy.class, bqY);
    public static final zzapk<Class> bqj = new zzapk<Class>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass1 */

        public void zza(zzaqr zzaqr, Class cls) {
            if (cls == null) {
                zzaqr.bA();
            } else {
                String valueOf = String.valueOf(cls.getName());
                throw new UnsupportedOperationException(new StringBuilder(String.valueOf(valueOf).length() + 76).append("Attempted to serialize java.lang.Class: ").append(valueOf).append(". Forgot to register a type adapter?").toString());
            }
        }

        /* renamed from: zzo */
        public Class zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        }
    };
    public static final zzapl bqk = zza(Class.class, bqj);
    public static final zzapk<BitSet> bql = new zzapk<BitSet>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass12 */

        public void zza(zzaqr zzaqr, BitSet bitSet) {
            if (bitSet == null) {
                zzaqr.bA();
                return;
            }
            zzaqr.bw();
            for (int i = 0; i < bitSet.length(); i++) {
                zzaqr.zzcs((long) (bitSet.get(i) ? 1 : 0));
            }
            zzaqr.bx();
        }

        /* renamed from: zzx */
        public BitSet zzb(zzaqp zzaqp) {
            boolean z;
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            BitSet bitSet = new BitSet();
            zzaqp.beginArray();
            zzaqq bq = zzaqp.bq();
            int i = 0;
            while (bq != zzaqq.END_ARRAY) {
                switch (AnonymousClass26.bpW[bq.ordinal()]) {
                    case 1:
                        if (zzaqp.nextInt() == 0) {
                            z = false;
                            break;
                        } else {
                            z = true;
                            break;
                        }
                    case 2:
                        z = zzaqp.nextBoolean();
                        break;
                    case 3:
                        String nextString = zzaqp.nextString();
                        try {
                            if (Integer.parseInt(nextString) == 0) {
                                z = false;
                                break;
                            } else {
                                z = true;
                                break;
                            }
                        } catch (NumberFormatException e) {
                            String valueOf = String.valueOf(nextString);
                            throw new zzaph(valueOf.length() != 0 ? "Error: Expecting: bitset number value (1, 0), Found: ".concat(valueOf) : new String("Error: Expecting: bitset number value (1, 0), Found: "));
                        }
                    default:
                        String valueOf2 = String.valueOf(bq);
                        throw new zzaph(new StringBuilder(String.valueOf(valueOf2).length() + 27).append("Invalid bitset value type: ").append(valueOf2).toString());
                }
                if (z) {
                    bitSet.set(i);
                }
                i++;
                bq = zzaqp.bq();
            }
            zzaqp.endArray();
            return bitSet;
        }
    };
    public static final zzapl bqm = zza(BitSet.class, bql);
    public static final zzapk<Boolean> bqn = new zzapk<Boolean>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass23 */

        public void zza(zzaqr zzaqr, Boolean bool) {
            if (bool == null) {
                zzaqr.bA();
            } else {
                zzaqr.zzdh(bool.booleanValue());
            }
        }

        /* renamed from: zzae */
        public Boolean zzb(zzaqp zzaqp) {
            if (zzaqp.bq() != zzaqq.NULL) {
                return zzaqp.bq() == zzaqq.STRING ? Boolean.valueOf(Boolean.parseBoolean(zzaqp.nextString())) : Boolean.valueOf(zzaqp.nextBoolean());
            }
            zzaqp.nextNull();
            return null;
        }
    };
    public static final zzapk<Boolean> bqo = new zzapk<Boolean>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass27 */

        public void zza(zzaqr zzaqr, Boolean bool) {
            zzaqr.zzut(bool == null ? "null" : bool.toString());
        }

        /* renamed from: zzae */
        public Boolean zzb(zzaqp zzaqp) {
            if (zzaqp.bq() != zzaqq.NULL) {
                return Boolean.valueOf(zzaqp.nextString());
            }
            zzaqp.nextNull();
            return null;
        }
    };
    public static final zzapl bqp = zza(Boolean.TYPE, Boolean.class, bqn);
    public static final zzapk<Number> bqq = new zzapk<Number>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass28 */

        public void zza(zzaqr zzaqr, Number number) {
            zzaqr.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            try {
                return Byte.valueOf((byte) zzaqp.nextInt());
            } catch (NumberFormatException e) {
                throw new zzaph(e);
            }
        }
    };
    public static final zzapl bqr = zza(Byte.TYPE, Byte.class, bqq);
    public static final zzapk<Number> bqs = new zzapk<Number>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass29 */

        public void zza(zzaqr zzaqr, Number number) {
            zzaqr.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            try {
                return Short.valueOf((short) zzaqp.nextInt());
            } catch (NumberFormatException e) {
                throw new zzaph(e);
            }
        }
    };
    public static final zzapl bqt = zza(Short.TYPE, Short.class, bqs);
    public static final zzapk<Number> bqu = new zzapk<Number>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass30 */

        public void zza(zzaqr zzaqr, Number number) {
            zzaqr.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            try {
                return Integer.valueOf(zzaqp.nextInt());
            } catch (NumberFormatException e) {
                throw new zzaph(e);
            }
        }
    };
    public static final zzapl bqv = zza(Integer.TYPE, Integer.class, bqu);
    public static final zzapk<Number> bqw = new zzapk<Number>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass31 */

        public void zza(zzaqr zzaqr, Number number) {
            zzaqr.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            try {
                return Long.valueOf(zzaqp.nextLong());
            } catch (NumberFormatException e) {
                throw new zzaph(e);
            }
        }
    };
    public static final zzapk<Number> bqx = new zzapk<Number>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass32 */

        public void zza(zzaqr zzaqr, Number number) {
            zzaqr.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaqp zzaqp) {
            if (zzaqp.bq() != zzaqq.NULL) {
                return Float.valueOf((float) zzaqp.nextDouble());
            }
            zzaqp.nextNull();
            return null;
        }
    };
    public static final zzapk<Number> bqy = new zzapk<Number>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass2 */

        public void zza(zzaqr zzaqr, Number number) {
            zzaqr.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaqp zzaqp) {
            if (zzaqp.bq() != zzaqq.NULL) {
                return Double.valueOf(zzaqp.nextDouble());
            }
            zzaqp.nextNull();
            return null;
        }
    };
    public static final zzapk<Number> bqz = new zzapk<Number>() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass3 */

        public void zza(zzaqr zzaqr, Number number) {
            zzaqr.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaqp zzaqp) {
            zzaqq bq = zzaqp.bq();
            switch (bq) {
                case NUMBER:
                    return new zzapv(zzaqp.nextString());
                case BOOLEAN:
                case STRING:
                default:
                    String valueOf = String.valueOf(bq);
                    throw new zzaph(new StringBuilder(String.valueOf(valueOf).length() + 23).append("Expecting number, got: ").append(valueOf).toString());
                case NULL:
                    zzaqp.nextNull();
                    return null;
            }
        }
    };
    public static final zzapl bra = new zzapl() {
        /* class com.google.android.gms.internal.zzaqn.AnonymousClass19 */

        @Override // com.google.android.gms.internal.zzapl
        public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
            Class<? super Object> bB = zzaqo.bB();
            if (!Enum.class.isAssignableFrom(bB) || bB == Enum.class) {
                return null;
            }
            if (!bB.isEnum()) {
                bB = bB.getSuperclass();
            }
            return new zza(bB);
        }
    };

    private static final class zza<T extends Enum<T>> extends zzapk<T> {
        private final Map<String, T> brk = new HashMap();
        private final Map<T, String> brl = new HashMap();

        public zza(Class<T> cls) {
            try {
                T[] enumConstants = cls.getEnumConstants();
                for (T t : enumConstants) {
                    String name = t.name();
                    zzapn zzapn = (zzapn) cls.getField(name).getAnnotation(zzapn.class);
                    if (zzapn != null) {
                        name = zzapn.value();
                        String[] bh = zzapn.bh();
                        for (String str : bh) {
                            this.brk.put(str, t);
                        }
                    }
                    this.brk.put(name, t);
                    this.brl.put(t, name);
                }
            } catch (NoSuchFieldException e) {
                throw new AssertionError();
            }
        }

        public void zza(zzaqr zzaqr, T t) {
            zzaqr.zzut(t == null ? null : this.brl.get(t));
        }

        /* renamed from: zzaf */
        public T zzb(zzaqp zzaqp) {
            if (zzaqp.bq() != zzaqq.NULL) {
                return this.brk.get(zzaqp.nextString());
            }
            zzaqp.nextNull();
            return null;
        }
    }

    public static <TT> zzapl zza(final zzaqo<TT> zzaqo, final zzapk<TT> zzapk) {
        return new zzapl() {
            /* class com.google.android.gms.internal.zzaqn.AnonymousClass20 */

            @Override // com.google.android.gms.internal.zzapl
            public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
                if (zzaqo.equals(zzaqo)) {
                    return zzapk;
                }
                return null;
            }
        };
    }

    public static <TT> zzapl zza(final Class<TT> cls, final zzapk<TT> zzapk) {
        return new zzapl() {
            /* class com.google.android.gms.internal.zzaqn.AnonymousClass21 */

            public String toString() {
                String valueOf = String.valueOf(cls.getName());
                String valueOf2 = String.valueOf(zzapk);
                return new StringBuilder(String.valueOf(valueOf).length() + 23 + String.valueOf(valueOf2).length()).append("Factory[type=").append(valueOf).append(",adapter=").append(valueOf2).append("]").toString();
            }

            @Override // com.google.android.gms.internal.zzapl
            public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
                if (zzaqo.bB() == cls) {
                    return zzapk;
                }
                return null;
            }
        };
    }

    public static <TT> zzapl zza(final Class<TT> cls, final Class<TT> cls2, final zzapk<? super TT> zzapk) {
        return new zzapl() {
            /* class com.google.android.gms.internal.zzaqn.AnonymousClass22 */

            public String toString() {
                String valueOf = String.valueOf(cls2.getName());
                String valueOf2 = String.valueOf(cls.getName());
                String valueOf3 = String.valueOf(zzapk);
                return new StringBuilder(String.valueOf(valueOf).length() + 24 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length()).append("Factory[type=").append(valueOf).append("+").append(valueOf2).append(",adapter=").append(valueOf3).append("]").toString();
            }

            @Override // com.google.android.gms.internal.zzapl
            public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
                Class<? super T> bB = zzaqo.bB();
                if (bB == cls || bB == cls2) {
                    return zzapk;
                }
                return null;
            }
        };
    }

    public static <TT> zzapl zzb(final Class<TT> cls, final zzapk<TT> zzapk) {
        return new zzapl() {
            /* class com.google.android.gms.internal.zzaqn.AnonymousClass25 */

            public String toString() {
                String valueOf = String.valueOf(cls.getName());
                String valueOf2 = String.valueOf(zzapk);
                return new StringBuilder(String.valueOf(valueOf).length() + 32 + String.valueOf(valueOf2).length()).append("Factory[typeHierarchy=").append(valueOf).append(",adapter=").append(valueOf2).append("]").toString();
            }

            @Override // com.google.android.gms.internal.zzapl
            public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
                if (cls.isAssignableFrom(zzaqo.bB())) {
                    return zzapk;
                }
                return null;
            }
        };
    }

    public static <TT> zzapl zzb(final Class<TT> cls, final Class<? extends TT> cls2, final zzapk<? super TT> zzapk) {
        return new zzapl() {
            /* class com.google.android.gms.internal.zzaqn.AnonymousClass24 */

            public String toString() {
                String valueOf = String.valueOf(cls.getName());
                String valueOf2 = String.valueOf(cls2.getName());
                String valueOf3 = String.valueOf(zzapk);
                return new StringBuilder(String.valueOf(valueOf).length() + 24 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length()).append("Factory[type=").append(valueOf).append("+").append(valueOf2).append(",adapter=").append(valueOf3).append("]").toString();
            }

            @Override // com.google.android.gms.internal.zzapl
            public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
                Class<? super T> bB = zzaqo.bB();
                if (bB == cls || bB == cls2) {
                    return zzapk;
                }
                return null;
            }
        };
    }
}
