package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.zze;
import com.google.android.gms.internal.zzqo;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public class zzsg {
    private static final zzqq<?>[] BE = new zzqq[0];
    public static final Status ym = new Status(8, "The connection to Google Play services was lost");
    private final Map<Api.zzc<?>, Api.zze> Aj;
    final Set<zzqq<?>> BF = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    private final zzb BG = new zzb() {
        /* class com.google.android.gms.internal.zzsg.AnonymousClass1 */

        @Override // com.google.android.gms.internal.zzsg.zzb
        public void zzc(zzqq<?> zzqq) {
            zzsg.this.BF.remove(zzqq);
            if (zzqq.zzarh() != null && zzsg.zza(zzsg.this) != null) {
                zzsg.zza(zzsg.this).remove(zzqq.zzarh().intValue());
            }
        }
    };

    /* access modifiers changed from: private */
    public static class zza implements IBinder.DeathRecipient, zzb {
        private final WeakReference<zzqq<?>> BI;
        private final WeakReference<zze> BJ;
        private final WeakReference<IBinder> BK;

        private zza(zzqq<?> zzqq, zze zze, IBinder iBinder) {
            this.BJ = new WeakReference<>(zze);
            this.BI = new WeakReference<>(zzqq);
            this.BK = new WeakReference<>(iBinder);
        }

        private void zzaug() {
            zzqq<?> zzqq = this.BI.get();
            zze zze = this.BJ.get();
            if (!(zze == null || zzqq == null)) {
                zze.remove(zzqq.zzarh().intValue());
            }
            IBinder iBinder = this.BK.get();
            if (iBinder != null) {
                iBinder.unlinkToDeath(this, 0);
            }
        }

        public void binderDied() {
            zzaug();
        }

        @Override // com.google.android.gms.internal.zzsg.zzb
        public void zzc(zzqq<?> zzqq) {
            zzaug();
        }
    }

    /* access modifiers changed from: package-private */
    public interface zzb {
        void zzc(zzqq<?> zzqq);
    }

    public zzsg(Map<Api.zzc<?>, Api.zze> map) {
        this.Aj = map;
    }

    static /* synthetic */ zze zza(zzsg zzsg) {
        return null;
    }

    private static void zza(zzqq<?> zzqq, zze zze, IBinder iBinder) {
        if (zzqq.isReady()) {
            zzqq.zza(new zza(zzqq, zze, iBinder));
        } else if (iBinder == null || !iBinder.isBinderAlive()) {
            zzqq.zza((zzb) null);
            zzqq.cancel();
            zze.remove(zzqq.zzarh().intValue());
        } else {
            zza zza2 = new zza(zzqq, zze, iBinder);
            zzqq.zza(zza2);
            try {
                iBinder.linkToDeath(zza2, 0);
            } catch (RemoteException e) {
                zzqq.cancel();
                zze.remove(zzqq.zzarh().intValue());
            }
        }
    }

    public void dump(PrintWriter printWriter) {
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.BF.size());
    }

    public void release() {
        zzqq[] zzqqArr = (zzqq[]) this.BF.toArray(BE);
        for (zzqq zzqq : zzqqArr) {
            zzqq.zza((zzb) null);
            if (zzqq.zzarh() != null) {
                zzqq.zzaru();
                zza(zzqq, null, this.Aj.get(((zzqo.zza) zzqq).zzaqv()).zzaqy());
                this.BF.remove(zzqq);
            } else if (zzqq.zzars()) {
                this.BF.remove(zzqq);
            }
        }
    }

    public void zzauf() {
        for (zzqq zzqq : (zzqq[]) this.BF.toArray(BE)) {
            zzqq.zzab(ym);
        }
    }

    /* access modifiers changed from: package-private */
    public void zzb(zzqq<? extends Result> zzqq) {
        this.BF.add(zzqq);
        zzqq.zza(this.BG);
    }
}
