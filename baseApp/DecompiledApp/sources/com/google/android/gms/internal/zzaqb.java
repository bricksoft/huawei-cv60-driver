package com.google.android.gms.internal;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;

public final class zzaqb<E> extends zzapk<Object> {
    public static final zzapl bpG = new zzapl() {
        /* class com.google.android.gms.internal.zzaqb.AnonymousClass1 */

        @Override // com.google.android.gms.internal.zzapl
        public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
            Type bC = zzaqo.bC();
            if (!(bC instanceof GenericArrayType) && (!(bC instanceof Class) || !((Class) bC).isArray())) {
                return null;
            }
            Type zzh = zzapr.zzh(bC);
            return new zzaqb(zzaos, zzaos.zza(zzaqo.zzl(zzh)), zzapr.zzf(zzh));
        }
    };
    private final Class<E> bpH;
    private final zzapk<E> bpI;

    public zzaqb(zzaos zzaos, zzapk<E> zzapk, Class<E> cls) {
        this.bpI = new zzaqm(zzaos, zzapk, cls);
        this.bpH = cls;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.google.android.gms.internal.zzapk<E> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.zzapk
    public void zza(zzaqr zzaqr, Object obj) {
        if (obj == null) {
            zzaqr.bA();
            return;
        }
        zzaqr.bw();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.bpI.zza(zzaqr, Array.get(obj, i));
        }
        zzaqr.bx();
    }

    @Override // com.google.android.gms.internal.zzapk
    public Object zzb(zzaqp zzaqp) {
        if (zzaqp.bq() == zzaqq.NULL) {
            zzaqp.nextNull();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        zzaqp.beginArray();
        while (zzaqp.hasNext()) {
            arrayList.add(this.bpI.zzb(zzaqp));
        }
        zzaqp.endArray();
        Object newInstance = Array.newInstance((Class<?>) this.bpH, arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }
}
