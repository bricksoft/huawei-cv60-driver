package com.google.android.gms.internal;

import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;

public final class zzapz {

    /* access modifiers changed from: private */
    public static final class zza extends Writer {
        private final C0075zza bpA;
        private final Appendable bpz;

        /* renamed from: com.google.android.gms.internal.zzapz$zza$zza  reason: collision with other inner class name */
        static class C0075zza implements CharSequence {
            char[] bpB;

            C0075zza() {
            }

            public char charAt(int i) {
                return this.bpB[i];
            }

            public int length() {
                return this.bpB.length;
            }

            public CharSequence subSequence(int i, int i2) {
                return new String(this.bpB, i, i2 - i);
            }
        }

        private zza(Appendable appendable) {
            this.bpA = new C0075zza();
            this.bpz = appendable;
        }

        @Override // java.io.Closeable, java.io.Writer, java.lang.AutoCloseable
        public void close() {
        }

        @Override // java.io.Writer, java.io.Flushable
        public void flush() {
        }

        @Override // java.io.Writer
        public void write(int i) {
            this.bpz.append((char) i);
        }

        @Override // java.io.Writer
        public void write(char[] cArr, int i, int i2) {
            this.bpA.bpB = cArr;
            this.bpz.append(this.bpA, i, i + i2);
        }
    }

    public static Writer zza(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new zza(appendable);
    }

    public static void zzb(zzaoy zzaoy, zzaqr zzaqr) {
        zzaqn.bqY.zza(zzaqr, zzaoy);
    }

    public static zzaoy zzh(zzaqp zzaqp) {
        boolean z = true;
        try {
            zzaqp.bq();
            z = false;
            return zzaqn.bqY.zzb(zzaqp);
        } catch (EOFException e) {
            if (z) {
                return zzapa.bou;
            }
            throw new zzaph(e);
        } catch (zzaqs e2) {
            throw new zzaph(e2);
        } catch (IOException e3) {
            throw new zzaoz(e3);
        } catch (NumberFormatException e4) {
            throw new zzaph(e4);
        }
    }
}
