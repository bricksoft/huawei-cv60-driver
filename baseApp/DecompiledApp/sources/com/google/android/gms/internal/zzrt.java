package com.google.android.gms.internal;

import android.app.Activity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.CancellationException;

public class zzrt extends zzqp {
    private TaskCompletionSource<Void> yg = new TaskCompletionSource<>();

    private zzrt(zzrp zzrp) {
        super(zzrp);
        this.Bf.zza("GmsAvailabilityHelper", this);
    }

    public static zzrt zzu(Activity activity) {
        zzrp zzs = zzs(activity);
        zzrt zzrt = (zzrt) zzs.zza("GmsAvailabilityHelper", zzrt.class);
        if (zzrt == null) {
            return new zzrt(zzs);
        }
        if (!zzrt.yg.getTask().isComplete()) {
            return zzrt;
        }
        zzrt.yg = new TaskCompletionSource<>();
        return zzrt;
    }

    public Task<Void> getTask() {
        return this.yg.getTask();
    }

    @Override // com.google.android.gms.internal.zzro
    public void onDestroy() {
        super.onDestroy();
        this.yg.setException(new CancellationException("Host activity was destroyed before Google Play services could be made available."));
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.zzqp
    public void zza(ConnectionResult connectionResult, int i) {
        this.yg.setException(zzb.zzk(connectionResult));
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.zzqp
    public void zzarm() {
        int isGooglePlayServicesAvailable = this.xP.isGooglePlayServicesAvailable(this.Bf.zzaty());
        if (isGooglePlayServicesAvailable == 0) {
            this.yg.setResult(null);
        } else {
            zzj(new ConnectionResult(isGooglePlayServicesAvailable, null));
        }
    }

    public void zzj(ConnectionResult connectionResult) {
        zzb(connectionResult, 0);
    }
}
