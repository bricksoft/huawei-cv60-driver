package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzag;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.internal.zzrf;

public class zzra implements zzre {
    private final zzrf zA;
    private boolean zB = false;

    public zzra(zzrf zzrf) {
        this.zA = zzrf;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.google.android.gms.internal.zzqo$zza<? extends com.google.android.gms.common.api.Result, A extends com.google.android.gms.common.api.Api$zzb> */
    /* JADX WARN: Multi-variable type inference failed */
    private <A extends Api.zzb> void zzd(zzqo.zza<? extends Result, A> zza) {
        this.zA.yW.Ap.zzb(zza);
        Api.zze zzb = this.zA.yW.zzb(zza.zzaqv());
        if (zzb.isConnected() || !this.zA.Ay.containsKey(zza.zzaqv())) {
            boolean z = zzb instanceof zzag;
            Api.zzb zzb2 = zzb;
            if (z) {
                zzb2 = ((zzag) zzb).zzawt();
            }
            zza.zzb(zzb2 == 1 ? 1 : 0);
            return;
        }
        zza.zzaa(new Status(17));
    }

    @Override // com.google.android.gms.internal.zzre
    public void begin() {
    }

    @Override // com.google.android.gms.internal.zzre
    public void connect() {
        if (this.zB) {
            this.zB = false;
            this.zA.zza(new zzrf.zza(this) {
                /* class com.google.android.gms.internal.zzra.AnonymousClass2 */

                @Override // com.google.android.gms.internal.zzrf.zza
                public void zzaso() {
                    zzra.this.zA.AC.zzn(null);
                }
            });
        }
    }

    @Override // com.google.android.gms.internal.zzre
    public boolean disconnect() {
        if (this.zB) {
            return false;
        }
        if (this.zA.yW.zzata()) {
            this.zB = true;
            for (zzsf zzsf : this.zA.yW.Ao) {
                zzsf.zzaud();
            }
            return false;
        }
        this.zA.zzh(null);
        return true;
    }

    @Override // com.google.android.gms.internal.zzre
    public void onConnected(Bundle bundle) {
    }

    @Override // com.google.android.gms.internal.zzre
    public void onConnectionSuspended(int i) {
        this.zA.zzh(null);
        this.zA.AC.zzc(i, this.zB);
    }

    @Override // com.google.android.gms.internal.zzre
    public <A extends Api.zzb, R extends Result, T extends zzqo.zza<R, A>> T zza(T t) {
        return (T) zzb(t);
    }

    @Override // com.google.android.gms.internal.zzre
    public void zza(ConnectionResult connectionResult, Api<?> api, int i) {
    }

    /* access modifiers changed from: package-private */
    public void zzasn() {
        if (this.zB) {
            this.zB = false;
            this.zA.yW.Ap.release();
            disconnect();
        }
    }

    @Override // com.google.android.gms.internal.zzre
    public <A extends Api.zzb, T extends zzqo.zza<? extends Result, A>> T zzb(T t) {
        try {
            zzd(t);
        } catch (DeadObjectException e) {
            this.zA.zza(new zzrf.zza(this) {
                /* class com.google.android.gms.internal.zzra.AnonymousClass1 */

                @Override // com.google.android.gms.internal.zzrf.zza
                public void zzaso() {
                    zzra.this.onConnectionSuspended(1);
                }
            });
        }
        return t;
    }
}
