package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.zzp;
import com.google.android.gms.signin.internal.zzd;

public interface zzxp extends Api.zze {
    void connect();

    void zza(zzp zzp, boolean z);

    void zza(zzd zzd);

    void zzcdc();
}
