package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.exoplayer.hls.HlsChunkSource;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.zzc;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzag;
import com.google.android.gms.common.internal.zze;
import com.google.android.gms.internal.zzqj;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class zzrh implements Handler.Callback {
    public static final Status AG = new Status(4, "Sign-out occurred while this API call was in progress.");
    private static final Status AH = new Status(4, "The user must be signed in to make this API call.");
    private static zzrh AJ;
    private static final Object zzaox = new Object();
    private long AI;
    private int AK;
    private final AtomicInteger AL;
    private final AtomicInteger AM;
    private zzqw AN;
    private final Set<zzql<?>> AO;
    private final Set<zzql<?>> AP;
    private long Af;
    private long Ag;
    private final Context mContext;
    private final Handler mHandler;
    private final GoogleApiAvailability xP;
    private final Map<zzql<?>, zza<?>> zj;

    /* access modifiers changed from: package-private */
    public class zza<O extends Api.ApiOptions> implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, zzqs {
        private final Queue<zzqj> AQ = new LinkedList();
        private final Api.zzb AR;
        private final zzqv AS;
        private final Set<zzqn> AT = new HashSet();
        private final Map<zzrr.zzb<?>, zzrx> AU = new HashMap();
        private final int AV;
        private ConnectionResult AW = null;
        private boolean Ae;
        private final Api.zze xB;
        private final zzql<O> xx;

        @WorkerThread
        public zza(zzc<O> zzc) {
            if (zzc.isConnectionlessGoogleApiClient()) {
                this.xB = zzc.getClient();
                zzc.getClientCallbacks().zza(this);
            } else {
                this.xB = zzc.buildApiClient(zzrh.this.mHandler.getLooper(), this, this);
            }
            if (this.xB instanceof zzag) {
                this.AR = ((zzag) this.xB).zzawt();
            } else {
                this.AR = this.xB;
            }
            this.xx = zzc.getApiKey();
            this.AS = new zzqv();
            this.AV = zzc.getInstanceId();
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        @WorkerThread
        private void connect() {
            if (!this.xB.isConnected() && !this.xB.isConnecting()) {
                if (this.xB.zzaqx() && zzrh.this.AK != 0) {
                    zzrh.this.AK = zzrh.this.xP.isGooglePlayServicesAvailable(zzrh.this.mContext);
                    if (zzrh.this.AK != 0) {
                        onConnectionFailed(new ConnectionResult(zzrh.this.AK, null));
                        return;
                    }
                }
                if (this.xB.zzain()) {
                }
                this.xB.zza(new zzb(this.xB, this.xx));
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        @WorkerThread
        private void resume() {
            if (this.Ae) {
                connect();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        @WorkerThread
        private void zzac(Status status) {
            for (zzqj zzqj : this.AQ) {
                zzqj.zzy(status);
            }
            this.AQ.clear();
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        @WorkerThread
        private void zzasx() {
            if (this.Ae) {
                zzatq();
                zzac(zzrh.this.xP.isGooglePlayServicesAvailable(zzrh.this.mContext) == 18 ? new Status(8, "Connection timed out while waiting for Google Play services update to complete.") : new Status(8, "API failed to connect while resuming due to an unknown error."));
                this.xB.disconnect();
            }
        }

        @WorkerThread
        private void zzatq() {
            if (this.Ae) {
                zzrh.this.mHandler.removeMessages(9, this.xx);
                zzrh.this.mHandler.removeMessages(7, this.xx);
                this.Ae = false;
            }
        }

        private void zzatr() {
            zzrh.this.mHandler.removeMessages(10, this.xx);
            zzrh.this.mHandler.sendMessageDelayed(zzrh.this.mHandler.obtainMessage(10, this.xx), zzrh.this.AI);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void zzats() {
            if (this.xB.isConnected() && this.AU.size() == 0) {
                if (this.AS.zzasi()) {
                    zzatr();
                } else {
                    this.xB.disconnect();
                }
            }
        }

        @WorkerThread
        private void zzb(zzqj zzqj) {
            zzqj.zza(this.AS, zzain());
            try {
                zzqj.zza(this);
            } catch (DeadObjectException e) {
                onConnectionSuspended(1);
                this.xB.disconnect();
            }
        }

        @WorkerThread
        private void zzi(ConnectionResult connectionResult) {
            for (zzqn zzqn : this.AT) {
                zzqn.zza(this.xx, connectionResult);
            }
            this.AT.clear();
        }

        public Api.zze getClient() {
            return this.xB;
        }

        public int getInstanceId() {
            return this.AV;
        }

        /* access modifiers changed from: package-private */
        public boolean isConnected() {
            return this.xB.isConnected();
        }

        @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
        @WorkerThread
        public void onConnected(@Nullable Bundle bundle) {
            zzato();
            zzi(ConnectionResult.wO);
            zzatq();
            for (zzrx zzrx : this.AU.values()) {
                try {
                    zzrx.yi.zza(this.AR, new TaskCompletionSource<>());
                } catch (DeadObjectException e) {
                    onConnectionSuspended(1);
                    this.xB.disconnect();
                }
            }
            zzatm();
            zzatr();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:21:0x005a, code lost:
            if (r5.AX.zzc(r6, r5.AV) != false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0062, code lost:
            if (r6.getErrorCode() != 18) goto L_0x0067;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0064, code lost:
            r5.Ae = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0069, code lost:
            if (r5.Ae == false) goto L_0x0088;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x006b, code lost:
            r5.AX.mHandler.sendMessageDelayed(android.os.Message.obtain(r5.AX.mHandler, 7, r5.xx), r5.AX.Ag);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0088, code lost:
            r2 = java.lang.String.valueOf(r5.xx.zzarl());
            zzac(new com.google.android.gms.common.api.Status(17, new java.lang.StringBuilder(java.lang.String.valueOf(r2).length() + 38).append("API: ").append(r2).append(" is not available on this device.").toString()));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
            return;
         */
        @Override // com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
        @android.support.annotation.WorkerThread
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onConnectionFailed(@android.support.annotation.NonNull com.google.android.gms.common.ConnectionResult r6) {
            /*
            // Method dump skipped, instructions count: 193
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzrh.zza.onConnectionFailed(com.google.android.gms.common.ConnectionResult):void");
        }

        @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
        @WorkerThread
        public void onConnectionSuspended(int i) {
            zzato();
            this.Ae = true;
            this.AS.zzask();
            zzrh.this.mHandler.sendMessageDelayed(Message.obtain(zzrh.this.mHandler, 7, this.xx), zzrh.this.Ag);
            zzrh.this.mHandler.sendMessageDelayed(Message.obtain(zzrh.this.mHandler, 9, this.xx), zzrh.this.Af);
            zzrh.this.AK = -1;
        }

        @WorkerThread
        public void signOut() {
            zzac(zzrh.AG);
            this.AS.zzasj();
            for (zzrr.zzb<?> zzb : this.AU.keySet()) {
                zza(new zzqj.zze(zzb, new TaskCompletionSource()));
            }
            this.xB.disconnect();
        }

        @Override // com.google.android.gms.internal.zzqs
        public void zza(ConnectionResult connectionResult, Api<?> api, int i) {
            onConnectionFailed(connectionResult);
        }

        @WorkerThread
        public void zza(zzqj zzqj) {
            if (this.xB.isConnected()) {
                zzb(zzqj);
                zzatr();
                return;
            }
            this.AQ.add(zzqj);
            if (this.AW == null || !this.AW.hasResolution()) {
                connect();
            } else {
                onConnectionFailed(this.AW);
            }
        }

        public boolean zzain() {
            return this.xB.zzain();
        }

        @WorkerThread
        public void zzatm() {
            while (this.xB.isConnected() && !this.AQ.isEmpty()) {
                zzb(this.AQ.remove());
            }
        }

        public Map<zzrr.zzb<?>, zzrx> zzatn() {
            return this.AU;
        }

        @WorkerThread
        public void zzato() {
            this.AW = null;
        }

        /* access modifiers changed from: package-private */
        public ConnectionResult zzatp() {
            return this.AW;
        }

        @WorkerThread
        public void zzb(zzqn zzqn) {
            this.AT.add(zzqn);
        }
    }

    /* access modifiers changed from: private */
    public class zzb implements zze.zzf {
        private final Api.zze xB;
        private final zzql<?> xx;

        public zzb(Api.zze zze, zzql<?> zzql) {
            this.xB = zze;
            this.xx = zzql;
        }

        @Override // com.google.android.gms.common.internal.zze.zzf
        @WorkerThread
        public void zzg(@NonNull ConnectionResult connectionResult) {
            if (!connectionResult.isSuccess()) {
                ((zza) zzrh.this.zj.get(this.xx)).onConnectionFailed(connectionResult);
            } else if (!this.xB.zzain()) {
                this.xB.zza(null, Collections.emptySet());
            }
        }
    }

    private zzrh(Context context) {
        this(context, GoogleApiAvailability.getInstance());
    }

    private zzrh(Context context, GoogleApiAvailability googleApiAvailability) {
        this.Ag = HlsChunkSource.DEFAULT_MIN_BUFFER_TO_SWITCH_UP_MS;
        this.Af = 120000;
        this.AI = 10000;
        this.AK = -1;
        this.AL = new AtomicInteger(1);
        this.AM = new AtomicInteger(0);
        this.zj = new ConcurrentHashMap(5, 0.75f, 1);
        this.AN = null;
        this.AO = new com.google.android.gms.common.util.zza();
        this.AP = new com.google.android.gms.common.util.zza();
        this.mContext = context;
        HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
        handlerThread.start();
        this.mHandler = new Handler(handlerThread.getLooper(), this);
        this.xP = googleApiAvailability;
    }

    @WorkerThread
    private void zza(int i, ConnectionResult connectionResult) {
        zza<?> zza2;
        Iterator<zza<?>> it = this.zj.values().iterator();
        while (true) {
            if (!it.hasNext()) {
                zza2 = null;
                break;
            }
            zza2 = it.next();
            if (zza2.getInstanceId() == i) {
                break;
            }
        }
        if (zza2 != null) {
            String valueOf = String.valueOf(this.xP.getErrorString(connectionResult.getErrorCode()));
            String valueOf2 = String.valueOf(connectionResult.getErrorMessage());
            zza2.zzac(new Status(17, new StringBuilder(String.valueOf(valueOf).length() + 69 + String.valueOf(valueOf2).length()).append("Error resolution was canceled by the user, original error message: ").append(valueOf).append(": ").append(valueOf2).toString()));
            return;
        }
        Log.wtf("GoogleApiManager", new StringBuilder(76).append("Could not find API instance ").append(i).append(" while trying to fail enqueued calls.").toString(), new Exception());
    }

    @WorkerThread
    private void zza(zzrv zzrv) {
        zza<?> zza2 = this.zj.get(zzrv.Bs.getApiKey());
        if (zza2 == null) {
            zzb(zzrv.Bs);
            zza2 = this.zj.get(zzrv.Bs.getApiKey());
        }
        if (!zza2.zzain() || this.AM.get() == zzrv.Br) {
            zza2.zza(zzrv.Bq);
            return;
        }
        zzrv.Bq.zzy(AG);
        zza2.signOut();
    }

    public static zzrh zzatg() {
        zzrh zzrh;
        synchronized (zzaox) {
            zzaa.zzb(AJ, "Must guarantee manager is non-null before using getInstance");
            zzrh = AJ;
        }
        return zzrh;
    }

    @WorkerThread
    private void zzati() {
        for (zza<?> zza2 : this.zj.values()) {
            zza2.zzato();
            zza2.connect();
        }
    }

    @WorkerThread
    private void zzb(zzc<?> zzc) {
        zzql<?> apiKey = zzc.getApiKey();
        if (!this.zj.containsKey(apiKey)) {
            this.zj.put(apiKey, new zza<>(zzc));
        }
        zza<?> zza2 = this.zj.get(apiKey);
        if (zza2.zzain()) {
            this.AP.add(apiKey);
        }
        zza2.connect();
    }

    public static zzrh zzbx(Context context) {
        zzrh zzrh;
        synchronized (zzaox) {
            if (AJ == null) {
                AJ = new zzrh(context.getApplicationContext());
            }
            zzrh = AJ;
        }
        return zzrh;
    }

    @WorkerThread
    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                zza((zzqn) message.obj);
                break;
            case 2:
                zzati();
                break;
            case 3:
            case 6:
            case 11:
                zza((zzrv) message.obj);
                break;
            case 4:
                zza(message.arg1, (ConnectionResult) message.obj);
                break;
            case 5:
                zzb((zzc) message.obj);
                break;
            case 7:
                if (this.zj.containsKey(message.obj)) {
                    this.zj.get(message.obj).resume();
                    break;
                }
                break;
            case 8:
                zzatj();
                break;
            case 9:
                if (this.zj.containsKey(message.obj)) {
                    this.zj.get(message.obj).zzasx();
                    break;
                }
                break;
            case 10:
                if (this.zj.containsKey(message.obj)) {
                    this.zj.get(message.obj).zzats();
                    break;
                }
                break;
            default:
                Log.w("GoogleApiManager", new StringBuilder(31).append("Unknown message id: ").append(message.what).toString());
                return false;
        }
        return true;
    }

    public <O extends Api.ApiOptions> Task<Void> zza(@NonNull zzc<O> zzc, @NonNull zzrr.zzb<?> zzb2) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.mHandler.sendMessage(this.mHandler.obtainMessage(11, new zzrv(new zzqj.zze(zzb2, taskCompletionSource), this.AM.get(), zzc)));
        return taskCompletionSource.getTask();
    }

    public <O extends Api.ApiOptions> Task<Void> zza(@NonNull zzc<O> zzc, @NonNull zzrw<Api.zzb> zzrw, @NonNull zzsh<Api.zzb> zzsh) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.mHandler.sendMessage(this.mHandler.obtainMessage(6, new zzrv(new zzqj.zzc(new zzrx(zzrw, zzsh), taskCompletionSource), this.AM.get(), zzc)));
        return taskCompletionSource.getTask();
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.gms.tasks.Task<java.lang.Void> zza(java.lang.Iterable<com.google.android.gms.common.api.zzc<?>> r5) {
        /*
            r4 = this;
            com.google.android.gms.internal.zzqn r1 = new com.google.android.gms.internal.zzqn
            r1.<init>(r5)
            java.util.Iterator r2 = r5.iterator()
        L_0x0009:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x003a
            java.lang.Object r0 = r2.next()
            com.google.android.gms.common.api.zzc r0 = (com.google.android.gms.common.api.zzc) r0
            java.util.Map<com.google.android.gms.internal.zzql<?>, com.google.android.gms.internal.zzrh$zza<?>> r3 = r4.zj
            com.google.android.gms.internal.zzql r0 = r0.getApiKey()
            java.lang.Object r0 = r3.get(r0)
            com.google.android.gms.internal.zzrh$zza r0 = (com.google.android.gms.internal.zzrh.zza) r0
            if (r0 == 0) goto L_0x0029
            boolean r0 = r0.isConnected()
            if (r0 != 0) goto L_0x0009
        L_0x0029:
            android.os.Handler r0 = r4.mHandler
            android.os.Handler r2 = r4.mHandler
            r3 = 1
            android.os.Message r2 = r2.obtainMessage(r3, r1)
            r0.sendMessage(r2)
            com.google.android.gms.tasks.Task r0 = r1.getTask()
        L_0x0039:
            return r0
        L_0x003a:
            r1.zzarp()
            com.google.android.gms.tasks.Task r0 = r1.getTask()
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzrh.zza(java.lang.Iterable):com.google.android.gms.tasks.Task");
    }

    public void zza(ConnectionResult connectionResult, int i) {
        if (!zzc(connectionResult, i)) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(4, i, 0, connectionResult));
        }
    }

    public void zza(zzc<?> zzc) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(5, zzc));
    }

    public <O extends Api.ApiOptions> void zza(zzc<O> zzc, int i, zzqo.zza<? extends Result, Api.zzb> zza2) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(3, new zzrv(new zzqj.zzb(i, zza2), this.AM.get(), zzc)));
    }

    public <O extends Api.ApiOptions, TResult> void zza(zzc<O> zzc, int i, zzse<Api.zzb, TResult> zzse, TaskCompletionSource<TResult> taskCompletionSource, zzsb zzsb) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(3, new zzrv(new zzqj.zzd(i, zzse, taskCompletionSource, zzsb), this.AM.get(), zzc)));
    }

    @WorkerThread
    public void zza(zzqn zzqn) {
        for (zzql<?> zzql : zzqn.zzaro()) {
            zza<?> zza2 = this.zj.get(zzql);
            if (zza2 == null) {
                zzqn.zza(zzql, new ConnectionResult(13));
                return;
            } else if (zza2.isConnected()) {
                zzqn.zza(zzql, ConnectionResult.wO);
            } else if (zza2.zzatp() != null) {
                zzqn.zza(zzql, zza2.zzatp());
            } else {
                zza2.zzb(zzqn);
            }
        }
    }

    public void zza(@NonNull zzqw zzqw) {
        synchronized (zzaox) {
            if (this.AN != zzqw) {
                this.AN = zzqw;
                this.AO.clear();
                this.AO.addAll(zzqw.zzasl());
            }
        }
    }

    public void zzarm() {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2));
    }

    public int zzath() {
        return this.AL.getAndIncrement();
    }

    @WorkerThread
    public void zzatj() {
        for (zzql<?> zzql : this.AP) {
            this.zj.remove(zzql).signOut();
        }
        this.AP.clear();
    }

    /* access modifiers changed from: package-private */
    public void zzb(@NonNull zzqw zzqw) {
        synchronized (zzaox) {
            if (this.AN == zzqw) {
                this.AN = null;
                this.AO.clear();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean zzc(ConnectionResult connectionResult, int i) {
        if (!connectionResult.hasResolution() && !this.xP.isUserResolvableError(connectionResult.getErrorCode())) {
            return false;
        }
        this.xP.zza(this.mContext, connectionResult, i);
        return true;
    }
}
