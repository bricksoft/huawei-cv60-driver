package com.google.android.gms.internal;

import com.google.android.gms.auth.api.proxy.ProxyResponse;
import com.google.android.gms.internal.zznz;

public class zznx extends zznz.zza {
    @Override // com.google.android.gms.internal.zznz
    public void zza(ProxyResponse proxyResponse) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.zznz
    public void zzfy(String str) {
        throw new UnsupportedOperationException();
    }
}
