package com.google.android.gms.internal;

import java.lang.reflect.Type;
import java.util.Collection;

public final class zzaqc implements zzapl {
    private final zzaps bod;

    private static final class zza<E> extends zzapk<Collection<E>> {
        private final zzapk<E> bpJ;
        private final zzapx<? extends Collection<E>> bpK;

        public zza(zzaos zzaos, Type type, zzapk<E> zzapk, zzapx<? extends Collection<E>> zzapx) {
            this.bpJ = new zzaqm(zzaos, zzapk, type);
            this.bpK = zzapx;
        }

        @Override // com.google.android.gms.internal.zzapk
        public /* bridge */ /* synthetic */ void zza(zzaqr zzaqr, Object obj) {
            zza(zzaqr, (Collection) ((Collection) obj));
        }

        public void zza(zzaqr zzaqr, Collection<E> collection) {
            if (collection == null) {
                zzaqr.bA();
                return;
            }
            zzaqr.bw();
            for (E e : collection) {
                this.bpJ.zza(zzaqr, e);
            }
            zzaqr.bx();
        }

        /* renamed from: zzj */
        public Collection<E> zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            Collection<E> collection = (Collection) this.bpK.bj();
            zzaqp.beginArray();
            while (zzaqp.hasNext()) {
                collection.add(this.bpJ.zzb(zzaqp));
            }
            zzaqp.endArray();
            return collection;
        }
    }

    public zzaqc(zzaps zzaps) {
        this.bod = zzaps;
    }

    @Override // com.google.android.gms.internal.zzapl
    public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
        Type bC = zzaqo.bC();
        Class<? super T> bB = zzaqo.bB();
        if (!Collection.class.isAssignableFrom(bB)) {
            return null;
        }
        Type zza2 = zzapr.zza(bC, (Class<?>) bB);
        return new zza(zzaos, zza2, zzaos.zza(zzaqo.zzl(zza2)), this.bod.zzb(zzaqo));
    }
}
