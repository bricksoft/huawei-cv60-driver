package com.google.android.gms.internal;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class zzaqd extends zzapk<Date> {
    public static final zzapl bpG = new zzapl() {
        /* class com.google.android.gms.internal.zzaqd.AnonymousClass1 */

        @Override // com.google.android.gms.internal.zzapl
        public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
            if (zzaqo.bB() == Date.class) {
                return new zzaqd();
            }
            return null;
        }
    };
    private final DateFormat bnQ = DateFormat.getDateTimeInstance(2, 2, Locale.US);
    private final DateFormat bnR = DateFormat.getDateTimeInstance(2, 2);
    private final DateFormat bnS = bp();

    private static DateFormat bp() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }

    private synchronized Date zzur(String str) {
        Date parse;
        try {
            parse = this.bnR.parse(str);
        } catch (ParseException e) {
            try {
                parse = this.bnQ.parse(str);
            } catch (ParseException e2) {
                try {
                    parse = this.bnS.parse(str);
                } catch (ParseException e3) {
                    throw new zzaph(str, e3);
                }
            }
        }
        return parse;
    }

    public synchronized void zza(zzaqr zzaqr, Date date) {
        if (date == null) {
            zzaqr.bA();
        } else {
            zzaqr.zzut(this.bnQ.format(date));
        }
    }

    /* renamed from: zzk */
    public Date zzb(zzaqp zzaqp) {
        if (zzaqp.bq() != zzaqq.NULL) {
            return zzur(zzaqp.nextString());
        }
        zzaqp.nextNull();
        return null;
    }
}
