package com.google.android.gms.internal;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

public final class zzaqh implements zzapl {
    private final zzaps bod;
    private final boolean bpS;

    private final class zza<K, V> extends zzapk<Map<K, V>> {
        private final zzapx<? extends Map<K, V>> bpK;
        private final zzapk<K> bpT;
        private final zzapk<V> bpU;

        public zza(zzaos zzaos, Type type, zzapk<K> zzapk, Type type2, zzapk<V> zzapk2, zzapx<? extends Map<K, V>> zzapx) {
            this.bpT = new zzaqm(zzaos, zzapk, type);
            this.bpU = new zzaqm(zzaos, zzapk2, type2);
            this.bpK = zzapx;
        }

        private String zze(zzaoy zzaoy) {
            if (zzaoy.aX()) {
                zzape bb = zzaoy.bb();
                if (bb.be()) {
                    return String.valueOf(bb.aT());
                }
                if (bb.bd()) {
                    return Boolean.toString(bb.getAsBoolean());
                }
                if (bb.bf()) {
                    return bb.aU();
                }
                throw new AssertionError();
            } else if (zzaoy.aY()) {
                return "null";
            } else {
                throw new AssertionError();
            }
        }

        @Override // com.google.android.gms.internal.zzapk
        public /* bridge */ /* synthetic */ void zza(zzaqr zzaqr, Object obj) {
            zza(zzaqr, (Map) ((Map) obj));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v10, resolved type: com.google.android.gms.internal.zzapk<V> */
        /* JADX DEBUG: Multi-variable search result rejected for r0v14, resolved type: com.google.android.gms.internal.zzapk<V> */
        /* JADX WARN: Multi-variable type inference failed */
        public void zza(zzaqr zzaqr, Map<K, V> map) {
            int i = 0;
            if (map == null) {
                zzaqr.bA();
            } else if (!zzaqh.this.bpS) {
                zzaqr.by();
                for (Map.Entry<K, V> entry : map.entrySet()) {
                    zzaqr.zzus(String.valueOf(entry.getKey()));
                    this.bpU.zza(zzaqr, entry.getValue());
                }
                zzaqr.bz();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                boolean z = false;
                for (Map.Entry<K, V> entry2 : map.entrySet()) {
                    zzaoy zzcn = this.bpT.zzcn(entry2.getKey());
                    arrayList.add(zzcn);
                    arrayList2.add(entry2.getValue());
                    z = (zzcn.aV() || zzcn.aW()) | z;
                }
                if (z) {
                    zzaqr.bw();
                    while (i < arrayList.size()) {
                        zzaqr.bw();
                        zzapz.zzb((zzaoy) arrayList.get(i), zzaqr);
                        this.bpU.zza(zzaqr, arrayList2.get(i));
                        zzaqr.bx();
                        i++;
                    }
                    zzaqr.bx();
                    return;
                }
                zzaqr.by();
                while (i < arrayList.size()) {
                    zzaqr.zzus(zze((zzaoy) arrayList.get(i)));
                    this.bpU.zza(zzaqr, arrayList2.get(i));
                    i++;
                }
                zzaqr.bz();
            }
        }

        /* renamed from: zzl */
        public Map<K, V> zzb(zzaqp zzaqp) {
            zzaqq bq = zzaqp.bq();
            if (bq == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            Map<K, V> map = (Map) this.bpK.bj();
            if (bq == zzaqq.BEGIN_ARRAY) {
                zzaqp.beginArray();
                while (zzaqp.hasNext()) {
                    zzaqp.beginArray();
                    K zzb = this.bpT.zzb(zzaqp);
                    if (map.put(zzb, this.bpU.zzb(zzaqp)) != null) {
                        String valueOf = String.valueOf(zzb);
                        throw new zzaph(new StringBuilder(String.valueOf(valueOf).length() + 15).append("duplicate key: ").append(valueOf).toString());
                    }
                    zzaqp.endArray();
                }
                zzaqp.endArray();
                return map;
            }
            zzaqp.beginObject();
            while (zzaqp.hasNext()) {
                zzapu.bph.zzi(zzaqp);
                K zzb2 = this.bpT.zzb(zzaqp);
                if (map.put(zzb2, this.bpU.zzb(zzaqp)) != null) {
                    String valueOf2 = String.valueOf(zzb2);
                    throw new zzaph(new StringBuilder(String.valueOf(valueOf2).length() + 15).append("duplicate key: ").append(valueOf2).toString());
                }
            }
            zzaqp.endObject();
            return map;
        }
    }

    public zzaqh(zzaps zzaps, boolean z) {
        this.bod = zzaps;
        this.bpS = z;
    }

    private zzapk<?> zza(zzaos zzaos, Type type) {
        return (type == Boolean.TYPE || type == Boolean.class) ? zzaqn.bqo : zzaos.zza(zzaqo.zzl(type));
    }

    @Override // com.google.android.gms.internal.zzapl
    public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
        Type bC = zzaqo.bC();
        if (!Map.class.isAssignableFrom(zzaqo.bB())) {
            return null;
        }
        Type[] zzb = zzapr.zzb(bC, zzapr.zzf(bC));
        return new zza(zzaos, zzb[0], zza(zzaos, zzb[0]), zzb[1], zzaos.zza(zzaqo.zzl(zzb[1])), this.bod.zzb(zzaqo));
    }
}
