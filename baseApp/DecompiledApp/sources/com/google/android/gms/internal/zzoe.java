package com.google.android.gms.internal;

public enum zzoe {
    CLIENT_LOGIN_DISABLED("ClientLoginDisabled"),
    DEVICE_MANAGEMENT_REQUIRED("DeviceManagementRequiredOrSyncDisabled"),
    SOCKET_TIMEOUT("SocketTimeout"),
    SUCCESS("Ok"),
    UNKNOWN_ERROR("UNKNOWN_ERR"),
    NETWORK_ERROR("NetworkError"),
    SERVICE_UNAVAILABLE("ServiceUnavailable"),
    INTNERNAL_ERROR("InternalError"),
    BAD_AUTHENTICATION("BadAuthentication"),
    EMPTY_CONSUMER_PKG_OR_SIG("EmptyConsumerPackageOrSig"),
    NEEDS_2F("InvalidSecondFactor"),
    NEEDS_POST_SIGN_IN_FLOW("PostSignInFlowRequired"),
    NEEDS_BROWSER("NeedsBrowser"),
    UNKNOWN("Unknown"),
    NOT_VERIFIED("NotVerified"),
    TERMS_NOT_AGREED("TermsNotAgreed"),
    ACCOUNT_DISABLED("AccountDisabled"),
    CAPTCHA("CaptchaRequired"),
    ACCOUNT_DELETED("AccountDeleted"),
    SERVICE_DISABLED("ServiceDisabled"),
    NEED_PERMISSION("NeedPermission"),
    NEED_REMOTE_CONSENT("NeedRemoteConsent"),
    INVALID_SCOPE("INVALID_SCOPE"),
    USER_CANCEL("UserCancel"),
    PERMISSION_DENIED("PermissionDenied"),
    INVALID_AUDIENCE("INVALID_AUDIENCE"),
    UNREGISTERED_ON_API_CONSOLE("UNREGISTERED_ON_API_CONSOLE"),
    THIRD_PARTY_DEVICE_MANAGEMENT_REQUIRED("ThirdPartyDeviceManagementRequired"),
    DM_INTERNAL_ERROR("DeviceManagementInternalError"),
    DM_SYNC_DISABLED("DeviceManagementSyncDisabled"),
    DM_ADMIN_BLOCKED("DeviceManagementAdminBlocked"),
    DM_ADMIN_PENDING_APPROVAL("DeviceManagementAdminPendingApproval"),
    DM_STALE_SYNC_REQUIRED("DeviceManagementStaleSyncRequired"),
    DM_DEACTIVATED("DeviceManagementDeactivated"),
    DM_REQUIRED("DeviceManagementRequired"),
    ALREADY_HAS_GMAIL("ALREADY_HAS_GMAIL"),
    BAD_PASSWORD("WeakPassword"),
    BAD_REQUEST("BadRequest"),
    BAD_USERNAME("BadUsername"),
    DELETED_GMAIL("DeletedGmail"),
    EXISTING_USERNAME("ExistingUsername"),
    LOGIN_FAIL("LoginFail"),
    NOT_LOGGED_IN("NotLoggedIn"),
    NO_GMAIL("NoGmail"),
    REQUEST_DENIED("RequestDenied"),
    SERVER_ERROR("ServerError"),
    USERNAME_UNAVAILABLE("UsernameUnavailable"),
    GPLUS_OTHER("GPlusOther"),
    GPLUS_NICKNAME("GPlusNickname"),
    GPLUS_INVALID_CHAR("GPlusInvalidChar"),
    GPLUS_INTERSTITIAL("GPlusInterstitial"),
    GPLUS_PROFILE_ERROR("ProfileUpgradeError");
    
    private final String kW;

    private zzoe(String str) {
        this.kW = str;
    }

    public static boolean zza(zzoe zzoe) {
        return BAD_AUTHENTICATION.equals(zzoe) || CAPTCHA.equals(zzoe) || NEED_PERMISSION.equals(zzoe) || NEED_REMOTE_CONSENT.equals(zzoe) || NEEDS_BROWSER.equals(zzoe) || USER_CANCEL.equals(zzoe) || DEVICE_MANAGEMENT_REQUIRED.equals(zzoe) || DM_INTERNAL_ERROR.equals(zzoe) || DM_SYNC_DISABLED.equals(zzoe) || DM_ADMIN_BLOCKED.equals(zzoe) || DM_ADMIN_PENDING_APPROVAL.equals(zzoe) || DM_STALE_SYNC_REQUIRED.equals(zzoe) || DM_DEACTIVATED.equals(zzoe) || DM_REQUIRED.equals(zzoe) || THIRD_PARTY_DEVICE_MANAGEMENT_REQUIRED.equals(zzoe);
    }

    public static boolean zzb(zzoe zzoe) {
        return NETWORK_ERROR.equals(zzoe) || SERVICE_UNAVAILABLE.equals(zzoe);
    }

    public static final zzoe zzgi(String str) {
        zzoe zzoe = null;
        zzoe[] values = values();
        int length = values.length;
        int i = 0;
        while (i < length) {
            zzoe zzoe2 = values[i];
            if (!zzoe2.kW.equals(str)) {
                zzoe2 = zzoe;
            }
            i++;
            zzoe = zzoe2;
        }
        return zzoe;
    }
}
