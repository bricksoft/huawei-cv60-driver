package com.google.android.gms.internal;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public final class zzapw<K, V> extends AbstractMap<K, V> implements Serializable {
    static final /* synthetic */ boolean $assertionsDisabled = (!zzapw.class.desiredAssertionStatus());
    private static final Comparator<Comparable> bpi = new Comparator<Comparable>() {
        /* class com.google.android.gms.internal.zzapw.AnonymousClass1 */

        /* renamed from: zza */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    };
    Comparator<? super K> bab;
    zzd<K, V> bpj;
    final zzd<K, V> bpk;
    private zza bpl;
    private zzb bpm;
    int modCount;
    int size;

    /* access modifiers changed from: package-private */
    public class zza extends AbstractSet<Map.Entry<K, V>> {
        zza() {
        }

        public void clear() {
            zzapw.this.clear();
        }

        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && zzapw.this.zzc((Map.Entry) obj) != null;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return new zzc<Map.Entry<K, V>>() {
                /* class com.google.android.gms.internal.zzapw.zza.AnonymousClass1 */

                {
                    zzapw zzapw = zzapw.this;
                }

                @Override // java.util.Iterator
                public Map.Entry<K, V> next() {
                    return bl();
                }
            };
        }

        public boolean remove(Object obj) {
            zzd<K, V> zzc;
            if (!(obj instanceof Map.Entry) || (zzc = zzapw.this.zzc((Map.Entry) obj)) == null) {
                return false;
            }
            zzapw.this.zza((zzd) zzc, true);
            return true;
        }

        public int size() {
            return zzapw.this.size;
        }
    }

    final class zzb extends AbstractSet<K> {
        zzb() {
        }

        public void clear() {
            zzapw.this.clear();
        }

        public boolean contains(Object obj) {
            return zzapw.this.containsKey(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new zzc<K>() {
                /* class com.google.android.gms.internal.zzapw.zzb.AnonymousClass1 */

                {
                    zzapw zzapw = zzapw.this;
                }

                @Override // java.util.Iterator
                public K next() {
                    return bl().bap;
                }
            };
        }

        public boolean remove(Object obj) {
            return zzapw.this.zzcr(obj) != null;
        }

        public int size() {
            return zzapw.this.size;
        }
    }

    private abstract class zzc<T> implements Iterator<T> {
        zzd<K, V> bpq;
        zzd<K, V> bpr;
        int bps;

        private zzc() {
            this.bpq = zzapw.this.bpk.bpq;
            this.bpr = null;
            this.bps = zzapw.this.modCount;
        }

        /* access modifiers changed from: package-private */
        public final zzd<K, V> bl() {
            zzd<K, V> zzd = this.bpq;
            if (zzd == zzapw.this.bpk) {
                throw new NoSuchElementException();
            } else if (zzapw.this.modCount != this.bps) {
                throw new ConcurrentModificationException();
            } else {
                this.bpq = zzd.bpq;
                this.bpr = zzd;
                return zzd;
            }
        }

        public final boolean hasNext() {
            return this.bpq != zzapw.this.bpk;
        }

        public final void remove() {
            if (this.bpr == null) {
                throw new IllegalStateException();
            }
            zzapw.this.zza((zzd) this.bpr, true);
            this.bpr = null;
            this.bps = zzapw.this.modCount;
        }
    }

    /* access modifiers changed from: package-private */
    public static final class zzd<K, V> implements Map.Entry<K, V> {
        final K bap;
        zzd<K, V> bpq;
        zzd<K, V> bpt;
        zzd<K, V> bpu;
        zzd<K, V> bpv;
        zzd<K, V> bpw;
        int height;
        V value;

        zzd() {
            this.bap = null;
            this.bpw = this;
            this.bpq = this;
        }

        zzd(zzd<K, V> zzd, K k, zzd<K, V> zzd2, zzd<K, V> zzd3) {
            this.bpt = zzd;
            this.bap = k;
            this.height = 1;
            this.bpq = zzd2;
            this.bpw = zzd3;
            zzd3.bpq = this;
            zzd2.bpw = this;
        }

        public zzd<K, V> bm() {
            for (zzd<K, V> zzd = this.bpu; zzd != null; zzd = zzd.bpu) {
                this = zzd;
            }
            return this;
        }

        public zzd<K, V> bn() {
            for (zzd<K, V> zzd = this.bpv; zzd != null; zzd = zzd.bpv) {
                this = zzd;
            }
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:10:0x001b A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                r0 = 0
                boolean r1 = r4 instanceof java.util.Map.Entry
                if (r1 == 0) goto L_0x001c
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                K r1 = r3.bap
                if (r1 != 0) goto L_0x001d
                java.lang.Object r1 = r4.getKey()
                if (r1 != 0) goto L_0x001c
            L_0x0011:
                V r1 = r3.value
                if (r1 != 0) goto L_0x002a
                java.lang.Object r1 = r4.getValue()
                if (r1 != 0) goto L_0x001c
            L_0x001b:
                r0 = 1
            L_0x001c:
                return r0
            L_0x001d:
                K r1 = r3.bap
                java.lang.Object r2 = r4.getKey()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x0011
            L_0x002a:
                V r1 = r3.value
                java.lang.Object r2 = r4.getValue()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x001b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzapw.zzd.equals(java.lang.Object):boolean");
        }

        @Override // java.util.Map.Entry
        public K getKey() {
            return this.bap;
        }

        @Override // java.util.Map.Entry
        public V getValue() {
            return this.value;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = this.bap == null ? 0 : this.bap.hashCode();
            if (this.value != null) {
                i = this.value.hashCode();
            }
            return hashCode ^ i;
        }

        @Override // java.util.Map.Entry
        public V setValue(V v) {
            V v2 = this.value;
            this.value = v;
            return v2;
        }

        public String toString() {
            String valueOf = String.valueOf(this.bap);
            String valueOf2 = String.valueOf(this.value);
            return new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length()).append(valueOf).append("=").append(valueOf2).toString();
        }
    }

    public zzapw() {
        this(bpi);
    }

    public zzapw(Comparator<? super K> comparator) {
        this.size = 0;
        this.modCount = 0;
        this.bpk = new zzd<>();
        this.bab = comparator == null ? bpi : comparator;
    }

    private boolean equal(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    private void zza(zzd<K, V> zzd2) {
        int i = 0;
        zzd<K, V> zzd3 = zzd2.bpu;
        zzd<K, V> zzd4 = zzd2.bpv;
        zzd<K, V> zzd5 = zzd4.bpu;
        zzd<K, V> zzd6 = zzd4.bpv;
        zzd2.bpv = zzd5;
        if (zzd5 != null) {
            zzd5.bpt = zzd2;
        }
        zza(zzd2, zzd4);
        zzd4.bpu = zzd2;
        zzd2.bpt = zzd4;
        zzd2.height = Math.max(zzd3 != null ? zzd3.height : 0, zzd5 != null ? zzd5.height : 0) + 1;
        int i2 = zzd2.height;
        if (zzd6 != null) {
            i = zzd6.height;
        }
        zzd4.height = Math.max(i2, i) + 1;
    }

    private void zza(zzd<K, V> zzd2, zzd<K, V> zzd3) {
        zzd<K, V> zzd4 = zzd2.bpt;
        zzd2.bpt = null;
        if (zzd3 != null) {
            zzd3.bpt = zzd4;
        }
        if (zzd4 == null) {
            this.bpj = zzd3;
        } else if (zzd4.bpu == zzd2) {
            zzd4.bpu = zzd3;
        } else if ($assertionsDisabled || zzd4.bpv == zzd2) {
            zzd4.bpv = zzd3;
        } else {
            throw new AssertionError();
        }
    }

    private void zzb(zzd<K, V> zzd2) {
        int i = 0;
        zzd<K, V> zzd3 = zzd2.bpu;
        zzd<K, V> zzd4 = zzd2.bpv;
        zzd<K, V> zzd5 = zzd3.bpu;
        zzd<K, V> zzd6 = zzd3.bpv;
        zzd2.bpu = zzd6;
        if (zzd6 != null) {
            zzd6.bpt = zzd2;
        }
        zza(zzd2, zzd3);
        zzd3.bpv = zzd2;
        zzd2.bpt = zzd3;
        zzd2.height = Math.max(zzd4 != null ? zzd4.height : 0, zzd6 != null ? zzd6.height : 0) + 1;
        int i2 = zzd2.height;
        if (zzd5 != null) {
            i = zzd5.height;
        }
        zzd3.height = Math.max(i2, i) + 1;
    }

    private void zzb(zzd<K, V> zzd2, boolean z) {
        while (zzd2 != null) {
            zzd<K, V> zzd3 = zzd2.bpu;
            zzd<K, V> zzd4 = zzd2.bpv;
            int i = zzd3 != null ? zzd3.height : 0;
            int i2 = zzd4 != null ? zzd4.height : 0;
            int i3 = i - i2;
            if (i3 == -2) {
                zzd<K, V> zzd5 = zzd4.bpu;
                zzd<K, V> zzd6 = zzd4.bpv;
                int i4 = (zzd5 != null ? zzd5.height : 0) - (zzd6 != null ? zzd6.height : 0);
                if (i4 == -1 || (i4 == 0 && !z)) {
                    zza(zzd2);
                } else if ($assertionsDisabled || i4 == 1) {
                    zzb(zzd4);
                    zza(zzd2);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i3 == 2) {
                zzd<K, V> zzd7 = zzd3.bpu;
                zzd<K, V> zzd8 = zzd3.bpv;
                int i5 = (zzd7 != null ? zzd7.height : 0) - (zzd8 != null ? zzd8.height : 0);
                if (i5 == 1 || (i5 == 0 && !z)) {
                    zzb(zzd2);
                } else if ($assertionsDisabled || i5 == -1) {
                    zza(zzd3);
                    zzb(zzd2);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i3 == 0) {
                zzd2.height = i + 1;
                if (z) {
                    return;
                }
            } else if ($assertionsDisabled || i3 == -1 || i3 == 1) {
                zzd2.height = Math.max(i, i2) + 1;
                if (!z) {
                    return;
                }
            } else {
                throw new AssertionError();
            }
            zzd2 = zzd2.bpt;
        }
    }

    public void clear() {
        this.bpj = null;
        this.size = 0;
        this.modCount++;
        zzd<K, V> zzd2 = this.bpk;
        zzd2.bpw = zzd2;
        zzd2.bpq = zzd2;
    }

    public boolean containsKey(Object obj) {
        return zzcq(obj) != null;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        zza zza2 = this.bpl;
        if (zza2 != null) {
            return zza2;
        }
        zza zza3 = new zza();
        this.bpl = zza3;
        return zza3;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        zzd<K, V> zzcq = zzcq(obj);
        if (zzcq != null) {
            return zzcq.value;
        }
        return null;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<K> keySet() {
        zzb zzb2 = this.bpm;
        if (zzb2 != null) {
            return zzb2;
        }
        zzb zzb3 = new zzb();
        this.bpm = zzb3;
        return zzb3;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V put(K k, V v) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        zzd<K, V> zza2 = zza((Object) k, true);
        V v2 = zza2.value;
        zza2.value = v;
        return v2;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        zzd<K, V> zzcr = zzcr(obj);
        if (zzcr != null) {
            return zzcr.value;
        }
        return null;
    }

    public int size() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    public zzd<K, V> zza(K k, boolean z) {
        int i;
        zzd<K, V> zzd2;
        Comparator<? super K> comparator = this.bab;
        zzd<K, V> zzd3 = this.bpj;
        if (zzd3 != null) {
            K k2 = comparator == bpi ? k : null;
            while (true) {
                i = k2 != null ? k2.compareTo(zzd3.bap) : comparator.compare(k, zzd3.bap);
                if (i == 0) {
                    return zzd3;
                }
                zzd<K, V> zzd4 = i < 0 ? zzd3.bpu : zzd3.bpv;
                if (zzd4 == null) {
                    break;
                }
                zzd3 = zzd4;
            }
        } else {
            i = 0;
        }
        if (!z) {
            return null;
        }
        zzd<K, V> zzd5 = this.bpk;
        if (zzd3 != null) {
            zzd2 = new zzd<>(zzd3, k, zzd5, zzd5.bpw);
            if (i < 0) {
                zzd3.bpu = zzd2;
            } else {
                zzd3.bpv = zzd2;
            }
            zzb(zzd3, true);
        } else if (comparator != bpi || (k instanceof Comparable)) {
            zzd2 = new zzd<>(zzd3, k, zzd5, zzd5.bpw);
            this.bpj = zzd2;
        } else {
            throw new ClassCastException(String.valueOf(k.getClass().getName()).concat(" is not Comparable"));
        }
        this.size++;
        this.modCount++;
        return zzd2;
    }

    /* access modifiers changed from: package-private */
    public void zza(zzd<K, V> zzd2, boolean z) {
        int i;
        int i2 = 0;
        if (z) {
            zzd2.bpw.bpq = zzd2.bpq;
            zzd2.bpq.bpw = zzd2.bpw;
        }
        zzd<K, V> zzd3 = zzd2.bpu;
        zzd<K, V> zzd4 = zzd2.bpv;
        zzd<K, V> zzd5 = zzd2.bpt;
        if (zzd3 == null || zzd4 == null) {
            if (zzd3 != null) {
                zza(zzd2, zzd3);
                zzd2.bpu = null;
            } else if (zzd4 != null) {
                zza(zzd2, zzd4);
                zzd2.bpv = null;
            } else {
                zza(zzd2, (zzd) null);
            }
            zzb(zzd5, false);
            this.size--;
            this.modCount++;
            return;
        }
        zzd<K, V> bn = zzd3.height > zzd4.height ? zzd3.bn() : zzd4.bm();
        zza((zzd) bn, false);
        zzd<K, V> zzd6 = zzd2.bpu;
        if (zzd6 != null) {
            i = zzd6.height;
            bn.bpu = zzd6;
            zzd6.bpt = bn;
            zzd2.bpu = null;
        } else {
            i = 0;
        }
        zzd<K, V> zzd7 = zzd2.bpv;
        if (zzd7 != null) {
            i2 = zzd7.height;
            bn.bpv = zzd7;
            zzd7.bpt = bn;
            zzd2.bpv = null;
        }
        bn.height = Math.max(i, i2) + 1;
        zza(zzd2, bn);
    }

    /* access modifiers changed from: package-private */
    public zzd<K, V> zzc(Map.Entry<?, ?> entry) {
        zzd<K, V> zzcq = zzcq(entry.getKey());
        if (zzcq != null && equal(zzcq.value, entry.getValue())) {
            return zzcq;
        }
        return null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    /* access modifiers changed from: package-private */
    public zzd<K, V> zzcq(Object obj) {
        if (obj == 0) {
            return null;
        }
        try {
            return zza(obj, false);
        } catch (ClassCastException e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public zzd<K, V> zzcr(Object obj) {
        zzd<K, V> zzcq = zzcq(obj);
        if (zzcq != null) {
            zza((zzd) zzcq, true);
        }
        return zzcq;
    }
}
