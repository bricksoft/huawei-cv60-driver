package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.proxy.ProxyRequest;
import com.google.android.gms.auth.api.proxy.ProxyResponse;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.internal.zzaa;

public class zzoc implements ProxyApi {
    @Override // com.google.android.gms.auth.api.proxy.ProxyApi
    public PendingResult<ProxyApi.ProxyResult> performProxyRequest(GoogleApiClient googleApiClient, final ProxyRequest proxyRequest) {
        zzaa.zzy(googleApiClient);
        zzaa.zzy(proxyRequest);
        return googleApiClient.zzb(new zzob(googleApiClient) {
            /* class com.google.android.gms.internal.zzoc.AnonymousClass1 */

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.zzob
            public void zza(Context context, zzoa zzoa) {
                zzoa.zza(new zznx() {
                    /* class com.google.android.gms.internal.zzoc.AnonymousClass1.AnonymousClass1 */

                    @Override // com.google.android.gms.internal.zznx, com.google.android.gms.internal.zznz
                    public void zza(ProxyResponse proxyResponse) {
                        AnonymousClass1.this.zzc(new zzod(proxyResponse));
                    }
                }, proxyRequest);
            }
        });
    }
}
