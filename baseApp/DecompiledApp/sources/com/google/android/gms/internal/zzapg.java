package com.google.android.gms.internal;

import java.lang.reflect.Type;

public interface zzapg<T> {
    zzaoy zza(T t, Type type, zzapf zzapf);
}
