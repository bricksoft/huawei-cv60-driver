package com.google.android.gms.internal;

import java.lang.reflect.Type;

public interface zzaox<T> {
    T zzb(zzaoy zzaoy, Type type, zzaow zzaow);
}
