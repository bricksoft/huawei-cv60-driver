package com.google.android.gms.internal;

public final class zzvu {
    private static zzvu WD;
    private final zzvr WE = new zzvr();
    private final zzvs WF = new zzvs();

    static {
        zza(new zzvu());
    }

    private zzvu() {
    }

    protected static void zza(zzvu zzvu) {
        synchronized (zzvu.class) {
            WD = zzvu;
        }
    }

    private static zzvu zzbhd() {
        zzvu zzvu;
        synchronized (zzvu.class) {
            zzvu = WD;
        }
        return zzvu;
    }

    public static zzvr zzbhe() {
        return zzbhd().WE;
    }

    public static zzvs zzbhf() {
        return zzbhd().WF;
    }
}
