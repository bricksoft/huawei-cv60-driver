package com.google.android.gms.internal;

import java.util.ArrayList;

public final class zzaqi extends zzapk<Object> {
    public static final zzapl bpG = new zzapl() {
        /* class com.google.android.gms.internal.zzaqi.AnonymousClass1 */

        @Override // com.google.android.gms.internal.zzapl
        public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
            if (zzaqo.bB() == Object.class) {
                return new zzaqi(zzaos);
            }
            return null;
        }
    };
    private final zzaos boC;

    private zzaqi(zzaos zzaos) {
        this.boC = zzaos;
    }

    @Override // com.google.android.gms.internal.zzapk
    public void zza(zzaqr zzaqr, Object obj) {
        if (obj == null) {
            zzaqr.bA();
            return;
        }
        zzapk zzk = this.boC.zzk(obj.getClass());
        if (zzk instanceof zzaqi) {
            zzaqr.by();
            zzaqr.bz();
            return;
        }
        zzk.zza(zzaqr, obj);
    }

    @Override // com.google.android.gms.internal.zzapk
    public Object zzb(zzaqp zzaqp) {
        switch (zzaqp.bq()) {
            case BEGIN_ARRAY:
                ArrayList arrayList = new ArrayList();
                zzaqp.beginArray();
                while (zzaqp.hasNext()) {
                    arrayList.add(zzb(zzaqp));
                }
                zzaqp.endArray();
                return arrayList;
            case BEGIN_OBJECT:
                zzapw zzapw = new zzapw();
                zzaqp.beginObject();
                while (zzaqp.hasNext()) {
                    zzapw.put(zzaqp.nextName(), zzb(zzaqp));
                }
                zzaqp.endObject();
                return zzapw;
            case STRING:
                return zzaqp.nextString();
            case NUMBER:
                return Double.valueOf(zzaqp.nextDouble());
            case BOOLEAN:
                return Boolean.valueOf(zzaqp.nextBoolean());
            case NULL:
                zzaqp.nextNull();
                return null;
            default:
                throw new IllegalStateException();
        }
    }
}
