package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.internal.zzst;

public class zzsr extends zzj<zzst> {
    public zzsr(Context context, Looper looper, zzf zzf, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 39, zzf, connectionCallbacks, onConnectionFailedListener);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzdz */
    public zzst zzh(IBinder iBinder) {
        return zzst.zza.zzeb(iBinder);
    }

    @Override // com.google.android.gms.common.internal.zze
    public String zzjx() {
        return "com.google.android.gms.common.service.START";
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjy() {
        return "com.google.android.gms.common.internal.service.ICommonService";
    }
}
