package com.google.android.gms.internal;

import tv.danmaku.ijk.media.player.IjkMediaMeta;

public interface zzaf {

    public static final class zza extends zzaru<zza> {
        public String zzcn = null;
        public String zzcp = null;
        public String zzcq = null;
        public String zzcr = null;
        public String zzda = null;
        public String zzdb = null;
        public Long zzdc = null;
        public Long zzdd = null;
        public Long zzde = null;
        public Long zzdf = null;
        public Long zzdg = null;
        public Long zzdh = null;
        public Long zzdi = null;
        public Long zzdj = null;
        public Long zzdk = null;
        public Long zzdl = null;
        public String zzdm = null;
        public Long zzdn = null;
        public Long zzdo = null;
        public Long zzdp = null;
        public Long zzdq = null;
        public Long zzdr = null;
        public Long zzds = null;
        public Long zzdt = null;
        public Long zzdu = null;
        public Long zzdv = null;
        public String zzdw = null;
        public Long zzdx = null;
        public Long zzdy = null;
        public Long zzdz = null;
        public Long zzea = null;
        public Long zzeb = null;
        public Long zzec = null;
        public zzb zzed;
        public Long zzee = null;
        public Long zzef = null;
        public Long zzeg = null;
        public Long zzeh = null;
        public Long zzei = null;
        public Long zzej = null;
        public Integer zzek = null;
        public Integer zzel = null;
        public Long zzem = null;
        public Long zzen = null;
        public Long zzeo = null;
        public Long zzep = null;
        public Long zzeq = null;
        public Integer zzer = null;
        public C0074zza zzes;
        public C0074zza[] zzet = C0074zza.zzaa();
        public zzb zzeu;
        public Long zzev = null;
        public String zzew = null;
        public Integer zzex = null;
        public Boolean zzey = null;
        public String zzez = null;
        public Long zzfa = null;
        public zze zzfb;

        /* renamed from: com.google.android.gms.internal.zzaf$zza$zza  reason: collision with other inner class name */
        public static final class C0074zza extends zzaru<C0074zza> {
            private static volatile C0074zza[] zzfc;
            public Long zzdn = null;
            public Long zzdo = null;
            public Long zzfd = null;
            public Long zzfe = null;
            public Long zzff = null;
            public Long zzfg = null;
            public Integer zzfh = null;
            public Long zzfi = null;
            public Long zzfj = null;
            public Long zzfk = null;
            public Integer zzfl = null;
            public Long zzfm = null;

            public C0074zza() {
                this.btP = -1;
            }

            public static C0074zza[] zzaa() {
                if (zzfc == null) {
                    synchronized (zzary.btO) {
                        if (zzfc == null) {
                            zzfc = new C0074zza[0];
                        }
                    }
                }
                return zzfc;
            }

            @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
            public void zza(zzart zzart) {
                if (this.zzdn != null) {
                    zzart.zzb(1, this.zzdn.longValue());
                }
                if (this.zzdo != null) {
                    zzart.zzb(2, this.zzdo.longValue());
                }
                if (this.zzfd != null) {
                    zzart.zzb(3, this.zzfd.longValue());
                }
                if (this.zzfe != null) {
                    zzart.zzb(4, this.zzfe.longValue());
                }
                if (this.zzff != null) {
                    zzart.zzb(5, this.zzff.longValue());
                }
                if (this.zzfg != null) {
                    zzart.zzb(6, this.zzfg.longValue());
                }
                if (this.zzfh != null) {
                    zzart.zzaf(7, this.zzfh.intValue());
                }
                if (this.zzfi != null) {
                    zzart.zzb(8, this.zzfi.longValue());
                }
                if (this.zzfj != null) {
                    zzart.zzb(9, this.zzfj.longValue());
                }
                if (this.zzfk != null) {
                    zzart.zzb(10, this.zzfk.longValue());
                }
                if (this.zzfl != null) {
                    zzart.zzaf(11, this.zzfl.intValue());
                }
                if (this.zzfm != null) {
                    zzart.zzb(12, this.zzfm.longValue());
                }
                super.zza(zzart);
            }

            /* renamed from: zzg */
            public C0074zza zzb(zzars zzars) {
                while (true) {
                    int bU = zzars.bU();
                    switch (bU) {
                        case 0:
                            break;
                        case 8:
                            this.zzdn = Long.valueOf(zzars.bX());
                            break;
                        case 16:
                            this.zzdo = Long.valueOf(zzars.bX());
                            break;
                        case 24:
                            this.zzfd = Long.valueOf(zzars.bX());
                            break;
                        case 32:
                            this.zzfe = Long.valueOf(zzars.bX());
                            break;
                        case 40:
                            this.zzff = Long.valueOf(zzars.bX());
                            break;
                        case 48:
                            this.zzfg = Long.valueOf(zzars.bX());
                            break;
                        case 56:
                            int bY = zzars.bY();
                            switch (bY) {
                                case 0:
                                case 1:
                                case 2:
                                case 1000:
                                    this.zzfh = Integer.valueOf(bY);
                                    continue;
                            }
                        case 64:
                            this.zzfi = Long.valueOf(zzars.bX());
                            break;
                        case 72:
                            this.zzfj = Long.valueOf(zzars.bX());
                            break;
                        case 80:
                            this.zzfk = Long.valueOf(zzars.bX());
                            break;
                        case 88:
                            int bY2 = zzars.bY();
                            switch (bY2) {
                                case 0:
                                case 1:
                                case 2:
                                case 1000:
                                    this.zzfl = Integer.valueOf(bY2);
                                    continue;
                            }
                        case 96:
                            this.zzfm = Long.valueOf(zzars.bX());
                            break;
                        default:
                            if (super.zza(zzars, bU)) {
                                break;
                            } else {
                                break;
                            }
                    }
                }
                return this;
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
            public int zzx() {
                int zzx = super.zzx();
                if (this.zzdn != null) {
                    zzx += zzart.zzf(1, this.zzdn.longValue());
                }
                if (this.zzdo != null) {
                    zzx += zzart.zzf(2, this.zzdo.longValue());
                }
                if (this.zzfd != null) {
                    zzx += zzart.zzf(3, this.zzfd.longValue());
                }
                if (this.zzfe != null) {
                    zzx += zzart.zzf(4, this.zzfe.longValue());
                }
                if (this.zzff != null) {
                    zzx += zzart.zzf(5, this.zzff.longValue());
                }
                if (this.zzfg != null) {
                    zzx += zzart.zzf(6, this.zzfg.longValue());
                }
                if (this.zzfh != null) {
                    zzx += zzart.zzah(7, this.zzfh.intValue());
                }
                if (this.zzfi != null) {
                    zzx += zzart.zzf(8, this.zzfi.longValue());
                }
                if (this.zzfj != null) {
                    zzx += zzart.zzf(9, this.zzfj.longValue());
                }
                if (this.zzfk != null) {
                    zzx += zzart.zzf(10, this.zzfk.longValue());
                }
                if (this.zzfl != null) {
                    zzx += zzart.zzah(11, this.zzfl.intValue());
                }
                return this.zzfm != null ? zzx + zzart.zzf(12, this.zzfm.longValue()) : zzx;
            }
        }

        public static final class zzb extends zzaru<zzb> {
            public Long zzep = null;
            public Long zzeq = null;
            public Long zzfn = null;

            public zzb() {
                this.btP = -1;
            }

            @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
            public void zza(zzart zzart) {
                if (this.zzep != null) {
                    zzart.zzb(1, this.zzep.longValue());
                }
                if (this.zzeq != null) {
                    zzart.zzb(2, this.zzeq.longValue());
                }
                if (this.zzfn != null) {
                    zzart.zzb(3, this.zzfn.longValue());
                }
                super.zza(zzart);
            }

            /* renamed from: zzh */
            public zzb zzb(zzars zzars) {
                while (true) {
                    int bU = zzars.bU();
                    switch (bU) {
                        case 0:
                            break;
                        case 8:
                            this.zzep = Long.valueOf(zzars.bX());
                            break;
                        case 16:
                            this.zzeq = Long.valueOf(zzars.bX());
                            break;
                        case 24:
                            this.zzfn = Long.valueOf(zzars.bX());
                            break;
                        default:
                            if (super.zza(zzars, bU)) {
                                break;
                            } else {
                                break;
                            }
                    }
                }
                return this;
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
            public int zzx() {
                int zzx = super.zzx();
                if (this.zzep != null) {
                    zzx += zzart.zzf(1, this.zzep.longValue());
                }
                if (this.zzeq != null) {
                    zzx += zzart.zzf(2, this.zzeq.longValue());
                }
                return this.zzfn != null ? zzx + zzart.zzf(3, this.zzfn.longValue()) : zzx;
            }
        }

        public zza() {
            this.btP = -1;
        }

        public static zza zzd(byte[] bArr) {
            return (zza) zzasa.zza(new zza(), bArr);
        }

        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public void zza(zzart zzart) {
            if (this.zzdb != null) {
                zzart.zzq(1, this.zzdb);
            }
            if (this.zzda != null) {
                zzart.zzq(2, this.zzda);
            }
            if (this.zzdc != null) {
                zzart.zzb(3, this.zzdc.longValue());
            }
            if (this.zzdd != null) {
                zzart.zzb(4, this.zzdd.longValue());
            }
            if (this.zzde != null) {
                zzart.zzb(5, this.zzde.longValue());
            }
            if (this.zzdf != null) {
                zzart.zzb(6, this.zzdf.longValue());
            }
            if (this.zzdg != null) {
                zzart.zzb(7, this.zzdg.longValue());
            }
            if (this.zzdh != null) {
                zzart.zzb(8, this.zzdh.longValue());
            }
            if (this.zzdi != null) {
                zzart.zzb(9, this.zzdi.longValue());
            }
            if (this.zzdj != null) {
                zzart.zzb(10, this.zzdj.longValue());
            }
            if (this.zzdk != null) {
                zzart.zzb(11, this.zzdk.longValue());
            }
            if (this.zzdl != null) {
                zzart.zzb(12, this.zzdl.longValue());
            }
            if (this.zzdm != null) {
                zzart.zzq(13, this.zzdm);
            }
            if (this.zzdn != null) {
                zzart.zzb(14, this.zzdn.longValue());
            }
            if (this.zzdo != null) {
                zzart.zzb(15, this.zzdo.longValue());
            }
            if (this.zzdp != null) {
                zzart.zzb(16, this.zzdp.longValue());
            }
            if (this.zzdq != null) {
                zzart.zzb(17, this.zzdq.longValue());
            }
            if (this.zzdr != null) {
                zzart.zzb(18, this.zzdr.longValue());
            }
            if (this.zzds != null) {
                zzart.zzb(19, this.zzds.longValue());
            }
            if (this.zzdt != null) {
                zzart.zzb(20, this.zzdt.longValue());
            }
            if (this.zzev != null) {
                zzart.zzb(21, this.zzev.longValue());
            }
            if (this.zzdu != null) {
                zzart.zzb(22, this.zzdu.longValue());
            }
            if (this.zzdv != null) {
                zzart.zzb(23, this.zzdv.longValue());
            }
            if (this.zzew != null) {
                zzart.zzq(24, this.zzew);
            }
            if (this.zzfa != null) {
                zzart.zzb(25, this.zzfa.longValue());
            }
            if (this.zzex != null) {
                zzart.zzaf(26, this.zzex.intValue());
            }
            if (this.zzcn != null) {
                zzart.zzq(27, this.zzcn);
            }
            if (this.zzey != null) {
                zzart.zzg(28, this.zzey.booleanValue());
            }
            if (this.zzdw != null) {
                zzart.zzq(29, this.zzdw);
            }
            if (this.zzez != null) {
                zzart.zzq(30, this.zzez);
            }
            if (this.zzdx != null) {
                zzart.zzb(31, this.zzdx.longValue());
            }
            if (this.zzdy != null) {
                zzart.zzb(32, this.zzdy.longValue());
            }
            if (this.zzdz != null) {
                zzart.zzb(33, this.zzdz.longValue());
            }
            if (this.zzcp != null) {
                zzart.zzq(34, this.zzcp);
            }
            if (this.zzea != null) {
                zzart.zzb(35, this.zzea.longValue());
            }
            if (this.zzeb != null) {
                zzart.zzb(36, this.zzeb.longValue());
            }
            if (this.zzec != null) {
                zzart.zzb(37, this.zzec.longValue());
            }
            if (this.zzed != null) {
                zzart.zza(38, this.zzed);
            }
            if (this.zzee != null) {
                zzart.zzb(39, this.zzee.longValue());
            }
            if (this.zzef != null) {
                zzart.zzb(40, this.zzef.longValue());
            }
            if (this.zzeg != null) {
                zzart.zzb(41, this.zzeg.longValue());
            }
            if (this.zzeh != null) {
                zzart.zzb(42, this.zzeh.longValue());
            }
            if (this.zzet != null && this.zzet.length > 0) {
                for (int i = 0; i < this.zzet.length; i++) {
                    C0074zza zza = this.zzet[i];
                    if (zza != null) {
                        zzart.zza(43, zza);
                    }
                }
            }
            if (this.zzei != null) {
                zzart.zzb(44, this.zzei.longValue());
            }
            if (this.zzej != null) {
                zzart.zzb(45, this.zzej.longValue());
            }
            if (this.zzcq != null) {
                zzart.zzq(46, this.zzcq);
            }
            if (this.zzcr != null) {
                zzart.zzq(47, this.zzcr);
            }
            if (this.zzek != null) {
                zzart.zzaf(48, this.zzek.intValue());
            }
            if (this.zzel != null) {
                zzart.zzaf(49, this.zzel.intValue());
            }
            if (this.zzes != null) {
                zzart.zza(50, this.zzes);
            }
            if (this.zzem != null) {
                zzart.zzb(51, this.zzem.longValue());
            }
            if (this.zzen != null) {
                zzart.zzb(52, this.zzen.longValue());
            }
            if (this.zzeo != null) {
                zzart.zzb(53, this.zzeo.longValue());
            }
            if (this.zzep != null) {
                zzart.zzb(54, this.zzep.longValue());
            }
            if (this.zzeq != null) {
                zzart.zzb(55, this.zzeq.longValue());
            }
            if (this.zzer != null) {
                zzart.zzaf(56, this.zzer.intValue());
            }
            if (this.zzeu != null) {
                zzart.zza(57, this.zzeu);
            }
            if (this.zzfb != null) {
                zzart.zza(201, this.zzfb);
            }
            super.zza(zzart);
        }

        /* renamed from: zzf */
        public zza zzb(zzars zzars) {
            while (true) {
                int bU = zzars.bU();
                switch (bU) {
                    case 0:
                        break;
                    case 10:
                        this.zzdb = zzars.readString();
                        break;
                    case 18:
                        this.zzda = zzars.readString();
                        break;
                    case 24:
                        this.zzdc = Long.valueOf(zzars.bX());
                        break;
                    case 32:
                        this.zzdd = Long.valueOf(zzars.bX());
                        break;
                    case 40:
                        this.zzde = Long.valueOf(zzars.bX());
                        break;
                    case 48:
                        this.zzdf = Long.valueOf(zzars.bX());
                        break;
                    case 56:
                        this.zzdg = Long.valueOf(zzars.bX());
                        break;
                    case 64:
                        this.zzdh = Long.valueOf(zzars.bX());
                        break;
                    case 72:
                        this.zzdi = Long.valueOf(zzars.bX());
                        break;
                    case 80:
                        this.zzdj = Long.valueOf(zzars.bX());
                        break;
                    case 88:
                        this.zzdk = Long.valueOf(zzars.bX());
                        break;
                    case 96:
                        this.zzdl = Long.valueOf(zzars.bX());
                        break;
                    case 106:
                        this.zzdm = zzars.readString();
                        break;
                    case 112:
                        this.zzdn = Long.valueOf(zzars.bX());
                        break;
                    case 120:
                        this.zzdo = Long.valueOf(zzars.bX());
                        break;
                    case 128:
                        this.zzdp = Long.valueOf(zzars.bX());
                        break;
                    case 136:
                        this.zzdq = Long.valueOf(zzars.bX());
                        break;
                    case IjkMediaMeta.FF_PROFILE_H264_HIGH_444 /*{ENCODED_INT: 144}*/:
                        this.zzdr = Long.valueOf(zzars.bX());
                        break;
                    case 152:
                        this.zzds = Long.valueOf(zzars.bX());
                        break;
                    case 160:
                        this.zzdt = Long.valueOf(zzars.bX());
                        break;
                    case 168:
                        this.zzev = Long.valueOf(zzars.bX());
                        break;
                    case 176:
                        this.zzdu = Long.valueOf(zzars.bX());
                        break;
                    case 184:
                        this.zzdv = Long.valueOf(zzars.bX());
                        break;
                    case 194:
                        this.zzew = zzars.readString();
                        break;
                    case 200:
                        this.zzfa = Long.valueOf(zzars.bX());
                        break;
                    case 208:
                        int bY = zzars.bY();
                        switch (bY) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                this.zzex = Integer.valueOf(bY);
                                continue;
                        }
                    case 218:
                        this.zzcn = zzars.readString();
                        break;
                    case 224:
                        this.zzey = Boolean.valueOf(zzars.ca());
                        break;
                    case 234:
                        this.zzdw = zzars.readString();
                        break;
                    case 242:
                        this.zzez = zzars.readString();
                        break;
                    case 248:
                        this.zzdx = Long.valueOf(zzars.bX());
                        break;
                    case 256:
                        this.zzdy = Long.valueOf(zzars.bX());
                        break;
                    case 264:
                        this.zzdz = Long.valueOf(zzars.bX());
                        break;
                    case 274:
                        this.zzcp = zzars.readString();
                        break;
                    case 280:
                        this.zzea = Long.valueOf(zzars.bX());
                        break;
                    case 288:
                        this.zzeb = Long.valueOf(zzars.bX());
                        break;
                    case 296:
                        this.zzec = Long.valueOf(zzars.bX());
                        break;
                    case 306:
                        if (this.zzed == null) {
                            this.zzed = new zzb();
                        }
                        zzars.zza(this.zzed);
                        break;
                    case 312:
                        this.zzee = Long.valueOf(zzars.bX());
                        break;
                    case 320:
                        this.zzef = Long.valueOf(zzars.bX());
                        break;
                    case 328:
                        this.zzeg = Long.valueOf(zzars.bX());
                        break;
                    case 336:
                        this.zzeh = Long.valueOf(zzars.bX());
                        break;
                    case 346:
                        int zzc = zzasd.zzc(zzars, 346);
                        int length = this.zzet == null ? 0 : this.zzet.length;
                        C0074zza[] zzaArr = new C0074zza[(zzc + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzet, 0, zzaArr, 0, length);
                        }
                        while (length < zzaArr.length - 1) {
                            zzaArr[length] = new C0074zza();
                            zzars.zza(zzaArr[length]);
                            zzars.bU();
                            length++;
                        }
                        zzaArr[length] = new C0074zza();
                        zzars.zza(zzaArr[length]);
                        this.zzet = zzaArr;
                        break;
                    case 352:
                        this.zzei = Long.valueOf(zzars.bX());
                        break;
                    case 360:
                        this.zzej = Long.valueOf(zzars.bX());
                        break;
                    case 370:
                        this.zzcq = zzars.readString();
                        break;
                    case 378:
                        this.zzcr = zzars.readString();
                        break;
                    case 384:
                        int bY2 = zzars.bY();
                        switch (bY2) {
                            case 0:
                            case 1:
                            case 2:
                            case 1000:
                                this.zzek = Integer.valueOf(bY2);
                                continue;
                        }
                    case 392:
                        int bY3 = zzars.bY();
                        switch (bY3) {
                            case 0:
                            case 1:
                            case 2:
                            case 1000:
                                this.zzel = Integer.valueOf(bY3);
                                continue;
                        }
                    case 402:
                        if (this.zzes == null) {
                            this.zzes = new C0074zza();
                        }
                        zzars.zza(this.zzes);
                        break;
                    case 408:
                        this.zzem = Long.valueOf(zzars.bX());
                        break;
                    case 416:
                        this.zzen = Long.valueOf(zzars.bX());
                        break;
                    case 424:
                        this.zzeo = Long.valueOf(zzars.bX());
                        break;
                    case 432:
                        this.zzep = Long.valueOf(zzars.bX());
                        break;
                    case 440:
                        this.zzeq = Long.valueOf(zzars.bX());
                        break;
                    case 448:
                        int bY4 = zzars.bY();
                        switch (bY4) {
                            case 0:
                            case 1:
                            case 2:
                            case 1000:
                                this.zzer = Integer.valueOf(bY4);
                                continue;
                        }
                    case 458:
                        if (this.zzeu == null) {
                            this.zzeu = new zzb();
                        }
                        zzars.zza(this.zzeu);
                        break;
                    case 1610:
                        if (this.zzfb == null) {
                            this.zzfb = new zze();
                        }
                        zzars.zza(this.zzfb);
                        break;
                    default:
                        if (super.zza(zzars, bU)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzdb != null) {
                zzx += zzart.zzr(1, this.zzdb);
            }
            if (this.zzda != null) {
                zzx += zzart.zzr(2, this.zzda);
            }
            if (this.zzdc != null) {
                zzx += zzart.zzf(3, this.zzdc.longValue());
            }
            if (this.zzdd != null) {
                zzx += zzart.zzf(4, this.zzdd.longValue());
            }
            if (this.zzde != null) {
                zzx += zzart.zzf(5, this.zzde.longValue());
            }
            if (this.zzdf != null) {
                zzx += zzart.zzf(6, this.zzdf.longValue());
            }
            if (this.zzdg != null) {
                zzx += zzart.zzf(7, this.zzdg.longValue());
            }
            if (this.zzdh != null) {
                zzx += zzart.zzf(8, this.zzdh.longValue());
            }
            if (this.zzdi != null) {
                zzx += zzart.zzf(9, this.zzdi.longValue());
            }
            if (this.zzdj != null) {
                zzx += zzart.zzf(10, this.zzdj.longValue());
            }
            if (this.zzdk != null) {
                zzx += zzart.zzf(11, this.zzdk.longValue());
            }
            if (this.zzdl != null) {
                zzx += zzart.zzf(12, this.zzdl.longValue());
            }
            if (this.zzdm != null) {
                zzx += zzart.zzr(13, this.zzdm);
            }
            if (this.zzdn != null) {
                zzx += zzart.zzf(14, this.zzdn.longValue());
            }
            if (this.zzdo != null) {
                zzx += zzart.zzf(15, this.zzdo.longValue());
            }
            if (this.zzdp != null) {
                zzx += zzart.zzf(16, this.zzdp.longValue());
            }
            if (this.zzdq != null) {
                zzx += zzart.zzf(17, this.zzdq.longValue());
            }
            if (this.zzdr != null) {
                zzx += zzart.zzf(18, this.zzdr.longValue());
            }
            if (this.zzds != null) {
                zzx += zzart.zzf(19, this.zzds.longValue());
            }
            if (this.zzdt != null) {
                zzx += zzart.zzf(20, this.zzdt.longValue());
            }
            if (this.zzev != null) {
                zzx += zzart.zzf(21, this.zzev.longValue());
            }
            if (this.zzdu != null) {
                zzx += zzart.zzf(22, this.zzdu.longValue());
            }
            if (this.zzdv != null) {
                zzx += zzart.zzf(23, this.zzdv.longValue());
            }
            if (this.zzew != null) {
                zzx += zzart.zzr(24, this.zzew);
            }
            if (this.zzfa != null) {
                zzx += zzart.zzf(25, this.zzfa.longValue());
            }
            if (this.zzex != null) {
                zzx += zzart.zzah(26, this.zzex.intValue());
            }
            if (this.zzcn != null) {
                zzx += zzart.zzr(27, this.zzcn);
            }
            if (this.zzey != null) {
                zzx += zzart.zzh(28, this.zzey.booleanValue());
            }
            if (this.zzdw != null) {
                zzx += zzart.zzr(29, this.zzdw);
            }
            if (this.zzez != null) {
                zzx += zzart.zzr(30, this.zzez);
            }
            if (this.zzdx != null) {
                zzx += zzart.zzf(31, this.zzdx.longValue());
            }
            if (this.zzdy != null) {
                zzx += zzart.zzf(32, this.zzdy.longValue());
            }
            if (this.zzdz != null) {
                zzx += zzart.zzf(33, this.zzdz.longValue());
            }
            if (this.zzcp != null) {
                zzx += zzart.zzr(34, this.zzcp);
            }
            if (this.zzea != null) {
                zzx += zzart.zzf(35, this.zzea.longValue());
            }
            if (this.zzeb != null) {
                zzx += zzart.zzf(36, this.zzeb.longValue());
            }
            if (this.zzec != null) {
                zzx += zzart.zzf(37, this.zzec.longValue());
            }
            if (this.zzed != null) {
                zzx += zzart.zzc(38, this.zzed);
            }
            if (this.zzee != null) {
                zzx += zzart.zzf(39, this.zzee.longValue());
            }
            if (this.zzef != null) {
                zzx += zzart.zzf(40, this.zzef.longValue());
            }
            if (this.zzeg != null) {
                zzx += zzart.zzf(41, this.zzeg.longValue());
            }
            if (this.zzeh != null) {
                zzx += zzart.zzf(42, this.zzeh.longValue());
            }
            if (this.zzet != null && this.zzet.length > 0) {
                int i = zzx;
                for (int i2 = 0; i2 < this.zzet.length; i2++) {
                    C0074zza zza = this.zzet[i2];
                    if (zza != null) {
                        i += zzart.zzc(43, zza);
                    }
                }
                zzx = i;
            }
            if (this.zzei != null) {
                zzx += zzart.zzf(44, this.zzei.longValue());
            }
            if (this.zzej != null) {
                zzx += zzart.zzf(45, this.zzej.longValue());
            }
            if (this.zzcq != null) {
                zzx += zzart.zzr(46, this.zzcq);
            }
            if (this.zzcr != null) {
                zzx += zzart.zzr(47, this.zzcr);
            }
            if (this.zzek != null) {
                zzx += zzart.zzah(48, this.zzek.intValue());
            }
            if (this.zzel != null) {
                zzx += zzart.zzah(49, this.zzel.intValue());
            }
            if (this.zzes != null) {
                zzx += zzart.zzc(50, this.zzes);
            }
            if (this.zzem != null) {
                zzx += zzart.zzf(51, this.zzem.longValue());
            }
            if (this.zzen != null) {
                zzx += zzart.zzf(52, this.zzen.longValue());
            }
            if (this.zzeo != null) {
                zzx += zzart.zzf(53, this.zzeo.longValue());
            }
            if (this.zzep != null) {
                zzx += zzart.zzf(54, this.zzep.longValue());
            }
            if (this.zzeq != null) {
                zzx += zzart.zzf(55, this.zzeq.longValue());
            }
            if (this.zzer != null) {
                zzx += zzart.zzah(56, this.zzer.intValue());
            }
            if (this.zzeu != null) {
                zzx += zzart.zzc(57, this.zzeu);
            }
            return this.zzfb != null ? zzx + zzart.zzc(201, this.zzfb) : zzx;
        }
    }

    public static final class zzb extends zzaru<zzb> {
        public Long zzfo = null;
        public Integer zzfp = null;
        public Boolean zzfq = null;
        public int[] zzfr = zzasd.btR;
        public Long zzfs = null;

        public zzb() {
            this.btP = -1;
        }

        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public void zza(zzart zzart) {
            if (this.zzfo != null) {
                zzart.zzb(1, this.zzfo.longValue());
            }
            if (this.zzfp != null) {
                zzart.zzaf(2, this.zzfp.intValue());
            }
            if (this.zzfq != null) {
                zzart.zzg(3, this.zzfq.booleanValue());
            }
            if (this.zzfr != null && this.zzfr.length > 0) {
                for (int i = 0; i < this.zzfr.length; i++) {
                    zzart.zzaf(4, this.zzfr[i]);
                }
            }
            if (this.zzfs != null) {
                zzart.zza(5, this.zzfs.longValue());
            }
            super.zza(zzart);
        }

        /* renamed from: zzi */
        public zzb zzb(zzars zzars) {
            while (true) {
                int bU = zzars.bU();
                switch (bU) {
                    case 0:
                        break;
                    case 8:
                        this.zzfo = Long.valueOf(zzars.bX());
                        break;
                    case 16:
                        this.zzfp = Integer.valueOf(zzars.bY());
                        break;
                    case 24:
                        this.zzfq = Boolean.valueOf(zzars.ca());
                        break;
                    case 32:
                        int zzc = zzasd.zzc(zzars, 32);
                        int length = this.zzfr == null ? 0 : this.zzfr.length;
                        int[] iArr = new int[(zzc + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzfr, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = zzars.bY();
                            zzars.bU();
                            length++;
                        }
                        iArr[length] = zzars.bY();
                        this.zzfr = iArr;
                        break;
                    case 34:
                        int zzagt = zzars.zzagt(zzars.cd());
                        int position = zzars.getPosition();
                        int i = 0;
                        while (zzars.ci() > 0) {
                            zzars.bY();
                            i++;
                        }
                        zzars.zzagv(position);
                        int length2 = this.zzfr == null ? 0 : this.zzfr.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzfr, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = zzars.bY();
                            length2++;
                        }
                        this.zzfr = iArr2;
                        zzars.zzagu(zzagt);
                        break;
                    case 40:
                        this.zzfs = Long.valueOf(zzars.bW());
                        break;
                    default:
                        if (super.zza(zzars, bU)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public int zzx() {
            int i = 0;
            int zzx = super.zzx();
            if (this.zzfo != null) {
                zzx += zzart.zzf(1, this.zzfo.longValue());
            }
            if (this.zzfp != null) {
                zzx += zzart.zzah(2, this.zzfp.intValue());
            }
            if (this.zzfq != null) {
                zzx += zzart.zzh(3, this.zzfq.booleanValue());
            }
            if (this.zzfr != null && this.zzfr.length > 0) {
                for (int i2 = 0; i2 < this.zzfr.length; i2++) {
                    i = zzart.zzagz(this.zzfr[i2]) + i;
                }
                zzx = zzx + i + (this.zzfr.length * 1);
            }
            return this.zzfs != null ? zzx + zzart.zze(5, this.zzfs.longValue()) : zzx;
        }
    }

    public static final class zzc extends zzaru<zzc> {
        public byte[] zzft = null;
        public byte[] zzfu = null;

        public zzc() {
            this.btP = -1;
        }

        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public void zza(zzart zzart) {
            if (this.zzft != null) {
                zzart.zzb(1, this.zzft);
            }
            if (this.zzfu != null) {
                zzart.zzb(2, this.zzfu);
            }
            super.zza(zzart);
        }

        /* renamed from: zzj */
        public zzc zzb(zzars zzars) {
            while (true) {
                int bU = zzars.bU();
                switch (bU) {
                    case 0:
                        break;
                    case 10:
                        this.zzft = zzars.readBytes();
                        break;
                    case 18:
                        this.zzfu = zzars.readBytes();
                        break;
                    default:
                        if (super.zza(zzars, bU)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzft != null) {
                zzx += zzart.zzc(1, this.zzft);
            }
            return this.zzfu != null ? zzx + zzart.zzc(2, this.zzfu) : zzx;
        }
    }

    public static final class zzd extends zzaru<zzd> {
        public byte[] data = null;
        public byte[] zzfv = null;
        public byte[] zzfw = null;
        public byte[] zzfx = null;

        public zzd() {
            this.btP = -1;
        }

        public static zzd zze(byte[] bArr) {
            return (zzd) zzasa.zza(new zzd(), bArr);
        }

        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public void zza(zzart zzart) {
            if (this.data != null) {
                zzart.zzb(1, this.data);
            }
            if (this.zzfv != null) {
                zzart.zzb(2, this.zzfv);
            }
            if (this.zzfw != null) {
                zzart.zzb(3, this.zzfw);
            }
            if (this.zzfx != null) {
                zzart.zzb(4, this.zzfx);
            }
            super.zza(zzart);
        }

        /* renamed from: zzk */
        public zzd zzb(zzars zzars) {
            while (true) {
                int bU = zzars.bU();
                switch (bU) {
                    case 0:
                        break;
                    case 10:
                        this.data = zzars.readBytes();
                        break;
                    case 18:
                        this.zzfv = zzars.readBytes();
                        break;
                    case 26:
                        this.zzfw = zzars.readBytes();
                        break;
                    case 34:
                        this.zzfx = zzars.readBytes();
                        break;
                    default:
                        if (super.zza(zzars, bU)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public int zzx() {
            int zzx = super.zzx();
            if (this.data != null) {
                zzx += zzart.zzc(1, this.data);
            }
            if (this.zzfv != null) {
                zzx += zzart.zzc(2, this.zzfv);
            }
            if (this.zzfw != null) {
                zzx += zzart.zzc(3, this.zzfw);
            }
            return this.zzfx != null ? zzx + zzart.zzc(4, this.zzfx) : zzx;
        }
    }

    public static final class zze extends zzaru<zze> {
        public Long zzfo = null;
        public String zzfy = null;
        public byte[] zzfz = null;

        public zze() {
            this.btP = -1;
        }

        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public void zza(zzart zzart) {
            if (this.zzfo != null) {
                zzart.zzb(1, this.zzfo.longValue());
            }
            if (this.zzfy != null) {
                zzart.zzq(3, this.zzfy);
            }
            if (this.zzfz != null) {
                zzart.zzb(4, this.zzfz);
            }
            super.zza(zzart);
        }

        /* renamed from: zzl */
        public zze zzb(zzars zzars) {
            while (true) {
                int bU = zzars.bU();
                switch (bU) {
                    case 0:
                        break;
                    case 8:
                        this.zzfo = Long.valueOf(zzars.bX());
                        break;
                    case 26:
                        this.zzfy = zzars.readString();
                        break;
                    case 34:
                        this.zzfz = zzars.readBytes();
                        break;
                    default:
                        if (super.zza(zzars, bU)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzfo != null) {
                zzx += zzart.zzf(1, this.zzfo.longValue());
            }
            if (this.zzfy != null) {
                zzx += zzart.zzr(3, this.zzfy);
            }
            return this.zzfz != null ? zzx + zzart.zzc(4, this.zzfz) : zzx;
        }
    }

    public static final class zzf extends zzaru<zzf> {
        public byte[] zzfv = null;
        public byte[][] zzga = zzasd.btX;
        public Integer zzgb = null;
        public Integer zzgc = null;

        public zzf() {
            this.btP = -1;
        }

        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public void zza(zzart zzart) {
            if (this.zzga != null && this.zzga.length > 0) {
                for (int i = 0; i < this.zzga.length; i++) {
                    byte[] bArr = this.zzga[i];
                    if (bArr != null) {
                        zzart.zzb(1, bArr);
                    }
                }
            }
            if (this.zzfv != null) {
                zzart.zzb(2, this.zzfv);
            }
            if (this.zzgb != null) {
                zzart.zzaf(3, this.zzgb.intValue());
            }
            if (this.zzgc != null) {
                zzart.zzaf(4, this.zzgc.intValue());
            }
            super.zza(zzart);
        }

        /* renamed from: zzm */
        public zzf zzb(zzars zzars) {
            while (true) {
                int bU = zzars.bU();
                switch (bU) {
                    case 0:
                        break;
                    case 10:
                        int zzc = zzasd.zzc(zzars, 10);
                        int length = this.zzga == null ? 0 : this.zzga.length;
                        byte[][] bArr = new byte[(zzc + length)][];
                        if (length != 0) {
                            System.arraycopy(this.zzga, 0, bArr, 0, length);
                        }
                        while (length < bArr.length - 1) {
                            bArr[length] = zzars.readBytes();
                            zzars.bU();
                            length++;
                        }
                        bArr[length] = zzars.readBytes();
                        this.zzga = bArr;
                        break;
                    case 18:
                        this.zzfv = zzars.readBytes();
                        break;
                    case 24:
                        int bY = zzars.bY();
                        switch (bY) {
                            case 0:
                            case 1:
                                this.zzgb = Integer.valueOf(bY);
                                continue;
                        }
                    case 32:
                        int bY2 = zzars.bY();
                        switch (bY2) {
                            case 0:
                            case 1:
                                this.zzgc = Integer.valueOf(bY2);
                                continue;
                        }
                    default:
                        if (super.zza(zzars, bU)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public int zzx() {
            int i;
            int i2;
            int zzx = super.zzx();
            if (this.zzga == null || this.zzga.length <= 0) {
                i = zzx;
            } else {
                int i3 = 0;
                int i4 = 0;
                int i5 = 0;
                while (i3 < this.zzga.length) {
                    byte[] bArr = this.zzga[i3];
                    if (bArr != null) {
                        i5++;
                        i2 = zzart.zzbg(bArr) + i4;
                    } else {
                        i2 = i4;
                    }
                    i3++;
                    i4 = i2;
                }
                i = zzx + i4 + (i5 * 1);
            }
            if (this.zzfv != null) {
                i += zzart.zzc(2, this.zzfv);
            }
            if (this.zzgb != null) {
                i += zzart.zzah(3, this.zzgb.intValue());
            }
            return this.zzgc != null ? i + zzart.zzah(4, this.zzgc.intValue()) : i;
        }
    }
}
