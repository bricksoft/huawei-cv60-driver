package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;

@TargetApi(11)
public final class zzrq extends Fragment implements zzrp {
    private static WeakHashMap<Activity, WeakReference<zzrq>> Bg = new WeakHashMap<>();
    private Map<String, zzro> Bh = new ArrayMap();
    private Bundle Bi;
    private int zzbtt = 0;

    private void zzb(final String str, @NonNull final zzro zzro) {
        if (this.zzbtt > 0) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                /* class com.google.android.gms.internal.zzrq.AnonymousClass1 */

                public void run() {
                    if (zzrq.this.zzbtt >= 1) {
                        zzro.onCreate(zzrq.this.Bi != null ? zzrq.this.Bi.getBundle(str) : null);
                    }
                    if (zzrq.this.zzbtt >= 2) {
                        zzro.onStart();
                    }
                    if (zzrq.this.zzbtt >= 3) {
                        zzro.onStop();
                    }
                    if (zzrq.this.zzbtt >= 4) {
                        zzro.onDestroy();
                    }
                }
            });
        }
    }

    public static zzrq zzt(Activity activity) {
        zzrq zzrq;
        WeakReference<zzrq> weakReference = Bg.get(activity);
        if (weakReference == null || (zzrq = weakReference.get()) == null) {
            try {
                zzrq = (zzrq) activity.getFragmentManager().findFragmentByTag("LifecycleFragmentImpl");
                if (zzrq == null || zzrq.isRemoving()) {
                    zzrq = new zzrq();
                    activity.getFragmentManager().beginTransaction().add(zzrq, "LifecycleFragmentImpl").commitAllowingStateLoss();
                }
                Bg.put(activity, new WeakReference<>(zzrq));
            } catch (ClassCastException e) {
                throw new IllegalStateException("Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl", e);
            }
        }
        return zzrq;
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (zzro zzro : this.Bh.values()) {
            zzro.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (zzro zzro : this.Bh.values()) {
            zzro.onActivityResult(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.zzbtt = 1;
        this.Bi = bundle;
        for (Map.Entry<String, zzro> entry : this.Bh.entrySet()) {
            entry.getValue().onCreate(bundle != null ? bundle.getBundle(entry.getKey()) : null);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.zzbtt = 4;
        for (zzro zzro : this.Bh.values()) {
            zzro.onDestroy();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry<String, zzro> entry : this.Bh.entrySet()) {
                Bundle bundle2 = new Bundle();
                entry.getValue().onSaveInstanceState(bundle2);
                bundle.putBundle(entry.getKey(), bundle2);
            }
        }
    }

    public void onStart() {
        super.onStart();
        this.zzbtt = 2;
        for (zzro zzro : this.Bh.values()) {
            zzro.onStart();
        }
    }

    public void onStop() {
        super.onStop();
        this.zzbtt = 3;
        for (zzro zzro : this.Bh.values()) {
            zzro.onStop();
        }
    }

    @Override // com.google.android.gms.internal.zzrp
    public <T extends zzro> T zza(String str, Class<T> cls) {
        return cls.cast(this.Bh.get(str));
    }

    @Override // com.google.android.gms.internal.zzrp
    public void zza(String str, @NonNull zzro zzro) {
        if (!this.Bh.containsKey(str)) {
            this.Bh.put(str, zzro);
            zzb(str, zzro);
            return;
        }
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 59).append("LifecycleCallback with tag ").append(str).append(" already added to this fragment.").toString());
    }

    @Override // com.google.android.gms.internal.zzrp
    public Activity zzaty() {
        return getActivity();
    }
}
