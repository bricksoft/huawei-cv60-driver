package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.signin.internal.zzg;

public final class zzxo {
    public static final Api<zzxq> API = new Api<>("SignIn.API", hh, hg);
    public static final Api<zza> Jb = new Api<>("SignIn.INTERNAL_API", aDj, aDi);
    public static final Api.zzf<zzg> aDi = new Api.zzf<>();
    static final Api.zza<zzg, zza> aDj = new Api.zza<zzg, zza>() {
        /* class com.google.android.gms.internal.zzxo.AnonymousClass2 */

        public zzg zza(Context context, Looper looper, zzf zzf, zza zza, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
            return new zzg(context, looper, false, zzf, zza.zzcdb(), connectionCallbacks, onConnectionFailedListener);
        }
    };
    public static final Api.zzf<zzg> hg = new Api.zzf<>();
    public static final Api.zza<zzg, zzxq> hh = new Api.zza<zzg, zzxq>() {
        /* class com.google.android.gms.internal.zzxo.AnonymousClass1 */

        public zzg zza(Context context, Looper looper, zzf zzf, zzxq zzxq, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
            return new zzg(context, looper, true, zzf, zzxq == null ? zzxq.aDl : zzxq, connectionCallbacks, onConnectionFailedListener);
        }
    };
    public static final Scope jn = new Scope(Scopes.PROFILE);
    public static final Scope jo = new Scope("email");

    public static class zza implements Api.ApiOptions.HasOptions {
        private final Bundle aDk;

        public Bundle zzcdb() {
            return this.aDk;
        }
    }
}
