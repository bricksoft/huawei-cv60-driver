package com.google.android.gms.internal;

import com.google.android.gms.internal.zzaru;

public abstract class zzaru<M extends zzaru<M>> extends zzasa {
    protected zzarw btG;

    /* renamed from: cn */
    public M clone() {
        M m = (M) ((zzaru) super.clone());
        zzary.zza(this, m);
        return m;
    }

    @Override // com.google.android.gms.internal.zzasa
    public /* synthetic */ zzasa co() {
        return (zzaru) clone();
    }

    public final <T> T zza(zzarv<M, T> zzarv) {
        zzarx zzahh;
        if (this.btG == null || (zzahh = this.btG.zzahh(zzasd.zzahl(zzarv.tag))) == null) {
            return null;
        }
        return (T) zzahh.zzb(zzarv);
    }

    @Override // com.google.android.gms.internal.zzasa
    public void zza(zzart zzart) {
        if (this.btG != null) {
            for (int i = 0; i < this.btG.size(); i++) {
                this.btG.zzahi(i).zza(zzart);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzars zzars, int i) {
        int position = zzars.getPosition();
        if (!zzars.zzagr(i)) {
            return false;
        }
        int zzahl = zzasd.zzahl(i);
        zzasc zzasc = new zzasc(i, zzars.zzae(position, zzars.getPosition() - position));
        zzarx zzarx = null;
        if (this.btG == null) {
            this.btG = new zzarw();
        } else {
            zzarx = this.btG.zzahh(zzahl);
        }
        if (zzarx == null) {
            zzarx = new zzarx();
            this.btG.zza(zzahl, zzarx);
        }
        zzarx.zza(zzasc);
        return true;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.zzasa
    public int zzx() {
        int i = 0;
        if (this.btG == null) {
            return 0;
        }
        for (int i2 = 0; i2 < this.btG.size(); i2++) {
            i = this.btG.zzahi(i2).zzx() + i;
        }
        return i;
    }
}
