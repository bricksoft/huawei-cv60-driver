package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.exoplayer.C;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.zzc;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.internal.zzrm;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/* access modifiers changed from: package-private */
public final class zzqt implements zzrm {
    private final Context mContext;
    private final zzrd yW;
    private final zzrf yX;
    private final zzrf yY;
    private final Map<Api.zzc<?>, zzrf> yZ;
    private final Set<zzsa> za = Collections.newSetFromMap(new WeakHashMap());
    private final Api.zze zb;
    private Bundle zc;
    private ConnectionResult zd = null;
    private ConnectionResult ze = null;
    private boolean zf = false;
    private final Lock zg;
    private int zh = 0;
    private final Looper zzajy;

    private class zza implements zzrm.zza {
        private zza() {
        }

        @Override // com.google.android.gms.internal.zzrm.zza
        public void zzc(int i, boolean z) {
            zzqt.this.zg.lock();
            try {
                if (zzqt.this.zf || zzqt.this.ze == null || !zzqt.this.ze.isSuccess()) {
                    zzqt.this.zf = false;
                    zzqt.this.zzb((zzqt) i, (int) z);
                    return;
                }
                zzqt.this.zf = true;
                zzqt.this.yY.onConnectionSuspended(i);
                zzqt.this.zg.unlock();
            } finally {
                zzqt.this.zg.unlock();
            }
        }

        @Override // com.google.android.gms.internal.zzrm.zza
        public void zzc(@NonNull ConnectionResult connectionResult) {
            zzqt.this.zg.lock();
            try {
                zzqt.this.zd = connectionResult;
                zzqt.this.zzasc();
            } finally {
                zzqt.this.zg.unlock();
            }
        }

        @Override // com.google.android.gms.internal.zzrm.zza
        public void zzn(@Nullable Bundle bundle) {
            zzqt.this.zg.lock();
            try {
                zzqt.this.zzm(bundle);
                zzqt.this.zd = ConnectionResult.wO;
                zzqt.this.zzasc();
            } finally {
                zzqt.this.zg.unlock();
            }
        }
    }

    private class zzb implements zzrm.zza {
        private zzb() {
        }

        @Override // com.google.android.gms.internal.zzrm.zza
        public void zzc(int i, boolean z) {
            zzqt.this.zg.lock();
            try {
                if (zzqt.this.zf) {
                    zzqt.this.zf = false;
                    zzqt.this.zzb((zzqt) i, (int) z);
                    return;
                }
                zzqt.this.zf = true;
                zzqt.this.yX.onConnectionSuspended(i);
                zzqt.this.zg.unlock();
            } finally {
                zzqt.this.zg.unlock();
            }
        }

        @Override // com.google.android.gms.internal.zzrm.zza
        public void zzc(@NonNull ConnectionResult connectionResult) {
            zzqt.this.zg.lock();
            try {
                zzqt.this.ze = connectionResult;
                zzqt.this.zzasc();
            } finally {
                zzqt.this.zg.unlock();
            }
        }

        @Override // com.google.android.gms.internal.zzrm.zza
        public void zzn(@Nullable Bundle bundle) {
            zzqt.this.zg.lock();
            try {
                zzqt.this.ze = ConnectionResult.wO;
                zzqt.this.zzasc();
            } finally {
                zzqt.this.zg.unlock();
            }
        }
    }

    private zzqt(Context context, zzrd zzrd, Lock lock, Looper looper, zzc zzc, Map<Api.zzc<?>, Api.zze> map, Map<Api.zzc<?>, Api.zze> map2, zzf zzf, Api.zza<? extends zzxp, zzxq> zza2, Api.zze zze, ArrayList<zzqr> arrayList, ArrayList<zzqr> arrayList2, Map<Api<?>, Integer> map3, Map<Api<?>, Integer> map4) {
        this.mContext = context;
        this.yW = zzrd;
        this.zg = lock;
        this.zzajy = looper;
        this.zb = zze;
        this.yX = new zzrf(context, this.yW, lock, looper, zzc, map2, null, map4, null, arrayList2, new zza());
        this.yY = new zzrf(context, this.yW, lock, looper, zzc, map, zzf, map3, zza2, arrayList, new zzb());
        ArrayMap arrayMap = new ArrayMap();
        for (Api.zzc<?> zzc2 : map2.keySet()) {
            arrayMap.put(zzc2, this.yX);
        }
        for (Api.zzc<?> zzc3 : map.keySet()) {
            arrayMap.put(zzc3, this.yY);
        }
        this.yZ = Collections.unmodifiableMap(arrayMap);
    }

    public static zzqt zza(Context context, zzrd zzrd, Lock lock, Looper looper, zzc zzc, Map<Api.zzc<?>, Api.zze> map, zzf zzf, Map<Api<?>, Integer> map2, Api.zza<? extends zzxp, zzxq> zza2, ArrayList<zzqr> arrayList) {
        Api.zze zze = null;
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        for (Map.Entry<Api.zzc<?>, Api.zze> entry : map.entrySet()) {
            Api.zze value = entry.getValue();
            if (value.zzajc()) {
                zze = value;
            }
            if (value.zzain()) {
                arrayMap.put(entry.getKey(), value);
            } else {
                arrayMap2.put(entry.getKey(), value);
            }
        }
        zzaa.zza(!arrayMap.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        ArrayMap arrayMap3 = new ArrayMap();
        ArrayMap arrayMap4 = new ArrayMap();
        for (Api<?> api : map2.keySet()) {
            Api.zzc<?> zzaqv = api.zzaqv();
            if (arrayMap.containsKey(zzaqv)) {
                arrayMap3.put(api, map2.get(api));
            } else if (arrayMap2.containsKey(zzaqv)) {
                arrayMap4.put(api, map2.get(api));
            } else {
                throw new IllegalStateException("Each API in the apiTypeMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        Iterator<zzqr> it = arrayList.iterator();
        while (it.hasNext()) {
            zzqr next = it.next();
            if (arrayMap3.containsKey(next.vS)) {
                arrayList2.add(next);
            } else if (arrayMap4.containsKey(next.vS)) {
                arrayList3.add(next);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the apiTypeMap");
            }
        }
        return new zzqt(context, zzrd, lock, looper, zzc, arrayMap, arrayMap2, zzf, zza2, zze, arrayList2, arrayList3, arrayMap3, arrayMap4);
    }

    private void zza(ConnectionResult connectionResult) {
        switch (this.zh) {
            case 2:
                this.yW.zzc(connectionResult);
            case 1:
                zzase();
                break;
            default:
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                break;
        }
        this.zh = 0;
    }

    private void zzasb() {
        this.ze = null;
        this.zd = null;
        this.yX.connect();
        this.yY.connect();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zzasc() {
        if (zzb(this.zd)) {
            if (zzb(this.ze) || zzasf()) {
                zzasd();
            } else if (this.ze == null) {
            } else {
                if (this.zh == 1) {
                    zzase();
                    return;
                }
                zza(this.ze);
                this.yX.disconnect();
            }
        } else if (this.zd != null && zzb(this.ze)) {
            this.yY.disconnect();
            zza(this.zd);
        } else if (this.zd != null && this.ze != null) {
            ConnectionResult connectionResult = this.zd;
            if (this.yY.AB < this.yX.AB) {
                connectionResult = this.ze;
            }
            zza(connectionResult);
        }
    }

    private void zzasd() {
        switch (this.zh) {
            case 2:
                this.yW.zzn(this.zc);
            case 1:
                zzase();
                break;
            default:
                Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                break;
        }
        this.zh = 0;
    }

    private void zzase() {
        for (zzsa zzsa : this.za) {
            zzsa.zzajb();
        }
        this.za.clear();
    }

    private boolean zzasf() {
        return this.ze != null && this.ze.getErrorCode() == 4;
    }

    @Nullable
    private PendingIntent zzasg() {
        if (this.zb == null) {
            return null;
        }
        return PendingIntent.getActivity(this.mContext, this.yW.getSessionId(), this.zb.zzajd(), C.SAMPLE_FLAG_DECODE_ONLY);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zzb(int i, boolean z) {
        this.yW.zzc(i, z);
        this.ze = null;
        this.zd = null;
    }

    private static boolean zzb(ConnectionResult connectionResult) {
        return connectionResult != null && connectionResult.isSuccess();
    }

    private boolean zzc(zzqo.zza<? extends Result, ? extends Api.zzb> zza2) {
        Api.zzc<? extends Api.zzb> zzaqv = zza2.zzaqv();
        zzaa.zzb(this.yZ.containsKey(zzaqv), "GoogleApiClient is not configured to use the API required for this call.");
        return this.yZ.get(zzaqv).equals(this.yY);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zzm(Bundle bundle) {
        if (this.zc == null) {
            this.zc = bundle;
        } else if (bundle != null) {
            this.zc.putAll(bundle);
        }
    }

    @Override // com.google.android.gms.internal.zzrm
    public ConnectionResult blockingConnect() {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.zzrm
    public ConnectionResult blockingConnect(long j, @NonNull TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.zzrm
    public void connect() {
        this.zh = 2;
        this.zf = false;
        zzasb();
    }

    @Override // com.google.android.gms.internal.zzrm
    public void disconnect() {
        this.ze = null;
        this.zd = null;
        this.zh = 0;
        this.yX.disconnect();
        this.yY.disconnect();
        zzase();
    }

    @Override // com.google.android.gms.internal.zzrm
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append("authClient").println(":");
        this.yY.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append((CharSequence) str).append("anonClient").println(":");
        this.yX.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }

    @Override // com.google.android.gms.internal.zzrm
    @Nullable
    public ConnectionResult getConnectionResult(@NonNull Api<?> api) {
        return this.yZ.get(api.zzaqv()).equals(this.yY) ? zzasf() ? new ConnectionResult(4, zzasg()) : this.yY.getConnectionResult(api) : this.yX.getConnectionResult(api);
    }

    @Override // com.google.android.gms.internal.zzrm
    public boolean isConnected() {
        boolean z = true;
        this.zg.lock();
        try {
            if (!this.yX.isConnected() || (!zzasa() && !zzasf() && this.zh != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.internal.zzrm
    public boolean isConnecting() {
        this.zg.lock();
        try {
            return this.zh == 2;
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.internal.zzrm
    public <A extends Api.zzb, R extends Result, T extends zzqo.zza<R, A>> T zza(@NonNull T t) {
        if (!zzc(t)) {
            return (T) this.yX.zza(t);
        }
        if (!zzasf()) {
            return (T) this.yY.zza(t);
        }
        t.zzaa(new Status(4, null, zzasg()));
        return t;
    }

    @Override // com.google.android.gms.internal.zzrm
    public boolean zza(zzsa zzsa) {
        this.zg.lock();
        try {
            if ((isConnecting() || isConnected()) && !zzasa()) {
                this.za.add(zzsa);
                if (this.zh == 0) {
                    this.zh = 1;
                }
                this.ze = null;
                this.yY.connect();
                return true;
            }
            this.zg.unlock();
            return false;
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.internal.zzrm
    public void zzard() {
        this.zg.lock();
        try {
            boolean isConnecting = isConnecting();
            this.yY.disconnect();
            this.ze = new ConnectionResult(4);
            if (isConnecting) {
                new Handler(this.zzajy).post(new Runnable() {
                    /* class com.google.android.gms.internal.zzqt.AnonymousClass1 */

                    public void run() {
                        zzqt.this.zg.lock();
                        try {
                            zzqt.this.zzasc();
                        } finally {
                            zzqt.this.zg.unlock();
                        }
                    }
                });
            } else {
                zzase();
            }
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.internal.zzrm
    public void zzarz() {
        this.yX.zzarz();
        this.yY.zzarz();
    }

    public boolean zzasa() {
        return this.yY.isConnected();
    }

    @Override // com.google.android.gms.internal.zzrm
    public <A extends Api.zzb, T extends zzqo.zza<? extends Result, A>> T zzb(@NonNull T t) {
        if (!zzc(t)) {
            return (T) this.yX.zzb(t);
        }
        if (!zzasf()) {
            return (T) this.yY.zzb(t);
        }
        t.zzaa(new Status(4, null, zzasg()));
        return t;
    }
}
