package com.google.android.gms.internal;

import android.os.Handler;
import java.util.concurrent.Executor;

public class zze implements zzn {
    private final Executor zzr;

    /* access modifiers changed from: private */
    public class zza implements Runnable {
        private final zzk zzu;
        private final zzm zzv;
        private final Runnable zzw;

        public zza(zzk zzk, zzm zzm, Runnable runnable) {
            this.zzu = zzk;
            this.zzv = zzm;
            this.zzw = runnable;
        }

        public void run() {
            if (this.zzu.isCanceled()) {
                this.zzu.zzd("canceled-at-delivery");
                return;
            }
            if (this.zzv.isSuccess()) {
                this.zzu.zza((Object) this.zzv.result);
            } else {
                this.zzu.zzc(this.zzv.zzbg);
            }
            if (this.zzv.zzbh) {
                this.zzu.zzc("intermediate-response");
            } else {
                this.zzu.zzd("done");
            }
            if (this.zzw != null) {
                this.zzw.run();
            }
        }
    }

    public zze(final Handler handler) {
        this.zzr = new Executor() {
            /* class com.google.android.gms.internal.zze.AnonymousClass1 */

            public void execute(Runnable runnable) {
                handler.post(runnable);
            }
        };
    }

    @Override // com.google.android.gms.internal.zzn
    public void zza(zzk<?> zzk, zzm<?> zzm) {
        zza(zzk, zzm, null);
    }

    @Override // com.google.android.gms.internal.zzn
    public void zza(zzk<?> zzk, zzm<?> zzm, Runnable runnable) {
        zzk.zzt();
        zzk.zzc("post-response");
        this.zzr.execute(new zza(zzk, zzm, runnable));
    }

    @Override // com.google.android.gms.internal.zzn
    public void zza(zzk<?> zzk, zzr zzr2) {
        zzk.zzc("post-error");
        this.zzr.execute(new zza(zzk, zzm.zzd(zzr2), null));
    }
}
