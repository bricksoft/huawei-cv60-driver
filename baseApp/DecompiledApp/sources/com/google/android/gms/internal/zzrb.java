package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.BinderThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zze;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzp;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.internal.zzrf;
import com.google.android.gms.signin.internal.SignInResponse;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

public class zzrb implements zzre {
    private final Context mContext;
    private final Api.zza<? extends zzxp, zzxq> xQ;
    private final zzrf zA;
    private int zD;
    private int zE = 0;
    private int zF;
    private final Bundle zG = new Bundle();
    private final Set<Api.zzc> zH = new HashSet();
    private zzxp zI;
    private int zJ;
    private boolean zK;
    private boolean zL;
    private zzp zM;
    private boolean zN;
    private boolean zO;
    private final com.google.android.gms.common.internal.zzf zP;
    private ArrayList<Future<?>> zQ = new ArrayList<>();
    private final Lock zg;
    private final Map<Api<?>, Integer> zk;
    private final com.google.android.gms.common.zzc zm;
    private ConnectionResult zq;

    private static class zza implements zze.zzf {
        private final Api<?> vS;
        private final int yU;
        private final WeakReference<zzrb> zS;

        public zza(zzrb zzrb, Api<?> api, int i) {
            this.zS = new WeakReference<>(zzrb);
            this.vS = api;
            this.yU = i;
        }

        @Override // com.google.android.gms.common.internal.zze.zzf
        public void zzg(@NonNull ConnectionResult connectionResult) {
            boolean z = false;
            zzrb zzrb = this.zS.get();
            if (zzrb != null) {
                if (Looper.myLooper() == zzrb.zA.yW.getLooper()) {
                    z = true;
                }
                zzaa.zza(z, "onReportServiceBinding must be called on the GoogleApiClient handler thread");
                zzrb.zg.lock();
                try {
                    if (zzrb.zzft(0)) {
                        if (!connectionResult.isSuccess()) {
                            zzrb.zzb(connectionResult, this.vS, this.yU);
                        }
                        if (zzrb.zzasp()) {
                            zzrb.zzasq();
                        }
                        zzrb.zg.unlock();
                    }
                } finally {
                    zzrb.zg.unlock();
                }
            }
        }
    }

    private class zzb extends zzf {
        private final Map<Api.zze, zza> zT;

        public zzb(Map<Api.zze, zza> map) {
            super();
            this.zT = map;
        }

        @Override // com.google.android.gms.internal.zzrb.zzf
        @WorkerThread
        public void zzaso() {
            boolean z;
            boolean z2;
            int i = 0;
            Iterator<Api.zze> it = this.zT.keySet().iterator();
            boolean z3 = true;
            boolean z4 = false;
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                Api.zze next = it.next();
                if (!next.zzaqx()) {
                    z2 = false;
                } else if (this.zT.get(next).yU == 0) {
                    z4 = true;
                    z = true;
                    break;
                } else {
                    z2 = z3;
                    z4 = true;
                }
                z3 = z2;
            }
            if (z4) {
                i = zzrb.this.zm.isGooglePlayServicesAvailable(zzrb.this.mContext);
            }
            if (i == 0 || (!z && !z3)) {
                if (zzrb.this.zK) {
                    zzrb.this.zI.connect();
                }
                for (Api.zze zze : this.zT.keySet()) {
                    final zza zza = this.zT.get(zze);
                    if (!zze.zzaqx() || i == 0) {
                        zze.zza(zza);
                    } else {
                        zzrb.this.zA.zza(new zzrf.zza(zzrb.this) {
                            /* class com.google.android.gms.internal.zzrb.zzb.AnonymousClass2 */

                            @Override // com.google.android.gms.internal.zzrf.zza
                            public void zzaso() {
                                zza.zzg(new ConnectionResult(16, null));
                            }
                        });
                    }
                }
                return;
            }
            final ConnectionResult connectionResult = new ConnectionResult(i, null);
            zzrb.this.zA.zza(new zzrf.zza(zzrb.this) {
                /* class com.google.android.gms.internal.zzrb.zzb.AnonymousClass1 */

                @Override // com.google.android.gms.internal.zzrf.zza
                public void zzaso() {
                    zzrb.this.zzf((zzrb) connectionResult);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public class zzc extends zzf {
        private final ArrayList<Api.zze> zX;

        public zzc(ArrayList<Api.zze> arrayList) {
            super();
            this.zX = arrayList;
        }

        @Override // com.google.android.gms.internal.zzrb.zzf
        @WorkerThread
        public void zzaso() {
            zzrb.this.zA.yW.Ak = zzrb.this.zzasv();
            Iterator<Api.zze> it = this.zX.iterator();
            while (it.hasNext()) {
                it.next().zza(zzrb.this.zM, zzrb.this.zA.yW.Ak);
            }
        }
    }

    private static class zzd extends com.google.android.gms.signin.internal.zzb {
        private final WeakReference<zzrb> zS;

        zzd(zzrb zzrb) {
            this.zS = new WeakReference<>(zzrb);
        }

        @Override // com.google.android.gms.signin.internal.zzd, com.google.android.gms.signin.internal.zzb
        @BinderThread
        public void zzb(final SignInResponse signInResponse) {
            final zzrb zzrb = this.zS.get();
            if (zzrb != null) {
                zzrb.zA.zza(new zzrf.zza(zzrb) {
                    /* class com.google.android.gms.internal.zzrb.zzd.AnonymousClass1 */

                    @Override // com.google.android.gms.internal.zzrf.zza
                    public void zzaso() {
                        zzrb.zza((zzrb) signInResponse);
                    }
                });
            }
        }
    }

    private class zze implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        private zze() {
        }

        @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
        public void onConnected(Bundle bundle) {
            zzrb.this.zI.zza(new zzd(zzrb.this));
        }

        @Override // com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            zzrb.this.zg.lock();
            try {
                if (zzrb.this.zze((zzrb) connectionResult)) {
                    zzrb.this.zzast();
                    zzrb.this.zzasq();
                } else {
                    zzrb.this.zzf((zzrb) connectionResult);
                }
            } finally {
                zzrb.this.zg.unlock();
            }
        }

        @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
        public void onConnectionSuspended(int i) {
        }
    }

    private abstract class zzf implements Runnable {
        private zzf() {
        }

        @WorkerThread
        public void run() {
            zzrb.this.zg.lock();
            try {
                if (!Thread.interrupted()) {
                    zzaso();
                    zzrb.this.zg.unlock();
                }
            } catch (RuntimeException e) {
                zzrb.this.zA.zza(e);
            } finally {
                zzrb.this.zg.unlock();
            }
        }

        /* access modifiers changed from: protected */
        @WorkerThread
        public abstract void zzaso();
    }

    public zzrb(zzrf zzrf, com.google.android.gms.common.internal.zzf zzf2, Map<Api<?>, Integer> map, com.google.android.gms.common.zzc zzc2, Api.zza<? extends zzxp, zzxq> zza2, Lock lock, Context context) {
        this.zA = zzrf;
        this.zP = zzf2;
        this.zk = map;
        this.zm = zzc2;
        this.xQ = zza2;
        this.zg = lock;
        this.mContext = context;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zza(SignInResponse signInResponse) {
        if (zzft(0)) {
            ConnectionResult zzawn = signInResponse.zzawn();
            if (zzawn.isSuccess()) {
                ResolveAccountResponse zzcdn = signInResponse.zzcdn();
                ConnectionResult zzawn2 = zzcdn.zzawn();
                if (!zzawn2.isSuccess()) {
                    String valueOf = String.valueOf(zzawn2);
                    Log.wtf("GoogleApiClientConnecting", new StringBuilder(String.valueOf(valueOf).length() + 48).append("Sign-in succeeded with resolve account failure: ").append(valueOf).toString(), new Exception());
                    zzf(zzawn2);
                    return;
                }
                this.zL = true;
                this.zM = zzcdn.zzawm();
                this.zN = zzcdn.zzawo();
                this.zO = zzcdn.zzawp();
                zzasq();
            } else if (zze(zzawn)) {
                zzast();
                zzasq();
            } else {
                zzf(zzawn);
            }
        }
    }

    private boolean zza(int i, int i2, ConnectionResult connectionResult) {
        if (i2 != 1 || zzd(connectionResult)) {
            return this.zq == null || i < this.zD;
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean zzasp() {
        this.zF--;
        if (this.zF > 0) {
            return false;
        }
        if (this.zF < 0) {
            Log.w("GoogleApiClientConnecting", this.zA.yW.zzatb());
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            zzf(new ConnectionResult(8, null));
            return false;
        } else if (this.zq == null) {
            return true;
        } else {
            this.zA.AB = this.zD;
            zzf(this.zq);
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zzasq() {
        if (this.zF == 0) {
            if (!this.zK || this.zL) {
                zzasr();
            }
        }
    }

    private void zzasr() {
        ArrayList arrayList = new ArrayList();
        this.zE = 1;
        this.zF = this.zA.Aj.size();
        for (Api.zzc<?> zzc2 : this.zA.Aj.keySet()) {
            if (!this.zA.Ay.containsKey(zzc2)) {
                arrayList.add(this.zA.Aj.get(zzc2));
            } else if (zzasp()) {
                zzass();
            }
        }
        if (!arrayList.isEmpty()) {
            this.zQ.add(zzrg.zzatf().submit(new zzc(arrayList)));
        }
    }

    private void zzass() {
        this.zA.zzatd();
        zzrg.zzatf().execute(new Runnable() {
            /* class com.google.android.gms.internal.zzrb.AnonymousClass1 */

            public void run() {
                zzrb.this.zm.zzbn(zzrb.this.mContext);
            }
        });
        if (this.zI != null) {
            if (this.zN) {
                this.zI.zza(this.zM, this.zO);
            }
            zzbr(false);
        }
        for (Api.zzc<?> zzc2 : this.zA.Ay.keySet()) {
            this.zA.Aj.get(zzc2).disconnect();
        }
        this.zA.AC.zzn(this.zG.isEmpty() ? null : this.zG);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zzast() {
        this.zK = false;
        this.zA.yW.Ak = Collections.emptySet();
        for (Api.zzc<?> zzc2 : this.zH) {
            if (!this.zA.Ay.containsKey(zzc2)) {
                this.zA.Ay.put(zzc2, new ConnectionResult(17, null));
            }
        }
    }

    private void zzasu() {
        Iterator<Future<?>> it = this.zQ.iterator();
        while (it.hasNext()) {
            it.next().cancel(true);
        }
        this.zQ.clear();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private Set<Scope> zzasv() {
        if (this.zP == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(this.zP.zzavp());
        Map<Api<?>, zzf.zza> zzavr = this.zP.zzavr();
        for (Api<?> api : zzavr.keySet()) {
            if (!this.zA.Ay.containsKey(api.zzaqv())) {
                hashSet.addAll(zzavr.get(api).jw);
            }
        }
        return hashSet;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zzb(ConnectionResult connectionResult, Api<?> api, int i) {
        if (i != 2) {
            int priority = api.zzaqs().getPriority();
            if (zza(priority, i, connectionResult)) {
                this.zq = connectionResult;
                this.zD = priority;
            }
        }
        this.zA.Ay.put(api.zzaqv(), connectionResult);
    }

    private void zzbr(boolean z) {
        if (this.zI != null) {
            if (this.zI.isConnected() && z) {
                this.zI.zzcdc();
            }
            this.zI.disconnect();
            this.zM = null;
        }
    }

    private boolean zzd(ConnectionResult connectionResult) {
        return connectionResult.hasResolution() || this.zm.zzfp(connectionResult.getErrorCode()) != null;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean zze(ConnectionResult connectionResult) {
        if (this.zJ != 2) {
            return this.zJ == 1 && !connectionResult.hasResolution();
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zzf(ConnectionResult connectionResult) {
        zzasu();
        zzbr(!connectionResult.hasResolution());
        this.zA.zzh(connectionResult);
        this.zA.AC.zzc(connectionResult);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean zzft(int i) {
        if (this.zE == i) {
            return true;
        }
        Log.w("GoogleApiClientConnecting", this.zA.yW.zzatb());
        String valueOf = String.valueOf(this);
        Log.w("GoogleApiClientConnecting", new StringBuilder(String.valueOf(valueOf).length() + 23).append("Unexpected callback in ").append(valueOf).toString());
        Log.w("GoogleApiClientConnecting", new StringBuilder(33).append("mRemainingConnections=").append(this.zF).toString());
        String valueOf2 = String.valueOf(zzfu(this.zE));
        String valueOf3 = String.valueOf(zzfu(i));
        Log.wtf("GoogleApiClientConnecting", new StringBuilder(String.valueOf(valueOf2).length() + 70 + String.valueOf(valueOf3).length()).append("GoogleApiClient connecting is in step ").append(valueOf2).append(" but received callback for step ").append(valueOf3).toString(), new Exception());
        zzf(new ConnectionResult(8, null));
        return false;
    }

    private String zzfu(int i) {
        switch (i) {
            case 0:
                return "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
            case 1:
                return "STEP_GETTING_REMOTE_SERVICE";
            default:
                return "UNKNOWN";
        }
    }

    @Override // com.google.android.gms.internal.zzre
    public void begin() {
        this.zA.Ay.clear();
        this.zK = false;
        this.zq = null;
        this.zE = 0;
        this.zJ = 2;
        this.zL = false;
        this.zN = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (Api<?> api : this.zk.keySet()) {
            Api.zze zze2 = this.zA.Aj.get(api.zzaqv());
            int intValue = this.zk.get(api).intValue();
            boolean z2 = (api.zzaqs().getPriority() == 1) | z;
            if (zze2.zzain()) {
                this.zK = true;
                if (intValue < this.zJ) {
                    this.zJ = intValue;
                }
                if (intValue != 0) {
                    this.zH.add(api.zzaqv());
                }
            }
            hashMap.put(zze2, new zza(this, api, intValue));
            z = z2;
        }
        if (z) {
            this.zK = false;
        }
        if (this.zK) {
            this.zP.zzc(Integer.valueOf(this.zA.yW.getSessionId()));
            zze zze3 = new zze();
            this.zI = (zzxp) this.xQ.zza(this.mContext, this.zA.yW.getLooper(), this.zP, this.zP.zzavv(), zze3, zze3);
        }
        this.zF = this.zA.Aj.size();
        this.zQ.add(zzrg.zzatf().submit(new zzb(hashMap)));
    }

    @Override // com.google.android.gms.internal.zzre
    public void connect() {
    }

    @Override // com.google.android.gms.internal.zzre
    public boolean disconnect() {
        zzasu();
        zzbr(true);
        this.zA.zzh(null);
        return true;
    }

    @Override // com.google.android.gms.internal.zzre
    public void onConnected(Bundle bundle) {
        if (zzft(1)) {
            if (bundle != null) {
                this.zG.putAll(bundle);
            }
            if (zzasp()) {
                zzass();
            }
        }
    }

    @Override // com.google.android.gms.internal.zzre
    public void onConnectionSuspended(int i) {
        zzf(new ConnectionResult(8, null));
    }

    @Override // com.google.android.gms.internal.zzre
    public <A extends Api.zzb, R extends Result, T extends zzqo.zza<R, A>> T zza(T t) {
        this.zA.yW.Ad.add(t);
        return t;
    }

    @Override // com.google.android.gms.internal.zzre
    public void zza(ConnectionResult connectionResult, Api<?> api, int i) {
        if (zzft(1)) {
            zzb(connectionResult, api, i);
            if (zzasp()) {
                zzass();
            }
        }
    }

    @Override // com.google.android.gms.internal.zzre
    public <A extends Api.zzb, T extends zzqo.zza<? extends Result, A>> T zzb(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }
}
