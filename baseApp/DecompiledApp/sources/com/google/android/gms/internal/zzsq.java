package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzqo;

abstract class zzsq<R extends Result> extends zzqo.zza<R, zzsr> {

    /* access modifiers changed from: package-private */
    public static abstract class zza extends zzsq<Status> {
        public zza(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        /* renamed from: zzb */
        public Status zzc(Status status) {
            return status;
        }
    }

    public zzsq(GoogleApiClient googleApiClient) {
        super(zzsn.API, googleApiClient);
    }
}
