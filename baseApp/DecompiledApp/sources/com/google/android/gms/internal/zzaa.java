package com.google.android.gms.internal;

import java.io.ByteArrayOutputStream;

public class zzaa extends ByteArrayOutputStream {
    private final zzu zzbq;

    public zzaa(zzu zzu, int i) {
        this.zzbq = zzu;
        this.buf = this.zzbq.zzb(Math.max(i, 256));
    }

    private void zzd(int i) {
        if (this.count + i > this.buf.length) {
            byte[] zzb = this.zzbq.zzb((this.count + i) * 2);
            System.arraycopy(this.buf, 0, zzb, 0, this.count);
            this.zzbq.zza(this.buf);
            this.buf = zzb;
        }
    }

    @Override // java.io.OutputStream, java.io.ByteArrayOutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.zzbq.zza(this.buf);
        this.buf = null;
        super.close();
    }

    @Override // java.lang.Object
    public void finalize() {
        this.zzbq.zza(this.buf);
    }

    @Override // java.io.OutputStream, java.io.ByteArrayOutputStream
    public synchronized void write(int i) {
        zzd(1);
        super.write(i);
    }

    @Override // java.io.OutputStream
    public synchronized void write(byte[] bArr, int i, int i2) {
        zzd(i2);
        super.write(bArr, i, i2);
    }
}
