package com.google.android.gms.internal;

public interface zzae {

    public static final class zza extends zzaru<zza> {
        public String stackTrace = null;
        public String zzcs = null;
        public Long zzct = null;
        public String zzcu = null;
        public String zzcv = null;
        public Long zzcw = null;
        public Long zzcx = null;
        public String zzcy = null;
        public Long zzcz = null;
        public String zzda = null;

        public zza() {
            this.btP = -1;
        }

        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public void zza(zzart zzart) {
            if (this.zzcs != null) {
                zzart.zzq(1, this.zzcs);
            }
            if (this.zzct != null) {
                zzart.zzb(2, this.zzct.longValue());
            }
            if (this.stackTrace != null) {
                zzart.zzq(3, this.stackTrace);
            }
            if (this.zzcu != null) {
                zzart.zzq(4, this.zzcu);
            }
            if (this.zzcv != null) {
                zzart.zzq(5, this.zzcv);
            }
            if (this.zzcw != null) {
                zzart.zzb(6, this.zzcw.longValue());
            }
            if (this.zzcx != null) {
                zzart.zzb(7, this.zzcx.longValue());
            }
            if (this.zzcy != null) {
                zzart.zzq(8, this.zzcy);
            }
            if (this.zzcz != null) {
                zzart.zzb(9, this.zzcz.longValue());
            }
            if (this.zzda != null) {
                zzart.zzq(10, this.zzda);
            }
            super.zza(zzart);
        }

        /* renamed from: zze */
        public zza zzb(zzars zzars) {
            while (true) {
                int bU = zzars.bU();
                switch (bU) {
                    case 0:
                        break;
                    case 10:
                        this.zzcs = zzars.readString();
                        break;
                    case 16:
                        this.zzct = Long.valueOf(zzars.bX());
                        break;
                    case 26:
                        this.stackTrace = zzars.readString();
                        break;
                    case 34:
                        this.zzcu = zzars.readString();
                        break;
                    case 42:
                        this.zzcv = zzars.readString();
                        break;
                    case 48:
                        this.zzcw = Long.valueOf(zzars.bX());
                        break;
                    case 56:
                        this.zzcx = Long.valueOf(zzars.bX());
                        break;
                    case 66:
                        this.zzcy = zzars.readString();
                        break;
                    case 72:
                        this.zzcz = Long.valueOf(zzars.bX());
                        break;
                    case 82:
                        this.zzda = zzars.readString();
                        break;
                    default:
                        if (super.zza(zzars, bU)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.zzaru, com.google.android.gms.internal.zzasa
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzcs != null) {
                zzx += zzart.zzr(1, this.zzcs);
            }
            if (this.zzct != null) {
                zzx += zzart.zzf(2, this.zzct.longValue());
            }
            if (this.stackTrace != null) {
                zzx += zzart.zzr(3, this.stackTrace);
            }
            if (this.zzcu != null) {
                zzx += zzart.zzr(4, this.zzcu);
            }
            if (this.zzcv != null) {
                zzx += zzart.zzr(5, this.zzcv);
            }
            if (this.zzcw != null) {
                zzx += zzart.zzf(6, this.zzcw.longValue());
            }
            if (this.zzcx != null) {
                zzx += zzart.zzf(7, this.zzcx.longValue());
            }
            if (this.zzcy != null) {
                zzx += zzart.zzr(8, this.zzcy);
            }
            if (this.zzcz != null) {
                zzx += zzart.zzf(9, this.zzcz.longValue());
            }
            return this.zzda != null ? zzx + zzart.zzr(10, this.zzda) : zzx;
        }
    }
}
