package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzse<A extends Api.zzb, TResult> {
    /* access modifiers changed from: protected */
    public abstract void zzb(A a2, TaskCompletionSource<TResult> taskCompletionSource);
}
