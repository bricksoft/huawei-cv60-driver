package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzsh<A extends Api.zzb> {
    private final zzrr.zzb<?> Bm;

    public zzrr.zzb<?> zzatz() {
        return this.Bm;
    }

    /* access modifiers changed from: protected */
    public abstract void zzc(A a2, TaskCompletionSource<Void> taskCompletionSource);
}
