package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.google.android.exoplayer.hls.HlsChunkSource;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzk;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.internal.zzrj;
import com.google.android.gms.internal.zzrm;
import com.google.android.gms.internal.zzsg;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;

public final class zzrd extends GoogleApiClient implements zzrm.zza {
    private final zzk Ab;
    private zzrm Ac = null;
    final Queue<zzqo.zza<?, ?>> Ad = new LinkedList();
    private volatile boolean Ae;
    private long Af = 120000;
    private long Ag = HlsChunkSource.DEFAULT_MIN_BUFFER_TO_SWITCH_UP_MS;
    private final zza Ah;
    zzrj Ai;
    final Map<Api.zzc<?>, Api.zze> Aj;
    Set<Scope> Ak = new HashSet();
    private final zzrs Al = new zzrs();
    private final ArrayList<zzqr> Am;
    private Integer An = null;
    Set<zzsf> Ao = null;
    final zzsg Ap;
    private final zzk.zza Aq = new zzk.zza() {
        /* class com.google.android.gms.internal.zzrd.AnonymousClass1 */

        @Override // com.google.android.gms.common.internal.zzk.zza
        public boolean isConnected() {
            return zzrd.this.isConnected();
        }

        @Override // com.google.android.gms.common.internal.zzk.zza
        public Bundle zzapn() {
            return null;
        }
    };
    private final Context mContext;
    private final int xN;
    private final GoogleApiAvailability xP;
    final Api.zza<? extends zzxp, zzxq> xQ;
    private boolean xT;
    final zzf zP;
    private final Lock zg;
    final Map<Api<?>, Integer> zk;
    private final Looper zzajy;

    /* access modifiers changed from: package-private */
    public final class zza extends Handler {
        zza(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    zzrd.this.zzasx();
                    return;
                case 2:
                    zzrd.this.resume();
                    return;
                default:
                    Log.w("GoogleApiClientImpl", new StringBuilder(31).append("Unknown message id: ").append(message.what).toString());
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public static class zzb extends zzrj.zza {
        private WeakReference<zzrd> Av;

        zzb(zzrd zzrd) {
            this.Av = new WeakReference<>(zzrd);
        }

        @Override // com.google.android.gms.internal.zzrj.zza
        public void zzarr() {
            zzrd zzrd = this.Av.get();
            if (zzrd != null) {
                zzrd.resume();
            }
        }
    }

    public zzrd(Context context, Lock lock, Looper looper, zzf zzf, GoogleApiAvailability googleApiAvailability, Api.zza<? extends zzxp, zzxq> zza2, Map<Api<?>, Integer> map, List<GoogleApiClient.ConnectionCallbacks> list, List<GoogleApiClient.OnConnectionFailedListener> list2, Map<Api.zzc<?>, Api.zze> map2, int i, int i2, ArrayList<zzqr> arrayList, boolean z) {
        this.mContext = context;
        this.zg = lock;
        this.xT = z;
        this.Ab = new zzk(looper, this.Aq);
        this.zzajy = looper;
        this.Ah = new zza(looper);
        this.xP = googleApiAvailability;
        this.xN = i;
        if (this.xN >= 0) {
            this.An = Integer.valueOf(i2);
        }
        this.zk = map;
        this.Aj = map2;
        this.Am = arrayList;
        this.Ap = new zzsg(this.Aj);
        for (GoogleApiClient.ConnectionCallbacks connectionCallbacks : list) {
            this.Ab.registerConnectionCallbacks(connectionCallbacks);
        }
        for (GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener : list2) {
            this.Ab.registerConnectionFailedListener(onConnectionFailedListener);
        }
        this.zP = zzf;
        this.xQ = zza2;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void resume() {
        this.zg.lock();
        try {
            if (isResuming()) {
                zzasw();
            }
        } finally {
            this.zg.unlock();
        }
    }

    public static int zza(Iterable<Api.zze> iterable, boolean z) {
        boolean z2 = false;
        boolean z3 = false;
        for (Api.zze zze : iterable) {
            if (zze.zzain()) {
                z3 = true;
            }
            z2 = zze.zzajc() ? true : z2;
        }
        if (z3) {
            return (!z2 || !z) ? 1 : 2;
        }
        return 3;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zza(final GoogleApiClient googleApiClient, final zzsc zzsc, final boolean z) {
        zzsn.EU.zzg(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            /* class com.google.android.gms.internal.zzrd.AnonymousClass4 */

            /* renamed from: zzp */
            public void onResult(@NonNull Status status) {
                com.google.android.gms.auth.api.signin.internal.zzk.zzba(zzrd.this.mContext).zzajo();
                if (status.isSuccess() && zzrd.this.isConnected()) {
                    zzrd.this.reconnect();
                }
                zzsc.zzc((Result) status);
                if (z) {
                    googleApiClient.disconnect();
                }
            }
        });
    }

    private void zzasw() {
        this.Ab.zzawd();
        this.Ac.connect();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zzasx() {
        this.zg.lock();
        try {
            if (zzasz()) {
                zzasw();
            }
        } finally {
            this.zg.unlock();
        }
    }

    private void zzb(@NonNull zzrn zzrn) {
        if (this.xN >= 0) {
            zzqm.zza(zzrn).zzfs(this.xN);
            return;
        }
        throw new IllegalStateException("Called stopAutoManage but automatic lifecycle management is not enabled.");
    }

    private void zzfv(int i) {
        if (this.An == null) {
            this.An = Integer.valueOf(i);
        } else if (this.An.intValue() != i) {
            String valueOf = String.valueOf(zzfw(i));
            String valueOf2 = String.valueOf(zzfw(this.An.intValue()));
            throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 51 + String.valueOf(valueOf2).length()).append("Cannot use sign-in mode: ").append(valueOf).append(". Mode was already set to ").append(valueOf2).toString());
        }
        if (this.Ac == null) {
            boolean z = false;
            boolean z2 = false;
            for (Api.zze zze : this.Aj.values()) {
                if (zze.zzain()) {
                    z2 = true;
                }
                z = zze.zzajc() ? true : z;
            }
            switch (this.An.intValue()) {
                case 1:
                    if (!z2) {
                        throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
                    } else if (z) {
                        throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
                    }
                    break;
                case 2:
                    if (z2) {
                        this.Ac = zzqt.zza(this.mContext, this, this.zg, this.zzajy, this.xP, this.Aj, this.zP, this.zk, this.xQ, this.Am);
                        return;
                    }
                    break;
            }
            if (!this.xT || z2 || z) {
                this.Ac = new zzrf(this.mContext, this, this.zg, this.zzajy, this.xP, this.Aj, this.zP, this.zk, this.xQ, this.Am, this);
            } else {
                this.Ac = new zzqu(this.mContext, this.zg, this.zzajy, this.xP, this.Aj, this.zk, this.Am, this);
            }
        }
    }

    static String zzfw(int i) {
        switch (i) {
            case 1:
                return "SIGN_IN_MODE_REQUIRED";
            case 2:
                return "SIGN_IN_MODE_OPTIONAL";
            case 3:
                return "SIGN_IN_MODE_NONE";
            default:
                return "UNKNOWN";
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public ConnectionResult blockingConnect() {
        boolean z = true;
        zzaa.zza(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.zg.lock();
        try {
            if (this.xN >= 0) {
                if (this.An == null) {
                    z = false;
                }
                zzaa.zza(z, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.An == null) {
                this.An = Integer.valueOf(zza(this.Aj.values(), false));
            } else if (this.An.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            zzfv(this.An.intValue());
            this.Ab.zzawd();
            return this.Ac.blockingConnect();
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public ConnectionResult blockingConnect(long j, @NonNull TimeUnit timeUnit) {
        boolean z = false;
        if (Looper.myLooper() != Looper.getMainLooper()) {
            z = true;
        }
        zzaa.zza(z, "blockingConnect must not be called on the UI thread");
        zzaa.zzb(timeUnit, "TimeUnit must not be null");
        this.zg.lock();
        try {
            if (this.An == null) {
                this.An = Integer.valueOf(zza(this.Aj.values(), false));
            } else if (this.An.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            zzfv(this.An.intValue());
            this.Ab.zzawd();
            return this.Ac.blockingConnect(j, timeUnit);
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public PendingResult<Status> clearDefaultAccountAndReconnect() {
        zzaa.zza(isConnected(), "GoogleApiClient is not connected yet.");
        zzaa.zza(this.An.intValue() != 2, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        final zzsc zzsc = new zzsc(this);
        if (this.Aj.containsKey(zzsn.hg)) {
            zza(this, zzsc, false);
        } else {
            final AtomicReference atomicReference = new AtomicReference();
            GoogleApiClient build = new GoogleApiClient.Builder(this.mContext).addApi(zzsn.API).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                /* class com.google.android.gms.internal.zzrd.AnonymousClass2 */

                @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
                public void onConnected(Bundle bundle) {
                    zzrd.this.zza((GoogleApiClient) atomicReference.get(), zzsc, true);
                }

                @Override // com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
                public void onConnectionSuspended(int i) {
                }
            }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                /* class com.google.android.gms.internal.zzrd.AnonymousClass3 */

                @Override // com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
                public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                    zzsc.zzc((Result) new Status(8));
                }
            }).setHandler(this.Ah).build();
            atomicReference.set(build);
            build.connect();
        }
        return zzsc;
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void connect() {
        boolean z = false;
        this.zg.lock();
        try {
            if (this.xN >= 0) {
                if (this.An != null) {
                    z = true;
                }
                zzaa.zza(z, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.An == null) {
                this.An = Integer.valueOf(zza(this.Aj.values(), false));
            } else if (this.An.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            connect(this.An.intValue());
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void connect(int i) {
        boolean z = true;
        this.zg.lock();
        if (!(i == 3 || i == 1 || i == 2)) {
            z = false;
        }
        try {
            zzaa.zzb(z, new StringBuilder(33).append("Illegal sign-in mode: ").append(i).toString());
            zzfv(i);
            zzasw();
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void disconnect() {
        this.zg.lock();
        try {
            this.Ap.release();
            if (this.Ac != null) {
                this.Ac.disconnect();
            }
            this.Al.release();
            for (zzqo.zza<?, ?> zza2 : this.Ad) {
                zza2.zza((zzsg.zzb) null);
                zza2.cancel();
            }
            this.Ad.clear();
            if (this.Ac != null) {
                zzasz();
                this.Ab.zzawc();
                this.zg.unlock();
            }
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append("mContext=").println(this.mContext);
        printWriter.append((CharSequence) str).append("mResuming=").print(this.Ae);
        printWriter.append(" mWorkQueue.size()=").print(this.Ad.size());
        this.Ap.dump(printWriter);
        if (this.Ac != null) {
            this.Ac.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    @NonNull
    public ConnectionResult getConnectionResult(@NonNull Api<?> api) {
        this.zg.lock();
        try {
            if (!isConnected() && !isResuming()) {
                throw new IllegalStateException("Cannot invoke getConnectionResult unless GoogleApiClient is connected");
            } else if (this.Aj.containsKey(api.zzaqv())) {
                ConnectionResult connectionResult = this.Ac.getConnectionResult(api);
                if (connectionResult != null) {
                    this.zg.unlock();
                    return connectionResult;
                } else if (isResuming()) {
                    return ConnectionResult.wO;
                } else {
                    Log.w("GoogleApiClientImpl", zzatb());
                    Log.wtf("GoogleApiClientImpl", String.valueOf(api.getName()).concat(" requested in getConnectionResult is not connected but is not present in the failed  connections map"), new Exception());
                    ConnectionResult connectionResult2 = new ConnectionResult(8, null);
                    this.zg.unlock();
                    return connectionResult2;
                }
            } else {
                throw new IllegalArgumentException(String.valueOf(api.getName()).concat(" was never registered with GoogleApiClient"));
            }
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public Context getContext() {
        return this.mContext;
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public Looper getLooper() {
        return this.zzajy;
    }

    public int getSessionId() {
        return System.identityHashCode(this);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public boolean hasConnectedApi(@NonNull Api<?> api) {
        if (!isConnected()) {
            return false;
        }
        Api.zze zze = this.Aj.get(api.zzaqv());
        return zze != null && zze.isConnected();
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public boolean isConnected() {
        return this.Ac != null && this.Ac.isConnected();
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public boolean isConnecting() {
        return this.Ac != null && this.Ac.isConnecting();
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public boolean isConnectionCallbacksRegistered(@NonNull GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        return this.Ab.isConnectionCallbacksRegistered(connectionCallbacks);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public boolean isConnectionFailedListenerRegistered(@NonNull GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        return this.Ab.isConnectionFailedListenerRegistered(onConnectionFailedListener);
    }

    /* access modifiers changed from: package-private */
    public boolean isResuming() {
        return this.Ae;
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void reconnect() {
        disconnect();
        connect();
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void registerConnectionCallbacks(@NonNull GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        this.Ab.registerConnectionCallbacks(connectionCallbacks);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void registerConnectionFailedListener(@NonNull GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.Ab.registerConnectionFailedListener(onConnectionFailedListener);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void stopAutoManage(@NonNull FragmentActivity fragmentActivity) {
        zzb(new zzrn(fragmentActivity));
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void unregisterConnectionCallbacks(@NonNull GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        this.Ab.unregisterConnectionCallbacks(connectionCallbacks);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void unregisterConnectionFailedListener(@NonNull GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.Ab.unregisterConnectionFailedListener(onConnectionFailedListener);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    @NonNull
    public <C extends Api.zze> C zza(@NonNull Api.zzc<C> zzc) {
        C c = (C) this.Aj.get(zzc);
        zzaa.zzb(c, "Appropriate Api was not requested.");
        return c;
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public <A extends Api.zzb, R extends Result, T extends zzqo.zza<R, A>> T zza(@NonNull T t) {
        zzaa.zzb(t.zzaqv() != null, "This task can not be enqueued (it's probably a Batch or malformed)");
        boolean containsKey = this.Aj.containsKey(t.zzaqv());
        String name = t.getApi() != null ? t.getApi().getName() : "the API";
        zzaa.zzb(containsKey, new StringBuilder(String.valueOf(name).length() + 65).append("GoogleApiClient is not configured to use ").append(name).append(" required for this call.").toString());
        this.zg.lock();
        try {
            if (this.Ac == null) {
                this.Ad.add(t);
                return t;
            }
            T t2 = (T) this.Ac.zza(t);
            this.zg.unlock();
            return t2;
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void zza(zzsf zzsf) {
        this.zg.lock();
        try {
            if (this.Ao == null) {
                this.Ao = new HashSet();
            }
            this.Ao.add(zzsf);
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public boolean zza(@NonNull Api<?> api) {
        return this.Aj.containsKey(api.zzaqv());
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public boolean zza(zzsa zzsa) {
        return this.Ac != null && this.Ac.zza(zzsa);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void zzard() {
        if (this.Ac != null) {
            this.Ac.zzard();
        }
    }

    /* access modifiers changed from: package-private */
    public void zzasy() {
        if (!isResuming()) {
            this.Ae = true;
            if (this.Ai == null) {
                this.Ai = this.xP.zza(this.mContext.getApplicationContext(), new zzb(this));
            }
            this.Ah.sendMessageDelayed(this.Ah.obtainMessage(1), this.Af);
            this.Ah.sendMessageDelayed(this.Ah.obtainMessage(2), this.Ag);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean zzasz() {
        if (!isResuming()) {
            return false;
        }
        this.Ae = false;
        this.Ah.removeMessages(2);
        this.Ah.removeMessages(1);
        if (this.Ai != null) {
            this.Ai.unregister();
            this.Ai = null;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean zzata() {
        boolean z = false;
        this.zg.lock();
        try {
            if (this.Ao != null) {
                if (!this.Ao.isEmpty()) {
                    z = true;
                }
                this.zg.unlock();
            }
            return z;
        } finally {
            this.zg.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public String zzatb() {
        StringWriter stringWriter = new StringWriter();
        dump("", null, new PrintWriter(stringWriter), null);
        return stringWriter.toString();
    }

    /* access modifiers changed from: package-private */
    public <C extends Api.zze> C zzb(Api.zzc<?> zzc) {
        C c = (C) this.Aj.get(zzc);
        zzaa.zzb(c, "Appropriate Api was not requested.");
        return c;
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public <A extends Api.zzb, T extends zzqo.zza<? extends Result, A>> T zzb(@NonNull T t) {
        zzaa.zzb(t.zzaqv() != null, "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.Aj.containsKey(t.zzaqv());
        String name = t.getApi() != null ? t.getApi().getName() : "the API";
        zzaa.zzb(containsKey, new StringBuilder(String.valueOf(name).length() + 65).append("GoogleApiClient is not configured to use ").append(name).append(" required for this call.").toString());
        this.zg.lock();
        try {
            if (this.Ac == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            } else if (isResuming()) {
                this.Ad.add(t);
                while (!this.Ad.isEmpty()) {
                    zzqo.zza<?, ?> remove = this.Ad.remove();
                    this.Ap.zzb(remove);
                    remove.zzaa(Status.yb);
                }
                return t;
            } else {
                T t2 = (T) this.Ac.zzb(t);
                this.zg.unlock();
                return t2;
            }
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void zzb(zzsf zzsf) {
        this.zg.lock();
        try {
            if (this.Ao == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.Ao.remove(zzsf)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!zzata()) {
                this.Ac.zzarz();
            }
        } finally {
            this.zg.unlock();
        }
    }

    @Override // com.google.android.gms.internal.zzrm.zza
    public void zzc(int i, boolean z) {
        if (i == 1 && !z) {
            zzasy();
        }
        this.Ap.zzauf();
        this.Ab.zzgn(i);
        this.Ab.zzawc();
        if (i == 2) {
            zzasw();
        }
    }

    @Override // com.google.android.gms.internal.zzrm.zza
    public void zzc(ConnectionResult connectionResult) {
        if (!this.xP.zzd(this.mContext, connectionResult.getErrorCode())) {
            zzasz();
        }
        if (!isResuming()) {
            this.Ab.zzn(connectionResult);
            this.Ab.zzawc();
        }
    }

    @Override // com.google.android.gms.internal.zzrm.zza
    public void zzn(Bundle bundle) {
        while (!this.Ad.isEmpty()) {
            zzb(this.Ad.remove());
        }
        this.Ab.zzp(bundle);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public <L> zzrr<L> zzs(@NonNull L l) {
        this.zg.lock();
        try {
            return this.Al.zzb(l, this.zzajy);
        } finally {
            this.zg.unlock();
        }
    }
}
