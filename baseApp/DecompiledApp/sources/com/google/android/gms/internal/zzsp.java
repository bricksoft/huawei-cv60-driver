package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzqo;
import com.google.android.gms.internal.zzsq;

public final class zzsp implements zzso {

    /* access modifiers changed from: private */
    public static class zza extends zzsm {
        private final zzqo.zzb<Status> EW;

        public zza(zzqo.zzb<Status> zzb) {
            this.EW = zzb;
        }

        @Override // com.google.android.gms.internal.zzss, com.google.android.gms.internal.zzsm
        public void zzgv(int i) {
            this.EW.setResult(new Status(i));
        }
    }

    @Override // com.google.android.gms.internal.zzso
    public PendingResult<Status> zzg(GoogleApiClient googleApiClient) {
        return googleApiClient.zzb(new zzsq.zza(googleApiClient) {
            /* class com.google.android.gms.internal.zzsp.AnonymousClass1 */

            /* access modifiers changed from: protected */
            public void zza(zzsr zzsr) {
                ((zzst) zzsr.zzavg()).zza(new zza(this));
            }
        });
    }
}
