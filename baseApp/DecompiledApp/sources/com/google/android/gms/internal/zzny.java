package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.auth.api.zza;
import com.google.android.gms.auth.api.zzb;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.internal.zzoa;

public final class zzny extends zzj<zzoa> {
    private final Bundle ik;

    public zzny(Context context, Looper looper, zzf zzf, zzb zzb, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 16, zzf, connectionCallbacks, onConnectionFailedListener);
        this.ik = zzb == null ? new Bundle() : zzb.zzaie();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public Bundle zzahv() {
        return this.ik;
    }

    @Override // com.google.android.gms.common.api.Api.zze, com.google.android.gms.common.internal.zze
    public boolean zzain() {
        zzf zzawb = zzawb();
        return !TextUtils.isEmpty(zzawb.getAccountName()) && !zzawb.zzc(zza.API).isEmpty();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzch */
    public zzoa zzh(IBinder iBinder) {
        return zzoa.zza.zzcj(iBinder);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjx() {
        return "com.google.android.gms.auth.service.START";
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjy() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }
}
