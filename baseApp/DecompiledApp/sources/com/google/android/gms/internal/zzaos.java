package com.google.android.gms.internal;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class zzaos {
    private final ThreadLocal<Map<zzaqo<?>, zza<?>>> boa;
    private final Map<zzaqo<?>, zzapk<?>> bob;
    private final List<zzapl> boc;
    private final zzaps bod;
    private final boolean boe;
    private final boolean bof;
    private final boolean bog;
    private final boolean boh;
    final zzaow boi;
    final zzapf boj;

    /* access modifiers changed from: package-private */
    public static class zza<T> extends zzapk<T> {
        private zzapk<T> bol;

        zza() {
        }

        public void zza(zzapk<T> zzapk) {
            if (this.bol != null) {
                throw new AssertionError();
            }
            this.bol = zzapk;
        }

        @Override // com.google.android.gms.internal.zzapk
        public void zza(zzaqr zzaqr, T t) {
            if (this.bol == null) {
                throw new IllegalStateException();
            }
            this.bol.zza(zzaqr, t);
        }

        @Override // com.google.android.gms.internal.zzapk
        public T zzb(zzaqp zzaqp) {
            if (this.bol != null) {
                return this.bol.zzb(zzaqp);
            }
            throw new IllegalStateException();
        }
    }

    public zzaos() {
        this(zzapt.boW, zzaoq.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, zzapi.DEFAULT, Collections.emptyList());
    }

    zzaos(zzapt zzapt, zzaor zzaor, Map<Type, zzaou<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, zzapi zzapi, List<zzapl> list) {
        this.boa = new ThreadLocal<>();
        this.bob = Collections.synchronizedMap(new HashMap());
        this.boi = new zzaow() {
            /* class com.google.android.gms.internal.zzaos.AnonymousClass1 */
        };
        this.boj = new zzapf() {
            /* class com.google.android.gms.internal.zzaos.AnonymousClass2 */
        };
        this.bod = new zzaps(map);
        this.boe = z;
        this.bog = z3;
        this.bof = z4;
        this.boh = z5;
        ArrayList arrayList = new ArrayList();
        arrayList.add(zzaqn.bqZ);
        arrayList.add(zzaqi.bpG);
        arrayList.add(zzapt);
        arrayList.addAll(list);
        arrayList.add(zzaqn.bqG);
        arrayList.add(zzaqn.bqv);
        arrayList.add(zzaqn.bqp);
        arrayList.add(zzaqn.bqr);
        arrayList.add(zzaqn.bqt);
        arrayList.add(zzaqn.zza(Long.TYPE, Long.class, zza(zzapi)));
        arrayList.add(zzaqn.zza(Double.TYPE, Double.class, zzdf(z6)));
        arrayList.add(zzaqn.zza(Float.TYPE, Float.class, zzdg(z6)));
        arrayList.add(zzaqn.bqA);
        arrayList.add(zzaqn.bqC);
        arrayList.add(zzaqn.bqI);
        arrayList.add(zzaqn.bqK);
        arrayList.add(zzaqn.zza(BigDecimal.class, zzaqn.bqE));
        arrayList.add(zzaqn.zza(BigInteger.class, zzaqn.bqF));
        arrayList.add(zzaqn.bqM);
        arrayList.add(zzaqn.bqO);
        arrayList.add(zzaqn.bqS);
        arrayList.add(zzaqn.bqX);
        arrayList.add(zzaqn.bqQ);
        arrayList.add(zzaqn.bqm);
        arrayList.add(zzaqd.bpG);
        arrayList.add(zzaqn.bqV);
        arrayList.add(zzaql.bpG);
        arrayList.add(zzaqk.bpG);
        arrayList.add(zzaqn.bqT);
        arrayList.add(zzaqb.bpG);
        arrayList.add(zzaqn.bqk);
        arrayList.add(new zzaqc(this.bod));
        arrayList.add(new zzaqh(this.bod, z2));
        arrayList.add(new zzaqe(this.bod));
        arrayList.add(zzaqn.bra);
        arrayList.add(new zzaqj(this.bod, zzaor, zzapt));
        this.boc = Collections.unmodifiableList(arrayList);
    }

    private zzapk<Number> zza(zzapi zzapi) {
        return zzapi == zzapi.DEFAULT ? zzaqn.bqw : new zzapk<Number>() {
            /* class com.google.android.gms.internal.zzaos.AnonymousClass5 */

            public void zza(zzaqr zzaqr, Number number) {
                if (number == null) {
                    zzaqr.bA();
                } else {
                    zzaqr.zzut(number.toString());
                }
            }

            /* renamed from: zzg */
            public Number zzb(zzaqp zzaqp) {
                if (zzaqp.bq() != zzaqq.NULL) {
                    return Long.valueOf(zzaqp.nextLong());
                }
                zzaqp.nextNull();
                return null;
            }
        };
    }

    private static void zza(Object obj, zzaqp zzaqp) {
        if (obj != null) {
            try {
                if (zzaqp.bq() != zzaqq.END_DOCUMENT) {
                    throw new zzaoz("JSON document was not fully consumed.");
                }
            } catch (zzaqs e) {
                throw new zzaph(e);
            } catch (IOException e2) {
                throw new zzaoz(e2);
            }
        }
    }

    private zzapk<Number> zzdf(boolean z) {
        return z ? zzaqn.bqy : new zzapk<Number>() {
            /* class com.google.android.gms.internal.zzaos.AnonymousClass3 */

            public void zza(zzaqr zzaqr, Number number) {
                if (number == null) {
                    zzaqr.bA();
                    return;
                }
                zzaos.this.zzm(number.doubleValue());
                zzaqr.zza(number);
            }

            /* renamed from: zze */
            public Double zzb(zzaqp zzaqp) {
                if (zzaqp.bq() != zzaqq.NULL) {
                    return Double.valueOf(zzaqp.nextDouble());
                }
                zzaqp.nextNull();
                return null;
            }
        };
    }

    private zzapk<Number> zzdg(boolean z) {
        return z ? zzaqn.bqx : new zzapk<Number>() {
            /* class com.google.android.gms.internal.zzaos.AnonymousClass4 */

            public void zza(zzaqr zzaqr, Number number) {
                if (number == null) {
                    zzaqr.bA();
                    return;
                }
                zzaos.this.zzm((double) number.floatValue());
                zzaqr.zza(number);
            }

            /* renamed from: zzf */
            public Float zzb(zzaqp zzaqp) {
                if (zzaqp.bq() != zzaqq.NULL) {
                    return Float.valueOf((float) zzaqp.nextDouble());
                }
                zzaqp.nextNull();
                return null;
            }
        };
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void zzm(double d) {
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            throw new IllegalArgumentException(new StringBuilder(168).append(d).append(" is not a valid double value as per JSON specification. To override this").append(" behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.").toString());
        }
    }

    public String toString() {
        return "{serializeNulls:" + this.boe + "factories:" + this.boc + ",instanceCreators:" + this.bod + "}";
    }

    public <T> zzapk<T> zza(zzapl zzapl, zzaqo<T> zzaqo) {
        boolean z = false;
        if (!this.boc.contains(zzapl)) {
            z = true;
        }
        boolean z2 = z;
        for (zzapl zzapl2 : this.boc) {
            if (z2) {
                zzapk<T> zza2 = zzapl2.zza(this, zzaqo);
                if (zza2 != null) {
                    return zza2;
                }
            } else if (zzapl2 == zzapl) {
                z2 = true;
            }
        }
        String valueOf = String.valueOf(zzaqo);
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 22).append("GSON cannot serialize ").append(valueOf).toString());
    }

    public <T> zzapk<T> zza(zzaqo<T> zzaqo) {
        Map<zzaqo<?>, zza<?>> map;
        zzapk<T> zzapk = (zzapk<T>) this.bob.get(zzaqo);
        if (zzapk == null) {
            Map<zzaqo<?>, zza<?>> map2 = this.boa.get();
            boolean z = false;
            if (map2 == null) {
                map = new HashMap<>();
                this.boa.set(map);
                z = true;
            } else {
                map = map2;
            }
            zzapk = map.get(zzaqo);
            if (zzapk == null) {
                try {
                    zza<?> zza2 = new zza<>();
                    map.put(zzaqo, zza2);
                    for (zzapl zzapl : this.boc) {
                        zzapk = zzapl.zza(this, zzaqo);
                        if (zzapk != null) {
                            zza2.zza(zzapk);
                            this.bob.put(zzaqo, zzapk);
                            map.remove(zzaqo);
                            if (z) {
                                this.boa.remove();
                            }
                        }
                    }
                    String valueOf = String.valueOf(zzaqo);
                    throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 19).append("GSON cannot handle ").append(valueOf).toString());
                } catch (Throwable th) {
                    map.remove(zzaqo);
                    if (z) {
                        this.boa.remove();
                    }
                    throw th;
                }
            }
        }
        return zzapk;
    }

    public zzaqr zza(Writer writer) {
        if (this.bog) {
            writer.write(")]}'\n");
        }
        zzaqr zzaqr = new zzaqr(writer);
        if (this.boh) {
            zzaqr.setIndent("  ");
        }
        zzaqr.zzdk(this.boe);
        return zzaqr;
    }

    public <T> T zza(zzaoy zzaoy, Class<T> cls) {
        return (T) zzapy.zzp(cls).cast(zza(zzaoy, (Type) cls));
    }

    public <T> T zza(zzaoy zzaoy, Type type) {
        if (zzaoy == null) {
            return null;
        }
        return (T) zza(new zzaqf(zzaoy), type);
    }

    public <T> T zza(zzaqp zzaqp, Type type) {
        boolean z = true;
        boolean isLenient = zzaqp.isLenient();
        zzaqp.setLenient(true);
        try {
            zzaqp.bq();
            z = false;
            T zzb = zza(zzaqo.zzl(type)).zzb(zzaqp);
            zzaqp.setLenient(isLenient);
            return zzb;
        } catch (EOFException e) {
            if (z) {
                zzaqp.setLenient(isLenient);
                return null;
            }
            throw new zzaph(e);
        } catch (IllegalStateException e2) {
            throw new zzaph(e2);
        } catch (IOException e3) {
            throw new zzaph(e3);
        } catch (Throwable th) {
            zzaqp.setLenient(isLenient);
            throw th;
        }
    }

    public <T> T zza(Reader reader, Type type) {
        zzaqp zzaqp = new zzaqp(reader);
        T t = (T) zza(zzaqp, type);
        zza(t, zzaqp);
        return t;
    }

    public <T> T zza(String str, Type type) {
        if (str == null) {
            return null;
        }
        return (T) zza(new StringReader(str), type);
    }

    public void zza(zzaoy zzaoy, zzaqr zzaqr) {
        boolean isLenient = zzaqr.isLenient();
        zzaqr.setLenient(true);
        boolean bM = zzaqr.bM();
        zzaqr.zzdj(this.bof);
        boolean bN = zzaqr.bN();
        zzaqr.zzdk(this.boe);
        try {
            zzapz.zzb(zzaoy, zzaqr);
            zzaqr.setLenient(isLenient);
            zzaqr.zzdj(bM);
            zzaqr.zzdk(bN);
        } catch (IOException e) {
            throw new zzaoz(e);
        } catch (Throwable th) {
            zzaqr.setLenient(isLenient);
            zzaqr.zzdj(bM);
            zzaqr.zzdk(bN);
            throw th;
        }
    }

    public void zza(zzaoy zzaoy, Appendable appendable) {
        try {
            zza(zzaoy, zza(zzapz.zza(appendable)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void zza(Object obj, Type type, zzaqr zzaqr) {
        zzapk zza2 = zza(zzaqo.zzl(type));
        boolean isLenient = zzaqr.isLenient();
        zzaqr.setLenient(true);
        boolean bM = zzaqr.bM();
        zzaqr.zzdj(this.bof);
        boolean bN = zzaqr.bN();
        zzaqr.zzdk(this.boe);
        try {
            zza2.zza(zzaqr, obj);
            zzaqr.setLenient(isLenient);
            zzaqr.zzdj(bM);
            zzaqr.zzdk(bN);
        } catch (IOException e) {
            throw new zzaoz(e);
        } catch (Throwable th) {
            zzaqr.setLenient(isLenient);
            zzaqr.zzdj(bM);
            zzaqr.zzdk(bN);
            throw th;
        }
    }

    public void zza(Object obj, Type type, Appendable appendable) {
        try {
            zza(obj, type, zza(zzapz.zza(appendable)));
        } catch (IOException e) {
            throw new zzaoz(e);
        }
    }

    public String zzb(zzaoy zzaoy) {
        StringWriter stringWriter = new StringWriter();
        zza(zzaoy, stringWriter);
        return stringWriter.toString();
    }

    public String zzc(Object obj, Type type) {
        StringWriter stringWriter = new StringWriter();
        zza(obj, type, stringWriter);
        return stringWriter.toString();
    }

    public String zzck(Object obj) {
        return obj == null ? zzb(zzapa.bou) : zzc(obj, obj.getClass());
    }

    public <T> T zzf(String str, Class<T> cls) {
        return (T) zzapy.zzp(cls).cast(zza(str, cls));
    }

    public <T> zzapk<T> zzk(Class<T> cls) {
        return zza(zzaqo.zzr(cls));
    }
}
