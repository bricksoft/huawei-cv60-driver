package com.google.android.gms.internal;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class zzaqj implements zzapl {
    private final zzaps bod;
    private final zzapt bom;
    private final zzaor boo;

    public static final class zza<T> extends zzapk<T> {
        private final zzapx<T> bpK;
        private final Map<String, zzb> bqd;

        private zza(zzapx<T> zzapx, Map<String, zzb> map) {
            this.bpK = zzapx;
            this.bqd = map;
        }

        @Override // com.google.android.gms.internal.zzapk
        public void zza(zzaqr zzaqr, T t) {
            if (t == null) {
                zzaqr.bA();
                return;
            }
            zzaqr.by();
            try {
                for (zzb zzb : this.bqd.values()) {
                    if (zzb.zzcs(t)) {
                        zzaqr.zzus(zzb.name);
                        zzb.zza(zzaqr, t);
                    }
                }
                zzaqr.bz();
            } catch (IllegalAccessException e) {
                throw new AssertionError();
            }
        }

        @Override // com.google.android.gms.internal.zzapk
        public T zzb(zzaqp zzaqp) {
            if (zzaqp.bq() == zzaqq.NULL) {
                zzaqp.nextNull();
                return null;
            }
            T bj = this.bpK.bj();
            try {
                zzaqp.beginObject();
                while (zzaqp.hasNext()) {
                    zzb zzb = this.bqd.get(zzaqp.nextName());
                    if (zzb == null || !zzb.bqf) {
                        zzaqp.skipValue();
                    } else {
                        zzb.zza(zzaqp, bj);
                    }
                }
                zzaqp.endObject();
                return bj;
            } catch (IllegalStateException e) {
                throw new zzaph(e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public static abstract class zzb {
        final boolean bqe;
        final boolean bqf;
        final String name;

        protected zzb(String str, boolean z, boolean z2) {
            this.name = str;
            this.bqe = z;
            this.bqf = z2;
        }

        /* access modifiers changed from: package-private */
        public abstract void zza(zzaqp zzaqp, Object obj);

        /* access modifiers changed from: package-private */
        public abstract void zza(zzaqr zzaqr, Object obj);

        /* access modifiers changed from: package-private */
        public abstract boolean zzcs(Object obj);
    }

    public zzaqj(zzaps zzaps, zzaor zzaor, zzapt zzapt) {
        this.bod = zzaps;
        this.boo = zzaor;
        this.bom = zzapt;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private zzapk<?> zza(zzaos zzaos, Field field, zzaqo<?> zzaqo) {
        zzapk<?> zza2;
        zzapm zzapm = (zzapm) field.getAnnotation(zzapm.class);
        return (zzapm == null || (zza2 = zzaqe.zza(this.bod, zzaos, zzaqo, zzapm)) == null) ? zzaos.zza(zzaqo) : zza2;
    }

    private zzb zza(final zzaos zzaos, final Field field, String str, final zzaqo<?> zzaqo, boolean z, boolean z2) {
        final boolean zzk = zzapy.zzk(zzaqo.bB());
        return new zzb(str, z, z2) {
            /* class com.google.android.gms.internal.zzaqj.AnonymousClass1 */
            final zzapk<?> bpX = zzaqj.this.zza((zzaqj) zzaos, (zzaos) field, (Field) zzaqo);

            /* access modifiers changed from: package-private */
            @Override // com.google.android.gms.internal.zzaqj.zzb
            public void zza(zzaqp zzaqp, Object obj) {
                Object zzb = this.bpX.zzb(zzaqp);
                if (zzb != null || !zzk) {
                    field.set(obj, zzb);
                }
            }

            /* access modifiers changed from: package-private */
            @Override // com.google.android.gms.internal.zzaqj.zzb
            public void zza(zzaqr zzaqr, Object obj) {
                new zzaqm(zzaos, this.bpX, zzaqo.bC()).zza(zzaqr, field.get(obj));
            }

            @Override // com.google.android.gms.internal.zzaqj.zzb
            public boolean zzcs(Object obj) {
                return this.bqe && field.get(obj) != obj;
            }
        };
    }

    static List<String> zza(zzaor zzaor, Field field) {
        zzapn zzapn = (zzapn) field.getAnnotation(zzapn.class);
        LinkedList linkedList = new LinkedList();
        if (zzapn == null) {
            linkedList.add(zzaor.zzc(field));
        } else {
            linkedList.add(zzapn.value());
            String[] bh = zzapn.bh();
            for (String str : bh) {
                linkedList.add(str);
            }
        }
        return linkedList;
    }

    private Map<String, zzb> zza(zzaos zzaos, zzaqo<?> zzaqo, Class<?> cls) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type bC = zzaqo.bC();
        while (cls != Object.class) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                boolean zza2 = zza(field, true);
                boolean zza3 = zza(field, false);
                if (zza2 || zza3) {
                    field.setAccessible(true);
                    Type zza4 = zzapr.zza(zzaqo.bC(), cls, field.getGenericType());
                    List<String> zzd = zzd(field);
                    zzb zzb2 = null;
                    int i = 0;
                    while (i < zzd.size()) {
                        String str = zzd.get(i);
                        if (i != 0) {
                            zza2 = false;
                        }
                        zzb zzb3 = (zzb) linkedHashMap.put(str, zza(zzaos, field, str, zzaqo.zzl(zza4), zza2, zza3));
                        if (zzb2 != null) {
                            zzb3 = zzb2;
                        }
                        i++;
                        zzb2 = zzb3;
                    }
                    if (zzb2 != null) {
                        String valueOf = String.valueOf(bC);
                        String str2 = zzb2.name;
                        throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 37 + String.valueOf(str2).length()).append(valueOf).append(" declares multiple JSON fields named ").append(str2).toString());
                    }
                }
            }
            zzaqo = zzaqo.zzl(zzapr.zza(zzaqo.bC(), cls, cls.getGenericSuperclass()));
            cls = zzaqo.bB();
        }
        return linkedHashMap;
    }

    static boolean zza(Field field, boolean z, zzapt zzapt) {
        return !zzapt.zza(field.getType(), z) && !zzapt.zza(field, z);
    }

    private List<String> zzd(Field field) {
        return zza(this.boo, field);
    }

    @Override // com.google.android.gms.internal.zzapl
    public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
        Class<? super T> bB = zzaqo.bB();
        if (!Object.class.isAssignableFrom(bB)) {
            return null;
        }
        return new zza(this.bod.zzb(zzaqo), zza(zzaos, (zzaqo<?>) zzaqo, (Class<?>) bB));
    }

    public boolean zza(Field field, boolean z) {
        return zza(field, z, this.bom);
    }
}
