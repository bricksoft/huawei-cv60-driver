package com.google.android.gms.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;

public class zzqo {

    public static abstract class zza<R extends Result, A extends Api.zzb> extends zzqq<R> implements zzb<R> {
        private final Api<?> vS;
        private final Api.zzc<A> yy;

        @Deprecated
        protected zza(Api.zzc<A> zzc, GoogleApiClient googleApiClient) {
            super((GoogleApiClient) zzaa.zzb(googleApiClient, "GoogleApiClient must not be null"));
            this.yy = (Api.zzc) zzaa.zzy(zzc);
            this.vS = null;
        }

        protected zza(Api<?> api, GoogleApiClient googleApiClient) {
            super((GoogleApiClient) zzaa.zzb(googleApiClient, "GoogleApiClient must not be null"));
            this.yy = (Api.zzc<A>) api.zzaqv();
            this.vS = api;
        }

        private void zza(RemoteException remoteException) {
            zzaa(new Status(8, remoteException.getLocalizedMessage(), null));
        }

        public final Api<?> getApi() {
            return this.vS;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.google.android.gms.internal.zzqo$zza<R extends com.google.android.gms.common.api.Result, A extends com.google.android.gms.common.api.Api$zzb> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.android.gms.internal.zzqo.zzb
        public /* synthetic */ void setResult(Object obj) {
            super.zzc((Result) obj);
        }

        /* access modifiers changed from: protected */
        public abstract void zza(A a2);

        @Override // com.google.android.gms.internal.zzqo.zzb
        public final void zzaa(Status status) {
            zzaa.zzb(!status.isSuccess(), "Failed result must not be success");
            R zzc = zzc(status);
            zzc(zzc);
            zzb(zzc);
        }

        public final Api.zzc<A> zzaqv() {
            return this.yy;
        }

        public final void zzb(A a2) {
            try {
                zza(a2);
            } catch (DeadObjectException e) {
                zza(e);
                throw e;
            } catch (RemoteException e2) {
                zza(e2);
            }
        }

        /* access modifiers changed from: protected */
        public void zzb(R r) {
        }
    }

    public interface zzb<R> {
        void setResult(R r);

        void zzaa(Status status);
    }
}
