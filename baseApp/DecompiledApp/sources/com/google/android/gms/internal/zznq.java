package com.google.android.gms.internal;

import android.accounts.Account;
import com.google.android.gms.auth.account.WorkAccount;
import com.google.android.gms.auth.account.WorkAccountApi;
import com.google.android.gms.auth.account.zza;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzqo;

public class zznq implements WorkAccountApi {
    private static final Status hS = new Status(13);

    static class zza extends zza.AbstractBinderC0050zza {
        zza() {
        }

        @Override // com.google.android.gms.auth.account.zza
        public void zzbc(boolean z) {
            throw new UnsupportedOperationException();
        }

        @Override // com.google.android.gms.auth.account.zza
        public void zzd(Account account) {
            throw new UnsupportedOperationException();
        }
    }

    /* access modifiers changed from: package-private */
    public static class zzb implements WorkAccountApi.AddAccountResult {
        private final Account gj;
        private final Status hv;

        public zzb(Status status, Account account) {
            this.hv = status;
            this.gj = account;
        }

        @Override // com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult
        public Account getAccount() {
            return this.gj;
        }

        @Override // com.google.android.gms.common.api.Result
        public Status getStatus() {
            return this.hv;
        }
    }

    static class zzc implements Result {
        private final Status hv;

        public zzc(Status status) {
            this.hv = status;
        }

        @Override // com.google.android.gms.common.api.Result
        public Status getStatus() {
            return this.hv;
        }
    }

    @Override // com.google.android.gms.auth.account.WorkAccountApi
    public PendingResult<WorkAccountApi.AddAccountResult> addWorkAccount(GoogleApiClient googleApiClient, final String str) {
        return googleApiClient.zzb(new zzqo.zza<WorkAccountApi.AddAccountResult, zznr>(WorkAccount.API, googleApiClient) {
            /* class com.google.android.gms.internal.zznq.AnonymousClass2 */

            /* access modifiers changed from: protected */
            public void zza(zznr zznr) {
                ((com.google.android.gms.auth.account.zzb) zznr.zzavg()).zza(new zza() {
                    /* class com.google.android.gms.internal.zznq.AnonymousClass2.AnonymousClass1 */

                    @Override // com.google.android.gms.internal.zznq.zza, com.google.android.gms.auth.account.zza
                    public void zzd(Account account) {
                        AnonymousClass2.this.zzc(new zzb(account != null ? Status.xZ : zznq.hS, account));
                    }
                }, str);
            }

            /* access modifiers changed from: protected */
            /* renamed from: zzf */
            public WorkAccountApi.AddAccountResult zzc(Status status) {
                return new zzb(status, null);
            }
        });
    }

    @Override // com.google.android.gms.auth.account.WorkAccountApi
    public PendingResult<Result> removeWorkAccount(GoogleApiClient googleApiClient, final Account account) {
        return googleApiClient.zzb(new zzqo.zza<Result, zznr>(WorkAccount.API, googleApiClient) {
            /* class com.google.android.gms.internal.zznq.AnonymousClass3 */

            /* access modifiers changed from: protected */
            public void zza(zznr zznr) {
                ((com.google.android.gms.auth.account.zzb) zznr.zzavg()).zza(new zza() {
                    /* class com.google.android.gms.internal.zznq.AnonymousClass3.AnonymousClass1 */

                    @Override // com.google.android.gms.internal.zznq.zza, com.google.android.gms.auth.account.zza
                    public void zzbc(boolean z) {
                        AnonymousClass3.this.zzc(new zzc(z ? Status.xZ : zznq.hS));
                    }
                }, account);
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.zzqq
            public Result zzc(Status status) {
                return new zzc(status);
            }
        });
    }

    @Override // com.google.android.gms.auth.account.WorkAccountApi
    public void setWorkAuthenticatorEnabled(GoogleApiClient googleApiClient, final boolean z) {
        googleApiClient.zzb(new zzqo.zza<Result, zznr>(WorkAccount.API, googleApiClient) {
            /* class com.google.android.gms.internal.zznq.AnonymousClass1 */

            /* access modifiers changed from: protected */
            public void zza(zznr zznr) {
                ((com.google.android.gms.auth.account.zzb) zznr.zzavg()).zzbd(z);
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.zzqq
            public Result zzc(Status status) {
                return null;
            }
        });
    }
}
