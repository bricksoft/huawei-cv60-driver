package com.google.android.gms.internal;

import android.annotation.SuppressLint;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.internal.zze;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.util.Collection;

public final class zzwx implements People {

    private static abstract class zza extends Plus.zza<People.LoadPeopleResult> {
        private zza(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        /* renamed from: zzeb */
        public People.LoadPeopleResult zzc(final Status status) {
            return new People.LoadPeopleResult() {
                /* class com.google.android.gms.internal.zzwx.zza.AnonymousClass1 */

                @Override // com.google.android.gms.plus.People.LoadPeopleResult
                public String getNextPageToken() {
                    return null;
                }

                @Override // com.google.android.gms.plus.People.LoadPeopleResult
                public PersonBuffer getPersonBuffer() {
                    return null;
                }

                @Override // com.google.android.gms.common.api.Result
                public Status getStatus() {
                    return status;
                }

                @Override // com.google.android.gms.common.api.Releasable
                public void release() {
                }
            };
        }
    }

    @Override // com.google.android.gms.plus.People
    public Person getCurrentPerson(GoogleApiClient googleApiClient) {
        return Plus.zzf(googleApiClient, true).zzcbs();
    }

    @Override // com.google.android.gms.plus.People
    @SuppressLint({"MissingRemoteException"})
    public PendingResult<People.LoadPeopleResult> load(GoogleApiClient googleApiClient, final Collection<String> collection) {
        return googleApiClient.zza(new zza(googleApiClient) {
            /* class com.google.android.gms.internal.zzwx.AnonymousClass4 */

            /* access modifiers changed from: protected */
            public void zza(zze zze) {
                zze.zza(this, collection);
            }
        });
    }

    @Override // com.google.android.gms.plus.People
    @SuppressLint({"MissingRemoteException"})
    public PendingResult<People.LoadPeopleResult> load(GoogleApiClient googleApiClient, final String... strArr) {
        return googleApiClient.zza(new zza(googleApiClient) {
            /* class com.google.android.gms.internal.zzwx.AnonymousClass5 */

            /* access modifiers changed from: protected */
            public void zza(zze zze) {
                zze.zzd(this, strArr);
            }
        });
    }

    @Override // com.google.android.gms.plus.People
    @SuppressLint({"MissingRemoteException"})
    public PendingResult<People.LoadPeopleResult> loadConnected(GoogleApiClient googleApiClient) {
        return googleApiClient.zza(new zza(googleApiClient) {
            /* class com.google.android.gms.internal.zzwx.AnonymousClass3 */

            /* access modifiers changed from: protected */
            public void zza(zze zze) {
                zze.zzv(this);
            }
        });
    }

    @Override // com.google.android.gms.plus.People
    @SuppressLint({"MissingRemoteException"})
    public PendingResult<People.LoadPeopleResult> loadVisible(GoogleApiClient googleApiClient, final int i, final String str) {
        return googleApiClient.zza(new zza(googleApiClient) {
            /* class com.google.android.gms.internal.zzwx.AnonymousClass1 */

            /* access modifiers changed from: protected */
            public void zza(zze zze) {
                zza(zze.zza(this, i, str));
            }
        });
    }

    @Override // com.google.android.gms.plus.People
    @SuppressLint({"MissingRemoteException"})
    public PendingResult<People.LoadPeopleResult> loadVisible(GoogleApiClient googleApiClient, final String str) {
        return googleApiClient.zza(new zza(googleApiClient) {
            /* class com.google.android.gms.internal.zzwx.AnonymousClass2 */

            /* access modifiers changed from: protected */
            public void zza(zze zze) {
                zza(zze.zzu(this, str));
            }
        });
    }
}
