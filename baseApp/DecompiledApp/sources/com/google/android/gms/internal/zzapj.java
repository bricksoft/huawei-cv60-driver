package com.google.android.gms.internal;

final class zzapj<T> extends zzapk<T> {
    private final zzapg<T> boA;
    private final zzaox<T> boB;
    private final zzaos boC;
    private final zzaqo<T> boD;
    private final zzapl boE;
    private zzapk<T> bol;

    private static class zza implements zzapl {
        private final zzapg<?> boA;
        private final zzaox<?> boB;
        private final zzaqo<?> boF;
        private final boolean boG;
        private final Class<?> boH;

        private zza(Object obj, zzaqo<?> zzaqo, boolean z, Class<?> cls) {
            this.boA = obj instanceof zzapg ? (zzapg) obj : null;
            this.boB = obj instanceof zzaox ? (zzaox) obj : null;
            zzapq.zzbt((this.boA == null && this.boB == null) ? false : true);
            this.boF = zzaqo;
            this.boG = z;
            this.boH = cls;
        }

        @Override // com.google.android.gms.internal.zzapl
        public <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo) {
            if (this.boF != null ? this.boF.equals(zzaqo) || (this.boG && this.boF.bC() == zzaqo.bB()) : this.boH.isAssignableFrom(zzaqo.bB())) {
                return new zzapj(this.boA, this.boB, zzaos, zzaqo, this);
            }
            return null;
        }
    }

    private zzapj(zzapg<T> zzapg, zzaox<T> zzaox, zzaos zzaos, zzaqo<T> zzaqo, zzapl zzapl) {
        this.boA = zzapg;
        this.boB = zzaox;
        this.boC = zzaos;
        this.boD = zzaqo;
        this.boE = zzapl;
    }

    private zzapk<T> bg() {
        zzapk<T> zzapk = this.bol;
        if (zzapk != null) {
            return zzapk;
        }
        zzapk<T> zza2 = this.boC.zza(this.boE, this.boD);
        this.bol = zza2;
        return zza2;
    }

    public static zzapl zza(zzaqo<?> zzaqo, Object obj) {
        return new zza(obj, zzaqo, false, null);
    }

    public static zzapl zzb(zzaqo<?> zzaqo, Object obj) {
        return new zza(obj, zzaqo, zzaqo.bC() == zzaqo.bB(), null);
    }

    @Override // com.google.android.gms.internal.zzapk
    public void zza(zzaqr zzaqr, T t) {
        if (this.boA == null) {
            bg().zza(zzaqr, t);
        } else if (t == null) {
            zzaqr.bA();
        } else {
            zzapz.zzb(this.boA.zza(t, this.boD.bC(), this.boC.boj), zzaqr);
        }
    }

    @Override // com.google.android.gms.internal.zzapk
    public T zzb(zzaqp zzaqp) {
        if (this.boB == null) {
            return bg().zzb(zzaqp);
        }
        zzaoy zzh = zzapz.zzh(zzaqp);
        if (zzh.aY()) {
            return null;
        }
        try {
            return this.boB.zzb(zzh, this.boD.bC(), this.boC.boi);
        } catch (zzapc e) {
            throw e;
        } catch (Exception e2) {
            throw new zzapc(e2);
        }
    }
}
