package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Api;

public final class zzrx {
    public final zzrw<Api.zzb> yi;
    public final zzsh<Api.zzb> yj;

    public zzrx(@NonNull zzrw<Api.zzb> zzrw, @NonNull zzsh<Api.zzb> zzsh) {
        this.yi = zzrw;
        this.yj = zzsh;
    }
}
