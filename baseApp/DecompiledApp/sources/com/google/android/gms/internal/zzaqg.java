package com.google.android.gms.internal;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class zzaqg extends zzaqr {
    private static final Writer bpO = new Writer() {
        /* class com.google.android.gms.internal.zzaqg.AnonymousClass1 */

        @Override // java.io.Closeable, java.io.Writer, java.lang.AutoCloseable
        public void close() {
            throw new AssertionError();
        }

        @Override // java.io.Writer, java.io.Flushable
        public void flush() {
            throw new AssertionError();
        }

        @Override // java.io.Writer
        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    };
    private static final zzape bpP = new zzape("closed");
    private final List<zzaoy> bpN = new ArrayList();
    private String bpQ;
    private zzaoy bpR = zzapa.bou;

    public zzaqg() {
        super(bpO);
    }

    private zzaoy bv() {
        return this.bpN.get(this.bpN.size() - 1);
    }

    private void zzd(zzaoy zzaoy) {
        if (this.bpQ != null) {
            if (!zzaoy.aY() || bN()) {
                ((zzapb) bv()).zza(this.bpQ, zzaoy);
            }
            this.bpQ = null;
        } else if (this.bpN.isEmpty()) {
            this.bpR = zzaoy;
        } else {
            zzaoy bv = bv();
            if (bv instanceof zzaov) {
                ((zzaov) bv).zzc(zzaoy);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr bA() {
        zzd(zzapa.bou);
        return this;
    }

    public zzaoy bu() {
        if (this.bpN.isEmpty()) {
            return this.bpR;
        }
        String valueOf = String.valueOf(this.bpN);
        throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 34).append("Expected one JSON element but was ").append(valueOf).toString());
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr bw() {
        zzaov zzaov = new zzaov();
        zzd(zzaov);
        this.bpN.add(zzaov);
        return this;
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr bx() {
        if (this.bpN.isEmpty() || this.bpQ != null) {
            throw new IllegalStateException();
        } else if (bv() instanceof zzaov) {
            this.bpN.remove(this.bpN.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr by() {
        zzapb zzapb = new zzapb();
        zzd(zzapb);
        this.bpN.add(zzapb);
        return this;
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr bz() {
        if (this.bpN.isEmpty() || this.bpQ != null) {
            throw new IllegalStateException();
        } else if (bv() instanceof zzapb) {
            this.bpN.remove(this.bpN.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // com.google.android.gms.internal.zzaqr, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (!this.bpN.isEmpty()) {
            throw new IOException("Incomplete document");
        }
        this.bpN.add(bpP);
    }

    @Override // com.google.android.gms.internal.zzaqr, java.io.Flushable
    public void flush() {
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr zza(Number number) {
        if (number == null) {
            return bA();
        }
        if (!isLenient()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                String valueOf = String.valueOf(number);
                throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 33).append("JSON forbids NaN and infinities: ").append(valueOf).toString());
            }
        }
        zzd(new zzape(number));
        return this;
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr zzcs(long j) {
        zzd(new zzape((Number) Long.valueOf(j)));
        return this;
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr zzdh(boolean z) {
        zzd(new zzape(Boolean.valueOf(z)));
        return this;
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr zzus(String str) {
        if (this.bpN.isEmpty() || this.bpQ != null) {
            throw new IllegalStateException();
        } else if (bv() instanceof zzapb) {
            this.bpQ = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // com.google.android.gms.internal.zzaqr
    public zzaqr zzut(String str) {
        if (str == null) {
            return bA();
        }
        zzd(new zzape(str));
        return this;
    }
}
