package com.google.android.gms.internal;

import com.google.android.gms.common.api.zzc;

public class zzrv {
    public final zzqj Bq;
    public final int Br;
    public final zzc<?> Bs;

    public zzrv(zzqj zzqj, int i, zzc<?> zzc) {
        this.Bq = zzqj;
        this.Br = i;
        this.Bs = zzc;
    }
}
