package com.google.android.gms.internal;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public final class zzaps {
    private final Map<Type, zzaou<?>> bop;

    public zzaps(Map<Type, zzaou<?>> map) {
        this.bop = map;
    }

    private <T> zzapx<T> zzc(final Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            return SortedSet.class.isAssignableFrom(cls) ? new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass7 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    return (T) new TreeSet();
                }
            } : EnumSet.class.isAssignableFrom(cls) ? new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass8 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    if (type instanceof ParameterizedType) {
                        Type type = ((ParameterizedType) type).getActualTypeArguments()[0];
                        if (type instanceof Class) {
                            return (T) EnumSet.noneOf((Class) type);
                        }
                        String valueOf = String.valueOf(type.toString());
                        throw new zzaoz(valueOf.length() != 0 ? "Invalid EnumSet type: ".concat(valueOf) : new String("Invalid EnumSet type: "));
                    }
                    String valueOf2 = String.valueOf(type.toString());
                    throw new zzaoz(valueOf2.length() != 0 ? "Invalid EnumSet type: ".concat(valueOf2) : new String("Invalid EnumSet type: "));
                }
            } : Set.class.isAssignableFrom(cls) ? new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass9 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    return (T) new LinkedHashSet();
                }
            } : Queue.class.isAssignableFrom(cls) ? new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass10 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    return (T) new LinkedList();
                }
            } : new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass11 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    return (T) new ArrayList();
                }
            };
        }
        if (Map.class.isAssignableFrom(cls)) {
            return SortedMap.class.isAssignableFrom(cls) ? new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass12 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    return (T) new TreeMap();
                }
            } : (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(zzaqo.zzl(((ParameterizedType) type).getActualTypeArguments()[0]).bB())) ? new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass3 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    return (T) new zzapw();
                }
            } : new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass2 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    return (T) new LinkedHashMap();
                }
            };
        }
        return null;
    }

    private <T> zzapx<T> zzd(final Type type, final Class<? super T> cls) {
        return new zzapx<T>() {
            /* class com.google.android.gms.internal.zzaps.AnonymousClass4 */
            private final zzaqa boS = zzaqa.bo();

            @Override // com.google.android.gms.internal.zzapx
            public T bj() {
                try {
                    return (T) this.boS.zzf(cls);
                } catch (Exception e) {
                    String valueOf = String.valueOf(type);
                    throw new RuntimeException(new StringBuilder(String.valueOf(valueOf).length() + 116).append("Unable to invoke no-args constructor for ").append(valueOf).append(". ").append("Register an InstanceCreator with Gson for this type may fix this problem.").toString(), e);
                }
            }
        };
    }

    private <T> zzapx<T> zzl(Class<? super T> cls) {
        try {
            final Constructor<? super T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass6 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    try {
                        return (T) declaredConstructor.newInstance(null);
                    } catch (InstantiationException e) {
                        String valueOf = String.valueOf(declaredConstructor);
                        throw new RuntimeException(new StringBuilder(String.valueOf(valueOf).length() + 30).append("Failed to invoke ").append(valueOf).append(" with no args").toString(), e);
                    } catch (InvocationTargetException e2) {
                        String valueOf2 = String.valueOf(declaredConstructor);
                        throw new RuntimeException(new StringBuilder(String.valueOf(valueOf2).length() + 30).append("Failed to invoke ").append(valueOf2).append(" with no args").toString(), e2.getTargetException());
                    } catch (IllegalAccessException e3) {
                        throw new AssertionError(e3);
                    }
                }
            };
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    public String toString() {
        return this.bop.toString();
    }

    public <T> zzapx<T> zzb(zzaqo<T> zzaqo) {
        final Type bC = zzaqo.bC();
        Class<? super T> bB = zzaqo.bB();
        final zzaou<?> zzaou = this.bop.get(bC);
        if (zzaou != null) {
            return new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass1 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    return (T) zzaou.zza(bC);
                }
            };
        }
        final zzaou<?> zzaou2 = this.bop.get(bB);
        if (zzaou2 != null) {
            return new zzapx<T>() {
                /* class com.google.android.gms.internal.zzaps.AnonymousClass5 */

                @Override // com.google.android.gms.internal.zzapx
                public T bj() {
                    return (T) zzaou2.zza(bC);
                }
            };
        }
        zzapx<T> zzl = zzl(bB);
        if (zzl != null) {
            return zzl;
        }
        zzapx<T> zzc = zzc(bC, bB);
        return zzc == null ? zzd(bC, bB) : zzc;
    }
}
