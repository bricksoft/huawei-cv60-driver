package com.google.android.gms.internal;

public interface zzapl {
    <T> zzapk<T> zza(zzaos zzaos, zzaqo<T> zzaqo);
}
