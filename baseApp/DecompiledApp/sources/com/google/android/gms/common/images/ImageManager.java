package com.google.android.gms.common.images;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.images.zza;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.internal.zzsl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ImageManager {
    private static final Object Co = new Object();
    private static HashSet<Uri> Cp = new HashSet<>();
    private static ImageManager Cq;
    private static ImageManager Cr;
    private final ExecutorService Cs = Executors.newFixedThreadPool(4);
    private final zzb Ct;
    private final zzsl Cu;
    private final Map<zza, ImageReceiver> Cv;
    private final Map<Uri, ImageReceiver> Cw;
    private final Map<Uri, Long> Cx;
    private final Context mContext;
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    /* access modifiers changed from: private */
    @KeepName
    public final class ImageReceiver extends ResultReceiver {
        private final ArrayList<zza> Cy = new ArrayList<>();
        private final Uri mUri;

        ImageReceiver(Uri uri) {
            super(new Handler(Looper.getMainLooper()));
            this.mUri = uri;
        }

        public void onReceiveResult(int i, Bundle bundle) {
            ImageManager.this.Cs.execute(new zzc(this.mUri, (ParcelFileDescriptor) bundle.getParcelable("com.google.android.gms.extra.fileDescriptor")));
        }

        public void zzauv() {
            Intent intent = new Intent("com.google.android.gms.common.images.LOAD_IMAGE");
            intent.putExtra("com.google.android.gms.extras.uri", this.mUri);
            intent.putExtra("com.google.android.gms.extras.resultReceiver", this);
            intent.putExtra("com.google.android.gms.extras.priority", 3);
            ImageManager.this.mContext.sendBroadcast(intent);
        }

        public void zzb(zza zza) {
            com.google.android.gms.common.internal.zzc.zzhs("ImageReceiver.addImageRequest() must be called in the main thread");
            this.Cy.add(zza);
        }

        public void zzc(zza zza) {
            com.google.android.gms.common.internal.zzc.zzhs("ImageReceiver.removeImageRequest() must be called in the main thread");
            this.Cy.remove(zza);
        }
    }

    public interface OnImageLoadedListener {
        void onImageLoaded(Uri uri, Drawable drawable, boolean z);
    }

    /* access modifiers changed from: private */
    @TargetApi(11)
    public static final class zza {
        static int zza(ActivityManager activityManager) {
            return activityManager.getLargeMemoryClass();
        }
    }

    /* access modifiers changed from: private */
    public static final class zzb extends LruCache<zza.C0057zza, Bitmap> {
        public zzb(Context context) {
            super(zzbz(context));
        }

        @TargetApi(11)
        private static int zzbz(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            return (int) (((float) (((!((context.getApplicationInfo().flags & 1048576) != 0) || !zzs.zzayn()) ? activityManager.getMemoryClass() : zza.zza(activityManager)) * 1048576)) * 0.33f);
        }

        /* access modifiers changed from: protected */
        /* renamed from: zza */
        public int sizeOf(zza.C0057zza zza, Bitmap bitmap) {
            return bitmap.getHeight() * bitmap.getRowBytes();
        }

        /* access modifiers changed from: protected */
        /* renamed from: zza */
        public void entryRemoved(boolean z, zza.C0057zza zza, Bitmap bitmap, Bitmap bitmap2) {
            super.entryRemoved(z, zza, bitmap, bitmap2);
        }
    }

    private final class zzc implements Runnable {
        private final ParcelFileDescriptor CA;
        private final Uri mUri;

        public zzc(Uri uri, ParcelFileDescriptor parcelFileDescriptor) {
            this.mUri = uri;
            this.CA = parcelFileDescriptor;
        }

        public void run() {
            com.google.android.gms.common.internal.zzc.zzht("LoadBitmapFromDiskRunnable can't be executed in the main thread");
            boolean z = false;
            Bitmap bitmap = null;
            if (this.CA != null) {
                try {
                    bitmap = BitmapFactory.decodeFileDescriptor(this.CA.getFileDescriptor());
                } catch (OutOfMemoryError e) {
                    String valueOf = String.valueOf(this.mUri);
                    Log.e("ImageManager", new StringBuilder(String.valueOf(valueOf).length() + 34).append("OOM while loading bitmap for uri: ").append(valueOf).toString(), e);
                    z = true;
                }
                try {
                    this.CA.close();
                } catch (IOException e2) {
                    Log.e("ImageManager", "closed failed", e2);
                }
            }
            CountDownLatch countDownLatch = new CountDownLatch(1);
            ImageManager.this.mHandler.post(new zzf(this.mUri, bitmap, z, countDownLatch));
            try {
                countDownLatch.await();
            } catch (InterruptedException e3) {
                String valueOf2 = String.valueOf(this.mUri);
                Log.w("ImageManager", new StringBuilder(String.valueOf(valueOf2).length() + 32).append("Latch interrupted while posting ").append(valueOf2).toString());
            }
        }
    }

    /* access modifiers changed from: private */
    public final class zzd implements Runnable {
        private final zza CB;

        public zzd(zza zza) {
            this.CB = zza;
        }

        public void run() {
            com.google.android.gms.common.internal.zzc.zzhs("LoadImageRunnable must be executed on the main thread");
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.Cv.get(this.CB);
            if (imageReceiver != null) {
                ImageManager.this.Cv.remove(this.CB);
                imageReceiver.zzc(this.CB);
            }
            zza.C0057zza zza = this.CB.CD;
            if (zza.uri == null) {
                this.CB.zza(ImageManager.this.mContext, ImageManager.this.Cu, true);
                return;
            }
            Bitmap zza2 = ImageManager.this.zza((ImageManager) zza);
            if (zza2 != null) {
                this.CB.zza(ImageManager.this.mContext, zza2, true);
                return;
            }
            Long l = (Long) ImageManager.this.Cx.get(zza.uri);
            if (l != null) {
                if (SystemClock.elapsedRealtime() - l.longValue() < 3600000) {
                    this.CB.zza(ImageManager.this.mContext, ImageManager.this.Cu, true);
                    return;
                }
                ImageManager.this.Cx.remove(zza.uri);
            }
            this.CB.zza(ImageManager.this.mContext, ImageManager.this.Cu);
            ImageReceiver imageReceiver2 = (ImageReceiver) ImageManager.this.Cw.get(zza.uri);
            if (imageReceiver2 == null) {
                imageReceiver2 = new ImageReceiver(zza.uri);
                ImageManager.this.Cw.put(zza.uri, imageReceiver2);
            }
            imageReceiver2.zzb(this.CB);
            if (!(this.CB instanceof zza.zzc)) {
                ImageManager.this.Cv.put(this.CB, imageReceiver2);
            }
            synchronized (ImageManager.Co) {
                if (!ImageManager.Cp.contains(zza.uri)) {
                    ImageManager.Cp.add(zza.uri);
                    imageReceiver2.zzauv();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(14)
    public static final class zze implements ComponentCallbacks2 {
        private final zzb Ct;

        public zze(zzb zzb) {
            this.Ct = zzb;
        }

        public void onConfigurationChanged(Configuration configuration) {
        }

        public void onLowMemory() {
            this.Ct.evictAll();
        }

        public void onTrimMemory(int i) {
            if (i >= 60) {
                this.Ct.evictAll();
            } else if (i >= 20) {
                this.Ct.trimToSize(this.Ct.size() / 2);
            }
        }
    }

    private final class zzf implements Runnable {
        private boolean CC;
        private final Bitmap mBitmap;
        private final Uri mUri;
        private final CountDownLatch zzank;

        public zzf(Uri uri, Bitmap bitmap, boolean z, CountDownLatch countDownLatch) {
            this.mUri = uri;
            this.mBitmap = bitmap;
            this.CC = z;
            this.zzank = countDownLatch;
        }

        private void zza(ImageReceiver imageReceiver, boolean z) {
            ArrayList arrayList = imageReceiver.Cy;
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                zza zza = (zza) arrayList.get(i);
                if (z) {
                    zza.zza(ImageManager.this.mContext, this.mBitmap, false);
                } else {
                    ImageManager.this.Cx.put(this.mUri, Long.valueOf(SystemClock.elapsedRealtime()));
                    zza.zza(ImageManager.this.mContext, ImageManager.this.Cu, false);
                }
                if (!(zza instanceof zza.zzc)) {
                    ImageManager.this.Cv.remove(zza);
                }
            }
        }

        public void run() {
            com.google.android.gms.common.internal.zzc.zzhs("OnBitmapLoadedRunnable must be executed in the main thread");
            boolean z = this.mBitmap != null;
            if (ImageManager.this.Ct != null) {
                if (this.CC) {
                    ImageManager.this.Ct.evictAll();
                    System.gc();
                    this.CC = false;
                    ImageManager.this.mHandler.post(this);
                    return;
                } else if (z) {
                    ImageManager.this.Ct.put(new zza.C0057zza(this.mUri), this.mBitmap);
                }
            }
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.Cw.remove(this.mUri);
            if (imageReceiver != null) {
                zza(imageReceiver, z);
            }
            this.zzank.countDown();
            synchronized (ImageManager.Co) {
                ImageManager.Cp.remove(this.mUri);
            }
        }
    }

    private ImageManager(Context context, boolean z) {
        this.mContext = context.getApplicationContext();
        if (z) {
            this.Ct = new zzb(this.mContext);
            if (zzs.zzayq()) {
                zzaut();
            }
        } else {
            this.Ct = null;
        }
        this.Cu = new zzsl();
        this.Cv = new HashMap();
        this.Cw = new HashMap();
        this.Cx = new HashMap();
    }

    public static ImageManager create(Context context) {
        return zzg(context, false);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private Bitmap zza(zza.C0057zza zza2) {
        if (this.Ct == null) {
            return null;
        }
        return (Bitmap) this.Ct.get(zza2);
    }

    @TargetApi(14)
    private void zzaut() {
        this.mContext.registerComponentCallbacks(new zze(this.Ct));
    }

    public static ImageManager zzg(Context context, boolean z) {
        if (z) {
            if (Cr == null) {
                Cr = new ImageManager(context, true);
            }
            return Cr;
        }
        if (Cq == null) {
            Cq = new ImageManager(context, false);
        }
        return Cq;
    }

    public void loadImage(ImageView imageView, int i) {
        zza(new zza.zzb(imageView, i));
    }

    public void loadImage(ImageView imageView, Uri uri) {
        zza(new zza.zzb(imageView, uri));
    }

    public void loadImage(ImageView imageView, Uri uri, int i) {
        zza.zzb zzb2 = new zza.zzb(imageView, uri);
        zzb2.zzgg(i);
        zza(zzb2);
    }

    public void loadImage(OnImageLoadedListener onImageLoadedListener, Uri uri) {
        zza(new zza.zzc(onImageLoadedListener, uri));
    }

    public void loadImage(OnImageLoadedListener onImageLoadedListener, Uri uri, int i) {
        zza.zzc zzc2 = new zza.zzc(onImageLoadedListener, uri);
        zzc2.zzgg(i);
        zza(zzc2);
    }

    public void zza(zza zza2) {
        com.google.android.gms.common.internal.zzc.zzhs("ImageManager.loadImage() must be called in the main thread");
        new zzd(zza2).run();
    }
}
