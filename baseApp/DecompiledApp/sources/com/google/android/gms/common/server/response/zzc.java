package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.common.server.response.FieldMappingDictionary;
import java.util.ArrayList;

public class zzc implements Parcelable.Creator<FieldMappingDictionary> {
    static void zza(FieldMappingDictionary fieldMappingDictionary, Parcel parcel, int i) {
        int zzcs = zzb.zzcs(parcel);
        zzb.zzc(parcel, 1, fieldMappingDictionary.mVersionCode);
        zzb.zzc(parcel, 2, fieldMappingDictionary.zzaxm(), false);
        zzb.zza(parcel, 3, fieldMappingDictionary.zzaxn(), false);
        zzb.zzaj(parcel, zzcs);
    }

    /* renamed from: zzcz */
    public FieldMappingDictionary createFromParcel(Parcel parcel) {
        int zzcr = zza.zzcr(parcel);
        int i = 0;
        String str = null;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < zzcr) {
            int zzcq = zza.zzcq(parcel);
            switch (zza.zzgu(zzcq)) {
                case 1:
                    i = zza.zzg(parcel, zzcq);
                    break;
                case 2:
                    arrayList = zza.zzc(parcel, zzcq, FieldMappingDictionary.Entry.CREATOR);
                    break;
                case 3:
                    str = zza.zzq(parcel, zzcq);
                    break;
                default:
                    zza.zzb(parcel, zzcq);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcr) {
            return new FieldMappingDictionary(i, arrayList, str);
        }
        throw new zza.C0058zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcr).toString(), parcel);
    }

    /* renamed from: zzhc */
    public FieldMappingDictionary[] newArray(int i) {
        return new FieldMappingDictionary[i];
    }
}
