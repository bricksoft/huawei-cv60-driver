package com.google.android.gms.common.data;

import java.util.NoSuchElementException;

public class zzg<T> extends zzb<T> {
    private T Cn;

    public zzg(DataBuffer<T> dataBuffer) {
        super(dataBuffer);
    }

    @Override // java.util.Iterator, com.google.android.gms.common.data.zzb
    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException(new StringBuilder(46).append("Cannot advance the iterator beyond ").append(this.BS).toString());
        }
        this.BS++;
        if (this.BS == 0) {
            this.Cn = (T) this.BR.get(0);
            if (!(this.Cn instanceof zzc)) {
                String valueOf = String.valueOf(this.Cn.getClass());
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 44).append("DataBuffer reference of type ").append(valueOf).append(" is not movable").toString());
            }
        } else {
            this.Cn.zzfy(this.BS);
        }
        return this.Cn;
    }
}
