package com.google.android.gms.common.internal;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.google.android.exoplayer.hls.HlsChunkSource;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/* access modifiers changed from: package-private */
public final class zzm extends zzl implements Handler.Callback {
    private final HashMap<zza, zzb> En = new HashMap<>();
    private final com.google.android.gms.common.stats.zza Eo;
    private final long Ep;
    private final Handler mHandler;
    private final Context zzatc;

    /* access modifiers changed from: private */
    public static final class zza {
        private final String Eq;
        private final ComponentName Er;
        private final String cd;

        public zza(ComponentName componentName) {
            this.cd = null;
            this.Eq = null;
            this.Er = (ComponentName) zzaa.zzy(componentName);
        }

        public zza(String str, String str2) {
            this.cd = zzaa.zzib(str);
            this.Eq = zzaa.zzib(str2);
            this.Er = null;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            return zzz.equal(this.cd, zza.cd) && zzz.equal(this.Er, zza.Er);
        }

        public int hashCode() {
            return zzz.hashCode(this.cd, this.Er);
        }

        public String toString() {
            return this.cd == null ? this.Er.flattenToString() : this.cd;
        }

        public Intent zzawe() {
            return this.cd != null ? new Intent(this.cd).setPackage(this.Eq) : new Intent().setComponent(this.Er);
        }
    }

    /* access modifiers changed from: private */
    public final class zzb {
        private IBinder DI;
        private ComponentName Er;
        private final zza Es = new zza();
        private final Set<ServiceConnection> Et = new HashSet();
        private boolean Eu;
        private final zza Ev;
        private int mState = 2;

        public class zza implements ServiceConnection {
            public zza() {
            }

            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                synchronized (zzm.this.En) {
                    zzb.this.DI = iBinder;
                    zzb.this.Er = componentName;
                    for (ServiceConnection serviceConnection : zzb.this.Et) {
                        serviceConnection.onServiceConnected(componentName, iBinder);
                    }
                    zzb.this.mState = 1;
                }
            }

            public void onServiceDisconnected(ComponentName componentName) {
                synchronized (zzm.this.En) {
                    zzb.this.DI = null;
                    zzb.this.Er = componentName;
                    for (ServiceConnection serviceConnection : zzb.this.Et) {
                        serviceConnection.onServiceDisconnected(componentName);
                    }
                    zzb.this.mState = 2;
                }
            }
        }

        public zzb(zza zza2) {
            this.Ev = zza2;
        }

        public IBinder getBinder() {
            return this.DI;
        }

        public ComponentName getComponentName() {
            return this.Er;
        }

        public int getState() {
            return this.mState;
        }

        public boolean isBound() {
            return this.Eu;
        }

        public void zza(ServiceConnection serviceConnection, String str) {
            zzm.this.Eo.zza(zzm.this.zzatc, serviceConnection, str, this.Ev.zzawe());
            this.Et.add(serviceConnection);
        }

        public boolean zza(ServiceConnection serviceConnection) {
            return this.Et.contains(serviceConnection);
        }

        public boolean zzawf() {
            return this.Et.isEmpty();
        }

        public void zzb(ServiceConnection serviceConnection, String str) {
            zzm.this.Eo.zzb(zzm.this.zzatc, serviceConnection);
            this.Et.remove(serviceConnection);
        }

        @TargetApi(14)
        public void zzhw(String str) {
            this.mState = 3;
            this.Eu = zzm.this.Eo.zza(zzm.this.zzatc, str, this.Ev.zzawe(), this.Es, 129);
            if (!this.Eu) {
                this.mState = 2;
                try {
                    zzm.this.Eo.zza(zzm.this.zzatc, this.Es);
                } catch (IllegalArgumentException e) {
                }
            }
        }

        public void zzhx(String str) {
            zzm.this.Eo.zza(zzm.this.zzatc, this.Es);
            this.Eu = false;
            this.mState = 2;
        }
    }

    zzm(Context context) {
        this.zzatc = context.getApplicationContext();
        this.mHandler = new Handler(context.getMainLooper(), this);
        this.Eo = com.google.android.gms.common.stats.zza.zzaxr();
        this.Ep = HlsChunkSource.DEFAULT_MIN_BUFFER_TO_SWITCH_UP_MS;
    }

    private boolean zza(zza zza2, ServiceConnection serviceConnection, String str) {
        boolean isBound;
        zzaa.zzb(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.En) {
            zzb zzb2 = this.En.get(zza2);
            if (zzb2 != null) {
                this.mHandler.removeMessages(0, zza2);
                if (!zzb2.zza(serviceConnection)) {
                    zzb2.zza(serviceConnection, str);
                    switch (zzb2.getState()) {
                        case 1:
                            serviceConnection.onServiceConnected(zzb2.getComponentName(), zzb2.getBinder());
                            break;
                        case 2:
                            zzb2.zzhw(str);
                            break;
                    }
                } else {
                    String valueOf = String.valueOf(zza2);
                    throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 81).append("Trying to bind a GmsServiceConnection that was already connected before.  config=").append(valueOf).toString());
                }
            } else {
                zzb2 = new zzb(zza2);
                zzb2.zza(serviceConnection, str);
                zzb2.zzhw(str);
                this.En.put(zza2, zzb2);
            }
            isBound = zzb2.isBound();
        }
        return isBound;
    }

    private void zzb(zza zza2, ServiceConnection serviceConnection, String str) {
        zzaa.zzb(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.En) {
            zzb zzb2 = this.En.get(zza2);
            if (zzb2 == null) {
                String valueOf = String.valueOf(zza2);
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 50).append("Nonexistent connection status for service config: ").append(valueOf).toString());
            } else if (!zzb2.zza(serviceConnection)) {
                String valueOf2 = String.valueOf(zza2);
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf2).length() + 76).append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=").append(valueOf2).toString());
            } else {
                zzb2.zzb(serviceConnection, str);
                if (zzb2.zzawf()) {
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0, zza2), this.Ep);
                }
            }
        }
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                zza zza2 = (zza) message.obj;
                synchronized (this.En) {
                    zzb zzb2 = this.En.get(zza2);
                    if (zzb2 != null && zzb2.zzawf()) {
                        if (zzb2.isBound()) {
                            zzb2.zzhx("GmsClientSupervisor");
                        }
                        this.En.remove(zza2);
                    }
                }
                return true;
            default:
                return false;
        }
    }

    @Override // com.google.android.gms.common.internal.zzl
    public boolean zza(ComponentName componentName, ServiceConnection serviceConnection, String str) {
        return zza(new zza(componentName), serviceConnection, str);
    }

    @Override // com.google.android.gms.common.internal.zzl
    public boolean zza(String str, String str2, ServiceConnection serviceConnection, String str3) {
        return zza(new zza(str, str2), serviceConnection, str3);
    }

    @Override // com.google.android.gms.common.internal.zzl
    public void zzb(ComponentName componentName, ServiceConnection serviceConnection, String str) {
        zzb(new zza(componentName), serviceConnection, str);
    }

    @Override // com.google.android.gms.common.internal.zzl
    public void zzb(String str, String str2, ServiceConnection serviceConnection, String str3) {
        zzb(new zza(str, str2), serviceConnection, str3);
    }
}
