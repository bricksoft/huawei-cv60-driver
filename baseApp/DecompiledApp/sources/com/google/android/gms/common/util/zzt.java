package com.google.android.gms.common.util;

import android.os.Process;
import android.os.StrictMode;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class zzt {
    private static String GE = null;
    private static final int GF = Process.myPid();

    public static String zzayz() {
        if (GE == null) {
            GE = zzhi(GF);
        }
        return GE;
    }

    /* JADX INFO: finally extract failed */
    static String zzhi(int i) {
        BufferedReader bufferedReader;
        Throwable th;
        BufferedReader bufferedReader2;
        String str = null;
        if (i > 0) {
            try {
                StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                try {
                    bufferedReader2 = new BufferedReader(new FileReader(new StringBuilder(25).append("/proc/").append(i).append("/cmdline").toString()));
                    try {
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        str = bufferedReader2.readLine().trim();
                        zzo.zzb(bufferedReader2);
                    } catch (IOException e) {
                        zzo.zzb(bufferedReader2);
                        return str;
                    } catch (Throwable th2) {
                        th = th2;
                        bufferedReader = bufferedReader2;
                        zzo.zzb(bufferedReader);
                        throw th;
                    }
                } catch (Throwable th3) {
                    StrictMode.setThreadPolicy(allowThreadDiskReads);
                    throw th3;
                }
            } catch (IOException e2) {
                bufferedReader2 = null;
                zzo.zzb(bufferedReader2);
                return str;
            } catch (Throwable th4) {
                th = th4;
                bufferedReader = null;
                zzo.zzb(bufferedReader);
                throw th;
            }
        }
        return str;
    }
}
