package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;

@KeepName
public final class BinderWrapper implements Parcelable {
    public static final Parcelable.Creator<BinderWrapper> CREATOR = new Parcelable.Creator<BinderWrapper>() {
        /* class com.google.android.gms.common.internal.BinderWrapper.AnonymousClass1 */

        /* renamed from: zzck */
        public BinderWrapper createFromParcel(Parcel parcel) {
            return new BinderWrapper(parcel);
        }

        /* renamed from: zzgl */
        public BinderWrapper[] newArray(int i) {
            return new BinderWrapper[i];
        }
    };
    private IBinder DI;

    public BinderWrapper() {
        this.DI = null;
    }

    public BinderWrapper(IBinder iBinder) {
        this.DI = null;
        this.DI = iBinder;
    }

    private BinderWrapper(Parcel parcel) {
        this.DI = null;
        this.DI = parcel.readStrongBinder();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStrongBinder(this.DI);
    }
}
