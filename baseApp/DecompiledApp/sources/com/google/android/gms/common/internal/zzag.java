package com.google.android.gms.common.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

public class zzag<T extends IInterface> extends zzj<T> {
    private final Api.zzg<T> EO;

    public zzag(Context context, Looper looper, int i, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener, zzf zzf, Api.zzg<T> zzg) {
        super(context, looper, i, zzf, connectionCallbacks, onConnectionFailedListener);
        this.EO = zzg;
    }

    public Api.zzg<T> zzawt() {
        return this.EO;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public void zzc(int i, T t) {
        this.EO.zza(i, t);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public T zzh(IBinder iBinder) {
        return this.EO.zzh(iBinder);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjx() {
        return this.EO.zzjx();
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.internal.zze
    public String zzjy() {
        return this.EO.zzjy();
    }
}
