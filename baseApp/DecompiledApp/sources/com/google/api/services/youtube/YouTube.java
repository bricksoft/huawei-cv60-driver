package com.google.api.services.youtube;

import com.google.api.client.googleapis.GoogleUtils;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.HttpMethods;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.Data;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.Key;
import com.google.api.client.util.Preconditions;
import com.google.api.services.youtube.model.Activity;
import com.google.api.services.youtube.model.ActivityListResponse;
import com.google.api.services.youtube.model.Caption;
import com.google.api.services.youtube.model.CaptionListResponse;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelBannerResource;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.ChannelSection;
import com.google.api.services.youtube.model.ChannelSectionListResponse;
import com.google.api.services.youtube.model.Comment;
import com.google.api.services.youtube.model.CommentListResponse;
import com.google.api.services.youtube.model.CommentThread;
import com.google.api.services.youtube.model.CommentThreadListResponse;
import com.google.api.services.youtube.model.FanFundingEventListResponse;
import com.google.api.services.youtube.model.GuideCategoryListResponse;
import com.google.api.services.youtube.model.I18nLanguageListResponse;
import com.google.api.services.youtube.model.I18nRegionListResponse;
import com.google.api.services.youtube.model.InvideoBranding;
import com.google.api.services.youtube.model.LiveBroadcast;
import com.google.api.services.youtube.model.LiveBroadcastListResponse;
import com.google.api.services.youtube.model.LiveChatBan;
import com.google.api.services.youtube.model.LiveChatMessage;
import com.google.api.services.youtube.model.LiveChatMessageListResponse;
import com.google.api.services.youtube.model.LiveChatModerator;
import com.google.api.services.youtube.model.LiveChatModeratorListResponse;
import com.google.api.services.youtube.model.LiveStream;
import com.google.api.services.youtube.model.LiveStreamListResponse;
import com.google.api.services.youtube.model.Playlist;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.api.services.youtube.model.PlaylistListResponse;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SponsorListResponse;
import com.google.api.services.youtube.model.Subscription;
import com.google.api.services.youtube.model.SubscriptionListResponse;
import com.google.api.services.youtube.model.ThumbnailSetResponse;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoAbuseReport;
import com.google.api.services.youtube.model.VideoAbuseReportReasonListResponse;
import com.google.api.services.youtube.model.VideoCategoryListResponse;
import com.google.api.services.youtube.model.VideoGetRatingResponse;
import com.google.api.services.youtube.model.VideoListResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;

public class YouTube extends AbstractGoogleJsonClient {
    public static final String DEFAULT_BASE_URL = "https://www.googleapis.com/youtube/v3/";
    public static final String DEFAULT_ROOT_URL = "https://www.googleapis.com/";
    public static final String DEFAULT_SERVICE_PATH = "youtube/v3/";

    static {
        Preconditions.checkState(GoogleUtils.MAJOR_VERSION.intValue() == 1 && GoogleUtils.MINOR_VERSION.intValue() >= 15, "You are currently running with version %s of google-api-client. You need at least version 1.15 of google-api-client to run version 1.22.0 of the YouTube Data API library.", GoogleUtils.VERSION);
    }

    public YouTube(HttpTransport httpTransport, JsonFactory jsonFactory, HttpRequestInitializer httpRequestInitializer) {
        this(new Builder(httpTransport, jsonFactory, httpRequestInitializer));
    }

    YouTube(Builder builder) {
        super(builder);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.api.client.googleapis.services.AbstractGoogleClient
    public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) {
        super.initialize(abstractGoogleClientRequest);
    }

    public Activities activities() {
        return new Activities();
    }

    public class Activities {
        public Activities() {
        }

        public Insert insert(String str, Activity activity) {
            Insert insert = new Insert(str, activity);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<Activity> {
            private static final String REST_PATH = "activities";
            @Key
            private String part;

            protected Insert(String str, Activity activity) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, activity, Activity.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Activity> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Activity> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Activity> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Activity> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Activity> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Activity> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Activity> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Activity> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<ActivityListResponse> {
            private static final String REST_PATH = "activities";
            @Key
            private String channelId;
            @Key
            private Boolean home;
            @Key
            private Long maxResults;
            @Key
            private Boolean mine;
            @Key
            private String pageToken;
            @Key
            private String part;
            @Key
            private DateTime publishedAfter;
            @Key
            private DateTime publishedBefore;
            @Key
            private String regionCode;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, ActivityListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ActivityListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ActivityListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ActivityListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ActivityListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ActivityListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ActivityListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ActivityListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getChannelId() {
                return this.channelId;
            }

            public List setChannelId(String str) {
                this.channelId = str;
                return this;
            }

            public Boolean getHome() {
                return this.home;
            }

            public List setHome(Boolean bool) {
                this.home = bool;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public Boolean getMine() {
                return this.mine;
            }

            public List setMine(Boolean bool) {
                this.mine = bool;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            public DateTime getPublishedAfter() {
                return this.publishedAfter;
            }

            public List setPublishedAfter(DateTime dateTime) {
                this.publishedAfter = dateTime;
                return this;
            }

            public DateTime getPublishedBefore() {
                return this.publishedBefore;
            }

            public List setPublishedBefore(DateTime dateTime) {
                this.publishedBefore = dateTime;
                return this;
            }

            public String getRegionCode() {
                return this.regionCode;
            }

            public List setRegionCode(String str) {
                this.regionCode = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Activities$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<ActivityListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public Captions captions() {
        return new Captions();
    }

    public class Captions {
        public Captions() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "captions";
            @Key
            private String id;
            @Key
            private String onBehalfOf;
            @Key
            private String onBehalfOfContentOwner;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            public String getOnBehalfOf() {
                return this.onBehalfOf;
            }

            public Delete setOnBehalfOf(String str) {
                this.onBehalfOf = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Delete setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Download download(String str) {
            Download download = new Download(str);
            YouTube.this.initialize(download);
            return download;
        }

        public class Download extends YouTubeRequest<Void> {
            private static final String REST_PATH = "captions/{id}";
            @Key
            private String id;
            @Key
            private String onBehalfOf;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String tfmt;
            @Key
            private String tlang;

            protected Download(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
                initializeMediaDownload();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public void executeMediaAndDownloadTo(OutputStream outputStream) {
                super.executeMediaAndDownloadTo(outputStream);
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public InputStream executeMediaAsInputStream() {
                return super.executeMediaAsInputStream();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeMedia() {
                return super.executeMedia();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Download' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Download) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Download' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Download) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Download' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Download) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Download' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Download) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Download' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Download) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Download' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Download) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Download' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Download) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Download setId(String str) {
                this.id = str;
                return this;
            }

            public String getOnBehalfOf() {
                return this.onBehalfOf;
            }

            public Download setOnBehalfOf(String str) {
                this.onBehalfOf = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Download setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getTfmt() {
                return this.tfmt;
            }

            public Download setTfmt(String str) {
                this.tfmt = str;
                return this;
            }

            public String getTlang() {
                return this.tlang;
            }

            public Download setTlang(String str) {
                this.tlang = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Download' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Download) super.set(str, obj);
            }
        }

        public Insert insert(String str, Caption caption) {
            Insert insert = new Insert(str, caption);
            YouTube.this.initialize(insert);
            return insert;
        }

        public Insert insert(String str, Caption caption, AbstractInputStreamContent abstractInputStreamContent) {
            Insert insert = new Insert(str, caption, abstractInputStreamContent);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<Caption> {
            private static final String REST_PATH = "captions";
            @Key
            private String onBehalfOf;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String part;
            @Key
            private Boolean sync;

            protected Insert(String str, Caption caption) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, caption, Caption.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            protected Insert(String str, Caption caption, AbstractInputStreamContent abstractInputStreamContent) {
                super(YouTube.this, HttpMethods.POST, "/upload/" + YouTube.this.getServicePath() + REST_PATH, caption, Caption.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
                initializeMediaUpload(abstractInputStreamContent);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOf() {
                return this.onBehalfOf;
            }

            public Insert setOnBehalfOf(String str) {
                this.onBehalfOf = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Insert setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public Boolean getSync() {
                return this.sync;
            }

            public Insert setSync(Boolean bool) {
                this.sync = bool;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Caption> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str, String str2) {
            List list = new List(str, str2);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<CaptionListResponse> {
            private static final String REST_PATH = "captions";
            @Key
            private String id;
            @Key
            private String onBehalfOf;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String part;
            @Key
            private String videoId;

            protected List(String str, String str2) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, CaptionListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
                this.videoId = (String) Preconditions.checkNotNull(str2, "Required parameter videoId must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CaptionListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CaptionListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CaptionListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CaptionListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CaptionListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CaptionListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CaptionListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getVideoId() {
                return this.videoId;
            }

            public List setVideoId(String str) {
                this.videoId = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public String getOnBehalfOf() {
                return this.onBehalfOf;
            }

            public List setOnBehalfOf(String str) {
                this.onBehalfOf = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<CaptionListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public Update update(String str, Caption caption) {
            Update update = new Update(str, caption);
            YouTube.this.initialize(update);
            return update;
        }

        public Update update(String str, Caption caption, AbstractInputStreamContent abstractInputStreamContent) {
            Update update = new Update(str, caption, abstractInputStreamContent);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<Caption> {
            private static final String REST_PATH = "captions";
            @Key
            private String onBehalfOf;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String part;
            @Key
            private Boolean sync;

            protected Update(String str, Caption caption) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, caption, Caption.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
                checkRequiredParameter(caption, "content");
                checkRequiredParameter(caption.getId(), "Caption.getId()");
            }

            protected Update(String str, Caption caption, AbstractInputStreamContent abstractInputStreamContent) {
                super(YouTube.this, HttpMethods.PUT, "/upload/" + YouTube.this.getServicePath() + REST_PATH, caption, Caption.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
                initializeMediaUpload(abstractInputStreamContent);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Caption> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOf() {
                return this.onBehalfOf;
            }

            public Update setOnBehalfOf(String str) {
                this.onBehalfOf = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Update setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public Boolean getSync() {
                return this.sync;
            }

            public Update setSync(Boolean bool) {
                this.sync = bool;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Captions$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Caption> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public ChannelBanners channelBanners() {
        return new ChannelBanners();
    }

    public class ChannelBanners {
        public ChannelBanners() {
        }

        public Insert insert(ChannelBannerResource channelBannerResource) {
            Insert insert = new Insert(channelBannerResource);
            YouTube.this.initialize(insert);
            return insert;
        }

        public Insert insert(ChannelBannerResource channelBannerResource, AbstractInputStreamContent abstractInputStreamContent) {
            Insert insert = new Insert(channelBannerResource, abstractInputStreamContent);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<ChannelBannerResource> {
            private static final String REST_PATH = "channelBanners/insert";
            @Key
            private String onBehalfOfContentOwner;

            protected Insert(ChannelBannerResource channelBannerResource) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, channelBannerResource, ChannelBannerResource.class);
            }

            protected Insert(ChannelBannerResource channelBannerResource, AbstractInputStreamContent abstractInputStreamContent) {
                super(YouTube.this, HttpMethods.POST, "/upload/" + YouTube.this.getServicePath() + REST_PATH, channelBannerResource, ChannelBannerResource.class);
                initializeMediaUpload(abstractInputStreamContent);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelBanners$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelBannerResource> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelBanners$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelBannerResource> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelBanners$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelBannerResource> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelBanners$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelBannerResource> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelBanners$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelBannerResource> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelBanners$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelBannerResource> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelBanners$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelBannerResource> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Insert setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelBanners$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<ChannelBannerResource> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }
    }

    public ChannelSections channelSections() {
        return new ChannelSections();
    }

    public class ChannelSections {
        public ChannelSections() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "channelSections";
            @Key
            private String id;
            @Key
            private String onBehalfOfContentOwner;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Delete setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, ChannelSection channelSection) {
            Insert insert = new Insert(str, channelSection);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<ChannelSection> {
            private static final String REST_PATH = "channelSections";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;

            protected Insert(String str, ChannelSection channelSection) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, channelSection, ChannelSection.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Insert setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Insert setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<ChannelSection> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<ChannelSectionListResponse> {
            private static final String REST_PATH = "channelSections";
            @Key
            private String channelId;
            @Key
            private String hl;
            @Key
            private String id;
            @Key
            private Boolean mine;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, ChannelSectionListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSectionListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSectionListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSectionListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSectionListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSectionListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSectionListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSectionListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getChannelId() {
                return this.channelId;
            }

            public List setChannelId(String str) {
                this.channelId = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public Boolean getMine() {
                return this.mine;
            }

            public List setMine(Boolean bool) {
                this.mine = bool;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<ChannelSectionListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public Update update(String str, ChannelSection channelSection) {
            Update update = new Update(str, channelSection);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<ChannelSection> {
            private static final String REST_PATH = "channelSections";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String part;

            protected Update(String str, ChannelSection channelSection) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, channelSection, ChannelSection.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelSection> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Update setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$ChannelSections$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<ChannelSection> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public Channels channels() {
        return new Channels();
    }

    public class Channels {
        public Channels() {
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<ChannelListResponse> {
            private static final String REST_PATH = "channels";
            @Key
            private String categoryId;
            @Key
            private String forUsername;
            @Key
            private String hl;
            @Key
            private String id;
            @Key
            private Boolean managedByMe;
            @Key
            private Long maxResults;
            @Key
            private Boolean mine;
            @Key
            private Boolean mySubscribers;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String pageToken;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, ChannelListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ChannelListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getCategoryId() {
                return this.categoryId;
            }

            public List setCategoryId(String str) {
                this.categoryId = str;
                return this;
            }

            public String getForUsername() {
                return this.forUsername;
            }

            public List setForUsername(String str) {
                this.forUsername = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public Boolean getManagedByMe() {
                return this.managedByMe;
            }

            public List setManagedByMe(Boolean bool) {
                this.managedByMe = bool;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public Boolean getMine() {
                return this.mine;
            }

            public List setMine(Boolean bool) {
                this.mine = bool;
                return this;
            }

            public Boolean getMySubscribers() {
                return this.mySubscribers;
            }

            public List setMySubscribers(Boolean bool) {
                this.mySubscribers = bool;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<ChannelListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public Update update(String str, Channel channel) {
            Update update = new Update(str, channel);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<Channel> {
            private static final String REST_PATH = "channels";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String part;

            protected Update(String str, Channel channel) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, channel, Channel.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Channel> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Channel> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Channel> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Channel> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Channel> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Channel> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Channel> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Update setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Channels$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Channel> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public CommentThreads commentThreads() {
        return new CommentThreads();
    }

    public class CommentThreads {
        public CommentThreads() {
        }

        public Insert insert(String str, CommentThread commentThread) {
            Insert insert = new Insert(str, commentThread);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<CommentThread> {
            private static final String REST_PATH = "commentThreads";
            @Key
            private String part;

            protected Insert(String str, CommentThread commentThread) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, commentThread, CommentThread.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<CommentThread> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<CommentThreadListResponse> {
            private static final String REST_PATH = "commentThreads";
            @Key
            private String allThreadsRelatedToChannelId;
            @Key
            private String channelId;
            @Key
            private String id;
            @Key
            private Long maxResults;
            @Key
            private String moderationStatus;
            @Key
            private String order;
            @Key
            private String pageToken;
            @Key
            private String part;
            @Key
            private String searchTerms;
            @Key
            private String textFormat;
            @Key
            private String videoId;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, CommentThreadListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThreadListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThreadListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThreadListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThreadListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThreadListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThreadListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThreadListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getAllThreadsRelatedToChannelId() {
                return this.allThreadsRelatedToChannelId;
            }

            public List setAllThreadsRelatedToChannelId(String str) {
                this.allThreadsRelatedToChannelId = str;
                return this;
            }

            public String getChannelId() {
                return this.channelId;
            }

            public List setChannelId(String str) {
                this.channelId = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public String getModerationStatus() {
                return this.moderationStatus;
            }

            public List setModerationStatus(String str) {
                this.moderationStatus = str;
                return this;
            }

            public String getOrder() {
                return this.order;
            }

            public List setOrder(String str) {
                this.order = str;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            public String getSearchTerms() {
                return this.searchTerms;
            }

            public List setSearchTerms(String str) {
                this.searchTerms = str;
                return this;
            }

            public String getTextFormat() {
                return this.textFormat;
            }

            public List setTextFormat(String str) {
                this.textFormat = str;
                return this;
            }

            public String getVideoId() {
                return this.videoId;
            }

            public List setVideoId(String str) {
                this.videoId = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<CommentThreadListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public Update update(String str, CommentThread commentThread) {
            Update update = new Update(str, commentThread);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<CommentThread> {
            private static final String REST_PATH = "commentThreads";
            @Key
            private String part;

            protected Update(String str, CommentThread commentThread) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, commentThread, CommentThread.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentThread> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$CommentThreads$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<CommentThread> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public Comments comments() {
        return new Comments();
    }

    public class Comments {
        public Comments() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "comments";
            @Key
            private String id;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, Comment comment) {
            Insert insert = new Insert(str, comment);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<Comment> {
            private static final String REST_PATH = "comments";
            @Key
            private String part;

            protected Insert(String str, Comment comment) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, comment, Comment.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Comment> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<CommentListResponse> {
            private static final String REST_PATH = "comments";
            @Key
            private String id;
            @Key
            private Long maxResults;
            @Key
            private String pageToken;
            @Key
            private String parentId;
            @Key
            private String part;
            @Key
            private String textFormat;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, CommentListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<CommentListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            public String getParentId() {
                return this.parentId;
            }

            public List setParentId(String str) {
                this.parentId = str;
                return this;
            }

            public String getTextFormat() {
                return this.textFormat;
            }

            public List setTextFormat(String str) {
                this.textFormat = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<CommentListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public MarkAsSpam markAsSpam(String str) {
            MarkAsSpam markAsSpam = new MarkAsSpam(str);
            YouTube.this.initialize(markAsSpam);
            return markAsSpam;
        }

        public class MarkAsSpam extends YouTubeRequest<Void> {
            private static final String REST_PATH = "comments/markAsSpam";
            @Key
            private String id;

            protected MarkAsSpam(String str) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$MarkAsSpam' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (MarkAsSpam) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$MarkAsSpam' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (MarkAsSpam) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$MarkAsSpam' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (MarkAsSpam) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$MarkAsSpam' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (MarkAsSpam) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$MarkAsSpam' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (MarkAsSpam) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$MarkAsSpam' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (MarkAsSpam) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$MarkAsSpam' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (MarkAsSpam) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public MarkAsSpam setId(String str) {
                this.id = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$MarkAsSpam' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (MarkAsSpam) super.set(str, obj);
            }
        }

        public SetModerationStatus setModerationStatus(String str, String str2) {
            SetModerationStatus setModerationStatus = new SetModerationStatus(str, str2);
            YouTube.this.initialize(setModerationStatus);
            return setModerationStatus;
        }

        public class SetModerationStatus extends YouTubeRequest<Void> {
            private static final String REST_PATH = "comments/setModerationStatus";
            @Key
            private Boolean banAuthor;
            @Key
            private String id;
            @Key
            private String moderationStatus;

            protected SetModerationStatus(String str, String str2) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
                this.moderationStatus = (String) Preconditions.checkNotNull(str2, "Required parameter moderationStatus must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$SetModerationStatus' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (SetModerationStatus) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$SetModerationStatus' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (SetModerationStatus) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$SetModerationStatus' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (SetModerationStatus) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$SetModerationStatus' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (SetModerationStatus) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$SetModerationStatus' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (SetModerationStatus) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$SetModerationStatus' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (SetModerationStatus) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$SetModerationStatus' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (SetModerationStatus) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public SetModerationStatus setId(String str) {
                this.id = str;
                return this;
            }

            public String getModerationStatus() {
                return this.moderationStatus;
            }

            public SetModerationStatus setModerationStatus(String str) {
                this.moderationStatus = str;
                return this;
            }

            public Boolean getBanAuthor() {
                return this.banAuthor;
            }

            public SetModerationStatus setBanAuthor(Boolean bool) {
                this.banAuthor = bool;
                return this;
            }

            public boolean isBanAuthor() {
                if (this.banAuthor == null || this.banAuthor == Data.NULL_BOOLEAN) {
                    return false;
                }
                return this.banAuthor.booleanValue();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$SetModerationStatus' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (SetModerationStatus) super.set(str, obj);
            }
        }

        public Update update(String str, Comment comment) {
            Update update = new Update(str, comment);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<Comment> {
            private static final String REST_PATH = "comments";
            @Key
            private String part;

            protected Update(String str, Comment comment) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, comment, Comment.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Comment> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Comments$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Comment> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public FanFundingEvents fanFundingEvents() {
        return new FanFundingEvents();
    }

    public class FanFundingEvents {
        public FanFundingEvents() {
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<FanFundingEventListResponse> {
            private static final String REST_PATH = "fanFundingEvents";
            @Key
            private String hl;
            @Key
            private Long maxResults;
            @Key
            private String pageToken;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, FanFundingEventListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$FanFundingEvents$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<FanFundingEventListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$FanFundingEvents$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<FanFundingEventListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$FanFundingEvents$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<FanFundingEventListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$FanFundingEvents$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<FanFundingEventListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$FanFundingEvents$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<FanFundingEventListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$FanFundingEvents$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<FanFundingEventListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$FanFundingEvents$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<FanFundingEventListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$FanFundingEvents$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<FanFundingEventListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public GuideCategories guideCategories() {
        return new GuideCategories();
    }

    public class GuideCategories {
        public GuideCategories() {
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<GuideCategoryListResponse> {
            private static final String REST_PATH = "guideCategories";
            @Key
            private String hl;
            @Key
            private String id;
            @Key
            private String part;
            @Key
            private String regionCode;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, GuideCategoryListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$GuideCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<GuideCategoryListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$GuideCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<GuideCategoryListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$GuideCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<GuideCategoryListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$GuideCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<GuideCategoryListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$GuideCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<GuideCategoryListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$GuideCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<GuideCategoryListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$GuideCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<GuideCategoryListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public String getRegionCode() {
                return this.regionCode;
            }

            public List setRegionCode(String str) {
                this.regionCode = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$GuideCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<GuideCategoryListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public I18nLanguages i18nLanguages() {
        return new I18nLanguages();
    }

    public class I18nLanguages {
        public I18nLanguages() {
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<I18nLanguageListResponse> {
            private static final String REST_PATH = "i18nLanguages";
            @Key
            private String hl;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, I18nLanguageListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nLanguages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nLanguageListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nLanguages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nLanguageListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nLanguages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nLanguageListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nLanguages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nLanguageListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nLanguages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nLanguageListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nLanguages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nLanguageListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nLanguages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nLanguageListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nLanguages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<I18nLanguageListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public I18nRegions i18nRegions() {
        return new I18nRegions();
    }

    public class I18nRegions {
        public I18nRegions() {
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<I18nRegionListResponse> {
            private static final String REST_PATH = "i18nRegions";
            @Key
            private String hl;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, I18nRegionListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nRegions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nRegionListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nRegions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nRegionListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nRegions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nRegionListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nRegions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nRegionListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nRegions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nRegionListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nRegions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nRegionListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nRegions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<I18nRegionListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$I18nRegions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<I18nRegionListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public LiveBroadcasts liveBroadcasts() {
        return new LiveBroadcasts();
    }

    public class LiveBroadcasts {
        public LiveBroadcasts() {
        }

        public Bind bind(String str, String str2) {
            Bind bind = new Bind(str, str2);
            YouTube.this.initialize(bind);
            return bind;
        }

        public class Bind extends YouTubeRequest<LiveBroadcast> {
            private static final String REST_PATH = "liveBroadcasts/bind";
            @Key
            private String id;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;
            @Key
            private String streamId;

            protected Bind(String str, String str2) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, null, LiveBroadcast.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
                this.part = (String) Preconditions.checkNotNull(str2, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Bind' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setAlt(String str) {
                return (Bind) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Bind' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setFields(String str) {
                return (Bind) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Bind' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setKey(String str) {
                return (Bind) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Bind' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setOauthToken(String str) {
                return (Bind) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Bind' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setPrettyPrint(Boolean bool) {
                return (Bind) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Bind' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setQuotaUser(String str) {
                return (Bind) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Bind' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setUserIp(String str) {
                return (Bind) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Bind setId(String str) {
                this.id = str;
                return this;
            }

            public String getPart() {
                return this.part;
            }

            public Bind setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Bind setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Bind setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            public String getStreamId() {
                return this.streamId;
            }

            public Bind setStreamId(String str) {
                this.streamId = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Bind' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveBroadcast> set(String str, Object obj) {
                return (Bind) super.set(str, obj);
            }
        }

        public Control control(String str, String str2) {
            Control control = new Control(str, str2);
            YouTube.this.initialize(control);
            return control;
        }

        public class Control extends YouTubeRequest<LiveBroadcast> {
            private static final String REST_PATH = "liveBroadcasts/control";
            @Key
            private Boolean displaySlate;
            @Key
            private String id;
            @Key
            private BigInteger offsetTimeMs;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;
            @Key
            private DateTime walltime;

            protected Control(String str, String str2) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, null, LiveBroadcast.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
                this.part = (String) Preconditions.checkNotNull(str2, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Control' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setAlt(String str) {
                return (Control) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Control' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setFields(String str) {
                return (Control) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Control' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setKey(String str) {
                return (Control) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Control' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setOauthToken(String str) {
                return (Control) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Control' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setPrettyPrint(Boolean bool) {
                return (Control) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Control' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setQuotaUser(String str) {
                return (Control) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Control' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setUserIp(String str) {
                return (Control) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Control setId(String str) {
                this.id = str;
                return this;
            }

            public String getPart() {
                return this.part;
            }

            public Control setPart(String str) {
                this.part = str;
                return this;
            }

            public Boolean getDisplaySlate() {
                return this.displaySlate;
            }

            public Control setDisplaySlate(Boolean bool) {
                this.displaySlate = bool;
                return this;
            }

            public BigInteger getOffsetTimeMs() {
                return this.offsetTimeMs;
            }

            public Control setOffsetTimeMs(BigInteger bigInteger) {
                this.offsetTimeMs = bigInteger;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Control setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Control setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            public DateTime getWalltime() {
                return this.walltime;
            }

            public Control setWalltime(DateTime dateTime) {
                this.walltime = dateTime;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Control' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveBroadcast> set(String str, Object obj) {
                return (Control) super.set(str, obj);
            }
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "liveBroadcasts";
            @Key
            private String id;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Delete setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Delete setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, LiveBroadcast liveBroadcast) {
            Insert insert = new Insert(str, liveBroadcast);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<LiveBroadcast> {
            private static final String REST_PATH = "liveBroadcasts";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;

            protected Insert(String str, LiveBroadcast liveBroadcast) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, liveBroadcast, LiveBroadcast.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Insert setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Insert setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveBroadcast> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<LiveBroadcastListResponse> {
            private static final String REST_PATH = "liveBroadcasts";
            @Key
            private String broadcastStatus;
            @Key
            private String broadcastType;
            @Key
            private String id;
            @Key
            private Long maxResults;
            @Key
            private Boolean mine;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String pageToken;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, LiveBroadcastListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcastListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcastListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcastListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcastListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcastListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcastListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcastListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getBroadcastStatus() {
                return this.broadcastStatus;
            }

            public List setBroadcastStatus(String str) {
                this.broadcastStatus = str;
                return this;
            }

            public String getBroadcastType() {
                return this.broadcastType;
            }

            public List setBroadcastType(String str) {
                this.broadcastType = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public Boolean getMine() {
                return this.mine;
            }

            public List setMine(Boolean bool) {
                this.mine = bool;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public List setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveBroadcastListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public Transition transition(String str, String str2, String str3) {
            Transition transition = new Transition(str, str2, str3);
            YouTube.this.initialize(transition);
            return transition;
        }

        public class Transition extends YouTubeRequest<LiveBroadcast> {
            private static final String REST_PATH = "liveBroadcasts/transition";
            @Key
            private String broadcastStatus;
            @Key
            private String id;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;

            protected Transition(String str, String str2, String str3) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, null, LiveBroadcast.class);
                this.broadcastStatus = (String) Preconditions.checkNotNull(str, "Required parameter broadcastStatus must be specified.");
                this.id = (String) Preconditions.checkNotNull(str2, "Required parameter id must be specified.");
                this.part = (String) Preconditions.checkNotNull(str3, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Transition' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setAlt(String str) {
                return (Transition) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Transition' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setFields(String str) {
                return (Transition) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Transition' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setKey(String str) {
                return (Transition) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Transition' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setOauthToken(String str) {
                return (Transition) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Transition' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setPrettyPrint(Boolean bool) {
                return (Transition) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Transition' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setQuotaUser(String str) {
                return (Transition) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Transition' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setUserIp(String str) {
                return (Transition) super.setUserIp(str);
            }

            public String getBroadcastStatus() {
                return this.broadcastStatus;
            }

            public Transition setBroadcastStatus(String str) {
                this.broadcastStatus = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public Transition setId(String str) {
                this.id = str;
                return this;
            }

            public String getPart() {
                return this.part;
            }

            public Transition setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Transition setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Transition setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Transition' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveBroadcast> set(String str, Object obj) {
                return (Transition) super.set(str, obj);
            }
        }

        public Update update(String str, LiveBroadcast liveBroadcast) {
            Update update = new Update(str, liveBroadcast);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<LiveBroadcast> {
            private static final String REST_PATH = "liveBroadcasts";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;

            protected Update(String str, LiveBroadcast liveBroadcast) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, liveBroadcast, LiveBroadcast.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
                checkRequiredParameter(liveBroadcast, "content");
                checkRequiredParameter(liveBroadcast.getId(), "LiveBroadcast.getId()");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveBroadcast> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Update setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Update setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveBroadcasts$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveBroadcast> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public LiveChatBans liveChatBans() {
        return new LiveChatBans();
    }

    public class LiveChatBans {
        public LiveChatBans() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "liveChat/bans";
            @Key
            private String id;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, LiveChatBan liveChatBan) {
            Insert insert = new Insert(str, liveChatBan);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<LiveChatBan> {
            private static final String REST_PATH = "liveChat/bans";
            @Key
            private String part;

            protected Insert(String str, LiveChatBan liveChatBan) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, liveChatBan, LiveChatBan.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatBan> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatBan> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatBan> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatBan> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatBan> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatBan> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatBan> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatBans$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveChatBan> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }
    }

    public LiveChatMessages liveChatMessages() {
        return new LiveChatMessages();
    }

    public class LiveChatMessages {
        public LiveChatMessages() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "liveChat/messages";
            @Key
            private String id;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, LiveChatMessage liveChatMessage) {
            Insert insert = new Insert(str, liveChatMessage);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<LiveChatMessage> {
            private static final String REST_PATH = "liveChat/messages";
            @Key
            private String part;

            protected Insert(String str, LiveChatMessage liveChatMessage) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, liveChatMessage, LiveChatMessage.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessage> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessage> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessage> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessage> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessage> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessage> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessage> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveChatMessage> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str, String str2) {
            List list = new List(str, str2);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<LiveChatMessageListResponse> {
            private static final String REST_PATH = "liveChat/messages";
            @Key
            private String hl;
            @Key
            private String liveChatId;
            @Key
            private Long maxResults;
            @Key
            private String pageToken;
            @Key
            private String part;
            @Key
            private Long profileImageSize;

            protected List(String str, String str2) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, LiveChatMessageListResponse.class);
                this.liveChatId = (String) Preconditions.checkNotNull(str, "Required parameter liveChatId must be specified.");
                this.part = (String) Preconditions.checkNotNull(str2, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessageListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessageListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessageListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessageListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessageListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessageListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatMessageListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getLiveChatId() {
                return this.liveChatId;
            }

            public List setLiveChatId(String str) {
                this.liveChatId = str;
                return this;
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            public Long getProfileImageSize() {
                return this.profileImageSize;
            }

            public List setProfileImageSize(Long l) {
                this.profileImageSize = l;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatMessages$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveChatMessageListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public LiveChatModerators liveChatModerators() {
        return new LiveChatModerators();
    }

    public class LiveChatModerators {
        public LiveChatModerators() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "liveChat/moderators";
            @Key
            private String id;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, LiveChatModerator liveChatModerator) {
            Insert insert = new Insert(str, liveChatModerator);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<LiveChatModerator> {
            private static final String REST_PATH = "liveChat/moderators";
            @Key
            private String part;

            protected Insert(String str, LiveChatModerator liveChatModerator) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, liveChatModerator, LiveChatModerator.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModerator> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModerator> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModerator> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModerator> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModerator> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModerator> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModerator> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveChatModerator> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str, String str2) {
            List list = new List(str, str2);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<LiveChatModeratorListResponse> {
            private static final String REST_PATH = "liveChat/moderators";
            @Key
            private String liveChatId;
            @Key
            private Long maxResults;
            @Key
            private String pageToken;
            @Key
            private String part;

            protected List(String str, String str2) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, LiveChatModeratorListResponse.class);
                this.liveChatId = (String) Preconditions.checkNotNull(str, "Required parameter liveChatId must be specified.");
                this.part = (String) Preconditions.checkNotNull(str2, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModeratorListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModeratorListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModeratorListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModeratorListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModeratorListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModeratorListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveChatModeratorListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getLiveChatId() {
                return this.liveChatId;
            }

            public List setLiveChatId(String str) {
                this.liveChatId = str;
                return this;
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveChatModerators$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveChatModeratorListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public LiveStreams liveStreams() {
        return new LiveStreams();
    }

    public class LiveStreams {
        public LiveStreams() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "liveStreams";
            @Key
            private String id;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Delete setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Delete setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, LiveStream liveStream) {
            Insert insert = new Insert(str, liveStream);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<LiveStream> {
            private static final String REST_PATH = "liveStreams";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;

            protected Insert(String str, LiveStream liveStream) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, liveStream, LiveStream.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Insert setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Insert setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveStream> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<LiveStreamListResponse> {
            private static final String REST_PATH = "liveStreams";
            @Key
            private String id;
            @Key
            private Long maxResults;
            @Key
            private Boolean mine;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String pageToken;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, LiveStreamListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStreamListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStreamListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStreamListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStreamListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStreamListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStreamListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStreamListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public Boolean getMine() {
                return this.mine;
            }

            public List setMine(Boolean bool) {
                this.mine = bool;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public List setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveStreamListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public Update update(String str, LiveStream liveStream) {
            Update update = new Update(str, liveStream);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<LiveStream> {
            private static final String REST_PATH = "liveStreams";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;

            protected Update(String str, LiveStream liveStream) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, liveStream, LiveStream.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
                checkRequiredParameter(liveStream, "content");
                checkRequiredParameter(liveStream.getId(), "LiveStream.getId()");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<LiveStream> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Update setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Update setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$LiveStreams$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<LiveStream> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public PlaylistItems playlistItems() {
        return new PlaylistItems();
    }

    public class PlaylistItems {
        public PlaylistItems() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "playlistItems";
            @Key
            private String id;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, PlaylistItem playlistItem) {
            Insert insert = new Insert(str, playlistItem);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<PlaylistItem> {
            private static final String REST_PATH = "playlistItems";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String part;

            protected Insert(String str, PlaylistItem playlistItem) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, playlistItem, PlaylistItem.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Insert setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<PlaylistItem> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<PlaylistItemListResponse> {
            private static final String REST_PATH = "playlistItems";
            @Key
            private String id;
            @Key
            private Long maxResults;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String pageToken;
            @Key
            private String part;
            @Key
            private String playlistId;
            @Key
            private String videoId;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, PlaylistItemListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItemListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItemListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItemListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItemListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItemListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItemListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItemListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            public String getPlaylistId() {
                return this.playlistId;
            }

            public List setPlaylistId(String str) {
                this.playlistId = str;
                return this;
            }

            public String getVideoId() {
                return this.videoId;
            }

            public List setVideoId(String str) {
                this.videoId = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<PlaylistItemListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public Update update(String str, PlaylistItem playlistItem) {
            Update update = new Update(str, playlistItem);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<PlaylistItem> {
            private static final String REST_PATH = "playlistItems";
            @Key
            private String part;

            protected Update(String str, PlaylistItem playlistItem) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, playlistItem, PlaylistItem.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistItem> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$PlaylistItems$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<PlaylistItem> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public Playlists playlists() {
        return new Playlists();
    }

    public class Playlists {
        public Playlists() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "playlists";
            @Key
            private String id;
            @Key
            private String onBehalfOfContentOwner;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Delete setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, Playlist playlist) {
            Insert insert = new Insert(str, playlist);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<Playlist> {
            private static final String REST_PATH = "playlists";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;

            protected Insert(String str, Playlist playlist) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, playlist, Playlist.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Insert setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Insert setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Playlist> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<PlaylistListResponse> {
            private static final String REST_PATH = "playlists";
            @Key
            private String channelId;
            @Key
            private String hl;
            @Key
            private String id;
            @Key
            private Long maxResults;
            @Key
            private Boolean mine;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String pageToken;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, PlaylistListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<PlaylistListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getChannelId() {
                return this.channelId;
            }

            public List setChannelId(String str) {
                this.channelId = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public Boolean getMine() {
                return this.mine;
            }

            public List setMine(Boolean bool) {
                this.mine = bool;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public List setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<PlaylistListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public Update update(String str, Playlist playlist) {
            Update update = new Update(str, playlist);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<Playlist> {
            private static final String REST_PATH = "playlists";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String part;

            protected Update(String str, Playlist playlist) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, playlist, Playlist.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Playlist> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Update setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Playlists$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Playlist> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public Search search() {
        return new Search();
    }

    public class Search {
        public Search() {
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<SearchListResponse> {
            private static final String REST_PATH = "search";
            @Key
            private String channelId;
            @Key
            private String channelType;
            @Key
            private String eventType;
            @Key
            private Boolean forContentOwner;
            @Key
            private Boolean forDeveloper;
            @Key
            private Boolean forMine;
            @Key
            private String location;
            @Key
            private String locationRadius;
            @Key
            private Long maxResults;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String order;
            @Key
            private String pageToken;
            @Key
            private String part;
            @Key
            private DateTime publishedAfter;
            @Key
            private DateTime publishedBefore;
            @Key
            private String q;
            @Key
            private String regionCode;
            @Key
            private String relatedToVideoId;
            @Key
            private String relevanceLanguage;
            @Key
            private String safeSearch;
            @Key
            private String topicId;
            @Key
            private String type;
            @Key
            private String videoCaption;
            @Key
            private String videoCategoryId;
            @Key
            private String videoDefinition;
            @Key
            private String videoDimension;
            @Key
            private String videoDuration;
            @Key
            private String videoEmbeddable;
            @Key
            private String videoLicense;
            @Key
            private String videoSyndicated;
            @Key
            private String videoType;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, SearchListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Search$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SearchListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Search$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SearchListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Search$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SearchListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Search$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SearchListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Search$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SearchListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Search$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SearchListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Search$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SearchListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getChannelId() {
                return this.channelId;
            }

            public List setChannelId(String str) {
                this.channelId = str;
                return this;
            }

            public String getChannelType() {
                return this.channelType;
            }

            public List setChannelType(String str) {
                this.channelType = str;
                return this;
            }

            public String getEventType() {
                return this.eventType;
            }

            public List setEventType(String str) {
                this.eventType = str;
                return this;
            }

            public Boolean getForContentOwner() {
                return this.forContentOwner;
            }

            public List setForContentOwner(Boolean bool) {
                this.forContentOwner = bool;
                return this;
            }

            public Boolean getForDeveloper() {
                return this.forDeveloper;
            }

            public List setForDeveloper(Boolean bool) {
                this.forDeveloper = bool;
                return this;
            }

            public Boolean getForMine() {
                return this.forMine;
            }

            public List setForMine(Boolean bool) {
                this.forMine = bool;
                return this;
            }

            public String getLocation() {
                return this.location;
            }

            public List setLocation(String str) {
                this.location = str;
                return this;
            }

            public String getLocationRadius() {
                return this.locationRadius;
            }

            public List setLocationRadius(String str) {
                this.locationRadius = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOrder() {
                return this.order;
            }

            public List setOrder(String str) {
                this.order = str;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            public DateTime getPublishedAfter() {
                return this.publishedAfter;
            }

            public List setPublishedAfter(DateTime dateTime) {
                this.publishedAfter = dateTime;
                return this;
            }

            public DateTime getPublishedBefore() {
                return this.publishedBefore;
            }

            public List setPublishedBefore(DateTime dateTime) {
                this.publishedBefore = dateTime;
                return this;
            }

            public String getQ() {
                return this.q;
            }

            public List setQ(String str) {
                this.q = str;
                return this;
            }

            public String getRegionCode() {
                return this.regionCode;
            }

            public List setRegionCode(String str) {
                this.regionCode = str;
                return this;
            }

            public String getRelatedToVideoId() {
                return this.relatedToVideoId;
            }

            public List setRelatedToVideoId(String str) {
                this.relatedToVideoId = str;
                return this;
            }

            public String getRelevanceLanguage() {
                return this.relevanceLanguage;
            }

            public List setRelevanceLanguage(String str) {
                this.relevanceLanguage = str;
                return this;
            }

            public String getSafeSearch() {
                return this.safeSearch;
            }

            public List setSafeSearch(String str) {
                this.safeSearch = str;
                return this;
            }

            public String getTopicId() {
                return this.topicId;
            }

            public List setTopicId(String str) {
                this.topicId = str;
                return this;
            }

            public String getType() {
                return this.type;
            }

            public List setType(String str) {
                this.type = str;
                return this;
            }

            public String getVideoCaption() {
                return this.videoCaption;
            }

            public List setVideoCaption(String str) {
                this.videoCaption = str;
                return this;
            }

            public String getVideoCategoryId() {
                return this.videoCategoryId;
            }

            public List setVideoCategoryId(String str) {
                this.videoCategoryId = str;
                return this;
            }

            public String getVideoDefinition() {
                return this.videoDefinition;
            }

            public List setVideoDefinition(String str) {
                this.videoDefinition = str;
                return this;
            }

            public String getVideoDimension() {
                return this.videoDimension;
            }

            public List setVideoDimension(String str) {
                this.videoDimension = str;
                return this;
            }

            public String getVideoDuration() {
                return this.videoDuration;
            }

            public List setVideoDuration(String str) {
                this.videoDuration = str;
                return this;
            }

            public String getVideoEmbeddable() {
                return this.videoEmbeddable;
            }

            public List setVideoEmbeddable(String str) {
                this.videoEmbeddable = str;
                return this;
            }

            public String getVideoLicense() {
                return this.videoLicense;
            }

            public List setVideoLicense(String str) {
                this.videoLicense = str;
                return this;
            }

            public String getVideoSyndicated() {
                return this.videoSyndicated;
            }

            public List setVideoSyndicated(String str) {
                this.videoSyndicated = str;
                return this;
            }

            public String getVideoType() {
                return this.videoType;
            }

            public List setVideoType(String str) {
                this.videoType = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Search$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<SearchListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public Sponsors sponsors() {
        return new Sponsors();
    }

    public class Sponsors {
        public Sponsors() {
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<SponsorListResponse> {
            private static final String REST_PATH = "sponsors";
            @Key
            private String filter;
            @Key
            private Long maxResults;
            @Key
            private String pageToken;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, SponsorListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Sponsors$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SponsorListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Sponsors$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SponsorListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Sponsors$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SponsorListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Sponsors$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SponsorListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Sponsors$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SponsorListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Sponsors$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SponsorListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Sponsors$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SponsorListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getFilter() {
                return this.filter;
            }

            public List setFilter(String str) {
                this.filter = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Sponsors$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<SponsorListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public Subscriptions subscriptions() {
        return new Subscriptions();
    }

    public class Subscriptions {
        public Subscriptions() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "subscriptions";
            @Key
            private String id;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public Insert insert(String str, Subscription subscription) {
            Insert insert = new Insert(str, subscription);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<Subscription> {
            private static final String REST_PATH = "subscriptions";
            @Key
            private String part;

            protected Insert(String str, Subscription subscription) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, subscription, Subscription.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Subscription> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Subscription> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Subscription> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Subscription> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Subscription> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Subscription> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Subscription> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Subscription> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<SubscriptionListResponse> {
            private static final String REST_PATH = "subscriptions";
            @Key
            private String channelId;
            @Key
            private String forChannelId;
            @Key
            private String id;
            @Key
            private Long maxResults;
            @Key
            private Boolean mine;
            @Key
            private Boolean myRecentSubscribers;
            @Key
            private Boolean mySubscribers;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String order;
            @Key
            private String pageToken;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, SubscriptionListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SubscriptionListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SubscriptionListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SubscriptionListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SubscriptionListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SubscriptionListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SubscriptionListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<SubscriptionListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getChannelId() {
                return this.channelId;
            }

            public List setChannelId(String str) {
                this.channelId = str;
                return this;
            }

            public String getForChannelId() {
                return this.forChannelId;
            }

            public List setForChannelId(String str) {
                this.forChannelId = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public Boolean getMine() {
                return this.mine;
            }

            public List setMine(Boolean bool) {
                this.mine = bool;
                return this;
            }

            public Boolean getMyRecentSubscribers() {
                return this.myRecentSubscribers;
            }

            public List setMyRecentSubscribers(Boolean bool) {
                this.myRecentSubscribers = bool;
                return this;
            }

            public Boolean getMySubscribers() {
                return this.mySubscribers;
            }

            public List setMySubscribers(Boolean bool) {
                this.mySubscribers = bool;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public List setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            public String getOrder() {
                return this.order;
            }

            public List setOrder(String str) {
                this.order = str;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Subscriptions$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<SubscriptionListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public Thumbnails thumbnails() {
        return new Thumbnails();
    }

    public class Thumbnails {
        public Thumbnails() {
        }

        public Set set(String str) {
            Set set = new Set(str);
            YouTube.this.initialize(set);
            return set;
        }

        public Set set(String str, AbstractInputStreamContent abstractInputStreamContent) {
            Set set = new Set(str, abstractInputStreamContent);
            YouTube.this.initialize(set);
            return set;
        }

        public class Set extends YouTubeRequest<ThumbnailSetResponse> {
            private static final String REST_PATH = "thumbnails/set";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String videoId;

            protected Set(String str) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, null, ThumbnailSetResponse.class);
                this.videoId = (String) Preconditions.checkNotNull(str, "Required parameter videoId must be specified.");
            }

            protected Set(String str, AbstractInputStreamContent abstractInputStreamContent) {
                super(YouTube.this, HttpMethods.POST, "/upload/" + YouTube.this.getServicePath() + REST_PATH, null, ThumbnailSetResponse.class);
                this.videoId = (String) Preconditions.checkNotNull(str, "Required parameter videoId must be specified.");
                initializeMediaUpload(abstractInputStreamContent);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Thumbnails$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ThumbnailSetResponse> setAlt(String str) {
                return (Set) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Thumbnails$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ThumbnailSetResponse> setFields(String str) {
                return (Set) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Thumbnails$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ThumbnailSetResponse> setKey(String str) {
                return (Set) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Thumbnails$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ThumbnailSetResponse> setOauthToken(String str) {
                return (Set) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Thumbnails$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ThumbnailSetResponse> setPrettyPrint(Boolean bool) {
                return (Set) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Thumbnails$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ThumbnailSetResponse> setQuotaUser(String str) {
                return (Set) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Thumbnails$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<ThumbnailSetResponse> setUserIp(String str) {
                return (Set) super.setUserIp(str);
            }

            public String getVideoId() {
                return this.videoId;
            }

            public Set setVideoId(String str) {
                this.videoId = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Set setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Thumbnails$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<ThumbnailSetResponse> set(String str, Object obj) {
                return (Set) super.set(str, obj);
            }
        }
    }

    public VideoAbuseReportReasons videoAbuseReportReasons() {
        return new VideoAbuseReportReasons();
    }

    public class VideoAbuseReportReasons {
        public VideoAbuseReportReasons() {
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<VideoAbuseReportReasonListResponse> {
            private static final String REST_PATH = "videoAbuseReportReasons";
            @Key
            private String hl;
            @Key
            private String part;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, VideoAbuseReportReasonListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoAbuseReportReasons$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoAbuseReportReasonListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoAbuseReportReasons$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoAbuseReportReasonListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoAbuseReportReasons$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoAbuseReportReasonListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoAbuseReportReasons$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoAbuseReportReasonListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoAbuseReportReasons$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoAbuseReportReasonListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoAbuseReportReasons$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoAbuseReportReasonListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoAbuseReportReasons$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoAbuseReportReasonListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoAbuseReportReasons$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<VideoAbuseReportReasonListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public VideoCategories videoCategories() {
        return new VideoCategories();
    }

    public class VideoCategories {
        public VideoCategories() {
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<VideoCategoryListResponse> {
            private static final String REST_PATH = "videoCategories";
            @Key
            private String hl;
            @Key
            private String id;
            @Key
            private String part;
            @Key
            private String regionCode;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, VideoCategoryListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoCategoryListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoCategoryListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoCategoryListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoCategoryListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoCategoryListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoCategoryListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoCategoryListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public String getRegionCode() {
                return this.regionCode;
            }

            public List setRegionCode(String str) {
                this.regionCode = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$VideoCategories$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<VideoCategoryListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }
    }

    public Videos videos() {
        return new Videos();
    }

    public class Videos {
        public Videos() {
        }

        public Delete delete(String str) {
            Delete delete = new Delete(str);
            YouTube.this.initialize(delete);
            return delete;
        }

        public class Delete extends YouTubeRequest<Void> {
            private static final String REST_PATH = "videos";
            @Key
            private String id;
            @Key
            private String onBehalfOfContentOwner;

            protected Delete(String str) {
                super(YouTube.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Delete) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Delete) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Delete) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Delete) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Delete) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Delete) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Delete) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Delete setId(String str) {
                this.id = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Delete setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Delete' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Delete) super.set(str, obj);
            }
        }

        public GetRating getRating(String str) {
            GetRating getRating = new GetRating(str);
            YouTube.this.initialize(getRating);
            return getRating;
        }

        public class GetRating extends YouTubeRequest<VideoGetRatingResponse> {
            private static final String REST_PATH = "videos/getRating";
            @Key
            private String id;
            @Key
            private String onBehalfOfContentOwner;

            protected GetRating(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, VideoGetRatingResponse.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$GetRating' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoGetRatingResponse> setAlt(String str) {
                return (GetRating) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$GetRating' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoGetRatingResponse> setFields(String str) {
                return (GetRating) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$GetRating' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoGetRatingResponse> setKey(String str) {
                return (GetRating) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$GetRating' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoGetRatingResponse> setOauthToken(String str) {
                return (GetRating) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$GetRating' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoGetRatingResponse> setPrettyPrint(Boolean bool) {
                return (GetRating) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$GetRating' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoGetRatingResponse> setQuotaUser(String str) {
                return (GetRating) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$GetRating' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoGetRatingResponse> setUserIp(String str) {
                return (GetRating) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public GetRating setId(String str) {
                this.id = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public GetRating setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$GetRating' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<VideoGetRatingResponse> set(String str, Object obj) {
                return (GetRating) super.set(str, obj);
            }
        }

        public Insert insert(String str, Video video) {
            Insert insert = new Insert(str, video);
            YouTube.this.initialize(insert);
            return insert;
        }

        public Insert insert(String str, Video video, AbstractInputStreamContent abstractInputStreamContent) {
            Insert insert = new Insert(str, video, abstractInputStreamContent);
            YouTube.this.initialize(insert);
            return insert;
        }

        public class Insert extends YouTubeRequest<Video> {
            private static final String REST_PATH = "videos";
            @Key
            private Boolean autoLevels;
            @Key
            private Boolean notifySubscribers;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String onBehalfOfContentOwnerChannel;
            @Key
            private String part;
            @Key
            private Boolean stabilize;

            protected Insert(String str, Video video) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, video, Video.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            protected Insert(String str, Video video, AbstractInputStreamContent abstractInputStreamContent) {
                super(YouTube.this, HttpMethods.POST, "/upload/" + YouTube.this.getServicePath() + REST_PATH, video, Video.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
                initializeMediaUpload(abstractInputStreamContent);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setAlt(String str) {
                return (Insert) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setFields(String str) {
                return (Insert) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setKey(String str) {
                return (Insert) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setOauthToken(String str) {
                return (Insert) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setPrettyPrint(Boolean bool) {
                return (Insert) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setQuotaUser(String str) {
                return (Insert) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setUserIp(String str) {
                return (Insert) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Insert setPart(String str) {
                this.part = str;
                return this;
            }

            public Boolean getAutoLevels() {
                return this.autoLevels;
            }

            public Insert setAutoLevels(Boolean bool) {
                this.autoLevels = bool;
                return this;
            }

            public Boolean getNotifySubscribers() {
                return this.notifySubscribers;
            }

            public Insert setNotifySubscribers(Boolean bool) {
                this.notifySubscribers = bool;
                return this;
            }

            public boolean isNotifySubscribers() {
                if (this.notifySubscribers == null || this.notifySubscribers == Data.NULL_BOOLEAN) {
                    return true;
                }
                return this.notifySubscribers.booleanValue();
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Insert setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getOnBehalfOfContentOwnerChannel() {
                return this.onBehalfOfContentOwnerChannel;
            }

            public Insert setOnBehalfOfContentOwnerChannel(String str) {
                this.onBehalfOfContentOwnerChannel = str;
                return this;
            }

            public Boolean getStabilize() {
                return this.stabilize;
            }

            public Insert setStabilize(Boolean bool) {
                this.stabilize = bool;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Insert' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Video> set(String str, Object obj) {
                return (Insert) super.set(str, obj);
            }
        }

        public List list(String str) {
            List list = new List(str);
            YouTube.this.initialize(list);
            return list;
        }

        public class List extends YouTubeRequest<VideoListResponse> {
            private static final String REST_PATH = "videos";
            @Key
            private String chart;
            @Key
            private String hl;
            @Key
            private String id;
            @Key
            private String locale;
            @Key
            private Long maxResults;
            @Key
            private String myRating;
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String pageToken;
            @Key
            private String part;
            @Key
            private String regionCode;
            @Key
            private String videoCategoryId;

            protected List(String str) {
                super(YouTube.this, HttpMethods.GET, REST_PATH, null, VideoListResponse.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpResponse executeUsingHead() {
                return super.executeUsingHead();
            }

            @Override // com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public HttpRequest buildHttpRequestUsingHead() {
                return super.buildHttpRequestUsingHead();
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoListResponse> setAlt(String str) {
                return (List) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoListResponse> setFields(String str) {
                return (List) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoListResponse> setKey(String str) {
                return (List) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoListResponse> setOauthToken(String str) {
                return (List) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoListResponse> setPrettyPrint(Boolean bool) {
                return (List) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoListResponse> setQuotaUser(String str) {
                return (List) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<VideoListResponse> setUserIp(String str) {
                return (List) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public List setPart(String str) {
                this.part = str;
                return this;
            }

            public String getChart() {
                return this.chart;
            }

            public List setChart(String str) {
                this.chart = str;
                return this;
            }

            public String getHl() {
                return this.hl;
            }

            public List setHl(String str) {
                this.hl = str;
                return this;
            }

            public String getId() {
                return this.id;
            }

            public List setId(String str) {
                this.id = str;
                return this;
            }

            public String getLocale() {
                return this.locale;
            }

            public List setLocale(String str) {
                this.locale = str;
                return this;
            }

            public Long getMaxResults() {
                return this.maxResults;
            }

            public List setMaxResults(Long l) {
                this.maxResults = l;
                return this;
            }

            public String getMyRating() {
                return this.myRating;
            }

            public List setMyRating(String str) {
                this.myRating = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public List setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            public String getPageToken() {
                return this.pageToken;
            }

            public List setPageToken(String str) {
                this.pageToken = str;
                return this;
            }

            public String getRegionCode() {
                return this.regionCode;
            }

            public List setRegionCode(String str) {
                this.regionCode = str;
                return this;
            }

            public String getVideoCategoryId() {
                return this.videoCategoryId;
            }

            public List setVideoCategoryId(String str) {
                this.videoCategoryId = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$List' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<VideoListResponse> set(String str, Object obj) {
                return (List) super.set(str, obj);
            }
        }

        public Rate rate(String str, String str2) {
            Rate rate = new Rate(str, str2);
            YouTube.this.initialize(rate);
            return rate;
        }

        public class Rate extends YouTubeRequest<Void> {
            private static final String REST_PATH = "videos/rate";
            @Key
            private String id;
            @Key
            private String rating;

            protected Rate(String str, String str2) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, null, Void.class);
                this.id = (String) Preconditions.checkNotNull(str, "Required parameter id must be specified.");
                this.rating = (String) Preconditions.checkNotNull(str2, "Required parameter rating must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Rate' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Rate) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Rate' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Rate) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Rate' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Rate) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Rate' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Rate) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Rate' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Rate) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Rate' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Rate) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Rate' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Rate) super.setUserIp(str);
            }

            public String getId() {
                return this.id;
            }

            public Rate setId(String str) {
                this.id = str;
                return this;
            }

            public String getRating() {
                return this.rating;
            }

            public Rate setRating(String str) {
                this.rating = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Rate' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Rate) super.set(str, obj);
            }
        }

        public ReportAbuse reportAbuse(VideoAbuseReport videoAbuseReport) {
            ReportAbuse reportAbuse = new ReportAbuse(videoAbuseReport);
            YouTube.this.initialize(reportAbuse);
            return reportAbuse;
        }

        public class ReportAbuse extends YouTubeRequest<Void> {
            private static final String REST_PATH = "videos/reportAbuse";
            @Key
            private String onBehalfOfContentOwner;

            protected ReportAbuse(VideoAbuseReport videoAbuseReport) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, videoAbuseReport, Void.class);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$ReportAbuse' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (ReportAbuse) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$ReportAbuse' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (ReportAbuse) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$ReportAbuse' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (ReportAbuse) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$ReportAbuse' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (ReportAbuse) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$ReportAbuse' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (ReportAbuse) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$ReportAbuse' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (ReportAbuse) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$ReportAbuse' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (ReportAbuse) super.setUserIp(str);
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public ReportAbuse setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$ReportAbuse' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (ReportAbuse) super.set(str, obj);
            }
        }

        public Update update(String str, Video video) {
            Update update = new Update(str, video);
            YouTube.this.initialize(update);
            return update;
        }

        public class Update extends YouTubeRequest<Video> {
            private static final String REST_PATH = "videos";
            @Key
            private String onBehalfOfContentOwner;
            @Key
            private String part;

            protected Update(String str, Video video) {
                super(YouTube.this, HttpMethods.PUT, REST_PATH, video, Video.class);
                this.part = (String) Preconditions.checkNotNull(str, "Required parameter part must be specified.");
                checkRequiredParameter(video, "content");
                checkRequiredParameter(video.getId(), "Video.getId()");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setAlt(String str) {
                return (Update) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setFields(String str) {
                return (Update) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setKey(String str) {
                return (Update) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setOauthToken(String str) {
                return (Update) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setPrettyPrint(Boolean bool) {
                return (Update) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setQuotaUser(String str) {
                return (Update) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Video> setUserIp(String str) {
                return (Update) super.setUserIp(str);
            }

            public String getPart() {
                return this.part;
            }

            public Update setPart(String str) {
                this.part = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Update setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Videos$Update' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Video> set(String str, Object obj) {
                return (Update) super.set(str, obj);
            }
        }
    }

    public Watermarks watermarks() {
        return new Watermarks();
    }

    public class Watermarks {
        public Watermarks() {
        }

        public Set set(String str, InvideoBranding invideoBranding) {
            Set set = new Set(str, invideoBranding);
            YouTube.this.initialize(set);
            return set;
        }

        public Set set(String str, InvideoBranding invideoBranding, AbstractInputStreamContent abstractInputStreamContent) {
            Set set = new Set(str, invideoBranding, abstractInputStreamContent);
            YouTube.this.initialize(set);
            return set;
        }

        public class Set extends YouTubeRequest<Void> {
            private static final String REST_PATH = "watermarks/set";
            @Key
            private String channelId;
            @Key
            private String onBehalfOfContentOwner;

            protected Set(String str, InvideoBranding invideoBranding) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, invideoBranding, Void.class);
                this.channelId = (String) Preconditions.checkNotNull(str, "Required parameter channelId must be specified.");
            }

            protected Set(String str, InvideoBranding invideoBranding, AbstractInputStreamContent abstractInputStreamContent) {
                super(YouTube.this, HttpMethods.POST, "/upload/" + YouTube.this.getServicePath() + REST_PATH, invideoBranding, Void.class);
                this.channelId = (String) Preconditions.checkNotNull(str, "Required parameter channelId must be specified.");
                initializeMediaUpload(abstractInputStreamContent);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Set) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Set) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Set) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Set) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Set) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Set) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Set) super.setUserIp(str);
            }

            public String getChannelId() {
                return this.channelId;
            }

            public Set setChannelId(String str) {
                this.channelId = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Set setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Set' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Set) super.set(str, obj);
            }
        }

        public Unset unset(String str) {
            Unset unset = new Unset(str);
            YouTube.this.initialize(unset);
            return unset;
        }

        public class Unset extends YouTubeRequest<Void> {
            private static final String REST_PATH = "watermarks/unset";
            @Key
            private String channelId;
            @Key
            private String onBehalfOfContentOwner;

            protected Unset(String str) {
                super(YouTube.this, HttpMethods.POST, REST_PATH, null, Void.class);
                this.channelId = (String) Preconditions.checkNotNull(str, "Required parameter channelId must be specified.");
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Unset' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setAlt(String str) {
                return (Unset) super.setAlt(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Unset' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setFields(String str) {
                return (Unset) super.setFields(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Unset' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setKey(String str) {
                return (Unset) super.setKey(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Unset' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setOauthToken(String str) {
                return (Unset) super.setOauthToken(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Unset' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setPrettyPrint(Boolean bool) {
                return (Unset) super.setPrettyPrint(bool);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Unset' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setQuotaUser(String str) {
                return (Unset) super.setQuotaUser(str);
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Unset' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest
            public YouTubeRequest<Void> setUserIp(String str) {
                return (Unset) super.setUserIp(str);
            }

            public String getChannelId() {
                return this.channelId;
            }

            public Unset setChannelId(String str) {
                this.channelId = str;
                return this;
            }

            public String getOnBehalfOfContentOwner() {
                return this.onBehalfOfContentOwner;
            }

            public Unset setOnBehalfOfContentOwner(String str) {
                this.onBehalfOfContentOwner = str;
                return this;
            }

            /* Return type fixed from 'com.google.api.services.youtube.YouTube$Watermarks$Unset' to match base method */
            @Override // com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.services.youtube.YouTubeRequest, com.google.api.client.util.GenericData, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest, com.google.api.client.googleapis.services.AbstractGoogleClientRequest
            public YouTubeRequest<Void> set(String str, Object obj) {
                return (Unset) super.set(str, obj);
            }
        }
    }

    public static final class Builder extends AbstractGoogleJsonClient.Builder {
        public Builder(HttpTransport httpTransport, JsonFactory jsonFactory, HttpRequestInitializer httpRequestInitializer) {
            super(httpTransport, jsonFactory, YouTube.DEFAULT_ROOT_URL, YouTube.DEFAULT_SERVICE_PATH, httpRequestInitializer, false);
        }

        @Override // com.google.api.client.googleapis.services.AbstractGoogleClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder
        public YouTube build() {
            return new YouTube(this);
        }

        @Override // com.google.api.client.googleapis.services.AbstractGoogleClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder
        public Builder setRootUrl(String str) {
            return (Builder) super.setRootUrl(str);
        }

        @Override // com.google.api.client.googleapis.services.AbstractGoogleClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder
        public Builder setServicePath(String str) {
            return (Builder) super.setServicePath(str);
        }

        @Override // com.google.api.client.googleapis.services.AbstractGoogleClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder
        public Builder setHttpRequestInitializer(HttpRequestInitializer httpRequestInitializer) {
            return (Builder) super.setHttpRequestInitializer(httpRequestInitializer);
        }

        @Override // com.google.api.client.googleapis.services.AbstractGoogleClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder
        public Builder setApplicationName(String str) {
            return (Builder) super.setApplicationName(str);
        }

        @Override // com.google.api.client.googleapis.services.AbstractGoogleClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder
        public Builder setSuppressPatternChecks(boolean z) {
            return (Builder) super.setSuppressPatternChecks(z);
        }

        @Override // com.google.api.client.googleapis.services.AbstractGoogleClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder
        public Builder setSuppressRequiredParameterChecks(boolean z) {
            return (Builder) super.setSuppressRequiredParameterChecks(z);
        }

        @Override // com.google.api.client.googleapis.services.AbstractGoogleClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder
        public Builder setSuppressAllChecks(boolean z) {
            return (Builder) super.setSuppressAllChecks(z);
        }

        public Builder setYouTubeRequestInitializer(YouTubeRequestInitializer youTubeRequestInitializer) {
            return (Builder) super.setGoogleClientRequestInitializer((GoogleClientRequestInitializer) youTubeRequestInitializer);
        }

        @Override // com.google.api.client.googleapis.services.AbstractGoogleClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder, com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder
        public Builder setGoogleClientRequestInitializer(GoogleClientRequestInitializer googleClientRequestInitializer) {
            return (Builder) super.setGoogleClientRequestInitializer(googleClientRequestInitializer);
        }
    }
}
