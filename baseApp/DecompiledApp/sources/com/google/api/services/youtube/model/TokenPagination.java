package com.google.api.services.youtube.model;

import com.google.api.client.json.GenericJson;

public final class TokenPagination extends GenericJson {
    @Override // com.google.api.client.util.GenericData, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson
    public TokenPagination set(String str, Object obj) {
        return (TokenPagination) super.set(str, obj);
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.util.GenericData, java.util.AbstractMap, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, java.lang.Object
    public TokenPagination clone() {
        return (TokenPagination) super.clone();
    }
}
