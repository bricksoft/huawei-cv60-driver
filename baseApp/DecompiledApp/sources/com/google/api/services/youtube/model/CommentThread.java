package com.google.api.services.youtube.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class CommentThread extends GenericJson {
    @Key
    private String etag;
    @Key
    private String id;
    @Key
    private String kind;
    @Key
    private CommentThreadReplies replies;
    @Key
    private CommentThreadSnippet snippet;

    public String getEtag() {
        return this.etag;
    }

    public CommentThread setEtag(String str) {
        this.etag = str;
        return this;
    }

    public String getId() {
        return this.id;
    }

    public CommentThread setId(String str) {
        this.id = str;
        return this;
    }

    public String getKind() {
        return this.kind;
    }

    public CommentThread setKind(String str) {
        this.kind = str;
        return this;
    }

    public CommentThreadReplies getReplies() {
        return this.replies;
    }

    public CommentThread setReplies(CommentThreadReplies commentThreadReplies) {
        this.replies = commentThreadReplies;
        return this;
    }

    public CommentThreadSnippet getSnippet() {
        return this.snippet;
    }

    public CommentThread setSnippet(CommentThreadSnippet commentThreadSnippet) {
        this.snippet = commentThreadSnippet;
        return this;
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson
    public CommentThread set(String str, Object obj) {
        return (CommentThread) super.set(str, obj);
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.util.GenericData, java.util.AbstractMap, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, java.lang.Object
    public CommentThread clone() {
        return (CommentThread) super.clone();
    }
}
