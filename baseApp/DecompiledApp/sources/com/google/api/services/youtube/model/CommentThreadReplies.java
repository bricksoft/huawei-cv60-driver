package com.google.api.services.youtube.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Data;
import com.google.api.client.util.Key;
import java.util.List;

public final class CommentThreadReplies extends GenericJson {
    @Key
    private List<Comment> comments;

    static {
        Data.nullOf(Comment.class);
    }

    public List<Comment> getComments() {
        return this.comments;
    }

    public CommentThreadReplies setComments(List<Comment> list) {
        this.comments = list;
        return this;
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson
    public CommentThreadReplies set(String str, Object obj) {
        return (CommentThreadReplies) super.set(str, obj);
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.util.GenericData, java.util.AbstractMap, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, java.lang.Object
    public CommentThreadReplies clone() {
        return (CommentThreadReplies) super.clone();
    }
}
