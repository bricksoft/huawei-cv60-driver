package com.google.api.services.youtube.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class LanguageTag extends GenericJson {
    @Key
    private String value;

    public String getValue() {
        return this.value;
    }

    public LanguageTag setValue(String str) {
        this.value = str;
        return this;
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson
    public LanguageTag set(String str, Object obj) {
        return (LanguageTag) super.set(str, obj);
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.util.GenericData, java.util.AbstractMap, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, java.lang.Object
    public LanguageTag clone() {
        return (LanguageTag) super.clone();
    }
}
