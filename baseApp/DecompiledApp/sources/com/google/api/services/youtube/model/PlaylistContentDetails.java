package com.google.api.services.youtube.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class PlaylistContentDetails extends GenericJson {
    @Key
    private Long itemCount;

    public Long getItemCount() {
        return this.itemCount;
    }

    public PlaylistContentDetails setItemCount(Long l) {
        this.itemCount = l;
        return this;
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson
    public PlaylistContentDetails set(String str, Object obj) {
        return (PlaylistContentDetails) super.set(str, obj);
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.util.GenericData, java.util.AbstractMap, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, java.lang.Object
    public PlaylistContentDetails clone() {
        return (PlaylistContentDetails) super.clone();
    }
}
