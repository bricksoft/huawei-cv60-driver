package com.google.api.services.youtube.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class LiveChatBan extends GenericJson {
    @Key
    private String etag;
    @Key
    private String id;
    @Key
    private String kind;
    @Key
    private LiveChatBanSnippet snippet;

    public String getEtag() {
        return this.etag;
    }

    public LiveChatBan setEtag(String str) {
        this.etag = str;
        return this;
    }

    public String getId() {
        return this.id;
    }

    public LiveChatBan setId(String str) {
        this.id = str;
        return this;
    }

    public String getKind() {
        return this.kind;
    }

    public LiveChatBan setKind(String str) {
        this.kind = str;
        return this;
    }

    public LiveChatBanSnippet getSnippet() {
        return this.snippet;
    }

    public LiveChatBan setSnippet(LiveChatBanSnippet liveChatBanSnippet) {
        this.snippet = liveChatBanSnippet;
        return this;
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson
    public LiveChatBan set(String str, Object obj) {
        return (LiveChatBan) super.set(str, obj);
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.util.GenericData, java.util.AbstractMap, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, java.lang.Object
    public LiveChatBan clone() {
        return (LiveChatBan) super.clone();
    }
}
