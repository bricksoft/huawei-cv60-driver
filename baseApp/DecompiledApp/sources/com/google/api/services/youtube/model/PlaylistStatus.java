package com.google.api.services.youtube.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class PlaylistStatus extends GenericJson {
    @Key
    private String privacyStatus;

    public String getPrivacyStatus() {
        return this.privacyStatus;
    }

    public PlaylistStatus setPrivacyStatus(String str) {
        this.privacyStatus = str;
        return this;
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson
    public PlaylistStatus set(String str, Object obj) {
        return (PlaylistStatus) super.set(str, obj);
    }

    @Override // com.google.api.client.util.GenericData, com.google.api.client.util.GenericData, java.util.AbstractMap, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, com.google.api.client.json.GenericJson, java.lang.Object
    public PlaylistStatus clone() {
        return (PlaylistStatus) super.clone();
    }
}
