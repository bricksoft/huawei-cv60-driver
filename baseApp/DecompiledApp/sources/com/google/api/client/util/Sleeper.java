package com.google.api.client.util;

public interface Sleeper {
    public static final Sleeper DEFAULT = new Sleeper() {
        /* class com.google.api.client.util.Sleeper.AnonymousClass1 */

        @Override // com.google.api.client.util.Sleeper
        public void sleep(long j) {
            Thread.sleep(j);
        }
    };

    void sleep(long j);
}
