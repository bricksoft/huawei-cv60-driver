package com.google.api.client.util.store;

import java.io.Serializable;

public interface DataStoreFactory {
    <V extends Serializable> DataStore<V> getDataStore(String str);
}
