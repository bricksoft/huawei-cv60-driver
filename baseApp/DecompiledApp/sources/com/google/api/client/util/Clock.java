package com.google.api.client.util;

public interface Clock {
    public static final Clock SYSTEM = new Clock() {
        /* class com.google.api.client.util.Clock.AnonymousClass1 */

        @Override // com.google.api.client.util.Clock
        public long currentTimeMillis() {
            return System.currentTimeMillis();
        }
    };

    long currentTimeMillis();
}
