package com.google.api.client.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.WeakHashMap;

public class FieldInfo {
    private static final Map<Field, FieldInfo> CACHE = new WeakHashMap();
    private final Field field;
    private final boolean isPrimitive;
    private final String name;

    public static FieldInfo of(Enum<?> r5) {
        boolean z = true;
        try {
            FieldInfo of = of(r5.getClass().getField(r5.name()));
            if (of == null) {
                z = false;
            }
            Preconditions.checkArgument(z, "enum constant missing @Value or @NullValue annotation: %s", r5);
            return of;
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    public static FieldInfo of(Field field2) {
        String value;
        String str;
        FieldInfo fieldInfo = null;
        if (field2 != null) {
            synchronized (CACHE) {
                FieldInfo fieldInfo2 = CACHE.get(field2);
                boolean isEnumConstant = field2.isEnumConstant();
                if (fieldInfo2 != null || (!isEnumConstant && Modifier.isStatic(field2.getModifiers()))) {
                    fieldInfo = fieldInfo2;
                } else {
                    if (isEnumConstant) {
                        Value value2 = (Value) field2.getAnnotation(Value.class);
                        if (value2 != null) {
                            value = value2.value();
                        } else if (((NullValue) field2.getAnnotation(NullValue.class)) != null) {
                            value = null;
                        }
                    } else {
                        Key key = (Key) field2.getAnnotation(Key.class);
                        if (key != null) {
                            value = key.value();
                            field2.setAccessible(true);
                        }
                    }
                    if ("##default".equals(value)) {
                        str = field2.getName();
                    } else {
                        str = value;
                    }
                    FieldInfo fieldInfo3 = new FieldInfo(field2, str);
                    CACHE.put(field2, fieldInfo3);
                    fieldInfo = fieldInfo3;
                }
            }
        }
        return fieldInfo;
    }

    FieldInfo(Field field2, String str) {
        this.field = field2;
        this.name = str == null ? null : str.intern();
        this.isPrimitive = Data.isPrimitive(getType());
    }

    public Field getField() {
        return this.field;
    }

    public String getName() {
        return this.name;
    }

    public Class<?> getType() {
        return this.field.getType();
    }

    public Type getGenericType() {
        return this.field.getGenericType();
    }

    public boolean isFinal() {
        return Modifier.isFinal(this.field.getModifiers());
    }

    public boolean isPrimitive() {
        return this.isPrimitive;
    }

    public Object getValue(Object obj) {
        return getFieldValue(this.field, obj);
    }

    public void setValue(Object obj, Object obj2) {
        setFieldValue(this.field, obj, obj2);
    }

    public ClassInfo getClassInfo() {
        return ClassInfo.of(this.field.getDeclaringClass());
    }

    public <T extends Enum<T>> T enumValue() {
        return (T) Enum.valueOf(this.field.getDeclaringClass(), this.field.getName());
    }

    public static Object getFieldValue(Field field2, Object obj) {
        try {
            return field2.get(obj);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void setFieldValue(Field field2, Object obj, Object obj2) {
        if (Modifier.isFinal(field2.getModifiers())) {
            Object fieldValue = getFieldValue(field2, obj);
            if (obj2 == null) {
                if (fieldValue == null) {
                    return;
                }
            } else if (obj2.equals(fieldValue)) {
                return;
            }
            String valueOf = String.valueOf(String.valueOf(fieldValue));
            String valueOf2 = String.valueOf(String.valueOf(obj2));
            String valueOf3 = String.valueOf(String.valueOf(field2.getName()));
            String valueOf4 = String.valueOf(String.valueOf(obj.getClass().getName()));
            throw new IllegalArgumentException(new StringBuilder(valueOf.length() + 48 + valueOf2.length() + valueOf3.length() + valueOf4.length()).append("expected final value <").append(valueOf).append("> but was <").append(valueOf2).append("> on ").append(valueOf3).append(" field in ").append(valueOf4).toString());
        }
        try {
            field2.set(obj, obj2);
        } catch (SecurityException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e2) {
            throw new IllegalArgumentException(e2);
        }
    }
}
