package com.google.api.client.util;

import java.io.OutputStream;

/* access modifiers changed from: package-private */
public final class ByteCountingOutputStream extends OutputStream {
    long count;

    ByteCountingOutputStream() {
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        this.count += (long) i2;
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        this.count++;
    }
}
