package com.google.api.client.util;

public interface BackOff {
    public static final long STOP = -1;
    public static final BackOff STOP_BACKOFF = new BackOff() {
        /* class com.google.api.client.util.BackOff.AnonymousClass2 */

        @Override // com.google.api.client.util.BackOff
        public void reset() {
        }

        @Override // com.google.api.client.util.BackOff
        public long nextBackOffMillis() {
            return -1;
        }
    };
    public static final BackOff ZERO_BACKOFF = new BackOff() {
        /* class com.google.api.client.util.BackOff.AnonymousClass1 */

        @Override // com.google.api.client.util.BackOff
        public void reset() {
        }

        @Override // com.google.api.client.util.BackOff
        public long nextBackOffMillis() {
            return 0;
        }
    };

    long nextBackOffMillis();

    void reset();
}
