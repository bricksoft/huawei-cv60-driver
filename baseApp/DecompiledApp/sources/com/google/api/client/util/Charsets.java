package com.google.api.client.util;

import com.google.android.exoplayer.C;
import java.nio.charset.Charset;

public final class Charsets {
    public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    public static final Charset UTF_8 = Charset.forName(C.UTF8_NAME);

    private Charsets() {
    }
}
