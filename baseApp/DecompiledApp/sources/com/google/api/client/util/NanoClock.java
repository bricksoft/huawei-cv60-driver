package com.google.api.client.util;

public interface NanoClock {
    public static final NanoClock SYSTEM = new NanoClock() {
        /* class com.google.api.client.util.NanoClock.AnonymousClass1 */

        @Override // com.google.api.client.util.NanoClock
        public long nanoTime() {
            return System.nanoTime();
        }
    };

    long nanoTime();
}
