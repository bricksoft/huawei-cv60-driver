package com.google.api.client.json;

import com.google.api.client.util.ClassInfo;
import com.google.api.client.util.Data;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.GenericData;
import com.google.api.client.util.Preconditions;
import com.google.api.client.util.Types;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;

public abstract class JsonGenerator {
    public abstract void close();

    public abstract void flush();

    public abstract JsonFactory getFactory();

    public abstract void writeBoolean(boolean z);

    public abstract void writeEndArray();

    public abstract void writeEndObject();

    public abstract void writeFieldName(String str);

    public abstract void writeNull();

    public abstract void writeNumber(double d);

    public abstract void writeNumber(float f);

    public abstract void writeNumber(int i);

    public abstract void writeNumber(long j);

    public abstract void writeNumber(String str);

    public abstract void writeNumber(BigDecimal bigDecimal);

    public abstract void writeNumber(BigInteger bigInteger);

    public abstract void writeStartArray();

    public abstract void writeStartObject();

    public abstract void writeString(String str);

    public final void serialize(Object obj) {
        serialize(false, obj);
    }

    private void serialize(boolean z, Object obj) {
        boolean z2;
        boolean z3 = true;
        if (obj != null) {
            Class<?> cls = obj.getClass();
            if (Data.isNull(obj)) {
                writeNull();
            } else if (obj instanceof String) {
                writeString((String) obj);
            } else if (obj instanceof Number) {
                if (z) {
                    writeString(obj.toString());
                } else if (obj instanceof BigDecimal) {
                    writeNumber((BigDecimal) obj);
                } else if (obj instanceof BigInteger) {
                    writeNumber((BigInteger) obj);
                } else if (obj instanceof Long) {
                    writeNumber(((Long) obj).longValue());
                } else if (obj instanceof Float) {
                    float floatValue = ((Number) obj).floatValue();
                    Preconditions.checkArgument(!Float.isInfinite(floatValue) && !Float.isNaN(floatValue));
                    writeNumber(floatValue);
                } else if ((obj instanceof Integer) || (obj instanceof Short) || (obj instanceof Byte)) {
                    writeNumber(((Number) obj).intValue());
                } else {
                    double doubleValue = ((Number) obj).doubleValue();
                    if (Double.isInfinite(doubleValue) || Double.isNaN(doubleValue)) {
                        z3 = false;
                    }
                    Preconditions.checkArgument(z3);
                    writeNumber(doubleValue);
                }
            } else if (obj instanceof Boolean) {
                writeBoolean(((Boolean) obj).booleanValue());
            } else if (obj instanceof DateTime) {
                writeString(((DateTime) obj).toStringRfc3339());
            } else if ((obj instanceof Iterable) || cls.isArray()) {
                writeStartArray();
                for (Object obj2 : Types.iterableOf(obj)) {
                    serialize(z, obj2);
                }
                writeEndArray();
            } else if (cls.isEnum()) {
                String name = FieldInfo.of((Enum) obj).getName();
                if (name == null) {
                    writeNull();
                } else {
                    writeString(name);
                }
            } else {
                writeStartObject();
                boolean z4 = (obj instanceof Map) && !(obj instanceof GenericData);
                ClassInfo of = z4 ? null : ClassInfo.of(cls);
                for (Map.Entry<String, Object> entry : Data.mapOf(obj).entrySet()) {
                    Object value = entry.getValue();
                    if (value != null) {
                        String key = entry.getKey();
                        if (z4) {
                            z2 = z;
                        } else {
                            Field field = of.getField(key);
                            z2 = (field == null || field.getAnnotation(JsonString.class) == null) ? false : true;
                        }
                        writeFieldName(key);
                        serialize(z2, value);
                    }
                }
                writeEndObject();
            }
        }
    }

    public void enablePrettyPrint() {
    }
}
