package com.google.api.client.http;

import java.io.OutputStream;

public class EmptyContent implements HttpContent {
    @Override // com.google.api.client.http.HttpContent
    public long getLength() {
        return 0;
    }

    @Override // com.google.api.client.http.HttpContent
    public String getType() {
        return null;
    }

    @Override // com.google.api.client.util.StreamingContent, com.google.api.client.http.HttpContent
    public void writeTo(OutputStream outputStream) {
        outputStream.flush();
    }

    @Override // com.google.api.client.http.HttpContent
    public boolean retrySupported() {
        return true;
    }
}
