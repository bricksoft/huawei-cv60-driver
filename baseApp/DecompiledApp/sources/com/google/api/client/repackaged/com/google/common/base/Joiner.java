package com.google.api.client.repackaged.com.google.common.base;

import com.google.api.client.repackaged.com.google.common.annotations.Beta;
import com.google.api.client.repackaged.com.google.common.annotations.GwtCompatible;
import java.io.IOException;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

@GwtCompatible
public class Joiner {
    private final String separator;

    public static Joiner on(String str) {
        return new Joiner(str);
    }

    public static Joiner on(char c) {
        return new Joiner(String.valueOf(c));
    }

    private Joiner(String str) {
        this.separator = (String) Preconditions.checkNotNull(str);
    }

    private Joiner(Joiner joiner) {
        this.separator = joiner.separator;
    }

    @Beta
    @Deprecated
    public final <A extends Appendable, I extends Iterable<?> & Iterator<?>> A appendTo(A a2, I i) {
        return (A) appendTo((Appendable) a2, (Iterator) i);
    }

    public <A extends Appendable> A appendTo(A a2, Iterable<?> iterable) {
        return (A) appendTo((Appendable) a2, iterable.iterator());
    }

    public <A extends Appendable> A appendTo(A a2, Iterator<?> it) {
        Preconditions.checkNotNull(a2);
        if (it.hasNext()) {
            a2.append(toString(it.next()));
            while (it.hasNext()) {
                a2.append(this.separator);
                a2.append(toString(it.next()));
            }
        }
        return a2;
    }

    public final <A extends Appendable> A appendTo(A a2, Object[] objArr) {
        return (A) appendTo((Appendable) a2, (Iterable<?>) Arrays.asList(objArr));
    }

    public final <A extends Appendable> A appendTo(A a2, Object obj, Object obj2, Object... objArr) {
        return (A) appendTo((Appendable) a2, iterable(obj, obj2, objArr));
    }

    @Beta
    @Deprecated
    public final <I extends Iterable<?> & Iterator<?>> StringBuilder appendTo(StringBuilder sb, I i) {
        return appendTo(sb, (Iterator) i);
    }

    public final StringBuilder appendTo(StringBuilder sb, Iterable<?> iterable) {
        return appendTo(sb, iterable.iterator());
    }

    public final StringBuilder appendTo(StringBuilder sb, Iterator<?> it) {
        try {
            appendTo((Appendable) sb, it);
            return sb;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public final StringBuilder appendTo(StringBuilder sb, Object[] objArr) {
        return appendTo(sb, (Iterable<?>) Arrays.asList(objArr));
    }

    public final StringBuilder appendTo(StringBuilder sb, Object obj, Object obj2, Object... objArr) {
        return appendTo(sb, iterable(obj, obj2, objArr));
    }

    @Beta
    @Deprecated
    public final <I extends Iterable<?> & Iterator<?>> String join(I i) {
        return join((Iterator) i);
    }

    public final String join(Iterable<?> iterable) {
        return join(iterable.iterator());
    }

    public final String join(Iterator<?> it) {
        return appendTo(new StringBuilder(), it).toString();
    }

    public final String join(Object[] objArr) {
        return join((Iterable<?>) Arrays.asList(objArr));
    }

    public final String join(Object obj, Object obj2, Object... objArr) {
        return join(iterable(obj, obj2, objArr));
    }

    public Joiner useForNull(final String str) {
        Preconditions.checkNotNull(str);
        return new Joiner(this) {
            /* class com.google.api.client.repackaged.com.google.common.base.Joiner.AnonymousClass1 */

            /* access modifiers changed from: package-private */
            @Override // com.google.api.client.repackaged.com.google.common.base.Joiner
            public CharSequence toString(Object obj) {
                return obj == null ? str : Joiner.this.toString(obj);
            }

            @Override // com.google.api.client.repackaged.com.google.common.base.Joiner
            public Joiner useForNull(String str) {
                Preconditions.checkNotNull(str);
                throw new UnsupportedOperationException("already specified useForNull");
            }

            @Override // com.google.api.client.repackaged.com.google.common.base.Joiner
            public Joiner skipNulls() {
                throw new UnsupportedOperationException("already specified useForNull");
            }
        };
    }

    public Joiner skipNulls() {
        return new Joiner(this) {
            /* class com.google.api.client.repackaged.com.google.common.base.Joiner.AnonymousClass2 */

            @Override // com.google.api.client.repackaged.com.google.common.base.Joiner
            public <A extends Appendable> A appendTo(A a2, Iterator<?> it) {
                Preconditions.checkNotNull(a2, "appendable");
                Preconditions.checkNotNull(it, "parts");
                while (true) {
                    if (it.hasNext()) {
                        Object next = it.next();
                        if (next != null) {
                            a2.append(Joiner.this.toString(next));
                            break;
                        }
                    } else {
                        break;
                    }
                }
                while (it.hasNext()) {
                    Object next2 = it.next();
                    if (next2 != null) {
                        a2.append(Joiner.this.separator);
                        a2.append(Joiner.this.toString(next2));
                    }
                }
                return a2;
            }

            @Override // com.google.api.client.repackaged.com.google.common.base.Joiner
            public Joiner useForNull(String str) {
                Preconditions.checkNotNull(str);
                throw new UnsupportedOperationException("already specified skipNulls");
            }

            @Override // com.google.api.client.repackaged.com.google.common.base.Joiner
            public MapJoiner withKeyValueSeparator(String str) {
                Preconditions.checkNotNull(str);
                throw new UnsupportedOperationException("can't use .skipNulls() with maps");
            }
        };
    }

    public MapJoiner withKeyValueSeparator(String str) {
        return new MapJoiner(str);
    }

    public static final class MapJoiner {
        private final Joiner joiner;
        private final String keyValueSeparator;

        private MapJoiner(Joiner joiner2, String str) {
            this.joiner = joiner2;
            this.keyValueSeparator = (String) Preconditions.checkNotNull(str);
        }

        public <A extends Appendable> A appendTo(A a2, Map<?, ?> map) {
            return (A) appendTo((Appendable) a2, (Iterable<? extends Map.Entry<?, ?>>) map.entrySet());
        }

        public StringBuilder appendTo(StringBuilder sb, Map<?, ?> map) {
            return appendTo(sb, (Iterable<? extends Map.Entry<?, ?>>) map.entrySet());
        }

        public String join(Map<?, ?> map) {
            return join((Iterable<? extends Map.Entry<?, ?>>) map.entrySet());
        }

        @Beta
        @Deprecated
        public <A extends Appendable, I extends Iterable<? extends Map.Entry<?, ?>> & Iterator<? extends Map.Entry<?, ?>>> A appendTo(A a2, I i) {
            return (A) appendTo((Appendable) a2, (Iterator) i);
        }

        @Beta
        public <A extends Appendable> A appendTo(A a2, Iterable<? extends Map.Entry<?, ?>> iterable) {
            return (A) appendTo((Appendable) a2, iterable.iterator());
        }

        @Beta
        public <A extends Appendable> A appendTo(A a2, Iterator<? extends Map.Entry<?, ?>> it) {
            Preconditions.checkNotNull(a2);
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                a2.append(this.joiner.toString(entry.getKey()));
                a2.append(this.keyValueSeparator);
                a2.append(this.joiner.toString(entry.getValue()));
                while (it.hasNext()) {
                    a2.append(this.joiner.separator);
                    Map.Entry entry2 = (Map.Entry) it.next();
                    a2.append(this.joiner.toString(entry2.getKey()));
                    a2.append(this.keyValueSeparator);
                    a2.append(this.joiner.toString(entry2.getValue()));
                }
            }
            return a2;
        }

        @Beta
        @Deprecated
        public <I extends Iterable<? extends Map.Entry<?, ?>> & Iterator<? extends Map.Entry<?, ?>>> StringBuilder appendTo(StringBuilder sb, I i) {
            return appendTo(sb, (Iterator) i);
        }

        @Beta
        public StringBuilder appendTo(StringBuilder sb, Iterable<? extends Map.Entry<?, ?>> iterable) {
            return appendTo(sb, iterable.iterator());
        }

        @Beta
        public StringBuilder appendTo(StringBuilder sb, Iterator<? extends Map.Entry<?, ?>> it) {
            try {
                appendTo((Appendable) sb, it);
                return sb;
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }

        @Beta
        @Deprecated
        public <I extends Iterable<? extends Map.Entry<?, ?>> & Iterator<? extends Map.Entry<?, ?>>> String join(I i) {
            return join((Iterator) i);
        }

        @Beta
        public String join(Iterable<? extends Map.Entry<?, ?>> iterable) {
            return join(iterable.iterator());
        }

        @Beta
        public String join(Iterator<? extends Map.Entry<?, ?>> it) {
            return appendTo(new StringBuilder(), it).toString();
        }

        public MapJoiner useForNull(String str) {
            return new MapJoiner(this.joiner.useForNull(str), this.keyValueSeparator);
        }
    }

    /* access modifiers changed from: package-private */
    public CharSequence toString(Object obj) {
        Preconditions.checkNotNull(obj);
        return obj instanceof CharSequence ? (CharSequence) obj : obj.toString();
    }

    private static Iterable<Object> iterable(final Object obj, final Object obj2, final Object[] objArr) {
        Preconditions.checkNotNull(objArr);
        return new AbstractList<Object>() {
            /* class com.google.api.client.repackaged.com.google.common.base.Joiner.AnonymousClass3 */

            public int size() {
                return objArr.length + 2;
            }

            @Override // java.util.List, java.util.AbstractList
            public Object get(int i) {
                switch (i) {
                    case 0:
                        return obj;
                    case 1:
                        return obj2;
                    default:
                        return objArr[i - 2];
                }
            }
        };
    }
}
