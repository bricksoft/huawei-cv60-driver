package com.google.api.client.googleapis.notifications;

import com.google.api.client.http.HttpMediaType;
import com.google.api.client.util.Beta;
import com.google.api.client.util.ObjectParser;
import com.google.api.client.util.Preconditions;

@Beta
public abstract class TypedNotificationCallback<T> implements UnparsedNotificationCallback {
    private static final long serialVersionUID = 1;

    /* access modifiers changed from: protected */
    public abstract Class<T> getDataClass();

    /* access modifiers changed from: protected */
    public abstract ObjectParser getObjectParser();

    /* access modifiers changed from: protected */
    public abstract void onNotification(StoredChannel storedChannel, TypedNotification<T> typedNotification);

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.google.api.client.googleapis.notifications.TypedNotificationCallback<T> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.google.api.client.googleapis.notifications.TypedNotification */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.api.client.googleapis.notifications.UnparsedNotificationCallback
    public final void onNotification(StoredChannel storedChannel, UnparsedNotification unparsedNotification) {
        TypedNotification typedNotification = new TypedNotification(unparsedNotification);
        String contentType = unparsedNotification.getContentType();
        if (contentType != null) {
            typedNotification.setContent(getObjectParser().parseAndClose(unparsedNotification.getContentStream(), new HttpMediaType(contentType).getCharsetParameter(), (Class) ((Class) Preconditions.checkNotNull(getDataClass()))));
        }
        onNotification(storedChannel, typedNotification);
    }
}
