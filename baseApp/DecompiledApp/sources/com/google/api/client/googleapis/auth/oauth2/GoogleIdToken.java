package com.google.api.client.googleapis.auth.oauth2;

import com.google.api.client.auth.openidconnect.IdToken;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.webtoken.JsonWebSignature;
import com.google.api.client.util.Beta;
import com.google.api.client.util.Key;
import java.util.List;

@Beta
public class GoogleIdToken extends IdToken {
    public static GoogleIdToken parse(JsonFactory jsonFactory, String str) {
        JsonWebSignature parse = JsonWebSignature.parser(jsonFactory).setPayloadClass(Payload.class).parse(str);
        return new GoogleIdToken(parse.getHeader(), (Payload) parse.getPayload(), parse.getSignatureBytes(), parse.getSignedContentBytes());
    }

    public GoogleIdToken(JsonWebSignature.Header header, Payload payload, byte[] bArr, byte[] bArr2) {
        super(header, payload, bArr, bArr2);
    }

    public boolean verify(GoogleIdTokenVerifier googleIdTokenVerifier) {
        return googleIdTokenVerifier.verify(this);
    }

    public Payload getPayload() {
        return GoogleIdToken.super.getPayload();
    }

    @Beta
    public static class Payload extends IdToken.Payload {
        @Key("email")
        private String email;
        @Key("email_verified")
        private Object emailVerified;
        @Key("hd")
        private String hostedDomain;

        @Deprecated
        public String getUserId() {
            return getSubject();
        }

        @Deprecated
        public Payload setUserId(String str) {
            return setSubject(str);
        }

        @Deprecated
        public String getIssuee() {
            return getAuthorizedParty();
        }

        @Deprecated
        public Payload setIssuee(String str) {
            return setAuthorizedParty(str);
        }

        public String getHostedDomain() {
            return this.hostedDomain;
        }

        public Payload setHostedDomain(String str) {
            this.hostedDomain = str;
            return this;
        }

        public String getEmail() {
            return this.email;
        }

        public Payload setEmail(String str) {
            this.email = str;
            return this;
        }

        public Boolean getEmailVerified() {
            if (this.emailVerified == null) {
                return null;
            }
            if (this.emailVerified instanceof Boolean) {
                return (Boolean) this.emailVerified;
            }
            return Boolean.valueOf((String) this.emailVerified);
        }

        public Payload setEmailVerified(Boolean bool) {
            this.emailVerified = bool;
            return this;
        }

        public Payload setAuthorizationTimeSeconds(Long l) {
            return GoogleIdToken.super.setAuthorizationTimeSeconds(l);
        }

        public Payload setAuthorizedParty(String str) {
            return GoogleIdToken.super.setAuthorizedParty(str);
        }

        public Payload setNonce(String str) {
            return GoogleIdToken.super.setNonce(str);
        }

        public Payload setAccessTokenHash(String str) {
            return GoogleIdToken.super.setAccessTokenHash(str);
        }

        public Payload setClassReference(String str) {
            return GoogleIdToken.super.setClassReference(str);
        }

        public Payload setMethodsReferences(List<String> list) {
            return GoogleIdToken.super.setMethodsReferences(list);
        }

        public Payload setExpirationTimeSeconds(Long l) {
            return GoogleIdToken.super.setExpirationTimeSeconds(l);
        }

        public Payload setNotBeforeTimeSeconds(Long l) {
            return GoogleIdToken.super.setNotBeforeTimeSeconds(l);
        }

        public Payload setIssuedAtTimeSeconds(Long l) {
            return GoogleIdToken.super.setIssuedAtTimeSeconds(l);
        }

        public Payload setIssuer(String str) {
            return GoogleIdToken.super.setIssuer(str);
        }

        public Payload setAudience(Object obj) {
            return GoogleIdToken.super.setAudience(obj);
        }

        public Payload setJwtId(String str) {
            return GoogleIdToken.super.setJwtId(str);
        }

        public Payload setType(String str) {
            return GoogleIdToken.super.setType(str);
        }

        public Payload setSubject(String str) {
            return GoogleIdToken.super.setSubject(str);
        }

        public Payload set(String str, Object obj) {
            return GoogleIdToken.super.set(str, obj);
        }

        public Payload clone() {
            return GoogleIdToken.super.clone();
        }
    }
}
