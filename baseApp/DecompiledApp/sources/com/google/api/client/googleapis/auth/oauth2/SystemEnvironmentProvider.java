package com.google.api.client.googleapis.auth.oauth2;

/* access modifiers changed from: package-private */
public class SystemEnvironmentProvider {
    static final SystemEnvironmentProvider INSTANCE = new SystemEnvironmentProvider();

    SystemEnvironmentProvider() {
    }

    /* access modifiers changed from: package-private */
    public String getEnv(String str) {
        return System.getenv(str);
    }
}
