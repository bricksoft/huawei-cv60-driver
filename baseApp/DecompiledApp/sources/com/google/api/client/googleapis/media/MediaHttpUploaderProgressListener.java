package com.google.api.client.googleapis.media;

public interface MediaHttpUploaderProgressListener {
    void progressChanged(MediaHttpUploader mediaHttpUploader);
}
