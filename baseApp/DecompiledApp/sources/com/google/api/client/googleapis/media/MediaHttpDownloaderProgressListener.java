package com.google.api.client.googleapis.media;

public interface MediaHttpDownloaderProgressListener {
    void progressChanged(MediaHttpDownloader mediaHttpDownloader);
}
