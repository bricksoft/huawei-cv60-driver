package com.google.api.client.googleapis.auth.oauth2;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.ClientParametersAuthentication;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.CredentialStore;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.Beta;
import com.google.api.client.util.Clock;
import com.google.api.client.util.Preconditions;
import com.google.api.client.util.store.DataStore;
import com.google.api.client.util.store.DataStoreFactory;
import java.util.Collection;

public class GoogleAuthorizationCodeFlow extends AuthorizationCodeFlow {
    private final String accessType;
    private final String approvalPrompt;

    public GoogleAuthorizationCodeFlow(HttpTransport httpTransport, JsonFactory jsonFactory, String str, String str2, Collection<String> collection) {
        this(new Builder(httpTransport, jsonFactory, str, str2, collection));
    }

    protected GoogleAuthorizationCodeFlow(Builder builder) {
        super(builder);
        this.accessType = builder.accessType;
        this.approvalPrompt = builder.approvalPrompt;
    }

    public GoogleAuthorizationCodeTokenRequest newTokenRequest(String str) {
        return new GoogleAuthorizationCodeTokenRequest(getTransport(), getJsonFactory(), getTokenServerEncodedUrl(), "", "", str, "").setClientAuthentication(getClientAuthentication()).setRequestInitializer(getRequestInitializer()).setScopes(getScopes());
    }

    public GoogleAuthorizationCodeRequestUrl newAuthorizationUrl() {
        return new GoogleAuthorizationCodeRequestUrl(getAuthorizationServerEncodedUrl(), getClientId(), "", getScopes()).setAccessType(this.accessType).setApprovalPrompt(this.approvalPrompt);
    }

    public final String getApprovalPrompt() {
        return this.approvalPrompt;
    }

    public final String getAccessType() {
        return this.accessType;
    }

    public static class Builder extends AuthorizationCodeFlow.Builder {
        String accessType;
        String approvalPrompt;

        public Builder(HttpTransport httpTransport, JsonFactory jsonFactory, String str, String str2, Collection<String> collection) {
            super(BearerToken.authorizationHeaderAccessMethod(), httpTransport, jsonFactory, new GenericUrl(GoogleOAuthConstants.TOKEN_SERVER_URL), new ClientParametersAuthentication(str, str2), str, GoogleOAuthConstants.AUTHORIZATION_SERVER_URL);
            setScopes(collection);
        }

        public Builder(HttpTransport httpTransport, JsonFactory jsonFactory, GoogleClientSecrets googleClientSecrets, Collection<String> collection) {
            super(BearerToken.authorizationHeaderAccessMethod(), httpTransport, jsonFactory, new GenericUrl(GoogleOAuthConstants.TOKEN_SERVER_URL), new ClientParametersAuthentication(googleClientSecrets.getDetails().getClientId(), googleClientSecrets.getDetails().getClientSecret()), googleClientSecrets.getDetails().getClientId(), GoogleOAuthConstants.AUTHORIZATION_SERVER_URL);
            setScopes(collection);
        }

        public GoogleAuthorizationCodeFlow build() {
            return new GoogleAuthorizationCodeFlow(this);
        }

        public Builder setDataStoreFactory(DataStoreFactory dataStoreFactory) {
            return GoogleAuthorizationCodeFlow.super.setDataStoreFactory(dataStoreFactory);
        }

        public Builder setCredentialDataStore(DataStore<StoredCredential> dataStore) {
            return GoogleAuthorizationCodeFlow.super.setCredentialDataStore(dataStore);
        }

        public Builder setCredentialCreatedListener(AuthorizationCodeFlow.CredentialCreatedListener credentialCreatedListener) {
            return GoogleAuthorizationCodeFlow.super.setCredentialCreatedListener(credentialCreatedListener);
        }

        @Beta
        @Deprecated
        public Builder setCredentialStore(CredentialStore credentialStore) {
            return GoogleAuthorizationCodeFlow.super.setCredentialStore(credentialStore);
        }

        public Builder setRequestInitializer(HttpRequestInitializer httpRequestInitializer) {
            return GoogleAuthorizationCodeFlow.super.setRequestInitializer(httpRequestInitializer);
        }

        public Builder setScopes(Collection<String> collection) {
            Preconditions.checkState(!collection.isEmpty());
            return GoogleAuthorizationCodeFlow.super.setScopes(collection);
        }

        public Builder setMethod(Credential.AccessMethod accessMethod) {
            return GoogleAuthorizationCodeFlow.super.setMethod(accessMethod);
        }

        public Builder setTransport(HttpTransport httpTransport) {
            return GoogleAuthorizationCodeFlow.super.setTransport(httpTransport);
        }

        public Builder setJsonFactory(JsonFactory jsonFactory) {
            return GoogleAuthorizationCodeFlow.super.setJsonFactory(jsonFactory);
        }

        public Builder setTokenServerUrl(GenericUrl genericUrl) {
            return GoogleAuthorizationCodeFlow.super.setTokenServerUrl(genericUrl);
        }

        public Builder setClientAuthentication(HttpExecuteInterceptor httpExecuteInterceptor) {
            return GoogleAuthorizationCodeFlow.super.setClientAuthentication(httpExecuteInterceptor);
        }

        public Builder setClientId(String str) {
            return GoogleAuthorizationCodeFlow.super.setClientId(str);
        }

        public Builder setAuthorizationServerEncodedUrl(String str) {
            return GoogleAuthorizationCodeFlow.super.setAuthorizationServerEncodedUrl(str);
        }

        public Builder setClock(Clock clock) {
            return GoogleAuthorizationCodeFlow.super.setClock(clock);
        }

        public Builder addRefreshListener(CredentialRefreshListener credentialRefreshListener) {
            return GoogleAuthorizationCodeFlow.super.addRefreshListener(credentialRefreshListener);
        }

        public Builder setRefreshListeners(Collection<CredentialRefreshListener> collection) {
            return GoogleAuthorizationCodeFlow.super.setRefreshListeners(collection);
        }

        public Builder setApprovalPrompt(String str) {
            this.approvalPrompt = str;
            return this;
        }

        public final String getApprovalPrompt() {
            return this.approvalPrompt;
        }

        public Builder setAccessType(String str) {
            this.accessType = str;
            return this;
        }

        public final String getAccessType() {
            return this.accessType;
        }
    }
}
