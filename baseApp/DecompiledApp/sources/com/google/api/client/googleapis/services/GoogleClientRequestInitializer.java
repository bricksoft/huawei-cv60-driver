package com.google.api.client.googleapis.services;

public interface GoogleClientRequestInitializer {
    void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest);
}
