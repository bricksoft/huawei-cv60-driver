package com.google.zxing.pdf417.decoder;

import com.google.zxing.FormatException;
import com.google.zxing.common.CharacterSetECI;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.pdf417.PDF417ResultMetadata;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.Arrays;

/* access modifiers changed from: package-private */
public final class DecodedBitStreamParser {
    private static final int AL = 28;
    private static final int AS = 27;
    private static final int BEGIN_MACRO_PDF417_CONTROL_BLOCK = 928;
    private static final int BEGIN_MACRO_PDF417_OPTIONAL_FIELD = 923;
    private static final int BYTE_COMPACTION_MODE_LATCH = 901;
    private static final int BYTE_COMPACTION_MODE_LATCH_6 = 924;
    private static final Charset DEFAULT_ENCODING = Charset.forName("ISO-8859-1");
    private static final int ECI_CHARSET = 927;
    private static final int ECI_GENERAL_PURPOSE = 926;
    private static final int ECI_USER_DEFINED = 925;
    private static final BigInteger[] EXP900;
    private static final int LL = 27;
    private static final int MACRO_PDF417_TERMINATOR = 922;
    private static final int MAX_NUMERIC_CODEWORDS = 15;
    private static final char[] MIXED_CHARS = "0123456789&\r\t,:#-.$/+%*=^".toCharArray();
    private static final int ML = 28;
    private static final int MODE_SHIFT_TO_BYTE_COMPACTION_MODE = 913;
    private static final int NUMBER_OF_SEQUENCE_CODEWORDS = 2;
    private static final int NUMERIC_COMPACTION_MODE_LATCH = 902;
    private static final int PAL = 29;
    private static final int PL = 25;
    private static final int PS = 29;
    private static final char[] PUNCT_CHARS = ";<>@[\\]_`~!\r\t,:\n-.$/\"|*()?{}'".toCharArray();
    private static final int TEXT_COMPACTION_MODE_LATCH = 900;

    /* access modifiers changed from: private */
    public enum Mode {
        ALPHA,
        LOWER,
        MIXED,
        PUNCT,
        ALPHA_SHIFT,
        PUNCT_SHIFT
    }

    static {
        BigInteger[] bigIntegerArr = new BigInteger[16];
        EXP900 = bigIntegerArr;
        bigIntegerArr[0] = BigInteger.ONE;
        BigInteger valueOf = BigInteger.valueOf(900);
        EXP900[1] = valueOf;
        for (int i = 2; i < EXP900.length; i++) {
            EXP900[i] = EXP900[i - 1].multiply(valueOf);
        }
    }

    private DecodedBitStreamParser() {
    }

    static DecoderResult decode(int[] iArr, String str) {
        int decodeMacroBlock;
        StringBuilder sb = new StringBuilder(iArr.length << 1);
        Charset charset = DEFAULT_ENCODING;
        int i = 2;
        int i2 = iArr[1];
        PDF417ResultMetadata pDF417ResultMetadata = new PDF417ResultMetadata();
        while (i < iArr[0]) {
            switch (i2) {
                case 900:
                    decodeMacroBlock = textCompaction(iArr, i, sb);
                    break;
                case 901:
                case BYTE_COMPACTION_MODE_LATCH_6 /*{ENCODED_INT: 924}*/:
                    decodeMacroBlock = byteCompaction(i2, iArr, charset, i, sb);
                    break;
                case 902:
                    decodeMacroBlock = numericCompaction(iArr, i, sb);
                    break;
                case MODE_SHIFT_TO_BYTE_COMPACTION_MODE /*{ENCODED_INT: 913}*/:
                    decodeMacroBlock = i + 1;
                    sb.append((char) iArr[i]);
                    break;
                case MACRO_PDF417_TERMINATOR /*{ENCODED_INT: 922}*/:
                case BEGIN_MACRO_PDF417_OPTIONAL_FIELD /*{ENCODED_INT: 923}*/:
                    throw FormatException.getFormatInstance();
                case ECI_USER_DEFINED /*{ENCODED_INT: 925}*/:
                    decodeMacroBlock = i + 1;
                    break;
                case ECI_GENERAL_PURPOSE /*{ENCODED_INT: 926}*/:
                    decodeMacroBlock = i + 2;
                    break;
                case ECI_CHARSET /*{ENCODED_INT: 927}*/:
                    decodeMacroBlock = i + 1;
                    charset = Charset.forName(CharacterSetECI.getCharacterSetECIByValue(iArr[i]).name());
                    break;
                case 928:
                    decodeMacroBlock = decodeMacroBlock(iArr, i, pDF417ResultMetadata);
                    break;
                default:
                    decodeMacroBlock = textCompaction(iArr, i - 1, sb);
                    break;
            }
            if (decodeMacroBlock < iArr.length) {
                i = decodeMacroBlock + 1;
                i2 = iArr[decodeMacroBlock];
            } else {
                throw FormatException.getFormatInstance();
            }
        }
        if (sb.length() == 0) {
            throw FormatException.getFormatInstance();
        }
        DecoderResult decoderResult = new DecoderResult(null, sb.toString(), null, str);
        decoderResult.setOther(pDF417ResultMetadata);
        return decoderResult;
    }

    private static int decodeMacroBlock(int[] iArr, int i, PDF417ResultMetadata pDF417ResultMetadata) {
        if (i + 2 > iArr[0]) {
            throw FormatException.getFormatInstance();
        }
        int[] iArr2 = new int[2];
        int i2 = 0;
        while (i2 < 2) {
            iArr2[i2] = iArr[i];
            i2++;
            i++;
        }
        pDF417ResultMetadata.setSegmentIndex(Integer.parseInt(decodeBase900toBase10(iArr2, 2)));
        StringBuilder sb = new StringBuilder();
        int textCompaction = textCompaction(iArr, i, sb);
        pDF417ResultMetadata.setFileId(sb.toString());
        if (iArr[textCompaction] == BEGIN_MACRO_PDF417_OPTIONAL_FIELD) {
            int i3 = textCompaction + 1;
            int[] iArr3 = new int[(iArr[0] - i3)];
            boolean z = false;
            int i4 = 0;
            int i5 = i3;
            while (i5 < iArr[0] && !z) {
                int i6 = i5 + 1;
                int i7 = iArr[i5];
                if (i7 < 900) {
                    iArr3[i4] = i7;
                    i4++;
                    i5 = i6;
                } else {
                    switch (i7) {
                        default:
                            throw FormatException.getFormatInstance();
                        case MACRO_PDF417_TERMINATOR /*{ENCODED_INT: 922}*/:
                            pDF417ResultMetadata.setLastSegment(true);
                            z = true;
                            i5 = i6 + 1;
                            continue;
                    }
                }
            }
            pDF417ResultMetadata.setOptionalData(Arrays.copyOf(iArr3, i4));
            return i5;
        } else if (iArr[textCompaction] != MACRO_PDF417_TERMINATOR) {
            return textCompaction;
        } else {
            pDF417ResultMetadata.setLastSegment(true);
            return textCompaction + 1;
        }
    }

    private static int textCompaction(int[] iArr, int i, StringBuilder sb) {
        int[] iArr2 = new int[((iArr[0] - i) << 1)];
        int[] iArr3 = new int[((iArr[0] - i) << 1)];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i4 < 900) {
                iArr2[i2] = i4 / 30;
                iArr2[i2 + 1] = i4 % 30;
                i2 += 2;
                i = i3;
            } else {
                switch (i4) {
                    case 900:
                        iArr2[i2] = 900;
                        i2++;
                        i = i3;
                        continue;
                    case 901:
                    case 902:
                    case MACRO_PDF417_TERMINATOR /*{ENCODED_INT: 922}*/:
                    case BEGIN_MACRO_PDF417_OPTIONAL_FIELD /*{ENCODED_INT: 923}*/:
                    case BYTE_COMPACTION_MODE_LATCH_6 /*{ENCODED_INT: 924}*/:
                    case 928:
                        i = i3 - 1;
                        z = true;
                        continue;
                    case MODE_SHIFT_TO_BYTE_COMPACTION_MODE /*{ENCODED_INT: 913}*/:
                        iArr2[i2] = MODE_SHIFT_TO_BYTE_COMPACTION_MODE;
                        i = i3 + 1;
                        iArr3[i2] = iArr[i3];
                        i2++;
                        continue;
                    default:
                        i = i3;
                        continue;
                }
            }
        }
        decodeTextCompaction(iArr2, iArr3, i2, sb);
        return i;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static void decodeTextCompaction(int[] iArr, int[] iArr2, int i, StringBuilder sb) {
        Mode mode;
        Mode mode2;
        Mode mode3 = Mode.ALPHA;
        Mode mode4 = Mode.ALPHA;
        int i2 = 0;
        while (i2 < i) {
            int i3 = iArr[i2];
            char c = 0;
            switch (mode3) {
                case ALPHA:
                    if (i3 >= 26) {
                        if (i3 != 26) {
                            if (i3 != 27) {
                                if (i3 != 28) {
                                    if (i3 != 29) {
                                        if (i3 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                            if (i3 == 900) {
                                                mode = mode4;
                                                mode2 = Mode.ALPHA;
                                                break;
                                            }
                                            mode = mode4;
                                            mode2 = mode3;
                                            break;
                                        } else {
                                            sb.append((char) iArr2[i2]);
                                            mode = mode4;
                                            mode2 = mode3;
                                            break;
                                        }
                                    } else {
                                        mode = mode3;
                                        mode2 = Mode.PUNCT_SHIFT;
                                        break;
                                    }
                                } else {
                                    mode = mode4;
                                    mode2 = Mode.MIXED;
                                    break;
                                }
                            } else {
                                mode = mode4;
                                mode2 = Mode.LOWER;
                                break;
                            }
                        } else {
                            c = ' ';
                            mode = mode4;
                            mode2 = mode3;
                            break;
                        }
                    } else {
                        c = (char) (i3 + 65);
                        mode = mode4;
                        mode2 = mode3;
                        break;
                    }
                case LOWER:
                    if (i3 >= 26) {
                        if (i3 != 26) {
                            if (i3 != 27) {
                                if (i3 != 28) {
                                    if (i3 != 29) {
                                        if (i3 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                            if (i3 == 900) {
                                                mode = mode4;
                                                mode2 = Mode.ALPHA;
                                                break;
                                            }
                                            mode = mode4;
                                            mode2 = mode3;
                                            break;
                                        } else {
                                            sb.append((char) iArr2[i2]);
                                            mode = mode4;
                                            mode2 = mode3;
                                            break;
                                        }
                                    } else {
                                        mode = mode3;
                                        mode2 = Mode.PUNCT_SHIFT;
                                        break;
                                    }
                                } else {
                                    mode = mode4;
                                    mode2 = Mode.MIXED;
                                    break;
                                }
                            } else {
                                mode = mode3;
                                mode2 = Mode.ALPHA_SHIFT;
                                break;
                            }
                        } else {
                            c = ' ';
                            mode = mode4;
                            mode2 = mode3;
                            break;
                        }
                    } else {
                        c = (char) (i3 + 97);
                        mode = mode4;
                        mode2 = mode3;
                        break;
                    }
                case MIXED:
                    if (i3 >= 25) {
                        if (i3 != 25) {
                            if (i3 != 26) {
                                if (i3 != 27) {
                                    if (i3 != 28) {
                                        if (i3 != 29) {
                                            if (i3 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                                if (i3 == 900) {
                                                    mode = mode4;
                                                    mode2 = Mode.ALPHA;
                                                    break;
                                                }
                                                mode = mode4;
                                                mode2 = mode3;
                                                break;
                                            } else {
                                                sb.append((char) iArr2[i2]);
                                                mode = mode4;
                                                mode2 = mode3;
                                                break;
                                            }
                                        } else {
                                            mode = mode3;
                                            mode2 = Mode.PUNCT_SHIFT;
                                            break;
                                        }
                                    } else {
                                        mode = mode4;
                                        mode2 = Mode.ALPHA;
                                        break;
                                    }
                                } else {
                                    mode = mode4;
                                    mode2 = Mode.LOWER;
                                    break;
                                }
                            } else {
                                c = ' ';
                                mode = mode4;
                                mode2 = mode3;
                                break;
                            }
                        } else {
                            mode = mode4;
                            mode2 = Mode.PUNCT;
                            break;
                        }
                    } else {
                        c = MIXED_CHARS[i3];
                        mode = mode4;
                        mode2 = mode3;
                        break;
                    }
                case PUNCT:
                    if (i3 >= 29) {
                        if (i3 != 29) {
                            if (i3 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                if (i3 == 900) {
                                    mode = mode4;
                                    mode2 = Mode.ALPHA;
                                    break;
                                }
                                mode = mode4;
                                mode2 = mode3;
                                break;
                            } else {
                                sb.append((char) iArr2[i2]);
                                mode = mode4;
                                mode2 = mode3;
                                break;
                            }
                        } else {
                            mode = mode4;
                            mode2 = Mode.ALPHA;
                            break;
                        }
                    } else {
                        c = PUNCT_CHARS[i3];
                        mode = mode4;
                        mode2 = mode3;
                        break;
                    }
                case ALPHA_SHIFT:
                    if (i3 >= 26) {
                        if (i3 != 26) {
                            if (i3 == 900) {
                                mode = mode4;
                                mode2 = Mode.ALPHA;
                                break;
                            }
                            mode = mode4;
                            mode2 = mode4;
                            break;
                        } else {
                            c = ' ';
                            mode = mode4;
                            mode2 = mode4;
                            break;
                        }
                    } else {
                        c = (char) (i3 + 65);
                        mode = mode4;
                        mode2 = mode4;
                        break;
                    }
                case PUNCT_SHIFT:
                    if (i3 >= 29) {
                        if (i3 != 29) {
                            if (i3 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                if (i3 == 900) {
                                    mode = mode4;
                                    mode2 = Mode.ALPHA;
                                    break;
                                }
                                mode = mode4;
                                mode2 = mode4;
                                break;
                            } else {
                                sb.append((char) iArr2[i2]);
                                mode = mode4;
                                mode2 = mode4;
                                break;
                            }
                        } else {
                            mode = mode4;
                            mode2 = Mode.ALPHA;
                            break;
                        }
                    } else {
                        c = PUNCT_CHARS[i3];
                        mode = mode4;
                        mode2 = mode4;
                        break;
                    }
                default:
                    mode = mode4;
                    mode2 = mode3;
                    break;
            }
            if (c != 0) {
                sb.append(c);
            }
            i2++;
            mode4 = mode;
            mode3 = mode2;
        }
    }

    private static int byteCompaction(int i, int[] iArr, Charset charset, int i2, StringBuilder sb) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (i == 901) {
            int i3 = 0;
            long j = 0;
            int[] iArr2 = new int[6];
            boolean z = false;
            int i4 = iArr[i2];
            int i5 = i2 + 1;
            while (i5 < iArr[0] && !z) {
                int i6 = i3 + 1;
                iArr2[i3] = i4;
                j = (j * 900) + ((long) i4);
                int i7 = i5 + 1;
                i4 = iArr[i5];
                if (i4 == 900 || i4 == 901 || i4 == 902 || i4 == BYTE_COMPACTION_MODE_LATCH_6 || i4 == 928 || i4 == BEGIN_MACRO_PDF417_OPTIONAL_FIELD || i4 == MACRO_PDF417_TERMINATOR) {
                    i5 = i7 - 1;
                    z = true;
                    i3 = i6;
                } else if (i6 % 5 != 0 || i6 <= 0) {
                    i3 = i6;
                    i5 = i7;
                } else {
                    for (int i8 = 0; i8 < 6; i8++) {
                        byteArrayOutputStream.write((byte) ((int) (j >> ((5 - i8) * 8))));
                    }
                    j = 0;
                    i3 = 0;
                    i5 = i7;
                }
            }
            if (i5 == iArr[0] && i4 < 900) {
                iArr2[i3] = i4;
                i3++;
            }
            for (int i9 = 0; i9 < i3; i9++) {
                byteArrayOutputStream.write((byte) iArr2[i9]);
            }
            i2 = i5;
        } else if (i == BYTE_COMPACTION_MODE_LATCH_6) {
            int i10 = 0;
            long j2 = 0;
            boolean z2 = false;
            while (i2 < iArr[0] && !z2) {
                int i11 = i2 + 1;
                int i12 = iArr[i2];
                if (i12 < 900) {
                    i10++;
                    j2 = (j2 * 900) + ((long) i12);
                    i2 = i11;
                } else if (i12 == 900 || i12 == 901 || i12 == 902 || i12 == BYTE_COMPACTION_MODE_LATCH_6 || i12 == 928 || i12 == BEGIN_MACRO_PDF417_OPTIONAL_FIELD || i12 == MACRO_PDF417_TERMINATOR) {
                    i2 = i11 - 1;
                    z2 = true;
                } else {
                    i2 = i11;
                }
                if (i10 % 5 == 0 && i10 > 0) {
                    for (int i13 = 0; i13 < 6; i13++) {
                        byteArrayOutputStream.write((byte) ((int) (j2 >> ((5 - i13) * 8))));
                    }
                    j2 = 0;
                    i10 = 0;
                }
            }
        }
        sb.append(new String(byteArrayOutputStream.toByteArray(), charset));
        return i2;
    }

    private static int numericCompaction(int[] iArr, int i, StringBuilder sb) {
        int[] iArr2 = new int[15];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i3 == iArr[0]) {
                z = true;
            }
            if (i4 < 900) {
                iArr2[i2] = i4;
                i2++;
                i = i3;
            } else if (i4 == 900 || i4 == 901 || i4 == BYTE_COMPACTION_MODE_LATCH_6 || i4 == 928 || i4 == BEGIN_MACRO_PDF417_OPTIONAL_FIELD || i4 == MACRO_PDF417_TERMINATOR) {
                i = i3 - 1;
                z = true;
            } else {
                i = i3;
            }
            if ((i2 % 15 == 0 || i4 == 902 || z) && i2 > 0) {
                sb.append(decodeBase900toBase10(iArr2, i2));
                i2 = 0;
            }
        }
        return i;
    }

    private static String decodeBase900toBase10(int[] iArr, int i) {
        BigInteger bigInteger = BigInteger.ZERO;
        for (int i2 = 0; i2 < i; i2++) {
            bigInteger = bigInteger.add(EXP900[(i - i2) - 1].multiply(BigInteger.valueOf((long) iArr[i2])));
        }
        String bigInteger2 = bigInteger.toString();
        if (bigInteger2.charAt(0) == '1') {
            return bigInteger2.substring(1);
        }
        throw FormatException.getFormatInstance();
    }
}
