package com.google.zxing.datamatrix.decoder;

import com.google.zxing.datamatrix.decoder.Version;

/* access modifiers changed from: package-private */
public final class DataBlock {
    private final byte[] codewords;
    private final int numDataCodewords;

    private DataBlock(int i, byte[] bArr) {
        this.numDataCodewords = i;
        this.codewords = bArr;
    }

    static DataBlock[] getDataBlocks(byte[] bArr, Version version) {
        int i;
        int i2;
        Version.ECBlocks eCBlocks = version.getECBlocks();
        Version.ECB[] eCBlocks2 = eCBlocks.getECBlocks();
        int i3 = 0;
        for (Version.ECB ecb : eCBlocks2) {
            i3 += ecb.getCount();
        }
        DataBlock[] dataBlockArr = new DataBlock[i3];
        int i4 = 0;
        for (Version.ECB ecb2 : eCBlocks2) {
            int i5 = 0;
            while (i5 < ecb2.getCount()) {
                int dataCodewords = ecb2.getDataCodewords();
                dataBlockArr[i4] = new DataBlock(dataCodewords, new byte[(eCBlocks.getECCodewords() + dataCodewords)]);
                i5++;
                i4++;
            }
        }
        int length = dataBlockArr[0].codewords.length - eCBlocks.getECCodewords();
        int i6 = length - 1;
        int i7 = 0;
        for (int i8 = 0; i8 < i6; i8++) {
            int i9 = 0;
            while (i9 < i4) {
                dataBlockArr[i9].codewords[i8] = bArr[i7];
                i9++;
                i7++;
            }
        }
        boolean z = version.getVersionNumber() == 24;
        if (z) {
            i = 8;
        } else {
            i = i4;
        }
        int i10 = 0;
        while (i10 < i) {
            dataBlockArr[i10].codewords[length - 1] = bArr[i7];
            i10++;
            i7++;
        }
        int length2 = dataBlockArr[0].codewords.length;
        int i11 = i7;
        while (length < length2) {
            int i12 = 0;
            int i13 = i11;
            while (i12 < i4) {
                if (z) {
                    i2 = (i12 + 8) % i4;
                } else {
                    i2 = i12;
                }
                dataBlockArr[i2].codewords[(!z || i2 <= 7) ? length : length - 1] = bArr[i13];
                i12++;
                i13++;
            }
            length++;
            i11 = i13;
        }
        if (i11 == bArr.length) {
            return dataBlockArr;
        }
        throw new IllegalArgumentException();
    }

    /* access modifiers changed from: package-private */
    public int getNumDataCodewords() {
        return this.numDataCodewords;
    }

    /* access modifiers changed from: package-private */
    public byte[] getCodewords() {
        return this.codewords;
    }
}
