package com.google.zxing.client.result;

import com.google.android.exoplayer.text.ttml.TtmlNode;
import com.google.zxing.Result;
import java.util.Map;
import java.util.regex.Pattern;

public final class EmailAddressResultParser extends ResultParser {
    private static final Pattern COMMA = Pattern.compile(",");

    @Override // com.google.zxing.client.result.ResultParser
    public EmailAddressParsedResult parse(Result result) {
        String[] strArr;
        String str;
        String str2;
        String[] strArr2;
        String[] strArr3;
        String[] strArr4;
        String str3;
        String[] strArr5 = null;
        String massagedText = getMassagedText(result);
        if (massagedText.startsWith("mailto:") || massagedText.startsWith("MAILTO:")) {
            String substring = massagedText.substring(7);
            int indexOf = substring.indexOf(63);
            if (indexOf >= 0) {
                substring = substring.substring(0, indexOf);
            }
            try {
                String urlDecode = urlDecode(substring);
                if (!urlDecode.isEmpty()) {
                    strArr = COMMA.split(urlDecode);
                } else {
                    strArr = null;
                }
                Map<String, String> parseNameValuePairs = parseNameValuePairs(massagedText);
                if (parseNameValuePairs != null) {
                    if (strArr != null || (str3 = parseNameValuePairs.get("to")) == null) {
                        strArr4 = strArr;
                    } else {
                        strArr4 = COMMA.split(str3);
                    }
                    String str4 = parseNameValuePairs.get("cc");
                    if (str4 != null) {
                        strArr3 = COMMA.split(str4);
                    } else {
                        strArr3 = null;
                    }
                    String str5 = parseNameValuePairs.get("bcc");
                    if (str5 != null) {
                        strArr5 = COMMA.split(str5);
                    }
                    str = parseNameValuePairs.get(TtmlNode.TAG_BODY);
                    str2 = parseNameValuePairs.get("subject");
                    strArr2 = strArr5;
                    strArr = strArr4;
                } else {
                    str = null;
                    str2 = null;
                    strArr2 = null;
                    strArr3 = null;
                }
                return new EmailAddressParsedResult(strArr, strArr3, strArr2, str2, str);
            } catch (IllegalArgumentException e) {
                return null;
            }
        } else if (EmailDoCoMoResultParser.isBasicallyValidEmailAddress(massagedText)) {
            return new EmailAddressParsedResult(massagedText);
        } else {
            return null;
        }
    }
}
