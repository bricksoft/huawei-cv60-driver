package ui_Controller.ui_Liveview;

import GeneralFunction.Player.player.SphericalVideoPlayer;
import GeneralFunction.Player.player.h;
import GeneralFunction.d;
import GeneralFunction.e;
import GeneralFunction.g;
import GeneralFunction.k;
import GeneralFunction.l.a;
import GeneralFunction.n;
import GeneralFunction.o;
import GeneralFunction.r.a;
import GeneralFunction.r.b;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.os.PowerManager;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.TextureView;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Space;
import android.widget.TextView;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.util.MimeTypes;
import com.google.api.client.http.HttpStatusCodes;
import com.huawei.cvIntl60.R;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import ui_Controller.CustomWidget.b;
import ui_Controller.a.c;
import ui_Controller.b.j;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class UI_LiveViewController extends GeneralFunction.a.a implements TextureView.SurfaceTextureListener {
    public static final String g = (Environment.getExternalStorageDirectory().getPath() + "/DCIM/VS8010W/2016_05_27/100_PICO/PIC_8005.JPG");
    private ImageButton A = null;
    private ImageButton B = null;
    private ImageButton C = null;
    private ImageButton D = null;
    private HorizontalScrollView E = null;
    private HorizontalScrollView F = null;
    private ImageButton G = null;
    private ImageButton H = null;
    private ImageButton I = null;
    private ImageButton J = null;
    private ImageButton K = null;
    private ImageButton L = null;
    private TextView M = null;
    private TextView N = null;
    private TextView O = null;
    private TextView P = null;
    private TextView Q = null;
    private TextView R = null;
    private LinearLayout S = null;
    private LinearLayout T = null;
    private LinearLayout U = null;
    private LinearLayout V = null;
    private LinearLayout W = null;
    private ArrayList<ImageButton> X = new ArrayList<>(5);
    private ArrayList<LinearLayout> Y = new ArrayList<>(5);
    private ImageButton Z = null;
    private LinearLayout aA = null;
    private LinearLayout aB = null;
    private ArrayList<ImageButton> aC = new ArrayList<>(9);
    private ArrayList<LinearLayout> aD = new ArrayList<>(9);
    private ImageButton aE = null;
    private LinearLayout aF = null;
    private LinearLayout aG = null;
    private LinearLayout aH = null;
    private LinearLayout aI = null;
    private LinearLayout aJ = null;
    private LinearLayout aK = null;
    private TextView aL = null;
    private TextView aM = null;
    private ImageButton aN = null;
    private ImageView aO = null;
    private LinearLayout aP = null;
    private ImageButton aQ = null;
    private LinearLayout aR = null;
    private ImageView aS = null;
    private ImageButton aT = null;
    private TextView aU = null;
    private SeekBar aV = null;
    private TextView aW = null;
    private TextView aX = null;
    private LinearLayout aY = null;
    private LinearLayout aZ = null;
    private ImageButton aa = null;
    private ImageButton ab = null;
    private ImageButton ac = null;
    private ImageButton ad = null;
    private ImageButton ae = null;
    private ImageButton af = null;
    private ImageButton ag = null;
    private ImageButton ah = null;
    private ImageButton ai = null;
    private TextView aj = null;
    private TextView ak = null;
    private TextView al = null;
    private TextView am = null;
    private TextView an = null;
    private TextView ao = null;
    private TextView ap = null;
    private TextView aq = null;
    private TextView ar = null;
    private TextView as = null;
    private LinearLayout at = null;
    private LinearLayout au = null;
    private LinearLayout av = null;
    private LinearLayout aw = null;
    private LinearLayout ax = null;
    private LinearLayout ay = null;
    private LinearLayout az = null;
    protected int b = 0;
    private ImageButton bA = null;
    private ImageButton bB = null;
    private ImageButton bC = null;
    private ImageButton bD = null;
    private ImageButton bE = null;
    private ImageButton bF = null;
    private ImageButton bG = null;
    private ImageButton bH = null;
    private TextView bI = null;
    private TextView bJ = null;
    private TextView bK = null;
    private TextView bL = null;
    private TextView bM = null;
    private TextView bN = null;
    private TextView bO = null;
    private TextView bP = null;
    private TextView bQ = null;
    private TextView bR = null;
    private TextView bS = null;
    private TextView bT = null;
    private LinearLayout bU = null;
    private TextView bV = null;
    private ArrayList<ImageButton> bW = null;
    private ArrayList<TextView> bX = null;
    private List<Integer> bY = null;
    private TabLayout bZ = null;
    private ImageButton ba = null;
    private ImageButton bb = null;
    private ImageView bc = null;
    private LinearLayout bd = null;
    private ImageView be = null;
    private LinearLayout bf = null;
    private TextView bg = null;
    private LinearLayout bh = null;
    private TextView bi = null;
    private ImageView bj = null;
    private LinearLayout bk = null;
    private ImageView bl = null;
    private LinearLayout bm = null;
    private LinearLayout bn = null;
    private ImageView bo = null;
    private TextView bp = null;
    private ImageButton bq = null;
    private ImageButton br = null;
    private ImageButton bs = null;
    private TextView bt = null;
    private LinearLayout bu = null;
    private Button bv = null;
    private ImageButton bw = null;
    private ImageButton bx = null;
    private ImageButton by = null;
    private ImageButton bz = null;
    protected UI_ModeMain c = null;
    private TextView cA = null;
    private ImageButton cB = null;
    private ImageButton cC = null;
    private LinearLayout cD = null;
    private ImageButton cE = null;
    private ImageButton cF = null;
    private TextView cG = null;
    private ArrayList<String> cH = null;
    private int[] cI = null;
    private LinearLayout cJ = null;
    private View.OnClickListener cK = new View.OnClickListener() {
        /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass56 */

        public void onClick(View view) {
            if (UI_LiveViewController.this.k((UI_LiveViewController) view.getId())) {
                UI_LiveViewController.this.a(false);
                UI_LiveViewController.this.e(view.getId());
            }
        }
    };
    private SphericalVideoPlayer.f cL = new SphericalVideoPlayer.f() {
        /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass64 */

        @Override // GeneralFunction.Player.player.SphericalVideoPlayer.f
        public void a(int i) {
            UI_LiveViewController.this.a((UI_LiveViewController) "returnVideoStatus", (String) 3);
            if (UI_LiveViewController.this.f != null) {
                a.c.a aVar = new a.c.a(8718);
                aVar.a("sphericalVideoPlayerStatus", i);
                UI_LiveViewController.this.a(aVar, 0L);
            }
        }
    };
    private b.a cM = new b.a() {
        /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass65 */
    };
    private View.OnClickListener cN = new View.OnClickListener() {
        /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass11 */

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.IB_ui_live_edit_menu_topBack:
                    if (UI_LiveViewController.this.cD.isShown()) {
                        UI_LiveViewController.this.X();
                    }
                    UI_LiveViewController.this.e(false);
                    UI_LiveViewController.this.bZ.a(UI_LiveViewController.this.d.h.ae).e();
                    return;
                case R.id.IB_ui_live_edit_menu_topConfirm:
                    if (UI_LiveViewController.this.cD.isShown()) {
                        UI_LiveViewController.this.X();
                    }
                    UI_LiveViewController.this.au();
                    UI_LiveViewController.this.e(false);
                    UI_LiveViewController.this.bZ.a(UI_LiveViewController.this.d.h.ae).e();
                    return;
                case R.id.IB_ui_live_setting_menu_topBack:
                    UI_LiveViewController.this.f(false);
                    return;
                case R.id.IB_ui_live_setting_submenu_topBack:
                    UI_LiveViewController.this.f(false);
                    UI_LiveViewController.this.g(false);
                    return;
                default:
                    return;
            }
        }
    };
    private View.OnClickListener cO = new View.OnClickListener() {
        /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass48 */

        public void onClick(View view) {
            for (int i = 0; i < UI_LiveViewController.this.X.size(); i++) {
                ((LinearLayout) UI_LiveViewController.this.Y.get(i)).setBackgroundResource(0);
                ((LinearLayout) UI_LiveViewController.this.Y.get(i)).setBackgroundColor(-15263977);
            }
            switch (view.getId()) {
                case R.id.IB_WB_ButtonTitle:
                    ((LinearLayout) UI_LiveViewController.this.Y.get(0)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.n(0);
                    UI_LiveViewController.this.E.smoothScrollTo(UI_LiveViewController.this.E.getWidth() * UI_LiveViewController.this.E.getLayoutDirection(), 0);
                    break;
                case R.id.IB_WB_Button1:
                    ((LinearLayout) UI_LiveViewController.this.Y.get(0)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.n(0);
                    break;
                case R.id.IB_WB_Button2:
                    ((LinearLayout) UI_LiveViewController.this.Y.get(1)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.n(1);
                    break;
                case R.id.IB_WB_Button3:
                    ((LinearLayout) UI_LiveViewController.this.Y.get(2)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.n(2);
                    break;
                case R.id.IB_WB_Button4:
                    ((LinearLayout) UI_LiveViewController.this.Y.get(3)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.n(3);
                    break;
                case R.id.IB_WB_Button5:
                    ((LinearLayout) UI_LiveViewController.this.Y.get(4)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.n(4);
                    break;
            }
            if (view.getId() == R.id.IB_WB_ButtonTitle || view.getId() == R.id.IB_WB_Button1) {
                UI_LiveViewController.this.G.setImageResource(R.drawable.shooting_effectmenu_wb_white);
                UI_LiveViewController.this.M.setTextColor(UI_LiveViewController.this.getResources().getColor(R.color.white));
            } else {
                UI_LiveViewController.this.G.setImageResource(R.drawable.shooting_effectmenu_icon_wb_green);
                UI_LiveViewController.this.M.setTextColor(UI_LiveViewController.this.getResources().getColor(R.color.iconcolor));
            }
            UI_LiveViewController.this.a(8758, 0L);
        }
    };
    private View.OnClickListener cP = new View.OnClickListener() {
        /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass49 */

        public void onClick(View view) {
            for (int i = 0; i < UI_LiveViewController.this.aC.size(); i++) {
                ((LinearLayout) UI_LiveViewController.this.aD.get(i)).setBackgroundColor(-15263977);
                ((LinearLayout) UI_LiveViewController.this.aD.get(i)).setBackgroundResource(0);
            }
            switch (view.getId()) {
                case R.id.IB_Filter_ButtonTitle:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(0)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(0);
                    UI_LiveViewController.this.F.smoothScrollTo(UI_LiveViewController.this.F.getWidth() * UI_LiveViewController.this.F.getLayoutDirection(), 0);
                    break;
                case R.id.IB_Filter_Button1:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(0)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(0);
                    break;
                case R.id.IB_Filter_Button2:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(1)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(1);
                    break;
                case R.id.IB_Filter_Button3:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(2)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(2);
                    break;
                case R.id.IB_Filter_Button4:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(3)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(3);
                    break;
                case R.id.IB_Filter_Button5:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(4)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(4);
                    break;
                case R.id.IB_Filter_Button6:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(5)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(5);
                    break;
                case R.id.IB_Filter_Button7:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(6)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(6);
                    break;
                case R.id.IB_Filter_Button8:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(7)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(7);
                    break;
                case R.id.IB_Filter_Button9:
                    ((LinearLayout) UI_LiveViewController.this.aD.get(8)).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
                    GeneralFunction.m.a.o(8);
                    break;
            }
            if (view.getId() == R.id.IB_Filter_ButtonTitle || view.getId() == R.id.IB_Filter_Button1) {
                UI_LiveViewController.this.Z.setImageResource(R.drawable.shooting_effectmenu_filter_white);
                UI_LiveViewController.this.aj.setTextColor(UI_LiveViewController.this.getResources().getColor(R.color.white));
            } else {
                UI_LiveViewController.this.Z.setImageResource(R.drawable.shooting_effectmenu_icon_filter_green);
                UI_LiveViewController.this.aj.setTextColor(UI_LiveViewController.this.getResources().getColor(R.color.iconcolor));
            }
            UI_LiveViewController.this.a(8759, 0L);
        }
    };
    private b.a cQ = new b.a() {
        /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass62 */

        @Override // GeneralFunction.r.b.a
        public void a(boolean z) {
            UI_LiveViewController.this.a((UI_LiveViewController) ("OnMuxerInitial: " + z), (String) 3);
            a.c.a aVar = new a.c.a(8742);
            aVar.a("muxerInitResult", z);
            UI_LiveViewController.this.a(aVar);
        }

        @Override // GeneralFunction.r.b.a
        public void a(long j) {
            UI_LiveViewController.this.c.c.h.aC = j / 1000;
            UI_LiveViewController.this.a(8736);
        }

        @Override // GeneralFunction.r.b.a
        public void a(String str) {
            GeneralFunction.e.b bVar;
            UI_LiveViewController.this.a((UI_LiveViewController) "*****Save file done*****", (String) 3);
            UI_LiveViewController.this.aZ();
            UI_LiveViewController.this.a((UI_LiveViewController) ("Video durarion2: " + ((UI_LiveViewController.this.d.h.aD - UI_LiveViewController.this.d.h.aC) / 1000)), (String) 3);
            if (UI_LiveViewController.this.c.a() != 305) {
                UI_LiveViewController.this.a((UI_LiveViewController) ("Current mode not correct: " + UI_LiveViewController.this.c.a()), (String) 1);
            } else if (UI_LiveViewController.this.c.c.h.S == 5) {
                UI_LiveViewController.this.a(8727);
            } else {
                if (str != null) {
                    File file = new File(c.f);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    try {
                        GeneralFunction.g.a.c(UI_LiveViewController.this.c.c.k.b.f1320a.e(UI_LiveViewController.this.d.k.e.c(), 0), c.f + "QuickView.jpg");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    UI_LiveViewController.this.ag();
                } else {
                    UI_LiveViewController.this.d.k.e.a(UI_LiveViewController.this.d.k.e.c(), 0);
                    UI_LiveViewController.this.c.x();
                    if (GeneralFunction.m.a.a() == 1) {
                        bVar = UI_LiveViewController.this.d.k.g;
                    } else {
                        bVar = UI_LiveViewController.this.d.k.f;
                    }
                    UI_LiveViewController.this.d.k.b.f1320a.a(UI_LiveViewController.this.d.k.e.a(), bVar);
                }
                UI_LiveViewController.this.a(8727);
            }
        }
    };
    private TextInputLayout ca = null;
    private EditText cb = null;
    private TextView cc = null;
    private Space cd = null;
    private TextInputLayout ce = null;
    private EditText cf = null;
    private TextView cg = null;
    private LinearLayout ch = null;
    private LinearLayout ci = null;
    private a cj = null;
    private b ck = null;
    private ListView cl = null;
    private ListView cm = null;
    private Integer[] cn = null;
    private Integer[] co = null;
    private int[] cp = null;
    private ArrayList<String> cq = null;
    private ArrayList<String> cr = null;
    private ArrayList<Integer> cs = null;
    private String[] ct = null;
    private ImageButton cu = null;
    private ImageButton cv = null;
    private TextView cw = null;
    private LinearLayout cx = null;
    private LinearLayout cy = null;
    private LinearLayout cz = null;
    protected j d = null;
    protected PowerManager.WakeLock e = null;
    public SphericalVideoPlayer f = null;
    protected LinearLayout h = null;
    protected LinearLayout i = null;
    protected ImageView j = null;
    protected a.AbstractC0003a k = new a.AbstractC0003a() {
        /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass45 */

        @Override // GeneralFunction.l.a.AbstractC0003a
        public void a(GeneralFunction.l.c cVar) {
            UI_LiveViewController.this.a((UI_LiveViewController) ("ShareResultCallback result: " + cVar.f122a), (String) 3);
            UI_LiveViewController.this.d.h.aL.a((a.AbstractC0003a) null);
            UI_LiveViewController.this.d.h.aL = null;
        }
    };
    private boolean l = false;
    private int m = 0;
    private int n = 0;
    private int o = -1;
    private int p = -1;
    private b q = null;
    private a r = null;
    private ImageButton s = null;
    private LinearLayout t = null;
    private ImageView u = null;
    private ImageButton v = null;
    private ImageButton w = null;
    private ImageView x = null;
    private ImageButton y = null;
    private LinearLayout z = null;

    /* access modifiers changed from: protected */
    public void c() {
        this.d.h.E.a();
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.d.h.E.b();
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.d.h.G.a();
    }

    /* access modifiers changed from: protected */
    public void f() {
        this.d.h.G.b();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        d.a("UI_LiveViewController", str, i2);
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return this.l;
    }

    @Override // GeneralFunction.a.a
    public void a(int i2, long j2) {
        if (this.c != null) {
            this.c.a(new a.c.a(i2), j2);
        } else {
            a("SendUiMsg error!! Msg=" + i2, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(a.c.a aVar, long j2) {
        if (this.c != null) {
            this.c.a(aVar, j2);
        } else {
            a("SendUiMsg error!! MsgEx=" + aVar.b().what, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(int i2, long j2) {
        if (this.c != null) {
            this.c.b(i2, j2);
        } else {
            a("UI_SendAidMsg error!!", 0);
        }
    }

    @Override // GeneralFunction.a.a
    public boolean b() {
        if (this.o == 2) {
            return true;
        }
        return false;
    }

    @Override // GeneralFunction.a.a
    public void b(Message message) {
        a("AidServer_Handler:0x" + Integer.toHexString(message.what), 3);
        this.q.j(message);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a("onCreate", 0);
        this.l = true;
        this.c = (UI_ModeMain) getApplication();
        this.c.v();
        m(true);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.m = Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.n = Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.o = 1;
        setContentView(R.layout.ui_liveview_portrait);
        this.q = new b(this);
        this.r = new a(this);
        this.d = this.c.c;
        this.c.a(272, this);
        this.c.f1526a = getPreferences(0);
        this.c.b = this.c.f1526a.edit();
        this.c.c.h.d = 288;
        this.c.c.h.u = 0;
        an();
        aY();
        t();
        b(true, false);
        b(true, getResources().getString(R.string.connecting));
        ay();
        am();
        if (this.c.c.k.b.f1320a.g(this.d.k.e.c(), 0) == null) {
            q();
            b(20508);
        }
        bd();
        a(12033, 200L);
    }

    private void am() {
        registerReceiver(this.c.c.h.aq, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (GeneralFunction.m.a.t() && GeneralFunction.m.a.u()) {
            i();
        }
        GeneralFunction.m.a.v();
    }

    /* access modifiers changed from: protected */
    public void i() {
        this.d.k.b.q = new GeneralFunction.a(getApplicationContext(), true, getResources().getString(R.string.sdcard_removed), 2000);
        GeneralFunction.m.a.a(0);
        GeneralFunction.n.a.b(this.c).putInt("storeLocation", GeneralFunction.m.a.a()).apply();
        bc();
        this.d.k.b.x = true;
    }

    /* access modifiers changed from: protected */
    public void j() {
        SharedPreferences.Editor b2 = GeneralFunction.n.a.b(this.c);
        b2.putInt("currentMode", this.c.c.h.T).apply();
        b2.putInt("pipMode", this.c.c.h.U).apply();
        b2.putInt("liveCurrentMode", this.c.c.h.ae).apply();
    }

    private void an() {
        this.c.c.h.ae = GeneralFunction.n.a.a(this.c).getInt("liveCurrentMode", 0);
        this.c.c.h.T = GeneralFunction.n.a.a(this.c).getInt("currentMode", 0);
        this.c.c.h.U = GeneralFunction.n.a.a(this.c).getInt("pipMode", 0);
        GeneralFunction.m.a.a(GeneralFunction.n.a.a(this.c).getInt("storeLocation", 0));
        GeneralFunction.m.a.b(GeneralFunction.n.a.a(this.c).getInt("videoResolution", 0));
        GeneralFunction.m.a.c(GeneralFunction.n.a.a(this.c).getInt("photoResolution", 0));
        GeneralFunction.m.a.d(GeneralFunction.n.a.a(this.c).getInt("liveViewMode", 0));
        GeneralFunction.m.a.e(GeneralFunction.n.a.a(this.c).getInt("recordingTime", 0));
        GeneralFunction.m.a.f(GeneralFunction.n.a.a(this.c).getInt("getSelfTimer", 0));
        GeneralFunction.m.a.g(GeneralFunction.n.a.a(this.c).getInt("pipView", 1));
        GeneralFunction.m.a.h(GeneralFunction.n.a.a(this.c).getInt("shutterSound", 0));
        GeneralFunction.m.a.i(GeneralFunction.n.a.a(this.c).getInt("waterMark", 0));
        this.d.l.n = GeneralFunction.n.a.a(this.c).getBoolean("haveNewApkVersion", false);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.c.v();
        o.a();
        if (!this.c.d.b()) {
            b(true, true);
            a(true, getResources().getString(R.string.no_camera));
            b(false, "");
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        a("onNewIntent", 0);
        this.l = true;
        this.c.a(272, this);
        setIntent(intent);
        this.c.c.h.av = null;
        a.c.a aVar = new a.c.a(12041);
        aVar.a("mode", 256);
        a(aVar, 0L);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onResume() {
        super.onResume();
        a("onResume", 0);
        this.l = true;
        if (this.c.c.h.aI != null) {
            a(true, this.c.c.h.aI);
        }
        if (!this.c.d.d() && this.c.a() != 288) {
            K();
        }
        a.c.a aVar = new a.c.a(12034);
        aVar.a("mode", 256);
        a(aVar, 0L);
        this.c.c.h.ap.enable();
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (!z2 && this.f != null && this.c.c.h.d != 305 && this.c.c.h.d != 304 && this.c.c.h.aJ) {
            int d2 = this.f.d();
            SphericalVideoPlayer sphericalVideoPlayer = this.f;
            if (d2 == 2) {
                this.c.c.h.aI = this.f.getBitmap();
                this.c.c.h.aJ = false;
            }
        }
    }

    @Override // GeneralFunction.a.a
    public void onPause() {
        super.onPause();
        a("onPause", 0);
        this.l = false;
        if (!(this.f == null || this.c.c.h.d == 305 || this.c.c.h.d == 304 || !this.c.c.h.aJ)) {
            int d2 = this.f.d();
            SphericalVideoPlayer sphericalVideoPlayer = this.f;
            if (d2 == 2) {
                this.c.c.h.aI = this.f.getBitmap();
                this.c.c.h.aJ = false;
            }
        }
        a.c.a aVar = new a.c.a(12035);
        aVar.a("mode", 256);
        a(aVar, 0L);
        this.c.c.h.ap.disable();
    }

    @Override // GeneralFunction.a.a
    public void onStop() {
        super.onStop();
        this.l = false;
        a.c.a aVar = new a.c.a(12036);
        aVar.a("mode", 256);
        a(aVar, 0L);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onDestroy() {
        super.onDestroy();
        a("onDestroy", 0);
        this.l = false;
        this.c.c.f = true;
        a.c.a aVar = new a.c.a(12037);
        aVar.a("mode", 256);
        a(aVar, 0L);
        try {
            unregisterReceiver(this.c.c.h.aq);
        } catch (IllegalArgumentException e2) {
            a("Exception: unregister battery info receiver failed!", 0);
        }
        ba();
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        a("onSurfaceTextureAvailable:" + i2 + " " + i3, 3);
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        a("onSurfaceTextureDestroyed", 3);
        return false;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        a("onSurfaceTextureSizeChanged:" + i2 + " " + i3, 3);
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    @Override // GeneralFunction.a.a
    public void a(Message message) {
        this.q.a(message);
    }

    /* access modifiers changed from: protected */
    public void c(int i2) {
        this.f.setFrameRate(i2);
    }

    /* access modifiers changed from: protected */
    public void a(a.a.a.d dVar) {
        this.f.a(dVar.f169a, dVar.c, dVar.d);
    }

    /* access modifiers changed from: protected */
    public boolean k() {
        if (this.f == null || this.f.d() != 2) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean l() {
        if (this.h == null || !this.h.isShown()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        if (z2) {
            m();
            this.i.setVisibility(0);
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.h, "translationY", 1600.0f, 0.0f);
            ofFloat.setDuration(500L);
            ofFloat.start();
            this.h.setVisibility(0);
            return;
        }
        this.i.setVisibility(4);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.h, "translationY", 0.0f, 1600.0f);
        ofFloat2.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass1 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                UI_LiveViewController.this.h.setVisibility(4);
            }
        });
        ofFloat2.setDuration(500L);
        ofFloat2.start();
    }

    private List<Integer> ao() {
        ArrayList arrayList = new ArrayList();
        this.d.k.b.af = d.b(this);
        for (int i2 = 0; i2 < this.d.k.b.af.size(); i2++) {
            switch (this.d.k.b.af.get(i2).intValue()) {
                case 1:
                case 3:
                case 7:
                case 8:
                case 9:
                    arrayList.add(this.d.k.b.af.get(i2));
                    break;
            }
        }
        return arrayList;
    }

    private void ap() {
        this.h = (LinearLayout) findViewById(R.id.LL_Liveview_shareLayout);
        this.i = (LinearLayout) findViewById(R.id.LL_Liveview_blank_shareLayout);
        a(false);
        this.j = (ImageView) findViewById(R.id.IB_Liveview_btnShareCancel);
        this.j.setImageResource(R.drawable.system_close_white);
        this.bw = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button1);
        this.bx = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button2);
        this.by = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button3);
        this.bz = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button4);
        this.bA = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button5);
        this.bB = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button6);
        this.bC = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button7);
        this.bD = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button8);
        this.bE = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button9);
        this.bF = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button10);
        this.bG = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button11);
        this.bH = (ImageButton) findViewById(R.id.IB_Liveview_Share_Button12);
        this.bw.setOnClickListener(this.cK);
        this.bx.setOnClickListener(this.cK);
        this.by.setOnClickListener(this.cK);
        this.bz.setOnClickListener(this.cK);
        this.bA.setOnClickListener(this.cK);
        this.bB.setOnClickListener(this.cK);
        this.bC.setOnClickListener(this.cK);
        this.bD.setOnClickListener(this.cK);
        this.bE.setOnClickListener(this.cK);
        this.bF.setOnClickListener(this.cK);
        this.bG.setOnClickListener(this.cK);
        this.bH.setOnClickListener(this.cK);
        this.bI = (TextView) findViewById(R.id.TV_Liveview_Share_Button1);
        this.bJ = (TextView) findViewById(R.id.TV_Liveview_Share_Button2);
        this.bK = (TextView) findViewById(R.id.TV_Liveview_Share_Button3);
        this.bL = (TextView) findViewById(R.id.TV_Liveview_Share_Button4);
        this.bM = (TextView) findViewById(R.id.TV_Liveview_Share_Button5);
        this.bN = (TextView) findViewById(R.id.TV_Liveview_Share_Button6);
        this.bO = (TextView) findViewById(R.id.TV_Liveview_Share_Button7);
        this.bP = (TextView) findViewById(R.id.TV_Liveview_Share_Button8);
        this.bQ = (TextView) findViewById(R.id.TV_Liveview_Share_Button9);
        this.bR = (TextView) findViewById(R.id.TV_Liveview_Share_Button10);
        this.bS = (TextView) findViewById(R.id.TV_Liveview_Share_Button11);
        this.bT = (TextView) findViewById(R.id.TV_Liveview_Share_Button12);
        int a2 = k.a(this, (float) (this.n / 50));
        this.bI.setTextSize((float) a2);
        this.bJ.setTextSize((float) a2);
        this.bK.setTextSize((float) a2);
        this.bL.setTextSize((float) a2);
        this.bM.setTextSize((float) a2);
        this.bN.setTextSize((float) a2);
        this.bO.setTextSize((float) a2);
        this.bP.setTextSize((float) a2);
        this.bQ.setTextSize((float) a2);
        this.bR.setTextSize((float) a2);
        this.bS.setTextSize((float) a2);
        this.bT.setTextSize((float) a2);
        this.bW = new ArrayList<>();
        this.bW.add(this.bw);
        this.bW.add(this.bx);
        this.bW.add(this.by);
        this.bW.add(this.bz);
        this.bW.add(this.bA);
        this.bW.add(this.bB);
        this.bW.add(this.bC);
        this.bW.add(this.bD);
        this.bW.add(this.bE);
        this.bW.add(this.bF);
        this.bW.add(this.bG);
        this.bW.add(this.bH);
        this.bX = new ArrayList<>();
        this.bX.add(this.bI);
        this.bX.add(this.bJ);
        this.bX.add(this.bK);
        this.bX.add(this.bL);
        this.bX.add(this.bM);
        this.bX.add(this.bN);
        this.bX.add(this.bO);
        this.bX.add(this.bP);
        this.bX.add(this.bQ);
        this.bX.add(this.bR);
        this.bX.add(this.bS);
        this.bX.add(this.bT);
        this.bU = (LinearLayout) findViewById(R.id.LL_Liveview_SNSLayout);
        this.bV = (TextView) findViewById(R.id.TV_Liveview_share_no_sns);
        this.i.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass12 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    UI_LiveViewController.this.a(false);
                }
                if (motionEvent.getAction() == 2) {
                }
                if (motionEvent.getAction() == 1) {
                }
                return true;
            }
        });
        this.h.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass23 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                }
                if (motionEvent.getAction() == 2) {
                }
                if (motionEvent.getAction() == 1) {
                }
                return true;
            }
        });
        this.j.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass34 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                }
                if (motionEvent.getAction() == 2) {
                }
                if (motionEvent.getAction() == 1) {
                    UI_LiveViewController.this.a(false);
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void d(int i2) {
        GeneralFunction.l.b bVar = new GeneralFunction.l.b();
        bVar.f = this.d.h.aH;
        bVar.c = 12288;
        bVar.b = 4096;
        switch (i2) {
            case 1:
                bVar.f121a = 8192;
                break;
            case 2:
                bVar.f121a = 8201;
                break;
            case 3:
                bVar.f121a = 8197;
                break;
            case 4:
                bVar.f121a = 8199;
                break;
            case 5:
                bVar.f121a = 8200;
                break;
            case 7:
                bVar.f121a = 8194;
                break;
            case 8:
                bVar.f121a = 8202;
                break;
            case 9:
                bVar.f121a = 8195;
                break;
            case 11:
                bVar.f121a = 8203;
                break;
        }
        this.d.h.aL = new GeneralFunction.l.a();
        this.d.h.aL.a(this);
        this.d.h.aL.a(this.k);
        this.d.h.aL.a(bVar, this);
    }

    /* access modifiers changed from: protected */
    public void e(int i2) {
        switch (i2) {
            case R.id.IB_Liveview_Share_Button1:
                if (this.bY.size() > 0) {
                    d(this.bY.get(0).intValue());
                    return;
                }
                return;
            case R.id.TV_Liveview_Share_Button1:
            case R.id.TV_Liveview_Share_Button2:
            case R.id.TV_Liveview_Share_Button3:
            case R.id.TV_Liveview_Share_Button4:
            case R.id.TV_Liveview_Share_Button5:
            case R.id.TV_Liveview_Share_Button6:
            case R.id.TV_Liveview_Share_Button7:
            case R.id.TV_Liveview_Share_Button8:
            case R.id.TV_Liveview_Share_Button9:
            case R.id.TV_Liveview_Share_Button10:
            case R.id.TV_Liveview_Share_Button11:
            default:
                return;
            case R.id.IB_Liveview_Share_Button2:
                if (this.bY.size() > 1) {
                    d(this.bY.get(1).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button3:
                if (this.bY.size() > 2) {
                    d(this.bY.get(2).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button4:
                if (this.bY.size() > 3) {
                    d(this.bY.get(3).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button5:
                if (this.bY.size() > 4) {
                    d(this.bY.get(4).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button6:
                if (this.bY.size() > 5) {
                    d(this.bY.get(5).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button7:
                if (this.bY.size() > 6) {
                    d(this.bY.get(6).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button8:
                if (this.bY.size() > 7) {
                    d(this.bY.get(7).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button9:
                if (this.bY.size() > 8) {
                    d(this.bY.get(8).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button10:
                if (this.bY.size() > 9) {
                    d(this.bY.get(9).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button11:
                if (this.bY.size() > 10) {
                    d(this.bY.get(10).intValue());
                    return;
                }
                return;
            case R.id.IB_Liveview_Share_Button12:
                if (this.bY.size() > 11) {
                    d(this.bY.get(11).intValue());
                    return;
                }
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean k(int i2) {
        for (int i3 = 0; i3 < this.c.c.h.aT; i3++) {
            if (i2 == this.bW.get(i3).getId()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void m() {
        this.bY = ao();
        this.c.c.h.aT = this.bY.size();
        for (int i2 = 0; i2 < 5; i2++) {
            this.bW.get(i2).setImageResource(0);
            this.bX.get(i2).setText("");
        }
        if (this.bY.size() == 0) {
            this.bU.setVisibility(8);
            this.bV.setVisibility(0);
            return;
        }
        this.bU.setVisibility(0);
        this.bV.setVisibility(8);
        for (int i3 = 0; i3 < this.bY.size(); i3++) {
            switch (this.bY.get(i3).intValue()) {
                case 0:
                    this.bW.get(i3).setImageResource(R.drawable.gallery_share_youtube_english);
                    this.bX.get(i3).setText(getResources().getString(R.string.share_sns_youtube));
                    break;
                case 1:
                    this.bW.get(i3).setImageResource(R.drawable.gallery_share_fb_english);
                    this.bX.get(i3).setText(getResources().getString(R.string.share_sns_facebook));
                    break;
                case 7:
                    this.bW.get(i3).setImageResource(R.drawable.gallery_share_line_english);
                    this.bX.get(i3).setText(getResources().getString(R.string.share_sns_line));
                    break;
                case 8:
                    this.bW.get(i3).setImageResource(R.drawable.gallery_share_whatsapp_english);
                    this.bX.get(i3).setText(getResources().getString(R.string.share_sns_whatsapp));
                    break;
                case 9:
                    this.bW.get(i3).setImageResource(R.drawable.gallery_share_twitter_english);
                    this.bX.get(i3).setText(getResources().getString(R.string.share_sns_twitter));
                    break;
                case 11:
                    this.bW.get(i3).setImageResource(R.drawable.gallery_share_youku_english);
                    this.bX.get(i3).setText(getResources().getString(R.string.share_sns_youku));
                    break;
                case 12:
                    this.bW.get(i3).setImageResource(R.drawable.gallery_share_vk_english);
                    this.bX.get(i3).setText(getResources().getString(R.string.share_sns_vk));
                    break;
                case 13:
                    this.bW.get(i3).setImageResource(R.drawable.gallery_share_snapchat_english);
                    this.bX.get(i3).setText(getResources().getString(R.string.share_sns_snapchat));
                    break;
                case 14:
                    this.bW.get(i3).setImageResource(R.drawable.gallery_share_instagram_english);
                    this.bX.get(i3).setText(getResources().getString(R.string.share_sns_instagram));
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void n() {
        if (this.f != null) {
            a("liveviewSphericalVideoPlayer != null", 0);
            this.f = null;
        } else {
            a("liveviewSphericalVideoPlayer == null", 0);
        }
        this.f = (SphericalVideoPlayer) findViewById(R.id.TV_ui_liveview_h264LiveView);
        this.f.setVisibility(0);
        a("h: " + this.f.getHeight() + " w: " + this.f.getWidth(), 3);
    }

    /* access modifiers changed from: protected */
    public void o() {
        if (this.c.d.b()) {
            if (this.c.c.h.av == null) {
                a("objActivity.objModeMain.gsUI.sView.sCurrentSphericalParameter == null", 3);
                this.c.c.h.av = new h();
            }
            if (this.f != null) {
                this.c.c.h.av.s = this.f.getSphericalParameter().s;
                this.c.c.h.av.m = this.f.getSphericalParameter().m;
                this.c.c.h.av.l = this.f.getSphericalParameter().l;
                this.c.c.h.av.f51a = 3;
                this.c.c.h.av.q = this.f.getSphericalParameter().q;
                this.c.c.h.av.r = this.f.getSphericalParameter().r;
            }
            a("get objActivity.objModeMain.gsUI.sView.sCurrentSphericalParameter.playerFormat: " + this.c.c.h.av.m, 3);
            a("get objActivity.objModeMain.gsUI.sView.sCurrentSphericalParameter.viewType: " + this.c.c.h.av.f51a, 3);
            return;
        }
        this.c.c.h.av = null;
    }

    /* access modifiers changed from: protected */
    public void p() {
        a("init360LiveView", 4);
        n();
        this.f.f();
        a("GetPlayerStatus(): " + this.f.d(), 3);
        this.f.setPlayerStatusListener(this.cL);
    }

    /* access modifiers changed from: protected */
    public void q() {
        String[] list;
        File file = new File(c.f);
        if (file.isDirectory() && (list = file.list()) != null) {
            for (String str : list) {
                new File(file, str).delete();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void f(int i2) {
        switch (i2) {
            case 49:
                a(true, true);
                return;
            case 50:
                this.c.b(268435455L);
                a(8818, 0L);
                return;
            case 51:
            case 52:
            default:
                return;
            case 53:
                this.c.b(268435455L);
                this.d.h.k = false;
                g(64);
                a(false, true);
                return;
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"DefaultLocale"})
    public void a(boolean z2, int i2) {
        a("recordTime: " + i2, 3);
        double d2 = (this.c.c.h.aE / 1024.0d) / 1024.0d;
        double d3 = (((double) this.c.c.h.z) / 1024.0d) / 1024.0d;
        if (z2) {
            this.aL.setText(String.format("%02d:%02d", Integer.valueOf((i2 / 60) % 60), Integer.valueOf(i2 % 60)));
            ObjectAnimator.ofFloat(this.aO, "alpha", 0.0f, 1.0f).setDuration(1000L).start();
            DecimalFormat decimalFormat = new DecimalFormat("#.#");
            String format = decimalFormat.format(d2 - d3);
            if (d2 - d3 <= 0.0d) {
                format = String.format("%d", 0);
                a("dlStorageSizeBeforRecording: " + d2, 3);
                a("dlAvailableMemorySize: " + d3, 3);
            }
            this.aM.setText(format + "MB / " + decimalFormat.format(d3 / 1024.0d) + "GB");
            if (i2 == r(GeneralFunction.m.a.e())) {
                a(8725);
            }
        } else {
            this.aL.setText(String.format("%02d:%02d", Integer.valueOf((i2 / 60) % 60), Integer.valueOf(i2 % 60)));
            DecimalFormat decimalFormat2 = new DecimalFormat("#.#");
            this.aM.setText(decimalFormat2.format(d2 - d3) + "MB / " + decimalFormat2.format(d3 / 1024.0d) + "GB");
        }
        if (ah()) {
            a(8778);
        }
    }

    /* access modifiers changed from: protected */
    public void r() {
        double a2;
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        if (GeneralFunction.m.a.a() == 0) {
            a2 = (double) d.a(c.f1314a, 0);
        } else {
            a2 = (double) d.a(this.c.c.k.b.ab, 0);
        }
        this.aM.setText(String.format("%d", 0) + "MB / " + decimalFormat.format(Double.parseDouble(Double.toString(((a2 / 1024.0d) / 1024.0d) / 1024.0d))) + "GB");
    }

    /* access modifiers changed from: protected */
    public void g(int i2) {
        switch (i2) {
            case 64:
                this.d.h.b = 262;
                b(true);
                return;
            case 65:
                this.d.h.b = 256;
                b(false);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void b(boolean z2) {
        if (z2) {
            this.t.setAlpha(0.7f);
            this.t.setBackgroundColor(getResources().getColor(R.color.black));
            this.t.setVisibility(0);
            ((AnimationDrawable) this.u.getDrawable()).start();
            return;
        }
        this.t.setBackgroundColor(getResources().getColor(R.color.transparent));
        this.t.setVisibility(4);
        ((AnimationDrawable) this.u.getDrawable()).stop();
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2, boolean z3) {
        if (z2) {
            if (z3) {
                this.d.h.h = 0;
            }
            this.d.h.A = new Timer();
            this.d.h.A.scheduleAtFixedRate(new TimerTask() {
                /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass66 */

                public void run() {
                    UI_LiveViewController.this.d.h.h++;
                    UI_LiveViewController.this.a(8825);
                }
            }, 1000, 1000);
            return;
        }
        a("*****lPlayTime: " + this.d.h.h, 0);
        this.d.h.aB = this.d.h.h;
        if (z3) {
            this.d.h.h = 0;
        }
        if (this.d.h.A != null) {
            this.d.h.n = false;
            this.d.h.A.cancel();
            this.d.h.A = null;
        }
    }

    /* access modifiers changed from: protected */
    public void s() {
        a("ReleaseAndUnlockAllLiveViewFrame_S", 3);
        c();
        d();
        a("ReleaseAndUnlockAllLiveViewFrame_E", 3);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        a("onActivityResult requestCode:" + i2, 3);
        this.c.h().a(i2, i3, intent);
        super.onActivityResult(i2, i3, intent);
    }

    /* access modifiers changed from: protected */
    public void t() {
        p();
        az();
        aN();
        aQ();
        aH();
        aM();
        aJ();
        aR();
        aI();
        aK();
        aS();
        aO();
        ar();
        aT();
        aA();
        aP();
        ax();
        aq();
        av();
        at();
        ap();
    }

    private void aq() {
        this.bu = (LinearLayout) findViewById(R.id.LL_ui_Liveview_FailLayout);
        this.bv = (Button) findViewById(R.id.B_ui_Liveview_Fail_Button);
        this.bv.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass2 */

            public void onClick(View view) {
                UI_LiveViewController.this.c(false);
            }
        });
        this.bu.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass3 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void c(boolean z2) {
        if (z2) {
            this.bu.setVisibility(0);
        } else {
            this.bu.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public boolean u() {
        if (this.bu == null || !this.bu.isShown()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean v() {
        if (this.cD == null || !this.cD.isShown()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean w() {
        if (this.ch == null || !this.ch.isShown()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean x() {
        if (this.ci == null || !this.ci.isShown()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean y() {
        if (this.ch != null && this.ch.isShown()) {
            return true;
        }
        if (this.ci != null) {
            return this.ci.isShown();
        }
        return false;
    }

    private void ar() {
        this.bn = (LinearLayout) findViewById(R.id.LL_Broadcasting_Layout);
        this.bo = (ImageView) findViewById(R.id.IV_Broadcasting_Icon);
        this.bp = (TextView) findViewById(R.id.TV_Current_Broadcasting_Time);
        this.bq = (ImageButton) findViewById(R.id.IB_ui_Broadcast_Stop_Button);
        this.br = (ImageButton) findViewById(R.id.IB_ui_Broadcast_Share_Button);
        if (this.d.h.ae == 0) {
            this.br.setVisibility(4);
        } else {
            this.br.setVisibility(0);
        }
        this.bs = (ImageButton) findViewById(R.id.IB_ui_Broadcast_Pause_Play_Button);
        this.bt = (TextView) findViewById(R.id.TV_ui_LiveSettingText_fix);
        this.bt.setText(this.cH.get(GeneralFunction.m.a.m()));
        this.bn.setVisibility(4);
        this.bn.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass4 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.bs.setImageResource(R.drawable.shooting_shutterkey_live_pause);
        this.br.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass5 */

            public void onClick(View view) {
                if (UI_LiveViewController.this.d.h.c == 299 || UI_LiveViewController.this.d.h.c == 300) {
                    UI_LiveViewController.this.a(8792, 0L);
                } else {
                    UI_LiveViewController.this.a((UI_LiveViewController) "error case of btnYoutubeShare ", (String) 4);
                }
            }
        });
        this.bq.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass6 */

            public void onClick(View view) {
                if (UI_LiveViewController.this.d.h.c != 299 && UI_LiveViewController.this.d.h.c != 300) {
                    UI_LiveViewController.this.a((UI_LiveViewController) "error case of btnYoutubeStop ", (String) 4);
                } else if (UI_LiveViewController.this.c.c.h.ae == 1) {
                    o.a((Context) UI_LiveViewController.this, true, false, UI_LiveViewController.this.getResources().getString(R.string.broadcast_check_stop_title), UI_LiveViewController.this.getResources().getString(R.string.broadcast_check_stop_description), new String[]{UI_LiveViewController.this.getResources().getString(R.string.dialog_option_no), UI_LiveViewController.this.getResources().getString(R.string.dialog_option_yes)}, new int[]{0, 8785}, true);
                } else {
                    o.a((Context) UI_LiveViewController.this, true, false, UI_LiveViewController.this.getResources().getString(R.string.broadcast_check_stop_title), UI_LiveViewController.this.getResources().getString(R.string.broadcast_check_stop_description), new String[]{UI_LiveViewController.this.getResources().getString(R.string.dialog_option_no), UI_LiveViewController.this.getResources().getString(R.string.dialog_option_yes)}, new int[]{0, 8796}, true);
                }
            }
        });
        this.bs.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass7 */

            public void onClick(View view) {
                UI_LiveViewController.this.aE();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void z() {
        this.bs.setImageResource(R.drawable.shooting_shutterkey_live_pause);
    }

    /* access modifiers changed from: protected */
    public void d(boolean z2) {
        if (z2) {
            ObjectAnimator.ofFloat(this.bn, "translationY", 500.0f, 0.0f).setDuration(300L).start();
            this.bn.setVisibility(0);
            this.d.h.h = (int) (this.d.h.O / 30);
            this.bp.setText(String.format("%02d:%02d", Integer.valueOf((this.d.h.h / 60) % 60), Integer.valueOf(this.d.h.h % 60)));
            a(true, false);
            return;
        }
        b(false, "");
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.bn, "translationY", 0.0f, 500.0f);
        ofFloat.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass8 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                UI_LiveViewController.this.bn.setVisibility(4);
            }
        });
        ofFloat.setDuration(300L);
        ofFloat.start();
        a(false, false);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"DefaultLocale"})
    public void b(boolean z2, int i2) {
        String format;
        a("broadcastTime: " + i2, 3);
        if (z2) {
            if (i2 >= 3600) {
                format = String.format("%d:%02d:%02d", Integer.valueOf(i2 / 3600), Integer.valueOf((i2 / 60) % 60), Integer.valueOf(i2 % 60));
            } else {
                format = String.format("%02d:%02d", Integer.valueOf((i2 / 60) % 60), Integer.valueOf(i2 % 60));
            }
            this.bp.setText(format);
            ObjectAnimator.ofFloat(this.bo, "alpha", 0.0f, 1.0f).setDuration(800L).start();
            return;
        }
        this.bp.setText("00:00");
    }

    /* access modifiers changed from: protected */
    public class a extends BaseAdapter {
        private int[] b;
        private List<String> c;
        private String[] d;
        private List<Integer> e;

        public a(Context context, List<String> list, String[] strArr, int[] iArr, List<Integer> list2) {
            this.c = list;
            this.b = iArr;
            this.d = strArr;
            this.e = list2;
        }

        public void a() {
            notifyDataSetChanged();
        }

        public int getCount() {
            return this.e.size();
        }

        public Object getItem(int i) {
            return this.c.get((int) getItemId(i));
        }

        public long getItemId(int i) {
            return (long) this.e.get(i).intValue();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = LayoutInflater.from(UI_LiveViewController.this.getApplicationContext()).inflate(R.layout.setting_menu, (ViewGroup) null);
            ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_menu_listview_text)).getLayoutParams().height = UI_LiveViewController.this.n / 11;
            ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_menu_listview_icon)).getLayoutParams().height = UI_LiveViewController.this.n / 11;
            ((ImageView) inflate.findViewById(R.id.setting_menu_list_icon)).setImageResource(this.b[i]);
            ((TextView) inflate.findViewById(R.id.textTitle)).setText(this.c.get(i));
            ((TextView) inflate.findViewById(R.id.textDetail)).setText(this.d[i]);
            return inflate;
        }
    }

    /* access modifiers changed from: protected */
    public class b extends BaseAdapter {
        private int[] b;
        private List<String> c;
        private List<Integer> d;

        public b(Context context, List<String> list, int[] iArr, List<Integer> list2) {
            this.c = list;
            this.b = iArr;
            this.d = list2;
        }

        public void a() {
            notifyDataSetChanged();
        }

        public void a(int i, View view, ViewGroup viewGroup) {
            for (int i2 = 0; i2 < getCount(); i2++) {
                if (i2 == i) {
                    ((ImageView) view.findViewById(R.id.setting_submenu_list_icon)).setImageResource(R.drawable.settings_radio_on);
                } else {
                    ((ImageView) viewGroup.getChildAt(i2).findViewById(R.id.setting_submenu_list_icon)).setImageResource(R.drawable.settings_radio_off);
                }
            }
        }

        public int getCount() {
            return this.d.size();
        }

        public Object getItem(int i) {
            return this.c.get((int) getItemId(i));
        }

        public long getItemId(int i) {
            return (long) this.d.get(i).intValue();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = LayoutInflater.from(UI_LiveViewController.this.getApplicationContext()).inflate(R.layout.setting_submenu, (ViewGroup) null);
            ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_submenu_listview_text)).getLayoutParams().height = UI_LiveViewController.this.n / 11;
            ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_submenu_listview_icon)).getLayoutParams().height = UI_LiveViewController.this.n / 11;
            ((TextView) inflate.findViewById(R.id.textDetail)).setText(this.c.get(i));
            ((ImageView) inflate.findViewById(R.id.setting_submenu_list_icon)).setImageResource(this.b[i]);
            return inflate;
        }
    }

    private void as() {
        this.cp = new int[]{R.drawable.settings_radio_off, R.drawable.settings_radio_off};
    }

    public void A() {
        this.ct = new String[this.cq.size()];
        this.ct[0] = this.cr.get(GeneralFunction.m.a.m());
    }

    private void at() {
        as();
        int[] iArr = {R.drawable.settings_arrow};
        this.cq = new ArrayList<>();
        this.cq.add(getResources().getString(R.string.bitrate));
        this.cr = new ArrayList<>();
        this.cr.add(getResources().getString(R.string.mbps_4));
        this.cr.add(getResources().getString(R.string.mbps_8));
        A();
        this.cn = new Integer[]{0};
        this.co = new Integer[]{0, 1};
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(this.cn));
        this.ch = (LinearLayout) findViewById(R.id.LL_ui_live_setting_menu);
        this.cl = (ListView) findViewById(R.id.LV_ui_live_setting_menu);
        this.cu = (ImageButton) findViewById(R.id.IB_ui_live_setting_menu_topBack);
        this.cu.setOnClickListener(this.cN);
        this.cj = new a(this, this.cq, this.ct, iArr, arrayList);
        this.cl.setAdapter((ListAdapter) this.cj);
        this.cl.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass9 */

            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                UI_LiveViewController.this.c.c.h.aO = i;
                if (i == 0) {
                    UI_LiveViewController.this.l((UI_LiveViewController) i);
                    ObjectAnimator ofFloat = ObjectAnimator.ofFloat(UI_LiveViewController.this.ci, "translationX", 1600.0f, 0.0f);
                    ofFloat.addListener(new AnimatorListenerAdapter() {
                        /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass9.AnonymousClass1 */

                        public void onAnimationEnd(Animator animator) {
                            super.onAnimationEnd(animator);
                            UI_LiveViewController.this.f(false);
                            UI_LiveViewController.this.p((UI_LiveViewController) false);
                        }
                    });
                    ofFloat.setDuration(300L);
                    UI_LiveViewController.this.p((UI_LiveViewController) true);
                    if (UI_LiveViewController.this.c.d.b()) {
                        ofFloat.start();
                        UI_LiveViewController.this.g(true);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void l(int i2) {
        this.cs = new ArrayList<>();
        switch (i2) {
            case 0:
                this.cs.addAll(Arrays.asList(this.co));
                this.cp[GeneralFunction.m.a.m()] = R.drawable.settings_radio_on;
                this.ck = new b(this, this.cr, this.cp, this.cs);
                break;
        }
        this.ci = (LinearLayout) findViewById(R.id.LL_ui_live_setting_submenu);
        this.cm = (ListView) findViewById(R.id.LV_ui_live_setting_submenu);
        this.cv = (ImageButton) findViewById(R.id.IB_ui_live_setting_submenu_topBack);
        this.cv.setOnClickListener(this.cN);
        this.cw = (TextView) findViewById(R.id.TV_ui_live_setting_submenu_title);
        this.cw.setText(this.cq.get(i2));
        this.cm.setAdapter((ListAdapter) this.ck);
        this.cm.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass10 */

            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                UI_LiveViewController.this.c.c.h.aP = i;
                UI_LiveViewController.this.ck.a(i, view, adapterView);
                UI_LiveViewController.this.B();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void B() {
        a("lLiveSelectOption: " + this.c.c.h.aO + "  lLiveSubSelectOption: " + this.c.c.h.aP, 3);
        switch (this.c.c.h.aO) {
            case 0:
                GeneralFunction.m.a.j(this.c.c.h.aP);
                this.cG.setText(this.cH.get(GeneralFunction.m.a.m()));
                this.bt.setText(this.cH.get(GeneralFunction.m.a.m()));
                this.ct[this.c.c.h.aO] = this.cr.get(this.c.c.h.aP);
                for (int i2 = 0; i2 < this.cp.length; i2++) {
                    if (i2 == this.c.c.h.aP) {
                        this.cp[i2] = R.drawable.settings_radio_on;
                    } else {
                        this.cp[i2] = R.drawable.settings_radio_off;
                    }
                }
                g(false);
                f(true);
                break;
        }
        this.cj.a();
        if (this.ck != null) {
            this.ck.a();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void au() {
        this.d.h.ae = this.bZ.getSelectedTabPosition();
        this.cF.setImageResource(this.cI[this.d.h.ae]);
        if (this.d.h.ae == 0) {
            this.br.setVisibility(4);
        } else {
            this.br.setVisibility(0);
        }
        n(this.d.h.ae);
    }

    private void av() {
        this.cA = (TextView) findViewById(R.id.TV_ui_Live_Title_livePlatform);
        int a2 = k.a(this, (float) (this.m / 26));
        this.cA.setTextSize((float) new GeneralFunction.c(a2).f((int) (((double) a2) * 0.9d)).a());
        this.cB = (ImageButton) findViewById(R.id.IB_ui_live_edit_menu_topBack);
        this.cB.setOnClickListener(this.cN);
        this.cC = (ImageButton) findViewById(R.id.IB_ui_live_edit_menu_topConfirm);
        this.cC.setOnClickListener(this.cN);
        aw();
        this.cD = (LinearLayout) findViewById(R.id.LL_ui_Liveview_EditLayout);
        this.cb = (EditText) findViewById(R.id.ET_ui_Live_Title_EditText);
        this.cc = (TextView) findViewById(R.id.TV_ui_Live_Title_EditCounter);
        this.cd = (Space) findViewById(R.id.S_ui_Live_Space);
        this.cf = (EditText) findViewById(R.id.ET_ui_Live_Description_EditText);
        this.cg = (TextView) findViewById(R.id.TV_ui_Live_Description_EditCounter);
        this.bZ.a(this.d.h.ae).e();
        m(this.d.h.ae);
        this.cD.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass13 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                UI_LiveViewController.this.X();
                UI_LiveViewController.this.cD.setFocusable(true);
                UI_LiveViewController.this.cD.setFocusableInTouchMode(true);
                return true;
            }
        });
        this.cb.addTextChangedListener(new TextWatcher() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass14 */
            private int b = 100;

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                UI_LiveViewController.this.a(editable, UI_LiveViewController.this.cb, this, this.b, UI_LiveViewController.this.cc);
            }
        });
        this.cf.addTextChangedListener(new TextWatcher() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass15 */
            private int b = 250;

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                UI_LiveViewController.this.a(editable, UI_LiveViewController.this.cf, this, this.b, UI_LiveViewController.this.cg);
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(Editable editable, EditText editText, TextWatcher textWatcher, int i2, TextView textView) {
        int selectionStart = editText.getSelectionStart();
        int selectionEnd = editText.getSelectionEnd();
        editText.removeTextChangedListener(textWatcher);
        if (!TextUtils.isEmpty(editText.getText())) {
            while (c(editable.toString()) > i2) {
                editable.delete(selectionStart - 1, selectionEnd);
                selectionStart--;
                selectionEnd--;
            }
        }
        textView.setText(Integer.toString(c(editable.toString())) + "/" + i2);
        editText.setSelection(selectionStart);
        editText.addTextChangedListener(textWatcher);
    }

    private int c(String str) {
        try {
            str.getBytes("GBK");
            return str.length();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    private void aw() {
        this.ca = (TextInputLayout) findViewById(R.id.TIL_ui_Live_Title_TextInputLayout);
        this.ce = (TextInputLayout) findViewById(R.id.TIL_ui_Live_Description_TextInputLayout);
        this.bZ = (TabLayout) findViewById(R.id.TL_ui_Live_Select_TabLayout);
        this.bZ.setTabGravity(0);
        this.bZ.a(this.bZ.a().d(R.string.share_sns_facebook));
        this.bZ.a(this.bZ.a().d(R.string.share_sns_youtube));
        this.bZ.a(0).c(this.cI[0]);
        this.bZ.a(1).c(this.cI[1]);
        this.bZ.a(new TabLayout.c() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass16 */

            @Override // android.support.design.widget.TabLayout.b
            public void a(TabLayout.f fVar) {
                UI_LiveViewController.this.m((UI_LiveViewController) fVar.c());
            }

            @Override // android.support.design.widget.TabLayout.b
            public void b(TabLayout.f fVar) {
            }

            @Override // android.support.design.widget.TabLayout.b
            public void c(TabLayout.f fVar) {
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void m(int i2) {
        if (i2 == 0) {
            this.cf.setText(this.d.h.aQ);
            this.ca.setVisibility(8);
            this.cc.setVisibility(8);
            this.cd.setVisibility(8);
            return;
        }
        this.cb.setText(this.d.h.aR);
        this.cf.setText(this.d.h.aS);
        this.ca.setVisibility(0);
        this.cc.setVisibility(0);
        this.cd.setVisibility(0);
        this.cb.requestFocus();
    }

    /* access modifiers changed from: protected */
    public a C() {
        return this.r;
    }

    private void ax() {
        this.bk = (LinearLayout) findViewById(R.id.LL_Recordindg_Capturing_Process_Layout);
        this.bl = (ImageView) findViewById(R.id.IV_ui_Recordindg_Capturing_Process_View);
        this.bm = (LinearLayout) findViewById(R.id.LL_Recordindg_Capturing_Thumbnail_Layout);
        this.bk.setVisibility(4);
        this.bk.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass17 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void a(int i2, boolean z2) {
        if (z2) {
            this.aE.setImageBitmap(null);
            if (i2 == 0) {
                this.bl.setImageResource(R.drawable.shooting_shutterkey_photo);
            } else if (i2 == 1) {
                this.bl.setImageResource(R.drawable.shooting_shutterkey_video);
            }
            this.bm.setBackgroundColor(getResources().getColor(17170444));
            this.bk.setVisibility(0);
            j(z2);
            return;
        }
        this.bk.setVisibility(4);
        this.bl.setImageResource(0);
        j(z2);
    }

    private void ay() {
        this.c.c.h.ar = 100;
        this.c.c.h.aq = new BroadcastReceiver() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass18 */

            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                char c = 65535;
                switch (action.hashCode()) {
                    case -1538406691:
                        if (action.equals("android.intent.action.BATTERY_CHANGED")) {
                            c = 0;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        int i = UI_LiveViewController.this.c.c.h.ar;
                        UI_LiveViewController.this.c.c.h.ar = intent.getIntExtra("level", 0);
                        UI_LiveViewController.this.a((UI_LiveViewController) ("Current battery level: " + UI_LiveViewController.this.c.c.h.ar + "%"), (String) 3);
                        if (UI_LiveViewController.this.c.c.h.ar <= 10 && i > 10) {
                            UI_LiveViewController.this.a((UI_LiveViewController) "lPhoneBatteryLevel<= 10", (String) 3);
                            if (!UI_LiveViewController.this.c.d.b()) {
                                return;
                            }
                            if (UI_LiveViewController.this.c.a() == 336 || UI_LiveViewController.this.c.a() == 416) {
                                UI_LiveViewController.this.a(8797);
                                return;
                            } else {
                                UI_LiveViewController.this.a(8726);
                                return;
                            }
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            }
        };
    }

    private void az() {
        this.bf = (LinearLayout) findViewById(R.id.LL_ui_Warning_Msg_Layout);
        this.bg = (TextView) findViewById(R.id.TV_ui_Warning_Msg_Content);
        this.bf.setVisibility(4);
    }

    private void aA() {
        this.aZ = (LinearLayout) findViewById(R.id.LL_ui_No_Connection_Layout);
        this.ba = (ImageButton) findViewById(R.id.IB_ui_ThumbnailButton_Noconnection);
        this.bb = (ImageButton) findViewById(R.id.IB_ui_SettingButton_No_Connection);
        this.bc = (ImageView) findViewById(R.id.IV_ui_LiveView_No_Connection);
        this.aY = (LinearLayout) findViewById(R.id.LL_ModeSlider_No_Connection);
        this.aY.setX((float) (((this.m * 420) / 960) / 3));
        this.bd = (LinearLayout) findViewById(R.id.LL_ui_No_Connection_Line_Layout);
        this.be = (ImageView) findViewById(R.id.IV_ui_No_Connection_Line);
        this.aZ.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass19 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.bb.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass20 */

            public void onClick(View view) {
                UI_LiveViewController.this.a(8454);
            }
        });
        this.ba.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass21 */

            public void onClick(View view) {
                UI_LiveViewController.this.a(8452);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void b(boolean z2, boolean z3) {
        if (z2) {
            if (this.c.c.l.n) {
                this.bb.setImageResource(R.drawable.shooting_settings_red);
            } else {
                this.bb.setImageResource(R.drawable.shooting_settings_white);
            }
            if (!this.c.d.b() || this.c.c.h.aI == null) {
                this.bc.setImageResource(R.drawable.shooting_nocamera_re);
            } else {
                this.bc.setImageBitmap(this.c.c.h.aI);
            }
            b(20508);
            this.aZ.setVisibility(0);
            if (z3) {
                this.be.setImageResource(R.drawable.shooting_nocamera_outline);
                this.bd.setVisibility(0);
                return;
            }
            this.be.setImageResource(0);
            this.bd.setVisibility(4);
            return;
        }
        this.bb.setImageResource(0);
        this.bc.setImageResource(0);
        this.aZ.setVisibility(4);
        this.be.setImageResource(0);
        this.bd.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public boolean D() {
        if (this.aZ.isShown()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean E() {
        if (!this.bh.isShown() || this.bi.getText().equals(getResources().getString(R.string.broadcast_initialize_busy)) || this.bi.getText().equals(getResources().getString(R.string.broadcast_wait_active))) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean F() {
        if (this.bh.isShown()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2, String str) {
        if (!z2) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.bf, "translationY", 0.0f, -100.0f);
            ofFloat.addListener(new AnimatorListenerAdapter() {
                /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass24 */

                public void onAnimationEnd(Animator animator) {
                    super.onAnimationEnd(animator);
                    if (UI_LiveViewController.this.bf != null) {
                        UI_LiveViewController.this.bf.setVisibility(4);
                    }
                }
            });
            ofFloat.setDuration(250L);
            ofFloat.start();
        } else if (!this.bg.getText().equals(str) || this.bf.getVisibility() != 0) {
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.bf, "translationY", -100.0f, 0.0f);
            ofFloat2.addListener(new AnimatorListenerAdapter() {
                /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass22 */

                public void onAnimationEnd(Animator animator) {
                    super.onAnimationEnd(animator);
                    if (UI_LiveViewController.this.bf != null) {
                        UI_LiveViewController.this.bf.setVisibility(0);
                    }
                }
            });
            ofFloat2.setDuration(300L);
            ofFloat2.start();
            this.bf.setVisibility(0);
            this.bg.setText(str);
        } else {
            a("No need to update warning layout", 2);
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap G() {
        GeneralFunction.e.b bVar;
        String m2;
        a("BARRY PATH: " + this.c.c.k.b.f1320a.h(this.d.k.e.c(), 0), 3);
        if (GeneralFunction.m.a.a() == 1) {
            bVar = this.d.k.g;
        } else {
            bVar = this.d.k.f;
        }
        ArrayList<GeneralFunction.e.a> arrayList = bVar.f74a;
        this.d.k.b.f1320a.a(this.d.k.e.a(), bVar);
        if (arrayList.size() == 0) {
            return null;
        }
        if (this.c.c.k.b.f1320a.h(arrayList.get(0).b, 0) == null) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        Bitmap bitmap = null;
        for (int i2 = 0; i2 < 5; i2++) {
            String o2 = this.c.c.k.b.f1320a.o(i2);
            if (o2 != null && new File(o2).exists() && (bitmap = BitmapFactory.decodeFile((m2 = this.c.c.k.b.f1320a.m(i2)), options)) != null) {
                a("THM_GroupIndex: " + i2 + "; thm path: " + m2, 0);
                return bitmap;
            }
        }
        return bitmap;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (new File(str).exists()) {
            Bitmap decodeFile = BitmapFactory.decodeFile(str);
            Bitmap a2 = a(decodeFile, decodeFile.getHeight());
            decodeFile.recycle();
            this.aE.setImageBitmap(a2);
            this.ba.setImageBitmap(a2);
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.aE, "alpha", 0.0f, 1.0f);
            ofFloat.setDuration(300L);
            ofFloat.start();
            return;
        }
        this.aE.setImageResource(R.drawable.shooting_thumbnail_empty);
        this.ba.setImageResource(R.drawable.shooting_thumbnail_empty);
    }

    /* access modifiers changed from: protected */
    public void H() {
        this.bm.setBackgroundColor(getResources().getColor(17170445));
        a(c.f + "QuickView.jpg");
    }

    /* access modifiers changed from: protected */
    public void a(Bitmap bitmap) {
        this.aE.setImageBitmap(null);
        this.ba.setImageBitmap(null);
        if (bitmap != null) {
            this.aE.setImageBitmap(bitmap);
            this.ba.setImageBitmap(bitmap);
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.aE, "alpha", 0.0f, 1.0f);
            ofFloat.setDuration(500L);
            ofFloat.start();
        } else {
            this.aE.setImageResource(R.drawable.shooting_thumbnail_empty);
            this.ba.setImageResource(R.drawable.shooting_thumbnail_empty);
        }
        this.d.k.b.f1320a.a(this.d.k.e.a(), this.d.k.f);
    }

    /* access modifiers changed from: protected */
    public void I() {
        this.w.setEnabled(false);
        this.aE.setEnabled(false);
        this.c.c.h.w = true;
    }

    /* access modifiers changed from: protected */
    public void J() {
        a("enable all leave button", 1);
        this.w.setEnabled(true);
        this.aE.setEnabled(true);
        this.c.c.h.w = false;
    }

    /* access modifiers changed from: protected */
    public void K() {
        this.v.setEnabled(false);
        this.D.setEnabled(false);
        I();
    }

    /* access modifiers changed from: protected */
    public void L() {
        this.v.setEnabled(true);
        this.D.setEnabled(true);
        J();
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2, Bitmap bitmap) {
        if (z2) {
            this.x.setImageBitmap(bitmap);
            this.x.setVisibility(0);
            return;
        }
        this.x.setVisibility(4);
        this.x.setImageBitmap(bitmap);
    }

    /* access modifiers changed from: protected */
    public boolean M() {
        return this.x.isShown();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void aB() {
        if (this.c.a() == 400) {
            a(8790);
        }
        if (this.c.c.h.T == 0) {
            if (this.c.c.h.Z) {
                return;
            }
            if (GeneralFunction.m.a.f() != 0) {
                V();
                k(true);
                this.c.c.h.Z = true;
                return;
            }
            this.c.c.h.Z = false;
            I();
            a(8775);
        } else if (this.c.c.h.T == 1) {
            if (!P()) {
                a(8800);
            } else if (this.c.c.h.Z) {
            } else {
                if (GeneralFunction.m.a.f() != 0) {
                    this.v.setEnabled(false);
                    V();
                    k(true);
                    this.c.c.h.Z = true;
                    return;
                }
                this.c.c.h.Z = false;
                a(8774);
            }
        } else if (!P()) {
            a(8800);
        } else if (this.c.c.h.ae == 0) {
            a(8795);
        } else {
            a(8798);
        }
    }

    /* access modifiers changed from: protected */
    public void N() {
        for (String str : c.s) {
            if (ActivityCompat.checkSelfPermission(this, str) == -1) {
                ActivityCompat.requestPermissions(this, c.s, 300);
                return;
            }
        }
        a(8799);
    }

    /* access modifiers changed from: protected */
    public void O() {
        for (String str : c.t) {
            if (ActivityCompat.checkSelfPermission(this, str) == -1) {
                ActivityCompat.requestPermissions(this, c.t, 400);
                return;
            }
        }
    }

    public boolean P() {
        if (ActivityCompat.checkSelfPermission(this, "android.permission.RECORD_AUDIO") != -1) {
            return true;
        }
        a("Manifest.permission.RECORD_AUDIO false", 0);
        return false;
    }

    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        a("onRequestPermissionsResult() " + i2, 4);
        if (strArr.length != 0) {
            for (int i3 = 0; i3 < strArr.length; i3++) {
                if (iArr[i3] != 0) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, strArr[i3])) {
                        if (i2 == 300) {
                            o.a((Context) this, true, false, getResources().getString(R.string.warning), getResources().getString(R.string.permission_deny_msg), new String[]{getResources().getString(R.string.dialog_option_cancel), getResources().getString(R.string.dialog_option_ok)}, new int[]{0, 8798});
                            return;
                        }
                        o.a((Context) this, true, false, getResources().getString(R.string.warning), getResources().getString(R.string.permission_deny_msg), new String[]{getResources().getString(R.string.dialog_option_cancel), getResources().getString(R.string.dialog_option_ok)}, new int[]{0, 8800});
                        return;
                    } else if (i2 == 300) {
                        o.a((Context) this, true, false, getResources().getString(R.string.warning), getResources().getString(R.string.permission_always_deny_msg), getResources().getString(R.string.dialog_option_ok), 0);
                        return;
                    } else if (ActivityCompat.checkSelfPermission(this, "android.permission.RECORD_AUDIO") == -1) {
                        o.a((Context) this, true, false, getResources().getString(R.string.warning), getResources().getString(R.string.permission_always_deny_msg), getResources().getString(R.string.dialog_option_ok), 0);
                        return;
                    } else {
                        return;
                    }
                }
            }
        }
        if (i2 == 300) {
            a(8799);
        }
    }

    /* access modifiers changed from: protected */
    public void Q() {
        if (this.c.h().g().d()) {
            a(8784);
        } else {
            o.a((Context) this, true, true, getResources().getString(R.string.google_play_service_error_title), getResources().getString(R.string.google_play_service_error_description), getResources().getString(R.string.dialog_option_ok), 12039);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void aC() {
        if (this.c.c.h.T == 0) {
            if (this.c.c.h.Z) {
                j(true);
                this.c.c.h.ab.end();
                this.c.c.h.Z = false;
            }
        } else if (this.c.c.h.T == 1 && this.c.c.h.Z) {
            this.c.c.h.ab.end();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void aD() {
        a(8778);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void aE() {
        if (this.d.h.c == 299) {
            this.bs.setImageResource(R.drawable.shooting_shutterkey_live_play);
            this.d.h.c = 300;
            if (this.d.h.an != null) {
                this.d.h.an.a(true);
            }
            if (this.c.c.h.ae == 1) {
                a(10768);
            }
            a(false, false);
        } else if (this.d.h.c == 300) {
            this.bs.setImageResource(R.drawable.shooting_shutterkey_live_pause);
            this.d.h.c = 299;
            if (this.d.h.an != null) {
                this.d.h.an.a(false);
            }
            if (this.c.c.h.ae == 1) {
                a(10769);
            }
            a(true, false);
        }
    }

    private void aF() {
        this.cH = new ArrayList<>();
        this.cH.add(getResources().getString(R.string.mbps_4_1280_640));
        this.cH.add(getResources().getString(R.string.mbps_8_1280_640));
    }

    private void aG() {
        this.cI = new int[]{R.drawable.shooting_sns_fb, R.drawable.shooting_sns_youtube};
    }

    private void aH() {
        aF();
        aG();
        this.cx = (LinearLayout) findViewById(R.id.LL_Live_SelectLayout);
        this.cy = (LinearLayout) findViewById(R.id.LL_EffectAndASetting);
        this.cz = (LinearLayout) findViewById(R.id.LL_Live_SettingLayout);
        this.cF = (ImageButton) findViewById(R.id.IB_ui_LiveButton);
        this.cF.setImageResource(this.cI[this.c.c.h.ae]);
        this.cE = (ImageButton) findViewById(R.id.IB_ui_LiveSettingButton);
        this.cG = (TextView) findViewById(R.id.TV_ui_LiveSettingText);
        this.cG.setText(this.cH.get(GeneralFunction.m.a.m()));
        this.x = (ImageView) findViewById(R.id.IV_ui_TempLiveView);
        this.v = (ImageButton) findViewById(R.id.IB_ui_CaptureButton);
        this.w = (ImageButton) findViewById(R.id.IB_ui_SettingButton);
        if (this.c.c.l.n) {
            this.w.setImageResource(R.drawable.shooting_settings_red);
        } else {
            this.w.setImageResource(R.drawable.shooting_settings_white);
        }
        this.D = (ImageButton) findViewById(R.id.IB_ui_EffectToolButton);
        this.aE = (ImageButton) findViewById(R.id.IB_ui_ThumbnailButton);
        this.aE.setImageResource(R.drawable.shooting_thumbnail_empty);
        I();
        a(false, (Bitmap) null);
        this.aE.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass25 */

            public void onClick(View view) {
                UI_LiveViewController.this.a(8452);
            }
        });
        this.v.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass26 */

            public void onClick(View view) {
                UI_LiveViewController.this.aB();
            }
        });
        this.v.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass27 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    if (UI_LiveViewController.this.c.c.h.T == 0) {
                        if (GeneralFunction.m.a.f() == 0) {
                            UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_photo);
                        } else {
                            UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_photo_selftimer);
                        }
                    } else if (UI_LiveViewController.this.c.c.h.T != 1) {
                        UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_live);
                    } else if (GeneralFunction.m.a.f() == 0) {
                        UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_video);
                    } else {
                        UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_video_selftimer);
                    }
                }
                if (motionEvent.getAction() != 2 || (motionEvent.getX() >= 0.0f && motionEvent.getX() <= ((float) UI_LiveViewController.this.v.getWidth()) && motionEvent.getY() >= 0.0f && motionEvent.getY() <= ((float) UI_LiveViewController.this.v.getHeight()))) {
                    if (motionEvent.getAction() == 1) {
                        if (UI_LiveViewController.this.c.c.h.T == 0) {
                            if (GeneralFunction.m.a.f() == 0) {
                                UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_photo);
                            } else {
                                UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_photo_selftimer);
                            }
                        } else if (UI_LiveViewController.this.c.c.h.T != 1) {
                            UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_live);
                        } else if (GeneralFunction.m.a.f() == 0) {
                            UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_video);
                        } else {
                            UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_video_selftimer);
                        }
                    }
                    return false;
                }
                if (UI_LiveViewController.this.c.c.h.T == 0) {
                    if (GeneralFunction.m.a.f() == 0) {
                        UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_photo);
                    } else {
                        UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_photo_selftimer);
                    }
                } else if (UI_LiveViewController.this.c.c.h.T != 1) {
                    UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_live);
                } else if (GeneralFunction.m.a.f() == 0) {
                    UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_video);
                } else {
                    UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_video_selftimer);
                }
                return false;
            }
        });
        this.D.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass28 */

            public void onClick(View view) {
                if (UI_LiveViewController.this.c.a() == 272 || UI_LiveViewController.this.c.a() == 400) {
                    UI_LiveViewController.this.D.setEnabled(false);
                    UI_LiveViewController.this.a(8790);
                }
            }
        });
        this.cF.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass29 */

            public void onClick(View view) {
                UI_LiveViewController.this.m((UI_LiveViewController) UI_LiveViewController.this.c.c.h.ae);
                UI_LiveViewController.this.e(true);
            }
        });
        this.w.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass30 */

            public void onClick(View view) {
                UI_LiveViewController.this.a(8454);
            }
        });
        this.cE.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass31 */

            public void onClick(View view) {
                UI_LiveViewController.this.f(true);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void e(boolean z2) {
        if (z2) {
            this.cD.setVisibility(0);
        } else {
            this.cD.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void f(boolean z2) {
        if (z2) {
            this.ch.setVisibility(0);
        } else {
            this.ch.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void g(boolean z2) {
        if (this.ci != null) {
            if (z2) {
                this.ci.setVisibility(0);
            } else {
                this.ci.setVisibility(4);
            }
        }
    }

    private void n(int i2) {
        switch (i2) {
            case 0:
                this.d.h.aQ = this.cf.getText().toString();
                return;
            case 1:
                this.d.h.aR = this.cb.getText().toString();
                this.d.h.aS = this.cf.getText().toString();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void o(int i2) {
        if (i2 == 2) {
            this.aE.setVisibility(4);
            this.cy.setVisibility(4);
            this.cx.setVisibility(0);
            this.cz.setVisibility(0);
            return;
        }
        this.aE.setVisibility(0);
        this.cy.setVisibility(0);
        this.cx.setVisibility(4);
        this.cz.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void h(boolean z2) {
        if (z2) {
            a(this.aF, 500);
            this.aG.setVisibility(0);
            this.D.setImageResource(R.drawable.shooting_effect_green);
            if (GeneralFunction.m.a.j() == 6) {
                this.aT.setImageResource(R.drawable.shooting_effectmenu_ev_white);
                this.aU.setTextColor(getResources().getColor(R.color.white));
            } else {
                this.aT.setImageResource(R.drawable.shooting_effectmenu_icon_ev_green);
                this.aU.setTextColor(getResources().getColor(R.color.iconcolor));
            }
            this.aV.setBackgroundResource(R.drawable.shooting_effectmenu_ev_mid);
            if (GeneralFunction.m.a.k() == 0) {
                this.G.setImageResource(R.drawable.shooting_effectmenu_wb_white);
                this.M.setTextColor(getResources().getColor(R.color.white));
            } else {
                this.G.setImageResource(R.drawable.shooting_effectmenu_icon_wb_green);
                this.M.setTextColor(getResources().getColor(R.color.iconcolor));
            }
            this.H.setImageResource(R.drawable.shooting_effectimage_wb_awb);
            this.I.setImageResource(R.drawable.shooting_effectimage_wb_daylight);
            this.J.setImageResource(R.drawable.shooting_effectimage_wb_cloudy);
            this.K.setImageResource(R.drawable.shooting_effectimage_wb_tungsten);
            this.L.setImageResource(R.drawable.shooting_effectimage_wb_fluorescent);
            int a2 = k.a(this, (float) (this.m / 35));
            this.N.setText(R.string.white_balance_auto);
            this.O.setText(R.string.white_balance_sunny);
            this.P.setText(R.string.white_balance_cloudy);
            this.Q.setText(R.string.white_balance_tungsten);
            this.R.setText(R.string.white_balance_fluorescent);
            GeneralFunction.c q2 = new GeneralFunction.c(a2).b((int) (((double) a2) * 0.9d)).c((int) (((double) a2) * 0.8d)).n((int) (((double) a2) * 0.9d)).e((int) (((double) a2) * 0.8d)).g((int) (((double) a2) * 0.8d)).d((int) (((double) a2) * 0.8d)).m((int) (((double) a2) * 0.9d)).k((int) (((double) a2) * 0.9d)).l((int) (((double) a2) * 0.9d)).h((int) (((double) a2) * 0.9d)).o((int) (((double) a2) * 0.9d)).i((int) (((double) a2) * 0.9d)).p((int) (((double) a2) * 0.8d)).j((int) (((double) a2) * 0.8d)).f((int) (((double) a2) * 0.8d)).q((int) (((double) a2) * 0.8d));
            this.N.setTextSize((float) q2.a());
            this.O.setTextSize((float) q2.a());
            this.P.setTextSize((float) q2.a());
            this.Q.setTextSize((float) q2.a());
            this.R.setTextSize((float) q2.a());
            if (GeneralFunction.m.a.l() == 0) {
                this.Z.setImageResource(R.drawable.shooting_effectmenu_filter_white);
                this.aj.setTextColor(getResources().getColor(R.color.white));
            } else {
                this.Z.setImageResource(R.drawable.shooting_effectmenu_icon_filter_green);
                this.aj.setTextColor(getResources().getColor(R.color.iconcolor));
            }
            this.aa.setImageResource(R.drawable.shooting_effectimage_filter_off);
            this.ab.setImageResource(R.drawable.shooting_effectimage_filter_f1);
            this.ac.setImageResource(R.drawable.shooting_effectimage_filter_f2);
            this.ad.setImageResource(R.drawable.shooting_effectimage_filter_f3);
            this.ae.setImageResource(R.drawable.shooting_effectimage_filter_f4);
            this.af.setImageResource(R.drawable.shooting_effectimage_filter_f5);
            this.ag.setImageResource(R.drawable.shooting_effectimage_filter_f6);
            this.ah.setImageResource(R.drawable.shooting_effectimage_filter_f7);
            this.ai.setImageResource(R.drawable.shooting_effectimage_filter_f8);
            this.ak.setText(R.string.filter_none);
            this.al.setText(R.string.filter_faded);
            this.am.setText(R.string.filter_nimbus);
            this.an.setText(R.string.filter_tea);
            this.ao.setText(R.string.filter_twilight);
            this.ap.setText(R.string.filter_sapphire);
            this.aq.setText(R.string.filter_vintage);
            this.ar.setText(R.string.filter_black_white);
            this.as.setText(R.string.filter_newspaper);
            GeneralFunction.c q3 = new GeneralFunction.c(a2).b((int) (((double) a2) * 0.9d)).c((int) (((double) a2) * 0.8d)).n((int) (((double) a2) * 0.9d)).e((int) (((double) a2) * 0.8d)).g((int) (((double) a2) * 0.8d)).d((int) (((double) a2) * 0.8d)).m((int) (((double) a2) * 0.9d)).k((int) (((double) a2) * 0.9d)).l((int) (((double) a2) * 0.9d)).h((int) (((double) a2) * 0.9d)).o((int) (((double) a2) * 0.9d)).i((int) (((double) a2) * 0.9d)).p((int) (((double) a2) * 0.8d)).j((int) (((double) a2) * 0.8d)).f((int) (((double) a2) * 0.8d)).q((int) (((double) a2) * 0.8d));
            this.ak.setTextSize((float) q3.a());
            this.al.setTextSize((float) q3.a());
            this.am.setTextSize((float) q3.a());
            this.an.setTextSize((float) q3.a());
            this.ao.setTextSize((float) q3.a());
            this.ap.setTextSize((float) q3.a());
            this.aq.setTextSize((float) q3.a());
            this.ar.setTextSize((float) q3.a());
            this.as.setTextSize((float) q3.a());
            return;
        }
        b(this.aF, 500);
        this.aG.setVisibility(4);
        this.D.setImageResource(R.drawable.shooting_effect_white);
    }

    private void aI() {
        this.z = (LinearLayout) findViewById(R.id.LL_ModeSlider);
        this.C = (ImageButton) findViewById(R.id.IB_LiveModeButton);
        this.A = (ImageButton) findViewById(R.id.IB_CaptureModeButton);
        this.B = (ImageButton) findViewById(R.id.IB_RecordModeButton);
        this.B.setVisibility(0);
        this.C.setVisibility(0);
        this.A.setVisibility(0);
        this.A.setImageResource(R.drawable.shooting_mode_photo_white);
        this.B.setImageResource(R.drawable.shooting_mode_video_white);
        this.C.setImageResource(R.drawable.shooting_mode_live_white);
        if (this.c.c.h.T == 1) {
            if (GeneralFunction.m.a.f() == 0) {
                this.v.setImageResource(R.drawable.shooting_shutterkey_video);
            } else {
                this.v.setImageResource(R.drawable.shooting_shutterkey_video_selftimer);
            }
            this.v.setVisibility(0);
        } else if (this.c.c.h.T == 0) {
            if (GeneralFunction.m.a.f() == 0) {
                this.v.setImageResource(R.drawable.shooting_shutterkey_photo);
            } else {
                this.v.setImageResource(R.drawable.shooting_shutterkey_photo_selftimer);
            }
            this.v.setVisibility(0);
            a(1, 0, 0);
        } else {
            this.v.setImageResource(R.drawable.shooting_shutterkey_live);
            this.v.setVisibility(0);
            a(1, 2, 0);
        }
        o(this.c.c.h.T);
        this.A.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass32 */

            public void onClick(View view) {
                UI_LiveViewController.this.A.setImageResource(R.drawable.shooting_mode_photo_green);
                UI_LiveViewController.this.B.setImageResource(R.drawable.shooting_mode_video_white);
                UI_LiveViewController.this.C.setImageResource(R.drawable.shooting_mode_live_white);
                if (!UI_LiveViewController.this.d.h.aM) {
                    if (GeneralFunction.m.a.f() == 0) {
                        UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_photo);
                    } else {
                        UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_photo_selftimer);
                    }
                    UI_LiveViewController.this.ah();
                } else {
                    UI_LiveViewController.this.n(true);
                }
                if (UI_LiveViewController.this.c.c.h.T != 0) {
                    if (UI_LiveViewController.this.c.c.h.T == 1) {
                        UI_LiveViewController.this.a(1, 0, 300);
                    } else if (UI_LiveViewController.this.c.c.h.T == 2) {
                        UI_LiveViewController.this.a(2, 0, 300);
                    }
                }
                UI_LiveViewController.this.c.c.h.T = 0;
                UI_LiveViewController.this.o((UI_LiveViewController) UI_LiveViewController.this.c.c.h.T);
            }
        });
        this.C.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass33 */

            public void onClick(View view) {
                UI_LiveViewController.this.C.setImageResource(R.drawable.shooting_mode_live_green);
                UI_LiveViewController.this.A.setImageResource(R.drawable.shooting_mode_photo_white);
                UI_LiveViewController.this.B.setImageResource(R.drawable.shooting_mode_video_white);
                UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_live);
                if (UI_LiveViewController.this.c.c.h.T == 0) {
                    UI_LiveViewController.this.a(0, 2, 300);
                } else if (UI_LiveViewController.this.c.c.h.T == 1) {
                    UI_LiveViewController.this.a(1, 2, 300);
                } else if (UI_LiveViewController.this.c.c.h.T == 2) {
                }
                UI_LiveViewController.this.c.c.h.T = 2;
                if (UI_LiveViewController.this.d.h.aM) {
                    UI_LiveViewController.this.n(false);
                }
                UI_LiveViewController.this.o((UI_LiveViewController) UI_LiveViewController.this.c.c.h.T);
                UI_LiveViewController.this.a(8800);
            }
        });
        this.B.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass35 */

            public void onClick(View view) {
                UI_LiveViewController.this.B.setImageResource(R.drawable.shooting_mode_video_green);
                UI_LiveViewController.this.C.setImageResource(R.drawable.shooting_mode_live_white);
                UI_LiveViewController.this.A.setImageResource(R.drawable.shooting_mode_photo_white);
                if (!UI_LiveViewController.this.d.h.aM) {
                    if (GeneralFunction.m.a.f() == 0) {
                        UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_video);
                    } else {
                        UI_LiveViewController.this.v.setImageResource(R.drawable.shooting_shutterkey_video_selftimer);
                    }
                    UI_LiveViewController.this.ah();
                } else {
                    UI_LiveViewController.this.n(true);
                }
                if (UI_LiveViewController.this.c.c.h.T == 0) {
                    UI_LiveViewController.this.a(0, 1, 300);
                } else if (UI_LiveViewController.this.c.c.h.T != 1 && UI_LiveViewController.this.c.c.h.T == 2) {
                    UI_LiveViewController.this.a(2, 1, 300);
                }
                UI_LiveViewController.this.c.c.h.T = 1;
                UI_LiveViewController.this.o((UI_LiveViewController) UI_LiveViewController.this.c.c.h.T);
                UI_LiveViewController.this.a(8800);
            }
        });
    }

    private void aJ() {
        this.y = (ImageButton) findViewById(R.id.IB_ui_FullScreenButton);
        if (this.c.c.h.U == 0) {
            this.y.setImageResource(R.drawable.shooting_pip_show);
        } else {
            this.y.setImageResource(R.drawable.shooting_pip_hide);
        }
        if (GeneralFunction.m.a.g() == 1) {
            this.y.setVisibility(4);
            this.y.setEnabled(false);
        }
        this.y.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass36 */

            public void onClick(View view) {
                if (UI_LiveViewController.this.c.c.h.U == 1) {
                    UI_LiveViewController.this.aU();
                    UI_LiveViewController.this.f.setIsShowThumbNail(false);
                    UI_LiveViewController.this.y.setImageResource(R.drawable.shooting_pip_show);
                    UI_LiveViewController.this.c.c.h.U = 0;
                    return;
                }
                UI_LiveViewController.this.aU();
                UI_LiveViewController.this.f.setIsShowThumbNail(true);
                UI_LiveViewController.this.y.setImageResource(R.drawable.shooting_pip_hide);
                UI_LiveViewController.this.c.c.h.U = 1;
            }
        });
    }

    private void aK() {
        this.aF = (LinearLayout) findViewById(R.id.LL_Effect_Tool_Layout);
        this.aG = (LinearLayout) findViewById(R.id.LL_Effect_Tool_UpBlank_Layout);
        this.aH = (LinearLayout) findViewById(R.id.LL_Effect_Tool_Blank_Layout);
        this.aF.setVisibility(4);
        this.aG.setVisibility(4);
        this.aG.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass37 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    UI_LiveViewController.this.a(8790);
                }
                if (motionEvent.getAction() == 2) {
                }
                if (motionEvent.getAction() == 1) {
                }
                return true;
            }
        });
        this.aH.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass38 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return false;
            }
        });
        this.aF.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass39 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    private void aL() {
        this.c.c.h.W = new ArrayList<>();
        this.c.c.h.W.add(getResources().getString(R.string.exposure_negitive_20));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_negitive_17));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_negitive_13));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_negitive_10));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_negitive_07));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_negitive_03));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_0));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_positive_03));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_positive_07));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_positive_10));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_positive_13));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_positive_17));
        this.c.c.h.W.add(getResources().getString(R.string.exposure_positive_20));
    }

    private void aM() {
        aL();
        this.aT = (ImageButton) findViewById(R.id.IB_EV_ButtonTitle);
        this.aU = (TextView) findViewById(R.id.TV_EV_ButtonTitle);
        this.aU.setTextSize((float) new GeneralFunction.c(13).n((int) (((double) 13) * 0.9d)).a());
        this.aU.setText(getResources().getString(R.string.effect_exposure));
        this.aV = (SeekBar) findViewById(R.id.SB_EV_Bar);
        a("UI_SettingControl.GetEvType(): " + GeneralFunction.m.a.j(), 3);
        this.aV.setProgress(GeneralFunction.m.a.j());
        this.aW = (TextView) findViewById(R.id.TV_EV_Value);
        this.aW.setText(this.c.c.h.W.get(GeneralFunction.m.a.j()));
        this.aT.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass40 */

            public void onClick(View view) {
                GeneralFunction.m.a.m(6);
                UI_LiveViewController.this.aT.setImageResource(R.drawable.shooting_effectmenu_ev_white);
                UI_LiveViewController.this.aU.setTextColor(UI_LiveViewController.this.getResources().getColor(R.color.white));
                UI_LiveViewController.this.aV.setProgress(GeneralFunction.m.a.j());
                UI_LiveViewController.this.aW.setText(UI_LiveViewController.this.c.c.h.W.get(GeneralFunction.m.a.j()));
                UI_LiveViewController.this.a(8753, 0L);
            }
        });
        this.aV.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass41 */

            /* renamed from: a  reason: collision with root package name */
            int f1463a = GeneralFunction.m.a.n();

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                this.f1463a = i;
                UI_LiveViewController.this.aW.setText(UI_LiveViewController.this.c.c.h.W.get(i));
                UI_LiveViewController.this.p((UI_LiveViewController) this.f1463a);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void p(int i2) {
        switch (i2) {
            case 0:
                GeneralFunction.m.a.m(0);
                break;
            case 1:
                GeneralFunction.m.a.m(1);
                break;
            case 2:
                GeneralFunction.m.a.m(2);
                break;
            case 3:
                GeneralFunction.m.a.m(3);
                break;
            case 4:
                GeneralFunction.m.a.m(4);
                break;
            case 5:
                GeneralFunction.m.a.m(5);
                break;
            case 6:
                GeneralFunction.m.a.m(6);
                break;
            case 7:
                GeneralFunction.m.a.m(7);
                break;
            case 8:
                GeneralFunction.m.a.m(8);
                break;
            case 9:
                GeneralFunction.m.a.m(9);
                break;
            case 10:
                GeneralFunction.m.a.m(10);
                break;
            case 11:
                GeneralFunction.m.a.m(11);
                break;
            case 12:
                GeneralFunction.m.a.m(12);
                break;
        }
        if (i2 == 6) {
            this.aT.setImageResource(R.drawable.shooting_effectmenu_ev_white);
            this.aU.setTextColor(getResources().getColor(R.color.white));
        } else {
            this.aT.setImageResource(R.drawable.shooting_effectmenu_icon_ev_green);
            this.aU.setTextColor(getResources().getColor(R.color.iconcolor));
        }
        if (!this.c.b(8753)) {
            a(8753, 0L);
        }
    }

    private void aN() {
        this.E = (HorizontalScrollView) findViewById(R.id.HSV_WB);
        this.E.setSmoothScrollingEnabled(true);
        this.G = (ImageButton) findViewById(R.id.IB_WB_ButtonTitle);
        this.M = (TextView) findViewById(R.id.TV_WB_ButtonTitle);
        this.M.setText(getResources().getString(R.string.effect_white_balance));
        this.H = (ImageButton) findViewById(R.id.IB_WB_Button1);
        this.I = (ImageButton) findViewById(R.id.IB_WB_Button2);
        this.J = (ImageButton) findViewById(R.id.IB_WB_Button3);
        this.K = (ImageButton) findViewById(R.id.IB_WB_Button4);
        this.L = (ImageButton) findViewById(R.id.IB_WB_Button5);
        this.N = (TextView) findViewById(R.id.TV_WB_Button1);
        this.O = (TextView) findViewById(R.id.TV_WB_Button2);
        this.P = (TextView) findViewById(R.id.TV_WB_Button3);
        this.Q = (TextView) findViewById(R.id.TV_WB_Button4);
        this.R = (TextView) findViewById(R.id.TV_WB_Button5);
        this.S = (LinearLayout) findViewById(R.id.LL_WB_Button_Layout1);
        this.T = (LinearLayout) findViewById(R.id.LL_WB_Button_Layout2);
        this.U = (LinearLayout) findViewById(R.id.LL_WB_Button_Layout3);
        this.V = (LinearLayout) findViewById(R.id.LL_WB_Button_Layout4);
        this.W = (LinearLayout) findViewById(R.id.LL_WB_Button_Layout5);
        this.X.add(this.H);
        this.X.add(this.I);
        this.X.add(this.J);
        this.X.add(this.K);
        this.X.add(this.L);
        this.Y.add(this.S);
        this.Y.add(this.T);
        this.Y.add(this.U);
        this.Y.add(this.V);
        this.Y.add(this.W);
        this.G.setOnClickListener(this.cO);
        for (int i2 = 0; i2 < this.X.size(); i2++) {
            this.X.get(i2).setOnClickListener(this.cO);
        }
        this.Y.get(GeneralFunction.m.a.k()).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
    }

    /* access modifiers changed from: protected */
    public void i(boolean z2) {
        if (z2) {
            this.aN.setEnabled(false);
        } else {
            this.aN.setEnabled(true);
        }
    }

    private void aO() {
        double a2;
        this.aJ = (LinearLayout) findViewById(R.id.LL_Recordindg_Blank_Layout);
        this.aK = (LinearLayout) findViewById(R.id.LL_Recordindg_Layout);
        this.aL = (TextView) findViewById(R.id.TV_Current_Rec_Time);
        this.aM = (TextView) findViewById(R.id.TV_Avaliable_Memory);
        this.aN = (ImageButton) findViewById(R.id.IB_ui_Recording_Stop_Button);
        this.aO = (ImageView) findViewById(R.id.IV_Recording_Icon);
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        if (GeneralFunction.m.a.a() == 1) {
            this.c.c.k.b.ab = ai();
            a2 = (double) d.a(this.c.c.k.b.ab, 0);
        } else {
            a2 = (double) d.a(c.f1314a, 0);
        }
        String format = decimalFormat.format(Double.parseDouble(Double.toString(((a2 / 1024.0d) / 1024.0d) / 1024.0d)));
        this.aM.setText(String.format("%d", 0) + "MB / " + format + "GB");
        this.aN.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass42 */

            public void onClick(View view) {
                UI_LiveViewController.this.aD();
            }
        });
        this.aJ.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass43 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return false;
            }
        });
        this.aK.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass44 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void R() {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.aK, "translationY", 0.0f, 500.0f);
        ofFloat.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass46 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                UI_LiveViewController.this.aK.setVisibility(4);
                UI_LiveViewController.this.aO.setImageResource(0);
                UI_LiveViewController.this.aN.setImageResource(0);
            }
        });
        ofFloat.setDuration(300L);
        ofFloat.start();
    }

    private void aP() {
        this.bh = (LinearLayout) findViewById(R.id.LL_ui_Liveview_ProgressLayout);
        this.bh.setVisibility(4);
        this.bi = (TextView) findViewById(R.id.TV_ui_Liveview_Content);
        this.bj = (ImageView) findViewById(R.id.IV_ui_Liveview_ProgressIcon);
        this.bh.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass47 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void b(boolean z2, String str) {
        if (this.bh != null && this.bi != null && this.bj != null) {
            if (!str.equals(getResources().getString(R.string.connecting)) || (this.c.d.b() && !this.c.c.e)) {
                this.bj.setVisibility(0);
                this.bj.setImageResource(R.drawable.anim_system_busy);
                if (z2) {
                    this.bh.setVisibility(0);
                    this.bi.setText(str);
                    ((AnimationDrawable) this.bj.getDrawable()).start();
                    return;
                }
                this.bh.setVisibility(4);
                this.bi.setText(str);
                ((AnimationDrawable) this.bj.getDrawable()).stop();
                this.bj.setImageResource(0);
                return;
            }
            this.bi.setText("");
            this.bj.setVisibility(4);
            if (z2) {
                this.bh.setVisibility(0);
            } else {
                this.bh.setVisibility(4);
            }
        }
    }

    private void aQ() {
        this.F = (HorizontalScrollView) findViewById(R.id.HSV_Filter);
        this.F.setSmoothScrollingEnabled(true);
        this.Z = (ImageButton) findViewById(R.id.IB_Filter_ButtonTitle);
        this.aj = (TextView) findViewById(R.id.TV_Filter_ButtonTitle);
        this.aj.setText(getResources().getString(R.string.effect_filter));
        this.aa = (ImageButton) findViewById(R.id.IB_Filter_Button1);
        this.ab = (ImageButton) findViewById(R.id.IB_Filter_Button2);
        this.ac = (ImageButton) findViewById(R.id.IB_Filter_Button3);
        this.ad = (ImageButton) findViewById(R.id.IB_Filter_Button4);
        this.ae = (ImageButton) findViewById(R.id.IB_Filter_Button5);
        this.af = (ImageButton) findViewById(R.id.IB_Filter_Button6);
        this.ag = (ImageButton) findViewById(R.id.IB_Filter_Button7);
        this.ah = (ImageButton) findViewById(R.id.IB_Filter_Button8);
        this.ai = (ImageButton) findViewById(R.id.IB_Filter_Button9);
        this.ak = (TextView) findViewById(R.id.TV_Filter_Button1);
        this.al = (TextView) findViewById(R.id.TV_Filter_Button2);
        this.am = (TextView) findViewById(R.id.TV_Filter_Button3);
        this.an = (TextView) findViewById(R.id.TV_Filter_Button4);
        this.ao = (TextView) findViewById(R.id.TV_Filter_Button5);
        this.ap = (TextView) findViewById(R.id.TV_Filter_Button6);
        this.aq = (TextView) findViewById(R.id.TV_Filter_Button7);
        this.ar = (TextView) findViewById(R.id.TV_Filter_Button8);
        this.as = (TextView) findViewById(R.id.TV_Filter_Button9);
        this.at = (LinearLayout) findViewById(R.id.LL_Filter_Button_Layout1);
        this.au = (LinearLayout) findViewById(R.id.LL_Filter_Button_Layout2);
        this.av = (LinearLayout) findViewById(R.id.LL_Filter_Button_Layout3);
        this.aw = (LinearLayout) findViewById(R.id.LL_Filter_Button_Layout4);
        this.ax = (LinearLayout) findViewById(R.id.LL_Filter_Button_Layout5);
        this.ay = (LinearLayout) findViewById(R.id.LL_Filter_Button_Layout6);
        this.az = (LinearLayout) findViewById(R.id.LL_Filter_Button_Layout7);
        this.aA = (LinearLayout) findViewById(R.id.LL_Filter_Button_Layout8);
        this.aB = (LinearLayout) findViewById(R.id.LL_Filter_Button_Layout9);
        this.aC.add(this.aa);
        this.aC.add(this.ab);
        this.aC.add(this.ac);
        this.aC.add(this.ad);
        this.aC.add(this.ae);
        this.aC.add(this.af);
        this.aC.add(this.ag);
        this.aC.add(this.ah);
        this.aC.add(this.ai);
        this.aD.add(this.at);
        this.aD.add(this.au);
        this.aD.add(this.av);
        this.aD.add(this.aw);
        this.aD.add(this.ax);
        this.aD.add(this.ay);
        this.aD.add(this.az);
        this.aD.add(this.aA);
        this.aD.add(this.aB);
        this.Z.setOnClickListener(this.cP);
        for (int i2 = 0; i2 < this.aC.size(); i2++) {
            this.aC.get(i2).setOnClickListener(this.cP);
        }
        this.aD.get(GeneralFunction.m.a.l()).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
    }

    /* access modifiers changed from: protected */
    public void S() {
        int size;
        int size2;
        int width = this.S.getWidth();
        int width2 = this.at.getWidth();
        if (this.E.getLayoutDirection() == 0) {
            size = GeneralFunction.m.a.k();
            size2 = GeneralFunction.m.a.l();
        } else {
            size = (this.X.size() - 1) - GeneralFunction.m.a.k();
            size2 = (this.aC.size() - 1) - GeneralFunction.m.a.l();
        }
        this.E.scrollTo(size * width, 0);
        this.F.scrollTo(size2 * width2, 0);
    }

    /* access modifiers changed from: protected */
    public void T() {
        for (int i2 = 0; i2 < this.aC.size(); i2++) {
            this.aD.get(i2).setBackgroundColor(-15263977);
            this.aD.get(i2).setBackgroundResource(0);
        }
        this.aD.get(GeneralFunction.m.a.l()).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
        if (GeneralFunction.m.a.l() == 0) {
            this.Z.setImageResource(R.drawable.shooting_effectmenu_filter_white);
            this.aj.setTextColor(getResources().getColor(R.color.white));
        } else {
            this.Z.setImageResource(R.drawable.shooting_effectmenu_icon_filter_green);
            this.aj.setTextColor(getResources().getColor(R.color.iconcolor));
        }
        for (int i3 = 0; i3 < this.X.size(); i3++) {
            this.Y.get(i3).setBackgroundResource(0);
            this.Y.get(i3).setBackgroundColor(-15263977);
        }
        this.Y.get(GeneralFunction.m.a.k()).setBackgroundResource(R.drawable.shooting_effectmenu_cursor);
        if (GeneralFunction.m.a.k() == 0) {
            this.G.setImageResource(R.drawable.shooting_effectmenu_wb_white);
            this.M.setTextColor(getResources().getColor(R.color.white));
        } else {
            this.G.setImageResource(R.drawable.shooting_effectmenu_icon_wb_green);
            this.M.setTextColor(getResources().getColor(R.color.iconcolor));
        }
        this.aV.setProgress(GeneralFunction.m.a.j());
        this.aW.setText(this.c.c.h.W.get(GeneralFunction.m.a.j()));
        if (GeneralFunction.m.a.j() == 6) {
            this.aT.setImageResource(R.drawable.shooting_effectmenu_ev_white);
            this.aU.setTextColor(getResources().getColor(R.color.white));
            return;
        }
        this.aT.setImageResource(R.drawable.shooting_effectmenu_icon_ev_green);
        this.aU.setTextColor(getResources().getColor(R.color.iconcolor));
    }

    private void aR() {
        this.aI = (LinearLayout) findViewById(R.id.LL_ui_Finger_Detect);
        this.aI.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass50 */

            /* renamed from: a  reason: collision with root package name */
            float f1473a = 0.0f;

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    this.f1473a = motionEvent.getX();
                } else if (motionEvent.getAction() == 1) {
                    if (motionEvent.getX() - this.f1473a > 80.0f) {
                        if (UI_LiveViewController.this.c.c.h.T == 0) {
                            if (UI_LiveViewController.this.z.getLayoutDirection() != 0) {
                                UI_LiveViewController.this.a((UI_LiveViewController) 0, 1);
                            }
                        } else if (UI_LiveViewController.this.c.c.h.T == 1) {
                            if (UI_LiveViewController.this.z.getLayoutDirection() == 0) {
                                UI_LiveViewController.this.a((UI_LiveViewController) 1, 0);
                            } else {
                                UI_LiveViewController.this.a((UI_LiveViewController) 1, 2);
                            }
                        } else if (UI_LiveViewController.this.c.c.h.T == 2 && UI_LiveViewController.this.z.getLayoutDirection() == 0) {
                            UI_LiveViewController.this.a((UI_LiveViewController) 2, 1);
                        }
                    } else if (this.f1473a - motionEvent.getX() > 80.0f) {
                        if (UI_LiveViewController.this.c.c.h.T == 0) {
                            if (UI_LiveViewController.this.z.getLayoutDirection() == 0) {
                                UI_LiveViewController.this.a((UI_LiveViewController) 0, 1);
                            }
                        } else if (UI_LiveViewController.this.c.c.h.T == 1) {
                            if (UI_LiveViewController.this.z.getLayoutDirection() == 0) {
                                UI_LiveViewController.this.a((UI_LiveViewController) 1, 2);
                            } else {
                                UI_LiveViewController.this.a((UI_LiveViewController) 1, 0);
                            }
                        } else if (UI_LiveViewController.this.c.c.h.T == 2 && UI_LiveViewController.this.z.getLayoutDirection() != 0) {
                            UI_LiveViewController.this.a((UI_LiveViewController) 2, 1);
                        }
                    }
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, int i3) {
        o(i3);
        switch (i3) {
            case 0:
                if (!this.d.h.aM) {
                    if (GeneralFunction.m.a.f() == 0) {
                        this.v.setImageResource(R.drawable.shooting_shutterkey_photo);
                    } else {
                        this.v.setImageResource(R.drawable.shooting_shutterkey_photo_selftimer);
                    }
                }
                ah();
                a(i2, i3, 300);
                this.c.c.h.T = 0;
                h(this.c.c.h.T);
                return;
            case 1:
                if (!this.d.h.aM) {
                    if (GeneralFunction.m.a.f() == 0) {
                        this.v.setImageResource(R.drawable.shooting_shutterkey_video);
                    } else {
                        this.v.setImageResource(R.drawable.shooting_shutterkey_video_selftimer);
                    }
                }
                ah();
                a(i2, i3, 300);
                this.c.c.h.T = 1;
                h(this.c.c.h.T);
                return;
            case 2:
                this.v.setImageResource(R.drawable.shooting_shutterkey_live);
                a(i2, i3, 300);
                this.c.c.h.T = 2;
                h(this.c.c.h.T);
                ah();
                return;
            default:
                a("Mode is error!", 0);
                return;
        }
    }

    private void aS() {
        this.aX = (TextView) findViewById(R.id.TV_Self_Timer);
        this.aX.setVisibility(4);
        this.aP = (LinearLayout) findViewById(R.id.LL_Self_Timer_Layout);
        this.aQ = (ImageButton) findViewById(R.id.IB_ui_Self_Timer_Button);
        this.aP.setVisibility(4);
        this.aQ.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass51 */

            public void onClick(View view) {
                UI_LiveViewController.this.aC();
            }
        });
        this.aP.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass52 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    private void aT() {
        this.aR = (LinearLayout) findViewById(R.id.LL_ui_Capture_Rec_Progress);
        this.aR.setVisibility(4);
        this.aS = (ImageView) findViewById(R.id.IV_ui_Capture_Rec_Anim);
        this.aR.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass53 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void j(boolean z2) {
        this.aS.setImageResource(R.drawable.anim_shooting_busy);
        if (z2) {
            this.aR.setVisibility(0);
            ((AnimationDrawable) this.aS.getDrawable()).start();
            return;
        }
        this.aR.setVisibility(4);
        ((AnimationDrawable) this.aS.getDrawable()).stop();
        this.aS.setImageResource(0);
    }

    /* access modifiers changed from: protected */
    public void U() {
        ui_Controller.b.k kVar = this.c.c.h;
        int i2 = kVar.Y;
        kVar.Y = i2 - 1;
        if (i2 == 1) {
            g.a(3);
            this.c.c.h.Y = q(GeneralFunction.m.a.f()) / 1000;
            this.aX.setText(Integer.toString(this.c.c.h.Y));
            this.c.c.h.Z = false;
            this.aX.setVisibility(4);
            return;
        }
        ObjectAnimator.ofFloat(this.aX, "alpha", 0.0f, 1.0f).setDuration(500L).start();
        this.aX.setText(Integer.toString(this.c.c.h.Y));
        aX();
    }

    /* access modifiers changed from: protected */
    public void V() {
        g.a(3);
        this.c.c.h.Y = q(GeneralFunction.m.a.f()) / 1000;
        this.c.c.h.Z = false;
        this.aX.setText(Integer.toString(this.c.c.h.Y));
        this.aX.setVisibility(4);
    }

    private void a(LinearLayout linearLayout, int i2) {
        Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(linearLayout, (linearLayout.getWidth() * 7) / 10, linearLayout.getHeight(), 0.0f, (float) Math.max(linearLayout.getWidth(), linearLayout.getHeight()));
        linearLayout.setVisibility(0);
        createCircularReveal.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass54 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                UI_LiveViewController.this.D.setEnabled(true);
            }
        });
        createCircularReveal.setDuration((long) i2);
        createCircularReveal.start();
    }

    private void b(final LinearLayout linearLayout, int i2) {
        Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(linearLayout, (linearLayout.getWidth() * 7) / 10, linearLayout.getHeight(), (float) linearLayout.getWidth(), 0.0f);
        createCircularReveal.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass55 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                linearLayout.setVisibility(4);
                UI_LiveViewController.this.D.setEnabled(true);
                UI_LiveViewController.this.aT.setImageResource(0);
                UI_LiveViewController.this.aV.setBackgroundResource(0);
                UI_LiveViewController.this.G.setImageResource(0);
                UI_LiveViewController.this.H.setImageResource(0);
                UI_LiveViewController.this.I.setImageResource(0);
                UI_LiveViewController.this.J.setImageResource(0);
                UI_LiveViewController.this.K.setImageResource(0);
                UI_LiveViewController.this.L.setImageResource(0);
                UI_LiveViewController.this.Z.setImageResource(0);
                UI_LiveViewController.this.aa.setImageResource(0);
                UI_LiveViewController.this.ab.setImageResource(0);
                UI_LiveViewController.this.ac.setImageResource(0);
                UI_LiveViewController.this.ad.setImageResource(0);
                UI_LiveViewController.this.ae.setImageResource(0);
                UI_LiveViewController.this.af.setImageResource(0);
                UI_LiveViewController.this.ag.setImageResource(0);
                UI_LiveViewController.this.ah.setImageResource(0);
                UI_LiveViewController.this.ai.setImageResource(0);
            }
        });
        createCircularReveal.setDuration((long) i2);
        createCircularReveal.start();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void aU() {
        new Path();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.y, "rotation", 180.0f, 0.0f, 0.0f);
        ofFloat.setDuration(1000L);
        ofFloat.start();
    }

    private ObjectAnimator a(Object obj, int i2) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat((ImageButton) obj, "rotation", 0.0f, 360.0f);
        ofFloat.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass57 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                UI_LiveViewController.this.V();
                UI_LiveViewController.this.v.setEnabled(true);
                if (UI_LiveViewController.this.c.c.h.aa) {
                    UI_LiveViewController.this.k(false);
                    UI_LiveViewController.this.V();
                    UI_LiveViewController.this.c.c.h.aa = false;
                } else if (UI_LiveViewController.this.a() != 0) {
                    UI_LiveViewController.this.k(false);
                    UI_LiveViewController.this.V();
                    UI_LiveViewController.this.a((UI_LiveViewController) "Activity not ready, ignore record msg", (String) 0);
                } else if (UI_LiveViewController.this.c.c.h.T == 0) {
                    UI_LiveViewController.this.c.c.h.Z = false;
                    UI_LiveViewController.this.a(8775);
                } else {
                    UI_LiveViewController.this.a(8774);
                }
            }

            public void onAnimationCancel(Animator animator) {
                super.onAnimationCancel(animator);
            }
        });
        ofFloat.setDuration((long) i2);
        ofFloat.start();
        return ofFloat;
    }

    /* access modifiers changed from: protected */
    public void h(int i2) {
        if (i2 == 0) {
            this.A.setImageResource(R.drawable.shooting_mode_photo_green);
            this.B.setImageResource(R.drawable.shooting_mode_video_white);
            this.C.setImageResource(R.drawable.shooting_mode_live_white);
        } else if (i2 == 1) {
            this.A.setImageResource(R.drawable.shooting_mode_photo_white);
            this.B.setImageResource(R.drawable.shooting_mode_video_green);
            this.C.setImageResource(R.drawable.shooting_mode_live_white);
        } else if (i2 == 2) {
            this.A.setImageResource(R.drawable.shooting_mode_photo_white);
            this.B.setImageResource(R.drawable.shooting_mode_video_white);
            this.C.setImageResource(R.drawable.shooting_mode_live_green);
        }
    }

    /* access modifiers changed from: protected */
    public void W() {
        if (this.ch != null && (this.cJ.isShown() || this.ch.isShown())) {
            f(false);
        }
        if (this.ci != null && (this.cJ.isShown() || this.ci.isShown())) {
            g(false);
        }
        if (this.cD != null && this.cD.isShown()) {
            X();
            e(false);
            this.bZ.a(this.d.h.ae).e();
        }
    }

    /* access modifiers changed from: protected */
    public void X() {
        InputMethodManager inputMethodManager;
        if (getCurrentFocus() != null && (inputMethodManager = (InputMethodManager) getSystemService("input_method")) != null) {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3, int i4) {
        int width = this.z.getWidth() / 3;
        int i5 = -1;
        if (this.z.getLayoutDirection() != 0) {
            i5 = 1;
        }
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.z, "translationX", (float) ((i2 - 1) * width * i5), (float) (i5 * width * (i3 - 1)));
        ofFloat.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass58 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
            }
        });
        ofFloat.setDuration((long) i4);
        ofFloat.start();
    }

    /* access modifiers changed from: protected */
    public void Y() {
        this.v.setEnabled(false);
        this.aP.setVisibility(4);
        this.c.c.h.Z = false;
        this.aO.setImageResource(R.drawable.shooting_recording);
        this.aN.setImageResource(R.drawable.shooting_shutterkey_video_stop);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.aK, "translationY", 500.0f, 0.0f);
        ofFloat.setDuration(150L);
        ofFloat.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass59 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                UI_LiveViewController.this.v.setEnabled(true);
            }
        });
        ofFloat.start();
        this.aK.setVisibility(0);
        this.aL.setText(String.format("%02d:%02d", 0, 0));
    }

    /* access modifiers changed from: protected */
    public void k(boolean z2) {
        if (!z2) {
            this.aP.setVisibility(4);
            this.aX.setVisibility(4);
            this.c.c.h.Z = false;
            this.aQ.setImageResource(0);
            return;
        }
        this.aP.setVisibility(0);
        this.aQ.setImageResource(R.drawable.shooting_shutterkey_selftimer_ring2);
        this.c.c.h.ab = a(this.aQ, q(GeneralFunction.m.a.f()));
        this.aX.setVisibility(0);
        ObjectAnimator.ofFloat(this.aX, "alpha", 0.0f, 1.0f).setDuration(500L).start();
        this.d.h.aN = 0;
        g.a(3, 1000);
        this.c.c.h.Z = true;
        aX();
    }

    /* access modifiers changed from: protected */
    public boolean Z() {
        if (w()) {
            f(false);
        } else if (x()) {
            g(false);
            f(true);
        } else if (!v()) {
            return false;
        } else {
            e(false);
            this.bZ.a(this.d.h.ae).e();
        }
        return true;
    }

    public void onBackPressed() {
        if (this.c.a(1L) && !this.c.c.h.w) {
            this.c.b(268435455L);
            a(32768, 0L);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (!this.c.d.b() || (i2 != 25 && i2 != 24)) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (v()) {
            return super.onKeyDown(i2, keyEvent);
        }
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 25 && i2 != 24) {
            return super.onKeyUp(i2, keyEvent);
        }
        if (!this.c.d.b()) {
            return true;
        }
        int a2 = this.c.a();
        if (this.aR.isShown() || u() || bb() || v()) {
            return true;
        }
        switch (a2) {
            case 272:
            case 400:
                if (this.c.c.h.Z) {
                    aC();
                    return true;
                }
                aB();
                return true;
            case 288:
            case 305:
                a("Ignore volume key while capturing or record-stopping", 0);
                return true;
            case HttpStatusCodes.STATUS_CODE_NOT_MODIFIED:
                aD();
                return true;
            case 336:
            case 416:
                aE();
                return true;
            default:
                a("Ignore volume key at unknown mode: " + a2, 0);
                return true;
        }
    }

    private int q(int i2) {
        switch (i2) {
            case 1:
                return 2000;
            case 2:
                return ExoPlayer.Factory.DEFAULT_MIN_REBUFFER_MS;
            case 3:
                return 10000;
            default:
                return 0;
        }
    }

    private int r(int i2) {
        switch (i2) {
            case 0:
                return 10;
            case 1:
                return 300;
            default:
                return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void aa() {
        this.c.c.h.x = false;
        this.c.c.h.y = false;
    }

    /* access modifiers changed from: protected */
    public boolean ab() {
        if (!this.c.c.h.y || !this.c.c.h.x) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void ac() {
        this.c.a(272, this);
        J();
        if (this.c.c.h.S != 1) {
            this.c.c(268435455);
        }
        if (this.c.c.h.au) {
            this.c.c.h.au = false;
            a("Send RESUME MSG by stop recording", 3);
            a(8734);
        }
    }

    /* access modifiers changed from: protected */
    public void ad() {
        if (this.c.c.h.am == null) {
            this.d.h.aw = System.currentTimeMillis();
            if (GeneralFunction.m.a.a() == 1) {
                this.d.k.b.ab = ai();
                if (this.d.k.b.ab == null) {
                    GeneralFunction.m.a.a(0);
                    GeneralFunction.n.a.b(this.c).putInt("storeLocation", GeneralFunction.m.a.a()).apply();
                    this.d.h.ax = GeneralFunction.g.a.a(c.c, GeneralFunction.g.a.a(Long.valueOf(this.d.h.aw), "mp4"));
                } else {
                    this.d.h.ax = GeneralFunction.g.a.a(this.d.k.b.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/", GeneralFunction.g.a.a(Long.valueOf(this.d.h.aw), "mp4"));
                }
            } else {
                this.d.h.ax = GeneralFunction.g.a.a(c.c, GeneralFunction.g.a.a(Long.valueOf(this.d.h.aw), "mp4"));
            }
            this.d.h.am = new GeneralFunction.r.b(this.d.h.ax, true, getResources().getString(R.string.internal_app_version), this.d.g.c);
            if (this.d.h.am != null) {
                this.d.h.am.a(this.cQ);
                this.d.h.an = new GeneralFunction.r.a(new a.AbstractC0005a() {
                    /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass60 */

                    @Override // GeneralFunction.r.a.AbstractC0005a
                    public void a(MediaFormat mediaFormat) {
                        if (UI_LiveViewController.this.d.h.am != null) {
                            UI_LiveViewController.this.d.h.am.a(mediaFormat);
                        }
                    }

                    @Override // GeneralFunction.r.a.AbstractC0005a
                    public void a(ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) {
                        if (UI_LiveViewController.this.d.h.am != null) {
                            UI_LiveViewController.this.d.h.aD = bufferInfo.presentationTimeUs / 1000;
                            UI_LiveViewController.this.d.h.am.a(byteBuffer, bufferInfo);
                        }
                    }

                    @Override // GeneralFunction.r.a.AbstractC0005a
                    public void a() {
                        if (UI_LiveViewController.this.d.h.am != null) {
                            UI_LiveViewController.this.d.h.am.a();
                        }
                    }
                });
                this.d.h.an.a();
                return;
            }
            return;
        }
        a("objMp4Muxer != null", 0);
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr) {
        MediaFormat createVideoFormat;
        a("Recording initLiveviewFramIntoRecorder", 3);
        if (GeneralFunction.m.a.b() == 0) {
            createVideoFormat = MediaFormat.createVideoFormat(MimeTypes.VIDEO_H264, 1920, 960);
        } else {
            createVideoFormat = MediaFormat.createVideoFormat(MimeTypes.VIDEO_H264, 1280, 640);
        }
        byte[] c2 = e.c(bArr);
        byte[] d2 = e.d(bArr);
        if (!(c2 == null || d2 == null)) {
            this.d.h.am.a(c2.length + d2.length);
            createVideoFormat.setByteBuffer("csd-0", ByteBuffer.wrap(c2));
            createVideoFormat.setByteBuffer("csd-1", ByteBuffer.wrap(d2));
        }
        this.d.h.am.b(createVideoFormat);
    }

    /* access modifiers changed from: protected */
    public void b(a.a.a.d dVar) {
        boolean b2 = e.b(dVar.f169a.a());
        this.d.h.aD = ((this.d.h.ay * 1000) + (((this.d.h.az * 1000) * 1000) / this.d.h.aA)) / 1000;
        this.d.h.am.a(dVar.f169a.a(), dVar.f169a.f68a, this.d.h.ay + ((this.d.h.az * 1000) / 30), b2);
    }

    /* access modifiers changed from: protected */
    public void ae() {
        l(true);
    }

    /* access modifiers changed from: protected */
    public void l(boolean z2) {
        GeneralFunction.e.b bVar;
        a("Recording stopRecorder", 3);
        if (this.d.h.an == null) {
            a("No need to stop audio recorder, send MSG_UI_VIEW_SAVE_VIDEO_FILE_DONE directly", 1);
            a(8727);
        } else if (this.d.h.am == null) {
            this.d.h.an.b();
            a("No need to stop Muxer, send MSG_UI_VIEW_SAVE_VIDEO_FILE_DONE directly", 1);
            a(8727);
        } else {
            this.d.h.an.b();
            this.d.h.am.b();
            if (z2 && this.d.h.S != 5) {
                String str = this.d.h.ax;
                int c2 = this.d.k.e.c();
                String substring = str.substring(0, str.lastIndexOf("/") + 1);
                String substring2 = str.substring(str.lastIndexOf("/") + 1, str.length());
                int z3 = GeneralFunction.m.a.z();
                int A2 = GeneralFunction.m.a.A();
                int i2 = (int) ((this.d.h.aD - this.d.h.aC) / 1000);
                if (GeneralFunction.m.a.a() == 1) {
                    this.d.k.e.a(substring2, substring, -1, 1, 1, z3, A2, (long) i2, this.d.h.aw, this.d.h.aw, c2 + 1, 0, true, true, true, true, false, true, true, true, false);
                } else {
                    this.d.k.e.a(substring2, substring, -1, 1, 1, z3, A2, (long) i2, this.d.h.aw, this.d.h.aw, c2 + 1, 0, true, true, true, true, false, true, true, false, false);
                }
                a("Video durarion: " + i2, 3);
                this.c.x();
                this.d.k.e.a(this.d.k.c, this.d.k.f, this.d.k.g, this.d.k.h);
                if (GeneralFunction.m.a.a() == 1) {
                    bVar = this.d.k.g;
                } else {
                    bVar = this.d.k.f;
                }
                this.d.k.b.f1320a.a(this.d.k.e.a(), bVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void i(int i2) {
        switch (i2) {
            case 0:
                aV();
                return;
            case 1:
                aW();
                return;
            default:
                return;
        }
    }

    private void aV() {
        if (GeneralFunction.m.a.h() == 0) {
            n.a().b();
        }
    }

    private void aW() {
        if (GeneralFunction.m.a.h() == 0) {
            n.a().c();
        }
    }

    private void aX() {
        a("playSelfTimerBeepSound() " + this.d.h.aN, 3);
        if (GeneralFunction.m.a.h() == 0 && this.d.h.aN < q(GeneralFunction.m.a.f()) / 1000) {
            n.a().d();
        }
        this.d.h.aN++;
    }

    public Bitmap a(Bitmap bitmap, int i2) {
        if (bitmap.getWidth() == 160 && bitmap.getHeight() == 120) {
            int height = (bitmap.getHeight() / 3) * 2;
            bitmap = Bitmap.createBitmap(bitmap, 0, (bitmap.getHeight() - height) / 2, bitmap.getWidth(), height);
        }
        if ((bitmap.getWidth() == 5376 && bitmap.getHeight() == 2688) || (bitmap.getWidth() == 3840 && bitmap.getHeight() == 1920)) {
            int height2 = (bitmap.getHeight() * 510) / 720;
            bitmap = Bitmap.createBitmap(bitmap, (bitmap.getWidth() - height2) / 2, (bitmap.getHeight() - height2) / 2, height2, height2);
        }
        int height3 = bitmap.getHeight();
        a("radius: " + height3, 3);
        if (!(bitmap.getWidth() == height3 && bitmap.getHeight() == height3)) {
            int width = (bitmap.getWidth() / 4) * 2;
            bitmap = Bitmap.createBitmap(bitmap, (bitmap.getWidth() - width) / 2, 0, width, bitmap.getHeight());
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#BAB399"));
        canvas.drawCircle((((float) bitmap.getWidth()) / 2.0f) + 0.7f, (((float) bitmap.getHeight()) / 2.0f) + 0.7f, (((float) bitmap.getWidth()) / 2.0f) + 0.1f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    public void j(int i2) {
        d.a(this, i2);
    }

    /* access modifiers changed from: protected */
    public void af() {
        a("setReversePortrait()", 3);
        this.c.c.h.ad = true;
        j(9);
        this.o = 1;
        this.p = 9;
        this.c.c.h.ac = 9;
    }

    private void aY() {
        this.c.c.h.ap = new OrientationEventListener(this.c) {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass61 */

            public void onOrientationChanged(int i) {
                UI_LiveViewController.this.c.c.h.u = i;
                if (i != -1) {
                    UI_LiveViewController.this.c.c.h.ad = true;
                    if (i > 350 || i < 10) {
                        if (!UI_LiveViewController.this.c.d.b()) {
                            if (UI_LiveViewController.this.p != 1) {
                                UI_LiveViewController.this.j(1);
                                UI_LiveViewController.this.p = 1;
                                UI_LiveViewController.this.c.c.h.ac = 1;
                            }
                        } else if (UI_LiveViewController.this.p != 9) {
                            UI_LiveViewController.this.j(9);
                            UI_LiveViewController.this.p = 9;
                            UI_LiveViewController.this.c.c.h.ac = 9;
                        }
                    } else if (i > 170 && i < 190 && UI_LiveViewController.this.p != 9) {
                        UI_LiveViewController.this.j(9);
                        UI_LiveViewController.this.p = 9;
                        UI_LiveViewController.this.c.c.h.ac = 9;
                    }
                }
            }
        };
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void aZ() {
        this.c.c.h.an.c();
        this.c.c.h.an.a((a.AbstractC0005a) null);
        this.d.h.az = 0;
        this.d.h.am = null;
    }

    private void ba() {
        this.bg.setText("");
        this.bi.setText("");
        this.x.setImageBitmap(null);
        this.v.setBackground(null);
        this.v.setImageResource(0);
        this.w.setBackground(null);
        this.w.setImageResource(0);
        this.y.setBackground(null);
        this.y.setImageResource(0);
        this.C.setBackground(null);
        this.C.setImageResource(0);
        this.A.setBackground(null);
        this.A.setImageResource(0);
        this.B.setBackground(null);
        this.B.setImageResource(0);
        this.C.setBackground(null);
        this.C.setImageResource(0);
        this.D.setBackground(null);
        this.D.setImageResource(0);
        this.aT.setImageResource(0);
        this.G.setImageResource(0);
        this.Z.setImageResource(0);
        this.aV.setOnSeekBarChangeListener(null);
        this.aV.clearFocus();
        this.H.setBackground(null);
        this.H.setImageResource(0);
        this.I.setBackground(null);
        this.I.setImageResource(0);
        this.J.setBackground(null);
        this.J.setImageResource(0);
        this.K.setBackground(null);
        this.K.setImageResource(0);
        this.L.setBackground(null);
        this.L.setImageResource(0);
        this.M.setText("");
        this.N.setText("");
        this.O.setText("");
        this.P.setText("");
        this.Q.setText("");
        this.R.setText("");
        this.aa.setBackground(null);
        this.ab.setBackground(null);
        this.ac.setBackground(null);
        this.ad.setBackground(null);
        this.ae.setBackground(null);
        this.af.setBackground(null);
        this.ag.setBackground(null);
        this.ah.setBackground(null);
        this.ai.setBackground(null);
        this.aa.setImageResource(0);
        this.ab.setImageResource(0);
        this.ac.setImageResource(0);
        this.ad.setImageResource(0);
        this.ae.setImageResource(0);
        this.af.setImageResource(0);
        this.ag.setImageResource(0);
        this.ah.setImageResource(0);
        this.ai.setImageResource(0);
        this.aj.setText("");
        this.ak.setText("");
        this.al.setText("");
        this.am.setText("");
        this.an.setText("");
        this.ao.setText("");
        this.ap.setText("");
        this.aq.setText("");
        this.ar.setText("");
        this.as.setText("");
        this.aE.setBackground(null);
        this.aE.setImageResource(0);
        this.aE.setImageBitmap(null);
        this.aN.setBackground(null);
        this.aN.setImageResource(0);
        this.aO.setBackground(null);
        this.aO.setImageResource(0);
        this.aQ.setBackground(null);
        this.aQ.setImageResource(0);
        this.aS.setBackground(null);
        this.aS.setImageResource(0);
        this.ba.setBackground(null);
        this.ba.setImageResource(0);
        this.ba.setImageBitmap(null);
        this.bb.setBackground(null);
        this.bb.setImageResource(0);
        this.bc.setBackground(null);
        this.bc.setImageResource(0);
        this.be.setBackground(null);
        this.be.setImageResource(0);
        this.bj.setBackground(null);
        this.bj.setImageResource(0);
        this.bl.setBackground(null);
        this.bl.setImageResource(0);
        this.aL.setText("");
        this.aM.setText("");
        this.X.clear();
        this.Y.clear();
        this.aC.clear();
        this.aD.clear();
        this.f.setPlayerStatusListener(null);
        this.f = null;
        this.at.setBackgroundColor(0);
        this.at.setBackground(null);
        this.au.setBackgroundColor(0);
        this.au.setBackground(null);
        this.av.setBackgroundColor(0);
        this.av.setBackground(null);
        this.aw.setBackgroundColor(0);
        this.aw.setBackground(null);
        this.ax.setBackgroundColor(0);
        this.ax.setBackground(null);
        this.ay.setBackgroundColor(0);
        this.ay.setBackground(null);
        this.az.setBackgroundColor(0);
        this.az.setBackground(null);
        this.aA.setBackgroundColor(0);
        this.aA.setBackground(null);
        this.aB.setBackgroundColor(0);
        this.aB.setBackground(null);
        this.S.setBackgroundColor(0);
        this.S.setBackground(null);
        this.T.setBackgroundColor(0);
        this.T.setBackground(null);
        this.U.setBackgroundColor(0);
        this.U.setBackground(null);
        this.V.setBackgroundColor(0);
        this.V.setBackground(null);
        this.W.setBackgroundColor(0);
        this.W.setBackground(null);
        this.v = null;
        this.w = null;
        this.y = null;
        this.C = null;
        this.A = null;
        this.B = null;
        this.D = null;
        this.E = null;
        this.F = null;
        this.G = null;
        this.H = null;
        this.I = null;
        this.J = null;
        this.K = null;
        this.L = null;
        this.S = null;
        this.T = null;
        this.U = null;
        this.V = null;
        this.W = null;
        this.X = null;
        this.Y = null;
        this.Z = null;
        this.aa = null;
        this.ab = null;
        this.ac = null;
        this.ad = null;
        this.ae = null;
        this.af = null;
        this.ag = null;
        this.ah = null;
        this.ai = null;
        this.at = null;
        this.au = null;
        this.av = null;
        this.aw = null;
        this.ax = null;
        this.ay = null;
        this.az = null;
        this.aA = null;
        this.aB = null;
        this.ak = null;
        this.al = null;
        this.am = null;
        this.an = null;
        this.ao = null;
        this.ap = null;
        this.aq = null;
        this.ar = null;
        this.as = null;
        this.aC = null;
        this.aD = null;
        this.aE = null;
        this.aF = null;
        this.aG = null;
        this.aH = null;
        this.aI = null;
        this.aJ = null;
        this.aK = null;
        this.aL = null;
        this.aM = null;
        this.aN = null;
        this.aO = null;
        this.aP = null;
        this.aQ = null;
        this.aR = null;
        this.aS = null;
        this.aT = null;
        this.aV = null;
        this.aW = null;
        this.aX = null;
        this.aZ = null;
        this.ba = null;
        this.bb = null;
        this.bc = null;
        this.be = null;
        this.bd = null;
        this.bf = null;
        this.bg = null;
        this.bh = null;
        this.bi = null;
        this.bj = null;
        this.bk = null;
        this.bl = null;
        this.bm = null;
        this.x = null;
        this.z = null;
        this.aY = null;
        this.bn = null;
        this.bo.setBackground(null);
        this.bo.setImageResource(0);
        this.bp.setText("");
        this.bq.setBackground(null);
        this.bq.setImageResource(0);
        this.br.setBackground(null);
        this.br.setImageResource(0);
        this.bs.setBackground(null);
        this.bs.setImageResource(0);
        this.bt.setText("");
        this.bu = null;
        this.bv = null;
        this.bZ = null;
        this.ca = null;
        this.cb.setText("");
        this.cc.setText("");
        this.cd = null;
        this.ce = null;
        this.cf.setText("");
        this.cg.setText("");
        this.ch = null;
        this.ci = null;
        this.cj = null;
        this.ck = null;
        this.cl = null;
        this.cm = null;
        this.cu.setBackground(null);
        this.cu.setImageResource(0);
        this.cx = null;
        this.cy = null;
        this.cz = null;
        this.cB.setBackground(null);
        this.cB.setImageResource(0);
        this.cC.setBackground(null);
        this.cC.setImageResource(0);
        this.cD = null;
        this.cE.setBackground(null);
        this.cE.setImageResource(0);
        this.cF.setBackground(null);
        this.cF.setImageResource(0);
        this.cG.setText("");
    }

    /* access modifiers changed from: protected */
    public void ag() {
        int c2 = this.d.k.e.c();
        try {
            long length = new File(this.c.c.k.b.f1320a.h(c2, 0)).length();
            a("lFileSize:" + length, 3);
            this.c.c.k.e.a(c2, 0, length);
            this.c.x();
        } catch (NullPointerException e2) {
            e2.printStackTrace();
            a("Skip update data size", 0);
        }
    }

    /* access modifiers changed from: protected */
    public boolean ah() {
        return m(false);
    }

    /* access modifiers changed from: protected */
    public boolean m(boolean z2) {
        if (!z2) {
            b(20507);
        } else if (GeneralFunction.m.a.a() == 0) {
            this.c.c.h.z = d.a(c.f1314a, 0);
        } else {
            if (this.c.c.k.b.ab == null) {
                this.c.c.k.b.ab = ai();
            }
            if (this.c.c.k.b.ab != null) {
                this.c.c.h.z = d.a(this.c.c.k.b.ab, 0);
            }
        }
        if (this.c.c.h.z < 52428800) {
            return true;
        }
        return false;
    }

    private boolean bb() {
        return this.c.c.h.z < 52428800;
    }

    /* access modifiers changed from: protected */
    public void n(boolean z2) {
        if (z2) {
            a(true, getResources().getString(R.string.memory_full));
            this.v.setEnabled(false);
            this.v.setImageResource(R.drawable.shooting_shutterkey_disable);
            return;
        }
        a(false, "");
        this.v.setEnabled(true);
        if (this.c.c.h.T == 0) {
            if (GeneralFunction.m.a.f() == 0) {
                this.v.setImageResource(R.drawable.shooting_shutterkey_photo);
            } else {
                this.v.setImageResource(R.drawable.shooting_shutterkey_photo_selftimer);
            }
        } else if (this.c.c.h.T != 1) {
            this.v.setImageResource(R.drawable.shooting_shutterkey_live);
        } else if (GeneralFunction.m.a.f() == 0) {
            this.v.setImageResource(R.drawable.shooting_shutterkey_video);
        } else {
            this.v.setImageResource(R.drawable.shooting_shutterkey_video_selftimer);
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, boolean z2) {
        this.d.k.b.q = new GeneralFunction.a(this.c, str, 2000, z2);
    }

    /* access modifiers changed from: protected */
    public String ai() {
        return d.a(this);
    }

    /* access modifiers changed from: protected */
    public void o(boolean z2) {
        if (z2) {
            this.w.setImageResource(R.drawable.shooting_settings_red);
            this.bb.setImageResource(R.drawable.shooting_settings_red);
            return;
        }
        this.w.setImageResource(R.drawable.shooting_settings_white);
        this.bb.setImageResource(R.drawable.shooting_settings_white);
    }

    private void bc() {
        if (this.c.a() != 288) {
            if (this.c.a() == 304) {
                this.c.c.h.S = 5;
                a(8778);
            } else {
                b(20508);
            }
        }
        ah();
    }

    /* access modifiers changed from: protected */
    public boolean aj() {
        a("GetPlayerStatus():" + this.f.d(), 3);
        if (this.f.d() == 2 || this.f.d() == 3 || this.f.d() == 4 || this.f.d() == 5) {
            return true;
        }
        return false;
    }

    public void b(String str) {
        if (str != null) {
            a("notifyFileToSystem", 3);
            new File(str);
            GeneralFunction.g.a.a(this, str);
        }
    }

    private void bd() {
        this.cJ = (LinearLayout) findViewById(R.id.LL_ui_live_protect_layout);
        this.cJ.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Liveview.UI_LiveViewController.AnonymousClass63 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void p(boolean z2) {
        if (z2) {
            this.cJ.setVisibility(0);
        } else {
            this.cJ.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void ak() {
        if (this.e != null) {
            this.e.release();
            this.e = null;
        }
    }

    /* access modifiers changed from: protected */
    public void al() {
        PowerManager powerManager = (PowerManager) getSystemService("power");
        if (this.e != null) {
            ak();
        }
        this.e = powerManager.newWakeLock(536870922, "Lexiconda");
        this.e.acquire();
    }
}
