package ui_Controller.ui_Liveview;

import GeneralFunction.d;
import GeneralFunction.e;
import GeneralFunction.o;
import GeneralFunction.r.a;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Message;
import com.huawei.cvIntl60.R;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Date;
import ui_Controller.b.n;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private UI_LiveViewController f1497a = null;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i) {
        d.a("UI_LiveViewBroadcast", str, i);
    }

    public a(UI_LiveViewController uI_LiveViewController) {
        this.f1497a = uI_LiveViewController;
    }

    /* access modifiers changed from: protected */
    public boolean a(String str) {
        AccountManager accountManager = AccountManager.get(this.f1497a.c);
        if (accountManager == null) {
            return false;
        }
        Account[] accounts = accountManager.getAccounts();
        for (Account account : accounts) {
            if (account.name.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public void a(Message message) {
        switch (message.what) {
            case 8719:
                this.f1497a.c.a(352, this.f1497a);
                this.f1497a.a(10756, 0L);
                return;
            case 8741:
                if (this.f1497a.d.h.c == 299) {
                    this.f1497a.a(8785, 0L);
                    return;
                } else {
                    a("error case of btnYoutubeStop ", 4);
                    return;
                }
            case 8779:
                this.f1497a.d.h.d = 305;
                this.f1497a.c.a(352, this.f1497a);
                this.f1497a.a(8785, 0L);
                return;
            case 8780:
                this.f1497a.d.h.d = 306;
                this.f1497a.c.a(352, this.f1497a);
                this.f1497a.a(8785, 0L);
                return;
            case 8785:
                this.f1497a.c.a(352, this.f1497a);
                this.f1497a.a(8785, 0L);
                return;
            case 8788:
                this.f1497a.al();
                this.f1497a.b(true, this.f1497a.getResources().getString(R.string.broadcast_wait_active));
                this.f1497a.c.c(new a.c.a(18450));
                this.f1497a.a(new a.c.a(10763));
                this.f1497a.d.h.c = 298;
                a();
                a(10759, this.f1497a.d.h.aR, this.f1497a.d.h.aS);
                b();
                return;
            case 8792:
                this.f1497a.a(true);
                return;
            case 8793:
                this.f1497a.c.a(352, this.f1497a);
                this.f1497a.a(8793, 0L);
                return;
            case 8797:
                if (this.f1497a.c.d.b()) {
                    this.f1497a.c.d.a(new a.c.a(18455));
                }
                this.f1497a.b(false, "");
                this.f1497a.b(true, false);
                this.f1497a.J();
                this.f1497a.a(8846);
                this.f1497a.a(8785);
                return;
            case 8825:
                if (this.f1497a.c.c.h.c == 299) {
                    this.f1497a.b(true, this.f1497a.d.h.h);
                    return;
                }
                return;
            case 8846:
                o.b(this.f1497a);
                o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.low_battery), this.f1497a.getResources().getString(R.string.low_battery_content), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                this.f1497a.c.c.g.f1324a = 1036;
                return;
            case 10776:
                this.f1497a.c.a(272, this.f1497a);
                return;
            case 10837:
            case 10838:
            case 10839:
                a.c.a aVar = new a.c.a(10760);
                aVar.a("StreamingType", 1);
                aVar.a("Width", 1440);
                aVar.a("Height", 720);
                aVar.a("AudioSampleRate", 44100);
                aVar.a("SpsPpsBuffer", n.c);
                aVar.a("SpsPpsSize", 27);
                aVar.a("VideoFps", 30);
                aVar.a("VideoBitrate", 3145728);
                aVar.a("VideoGOP", 15);
                this.f1497a.a(aVar);
                return;
            case 10840:
                this.f1497a.b(false, "");
                this.f1497a.z();
                this.f1497a.d(true);
                return;
            case 10842:
                if (!new a.c.a(message).a("havePermission")) {
                    this.f1497a.b(false, "");
                    return;
                }
                return;
            case 10845:
                this.f1497a.d.h.c = 299;
                this.f1497a.a(8737);
                return;
            case 10846:
                a.c.a aVar2 = new a.c.a(message);
                this.f1497a.c.c.h.aH = aVar2.d("streamingLink");
                a("[YOUTUBE TEST] Link: " + this.f1497a.c.c.h.aH, 0);
                return;
            case 10917:
            case 10918:
                this.f1497a.d.h.c = 288;
                this.f1497a.b(false, "");
                c();
                a.c.a aVar3 = new a.c.a(message);
                if (aVar3.b("youtubeApiResult") == -2) {
                    o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_client_error_title), this.f1497a.getResources().getString(R.string.broadcast_client_error_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                } else if (aVar3.b("youtubeApiResult") == -3) {
                    o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_server_error_title), this.f1497a.getResources().getString(R.string.broadcast_server_error_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                } else if (aVar3.b("youtubeApiResult") == -5) {
                    o.a(this.f1497a, true, true, this.f1497a.getResources().getString(R.string.youtube_live_not_enable_title), this.f1497a.getResources().getString(R.string.youtube_live_not_enable_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039, 12039, false, true);
                } else if (aVar3.b("youtubeApiResult") == -6) {
                    o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_title_invalid_title), this.f1497a.getResources().getString(R.string.broadcast_title_invalid_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                } else {
                    o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_internet_error_title), this.f1497a.getResources().getString(R.string.broadcast_internet_error_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                }
                this.f1497a.c.a(272, this.f1497a);
                return;
            case 10921:
                this.f1497a.c.a(352, this.f1497a);
                this.f1497a.a(8743, 0L);
                return;
            case 10922:
                this.f1497a.c.a(352, this.f1497a);
                this.f1497a.a(8747, 0L);
                return;
            case 32768:
                a("MSG_KEY_BACK youtube", 4);
                if (this.f1497a.l()) {
                    this.f1497a.a(false);
                } else if (!this.f1497a.F()) {
                    o.a((Context) this.f1497a, true, false, this.f1497a.getResources().getString(R.string.broadcast_check_stop_title), this.f1497a.getResources().getString(R.string.broadcast_check_stop_description), new String[]{this.f1497a.getResources().getString(R.string.dialog_option_no), this.f1497a.getResources().getString(R.string.dialog_option_yes)}, new int[]{0, 8785}, true);
                }
                this.f1497a.c.c(268435455);
                return;
            default:
                return;
        }
    }

    public void b(Message message) {
        switch (message.what) {
            case 8743:
                this.f1497a.d.h.c = 288;
                o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_no_internet_title), this.f1497a.getResources().getString(R.string.broadcast_no_internet_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039, true);
                a.c.a aVar = new a.c.a(10761);
                aVar.a("StreamingType", 1);
                this.f1497a.a(aVar);
                this.f1497a.a(false, false);
                this.f1497a.d(false);
                this.f1497a.c.a(272, this.f1497a);
                this.f1497a.L();
                a();
                c();
                this.f1497a.c.c(new a.c.a(18450));
                return;
            case 8747:
                this.f1497a.d.h.c = 288;
                o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_internet_error_title), this.f1497a.getResources().getString(R.string.broadcast_internet_error_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                this.f1497a.b(false, "");
                this.f1497a.d(false);
                this.f1497a.c.a(272, this.f1497a);
                c();
                this.f1497a.L();
                this.f1497a.c.c(new a.c.a(18450));
                return;
            case 8785:
                this.f1497a.ak();
                this.f1497a.d.h.c = 288;
                this.f1497a.b(true, this.f1497a.getResources().getString(R.string.broadcast_stop_busy));
                a.c.a aVar2 = new a.c.a(10761);
                aVar2.a("StreamingType", 1);
                this.f1497a.a(aVar2);
                this.f1497a.a(false, false);
                a();
                c();
                this.f1497a.c.c(new a.c.a(18450));
                return;
            case 8793:
                this.f1497a.a(8785);
                this.f1497a.d(false);
                a();
                this.f1497a.L();
                this.f1497a.b(false, "");
                this.f1497a.f(false);
                this.f1497a.g(false);
                return;
            case 10836:
                this.f1497a.c.a(272, this.f1497a);
                this.f1497a.c.c(268435455);
                return;
            case 10841:
            case 10919:
                this.f1497a.b(false, "");
                this.f1497a.d(false);
                a();
                this.f1497a.c.a(272, this.f1497a);
                this.f1497a.L();
                return;
            default:
                return;
        }
    }

    public void c(Message message) {
        switch (message.what) {
            case 8741:
                if (this.f1497a.d.h.c == 299) {
                    this.f1497a.a(8796, 0L);
                    return;
                } else {
                    a("error case of btnfacebookStop ", 4);
                    return;
                }
            case 8779:
                this.f1497a.d.h.d = 305;
                this.f1497a.c.a(432, this.f1497a);
                this.f1497a.a(8796, 0L);
                return;
            case 8780:
                this.f1497a.d.h.d = 306;
                this.f1497a.c.a(432, this.f1497a);
                this.f1497a.a(8796, 0L);
                return;
            case 8793:
                this.f1497a.c.a(432, this.f1497a);
                this.f1497a.a(8793, 0L);
                return;
            case 8794:
                this.f1497a.al();
                this.f1497a.b(true, this.f1497a.getResources().getString(R.string.broadcast_wait_active));
                this.f1497a.d.h.c = 298;
                this.f1497a.c.c(new a.c.a(18450));
                a.c.a aVar = new a.c.a(10754);
                aVar.a("liveStreamDescription", this.f1497a.d.h.aQ);
                this.f1497a.a(aVar);
                a();
                b();
                return;
            case 8796:
                this.f1497a.c.a(432, this.f1497a);
                this.f1497a.a(8796, 0L);
                return;
            case 8797:
                if (this.f1497a.c.d.b()) {
                    this.f1497a.c.d.a(new a.c.a(18455));
                }
                this.f1497a.b(false, "");
                this.f1497a.b(true, false);
                this.f1497a.J();
                this.f1497a.a(8846);
                this.f1497a.a(8796);
                return;
            case 8825:
                if (this.f1497a.c.c.h.c == 299) {
                    this.f1497a.b(true, this.f1497a.d.h.h);
                    return;
                }
                return;
            case 8846:
                o.b(this.f1497a);
                o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.low_battery), this.f1497a.getResources().getString(R.string.low_battery_content), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                this.f1497a.c.c.g.f1324a = 1036;
                return;
            case 10776:
                this.f1497a.c.a(272, this.f1497a);
                return;
            case 10833:
                this.f1497a.c.a(272, this.f1497a);
                return;
            case 10834:
                a.c.a aVar2 = new a.c.a(message);
                a.c.a aVar3 = new a.c.a(10764);
                aVar3.a("StreamingType", 1);
                aVar3.a("Width", 1440);
                aVar3.a("Height", 720);
                aVar3.a("AudioSampleRate", 44100);
                aVar3.a("SpsPpsBuffer", n.c);
                aVar3.a("SpsPpsSize", 27);
                aVar3.a("VideoFps", 30);
                aVar3.a("VideoBitrate", 3145728);
                aVar3.a("VideoGOP", 15);
                aVar3.a("rtmpURL", aVar2.d("rtmpURL"));
                this.f1497a.a(aVar3);
                return;
            case 10845:
                this.f1497a.d.h.c = 299;
                this.f1497a.a(8737);
                this.f1497a.b(false, "");
                this.f1497a.z();
                this.f1497a.d(true);
                return;
            case 10913:
                this.f1497a.d.h.c = 288;
                this.f1497a.b(false, "");
                c();
                a.c.a aVar4 = new a.c.a(message);
                if (aVar4.b("httpStatusCode") >= 500) {
                    o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_server_error_title), this.f1497a.getResources().getString(R.string.broadcast_server_error_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                } else if (aVar4.b("httpStatusCode") >= 400) {
                    o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_client_error_title), this.f1497a.getResources().getString(R.string.broadcast_client_error_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                } else {
                    o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_internet_error_title), this.f1497a.getResources().getString(R.string.broadcast_internet_error_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                }
                this.f1497a.c.a(272, this.f1497a);
                return;
            case 10921:
                this.f1497a.c.a(432, this.f1497a);
                this.f1497a.a(8743, 0L);
                return;
            case 10922:
                this.f1497a.c.a(352, this.f1497a);
                this.f1497a.a(8747, 0L);
                return;
            case 32768:
                a("MSG_KEY_BACK facebook", 4);
                if (!this.f1497a.F()) {
                    o.a((Context) this.f1497a, true, false, this.f1497a.getResources().getString(R.string.broadcast_check_stop_title), this.f1497a.getResources().getString(R.string.broadcast_check_stop_description), new String[]{this.f1497a.getResources().getString(R.string.dialog_option_no), this.f1497a.getResources().getString(R.string.dialog_option_yes)}, new int[]{0, 8796}, true);
                }
                this.f1497a.c.c(268435455);
                return;
            default:
                return;
        }
    }

    public void d(Message message) {
        switch (message.what) {
            case 8743:
                this.f1497a.d.h.c = 288;
                o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_no_internet_title), this.f1497a.getResources().getString(R.string.broadcast_no_internet_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039, true);
                a.c.a aVar = new a.c.a(10773);
                aVar.a("StreamingType", 1);
                this.f1497a.a(aVar);
                this.f1497a.a(false, false);
                this.f1497a.d(false);
                this.f1497a.c.a(272, this.f1497a);
                this.f1497a.L();
                a();
                c();
                this.f1497a.c.c(new a.c.a(18450));
                return;
            case 8747:
                this.f1497a.d.h.c = 288;
                o.a((Context) this.f1497a, true, true, this.f1497a.getResources().getString(R.string.broadcast_internet_error_title), this.f1497a.getResources().getString(R.string.broadcast_internet_error_description), this.f1497a.getResources().getString(R.string.dialog_option_ok), 12039);
                this.f1497a.b(false, "");
                this.f1497a.d(false);
                this.f1497a.c.a(272, this.f1497a);
                c();
                this.f1497a.L();
                this.f1497a.c.c(new a.c.a(18450));
                return;
            case 8793:
                this.f1497a.a(8796);
                this.f1497a.d(false);
                a();
                this.f1497a.L();
                this.f1497a.b(false, "");
                this.f1497a.f(false);
                this.f1497a.g(false);
                return;
            case 8796:
                this.f1497a.ak();
                this.f1497a.d.h.c = 288;
                this.f1497a.b(true, this.f1497a.getResources().getString(R.string.broadcast_stop_busy));
                a.c.a aVar2 = new a.c.a(10773);
                aVar2.a("StreamingType", 1);
                this.f1497a.a(aVar2);
                this.f1497a.a(false, false);
                a();
                c();
                this.f1497a.c.c(new a.c.a(18450));
                return;
            case 10844:
                this.f1497a.b(false, "");
                this.f1497a.d(false);
                this.f1497a.c.a(272);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f1497a.d.h.K = false;
        this.f1497a.d.h.L = false;
        this.f1497a.d.h.M.clear();
        this.f1497a.d.h.N = 0;
        this.f1497a.d.h.P = 0;
        this.f1497a.d.h.O = 0;
        this.f1497a.d.h.Q = 0;
    }

    /* access modifiers changed from: protected */
    public void a(int i, String str, String str2) {
        a("CreateYoutubeType", 3);
        a.c.a aVar = new a.c.a(i);
        if (i == 10759) {
            a("MSG_UI_LIVE_STREAMING_YOUTUBE_CREATE_360_EVENT", 3);
            String date = new Date().toString();
            if (!str.equals("")) {
                aVar.a("liveStreamTitle", str);
            } else {
                aVar.a("liveStreamTitle", "360 Live Stream - " + date);
            }
            if (!str2.equals("")) {
                aVar.a("liveStreamDescription", str2);
            }
            aVar.a("streamFormat", "720p");
        } else if (i == 10758) {
            a("MSG_UI_LIVE_STREAMING_YOUTUBE_CREATE_EVENT", 3);
            aVar.a("liveStreamTitle", "Live Stream - " + new Date().toString());
            aVar.a("streamFormat", "720p");
        }
        this.f1497a.c.a(aVar, 0);
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.f1497a.d.h.an = new GeneralFunction.r.a(new a.AbstractC0005a() {
            /* class ui_Controller.ui_Liveview.a.AnonymousClass1 */

            @Override // GeneralFunction.r.a.AbstractC0005a
            public void a(MediaFormat mediaFormat) {
            }

            @Override // GeneralFunction.r.a.AbstractC0005a
            public void a(ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) {
                if (byteBuffer != null) {
                    byteBuffer.position(bufferInfo.offset);
                    byte[] bArr = new byte[bufferInfo.size];
                    byteBuffer.get(bArr, bufferInfo.offset, bufferInfo.size);
                    a.this.a((a) new GeneralFunction.c.d(bArr), (GeneralFunction.c.d) bufferInfo);
                    return;
                }
                a.this.a((a) ("OnAudioFrameReady receive objAudioData is null:" + bufferInfo), (String) 0);
            }

            @Override // GeneralFunction.r.a.AbstractC0005a
            public void a() {
                a.this.f1497a.c.c.h.an.c();
                a.this.f1497a.c.c.h.an.a((a.AbstractC0005a) null);
            }
        });
        this.f1497a.d.h.an.a();
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.f1497a.c.c.h.an != null) {
            this.f1497a.c.c.h.an.b();
        }
    }

    /* access modifiers changed from: protected */
    public void a(a.a.a.d dVar) {
        int i = this.f1497a.d.h.c;
        boolean b = e.b(dVar.f169a.a());
        if (i == 299 || i == 300) {
            boolean z = false;
            long j = 0;
            if (this.f1497a.d.h.L) {
                z = true;
                j = this.f1497a.d.h.N + ((this.f1497a.d.h.O * 1000) / 30);
                if (this.f1497a.d.h.O % 60 == 0) {
                    a("[Live DBG]RECEIVE video frame number:" + this.f1497a.d.h.O, 2);
                    a("[Live DBG] Video pts: " + j, 3);
                    a("[Live DBG] Audio pts: " + this.f1497a.d.h.Q, 3);
                }
            } else if (b) {
                a("[Live DBG] Send first I frame", 2);
                this.f1497a.d.h.L = true;
                this.f1497a.d.h.N = dVar.f169a.b - 166;
                z = true;
                j = this.f1497a.d.h.N + ((this.f1497a.d.h.O * 1000) / 30);
            }
            if (z) {
                if (this.f1497a.d.h.K) {
                    if (i == 299 && b) {
                        this.f1497a.d.h.K = false;
                    }
                } else if (i == 300 && b) {
                    this.f1497a.d.h.K = true;
                }
                if (this.f1497a.d.h.K) {
                    InputStream openRawResource = this.f1497a.getResources().openRawResource(R.raw.h264_360test_960p_0001 + ((int) (this.f1497a.d.h.O % 15)));
                    try {
                        GeneralFunction.c.d dVar2 = new GeneralFunction.c.d(new byte[openRawResource.available()]);
                        openRawResource.read(dVar2.a());
                        b(dVar2, j);
                    } catch (IOException e) {
                        a("***FAKE FRAME IO ERROR***", 0);
                    }
                    try {
                        openRawResource.close();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                } else {
                    b(dVar.f169a, j);
                }
                this.f1497a.d.h.O++;
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(GeneralFunction.c.d dVar, MediaCodec.BufferInfo bufferInfo) {
        if (this.f1497a.d.h.c == 299 || this.f1497a.d.h.c == 300) {
            dVar.f = bufferInfo.presentationTimeUs / 1000;
            this.f1497a.d.h.Q = dVar.f;
            this.f1497a.d.h.M.add(dVar);
            if (this.f1497a.d.h.L) {
                for (int i = 0; i < this.f1497a.d.h.M.size(); i++) {
                    GeneralFunction.c.d dVar2 = this.f1497a.d.h.M.get(0);
                    this.f1497a.d.h.M.remove(0);
                    if (dVar2.f >= this.f1497a.d.h.N) {
                        a(dVar2, dVar2.f);
                    }
                }
            }
            this.f1497a.d.h.P++;
        }
    }

    /* access modifiers changed from: protected */
    public void a(GeneralFunction.c.d dVar, long j) {
        this.f1497a.c.h().a(dVar, j);
    }

    /* access modifiers changed from: protected */
    public void b(GeneralFunction.c.d dVar, long j) {
        this.f1497a.c.h().b(dVar, j);
    }
}
