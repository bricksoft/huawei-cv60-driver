package ui_Controller.ui_Liveview;

import GeneralFunction.d;
import GeneralFunction.e;
import GeneralFunction.g;
import GeneralFunction.n;
import GeneralFunction.o;
import GeneralFunction.p;
import a.c.a;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Message;
import android.os.Process;
import com.google.api.client.http.HttpStatusCodes;
import com.huawei.cvIntl60.R;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import tv.danmaku.ijk.media.player.IjkMediaMeta;
import ui_Controller.a.c;
import ui_Controller.b.k;
import ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController;
import ui_Controller.ui_Setting.UI_SettingMenuController;

public class b {

    /* renamed from: a  reason: collision with root package name */
    Integer f1499a = 0;
    private UI_LiveViewController b = null;
    private boolean c = false;
    private boolean d = false;
    private boolean e = false;

    private void a(String str, int i) {
        d.a("UI_LiveViewHandler", str, i);
    }

    public b(UI_LiveViewController uI_LiveViewController) {
        this.b = uI_LiveViewController;
    }

    @SuppressLint({"Wakelock"})
    public void a(Message message) {
        a aVar = new a(message);
        switch (message.what) {
            case 8449:
                this.b.c.c.f = false;
                this.b.c.a(this.b);
                this.b.c.a(0, (GeneralFunction.a.a) null);
                this.b.c.b(268435455L);
                return;
            case 8452:
                o.b(this.b);
                this.b.c.c.g.f1324a = 0;
                if (this.b.f == null || !this.b.aj()) {
                    this.b.a(8452, 50L);
                    return;
                }
                this.b.o();
                if (this.b.c.c.h.aI != null) {
                    if (!this.b.c.c.h.aI.isRecycled()) {
                        this.b.c.c.h.aI.recycle();
                    }
                    this.b.c.c.h.aI = null;
                }
                if (this.b.c.d.b()) {
                    this.b.c.c.h.aI = this.b.f.getBitmap();
                    this.b.a(true, this.b.c.c.h.aI);
                }
                this.b.f.k();
                this.b.f.i();
                this.b.c.c(new a(18443));
                this.b.c.c.h.at = false;
                this.c = false;
                this.d = false;
                this.e = false;
                this.b.j();
                this.b.c.c.h.d = 305;
                a aVar2 = new a(8735);
                aVar2.a("nextActivity", 1024);
                this.b.a(aVar2);
                return;
            case 8454:
                o.b(this.b);
                this.b.c.c.g.f1324a = 0;
                if (this.b.f == null || !this.b.aj()) {
                    this.b.a(8454, 50L);
                    return;
                }
                this.b.o();
                if (this.b.c.c.h.aI != null) {
                    if (!this.b.c.c.h.aI.isRecycled()) {
                        this.b.c.c.h.aI.recycle();
                    }
                    this.b.c.c.h.aI = null;
                }
                if (this.b.c.d.b()) {
                    this.b.c.c.h.aI = this.b.f.getBitmap();
                    this.b.a(true, this.b.c.c.h.aI);
                }
                this.b.f.k();
                this.b.f.i();
                this.b.c.c(new a(18443));
                this.b.c.c.h.at = false;
                this.c = false;
                this.d = false;
                this.e = false;
                this.b.j();
                this.b.c.c.h.d = HttpStatusCodes.STATUS_CODE_NOT_MODIFIED;
                a aVar3 = new a(8735);
                aVar3.a("nextActivity", 2064);
                this.b.a(aVar3);
                return;
            case 8704:
                if (this.b.d.h.F.size() > 0) {
                    this.b.a(8717, 0L);
                }
                this.b.g(64);
                return;
            case 8715:
            case 12038:
            case 12046:
            case 12047:
            case 12048:
                return;
            case 8716:
                a("MSG_UI_VIEW_EVENT_TIME_OUT_STOP", 3);
                g.a(2);
                return;
            case 8718:
                a("MSG_UI_VIEW_360_DECODER_STATUS_CHANGE", 3);
                int b2 = aVar.b("sphericalVideoPlayerStatus");
                if (this.b.d.h.d != 305 && this.b.d.h.d != 304) {
                    if (b2 == 1) {
                        if (this.f1499a.intValue() != 2 && this.f1499a.intValue() != 1) {
                            this.f1499a = 1;
                            a("MSG_UI_VIEW_360_DECODER_STATUS_CHANGE STAND_BY", 3);
                            this.b.d.h.q = 2;
                            if (GeneralFunction.m.a.g() == 0) {
                                if (this.b.c.c.h.U == 0) {
                                    this.b.f.setIsShowThumbNail(false);
                                } else {
                                    this.b.f.setIsShowThumbNail(true);
                                }
                            }
                            this.b.f.setPlayerMode(2);
                            if (this.b.d.h.q == 0) {
                                this.b.f.setViewType(4);
                                this.b.f.setInteractiveMode(0);
                            } else if (this.b.d.h.q == 1) {
                                this.b.f.setViewType(1);
                                this.b.f.setInteractiveMode(3);
                            } else if (this.b.d.h.q == 2) {
                                this.b.f.setViewType(3);
                                this.b.f.setInteractiveMode(1);
                            }
                            if (this.b.c.c.h.av != null) {
                                a("set objActivity.objModeMain.gsUI.sView.sCurrentSphericalParameter.playerFormat: " + this.b.c.c.h.av.m, 3);
                                a("set objActivity.objModeMain.gsUI.sView.sCurrentSphericalParameter.focalLength: " + this.b.c.c.h.av.l, 3);
                                a("set objActivity.objModeMain.gsUI.sView.sCurrentSphericalParameter.xAxis: " + this.b.c.c.h.av.q, 3);
                                a("set objActivity.objModeMain.gsUI.sView.sCurrentSphericalParameter.yAxis: " + this.b.c.c.h.av.r, 3);
                                this.b.f.setSphericalParameter(this.b.c.c.h.av);
                            }
                            if (GeneralFunction.m.a.d() == 1) {
                                this.b.f.b(1920, 960);
                            } else {
                                this.b.f.b(1280, 640);
                            }
                            this.b.f.g();
                            if (this.b.c.a() != 288) {
                                this.b.J();
                                return;
                            }
                            return;
                        }
                        return;
                    } else if (b2 == 0) {
                        this.f1499a = 0;
                        a("MSG_UI_VIEW_360_DECODER_STATUS_CHANGE NOT_READY", 3);
                        this.b.p();
                        return;
                    } else if (b2 == 2) {
                        this.f1499a = 2;
                        a("MSG_UI_VIEW_360_DECODER_STATUS_CHANGE PLAY_READY", 3);
                        return;
                    } else if (b2 == 3) {
                        this.f1499a = 3;
                        a("MSG_UI_VIEW_360_DECODER_STATUS_CHANGE PLAY_COMPLETE", 3);
                        return;
                    } else if (b2 == 4) {
                        this.f1499a = 4;
                        a("MSG_UI_VIEW_360_DECODER_STATUS_CHANGE STOP", 3);
                        return;
                    } else if (b2 == 5) {
                        this.f1499a = 5;
                        a("MSG_UI_VIEW_360_DECODER_STATUS_CHANGE DESTROY", 3);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 8730:
                if (this.b.c.a() == 288) {
                    a("new capture, no need to enable leave button", 2);
                } else {
                    a("all main pic downloaded, enable leave button", 2);
                    this.b.J();
                }
                if (this.b.g()) {
                    a("[CaptureDBG] Download complete, init live view", 1);
                    this.b.c.l();
                    return;
                } else if (this.b.c.a() != 288) {
                    a("[CaptureDBG] Download complete and capture done, close communication", 1);
                    this.b.c.c(new a(18443), 0);
                    return;
                } else {
                    return;
                }
            case 8732:
                a aVar4 = new a(18519);
                a("MSG_REMOTE_USB_CMD_WRITE_SET_ALL_SETTING PhotoResolutionType: " + GeneralFunction.m.a.c(), 3);
                a("MSG_REMOTE_USB_CMD_WRITE_SET_ALL_SETTING VideoResolutionType: " + GeneralFunction.m.a.b(), 3);
                a("MSG_REMOTE_USB_CMD_WRITE_SET_ALL_SETTING EvbValue: " + GeneralFunction.m.a.n(), 3);
                a("MSG_REMOTE_USB_CMD_WRITE_SET_ALL_SETTING WbValue: " + GeneralFunction.m.a.k(), 3);
                a("MSG_REMOTE_USB_CMD_WRITE_SET_ALL_SETTING FilterValue: " + GeneralFunction.m.a.l(), 3);
                aVar4.a("photo_resolution", a.c.b.e(GeneralFunction.m.a.c()));
                aVar4.a("video_resolution", a.c.b.f(GeneralFunction.m.a.b()));
                aVar4.a("evb", a.c.b.b(GeneralFunction.m.a.n()));
                aVar4.a("white_balance", a.c.b.c(GeneralFunction.m.a.o()));
                aVar4.a("filter", a.c.b.d(GeneralFunction.m.a.p()));
                aVar4.a("logo_type", a.c.b.h(GeneralFunction.m.a.i()));
                if (this.b.c.a() == 336 || this.b.c.a() == 416) {
                    aVar4.a(IjkMediaMeta.IJKM_KEY_BITRATE, a.c.b.i(GeneralFunction.m.a.m()));
                } else if (GeneralFunction.m.a.d() == 0) {
                    aVar4.a(IjkMediaMeta.IJKM_KEY_BITRATE, 8);
                } else {
                    aVar4.a(IjkMediaMeta.IJKM_KEY_BITRATE, 16);
                }
                a("MSG_REMOTE_USB_CMD_WRITE_SET_ALL_SETTING Bitrate: " + aVar4.b(IjkMediaMeta.IJKM_KEY_BITRATE), 0);
                a.a.a.b bVar = new a.a.a.b();
                Date date = new Date(System.currentTimeMillis());
                bVar.f167a = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.US).format(date));
                bVar.b = Byte.parseByte(new SimpleDateFormat("MM", Locale.US).format(date));
                bVar.c = Byte.parseByte(new SimpleDateFormat("dd", Locale.US).format(date));
                bVar.d = Byte.parseByte(new SimpleDateFormat("HH", Locale.US).format(date));
                bVar.e = Byte.parseByte(new SimpleDateFormat("mm", Locale.US).format(date));
                bVar.f = Byte.parseByte(new SimpleDateFormat("ss", Locale.US).format(date));
                bVar.h = Integer.parseInt(new SimpleDateFormat("SSS", Locale.US).format(date));
                a("Set DateTime: " + bVar.f167a + "/" + ((int) bVar.b) + "/" + ((int) bVar.c) + " " + ((int) bVar.d) + ":" + ((int) bVar.e) + ":" + ((int) bVar.f), 2);
                aVar4.a("DateTime", new a.C0010a(bVar));
                this.b.c.c(aVar4);
                return;
            case 8733:
                a("MSG_UI_VIEW_USB_CONNECTION_ERROR!", 0);
                if (this.b.c.d.b()) {
                    o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.connection_error_title), this.b.getResources().getString(R.string.connection_usb_communication_error), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                    this.b.b(false, "");
                    this.b.a(true, this.b.getResources().getString(R.string.no_camera));
                    this.b.b(true, true);
                    return;
                }
                return;
            case 8734:
            case 12034:
                a("MSG_UI_SYSTEM_ACTIVITY_RESUME:" + this.b.c.a(), 0);
                if (message.what == 12034) {
                    this.b.h();
                    this.b.b(20507);
                }
                if (this.b.c.a() == 416 || this.b.c.a() == 336) {
                    this.b.al();
                }
                if (this.b.c.a() == 305) {
                    this.b.c.c.h.au = true;
                } else {
                    if (this.b.c.c.d) {
                        this.b.c.c.d = false;
                        this.b.c.i();
                        if (this.b.v()) {
                            this.b.e(false);
                        }
                        if (this.b.y()) {
                            this.b.f(false);
                            this.b.g(false);
                        }
                    } else {
                        a();
                    }
                    this.b.T();
                }
                if (!this.b.c.h().g().c()) {
                    a("Google account not exist", 0);
                    this.b.c.h().a((String) null);
                    this.b.c.h().a(false);
                    return;
                }
                return;
            case 8735:
                if (this.b.f.d() == 5 || this.b.f.d() == 1) {
                    a("OK free done", 3);
                    if (aVar.b("nextActivity") != 1024) {
                        this.b.c.c.f = false;
                        this.b.c.a(2048, this.b, new Intent(this.b, UI_SettingMenuController.class));
                        this.b.c.a(0, (GeneralFunction.a.a) null);
                        this.b.c.b(268435455L);
                        return;
                    } else if (!this.b.d.f) {
                        this.b.a(aVar, 100L);
                        return;
                    } else {
                        this.b.c.c.f = false;
                        this.b.c.a(1024, this.b, new Intent(this.b, UI_PhoneGalleryController.class));
                        this.b.c.b(268435455L);
                        return;
                    }
                } else {
                    this.b.a(aVar, 30L);
                    return;
                }
            case 8737:
                if (this.b.g()) {
                    this.b.a(false, (Bitmap) null);
                    this.b.c.c.h.aJ = true;
                }
                this.b.ah();
                return;
            case 8738:
                this.b.a((Bitmap) aVar.c());
                return;
            case 8739:
                if (this.b.c.a() != 288 && this.b.c.f() == 0) {
                    this.b.c.c(new a(18459));
                }
                this.b.a(8732);
                return;
            case 8744:
                if (!this.b.c.d.b()) {
                    return;
                }
                if (aVar.a("isMemoryFull")) {
                    if (this.b.c.c.h.T != 2) {
                        this.b.n(true);
                        return;
                    } else {
                        this.b.n(false);
                        return;
                    }
                } else if (this.b.c.c.h.T != 2) {
                    this.b.n(false);
                    return;
                } else {
                    return;
                }
            case 8746:
                if (this.b.c.d.b()) {
                    if (this.b.D()) {
                        this.b.b(false, false);
                        this.b.d.e = true;
                        this.b.ah();
                    }
                    if (this.b.E() && this.b.c.a() == 272) {
                        this.b.b(false, "");
                    }
                    if (this.b.c.c.h.T == 1 || this.b.c.c.h.T == 2) {
                        this.b.a(8800);
                        return;
                    }
                    return;
                }
                return;
            case 8748:
            case 8749:
                if (this.b.f != null) {
                    this.b.f.j();
                    return;
                }
                return;
            case 8840:
                this.b.s();
                this.b.a(false, true);
                o.b(this.b);
                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.error), this.b.getResources().getString(R.string.disconnect_msg), this.b.getResources().getString(R.string.dialog_option_ok), 8452);
                this.b.c.c.g.f1324a = 3;
                return;
            case 10832:
                a("MSG_UI_LIVE_STREAMING_FACEBOOK_LOGIN_DONE", 3);
                this.b.b(false, "");
                if (!this.b.c.d.b()) {
                    a("USB detached, ignore facebook login done", 0);
                    return;
                } else {
                    d();
                    return;
                }
            case 10835:
                a("MSG_UI_LIVE_STREAMING_GOOGLE_LOGIN_DONE", 3);
                this.b.b(false, "");
                if (!this.b.c.d.b()) {
                    a("USB detached, ignore youtube login done", 0);
                    return;
                } else {
                    d();
                    return;
                }
            case 10912:
                a("MSG_UI_LIVE_STREAMING_FACEBOOK_LOGIN_FAIL", 3);
                this.b.c.a(272, this.b);
                this.b.c(true);
                return;
            case 10914:
                a("MSG_UI_LIVE_STREAMING_GOOGLE_LOGIN_FAIL", 3);
                this.b.c.a(272, this.b);
                this.b.c(true);
                return;
            case 12033:
                a("MSG_UI_SYSTEM_ACTIVITY_INIT", 3);
                GeneralFunction.m.a.v();
                n.a().a(this.b);
                this.b.c.c(268435455);
                if (this.b.c.c.h.T != 1) {
                    if (this.b.c.c.h.T == 0) {
                        this.b.a(1, 0, 0);
                    } else {
                        this.b.a(0, 2, 0);
                    }
                }
                this.b.h(this.b.c.c.h.T);
                return;
            case 12035:
                a("MSG_UI_SYSTEM_ACTIVITY_PAUSE", 0);
                this.b.ak();
                this.b.j();
                GeneralFunction.m.a.a(true);
                if (this.b.c.a() == 272) {
                    if (this.b.c.c.h.Z) {
                        this.b.c.c.h.aa = true;
                        this.b.c.c.h.ab.cancel();
                    }
                    if (this.b.c.d.b()) {
                        if (this.b.c.f() == 0) {
                            this.b.c.c(new a(18443));
                        }
                        this.b.c.c.h.ak = 0;
                    }
                    if (this.b.d.h.am != null) {
                        this.b.ak();
                        this.b.R();
                        this.b.a(false, 0);
                        this.b.r();
                        this.b.a(false, true);
                        this.b.l(false);
                    }
                } else if (this.b.c.a() == 304) {
                    this.b.c.c.h.S = 2;
                    this.b.c.c.h.y = true;
                    this.b.a(8778);
                } else if (this.b.c.a() == 305) {
                    this.b.c.c.h.au = false;
                    this.b.c.c.h.y = true;
                    if (this.b.c.c.h.x) {
                        this.b.ac();
                    }
                } else if (this.b.c.a() == 400) {
                    this.b.a(8790);
                }
                if (this.b.c.d.b()) {
                    if (!(this.b.c.a() == 288 || this.b.c.f() != 0 || this.b.c.a() == 336 || this.b.c.a() == 352 || this.b.c.a() == 416 || this.b.c.a() == 432)) {
                        this.b.c.c(new a(18443));
                    }
                    this.b.c.c.h.ak = 0;
                }
                this.b.o();
                return;
            case 12036:
                a("ACTIVITY_STOP", 3);
                return;
            case 12037:
                if (this.b.c.d.b()) {
                    a("Send Close communication at onDestroy", 0);
                    this.b.c.c(new a(18443));
                }
                a("AAA_ACTIVITY_DESTROY", 3);
                return;
            case 12039:
                this.b.c.c.g.f1324a = 0;
                if (!(this.b.c.a() == 304 || this.b.c.a() == 336 || this.b.c.a() == 416)) {
                    this.b.c.a(272, this.b);
                }
                o.b(this.b);
                this.b.c.c(268435455);
                return;
            case 12042:
                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.warning), this.b.getResources().getString(R.string.permission_always_deny_msg), this.b.getResources().getString(R.string.dialog_option_ok), 12032);
                return;
            case 12049:
                if (!GeneralFunction.m.a.t()) {
                    this.b.i();
                    return;
                } else {
                    GeneralFunction.m.a.b(true);
                    return;
                }
            case 17921:
                a("MSG_REMOTE_EVENT_POWER_OFF", 4);
                return;
            case 17927:
                a("MSG_REMOTE_EVENT_LOW_BATTERY", 4);
                GeneralFunction.m.a.C(0);
                return;
            case 17928:
                a("MSG_REMOTE_EVENT_MEMORY_FULL", 4);
                return;
            case 17938:
                a("MSG_REMOTE_EVENT_CAMERA_OVERHEAT", 4);
                if (aVar.b("value") == 1 && !this.c) {
                    this.c = true;
                    o.a((Context) this.b, true, true, this.b.getResources().getString(R.string.camera_overheat), this.b.getResources().getString(R.string.camera_overheat_first_stage), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                    return;
                } else if (aVar.b("value") == 2 && !this.e) {
                    this.e = true;
                    a("Camera overheat, auto power off", 0);
                    o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_overheat), this.b.getResources().getString(R.string.camera_overheat_second_stage), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                    this.b.c.c(new a(18455), 2000);
                    this.b.a(8741);
                    return;
                } else if (aVar.b("value") != 3 || this.d) {
                    a("Unknown thermal status: " + aVar.b("value"), 0);
                    return;
                } else {
                    a("Camera cold", 0);
                    this.d = true;
                    o.a((Context) this.b, true, true, this.b.getResources().getString(R.string.camera_low_temp), this.b.getResources().getString(R.string.camera_low_temp_content), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                    return;
                }
            case 18176:
                a.a.a.d dVar = (a.a.a.d) aVar.f("LiveViewFrame").a();
                if (this.b.d.h.I.l) {
                    dVar.f169a.d();
                } else {
                    this.b.d.h.R += dVar.f169a.f68a;
                    if (dVar.b.equals("V")) {
                        if (this.b.k()) {
                            if (this.b.E() || this.b.D()) {
                                this.b.a(8746, 100L);
                            }
                            this.b.a(dVar);
                            if (!(!this.b.M() || this.b.c.c.h.d == 305 || this.b.c.c.h.d == 304)) {
                                this.b.a(8737, 300L);
                            }
                            if (this.b.c.a() == 304) {
                                if (this.b.d.h.am == null) {
                                    return;
                                }
                                if (this.b.d.h.az != 0 || e.b(dVar.f169a.a())) {
                                    if (this.b.d.h.az == 0) {
                                        this.b.d.h.ay = dVar.f169a.b - 166;
                                        byte[] bArr = new byte[dVar.f169a.f68a];
                                        System.arraycopy(dVar.f169a.a(), 0, bArr, 0, dVar.f169a.f68a);
                                        this.b.a(bArr);
                                        if (GeneralFunction.m.a.a() == 1) {
                                            this.b.d.h.aE = (double) d.a(this.b.d.k.b.ab, 0);
                                        } else {
                                            this.b.d.h.aE = (double) d.a(c.f1314a, 0);
                                        }
                                        this.b.m(true);
                                    }
                                    this.b.b(dVar);
                                    this.b.d.h.az++;
                                } else {
                                    return;
                                }
                            }
                        } else {
                            dVar.f169a.d();
                        }
                        this.b.C().a(dVar);
                    } else if (!dVar.b.equals("A")) {
                        a("unknow liveview frame type:" + dVar.b, 0);
                    }
                }
                if (!this.b.c.c.j.c) {
                    this.b.c.c.j.c = true;
                    return;
                }
                return;
            case 18179:
                a("MSG_REMOTE_EVENT_DISCONNECTION", 4);
                this.b.a(8840, 0L);
                return;
            case 18185:
                a("Add CroppedArea tag result: " + new p().a(this.b.d.k.b.f1320a.h(this.b.d.k.e.c(), 0)), 1);
                this.b.b(this.b.d.k.b.f1320a.h(this.b.d.k.e.c(), 0));
                this.b.ah();
                return;
            case 18432:
                if (this.b.c.c.h.ar <= 10) {
                    this.b.a(8726, 0L);
                    return;
                } else if (!this.b.c.d.b()) {
                    this.b.b(true, true);
                    this.b.a(true, this.b.getResources().getString(R.string.no_camera));
                    this.b.b(false, "");
                    this.b.L();
                    this.b.W();
                    return;
                } else {
                    this.b.af();
                    this.b.b(true, this.b.getResources().getString(R.string.connecting));
                    this.b.c.c(new a(18484));
                    a aVar5 = new a(18480);
                    aVar5.a("app_version", this.b.getResources().getString(R.string.internal_app_version));
                    this.b.c.c(aVar5);
                    return;
                }
            case 18435:
                a("MSG_REMOTE_USB_CORE_DETACH", 3);
                this.b.b(false, "");
                this.b.c(false);
                this.b.R();
                if (this.b.c.c.h.ar <= 10) {
                    this.b.a(true, this.b.getResources().getString(R.string.low_battery_warning_title));
                    this.b.b(true, false);
                } else {
                    this.b.a(true, this.b.getResources().getString(R.string.no_camera));
                    this.b.b(true, true);
                }
                this.b.W();
                this.b.c.c.h.at = false;
                this.b.c.c.h.y = true;
                this.c = false;
                this.d = false;
                this.e = false;
                this.b.c.c.h.av = null;
                if (this.b.c.f() != 0) {
                    if (this.b.c.e.a() != null) {
                        a("Delete un-complete file: " + this.b.c.e.a(), 1);
                        new File(this.b.c.e.a()).delete();
                    }
                    this.b.c.e();
                }
                this.b.f.a();
                if (this.b.c.c.h.ak != 0) {
                    o.a((Context) this.b, true, true, this.b.getResources().getString(R.string.camera_update_for_update_failed), this.b.getResources().getString(R.string.camera_update_fail), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                    this.b.c.c.h.ak = 0;
                }
                if (this.b.c.c.h.Z) {
                    this.b.c.c.h.aa = true;
                    this.b.c.c.h.ab.cancel();
                }
                if (this.b.c.a() == 288) {
                    this.b.c.a(272, this.b);
                    this.b.a(0, false);
                    this.b.a(8716, 0L);
                    this.b.J();
                } else if (this.b.c.a() == 304) {
                    a("[USB_DETACH] ui_ModeDef.UI_MODE_VIEW_RECORDING", 3);
                    this.b.c.c.h.S = 4;
                    this.b.a(8791);
                } else if (this.b.c.a() == 305) {
                    a("[USB_DETACH] ui_ModeDef.UI_MODE_VIEW_RECORDING_STOP", 3);
                    if (this.b.c.c.h.x) {
                        this.b.ac();
                    }
                } else if (this.b.c.a() == 400) {
                    b();
                    this.b.a(8790);
                } else if (this.b.c.a() == 336 || this.b.c.a() == 352 || this.b.c.a() == 416 || this.b.c.a() == 432) {
                    this.b.a(8793);
                } else {
                    b();
                    this.b.ae();
                }
                if (this.b.l()) {
                    this.b.a(false);
                }
                if (o.a(this.b) && this.b.d.h.ar > 10) {
                    o.b(this.b);
                    return;
                }
                return;
            case 18436:
                a("MSG_REMOTE_USB_CORE_ATTACH", 2);
                this.b.c.c.e = false;
                this.b.c.c.h.at = false;
                this.b.b(true, this.b.getResources().getString(R.string.connecting));
                this.b.c.c.d = true;
                if (this.b.c.p()) {
                    this.b.c.i();
                }
                this.b.T();
                return;
            case 18437:
                if (aVar.a("usb_permission")) {
                    a("has permission, need init usb", 0);
                    this.b.c.c.d = false;
                    a();
                    return;
                }
                this.b.b(false, "");
                this.b.L();
                return;
            case 18464:
            case 18465:
                if (aVar.b("thermal_status") != 0) {
                    a aVar6 = new a(17938);
                    aVar6.a("value", aVar.b("thermal_status"));
                    this.b.a(aVar6);
                    return;
                }
                return;
            case 18480:
                if (aVar.b("result") != 0) {
                    a("Get camera FW version fail", 0);
                    if (!this.b.c.d.b()) {
                        a("USB disconnected, get fw version fail", 0);
                        this.b.a(true, this.b.getResources().getString(R.string.no_camera));
                        this.b.b(true, true);
                        return;
                    }
                    return;
                } else if (!this.b.g()) {
                    a("APP onPaused, do not continue, send close communication instead", 0);
                    this.b.c.c(new a(18443), 0);
                    return;
                } else {
                    String d2 = aVar.d("fw_version");
                    this.b.d.g.c = d2;
                    a("Camera FW version:" + d2 + ", Target FW version" + "v1.3B00" + ", bIsForceFWUpgrade: " + this.b.c.g, 2);
                    if (d2.compareTo("v1.3B00") > 0) {
                        this.b.c.c.l.r = false;
                        if (d2.compareTo("v1.3C00") < 0) {
                            a("Camera is test FW", 0);
                        } else {
                            a("Need to update App (" + d2 + ") to latest version: " + "v1.3B00", 0);
                            if (this.b.c.h) {
                                o.a((Context) this.b, true, true, this.b.getResources().getString(R.string.application_update), this.b.getResources().getString(R.string.application_update_content), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                                this.b.b(true, false);
                                this.b.b(false, "");
                                this.b.a(true, this.b.getResources().getString(R.string.application_update_warning_title));
                                return;
                            }
                        }
                    } else if (d2.compareTo("v1.3B00") < 0) {
                        a("Need to update camera (" + d2 + ") to latest version: " + "v1.3B00", 0);
                        if (this.b.c.g) {
                            if (d2.compareTo("v0.2000") < 0) {
                                this.b.b(false, "");
                                this.b.c.c(new a(18455));
                                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_update_for_newer_program), this.b.getResources().getString(R.string.camera_update_test_app_cv60_0426), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                                return;
                            } else if (d2.compareTo("v0.2300") < 0) {
                                this.b.b(false, "");
                                this.b.c.c(new a(18455));
                                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_update_for_newer_program), this.b.getResources().getString(R.string.camera_update_test_app_2900_2901_fw23), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                                return;
                            } else if (d2.compareTo("v0.2B00") < 0) {
                                this.b.b(false, "");
                                this.b.c.c(new a(18455));
                                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_update_for_newer_program), this.b.getResources().getString(R.string.camera_update_test_app_3400_3401_fw2b), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                                return;
                            } else if (d2.compareTo("v0.2D00") < 0) {
                                this.b.b(false, "");
                                this.b.c.c(new a(18455));
                                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_update_for_newer_program), this.b.getResources().getString(R.string.camera_update_test_app_3700_fw2d), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                                return;
                            } else if (d2.compareTo("v0.3700") < 0 && !"v1.3B00".equals("v0.3700")) {
                                this.b.b(false, "");
                                this.b.c.c(new a(18455));
                                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_update_for_newer_program), this.b.getResources().getString(R.string.camera_update_test_app_4900_fw37), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                                return;
                            } else if (d2.compareTo("v0.4300") < 0 && !"v1.3B00".equals("v0.4300")) {
                                this.b.b(false, "");
                                this.b.c.c(new a(18455));
                                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_update_for_newer_program), this.b.getResources().getString(R.string.camera_update_test_app_6300_fw43), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                                return;
                            } else if (d2.compareTo("v1.0900") < 0 && !"v1.3B00".equals("v1.0900")) {
                                this.b.c.c.l.r = true;
                                this.b.b(false, "");
                                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_update_for_newer_camera_program), this.b.getResources().getString(R.string.camera_update_content), this.b.getResources().getString(R.string.dialog_option_ok), 9255);
                                return;
                            } else if (d2.compareTo("v1.3A00") == 0) {
                                this.b.c.c.l.r = true;
                                this.b.b(false, "");
                                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_update_for_newer_camera_program), this.b.getResources().getString(R.string.camera_update_content), this.b.getResources().getString(R.string.dialog_option_ok), 9255);
                                return;
                            } else if (d2.compareTo("v1.2900") == 0) {
                                a("Domestic last version checked: camera (" + d2 + ")", 0);
                                this.b.c.c.l.r = false;
                                this.b.a(8739);
                                return;
                            } else {
                                this.b.a(false, "");
                                this.b.c.c.l.r = true;
                                this.b.a(new a(8847));
                                if ((d2.compareTo("v1.0A00") <= 0 && !this.b.c.c.e) || !GeneralFunction.n.a.a(this.b.c).getBoolean("fwUpdateLater", false)) {
                                    this.b.b(false, "");
                                    o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.camera_update_for_newer_camera_program), this.b.getResources().getString(R.string.camera_update_content), new String[]{this.b.getResources().getString(R.string.dialog_option_later), this.b.getResources().getString(R.string.dialog_option_update)}, new int[]{9256, 9255});
                                    return;
                                }
                            }
                        }
                    } else {
                        this.b.c.c.l.r = false;
                    }
                    this.b.a(8739);
                    return;
                }
            case 18519:
                if (aVar.b("result") == 0) {
                    this.b.c.c.h.v = 0;
                    a aVar7 = new a(18449);
                    if (this.b.c.a() == 336 || this.b.c.a() == 416) {
                        aVar7.a("resolution", 10);
                    } else {
                        aVar7.a("resolution", a.c.b.g(GeneralFunction.m.a.d()));
                    }
                    this.b.c.c(aVar7);
                    return;
                }
                return;
            case 18545:
                if (aVar.b("result") == 0) {
                    this.b.c.c(new a(18464));
                    this.b.L();
                    this.b.a(false, "");
                    this.b.c.c.h.v = 0;
                    if (GeneralFunction.m.a.d() != 0) {
                        GeneralFunction.n.a.b(this.b.c).putBoolean("showLiveViewModeToast", true).apply();
                        return;
                    } else if (GeneralFunction.n.a.a(this.b.c).getBoolean("showLiveViewModeToast", true)) {
                        this.b.a(this.b.getResources().getString(R.string.live_mode_toast_msg), true);
                        GeneralFunction.n.a.b(this.b.c).putBoolean("showLiveViewModeToast", false).apply();
                        return;
                    } else {
                        return;
                    }
                } else if (aVar.b("result") == 1) {
                    if (this.b.c.c.h.v > 1000) {
                        a("Over 1000 times check start live view status get busy. Connection error.", 0);
                        this.b.c.a(8733, 0);
                        this.b.c.c.h.v = 0;
                        return;
                    }
                    this.b.c.c(new a(18545), 5);
                    this.b.c.c.h.v++;
                    return;
                } else if (aVar.b("result") == 2) {
                    a("[UsbRemote] preparing live view", 0);
                    return;
                } else if (aVar.b("result") == 5) {
                    a("[UsbRemote] skip start live view checking", 0);
                    return;
                } else {
                    this.b.b(false, "");
                    a("[UsbRemote] check start live view status: fail", 0);
                    this.b.c.a(8733, 0);
                    this.b.c.c.h.v = 0;
                    return;
                }
            case 18546:
                if (aVar.b("result") == 0) {
                    a("MSG_REMOTE_USB_CMD_CHECK_STOP_LIVEVIEW_STATUS success", 0);
                    this.b.c.a(new a(8732));
                    return;
                } else if (aVar.b("result") == 1) {
                    this.b.c.c(new a(18546), 5);
                    return;
                } else if (aVar.b("result") == 5) {
                    a("[UsbRemote] skip stop live view checking", 0);
                    return;
                } else {
                    a("[UsbRemote] check stop live view status: fail", 0);
                    this.b.c.a(8733, 0);
                    return;
                }
            case 61442:
                a("*****Capture or recording event time out*****", 0);
                g.a(2);
                this.b.a(8840, 0L);
                return;
            case 61443:
                this.b.U();
                return;
            default:
                switch (this.b.c.a()) {
                    case 272:
                        b(message);
                        return;
                    case 288:
                        c(message);
                        return;
                    case HttpStatusCodes.STATUS_CODE_NOT_MODIFIED:
                        d(message);
                        return;
                    case 305:
                        e(message);
                        return;
                    case 320:
                        f(message);
                        return;
                    case 336:
                        this.b.C().a(message);
                        return;
                    case 352:
                        this.b.C().b(message);
                        return;
                    case 368:
                        g(message);
                        return;
                    case 384:
                        h(message);
                        return;
                    case 400:
                        i(message);
                        return;
                    case 416:
                        this.b.C().c(message);
                        return;
                    case 432:
                        this.b.C().d(message);
                        return;
                    default:
                        return;
                }
        }
    }

    @SuppressLint({"Wakelock"})
    public void b(Message message) {
        a aVar = new a(message);
        switch (message.what) {
            case 0:
                o.b(this.b);
                this.b.c.c.g.f1324a = 0;
                this.b.c.c(268435455);
                return;
            case 8480:
                int b2 = aVar.b("CameraStatus");
                a("ulDscStatus: " + b2, 3);
                if (b2 == 5 || b2 == 11) {
                    this.b.c.b(268435455L);
                    this.b.a(8774, 0L);
                    return;
                } else if (b2 == 7 || b2 == 10) {
                    this.b.c.b(268435455L);
                    this.b.a(8782, 0L);
                    return;
                } else if (b2 == 9) {
                    this.b.c.a(HttpStatusCodes.STATUS_CODE_NOT_MODIFIED, this.b);
                    this.b.c.c(new a(16996));
                    return;
                } else if (b2 == 8) {
                    this.b.c.a(320, this.b);
                    return;
                } else {
                    this.b.a(8754, 0L);
                    return;
                }
            case 8709:
                this.b.c.c(new a(16946));
                return;
            case 8717:
                this.b.e();
                for (int size = this.b.d.h.F.size() - 1; size >= 0; size--) {
                }
                this.b.d.h.F.clear();
                this.b.f();
                return;
            case 8726:
                if (this.b.c.d.b()) {
                    this.b.c.d.a(new a(18455));
                }
                this.b.b(false, "");
                this.b.b(true, false);
                this.b.J();
                this.b.a(8846);
                return;
            case 8742:
                if (aVar.a("muxerInitResult")) {
                    this.b.i(1);
                    this.b.Y();
                    this.b.k(false);
                    this.b.I();
                    this.b.i(true);
                    this.b.aa();
                    this.b.c.c(new a(18451));
                    return;
                }
                this.b.ak();
                this.b.k(false);
                this.b.d.h.c = 288;
                return;
            case 8745:
                a("MSG_UI_VIEW_CHECK_AVG_PING_TIME_DONE, ping result: " + aVar.b("avgPingTime"), 1);
                if (!this.b.c.d.b()) {
                    this.b.b(false, "");
                    return;
                }
                if (!a.b.b.a.a(this.b)) {
                    c();
                } else if (a.b.b.a.b(this.b) == 2) {
                    o.a((Context) this.b, true, true, this.b.getResources().getString(R.string.broadcast_mobile_network_title), this.b.getResources().getString(R.string.broadcast_mobile_network_description), new String[]{this.b.getResources().getString(R.string.dialog_option_no), this.b.getResources().getString(R.string.dialog_option_yes)}, new int[]{12039, 10777});
                } else {
                    this.b.a(10777);
                }
                this.b.b(false, "");
                return;
            case 8754:
                this.b.c.c(new a(17153));
                return;
            case 8755:
                this.b.c.c(new a(17154));
                return;
            case 8756:
                a("MSG_UI_VIEW_GET_THUMBNAIL_IMAGE", 4);
                this.b.c.c(new a(18466), 0);
                return;
            case 8772:
                this.b.d.h.c = HttpStatusCodes.STATUS_CODE_SEE_OTHER;
                this.b.a(8458, 0L);
                return;
            case 8773:
                this.b.d.h.c = HttpStatusCodes.STATUS_CODE_NOT_MODIFIED;
                this.b.a(8454, 0L);
                return;
            case 8774:
                if (!this.b.c.d.b()) {
                    a("USB detached, ignore record msg", 0);
                    return;
                } else if (this.b.M()) {
                    a("Live view not ready, ignore record msg", 0);
                    return;
                } else {
                    d.a((Context) this.b, true);
                    this.b.al();
                    this.b.d.h.c = 294;
                    this.b.ad();
                    return;
                }
            case 8775:
                if (!this.b.c.d.b()) {
                    a("USB detached, ignore capture msg", 0);
                    return;
                } else if (this.b.M()) {
                    a("Live view not ready, ignore capture msg", 0);
                    return;
                } else if (!this.b.c.a(268435455L)) {
                    a("All key disabled, ignore capture cmd", 0);
                    return;
                } else {
                    a("[CaptureDBG] Capture!", 1);
                    this.b.I();
                    this.b.c.a(288, this.b);
                    this.b.a(0, true);
                    this.b.k(false);
                    this.b.d.h.c = 289;
                    a aVar2 = new a(18453);
                    aVar2.a("orientation", this.b.d.h.u);
                    this.b.c.c(aVar2);
                    return;
                }
            case 8776:
                this.b.d.h.c = HttpStatusCodes.STATUS_CODE_TEMPORARY_REDIRECT;
                this.b.a(8459, 0L);
                return;
            case 8779:
                this.b.d.h.c = 305;
                this.b.a(8449, 0L);
                return;
            case 8780:
                this.b.d.h.c = 306;
                this.b.a(8460, 0L);
                return;
            case 8782:
                this.b.c.a(320, this.b);
                this.b.d.h.c = 291;
                this.b.c.c(new a(16941));
                this.b.a(8715, 0L);
                return;
            case 8784:
                this.b.b(true, this.b.getResources().getString(R.string.broadcast_initialize_busy));
                this.b.b(20510);
                return;
            case 8786:
                if (this.b.d.h.q == 0) {
                    this.b.d.h.q = 1;
                    this.b.f.setViewType(1);
                    this.b.f.setInteractiveMode(3);
                } else if (this.b.d.h.q == 1) {
                    this.b.d.h.q = 2;
                    this.b.f.setViewType(1);
                    this.b.f.setInteractiveMode(1);
                } else if (this.b.d.h.q == 2) {
                    this.b.d.h.q = 0;
                    this.b.f.setViewType(4);
                    this.b.f.setInteractiveMode(0);
                }
                this.b.c.c(268435455);
                return;
            case 8787:
                if (GeneralFunction.m.a.s()) {
                    GeneralFunction.m.a.d(false);
                    GeneralFunction.m.a.c(a.c.b.a(false));
                } else {
                    GeneralFunction.m.a.d(true);
                    GeneralFunction.m.a.c(a.c.b.a(true));
                }
                this.b.c.c(268435455);
                return;
            case 8788:
                this.b.c.a(336, this.b);
                this.b.a(8788);
                return;
            case 8789:
                if (this.b.d.h.t == 0) {
                    this.b.d.h.t = 1;
                } else {
                    this.b.d.h.t = 0;
                }
                this.b.c.c(268435455);
                return;
            case 8790:
                if (this.b.c.c.h.V == 0) {
                    this.b.c.a(400, this.b);
                    this.b.S();
                    this.b.h(true);
                    this.b.c.c.h.V = 1;
                    return;
                }
                this.b.h(false);
                this.b.c.c.h.V = 0;
                return;
            case 8794:
                this.b.c.a(416, this.b);
                this.b.a(8794);
                return;
            case 8795:
                this.b.b(true, this.b.getResources().getString(R.string.broadcast_initialize_busy));
                this.b.b(20510);
                return;
            case 8798:
                this.b.N();
                return;
            case 8799:
                this.b.Q();
                return;
            case 8800:
                this.b.O();
                return;
            case 8845:
                o.b(this.b);
                this.b.c.c.g.f1324a = 337;
                return;
            case 8846:
                o.b(this.b);
                o.a((Context) this.b, true, true, this.b.getResources().getString(R.string.low_battery), this.b.getResources().getString(R.string.low_battery_content), this.b.getResources().getString(R.string.dialog_option_ok), 12039);
                this.b.a(true, this.b.getResources().getString(R.string.low_battery_warning_title));
                this.b.c.c.g.f1324a = 1036;
                return;
            case 8847:
                if (this.b.c.c.l.n || this.b.c.c.l.r) {
                    this.b.o(true);
                    return;
                }
                return;
            case 9248:
                this.b.c.c.l.o = false;
                if (!(GeneralFunction.n.a.a(this.b.c).getBoolean("apkUpdateLater", false) || this.b.c.c.l.ag)) {
                    this.b.c.c.l.ag = true;
                    o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.application_update_opimization_title), this.b.getResources().getString(R.string.application_update_opimization) + "\n" + GeneralFunction.n.a.a(this.b.c).getString("hotaOptimization", "") + "\n", new String[]{this.b.getResources().getString(R.string.dialog_option_later), this.b.getResources().getString(R.string.dialog_option_update)}, new int[]{9249, 9250});
                    return;
                }
                return;
            case 9249:
                GeneralFunction.n.a.b(this.b.c).putBoolean("apkUpdateLater", true).apply();
                this.b.c.c.l.ag = false;
                return;
            case 9250:
                this.b.c.c.l.o = true;
                if (!a.b.b.a.a(this.b) || a.b.b.a.b(this.b) != 2) {
                    this.b.a(8454);
                } else {
                    o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.version_update_title), this.b.getResources().getString(R.string.version_update_update_not_wifi), new String[]{this.b.getResources().getString(R.string.dialog_option_cancel), this.b.getResources().getString(R.string.dialog_option_continue)}, new int[]{9254, 8454});
                }
                this.b.c.c.l.ag = false;
                return;
            case 9254:
                this.b.c.c.l.o = false;
                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.application_update_opimization_title), this.b.getResources().getString(R.string.application_update_opimization) + "\n" + GeneralFunction.n.a.a(this.b.c).getString("hotaOptimization", "") + "\n", new String[]{this.b.getResources().getString(R.string.dialog_option_later), this.b.getResources().getString(R.string.dialog_option_update)}, new int[]{9249, 9250});
                return;
            case 9255:
                this.b.c.c.l.s = true;
                this.b.a(8454);
                return;
            case 9256:
                this.b.b(false, "");
                GeneralFunction.n.a.b(this.b.c).putBoolean("fwUpdateLater", true).apply();
                this.b.a(8739);
                return;
            case 10774:
                if (!this.b.c.d.b()) {
                    a("USB detached, ignore start facebook", 0);
                    return;
                }
                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.broadcast_check_start_title), this.b.getResources().getString(R.string.broadcast_check_start_description), new String[]{this.b.getResources().getString(R.string.dialog_option_no), this.b.getResources().getString(R.string.dialog_option_yes)}, new int[]{10776, 8794}, true);
                return;
            case 10775:
                if (!this.b.c.d.b()) {
                    a("USB detached, ignore start youtube", 0);
                    return;
                }
                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.broadcast_check_start_title), this.b.getResources().getString(R.string.broadcast_check_start_description), new String[]{this.b.getResources().getString(R.string.dialog_option_no), this.b.getResources().getString(R.string.dialog_option_yes)}, new int[]{10776, 8788}, true);
                return;
            case 10777:
                d();
                return;
            case 12032:
                Process.killProcess(Process.myPid());
                this.b.finish();
                return;
            case 12041:
                a("MSG_UI_SYSTEM_ACTIVITY_NEW_INTENT", 3);
                if (this.b.c.c.k.b.f1320a.g(this.b.d.k.e.c(), 0) == null) {
                    this.b.q();
                    this.b.b(20508);
                }
                this.b.b(true, false);
                if (this.b.f != null) {
                    a("[onNewIntent] liveviewSphericalVideoPlayer != null " + this.b.f.d(), 0);
                    if (this.b.f.d() == 1) {
                        if (GeneralFunction.m.a.g() == 0) {
                            if (this.b.c.c.h.U == 0) {
                                this.b.f.setIsShowThumbNail(false);
                            } else {
                                this.b.f.setIsShowThumbNail(true);
                            }
                        }
                        this.b.f.setPlayerMode(2);
                        this.b.f.setViewType(3);
                        this.b.f.setInteractiveMode(1);
                        if (GeneralFunction.m.a.d() == 1) {
                            this.b.f.b(1920, 960);
                        } else {
                            this.b.f.b(1280, 640);
                        }
                        this.b.f.g();
                        return;
                    } else if (this.b.f.d() == 0) {
                        this.b.p();
                        return;
                    } else {
                        return;
                    }
                } else {
                    a("[onNewIntent] liveviewSphericalVideoPlayer == null", 0);
                    this.b.p();
                    return;
                }
            case 12043:
            default:
                return;
            case 17153:
                a("objMsgEx.getIntData(value1): " + aVar.b("value1"), 3);
                GeneralFunction.m.a.D(aVar.b("value1"));
                this.b.d.h.e = aVar.b("value2");
                this.b.a(8755, 0L);
                return;
            case 17154:
                a("objMsgEx.getIntData(value1): " + aVar.b("value1"), 3);
                GeneralFunction.m.a.E(aVar.b("value1"));
                this.b.d.h.f = aVar.b("value2");
                if (this.b.d.h.d == 305) {
                    this.b.a(8449, 0L);
                    return;
                } else if (this.b.d.h.d == 306) {
                    this.b.a(8460, 0L);
                    return;
                } else {
                    this.b.g(65);
                    if (this.b.d.h.o) {
                        this.b.d.h.o = false;
                        this.b.a(8845, 0L);
                        return;
                    }
                    return;
                }
            case 17924:
                a("MSG_REMOTE_EVENT_CAPTURE_DONE2", 4);
                return;
            case 17934:
                a("MSG_REMOTE_EVENT_RECORDING_START_OK", 3);
                this.b.c.a(HttpStatusCodes.STATUS_CODE_NOT_MODIFIED, this.b);
                this.b.d.h.c = 295;
                this.b.c(15);
                this.b.d.h.k = true;
                this.b.a(8716, 0L);
                return;
            case 17935:
                a("MSG_REMOTE_EVENT_RECORDING_START_FAIL", 4);
                this.b.g(64);
                this.b.a(8714, 0L);
                this.b.a(8716, 0L);
                return;
            case 18501:
                a.a.a.b bVar = (a.a.a.b) aVar.f("DateTime").a();
                a("[CameraDateTime] " + bVar.f167a + "/" + ((int) bVar.b) + "/" + ((int) bVar.c) + " " + ((int) bVar.d) + ":" + ((int) bVar.e) + ":" + ((int) bVar.f), 2);
                return;
            case 18503:
                if (aVar.b("result") != 0) {
                    a("Get camera all setting fail", 0);
                    return;
                }
                return;
            case 18517:
                if (aVar.b("result") != 0) {
                    a("Set camera date time fail", 0);
                    return;
                } else {
                    this.b.c.c(new a(18503));
                    return;
                }
            case 32768:
                if (this.b.c.c.h.Z) {
                    this.b.c.c.h.aa = true;
                    this.b.c.c.h.ab.cancel();
                    this.b.c.c(268435455);
                    return;
                } else if (this.b.Z() || this.b.F()) {
                    this.b.c.c(268435455);
                    return;
                } else if (this.b.u()) {
                    this.b.c(false);
                    this.b.c.c(268435455);
                    return;
                } else {
                    this.b.a(8452);
                    return;
                }
        }
    }

    public void c(Message message) {
        switch (message.what) {
            case 8710:
                a("MSG_UI_VIEW_CAPTURE_DONE", 3);
                if (this.b.c.f() == 0) {
                    this.b.J();
                    this.b.b(this.b.d.k.b.f1320a.h(this.b.d.k.e.c(), 0));
                }
                this.b.d.h.c = 288;
                this.b.c.a(272, this.b);
                this.b.a(0, false);
                this.b.a(8716, 0L);
                if (this.b.g()) {
                    a("[CaptureDBG] Keep get frame", 1);
                    this.b.c.c(new a(18464), 0);
                } else {
                    a("[CaptureDBG] Capture back with activity pause. UI_CheckNeedDownloadPicNum = " + this.b.c.f(), 1);
                    if (this.b.c.f() == 0) {
                        a("[CaptureDBG] Capture and download complete, close communication", 1);
                        this.b.c.c(new a(18443), 0);
                    }
                }
                if (this.b.d.h.aK) {
                    this.b.a(8726);
                    this.b.d.h.aK = false;
                    return;
                }
                return;
            case 8726:
                this.b.d.h.aK = true;
                return;
            case 8729:
                this.b.i(0);
                return;
            case 18466:
                this.b.H();
                return;
            case 32768:
                a("UI_LiveviewCapturing MSG_KEY_BACK", 3);
                return;
            default:
                return;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void d(Message message) {
        a aVar = new a(message);
        switch (message.what) {
            case 8711:
                this.b.d.h.c = 295;
                this.b.a(8756, 0L);
                this.b.a(8716, 0L);
                return;
            case 8712:
                this.b.d.h.c = 295;
                this.b.a(8716, 0L);
                this.b.g(65);
                this.b.c.c(268435455);
                return;
            case 8714:
                this.b.d.h.c = 288;
                this.b.c.a(272, this.b);
                if (GeneralFunction.m.a.r()) {
                    this.b.d.h.o = true;
                }
                this.b.a(8754, 0L);
                this.b.a(8716, 0L);
                return;
            case 8725:
                this.b.a(false, this.b.d.h.h);
                this.b.a(8778);
                return;
            case 8726:
            case 32768:
                a("MSG_KEY_BACK stop recording", 4);
                if (message.what != 32768) {
                    if (message.what == 8726) {
                        this.b.c.c.h.S = 3;
                        break;
                    }
                } else {
                    this.b.c.c.h.S = 1;
                    break;
                }
                break;
            case 8736:
                this.b.a(true, true);
                this.b.J();
                this.b.i(false);
                UI_LiveViewController uI_LiveViewController = this.b;
                return;
            case 8741:
                this.b.c.c.h.S = 6;
                this.b.a(8778, 0L);
                return;
            case 8756:
                a("MSG_UI_VIEW_GET_PIV_THUMBNAIL_IMAGE", 4);
                this.b.c.c(new a(18466), 0);
                return;
            case 8760:
                a("MSG_UI_VIEW_GET_PICTURE_IMAGE", 4);
                a aVar2 = new a(18465);
                aVar2.a("IsMiddleData", 0);
                this.b.c.c(aVar2, 0);
                return;
            case 8777:
                this.b.c.b(268435455L);
                this.b.g(64);
                this.b.d.h.c = 296;
                if (this.b.d.h.h > 0) {
                    this.b.c.c(new a(16938));
                    this.b.a(8715, 0L);
                    return;
                }
                this.b.a(8777, 200L);
                return;
            case 8778:
                break;
            case 8779:
                this.b.d.h.d = 305;
                this.b.a(8778, 0L);
                return;
            case 8780:
                this.b.d.h.d = 306;
                this.b.a(8778, 0L);
                return;
            case 8791:
                a("*****MSG_UI_VIEW_KEY_RECORD_END_BY_USB_DETACH", 0);
                this.b.ak();
                this.b.R();
                this.b.a(1, true);
                this.b.j(true);
                this.b.c.a(305, this.b);
                this.b.a(false, true);
                this.b.ae();
                this.b.i(1);
                return;
            case 8825:
                this.b.a(true, this.b.d.h.h);
                return;
            case 8843:
                this.b.f(52);
                return;
            case 12326:
                a("MSG_FILE_DOWNLOAD_PIV_QUICKVIE_DONE", 4);
                a("update thumbnail and continue recording", 3);
                this.b.g(65);
                this.b.c.c(268435455);
                return;
            case 16996:
                a("MSG_REMOTE_GET_CURRENT_RECORDING_TIME value: " + aVar.b("value"), 4);
                this.b.d.h.h = aVar.b("value");
                if (!this.b.d.h.k) {
                    this.b.a(8843, 0L);
                    return;
                } else if (this.b.d.h.h <= 0) {
                    this.b.c.c(new a(16996), 200);
                    return;
                } else {
                    this.b.f(49);
                    return;
                }
            case 17937:
                a("MSG_REMOTE_EVENT_STOP_RECORDING", 4);
                this.b.c(30);
                this.b.f(53);
                return;
            case 17966:
                a("MSG_REMOTE_EVENT_PIV_DONE2 result: " + aVar.b("result"), 4);
                if (aVar.b("result") == -17) {
                    this.b.a(8712, 0L);
                    return;
                } else {
                    this.b.a(8711, 0L);
                    return;
                }
            default:
                return;
        }
        switch (this.b.c.c.h.S) {
            case 2:
                this.b.c.c(new a(18443));
                this.b.ak();
                this.b.R();
                this.b.a(1, true);
                this.b.j(true);
                this.b.a(false, 0);
                this.b.r();
                this.b.c.a(305, this.b);
                this.b.a(false, true);
                this.b.ae();
                this.b.k(false);
                this.b.i(1);
                return;
            default:
                this.b.ak();
                this.b.c.c(new a(18452));
                this.b.R();
                this.b.a(1, true);
                this.b.j(true);
                this.b.a(false, 0);
                this.b.r();
                this.b.c.a(305, this.b);
                return;
        }
    }

    public void e(Message message) {
        new a(message);
        switch (message.what) {
            case 8726:
                if (this.b.d.h.S != 1 && this.b.d.h.S != 4) {
                    this.b.d.h.S = 3;
                    return;
                }
                return;
            case 8727:
                this.b.c.c.h.x = true;
                this.b.a(1, false);
                this.b.ah();
                if (this.b.d.k.b.f1320a.h(this.b.d.k.e.c(), 0) != null) {
                    this.b.b(this.b.d.k.b.f1320a.h(this.b.d.k.e.c(), 0));
                }
                this.b.b(20508);
                switch (this.b.c.c.h.S) {
                    case 0:
                        a("[lRecordStopCase] RECORD_STOP_NORMAL", 3);
                        break;
                    case 1:
                        a("[lRecordStopCase] RECORD_STOP_BACK_KEY", 3);
                        this.b.a(8452);
                        break;
                    case 2:
                        a("[lRecordStopCase] RECORD_STOP_HOME_KEY", 3);
                        break;
                    case 3:
                        a("[lRecordStopCase] RECORD_STOP_PHONE_LOW_BATTERY", 3);
                        this.b.a(8726);
                        break;
                    case 4:
                        a("[lRecordStopCase] RECORD_STOP_DISCONNECT_CAMERA", 3);
                        this.b.b(false, "");
                        this.b.a(true, this.b.getResources().getString(R.string.no_camera));
                        this.b.b(true, true);
                        break;
                    case 5:
                        a("[lRecordStopCase] RECORD_STOP_REMOVE_SD_CARD", 3);
                        break;
                }
                this.b.c.c.h.S = 0;
                if (this.b.ab()) {
                    this.b.ac();
                }
                d.a((Context) this.b, false);
                return;
            case 8778:
                a("Receive MSG_UI_VIEW_KEY_RECORD_END in UI_LiveviewStopRecording!", 1);
                return;
            case 17967:
                a("MSG_REMOTE_EVENT_RECORDING_DONE_2", 2);
                this.b.c.c.h.y = true;
                if (this.b.ab()) {
                    this.b.ac();
                    return;
                }
                return;
            case 18452:
                a("*****lVideoTime: " + this.b.c.c.h.aB, 0);
                this.b.a(false, true);
                this.b.ae();
                this.b.i(1);
                return;
            case 32768:
                a("Receive MSG_KEY_BACK in UI_LiveviewStopRecording!", 1);
                return;
            default:
                return;
        }
    }

    public void f(Message message) {
        switch (message.what) {
            case 8779:
                this.b.d.h.d = 305;
                this.b.a(8783, 0L);
                return;
            case 8780:
                this.b.d.h.d = 306;
                this.b.a(8783, 0L);
                return;
            case 8783:
            case 32768:
                this.b.g(64);
                this.b.c.c(new a(16942));
                this.b.a(8715, 0L);
                return;
            case 8844:
            case 17963:
                this.b.c.c(268435455);
                this.b.d.h.c = 292;
                this.b.a(8716, 0L);
                return;
            case 17924:
                a("MSG_REMOTE_EVENT_CAPTURE_DONE2", 4);
                this.b.a(8716, 0L);
                this.b.g(65);
                this.b.c.c(268435455);
                return;
            case 17964:
                a("MSG_REMOTE_EVENT_CAPTURE_START", 4);
                this.b.c.b(268435455L);
                this.b.g(64);
                return;
            case 17965:
                this.b.c.b(268435455L);
                this.b.d.h.c = 288;
                this.b.c.a(272, this.b);
                this.b.a(8756, 0L);
                this.b.a(8716, 0L);
                return;
            default:
                return;
        }
    }

    public void g(Message message) {
        switch (message.what) {
            case 8752:
                this.b.d.h.c = HttpStatusCodes.STATUS_CODE_FOUND;
                a("UI_SettingControl.GetCaptureType(): " + GeneralFunction.m.a.q(), 3);
                this.b.c.a(256, this.b, (Intent) null);
                this.b.a(8715, 0L);
                return;
            case 8779:
                this.b.d.h.b = 256;
                this.b.c.a(272, this.b);
                this.b.c.b(268435455L);
                this.b.a(8779, 0L);
                return;
            case 8780:
                this.b.d.h.b = 256;
                this.b.c.a(272, this.b);
                this.b.c.b(268435455L);
                this.b.a(8780, 0L);
                return;
            default:
                return;
        }
    }

    public void h(Message message) {
        switch (message.what) {
            case 8753:
                a aVar = new a(18514);
                aVar.a("value", a.c.b.a(GeneralFunction.m.a.n()));
                this.b.c.c(aVar);
                return;
            case 8779:
                this.b.d.h.b = 256;
                this.b.c.a(272, this.b);
                this.b.c.b(268435455L);
                this.b.a(8779, 0L);
                return;
            case 8780:
                this.b.d.h.b = 256;
                this.b.c.a(272, this.b);
                this.b.c.b(268435455L);
                this.b.a(8780, 0L);
                return;
            default:
                return;
        }
    }

    public void i(Message message) {
        switch (message.what) {
            case 8753:
                if (System.currentTimeMillis() - this.b.c.c.h.aG <= 200) {
                    this.b.a(8753);
                    return;
                }
                a aVar = new a(18514);
                aVar.a("setting", a.c.b.b(GeneralFunction.m.a.n()));
                this.b.c.c(aVar);
                this.b.c.c.h.aG = System.currentTimeMillis();
                return;
            case 8758:
                a aVar2 = new a(18515);
                aVar2.a("setting", a.c.b.c(GeneralFunction.m.a.o()));
                this.b.c.c(aVar2);
                return;
            case 8759:
                a aVar3 = new a(18516);
                aVar3.a("setting", a.c.b.d(GeneralFunction.m.a.p()));
                this.b.c.c(aVar3);
                return;
            case 8790:
                if (this.b.c.c.h.V == 0) {
                    this.b.c.a(400, this.b);
                    this.b.h(true);
                    this.b.c.c.h.V = 1;
                } else {
                    this.b.c.a(272, this.b);
                    this.b.h(false);
                    this.b.c.c.h.V = 0;
                }
                this.b.c.c(268435455);
                return;
            case 18514:
            case 18515:
            case 18516:
            default:
                return;
            case 32768:
                a("MSG_KEY_BACK hide effect tool menu", 3);
                this.b.a(8790);
                return;
        }
    }

    private void a() {
        if (this.b.c.c.h.ar <= 10) {
            this.b.a(8726, 0L);
        } else if (this.b.c.d.b()) {
            this.b.c.l();
            this.b.af();
        } else if (!this.b.c.c.h.at) {
            this.b.c.c.h.at = true;
            this.b.c.j();
        }
    }

    private void b() {
        this.b.f.k();
        this.b.f.l();
    }

    public void j(Message message) {
        boolean z = false;
        switch (message.what) {
            case 20507:
                if (GeneralFunction.m.a.a() == 0) {
                    this.b.c.c.h.z = d.a(c.f1314a, 0);
                } else {
                    if (this.b.c.c.k.b.ab == null) {
                        this.b.c.c.k.b.ab = this.b.ai();
                    }
                    if (this.b.c.c.k.b.ab != null) {
                        this.b.c.c.h.z = d.a(this.b.c.c.k.b.ab, 0);
                    }
                }
                k kVar = this.b.d.h;
                if (this.b.c.c.h.z < 52428800) {
                    z = true;
                }
                kVar.aM = z;
                a aVar = new a(8744);
                aVar.a("isMemoryFull", this.b.d.h.aM);
                this.b.a(aVar);
                return;
            case 20508:
                Bitmap G = this.b.G();
                Bitmap bitmap = null;
                if (G != null) {
                    bitmap = this.b.a(G, G.getHeight());
                    G.recycle();
                }
                this.b.d.k.b.f1320a.a(this.b.d.k.e.a(), this.b.d.k.f);
                a aVar2 = new a(8738);
                aVar2.a(bitmap);
                this.b.a(aVar2);
                return;
            case 20509:
            default:
                return;
            case 20510:
                a aVar3 = new a(8745);
                aVar3.a("avgPingTime", (int) GeneralFunction.k.a.a(this.b).a(this.b.c.c.h.ae));
                this.b.a(aVar3);
                return;
        }
    }

    private void c() {
        o.b(this.b);
        o.a((Context) this.b, true, true, this.b.getResources().getString(R.string.broadcast_no_internet_title), this.b.getResources().getString(R.string.broadcast_no_internet_description), this.b.getResources().getString(R.string.dialog_option_ok), 12039, true);
    }

    private void d() {
        if (this.b.c.c.h.ae == 0) {
            this.b.c.h();
            if (GeneralFunction.j.b.a()) {
                o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.facebook_live_signin_title), this.b.c.h().d(), new String[]{this.b.getResources().getString(R.string.broadcast_account_sign_out), this.b.getResources().getString(R.string.broadcast_account_confirm)}, new int[]{10753, 10774}, true);
                return;
            }
            this.b.a(10752, 0L);
        } else if (!this.b.c.h().b() || !this.b.C().a(this.b.c.h().c())) {
            this.b.a(10755, 0L);
        } else {
            o.a((Context) this.b, true, false, this.b.getResources().getString(R.string.youtube_live_signin_title), this.b.c.h().c(), new String[]{this.b.getResources().getString(R.string.broadcast_account_sign_out), this.b.getResources().getString(R.string.broadcast_account_confirm)}, new int[]{10756, 10775}, true);
        }
    }
}
