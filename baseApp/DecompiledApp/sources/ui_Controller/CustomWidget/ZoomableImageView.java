package ui_Controller.CustomWidget;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;

public class ZoomableImageView extends AppCompatImageView {

    /* renamed from: a  reason: collision with root package name */
    private Matrix f1290a;
    private Matrix b;
    private int c = 0;
    private final PointF d = new PointF();
    private final PointF e = new PointF();
    private float f = 4.0f;
    private float[] g;
    private int h;
    private int i;
    private float j = 1.0f;
    private float k;
    private float l;
    private int m;
    private ScaleGestureDetector n;
    private GestureDetector o;
    private boolean p = false;

    public ZoomableImageView(Context context) {
        super(context);
        a(context);
    }

    public ZoomableImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        super.setClickable(true);
        this.n = new ScaleGestureDetector(context, new b());
        this.o = new GestureDetector(context, new a());
        this.f1290a = new Matrix();
        this.b = new Matrix();
        this.g = new float[9];
        setImageMatrix(this.f1290a);
        setScaleType(ImageView.ScaleType.MATRIX);
        setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.CustomWidget.ZoomableImageView.AnonymousClass1 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                ZoomableImageView.this.n.onTouchEvent(motionEvent);
                ZoomableImageView.this.o.onTouchEvent(motionEvent);
                PointF pointF = new PointF(motionEvent.getX(), motionEvent.getY());
                switch (motionEvent.getAction()) {
                    case 0:
                        ZoomableImageView.this.d.set(pointF);
                        ZoomableImageView.this.e.set(ZoomableImageView.this.d);
                        ZoomableImageView.this.c = 1;
                        break;
                    case 1:
                        ZoomableImageView.this.c = 0;
                        int abs = (int) Math.abs(pointF.x - ZoomableImageView.this.e.x);
                        int abs2 = (int) Math.abs(pointF.y - ZoomableImageView.this.e.y);
                        if (abs < 3 && abs2 < 3) {
                            ZoomableImageView.this.performClick();
                            break;
                        }
                    case 2:
                        if (ZoomableImageView.this.c == 1) {
                            float f = pointF.x - ZoomableImageView.this.d.x;
                            float f2 = pointF.y - ZoomableImageView.this.d.y;
                            ZoomableImageView.this.f1290a.postTranslate(ZoomableImageView.this.b(f, (float) ZoomableImageView.this.h, ZoomableImageView.this.k * ZoomableImageView.this.j), ZoomableImageView.this.b(f2, (float) ZoomableImageView.this.i, ZoomableImageView.this.l * ZoomableImageView.this.j));
                            ZoomableImageView.this.b();
                            ZoomableImageView.this.d.set(pointF.x, pointF.y);
                            break;
                        }
                        break;
                    case 6:
                        ZoomableImageView.this.c = 0;
                        break;
                }
                ZoomableImageView.this.setImageMatrix(ZoomableImageView.this.f1290a);
                ZoomableImageView.this.invalidate();
                return false;
            }
        });
    }

    public void setMaxZoom(float f2) {
        if (f2 == 0.0f) {
            this.f = 4.0f;
        } else {
            this.f = f2;
        }
    }

    public void a(boolean z) {
        if (z) {
            setScaleType(ImageView.ScaleType.MATRIX);
        } else {
            setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
    }

    public void a() {
        this.f1290a.reset();
        this.j = 1.0f;
        setImageMatrix(this.f1290a);
        this.p = true;
    }

    /* access modifiers changed from: private */
    public class b extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private b() {
        }

        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            ZoomableImageView.this.c = 2;
            return true;
        }

        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            float scaleFactor = scaleGestureDetector.getScaleFactor();
            float f = ZoomableImageView.this.j;
            ZoomableImageView.this.j *= scaleFactor;
            if (ZoomableImageView.this.j > ZoomableImageView.this.f) {
                ZoomableImageView.this.j = ZoomableImageView.this.f;
                scaleFactor = ZoomableImageView.this.f / f;
            } else if (ZoomableImageView.this.j < 1.0f) {
                ZoomableImageView.this.j = 1.0f;
                scaleFactor = 1.0f / f;
            }
            if (ZoomableImageView.this.k * ZoomableImageView.this.j <= ((float) ZoomableImageView.this.h) || ZoomableImageView.this.l * ZoomableImageView.this.j <= ((float) ZoomableImageView.this.i)) {
                ZoomableImageView.this.f1290a.postScale(scaleFactor, scaleFactor, (float) (ZoomableImageView.this.h / 2), (float) (ZoomableImageView.this.i / 2));
            } else {
                ZoomableImageView.this.f1290a.postScale(scaleFactor, scaleFactor, scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
            }
            ZoomableImageView.this.b();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public class a extends GestureDetector.SimpleOnGestureListener {
        private a() {
        }

        public boolean onDoubleTap(MotionEvent motionEvent) {
            if (ZoomableImageView.this.getScaleType() == ImageView.ScaleType.MATRIX) {
                if (ZoomableImageView.this.j == 1.0f) {
                    ZoomableImageView.this.j = 2.0f;
                } else {
                    ZoomableImageView.this.j = 1.0f;
                }
                ZoomableImageView.this.f1290a.set(ZoomableImageView.this.b);
                if (((double) ZoomableImageView.this.j) != 1.0d) {
                    ZoomableImageView.this.f1290a.postScale(ZoomableImageView.this.j, ZoomableImageView.this.j, (float) (ZoomableImageView.this.h / 2), (float) (ZoomableImageView.this.i / 2));
                }
            }
            return super.onDoubleTap(motionEvent);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void b() {
        this.f1290a.getValues(this.g);
        float f2 = this.g[2];
        float f3 = this.g[5];
        float a2 = a(f2, (float) this.h, this.k * this.j);
        float a3 = a(f3, (float) this.i, this.l * this.j);
        if (a2 != 0.0f || a3 != 0.0f) {
            this.f1290a.postTranslate(a2, a3);
        }
    }

    private float a(float f2, float f3, float f4) {
        float f5;
        float f6;
        if (f4 <= f3) {
            f6 = f3 - f4;
            f5 = 0.0f;
        } else {
            f5 = f3 - f4;
            f6 = 0.0f;
        }
        if (f2 < f5) {
            return (-f2) + f5;
        }
        if (f2 > f6) {
            return (-f2) + f6;
        }
        return 0.0f;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float b(float f2, float f3, float f4) {
        if (f4 <= f3) {
            return 0.0f;
        }
        return f2;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        this.h = View.MeasureSpec.getSize(i2);
        this.i = View.MeasureSpec.getSize(i3);
        if ((this.m != this.h || this.m != this.i) && this.h != 0 && this.i != 0) {
            this.m = this.i;
            int i4 = this.h;
            if (this.j == 1.0f) {
                Drawable drawable = getDrawable();
                if (drawable != null && drawable.getIntrinsicWidth() != 0 && drawable.getIntrinsicHeight() != 0) {
                    int intrinsicWidth = drawable.getIntrinsicWidth();
                    int intrinsicHeight = drawable.getIntrinsicHeight();
                    Log.d("bmSize", "bmWidth: " + intrinsicWidth + " bmHeight : " + intrinsicHeight);
                    float min = Math.min(((float) this.h) / ((float) intrinsicWidth), ((float) this.i) / ((float) intrinsicHeight));
                    this.f1290a.setScale(min, min);
                    float f2 = (((float) this.i) - (((float) intrinsicHeight) * min)) / 2.0f;
                    float f3 = (((float) this.h) - (((float) intrinsicWidth) * min)) / 2.0f;
                    this.f1290a.postTranslate(f3, f2);
                    this.k = ((float) this.h) - (f3 * 2.0f);
                    this.l = ((float) this.i) - (f2 * 2.0f);
                    setImageMatrix(this.f1290a);
                    this.b.set(this.f1290a);
                    this.p = false;
                } else {
                    return;
                }
            } else if (this.p) {
                a();
                this.p = false;
            }
            b();
        }
    }
}
