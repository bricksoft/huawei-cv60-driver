package ui_Controller.CustomWidget.a.a;

import GeneralFunction.k;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import java.util.List;

public class d extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f1308a;
    private LayoutInflater b;
    private List<String> c;
    private int d;
    private int[] e;
    private int f;
    private int g;

    private void a(String str, int i) {
        if (i < this.g) {
            Log.e(getClass().getSimpleName(), str);
        }
    }

    public int getCount() {
        return this.c.size();
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        a aVar;
        a("MainMenuAdapter getView " + this.e[i], 4);
        if (view == null) {
            a aVar2 = new a();
            view = this.b.inflate(R.layout.style_main_menu, (ViewGroup) null);
            aVar2.f1309a = (TextView) view.findViewById(R.id.main_menu_optionText);
            aVar2.b = (ImageView) view.findViewById(R.id.IV_gallery_selectIcon);
            view.setTag(aVar2);
            aVar = aVar2;
        } else {
            aVar = (a) view.getTag();
        }
        float f2 = this.f1308a.getResources().getDisplayMetrics().scaledDensity;
        aVar.f1309a.setTextSize((float) k.a(this.f1308a, (float) ((this.d * 2) / 5)));
        aVar.f1309a.getLayoutParams().height = this.d;
        aVar.f1309a.setPadding(this.d / 5, 0, 0, 0);
        if (this.f == 2) {
            aVar.b.setImageResource(17170445);
            if (this.e[i] == 2) {
                aVar.f1309a.setTextColor(-7829368);
            } else if (this.e[i] == 0) {
                aVar.f1309a.setTextColor(-7829368);
            } else {
                aVar.f1309a.setTextColor(-1);
            }
        } else {
            aVar.b.setVisibility(0);
            if (this.e[i] == 2) {
                aVar.f1309a.setTextColor(-7829368);
                aVar.b.setImageResource(R.drawable.gallery_list_radiooff);
            } else if (this.e[i] == 0) {
                aVar.f1309a.setTextColor(-1);
                aVar.b.setImageResource(R.drawable.gallery_list_radioon);
            } else {
                aVar.f1309a.setTextColor(-1);
                aVar.b.setImageResource(R.drawable.gallery_list_radiooff);
            }
        }
        aVar.f1309a.setText(this.c.get(i));
        return view;
    }

    class a {

        /* renamed from: a  reason: collision with root package name */
        public TextView f1309a;
        public ImageView b;

        a() {
        }
    }
}
