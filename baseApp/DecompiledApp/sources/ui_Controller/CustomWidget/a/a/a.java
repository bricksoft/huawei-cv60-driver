package ui_Controller.CustomWidget.a.a;

import GeneralFunction.j;
import GeneralFunction.k;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import com.tonicartos.widget.stickygridheaders.d;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import ui_Controller.CustomWidget.a;
import ui_Controller.b.c;

public class a<T> extends BaseAdapter implements d {
    private int A = 0;
    private int B = 0;
    private int C = 0;
    private boolean D = false;
    private int E = -1090519040;
    private int F = 3;
    private a.d G = new a.d() {
        /* class ui_Controller.CustomWidget.a.a.a.AnonymousClass1 */

        @Override // ui_Controller.CustomWidget.a.d
        public void a(int i, Bitmap bitmap) {
            if (bitmap != null) {
                Set<Map.Entry> entrySet = a.this.i.entrySet();
                ArrayList arrayList = new ArrayList();
                if (entrySet != null) {
                    for (Map.Entry entry : entrySet) {
                        arrayList.add((b) entry.getKey());
                    }
                    for (int i2 = 0; i2 < arrayList.size(); i2++) {
                        Bitmap bitmap2 = (Bitmap) a.this.i.get(arrayList.get(i2));
                        if (bitmap2 != null && bitmap2 == bitmap) {
                            a.this.a((a) ("Release bitmap link! " + bitmap2), (String) 3);
                            ((b) arrayList.get(i2)).f1306a.setImageBitmap(null);
                        }
                    }
                }
            }
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public GeneralFunction.e.d f1302a;
    final j b = new j();
    private AbstractC0101a c;
    private Context d = null;
    private LayoutInflater e;
    private List<c> f;
    private ArrayList<e> g;
    private HashMap<Integer, b> h;
    private HashMap<b, Bitmap> i;
    private ui_Controller.CustomWidget.a j;
    private int k;
    private int l = 0;
    private boolean m = false;
    private int n = 0;
    private int o = 0;
    private int p = 0;
    private int q = 0;
    private int r = 0;
    private int s = 0;
    private int t = 0;
    private int u = 0;
    private int v = 0;
    private int w = 0;
    private int x = 0;
    private int y = 0;
    private int z = -1;

    /* renamed from: ui_Controller.CustomWidget.a.a.a$a  reason: collision with other inner class name */
    public interface AbstractC0101a {
        void a();

        void a(int i);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        if (i2 <= this.F) {
            Log.e(getClass().getSimpleName(), str);
        }
    }

    @SuppressLint({"UseSparseArrays"})
    public a(Context context, GeneralFunction.e.d dVar, int i2, int i3, boolean z2, ui_Controller.CustomWidget.a aVar) {
        c cVar;
        this.e = LayoutInflater.from(context);
        ArrayList arrayList = new ArrayList();
        for (int i4 = 0; i4 < dVar.e(); i4++) {
            if (z2) {
                cVar = new c(dVar.s(i4), dVar.t(i4), dVar.h(i4));
            } else {
                cVar = new c(dVar.s(i4), dVar.t(i4), dVar.g(i4));
            }
            arrayList.add(cVar);
        }
        a((List<c>) arrayList);
        this.d = context;
        this.f1302a = dVar;
        this.f = arrayList;
        this.l = i2;
        this.m = z2;
        this.h = new HashMap<>();
        this.i = new HashMap<>();
        this.j = aVar;
        this.j.a(this.G);
        this.k = i3;
        this.n = R.drawable.gallery_nxn_select_yes;
        this.o = R.drawable.gallery_nxn_select_no;
        this.p = R.drawable.gallery_nxn_select_yes_large;
        this.q = R.drawable.gallery_nxn_select_no_large;
        this.r = R.drawable.gallery_nxn_imageloading;
        this.s = R.drawable.gallery_nxn_imageloading_large;
        this.t = R.drawable.gallery_nxn_videoplay;
        this.u = R.drawable.gallery_nxn_filetype_burst;
        this.v = R.drawable.gallery_nxn_filetype_timelapse;
        this.w = R.drawable.gallery_icon_video;
        this.x = R.drawable.gallery_nxn_gif;
        this.A = R.drawable.gallery_nxn_videoplay;
        this.B = R.drawable.gallery_nxn_imageloading;
        this.C = R.drawable.gallery_nxn_imageloading_large;
        h();
    }

    public void b(int i2) {
        a("setMode:" + i2, 2);
        this.z = -1;
        this.y = i2;
    }

    public void a() {
        a("GridViewAdapter Deinit", 2);
        Set<Map.Entry<Integer, b>> entrySet = this.h.entrySet();
        ArrayList arrayList = new ArrayList();
        if (entrySet != null) {
            for (Map.Entry<Integer, b> entry : entrySet) {
                arrayList.add(entry.getKey());
            }
            int size = this.h.size();
            for (int i2 = 0; i2 < size; i2++) {
                Integer num = (Integer) arrayList.get(i2);
                if (this.h.get(num) != null) {
                    b bVar = this.h.get(num);
                    if (bVar.f1306a.getDrawable() != null) {
                        bVar.f1306a.getDrawable().setCallback(null);
                    }
                    bVar.f1306a.setImageBitmap(null);
                    bVar.f1306a.setImageResource(17170445);
                    bVar.b.setImageResource(17170445);
                    bVar.d.setImageResource(17170445);
                    bVar.e.setImageResource(17170445);
                    bVar.f.setImageResource(17170445);
                    bVar.g.setImageResource(17170445);
                    bVar.c.setImageResource(17170445);
                    bVar.i.setImageResource(17170445);
                    bVar.j.setText("");
                    this.h.remove(num);
                }
            }
            this.f.clear();
            this.j.a();
            this.j = null;
            this.i.clear();
            b();
        }
    }

    public void c(int i2) {
        this.k = i2;
        b();
    }

    public void b() {
        notifyDataSetChanged();
    }

    public void d(int i2) {
        a("DeleteGridViewItem:" + i2, 3);
        b bVar = this.h.get(Integer.valueOf(i2));
        if (bVar != null) {
            if (bVar.f1306a.getDrawable() != null) {
                bVar.f1306a.getDrawable().setCallback(null);
            }
            bVar.f1306a.setImageBitmap(null);
            bVar.f1306a.setImageResource(17170445);
            bVar.b.setImageResource(17170445);
            bVar.d.setImageResource(17170445);
            bVar.e.setImageResource(17170445);
            bVar.f.setImageResource(17170445);
            bVar.g.setImageResource(17170445);
            bVar.c.setImageResource(17170445);
            bVar.i.setImageResource(17170445);
            bVar.j.setText("");
            this.h.remove(Integer.valueOf(i2));
        }
        this.f.remove(i2);
        a(this.f);
        this.j.a(i2);
        b();
    }

    public void a(ArrayList<GeneralFunction.e.a> arrayList) {
        int i2 = 0;
        while (i2 < this.f.size()) {
            int a2 = this.f.get(i2).a();
            int b2 = this.f.get(i2).b();
            if (i2 > arrayList.size() - 1) {
                d(i2);
            } else if (a2 == arrayList.get(i2).b) {
                int i3 = arrayList.get(i2).b;
                int intValue = arrayList.get(i2).c.get(0).intValue();
                if (b2 == intValue) {
                    i2++;
                } else {
                    a(i2, i3, intValue);
                    i2++;
                }
            } else {
                d(i2);
            }
        }
    }

    public void a(int i2, int i3, int i4) {
        this.f.get(i2).a(i3, i4);
        b();
    }

    public void c() {
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            this.f.get(i2).a(false);
        }
        for (int i3 = 0; i3 < this.g.size(); i3++) {
            this.g.get(i3).f1310a = false;
        }
    }

    public int d() {
        return this.f.size();
    }

    public int e() {
        int i2 = 0;
        for (int i3 = 0; i3 < this.f.size(); i3++) {
            if (this.f.get(i3).e()) {
                i2++;
            }
        }
        return i2;
    }

    public int e(int i2) {
        int i3 = 0;
        for (int i4 = 0; i4 < this.f.size(); i4++) {
            if (this.f.get(i4).c() == i2) {
                i3++;
            }
        }
        a("GetHeaderMemberNum " + i2 + " " + i3, 4);
        return i3;
    }

    public long f() {
        long j2 = 0;
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            if (this.f.get(i2).e()) {
                j2 += this.f1302a.f(i2);
            }
        }
        a("GetTotalSelectFileSize " + ((j2 / 1024) / 1024) + "MB", 4);
        return j2;
    }

    public void a(int i2, boolean z2) {
        int c2 = this.f.get(i2).c();
        this.f.get(i2).a(z2);
        a("SetSelectStatus " + i2 + " " + z2, 4);
        if (g(c2)) {
            this.g.get(c2).f1310a = true;
        } else {
            this.g.get(c2).f1310a = false;
        }
    }

    public boolean a(boolean z2, boolean z3) {
        return a(z2, -1L, z3, true);
    }

    public boolean a(boolean z2, long j2, boolean z3, boolean z4) {
        long j3 = 0;
        if (z2) {
            j3 = f();
        }
        long j4 = j3;
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            if (!z2) {
                a(i2, false);
            } else if (!z3 && this.f.get(i2).g()) {
                a(i2, false);
            } else if (!z4 && !this.f.get(i2).f()) {
                a(i2, false);
            } else if (j2 == -1) {
                a(i2, true);
            } else {
                j4 += this.f1302a.f(i2);
                if (j4 > j2) {
                    a("Select All Over Size Limit", 4);
                    b();
                    return false;
                }
                a(i2, true);
            }
        }
        b();
        return true;
    }

    public boolean a(int i2, long j2, boolean z2, boolean z3) {
        boolean z4 = !this.g.get(i2).f1310a;
        long j3 = 0;
        if (z4) {
            j3 = f();
        }
        long j4 = j3;
        for (int i3 = 0; i3 < this.f.size(); i3++) {
            if (this.f.get(i3).c() == i2) {
                if (!z4) {
                    a(i3, false);
                } else if (!z2 && this.f.get(i3).g()) {
                    a(i3, false);
                } else if (!z3 && !this.f.get(i3).f()) {
                    a(i3, false);
                } else if (j2 == -1) {
                    a(i3, true);
                } else {
                    j4 += this.f1302a.f(i3);
                    if (j4 > j2) {
                        a("Select All Over Size Limit", 4);
                        b();
                        return false;
                    }
                    a(i3, true);
                }
            }
        }
        this.g.get(i2).f1310a = z4;
        b();
        return true;
    }

    public void a(int i2, int i3) {
        this.f.get(i2).b(i3);
    }

    public boolean f(int i2) {
        return this.f.get(i2).e();
    }

    public boolean g(int i2) {
        for (int i3 = 0; i3 < this.f.size(); i3++) {
            if (this.f.get(i3).c() == i2 && !this.f.get(i3).e()) {
                return false;
            }
        }
        return true;
    }

    public boolean h(int i2) {
        return this.f.get(i2).g();
    }

    public int g() {
        int e2 = e();
        if (e2 == 0) {
            return 0;
        }
        if (e2 == this.f.size()) {
            return 2;
        }
        return 1;
    }

    private void a(int i2, b bVar) {
        if (this.h.containsValue(bVar)) {
            Iterator<Map.Entry<Integer, b>> it = this.h.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry<Integer, b> next = it.next();
                if (next.getValue().equals(bVar)) {
                    a("entry.getKey(): " + next.getKey(), 3);
                    this.h.remove(next.getKey());
                    break;
                }
            }
        }
        if (this.h.containsKey(Integer.valueOf(i2))) {
            this.h.remove(Integer.valueOf(i2));
        }
        this.h.put(Integer.valueOf(i2), bVar);
    }

    private void a(ImageView imageView, boolean z2) {
        if (z2) {
            imageView.setColorFilter(this.E);
        } else {
            imageView.setColorFilter(0);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(Bitmap bitmap, int i2) {
        this.b.a();
        a("LoadThumbnail:" + bitmap + " " + i2, 2);
        if (this.h == null) {
            this.b.b();
        } else if (i2 > this.f.size() - 1) {
            a("Error position: " + this.f.size() + " " + i2, 3);
            this.b.b();
        } else {
            b bVar = this.h.get(Integer.valueOf(i2));
            a("loadViewHolder:" + bVar, 3);
            if (bVar != null) {
                if (bVar.h != null) {
                    bVar.h.setVisibility(4);
                }
                this.i.put(bVar, bitmap);
                if (bitmap == null) {
                    a(i2, 2);
                    bVar.f1306a.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    bVar.b.setImageResource(17170445);
                    if (this.m) {
                        if (this.k == 2) {
                            a(bVar.f1306a, true);
                        } else {
                            a(bVar.f1306a, false);
                        }
                        if (this.k == 5) {
                            a(bVar.f1306a, true);
                        } else {
                            a(bVar.f1306a, false);
                        }
                        if (this.y == 3) {
                            bVar.f1306a.setImageResource(this.r);
                        } else {
                            bVar.f1306a.setImageResource(this.s);
                        }
                        a("imageLoaded, local_loadViewHolder.ivImage:" + bVar.f1306a.hashCode(), 5);
                    } else {
                        if (this.k == 3) {
                            a(bVar.f1306a, true);
                        } else {
                            a(bVar.f1306a, false);
                        }
                        if (this.y == 3) {
                            bVar.f1306a.setImageResource(this.r);
                        } else {
                            bVar.f1306a.setImageResource(this.s);
                        }
                        a("imageLoaded, loadViewHolder.ivImage:" + bVar.f1306a.hashCode(), 5);
                    }
                } else if (bitmap.getWidth() / bitmap.getHeight() >= 3) {
                    a(i2, 1);
                    bVar.f1306a.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    bVar.f1306a.setImageBitmap(bitmap);
                } else {
                    a(i2, 1);
                    bVar.f1306a.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    bVar.f1306a.setImageBitmap(bitmap);
                }
            }
            this.b.b();
        }
    }

    public void h() {
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            a("=======[" + i2 + "]=======", 4);
            a("" + this.f.get(i2).h(), 4);
        }
    }

    private void a(List<c> list) {
        this.g = new ArrayList<>();
        int i2 = -1;
        long j2 = -1;
        int offset = TimeZone.getDefault().getOffset(new Date().getTime());
        int i3 = 0;
        while (i3 < list.size()) {
            c cVar = list.get(i3);
            long d2 = (cVar.d() + ((long) offset)) / 86400000;
            if (d2 != j2) {
                this.g.add(new e());
                i2++;
            } else {
                d2 = j2;
            }
            cVar.a(i2);
            i3++;
            j2 = d2;
        }
    }

    public void a(AbstractC0101a aVar) {
        this.c = aVar;
    }

    public void i() {
        this.c.a();
    }

    public void i(int i2) {
        this.c.a(i2);
    }

    public int getCount() {
        if (this.f == null) {
            return 0;
        }
        return this.f.size();
    }

    public Object getItem(int i2) {
        return this.f.get(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        b bVar;
        String str;
        a("getView, position:" + i2 + " parent.getChildCount():" + viewGroup.getChildCount(), 2);
        String m2 = this.f1302a.m(i2);
        String o2 = this.f1302a.o(i2);
        int b2 = this.f1302a.b(i2);
        int c2 = this.f1302a.c(i2);
        int a2 = this.f1302a.a(i2);
        if (m2 == null || o2 == null) {
            this.D = true;
        }
        a("test path " + m2, 4);
        this.b.a();
        if (view == null) {
            b bVar2 = new b();
            view = this.e.inflate(R.layout.style_grid, (ViewGroup) null);
            bVar2.f1306a = (ImageView) view.findViewById(R.id.grid_img);
            bVar2.b = (ImageView) view.findViewById(R.id.grid_movieIcon);
            bVar2.d = (ImageView) view.findViewById(R.id.grid_BurstIcon);
            bVar2.e = (ImageView) view.findViewById(R.id.grid_TimelapseIcon);
            bVar2.f = (ImageView) view.findViewById(R.id.grid_panoramaIcon);
            bVar2.g = (ImageView) view.findViewById(R.id.grid_GifIcon);
            bVar2.c = (ImageView) view.findViewById(R.id.grid_checkbox);
            bVar2.f1306a.setScaleType(ImageView.ScaleType.CENTER_CROP);
            bVar2.f1306a.getLayoutParams().height = this.l;
            bVar2.h = (LinearLayout) view.findViewById(R.id.grid_loadingLayout);
            bVar2.i = (ImageView) view.findViewById(R.id.grid_video360Icon);
            bVar2.j = (TextView) view.findViewById(R.id.grid_tv_videoTime);
            bVar2.j.setTextSize((float) k.a(this.d, (float) (this.l / 10)));
            if (this.y == 3) {
                bVar2.h.setBackgroundResource(this.B);
            } else {
                bVar2.h.setBackgroundResource(this.C);
            }
            bVar2.h.getLayoutParams().height = this.l;
            bVar2.c.getLayoutParams().height = this.l;
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(bVar2.d.getLayoutParams());
            layoutParams.gravity = 83;
            layoutParams.height = (this.l * 9) / 50;
            layoutParams.width = (this.l * 9) / 50;
            layoutParams.setMargins(this.l / 32, 0, 0, this.l / 32);
            bVar2.d.setLayoutParams(layoutParams);
            bVar2.e.setLayoutParams(layoutParams);
            bVar2.f.setLayoutParams(layoutParams);
            layoutParams.width = (this.l * 9) / 25;
            bVar2.j.setLayoutParams(layoutParams);
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(bVar2.b.getLayoutParams());
            layoutParams2.gravity = 83;
            layoutParams2.height = (this.l * 10) / 50;
            layoutParams2.width = (this.l * 10) / 50;
            layoutParams2.setMargins(this.l / 15, 0, 0, this.l / 4);
            bVar2.b.setLayoutParams(layoutParams2);
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(bVar2.g.getLayoutParams());
            layoutParams3.gravity = 51;
            layoutParams3.height = (this.l * 11) / 50;
            layoutParams3.width = (this.l * 11) / 50;
            layoutParams3.setMargins(this.l / 32, 0, 0, 0);
            bVar2.g.setLayoutParams(layoutParams3);
            FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(bVar2.d.getLayoutParams());
            layoutParams4.gravity = 17;
            layoutParams4.height = (this.l * 9) / 25;
            layoutParams4.width = (this.l * 9) / 25;
            bVar2.i.setLayoutParams(layoutParams4);
            bVar2.k = -1;
            view.setTag(bVar2);
            a(i2, bVar2);
            this.i.put(bVar2, null);
            a("getView, convertView is null, viewList position:" + i2 + ", viewList viewHolder.ivImage.hashCode():" + bVar2.f1306a.hashCode(), 5);
            bVar = bVar2;
        } else {
            b bVar3 = (b) view.getTag();
            a(i2, bVar3);
            a("getView, convertView is not null, viewList position:" + i2 + ", viewList viewHolder.ivImage.hashCode():" + bVar3.f1306a.hashCode(), 5);
            bVar = bVar3;
        }
        this.b.b();
        if (this.D) {
            a("delete position:" + i2, 3);
            bVar.f1306a.setImageResource(17170445);
            bVar.c.setImageResource(17170445);
        } else {
            if (!new File(o2).exists()) {
                str = null;
            } else {
                str = m2;
            }
            if (str != null) {
                if (bVar.k == -1) {
                    bVar.k = i2;
                    bVar.h.setVisibility(0);
                } else if (bVar.k != i2) {
                    bVar.h.setVisibility(0);
                    bVar.k = i2;
                }
                this.j.b(str, i2, i2, b2, c2, new a.AbstractC0100a() {
                    /* class ui_Controller.CustomWidget.a.a.a.AnonymousClass2 */

                    @Override // ui_Controller.CustomWidget.a.AbstractC0100a
                    public void a(Bitmap bitmap, int i) {
                        a.this.a((a) bitmap, (Bitmap) i);
                        a.this.i();
                    }
                });
            } else {
                i(i2);
            }
            if (h(i2)) {
                bVar.h.setVisibility(4);
                if (this.m) {
                    if (this.y == 3) {
                        bVar.f1306a.setImageResource(this.r);
                    } else {
                        bVar.f1306a.setImageResource(this.s);
                    }
                    a("isFileError, local_ViewHolder.ivImage:" + bVar.f1306a.hashCode(), 5);
                } else {
                    a(bVar.f1306a, true);
                    if (this.y == 3) {
                        bVar.f1306a.setImageResource(this.r);
                    } else {
                        bVar.f1306a.setImageResource(this.s);
                    }
                    a("isFileError, ViewHolder.ivImage:" + bVar.f1306a.hashCode(), 5);
                }
            }
            if (this.k == 5) {
                if (h(i2)) {
                    a(bVar.f1306a, true);
                } else {
                    a(bVar.f1306a, false);
                }
            } else if (this.k == 2) {
                if (h(i2)) {
                    a(bVar.f1306a, true);
                } else {
                    a(bVar.f1306a, false);
                }
            } else if (this.k == 3) {
                if (h(i2)) {
                    a(bVar.f1306a, true);
                } else {
                    a(bVar.f1306a, false);
                }
            } else if (this.k == 4) {
                if (!h(i2)) {
                    switch (a2) {
                        case 3:
                            a(bVar.f1306a, false);
                            break;
                        default:
                            a(bVar.f1306a, true);
                            break;
                    }
                } else {
                    a(bVar.f1306a, true);
                }
            } else {
                a(bVar.f1306a, false);
            }
            switch (a2) {
                case 0:
                    bVar.b.setImageResource(17170445);
                    bVar.e.setImageResource(17170445);
                    bVar.d.setImageResource(17170445);
                    bVar.f.setImageResource(17170445);
                    bVar.g.setImageResource(17170445);
                    bVar.i.setImageResource(17170445);
                    bVar.j.setText("");
                    break;
                case 1:
                case 5:
                    bVar.b.setImageResource(this.t);
                    bVar.e.setImageResource(17170445);
                    bVar.d.setImageResource(17170445);
                    bVar.f.setImageResource(17170445);
                    bVar.g.setImageResource(17170445);
                    bVar.i.setImageResource(17170445);
                    int d2 = this.f1302a.d(this.f.get(i2).a(), this.f.get(i2).b());
                    bVar.j.setText(String.format("%02d:%02d", Integer.valueOf(d2 / 60), Integer.valueOf(d2 % 60)));
                    break;
                case 2:
                    bVar.b.setImageResource(17170445);
                    bVar.e.setImageResource(17170445);
                    bVar.d.setImageResource(17170445);
                    bVar.f.setImageResource(17170445);
                    bVar.g.setImageResource(17170445);
                    bVar.i.setImageResource(17170445);
                    bVar.j.setText("");
                    break;
                case 3:
                    bVar.b.setImageResource(17170445);
                    bVar.e.setImageResource(17170445);
                    bVar.d.setImageResource(17170445);
                    bVar.f.setImageResource(17170445);
                    bVar.g.setImageResource(17170445);
                    bVar.i.setImageResource(17170445);
                    bVar.j.setText("");
                    break;
                case 4:
                    bVar.b.setImageResource(17170445);
                    bVar.e.setImageResource(17170445);
                    bVar.d.setImageResource(17170445);
                    bVar.f.setImageResource(17170445);
                    bVar.g.setImageResource(17170445);
                    bVar.i.setImageResource(17170445);
                    bVar.j.setText("");
                    break;
                case 6:
                    bVar.b.setImageResource(17170445);
                    bVar.e.setImageResource(17170445);
                    bVar.d.setImageResource(17170445);
                    bVar.f.setImageResource(17170445);
                    bVar.g.setImageResource(this.x);
                    bVar.i.setImageResource(17170445);
                    bVar.j.setText("");
                    break;
                default:
                    bVar.b.setImageResource(17170445);
                    bVar.e.setImageResource(17170445);
                    bVar.d.setImageResource(17170445);
                    bVar.f.setImageResource(17170445);
                    bVar.g.setImageResource(17170445);
                    bVar.i.setImageResource(17170445);
                    bVar.j.setText("");
                    break;
            }
            if (this.k > 0) {
                bVar.c.setVisibility(0);
                if (this.f.get(i2).e()) {
                    if (this.y == 3) {
                        bVar.c.setImageResource(this.n);
                    } else {
                        bVar.c.setImageResource(this.p);
                    }
                } else if (this.y == 3) {
                    bVar.c.setImageResource(this.o);
                } else {
                    bVar.c.setImageResource(this.q);
                }
            } else {
                bVar.c.setVisibility(4);
            }
        }
        return view;
    }

    @Override // com.tonicartos.widget.stickygridheaders.d
    public View a(int i2, View view, ViewGroup viewGroup) {
        c cVar;
        a("getHeaderView " + i2 + " " + this.k, 4);
        if (this.f == null) {
            return null;
        }
        if (view == null) {
            DisplayMetrics displayMetrics = this.d.getResources().getDisplayMetrics();
            int max = Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels) / 5;
            cVar = new c();
            view = this.e.inflate(R.layout.style_grid_header, viewGroup, false);
            cVar.f1307a = (ImageView) view.findViewById(R.id.grid_header_selectIcon);
            cVar.b = (TextView) view.findViewById(R.id.grid_header_title);
            cVar.c = (LinearLayout) view.findViewById(R.id.grid_header_restSpace);
            cVar.b.setTextSize((float) k.a(this.d, (float) (max / 9)));
            cVar.f1307a.getLayoutParams().width = (int) (((double) max) / 5.5d);
            cVar.f1307a.getLayoutParams().height = (int) (((double) max) / 5.5d);
            cVar.b.getLayoutParams().height = max / 4;
            cVar.c.setOnTouchListener(new View.OnTouchListener() {
                /* class ui_Controller.CustomWidget.a.a.a.AnonymousClass3 */

                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });
            view.setTag(cVar);
        } else {
            cVar = (c) view.getTag();
        }
        int c2 = this.f.get(i2).c();
        if (this.k == 0) {
            cVar.f1307a.setVisibility(4);
        } else {
            cVar.f1307a.setVisibility(0);
            if (this.g.get(c2).f1310a) {
                cVar.f1307a.setImageResource(R.drawable.gallery_nxn_date_select_yes);
            } else {
                cVar.f1307a.setImageResource(R.drawable.gallery_nxn_date_select_no);
            }
        }
        int a2 = GeneralFunction.d.a();
        if (a2 == 10) {
            new SimpleDateFormat("MMM dd,yyyy", Locale.UK);
        } else {
            new SimpleDateFormat();
        }
        String valueOf = String.valueOf(Calendar.getInstance().get(1));
        long d2 = this.f.get(i2).d();
        if (d2 == 0) {
            cVar.b.setText(R.string.official_sample);
            return view;
        }
        String format = DateFormat.getDateFormat(this.d).format(new Date(d2));
        if (a2 == 0) {
            if (format.contains(valueOf + "年")) {
                format = format.replace(valueOf + "年", "");
            }
        } else if (format.contains(valueOf)) {
            format = format.replace("," + valueOf, " ");
        }
        cVar.b.setText(" " + format + " (" + e(c2) + ")");
        return view;
    }

    @Override // com.tonicartos.widget.stickygridheaders.d
    public long a(int i2) {
        return (long) this.f.get(i2).c();
    }
}
