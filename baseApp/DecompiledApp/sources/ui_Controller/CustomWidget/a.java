package ui_Controller.CustomWidget;

import GeneralFunction.j;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final j f1294a = new j();
    private ExifInterface b = null;
    private ExecutorService c = null;
    private ExecutorService d = null;
    private Handler e = new Handler(Looper.getMainLooper());
    private HandlerThread f = null;
    private Handler g = null;
    private BitmapFactory.Options h = null;
    private Map<Integer, Bitmap> i = null;
    private Map<Integer, Bitmap> j = null;
    private b k = null;
    private int l = 0;
    private int m = 0;
    private d n = null;
    private d o = null;
    private final j p = new j();
    private boolean q = false;

    /* renamed from: ui_Controller.CustomWidget.a$a  reason: collision with other inner class name */
    public interface AbstractC0100a {
        void a(Bitmap bitmap, int i);
    }

    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public String f1311a = null;
        public int b = 0;
        public int c = 0;
        public int d = 0;
        public int e = 0;
        public boolean f = true;
        public boolean g = true;
        public boolean h = true;
        public int i = 10;
        public int j = 0;
    }

    public interface d {
        void a(int i, Bitmap bitmap);
    }

    /* access modifiers changed from: private */
    public class c implements Runnable {
        private boolean b = false;
        private Semaphore c = new Semaphore(0, true);
        private d d = null;
        private int e = 0;
        private Bitmap f = null;

        public c(d dVar, int i, Bitmap bitmap) {
            this.d = dVar;
            this.e = i;
            this.f = bitmap;
        }

        public void a() {
            if (!this.b) {
                try {
                    this.c.acquire();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                this.b = true;
            }
        }

        public void run() {
            this.d.a(this.e, this.f);
            this.c.release();
        }
    }

    /* access modifiers changed from: private */
    public static void b(String str, int i2) {
        GeneralFunction.d.a("AsyncImageLoader", str, i2);
    }

    @SuppressLint({"UseSparseArrays"})
    public a(b bVar) {
        this.k = bVar;
        this.i = new HashMap();
        this.j = new HashMap();
        this.c = Executors.newFixedThreadPool(1);
        this.d = Executors.newFixedThreadPool(1);
        this.h = new BitmapFactory.Options();
        this.q = false;
        b();
        new File(this.k.f1311a + "/").mkdirs();
    }

    private void b() {
        if (this.k.h) {
            this.g = new Handler(Looper.getMainLooper());
            return;
        }
        this.f = new HandlerThread("FileLoadThread");
        this.f.start();
        this.g = new Handler(this.f.getLooper());
    }

    public void a() {
        b("AsyncImageLoader Release", 3);
        this.p.a();
        this.q = true;
        ArrayList<Integer> a2 = a(this.i);
        for (int i2 = 0; i2 < a2.size(); i2++) {
            Bitmap bitmap = this.i.get(a2.get(i2));
            if (bitmap != null && !bitmap.isRecycled()) {
                if (this.n != null) {
                    this.n.a(a2.get(i2).intValue(), bitmap);
                }
                bitmap.recycle();
            }
            this.i.remove(a2.get(i2));
        }
        ArrayList<Integer> a3 = a(this.j);
        for (int i3 = 0; i3 < a3.size(); i3++) {
            Bitmap bitmap2 = this.j.get(a3.get(i3));
            if (bitmap2 != null && !bitmap2.isRecycled()) {
                if (this.o != null) {
                    this.o.a(a3.get(i3).intValue(), bitmap2);
                }
                bitmap2.recycle();
            }
            this.j.remove(a3.get(i3));
        }
        if (this.f != null) {
            this.f.quit();
        }
        this.g = null;
        this.c.shutdown();
        this.c.shutdownNow();
        this.c = null;
        this.d.shutdown();
        this.d.shutdownNow();
        this.d = null;
        this.p.b();
    }

    public void a(d dVar) {
        this.o = dVar;
    }

    public void a(final String str, final int i2, int i3, final int i4, final int i5, final AbstractC0100a aVar) {
        this.l = i3;
        this.d.submit(new Runnable() {
            /* class ui_Controller.CustomWidget.a.AnonymousClass1 */

            public void run() {
                if (!a.this.q) {
                    int i = a.this.l;
                    if ((i2 > i && i2 - i > a.this.k.i) || (i2 < i && i - i2 > a.this.k.i)) {
                        a.b("AsyncLoadMainImageBitmap out of range thus release:" + i2, 2);
                    } else if (a.this.i.containsKey(Integer.valueOf(i2))) {
                        a.this.g.post(new Runnable() {
                            /* class ui_Controller.CustomWidget.a.AnonymousClass1.AnonymousClass1 */

                            public void run() {
                                a.this.a((a) aVar, (AbstractC0100a) ((Bitmap) a.this.i.get(Integer.valueOf(i2))), (Bitmap) i2);
                            }
                        });
                    } else {
                        if (a.this.k.f) {
                            a.this.d();
                        }
                        final Bitmap b2 = a.this.b((a) str, (String) i4, i5);
                        a.this.p.a();
                        if (a.this.q) {
                            if (b2 != null && !b2.isRecycled()) {
                                b2.recycle();
                            }
                            a.this.p.b();
                            return;
                        }
                        if (a.this.k.f) {
                            a.this.i.put(Integer.valueOf(i2), b2);
                        }
                        a.this.p.b();
                        a.this.g.post(new Runnable() {
                            /* class ui_Controller.CustomWidget.a.AnonymousClass1.AnonymousClass2 */

                            public void run() {
                                a.b("AsyncLoadMainImageBitmap post:" + b2 + " " + i2, 4);
                                a.this.a((a) aVar, (AbstractC0100a) b2, (Bitmap) i2);
                            }
                        });
                    }
                }
            }
        });
    }

    public void b(final String str, final int i2, int i3, final int i4, final int i5, final AbstractC0100a aVar) {
        this.m = i3;
        this.c.submit(new Runnable() {
            /* class ui_Controller.CustomWidget.a.AnonymousClass2 */

            public void run() {
                a.b("AsyncLoadThumbnialBitmap S " + str + " " + i2, 4);
                if (!a.this.q) {
                    int i = a.this.m;
                    if ((i2 <= i || i2 - i <= a.this.k.i) && (i2 >= i || i - i2 <= a.this.k.i)) {
                        if (a.this.j.containsKey(Integer.valueOf(i2))) {
                            a.this.g.post(new Runnable() {
                                /* class ui_Controller.CustomWidget.a.AnonymousClass2.AnonymousClass1 */

                                public void run() {
                                    a.this.a((a) aVar, (AbstractC0100a) ((Bitmap) a.this.j.get(Integer.valueOf(i2))), (Bitmap) i2);
                                }
                            });
                        } else {
                            if (a.this.k.g) {
                                a.this.c();
                            }
                            if (a.this.p.a() < 0) {
                                a.b("pMutex.Get() fail " + i2, 2);
                                return;
                            }
                            final Bitmap a2 = a.this.a((a) str, (String) i4, i5);
                            if (a.this.q) {
                                if (a2 != null && !a2.isRecycled()) {
                                    a2.recycle();
                                }
                                a.this.p.b();
                                return;
                            }
                            if (a.this.k.g) {
                                a.this.j.put(Integer.valueOf(i2), a2);
                            }
                            a.this.p.b();
                            a.this.g.post(new Runnable() {
                                /* class ui_Controller.CustomWidget.a.AnonymousClass2.AnonymousClass2 */

                                public void run() {
                                    a.this.a((a) aVar, (AbstractC0100a) a2, (Bitmap) i2);
                                }
                            });
                        }
                        a.b("AsyncLoadThumbnialBitmap E" + str + " " + i2, 4);
                        return;
                    }
                    a.b("AsyncLoadThumbnailBitmap out of range thus release:" + i2, 2);
                }
            }
        });
    }

    public void a(int i2) {
        this.p.a();
        ArrayList<Integer> a2 = a(this.j);
        HashMap hashMap = new HashMap();
        for (int i3 = 0; i3 < a2.size(); i3++) {
            if (a2.get(i3).intValue() > i2) {
                hashMap.put(Integer.valueOf(a2.get(i3).intValue() - 1), this.j.get(a2.get(i3)));
            } else if (a2.get(i3).intValue() != i2) {
                hashMap.put(a2.get(i3), this.j.get(a2.get(i3)));
            } else if (this.j.get(a2.get(i3)) != null && !this.j.get(a2.get(i3)).isRecycled()) {
                b("DeleteImage:" + this.j.get(a2.get(i3)), 4);
                if (this.o != null) {
                    this.o.a(a2.get(i3).intValue(), this.j.get(a2.get(i3)));
                }
                this.j.get(a2.get(i3)).recycle();
            }
        }
        this.j = hashMap;
        ArrayList<Integer> a3 = a(this.i);
        HashMap hashMap2 = new HashMap();
        for (int i4 = 0; i4 < a3.size(); i4++) {
            if (a3.get(i4).intValue() > i2) {
                hashMap2.put(Integer.valueOf(a3.get(i4).intValue() - 1), this.i.get(a3.get(i4)));
            } else if (a3.get(i4).intValue() != i2) {
                hashMap2.put(a3.get(i4), this.i.get(a3.get(i4)));
            } else if (!this.i.get(a3.get(i4)).isRecycled()) {
                if (this.n != null) {
                    this.n.a(a3.get(i4).intValue(), this.i.get(a3.get(i4)));
                }
                this.i.get(a3.get(i4)).recycle();
            }
        }
        this.i = hashMap2;
        this.p.b();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(AbstractC0100a aVar, Bitmap bitmap, int i2) {
        aVar.a(bitmap, i2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void c() {
        int intValue;
        if (this.j.size() == this.k.e) {
            int i2 = this.m;
            ArrayList<Integer> a2 = a(this.j);
            if (i2 - a2.get(0).intValue() > a2.get(a2.size() - 1).intValue() - i2) {
                intValue = a2.get(0).intValue();
            } else {
                intValue = a2.get(a2.size() - 1).intValue();
            }
            for (int i3 = 0; i3 < a2.size(); i3++) {
                b("pKeyList:" + a2.get(i3), 4);
            }
            b("CheckFreeThumbnalImage onFree:" + intValue + " " + i2, 4);
            Bitmap bitmap = this.j.get(Integer.valueOf(intValue));
            if (this.o != null) {
                c cVar = new c(this.o, intValue, bitmap);
                this.e.post(cVar);
                cVar.a();
            }
            this.j.remove(Integer.valueOf(intValue));
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    @SuppressLint({"DefaultLocale"})
    private Bitmap a(String str, int i2, int i3) {
        Bitmap c2;
        FileOutputStream fileOutputStream;
        b("LoadThumbnialFromSD S: pszFilePath " + str + " ulWidth " + i2 + " ulHeight " + i3, 4);
        String lowerCase = str.substring(str.lastIndexOf(".") + 1).toLowerCase();
        String lowerCase2 = str.substring(str.lastIndexOf("/") + 1).toLowerCase();
        String substring = str.substring(0, str.lastIndexOf("/"));
        String str2 = this.k.f1311a + "thm_" + substring.substring(substring.lastIndexOf("/") + 1).toLowerCase() + "_" + lowerCase2;
        File file = new File(str);
        if (!file.exists() && !file.getPath().contains("thm")) {
            b("File not exist: " + str, 3);
            return null;
        } else if (f1294a.a() < 0) {
            b("pFileCreateMutex.Get() fail ", 2);
            return null;
        } else if (new File(str2).exists()) {
            b("runtime file exist", 4);
            this.h.inSampleSize = 1;
            this.h.inJustDecodeBounds = false;
            this.h.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap decodeFile = BitmapFactory.decodeFile(str2, this.h);
            f1294a.b();
            return decodeFile;
        } else {
            f1294a.b();
            if (!lowerCase.toLowerCase().equals("jpg") && !lowerCase.toLowerCase().equals("thm") && !lowerCase.toLowerCase().equals("gif")) {
                return null;
            }
            if (!new File(str).exists()) {
                c2 = a(str, this.k.c / 4, i2, i3);
            } else {
                try {
                    this.b = new ExifInterface(str);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                byte[] thumbnail = this.b.getThumbnail();
                int attributeInt = this.b.getAttributeInt("Orientation", -1);
                if (thumbnail != null) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = this.b.getAttributeInt("ImageWidth", 0) / 768;
                    Bitmap decodeFile2 = BitmapFactory.decodeFile(str, options);
                    if (decodeFile2 == null) {
                        return null;
                    }
                    Matrix matrix = new Matrix();
                    Bitmap a2 = a(decodeFile2, i2, i3);
                    if (attributeInt != 6) {
                        if (attributeInt == 3) {
                            matrix.postRotate(180.0f);
                            Bitmap createBitmap = Bitmap.createBitmap(a2, 0, 0, a2.getWidth(), a2.getHeight(), matrix, true);
                            a2.recycle();
                            a2 = createBitmap;
                        } else if (attributeInt == 8) {
                        }
                    }
                    c2 = a2;
                } else if (str.contains("thm")) {
                    c2 = a(str, this.k.c / 4, i2, i3);
                } else {
                    c2 = c(str, this.k.c / 4, i2, i3);
                }
            }
            if (c2 != null) {
                if (f1294a.a() < 0) {
                    b("pFileCreateMutex.Get() fail ", 2);
                    return null;
                }
                b("Create runtime file:" + str2, 4);
                String b2 = GeneralFunction.g.a.b(str2, "tmp");
                try {
                    fileOutputStream = new FileOutputStream(b2);
                } catch (FileNotFoundException e3) {
                    e3.printStackTrace();
                    fileOutputStream = null;
                }
                c2.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                try {
                    fileOutputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                if (!new File(str).exists()) {
                    String b3 = GeneralFunction.g.a.b(str, "tmp");
                    try {
                        GeneralFunction.g.a.c(b2, b3);
                        new File(b3).renameTo(new File(str));
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                }
                new File(b2).renameTo(new File(str2));
                f1294a.b();
            }
            b("LoadThumbnialFromSD E:" + str, 4);
            return c2;
        }
    }

    private static Bitmap a(Bitmap bitmap, int i2, int i3) {
        int i4;
        if (bitmap == null || i2 == -1 || i3 == -1) {
            return bitmap;
        }
        float width = (float) bitmap.getWidth();
        float height = (float) bitmap.getHeight();
        float f2 = ((float) i2) / width;
        float f3 = ((float) i3) / height;
        Matrix matrix = new Matrix();
        if (f2 < f3) {
            int i5 = (int) (((f3 - f2) * width) / f3);
            if (i5 == 0) {
                return bitmap;
            }
            b("RemoveBlackPorion W>H:" + i5, 4);
            Bitmap createBitmap = Bitmap.createBitmap(bitmap, i5 / 2, 0, ((int) width) - i5, (int) height, matrix, true);
            if (bitmap != createBitmap) {
                bitmap.recycle();
            }
            return createBitmap;
        } else if (f3 >= f2 || (i4 = (int) (((f2 - f3) * height) / f2)) == 0) {
            return bitmap;
        } else {
            b("RemoveBlackPorion H>W:" + i4, 4);
            Bitmap createBitmap2 = Bitmap.createBitmap(bitmap, 0, i4 / 2, (int) width, ((int) height) - i4, matrix, true);
            if (bitmap != createBitmap2) {
                bitmap.recycle();
            }
            return createBitmap2;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void d() {
        int intValue;
        if (this.i.size() == this.k.d) {
            int i2 = this.l;
            ArrayList<Integer> a2 = a(this.i);
            if (i2 - a2.get(0).intValue() > a2.get(a2.size() - 1).intValue() - i2) {
                intValue = a2.get(0).intValue();
            } else {
                intValue = a2.get(a2.size() - 1).intValue();
            }
            for (int i3 = 0; i3 < a2.size(); i3++) {
                b("pKeyList:" + a2.get(i3), 4);
            }
            b("CheckFreeMainImage onFree:" + intValue + " " + i2, 4);
            Bitmap bitmap = this.i.get(Integer.valueOf(intValue));
            if (this.n != null) {
                this.n.a(intValue, bitmap);
            }
            this.i.remove(Integer.valueOf(intValue));
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    @SuppressLint({"DefaultLocale"})
    private Bitmap b(String str, int i2, int i3) {
        b("LoadMainImageFromSD pszFilePath " + str + " ulWidth " + i2 + " ulHeight " + i3, 4);
        try {
            String lowerCase = str.substring(str.lastIndexOf(".") + 1, str.length()).toLowerCase();
            if (!new File(str).exists()) {
                return null;
            }
            if (!lowerCase.equals("jpg") && !lowerCase.equals("thm") && !lowerCase.equals("gif")) {
                return null;
            }
            b("LoadMainImageFromSD jpg thm gif", 4);
            return b(str, this.k.b, i2, i3);
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public static Bitmap a(String str, int i2, int i3, int i4) {
        Bitmap bitmap = null;
        String lowerCase = str.substring(str.lastIndexOf(".") + 1, str.length()).toLowerCase();
        File file = new File(str);
        if (!lowerCase.equals("jpg") && !lowerCase.equals("thm") && !lowerCase.equals("gif")) {
            return null;
        }
        if (file.exists()) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            options.inSampleSize = options.outWidth / 640;
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeFile(str, options);
        }
        return a(bitmap, i3, i4);
    }

    public static Bitmap b(String str, int i2, int i3, int i4) {
        ExifInterface exifInterface;
        Bitmap bitmap = null;
        String lowerCase = str.substring(str.lastIndexOf(".") + 1, str.length()).toLowerCase();
        if (new File(str).exists() && (lowerCase.equals("jpg") || lowerCase.equals("thm") || lowerCase.equals("gif"))) {
            try {
                exifInterface = new ExifInterface(str);
            } catch (IOException e2) {
                e2.printStackTrace();
                exifInterface = null;
            }
            int attributeInt = exifInterface.getAttributeInt("Orientation", -1);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            options.inSampleSize = a(options.outWidth, options.outHeight, i2);
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
            new Matrix();
            bitmap = a(decodeFile, i3, i4);
            if (attributeInt == 6) {
                b("ORIENTATION_ROTATE_90", 3);
            } else if (attributeInt == 3) {
                b("ORIENTATION_ROTATE_180", 3);
            } else if (attributeInt == 8) {
                b("ORIENTATION_ROTATE_2700", 3);
            }
        }
        return bitmap;
    }

    public static Bitmap c(String str, int i2, int i3, int i4) {
        Bitmap decodeFile;
        String lowerCase = str.substring(str.lastIndexOf(".") + 1, str.length()).toLowerCase();
        if (!new File(str).exists()) {
            return null;
        }
        if (!lowerCase.equals("jpg") && !lowerCase.equals("gif")) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        options.inSampleSize = a(options.outWidth, options.outHeight, i2) / 2;
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        if (options.outWidth / options.outHeight >= 4) {
            options.inSampleSize = b(options.outWidth, options.outHeight, i2);
            decodeFile = BitmapFactory.decodeFile(str, options);
        } else {
            decodeFile = BitmapFactory.decodeFile(str, options);
        }
        return a(decodeFile, i3, i4);
    }

    public static int a(int i2, int i3, int i4) {
        int i5 = 1;
        int ceil = i4 == -1 ? 1 : (int) Math.ceil(Math.sqrt((((double) i2) * ((double) i3)) / ((double) i4)));
        if (128 >= ceil && i4 == -1) {
            ceil = 1;
        }
        while (i2 / ceil > 4096) {
            ceil++;
        }
        if (ceil > 8) {
            return ((ceil + 7) / 8) * 8;
        }
        while (i5 < ceil) {
            i5 <<= 1;
        }
        return i5;
    }

    public static int b(int i2, int i3, int i4) {
        int ceil = (int) Math.ceil(Math.sqrt((double) i4));
        int max = Math.max(i2 / ceil, i3 / ceil);
        b("AAA " + max, 3);
        return Math.max(1, max);
    }

    private ArrayList<Integer> a(Map map) {
        Set<Map.Entry> entrySet = map.entrySet();
        ArrayList<Integer> arrayList = new ArrayList<>();
        if (entrySet != null) {
            for (Map.Entry entry : entrySet) {
                arrayList.add((Integer) entry.getKey());
            }
        }
        Collections.sort(arrayList, new Comparator<Integer>() {
            /* class ui_Controller.CustomWidget.a.AnonymousClass3 */

            /* renamed from: a */
            public int compare(Integer num, Integer num2) {
                return num.compareTo(num2);
            }
        });
        return arrayList;
    }
}
