package ui_Controller.CustomWidget.ListHorizontalScrollView;

import GeneralFunction.j;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import ui_Controller.CustomWidget.a;

public class ListHorizontalScrollView extends HorizontalScrollView implements View.OnClickListener {
    private final j A = new j();
    private int B = 0;
    private int C = 0;
    private Handler D = new Handler(Looper.getMainLooper()) {
        /* class ui_Controller.CustomWidget.ListHorizontalScrollView.ListHorizontalScrollView.AnonymousClass1 */

        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 1:
                    ListHorizontalScrollView.this.scrollTo(ListHorizontalScrollView.this.o, 0);
                    ListHorizontalScrollView.this.setVisibility(0);
                    ListHorizontalScrollView.this.a((ListHorizontalScrollView) false);
                    ListHorizontalScrollView.this.j = true;
                    return;
                case 2:
                    if (ListHorizontalScrollView.this.j && ListHorizontalScrollView.this.x == 2) {
                        if (System.currentTimeMillis() - ListHorizontalScrollView.this.p > 100) {
                            int b = ListHorizontalScrollView.this.b((ListHorizontalScrollView) ListHorizontalScrollView.this.getScrollX());
                            ListHorizontalScrollView.this.smoothScrollTo(ListHorizontalScrollView.this.c((ListHorizontalScrollView) b), 0);
                            ListHorizontalScrollView.this.a((ListHorizontalScrollView) b, true);
                            ListHorizontalScrollView.this.s = false;
                            ListHorizontalScrollView.this.x = 0;
                            return;
                        }
                        ListHorizontalScrollView.this.a((ListHorizontalScrollView) 2, 50);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    @SuppressLint({"ClickableViewAccessibility"})
    private View.OnTouchListener E = new View.OnTouchListener() {
        /* class ui_Controller.CustomWidget.ListHorizontalScrollView.ListHorizontalScrollView.AnonymousClass4 */

        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (!ListHorizontalScrollView.this.c()) {
                if (!ListHorizontalScrollView.this.a()) {
                    if (ListHorizontalScrollView.this.x == 0) {
                        switch (motionEvent.getAction() & 255) {
                            case 0:
                                ListHorizontalScrollView.this.a((ListHorizontalScrollView) motionEvent);
                                ListHorizontalScrollView.this.v.set(motionEvent.getX(), motionEvent.getY());
                                ListHorizontalScrollView.this.w = 1;
                                break;
                            case 1:
                            case 6:
                                ListHorizontalScrollView.this.a((ListHorizontalScrollView) motionEvent);
                                if (ListHorizontalScrollView.this.w != 2 && ListHorizontalScrollView.this.c((ListHorizontalScrollView) motionEvent)) {
                                    ListHorizontalScrollView.this.b((ListHorizontalScrollView) view);
                                }
                                ListHorizontalScrollView.this.w = 0;
                                break;
                            case 2:
                                ListHorizontalScrollView.this.a((ListHorizontalScrollView) motionEvent);
                                if (ListHorizontalScrollView.this.w == 1 && ListHorizontalScrollView.this.b((ListHorizontalScrollView) motionEvent)) {
                                    ListHorizontalScrollView.this.x = 1;
                                    ListHorizontalScrollView.this.w = 0;
                                    break;
                                }
                        }
                    }
                } else {
                    ListHorizontalScrollView.this.w = 0;
                }
            }
            return true;
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private int f1270a = 0;
    private Context b = null;
    private a c;
    private c d;
    private d e;
    private b f;
    private LinearLayout g = null;
    private ImageView h = null;
    private e i = null;
    private boolean j = false;
    private int k;
    private int l;
    private int m = 0;
    private int n = 0;
    private int o = 0;
    private long p = -1;
    private int q = -1;
    private boolean r = false;
    private boolean s = false;
    private PointF t = null;
    private PointF u = null;
    private PointF v = new PointF();
    private int w = 0;
    private int x = 0;
    private Map<Integer, LinearLayout> y = new HashMap();
    private Map<Integer, View> z = new HashMap();

    public interface a {
        void a(int i, View view);
    }

    public interface b {
        boolean a();
    }

    public interface c {
        void a(int i);
    }

    public interface d {
        void a(int i, MotionEvent motionEvent);
    }

    public static class e {

        /* renamed from: a  reason: collision with root package name */
        public ui_Controller.CustomWidget.ListHorizontalScrollView.a.a f1275a;
        public ui_Controller.CustomWidget.a b;
        public int c;
        public int d;
        public int e;
    }

    private void a(String str, int i2) {
        if (i2 <= this.f1270a) {
            GeneralFunction.d.a("ListHorizontalScroll...", str, i2);
        }
    }

    public ListHorizontalScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        this.l = displayMetrics.widthPixels;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        this.g = (LinearLayout) getChildAt(0);
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, int i3) {
        Message message = new Message();
        message.what = i2;
        this.D.sendMessageDelayed(message, (long) i3);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, boolean z2) {
        a("UpdateViewByIndex:" + i2, 4);
        if (this.k != i2) {
            if (this.k == -1) {
                for (int i3 = i2 - this.i.c; i3 <= this.i.c + i2; i3++) {
                    if (a(i3)) {
                        d(i3);
                    }
                }
            } else {
                b(this.k, false);
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                for (int i4 = this.k - this.i.c; i4 <= this.k + this.i.c; i4++) {
                    if (a(i4)) {
                        arrayList.add(Integer.valueOf(i4));
                    }
                }
                for (int i5 = i2 - this.i.c; i5 <= this.i.c + i2; i5++) {
                    if (a(i5)) {
                        arrayList2.add(Integer.valueOf(i5));
                    }
                }
                for (int i6 = 0; i6 < arrayList.size(); i6++) {
                    int intValue = ((Integer) arrayList.get(i6)).intValue();
                    if (!arrayList2.contains(Integer.valueOf(intValue))) {
                        e(intValue);
                    }
                }
                for (int i7 = 0; i7 < arrayList2.size(); i7++) {
                    int intValue2 = ((Integer) arrayList2.get(i7)).intValue();
                    if (!arrayList.contains(Integer.valueOf(intValue2))) {
                        d(intValue2);
                    }
                }
            }
            this.k = i2;
            b(this.k, true);
            if (z2) {
                b();
            }
        }
    }

    private boolean a(int i2) {
        if (i2 < 0 || i2 >= this.i.f1275a.getCount()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int b(int i2) {
        return (int) ((((float) i2) / ((float) this.i.d)) + 0.5f);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int c(int i2) {
        return this.i.d * i2;
    }

    private View d(int i2) {
        this.A.a();
        a("AddView:" + i2, 4);
        ui_Controller.CustomWidget.ListHorizontalScrollView.a.c f2 = f(i2);
        View view = this.i.f1275a.getView(i2, null, this.g);
        view.setOnClickListener(this);
        view.setOnTouchListener(this.E);
        this.y.get(Integer.valueOf(i2)).addView(view);
        this.z.put(Integer.valueOf(i2), view);
        if (f2.e()) {
            i(i2);
        } else if (f(i2).a() != null) {
            this.i.b.b(f(i2).a(), i2, this.k, this.i.f1275a.getItem(i2).b(), this.i.f1275a.getItem(i2).c(), new a.AbstractC0100a() {
                /* class ui_Controller.CustomWidget.ListHorizontalScrollView.ListHorizontalScrollView.AnonymousClass2 */

                @Override // ui_Controller.CustomWidget.a.AbstractC0100a
                public void a(Bitmap bitmap, int i) {
                    ListHorizontalScrollView.this.a((ListHorizontalScrollView) bitmap, (Bitmap) i);
                }
            });
        } else {
            j(i2);
        }
        this.A.b();
        return view;
    }

    private void e(int i2) {
        this.A.a();
        a("DeleteView:" + i2, 4);
        if (f(i2).d() != null) {
            f(i2).a((Bitmap) null);
        }
        ui_Controller.CustomWidget.ListHorizontalScrollView.a.b h2 = h(i2);
        h2.a().setImageResource(17170445);
        h2.a().setImageBitmap(null);
        this.z.remove(Integer.valueOf(i2));
        this.y.get(Integer.valueOf(i2)).removeAllViews();
        this.A.b();
    }

    private ui_Controller.CustomWidget.ListHorizontalScrollView.a.c f(int i2) {
        return this.i.f1275a.a(i2);
    }

    private ArrayList<Integer> a(Map map) {
        Set<Map.Entry> entrySet = map.entrySet();
        ArrayList<Integer> arrayList = new ArrayList<>();
        if (entrySet != null) {
            for (Map.Entry entry : entrySet) {
                arrayList.add((Integer) entry.getKey());
            }
        }
        Collections.sort(arrayList, new Comparator<Integer>() {
            /* class ui_Controller.CustomWidget.ListHorizontalScrollView.ListHorizontalScrollView.AnonymousClass3 */

            /* renamed from: a */
            public int compare(Integer num, Integer num2) {
                return num.compareTo(num2);
            }
        });
        return arrayList;
    }

    private View g(int i2) {
        View childAt = this.y.get(Integer.valueOf(i2)).getChildAt(0);
        if (childAt == null) {
            a("GetView:" + i2, 4);
        }
        return childAt;
    }

    private int a(View view) {
        ArrayList<Integer> a2 = a(this.z);
        for (int i2 = 0; i2 < this.z.size(); i2++) {
            if (view.equals(this.z.get(a2.get(i2)))) {
                return i2;
            }
        }
        return -1;
    }

    private ui_Controller.CustomWidget.ListHorizontalScrollView.a.b h(int i2) {
        View g2 = g(i2);
        if (g2 != null) {
            return (ui_Controller.CustomWidget.ListHorizontalScrollView.a.b) g2.getTag();
        }
        return null;
    }

    private void b(int i2, boolean z2) {
        ui_Controller.CustomWidget.ListHorizontalScrollView.a.b h2 = h(i2);
        if (h2 != null && h2.b() != null) {
            if (z2) {
                h2.b().setImageResource(this.B);
            } else {
                h2.b().setImageResource(17170445);
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean a() {
        return this.r;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(boolean z2) {
        a("SetKeyBlock:" + z2, 2);
        this.r = z2;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if ((motionEvent.getAction() & 255) == 2 && this.x == 0) {
            return false;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (c() || a()) {
            return true;
        }
        if (this.x != 1 && this.x != 2) {
            return true;
        }
        switch (motionEvent.getAction() & 255) {
            case 1:
                this.u = null;
                this.t = null;
                this.x = 2;
                a(2, 50);
                a(motionEvent);
                break;
            case 2:
                this.s = true;
                this.x = 1;
                if (this.u == null) {
                    this.u = new PointF();
                    this.t = new PointF();
                    this.t.set(motionEvent.getX(), motionEvent.getY());
                }
                this.u.set(motionEvent.getX(), motionEvent.getY());
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i2, int i3, int i4, int i5) {
        super.onScrollChanged(i2, i3, i4, i5);
        if (this.p != -1) {
            System.currentTimeMillis();
            this.p = System.currentTimeMillis();
            this.q = i2;
        }
        this.p = System.currentTimeMillis();
        this.q = i2;
        a(b(i2), this.s);
    }

    public void onClick(View view) {
        a("onClick", 4);
    }

    private void b() {
        if (this.c != null) {
            this.c.a(this.k, this.g.getChildAt(0));
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void b(View view) {
        if (this.d != null) {
            this.d.a(a(view));
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(MotionEvent motionEvent) {
        if (this.e != null) {
            this.e.a(this.k, motionEvent);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean c() {
        if (this.f != null) {
            return this.f.a();
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean b(MotionEvent motionEvent) {
        if (Math.abs(this.v.x - motionEvent.getX()) > 8.0f) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean c(MotionEvent motionEvent) {
        if (Math.abs(motionEvent.getX() - (((float) this.i.d) / 2.0f)) >= ((float) this.i.d) / 2.0f || Math.abs(motionEvent.getY() - (((float) this.i.e) / 2.0f)) >= ((float) this.i.d) / 2.0f) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(Bitmap bitmap, int i2) {
        this.A.a();
        a("ThumbnialLoaded:" + i2 + " " + bitmap, 4);
        if (bitmap == null) {
            a("thumbnialLoaded no thumbnial:" + i2, 4);
            if (f(i2) != null) {
                f(i2).a(true);
            }
        }
        if (!this.z.containsKey(Integer.valueOf(i2))) {
            a("thumbnialLoaded view is delete , so ignore!", 4);
        } else if (f(i2).e()) {
            i(i2);
        } else {
            f(i2).a(bitmap);
            b(bitmap, i2);
        }
        this.A.b();
    }

    private void b(Bitmap bitmap, int i2) {
        h(i2).a().setImageBitmap(bitmap);
        if (this.h != null && i2 == this.k) {
            this.h.setImageBitmap(bitmap);
        }
    }

    private void i(int i2) {
        h(i2).a().setImageResource(this.C);
    }

    private void j(int i2) {
        ui_Controller.CustomWidget.ListHorizontalScrollView.a.b h2 = h(i2);
        h2.a().setImageResource(17170445);
        h2.a().setImageBitmap(null);
    }
}
