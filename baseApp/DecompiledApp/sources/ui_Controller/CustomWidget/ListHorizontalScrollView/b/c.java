package ui_Controller.CustomWidget.ListHorizontalScrollView.b;

import GeneralFunction.e.d;
import GeneralFunction.g.a;
import android.graphics.Bitmap;

public class c extends ui_Controller.CustomWidget.ListHorizontalScrollView.a.c {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f1278a = null;
    private boolean b = false;
    private d c = null;
    private int d = -1;
    private int e = -1;

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.c
    public String a() {
        String e2 = this.c.e(this.d, this.e);
        int a2 = this.c.a(this.d, this.e);
        if (e2 == null) {
            return null;
        }
        if (a2 == 1) {
            return a.d(e2);
        }
        return e2;
    }

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.c
    public int b() {
        return this.c.b(this.d, this.e);
    }

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.c
    public int c() {
        return this.c.c(this.d, this.e);
    }

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.c
    public void a(Bitmap bitmap) {
        this.f1278a = bitmap;
    }

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.c
    public Bitmap d() {
        return this.f1278a;
    }

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.c
    public void a(boolean z) {
        this.b = z;
    }

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.c
    public boolean e() {
        return this.b;
    }
}
