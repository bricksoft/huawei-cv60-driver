package ui_Controller.CustomWidget.ListHorizontalScrollView.b;

import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.huawei.cvIntl60.R;
import java.util.ArrayList;
import ui_Controller.CustomWidget.ListHorizontalScrollView.a.c;

public class a extends ui_Controller.CustomWidget.ListHorizontalScrollView.a.a {

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<c> f1276a;
    private LayoutInflater b;
    private int c;
    private int d;

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.a
    public c a(int i) {
        return this.f1276a.get(i);
    }

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.a
    public int getCount() {
        return this.f1276a.size();
    }

    /* renamed from: c */
    public c getItem(int i) {
        return this.f1276a.get(i);
    }

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.a
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.a.a
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            b bVar = new b();
            View inflate = this.b.inflate(R.layout.style_listview_item, viewGroup, false);
            bVar.f1277a = (ImageView) inflate.findViewById(R.id.IV_listViewImage);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(bVar.f1277a.getLayoutParams());
            layoutParams.width = this.c - 2;
            layoutParams.height = this.d - 2;
            bVar.f1277a.setLayoutParams(layoutParams);
            bVar.f1277a.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            bVar.f1277a.setImageBitmap(null);
            bVar.f1277a.setScaleType(ImageView.ScaleType.CENTER_CROP);
            bVar.b = (ImageView) inflate.findViewById(R.id.IV_FocusIcon);
            inflate.setTag(bVar);
            return inflate;
        }
        b bVar2 = (b) view.getTag();
        return view;
    }
}
