package ui_Controller.CustomWidget.CustomRecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.huawei.cvIntl60.R;

public class CustomRecyclerView extends RecyclerView {
    public a M;
    private Bitmap N;
    private Context O;

    public CustomRecyclerView(Context context) {
        this(context, null);
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setOverScrollMode(2);
        this.O = context;
        this.M = new a();
    }

    public void setBitmap(Bitmap bitmap) {
        this.N = bitmap;
    }

    public void y() {
        if (this.N != null) {
            this.N.recycle();
            this.N = null;
        }
    }

    @Override // android.support.v7.widget.RecyclerView
    public a getAdapter() {
        return this.M;
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.widget.RecyclerView
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    public class a extends RecyclerView.a<b> {
        a() {
        }

        /* renamed from: a */
        public b b(ViewGroup viewGroup, int i) {
            return new b(LayoutInflater.from(CustomRecyclerView.this.O).inflate(R.layout.recycler_item, viewGroup, false));
        }

        public void a(b bVar, int i) {
            if (i <= 1 || i >= a() - 2) {
                CustomRecyclerView.this.a(1073741823);
            }
            bVar.q.setImageBitmap(CustomRecyclerView.this.N);
        }

        @Override // android.support.v7.widget.RecyclerView.a
        public int a() {
            return Integer.MAX_VALUE;
        }
    }

    /* access modifiers changed from: package-private */
    public class b extends RecyclerView.w {
        ImageView q;

        public b(View view) {
            super(view);
            this.q = (ImageView) view.findViewById(R.id.item_img);
        }
    }
}
