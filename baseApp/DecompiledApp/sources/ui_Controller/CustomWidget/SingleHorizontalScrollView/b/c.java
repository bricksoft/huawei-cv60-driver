package ui_Controller.CustomWidget.SingleHorizontalScrollView.b;

import GeneralFunction.e.a;
import android.graphics.Bitmap;
import ui_Controller.CustomWidget.SingleHorizontalScrollView.a.d;

public class c extends ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c {

    /* renamed from: a  reason: collision with root package name */
    public d f1289a;
    private Bitmap b = null;
    private Bitmap c = null;
    private boolean d = false;
    private boolean e = false;
    private GeneralFunction.e.d f = null;
    private int g = -1;
    private int h = -1;

    public void a(GeneralFunction.e.d dVar, a aVar) {
        this.b = null;
        this.c = null;
        this.f1289a = new d();
        this.d = false;
        this.f = dVar;
        this.g = aVar.b;
        this.h = aVar.c.get(0).intValue();
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public String a() {
        String e2 = this.f.e(this.g, this.h);
        int a2 = this.f.a(this.g, this.h);
        if (e2 == null) {
            return null;
        }
        if (a2 == 1) {
            return GeneralFunction.g.a.d(e2);
        }
        return e2;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public String b() {
        String f2 = this.f.f(this.g, this.h);
        int a2 = this.f.a(this.g, this.h);
        if (f2 == null || a2 != 1) {
            return f2;
        }
        return GeneralFunction.g.a.d(f2);
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public int c() {
        return this.f.b(this.g, this.h);
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public int d() {
        return this.f.c(this.g, this.h);
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public void a(Bitmap bitmap) {
        this.b = bitmap;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public Bitmap e() {
        return this.b;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public void b(Bitmap bitmap) {
        this.c = bitmap;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public Bitmap f() {
        return this.c;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public void a(boolean z) {
        this.d = z;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public boolean g() {
        return this.d;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public boolean h() {
        if (this.c != null) {
            return true;
        }
        return false;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public boolean i() {
        switch (this.f.a(this.g, this.h)) {
            case 0:
            case 2:
            case 3:
            case 4:
                return true;
            case 1:
            default:
                return false;
        }
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public boolean j() {
        if (this.f.a(this.g, this.h) == 6) {
            return true;
        }
        return false;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public boolean k() {
        return this.e;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public void b(boolean z) {
        this.e = z;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public int l() {
        int a2 = this.f.a(this.g, this.h);
        switch (a2) {
            case 2:
            case 3:
                if (this.f.q(this.f.u(this.g)) == 1) {
                    return 0;
                }
                return a2;
            default:
                return a2;
        }
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public boolean m() {
        switch (l()) {
            case 1:
            case 2:
            case 3:
            case 5:
                return false;
            case 4:
            default:
                return true;
        }
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c
    public d n() {
        return this.f1289a;
    }
}
