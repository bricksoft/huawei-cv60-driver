package ui_Controller.CustomWidget.SingleHorizontalScrollView.b;

import GeneralFunction.e.d;
import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.huawei.cvIntl60.R;
import java.util.ArrayList;
import ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c;

public class a extends ui_Controller.CustomWidget.SingleHorizontalScrollView.a.a {

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<c> f1287a;
    private Context b;
    private LayoutInflater c;
    private int d = 0;
    private int e = 0;
    private ImageView f;
    private boolean g;

    public a(Context context, d dVar, int i, int i2, boolean z) {
        this.b = context;
        this.c = LayoutInflater.from(context);
        this.d = i;
        this.e = i2;
        this.f1287a = new ArrayList<>();
        this.g = z;
        for (int i3 = 0; i3 < dVar.e(); i3++) {
            c cVar = new c();
            cVar.a(dVar, dVar.r(i3));
            this.f1287a.add(cVar);
        }
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.a
    public void a(int i) {
        this.f1287a.remove(i);
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.a
    public c b(int i) {
        return this.f1287a.get(i);
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.a
    public int getCount() {
        return this.f1287a.size();
    }

    /* renamed from: d */
    public c getItem(int i) {
        return this.f1287a.get(i);
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.a
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.a.a
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            b bVar = new b();
            view = this.c.inflate(R.layout.style_singleview_item, viewGroup, false);
            bVar.f1288a = (ImageView) view.findViewById(R.id.IV_SingleViewImage);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(bVar.f1288a.getLayoutParams());
            layoutParams.width = this.d + 0;
            layoutParams.height = this.e + 0;
            bVar.f1288a.setLayoutParams(layoutParams);
            bVar.f1288a.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            bVar.f1288a.setImageBitmap(null);
            bVar.f1288a.setScaleType(ImageView.ScaleType.CENTER_CROP);
            bVar.c = (ImageView) view.findViewById(R.id.IV_GIFViewImage);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(bVar.c.getLayoutParams());
            layoutParams2.width = this.d + 0;
            layoutParams2.height = this.e + 0;
            bVar.c.setLayoutParams(layoutParams2);
            bVar.c.setBackgroundColor(0);
            bVar.c.setImageBitmap(null);
            bVar.c.setScaleType(ImageView.ScaleType.CENTER_CROP);
            bVar.b = (ImageView) view.findViewById(R.id.IV_SingleViewTypeIcon);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(bVar.b.getLayoutParams());
            layoutParams3.width = (this.d + 0) / 9;
            layoutParams3.height = layoutParams3.width;
            layoutParams3.addRule(13, -1);
            bVar.b.setLayoutParams(layoutParams3);
            bVar.b.setImageBitmap(null);
            bVar.b.setScaleType(ImageView.ScaleType.FIT_CENTER);
            this.f = (ImageView) view.findViewById(R.id.IV_SingleViewVideoIcon);
            this.f.setImageResource(R.drawable.gallery_singleview_filetype_video);
            this.f.setVisibility(4);
            this.f1287a.get(i).l();
            bVar.b.setImageBitmap(null);
            switch (this.f1287a.get(i).l()) {
                case 0:
                case 4:
                case 6:
                    bVar.b.setImageResource(0);
                    this.f.setVisibility(4);
                    break;
                case 1:
                case 5:
                    bVar.b.setImageResource(R.drawable.gallery_singleview_play_video);
                    if (!this.g) {
                        this.f.setVisibility(4);
                        break;
                    } else {
                        this.f.setVisibility(0);
                        break;
                    }
                case 2:
                    bVar.b.setImageResource(R.drawable.gallery_singleview_play_timelapse);
                    this.f.setVisibility(4);
                    break;
                case 3:
                    bVar.b.setImageResource(R.drawable.gallery_singleview_play_burst);
                    this.f.setVisibility(4);
                    break;
            }
            view.setTag(bVar);
        } else {
            b bVar2 = (b) view.getTag();
        }
        return view;
    }
}
