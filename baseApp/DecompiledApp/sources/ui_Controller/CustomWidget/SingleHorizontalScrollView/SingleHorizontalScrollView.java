package ui_Controller.CustomWidget.SingleHorizontalScrollView;

import GeneralFunction.j;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.huawei.cvIntl60.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import ui_Controller.CustomWidget.a;
import ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController;

public class SingleHorizontalScrollView extends HorizontalScrollView implements View.OnClickListener {
    private PointF A = null;
    private PointF B = null;
    private int C = 0;
    private int D = 0;
    private int E = 0;
    private Map<Integer, LinearLayout> F = new HashMap();
    private Map<Integer, View> G = new HashMap();
    private final j H = new j();
    private int I = 0;
    private boolean J = false;
    private int K = 0;
    private boolean L = false;
    private boolean M = false;
    private UI_PhoneGalleryController N = null;
    private Handler O = new Handler(Looper.getMainLooper()) {
        /* class ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.AnonymousClass1 */

        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 1:
                    SingleHorizontalScrollView.this.scrollTo(SingleHorizontalScrollView.this.o, 0);
                    SingleHorizontalScrollView.this.setVisibility(0);
                    SingleHorizontalScrollView.this.a((SingleHorizontalScrollView) 2, 0);
                    SingleHorizontalScrollView.this.a((SingleHorizontalScrollView) false);
                    SingleHorizontalScrollView.this.j = true;
                    return;
                case 2:
                    SingleHorizontalScrollView.this.O.removeMessages(2);
                    if (SingleHorizontalScrollView.this.j) {
                        if (System.currentTimeMillis() - SingleHorizontalScrollView.this.p > 50) {
                            SingleHorizontalScrollView.this.h((SingleHorizontalScrollView) SingleHorizontalScrollView.this.k);
                        }
                        SingleHorizontalScrollView.this.a((SingleHorizontalScrollView) 2, 50);
                        return;
                    }
                    return;
                case 3:
                    if (SingleHorizontalScrollView.this.D == 1) {
                        SingleHorizontalScrollView.this.D = 0;
                        SingleHorizontalScrollView.this.a((SingleHorizontalScrollView) 1, (int) ((PointF) message.obj));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private View.OnTouchListener P = new View.OnTouchListener() {
        /* class ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.AnonymousClass5 */

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x024a, code lost:
            if ((r10.f1284a.k - 1) >= 0) goto L_0x024c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x0276, code lost:
            if (r10.f1284a.i((ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView) (r10.f1284a.k + 1)) == false) goto L_0x000c;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTouch(android.view.View r11, android.view.MotionEvent r12) {
            /*
            // Method dump skipped, instructions count: 732
            */
            throw new UnsupportedOperationException("Method not decompiled: ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.AnonymousClass5.onTouch(android.view.View, android.view.MotionEvent):boolean");
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public float f1279a = 4.0f;
    private Context b = null;
    private a c;
    private b d;
    private d e;
    private e f;
    private c g;
    private LinearLayout h = null;
    private f i = null;
    private boolean j = false;
    private int k;
    private int l;
    private int m = 0;
    private int n = 0;
    private int o = 0;
    private long p = -1;
    private int q = -1;
    private int r = -1;
    private int s = -1;
    private boolean t = false;
    private float u = 0.02f;
    private Matrix v = new Matrix();
    private Matrix w = new Matrix();
    private PointF x = new PointF();
    private PointF y = new PointF();
    private float z;

    public interface a {
        void a(int i, View view);
    }

    public interface b {
        void a(int i, Bitmap bitmap);
    }

    public interface c {
        boolean a();
    }

    public interface d {
        void a(int i, PointF pointF);
    }

    public interface e {
        void a(int i, MotionEvent motionEvent);
    }

    public static class f {

        /* renamed from: a  reason: collision with root package name */
        public ui_Controller.CustomWidget.SingleHorizontalScrollView.a.a f1285a = null;
        public ui_Controller.CustomWidget.a b = null;
        public int c = 0;
        public int d = 0;
        public int e = 0;
    }

    private void a(String str, int i2) {
        GeneralFunction.d.a("SingleHorizontalScrollView", str, i2);
    }

    public SingleHorizontalScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        this.l = displayMetrics.widthPixels;
    }

    public void a(f fVar, int i2) {
        this.i = fVar;
        this.k = -1;
        this.m = 0;
        this.n = j(this.i.f1285a.getCount() - 1);
        this.I = R.drawable.file_error_640_480;
        a(true);
        setVisibility(4);
        g(i2);
    }

    public void a() {
        this.j = false;
        this.p = -1;
        this.q = -1;
        this.s = -1;
        this.t = false;
        this.C = 0;
        this.E = 0;
        g();
    }

    public void b() {
        this.K = 1;
        this.L = true;
    }

    public void c() {
        this.K = 0;
        this.M = true;
    }

    public void setSingleHorizontalScrollViewCallback(UI_PhoneGalleryController uI_PhoneGalleryController) {
        this.N = uI_PhoneGalleryController;
    }

    public void a(int i2) {
        a("TouchScrollTo " + i2 + " " + this.k, 4);
        if (i2 != this.k) {
            a(i2, true);
            smoothScrollTo(j(i2), 0);
        }
    }

    public void b(int i2) {
        this.p = System.currentTimeMillis();
        if (i2 != this.k) {
            a("Update 0", 1);
            a(i2, false);
        }
        scrollTo(j(i2), 0);
    }

    public void a(d dVar) {
        this.e = dVar;
    }

    public void a(a aVar) {
        this.c = aVar;
    }

    public void a(b bVar) {
        this.d = bVar;
    }

    public void a(e eVar) {
        this.f = eVar;
    }

    public void c(int i2) {
        if (i(i2)) {
            this.H.a();
            this.i.f1285a.a(i2);
            ui_Controller.CustomWidget.SingleHorizontalScrollView.a.b n2 = n(i2);
            n2.a().setImageResource(17170445);
            n2.a().setImageBitmap(null);
            this.G.remove(Integer.valueOf(i2));
            this.F.get(Integer.valueOf(i2)).removeAllViews();
            this.h.removeView(this.F.get(Integer.valueOf(i2)));
            this.F.remove(Integer.valueOf(i2));
            ArrayList<Integer> a2 = a(this.G);
            HashMap hashMap = new HashMap();
            for (int i3 = 0; i3 < a2.size(); i3++) {
                if (a2.get(i3).intValue() > i2) {
                    hashMap.put(Integer.valueOf(a2.get(i3).intValue() - 1), this.G.get(a2.get(i3)));
                } else {
                    hashMap.put(a2.get(i3), this.G.get(a2.get(i3)));
                }
            }
            this.G = hashMap;
            ArrayList<Integer> a3 = a(this.F);
            HashMap hashMap2 = new HashMap();
            for (int i4 = 0; i4 < a3.size(); i4++) {
                if (a3.get(i4).intValue() > i2) {
                    hashMap2.put(Integer.valueOf(a3.get(i4).intValue() - 1), this.F.get(a3.get(i4)));
                } else {
                    hashMap2.put(a3.get(i4), this.F.get(a3.get(i4)));
                }
            }
            this.F = hashMap2;
            if (!i(i2)) {
                this.k--;
            }
            this.H.b();
        }
    }

    public void d() {
        for (int i2 = this.k - this.i.c; i2 <= this.k + this.i.c; i2++) {
            if (i(i2) && m(i2) == null) {
                k(i2);
            }
        }
        this.s = -1;
    }

    private void g() {
        if (this.h != null) {
            a(true);
            this.H.a();
            ArrayList<Integer> a2 = a(this.G);
            this.H.b();
            a("ReleaseViews S:" + a2.size(), 2);
            for (int i2 = 0; i2 < a2.size(); i2++) {
                l(a2.get(i2).intValue());
            }
            a("ReleaseViews E", 2);
        }
    }

    public void setEnableScrolling(boolean z2) {
        this.J = z2;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        this.h = (LinearLayout) getChildAt(0);
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, int i3) {
        a(i2, (Object) null, i3);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, Object obj, int i3) {
        Message message = new Message();
        message.what = i2;
        message.obj = obj;
        this.O.sendMessageDelayed(message, (long) i3);
    }

    private void g(int i2) {
        this.h = (LinearLayout) getChildAt(0);
        this.h.removeAllViews();
        this.G.clear();
        this.F.clear();
        for (int i3 = 0; i3 < this.i.f1285a.getCount(); i3++) {
            b(i3, false);
        }
        a(i2, false);
        this.o = j(i2);
        a(1, 100);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void h(int i2) {
        int i3;
        if (this.s != i2) {
            a("UpdateMainImageByIndex:" + i2, 4);
            this.s = i2;
            int i4 = 0;
            while (i4 < (this.i.c * 2) + 1) {
                a("UpdateMainImageByIndex " + i4 + " " + i(i2) + " " + this.G.containsKey(Integer.valueOf(i2)), 4);
                if (i4 % 2 == 0) {
                    i3 = i2 - i4;
                } else {
                    i3 = i2 + i4;
                }
                if (i(i3) && this.G.containsKey(Integer.valueOf(i3))) {
                    if (e(i3).f() != null || e(i3).g()) {
                        if (f(i3)) {
                            if (i3 == this.k) {
                                p(i3);
                            } else {
                                o(i3);
                            }
                        }
                        a(i3, e(i3).f());
                    } else {
                        a("UpdateMainImageByIndex: " + e(i3).b(), 4);
                        if (e(i3).b() != null) {
                            int c2 = e(i3).c();
                            int d2 = e(i3).d();
                            a("UpdateMainImageByIndex: " + i3 + " lImageWidth " + c2 + " lImageHeight " + d2, 4);
                            this.i.b.a(e(i3).b(), i3, this.k, c2, d2, new a.AbstractC0100a() {
                                /* class ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.AnonymousClass2 */

                                @Override // ui_Controller.CustomWidget.a.AbstractC0100a
                                public void a(Bitmap bitmap, int i) {
                                    SingleHorizontalScrollView.this.a((SingleHorizontalScrollView) bitmap, (Bitmap) i);
                                }
                            });
                        }
                    }
                    if (this.L || this.M) {
                        if (f(i3) && e(i3).f() != null) {
                            if (this.L) {
                                o(i3);
                            }
                            if (this.M) {
                                p(i3);
                            }
                        }
                        this.L = false;
                        this.M = false;
                    }
                }
                i4++;
                i2 = i3;
            }
        }
    }

    private void a(int i2, boolean z2) {
        a("UpdateViewByIndex:" + i2, 4);
        if (this.k != i2) {
            if (this.k == -1) {
                for (int i3 = i2 - this.i.c; i3 <= this.i.c + i2; i3++) {
                    if (i(i3)) {
                        k(i3);
                    }
                }
            } else {
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                for (int i4 = this.k - this.i.c; i4 <= this.k + this.i.c; i4++) {
                    if (i(i4)) {
                        arrayList.add(Integer.valueOf(i4));
                    }
                }
                for (int i5 = i2 - this.i.c; i5 <= this.i.c + i2; i5++) {
                    if (i(i5)) {
                        arrayList2.add(Integer.valueOf(i5));
                    }
                }
                for (int i6 = 0; i6 < arrayList.size(); i6++) {
                    int intValue = ((Integer) arrayList.get(i6)).intValue();
                    if (!arrayList2.contains(Integer.valueOf(intValue))) {
                        l(intValue);
                    }
                }
                for (int i7 = 0; i7 < arrayList2.size(); i7++) {
                    int intValue2 = ((Integer) arrayList2.get(i7)).intValue();
                    if (!arrayList.contains(Integer.valueOf(intValue2))) {
                        k(intValue2);
                    }
                }
            }
            this.k = i2;
            this.s = -1;
            if (z2) {
                i();
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean i(int i2) {
        if (i2 < 0 || i2 >= this.i.f1285a.getCount()) {
            return false;
        }
        return true;
    }

    private int j(int i2) {
        return this.i.d * i2;
    }

    public ImageView d(int i2) {
        return n(i2).b();
    }

    private View k(int i2) {
        this.H.a();
        a("AddView:" + i2, 4);
        ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c e2 = e(i2);
        View view = this.i.f1285a.getView(i2, null, this.h);
        view.setOnClickListener(this);
        view.setOnTouchListener(this.P);
        this.F.get(Integer.valueOf(i2)).addView(view);
        this.G.put(Integer.valueOf(i2), view);
        if (e2.g()) {
            q(i2);
        } else if (e(i2).a() != null) {
            this.i.b.b(e(i2).a(), i2, this.k, this.i.f1285a.getItem(i2).c(), this.i.f1285a.getItem(i2).d(), new a.AbstractC0100a() {
                /* class ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.AnonymousClass3 */

                @Override // ui_Controller.CustomWidget.a.AbstractC0100a
                public void a(Bitmap bitmap, int i) {
                    SingleHorizontalScrollView.this.b((SingleHorizontalScrollView) bitmap, (Bitmap) i);
                }
            });
        } else {
            r(i2);
        }
        this.H.b();
        return view;
    }

    private void l(int i2) {
        this.H.a();
        a("DeleteView:" + i2, 4);
        ui_Controller.CustomWidget.SingleHorizontalScrollView.a.b n2 = n(i2);
        n2.a().setImageResource(17170445);
        n2.a().setImageBitmap(null);
        if (e(i2).f() != null) {
            if (!e(i2).f().isRecycled()) {
                a("recycle bitmap by DeleteView:" + e(i2).f(), 4);
                e(i2).f().recycle();
            }
            e(i2).b((Bitmap) null);
        }
        if (e(i2).e() != null) {
            e(i2).a((Bitmap) null);
        }
        this.G.remove(Integer.valueOf(i2));
        this.F.get(Integer.valueOf(i2)).removeAllViews();
        this.H.b();
    }

    public ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c e(int i2) {
        return this.i.f1285a.b(i2);
    }

    private ArrayList<Integer> a(Map map) {
        Set<Map.Entry> entrySet = map.entrySet();
        ArrayList<Integer> arrayList = new ArrayList<>();
        if (entrySet != null) {
            for (Map.Entry entry : entrySet) {
                arrayList.add((Integer) entry.getKey());
            }
        }
        Collections.sort(arrayList, new Comparator<Integer>() {
            /* class ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.AnonymousClass4 */

            /* renamed from: a */
            public int compare(Integer num, Integer num2) {
                return num.compareTo(num2);
            }
        });
        return arrayList;
    }

    private View m(int i2) {
        View childAt = this.F.get(Integer.valueOf(i2)).getChildAt(0);
        if (childAt == null) {
            a("GetView:" + i2, 4);
        }
        return childAt;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private ui_Controller.CustomWidget.SingleHorizontalScrollView.a.b n(int i2) {
        View m2 = m(i2);
        if (m2 != null) {
            return (ui_Controller.CustomWidget.SingleHorizontalScrollView.a.b) m2.getTag();
        }
        return null;
    }

    private LinearLayout b(int i2, boolean z2) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((ViewGroup.MarginLayoutParams) new FrameLayout.LayoutParams(this.i.d, this.i.e));
        LinearLayout linearLayout = new LinearLayout(this.b);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOnClickListener(this);
        linearLayout.setOnTouchListener(this.P);
        if (z2) {
            this.h.addView(linearLayout, 0);
        } else {
            this.h.addView(linearLayout);
        }
        this.F.put(Integer.valueOf(i2), linearLayout);
        return linearLayout;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean h() {
        return this.t;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(boolean z2) {
        a("SetKeyBlock:" + z2, 2);
        this.t = z2;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (((motionEvent.getAction() & 255) != 2 || this.E != 0) && this.J) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return false;
    }

    public void e() {
        if (i(this.k - 1)) {
            a(this.k - 1, true);
            smoothScrollTo(j(this.k), 0);
        }
    }

    public void f() {
        if (i(this.k + 1)) {
            a(this.k + 1, true);
            smoothScrollTo(j(this.k), 0);
        }
    }

    public int getCurrentIndex() {
        return this.k;
    }

    public boolean f(int i2) {
        if (e(i2).l() == 6) {
            return true;
        }
        return false;
    }

    public String getImagePath() {
        return e(this.k).b();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (j() || h()) {
            return true;
        }
        if (this.E != 1 && this.E != 2) {
            return true;
        }
        int i2 = (int) (((float) this.i.d) * this.u);
        int action = motionEvent.getAction() & 255;
        switch (action) {
            case 1:
            case 5:
                if (this.E == 2) {
                    return true;
                }
                if (action == 5) {
                    this.E = 2;
                }
                if (this.A == null) {
                    this.B = new PointF();
                    this.A = new PointF();
                    this.A.set(motionEvent.getX(), motionEvent.getY());
                }
                if (motionEvent.getX() - this.A.x > ((float) i2)) {
                    if (i(this.k - 1)) {
                        s(this.k);
                        a(this.k - 1, true);
                        b(j(this.k), 0);
                    }
                } else if (this.A.x - motionEvent.getX() <= ((float) i2)) {
                    b(j(this.k), 0);
                } else if (i(this.k + 1)) {
                    s(this.k);
                    a(this.k + 1, true);
                    b(j(this.k), 0);
                }
                this.B = null;
                this.A = null;
                a(motionEvent);
                this.E = 0;
                return true;
            case 2:
                a(motionEvent);
                if (this.B == null) {
                    this.B = new PointF();
                    this.A = new PointF();
                    this.A.set(motionEvent.getX(), motionEvent.getY());
                }
                this.B.set(motionEvent.getX(), motionEvent.getY());
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    private void b(int i2, int i3) {
        if (Math.abs(this.q - i2) > 1 && this.q != -1) {
            a("blockKeyAndSmoothScroll" + i2, 2);
            this.r = i2;
            a(true);
        }
        smoothScrollTo(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i2, int i3, int i4, int i5) {
        super.onScrollChanged(i2, i3, i4, i5);
        if (this.p != -1) {
            System.currentTimeMillis();
            this.p = System.currentTimeMillis();
            this.q = i2;
        }
        this.p = System.currentTimeMillis();
        this.q = i2;
        if (this.r != -1 && Math.abs(this.q - this.r) <= 1) {
            a("scroll done:" + this.r, 2);
            a(false);
            this.r = -1;
        }
    }

    public void onClick(View view) {
        a("onClick", 4);
    }

    private void i() {
        if (this.c != null) {
            this.c.a(this.k, this.h.getChildAt(0));
        }
    }

    private void a(int i2, Bitmap bitmap) {
        if (this.d != null) {
            this.d.a(i2, bitmap);
        }
    }

    private void o(int i2) {
        ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c e2 = e(i2);
        if (e2.k()) {
            e2.b(false);
            com.a.a.c.b(this.N.getApplication()).a((View) d(i2));
        }
    }

    private void p(int i2) {
        ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c e2 = e(i2);
        if (!e2.k()) {
            e2.b(true);
            com.a.a.c.b(this.N.getApplication()).a(getImagePath()).a(d(i2));
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, PointF pointF) {
        if (this.e != null) {
            this.e.a(i2, pointF);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(MotionEvent motionEvent) {
        if (this.f != null) {
            this.f.a(this.k, motionEvent);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean j() {
        if (this.g != null) {
            return this.g.a();
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean b(MotionEvent motionEvent) {
        if (Math.abs(this.x.x - motionEvent.getX()) > ((float) this.i.d) / 16.0f) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean c(MotionEvent motionEvent) {
        if (Math.abs(motionEvent.getX() - this.x.x) >= ((float) this.i.d) / 16.0f || Math.abs(motionEvent.getY() - this.x.y) >= ((float) this.i.d) / 16.0f) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(Bitmap bitmap, int i2) {
        this.H.a();
        a("MainImageLoaded:" + i2 + " " + bitmap, 4);
        if (bitmap != null) {
            a("isRecycle:" + bitmap.isRecycled(), 4);
        }
        if (bitmap == null) {
            a("MainImageLoaded no image:" + i2, 4);
            if (e(i2) != null) {
                e(i2).a(true);
            }
        }
        if (this.G.containsKey(Integer.valueOf(i2))) {
            e(i2).b(bitmap);
            c(bitmap, i2);
            if (f(i2) && i2 == this.k && this.K == 0) {
                p(i2);
            }
        } else {
            a("MainImageLoaded view is delete , recycle bitmap!" + bitmap, 4);
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        a(i2, bitmap);
        this.H.b();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void b(Bitmap bitmap, int i2) {
        this.H.a();
        a("ThumbnialLoaded:" + i2 + " " + bitmap, 4);
        if (bitmap == null) {
            a("thumbnialLoaded no thumbnial:" + i2, 4);
            if (e(i2) != null) {
                e(i2).a(true);
            }
        }
        if (!this.G.containsKey(Integer.valueOf(i2))) {
            a("thumbnialLoaded view is delete , so ignore!", 4);
        } else if (e(i2).g()) {
            q(i2);
        } else {
            e(i2).a(bitmap);
            if (e(i2).f() == null) {
                c(bitmap, i2);
            }
        }
        this.H.b();
    }

    private void c(Bitmap bitmap, int i2) {
        ui_Controller.CustomWidget.SingleHorizontalScrollView.a.b n2 = n(i2);
        ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c e2 = e(i2);
        e2.n().f1286a.reset();
        e2.n().b = a(bitmap, e2.n().f1286a);
        e2.n().c = b(bitmap, e2.n().f1286a);
        n2.b().setScaleType(ImageView.ScaleType.MATRIX);
        n2.b().setImageMatrix(e2.n().f1286a);
        n2.a().setScaleType(ImageView.ScaleType.MATRIX);
        n2.a().setImageMatrix(e2.n().f1286a);
        n2.a().setImageBitmap(bitmap);
    }

    private void q(int i2) {
        n(i2).a().setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    private void r(int i2) {
        ui_Controller.CustomWidget.SingleHorizontalScrollView.a.b n2 = n(i2);
        n2.a().setScaleType(ImageView.ScaleType.FIT_CENTER);
        n2.a().setImageResource(17170445);
        n2.a().setImageBitmap(null);
    }

    private float a(Bitmap bitmap, Matrix matrix) {
        if (bitmap == null) {
            return 1.0f;
        }
        float min = Math.min(((float) this.i.d) / ((float) bitmap.getWidth()), ((float) this.i.e) / ((float) bitmap.getHeight()));
        matrix.postScale(min, min);
        return min;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float b(Bitmap bitmap, Matrix matrix) {
        return a(true, true, bitmap, matrix);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float a(boolean r9, boolean r10, android.graphics.Bitmap r11, android.graphics.Matrix r12) {
        /*
        // Method dump skipped, instructions count: 128
        */
        throw new UnsupportedOperationException("Method not decompiled: ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.a(boolean, boolean, android.graphics.Bitmap, android.graphics.Matrix):float");
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c cVar, PointF pointF) {
        float f2;
        if (cVar.n().a() > cVar.n().b) {
            f2 = 1.0f;
        } else {
            f2 = 2.0f;
        }
        Bitmap f3 = cVar.f();
        if (f3 == null) {
            f3 = cVar.e();
        }
        cVar.n().f1286a.reset();
        cVar.n().b = a(f3, cVar.n().f1286a);
        cVar.n().c = b(f3, cVar.n().f1286a);
        if (((double) f2) != 1.0d) {
            cVar.n().f1286a.postScale(f2, f2, pointF.x, pointF.y);
            cVar.n().c = b(f3, cVar.n().f1286a);
        }
    }

    private void s(int i2) {
        ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c e2 = e(i2);
        ui_Controller.CustomWidget.SingleHorizontalScrollView.a.b n2 = n(i2);
        Matrix matrix = new Matrix();
        float f2 = e2.n().b;
        Bitmap f3 = e2.f();
        if (f3 == null) {
            f3 = e2.e();
        }
        matrix.reset();
        matrix.postScale(f2 * 1.0f, 1.0f * f2, 0.0f, 0.0f);
        e2.n().f1286a.set(matrix);
        e2.n().c = b(f3, e2.n().f1286a);
        n2.b().setImageMatrix(e2.n().f1286a);
        n2.a().setImageMatrix(e2.n().f1286a);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(ui_Controller.CustomWidget.SingleHorizontalScrollView.a.c cVar) {
        Matrix matrix = new Matrix();
        float a2 = cVar.n().a();
        float f2 = cVar.n().b;
        matrix.set(cVar.n().f1286a);
        if (a2 < f2) {
            matrix.reset();
            matrix.postScale(f2, f2);
        } else if (a2 > f2 * this.f1279a) {
            matrix.set(this.v);
        } else {
            this.v.set(cVar.n().f1286a);
        }
        cVar.n().f1286a.set(matrix);
        Bitmap f3 = cVar.f();
        if (f3 == null) {
            f3 = cVar.e();
        }
        cVar.n().c = b(f3, cVar.n().f1286a);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean c(Bitmap bitmap, Matrix matrix) {
        Matrix matrix2 = new Matrix();
        matrix2.set(matrix);
        RectF rectF = new RectF(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight());
        matrix2.mapRect(rectF);
        float width = rectF.width();
        int i2 = this.i.d;
        if (width < ((float) i2) || rectF.left == 0.0f || rectF.right == ((float) i2)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float d(MotionEvent motionEvent) {
        float x2 = motionEvent.getX(0) - motionEvent.getX(1);
        float y2 = motionEvent.getY(0) - motionEvent.getY(1);
        return (float) Math.sqrt((double) ((x2 * x2) + (y2 * y2)));
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(PointF pointF, MotionEvent motionEvent) {
        pointF.set((motionEvent.getX(0) + motionEvent.getX(1)) / 2.0f, (motionEvent.getY(0) + motionEvent.getY(1)) / 2.0f);
    }
}
