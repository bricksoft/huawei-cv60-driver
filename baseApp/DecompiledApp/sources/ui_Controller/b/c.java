package ui_Controller.b;

public class c {

    /* renamed from: a  reason: collision with root package name */
    int f1317a = 0;
    private int b;
    private int c;
    private int d = 0;
    private boolean e = false;
    private long f;

    public c(int i, int i2, long j) {
        this.b = i;
        this.c = i2;
        this.f = j;
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public int c() {
        return this.d;
    }

    public long d() {
        return this.f;
    }

    public void a(int i, int i2) {
        this.b = i;
        this.c = i2;
    }

    public void a(int i) {
        this.d = i;
    }

    public void a(boolean z) {
        this.e = z;
    }

    public void b(int i) {
        this.f1317a = i;
    }

    public boolean e() {
        return this.e;
    }

    public boolean f() {
        if (this.f1317a == 1) {
            return true;
        }
        return false;
    }

    public boolean g() {
        if (this.f1317a == 2) {
            return true;
        }
        return false;
    }

    public String h() {
        return "lHeaderId:" + this.d + "\r\nbIsSelected:" + this.e + "\r\nlStatus:" + this.f1317a + "\r\nlGroupIndex:" + this.b + "\r\nlFileIndex:" + this.c + "\r\nlSortTime:" + this.f + "\r\n";
    }
}
