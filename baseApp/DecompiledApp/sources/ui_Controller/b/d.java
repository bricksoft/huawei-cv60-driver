package ui_Controller.b;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static d f1318a = null;
    private a b = new a();

    public static d a() {
        if (f1318a == null) {
            synchronized (d.class) {
                if (f1318a == null) {
                    f1318a = new d();
                }
            }
        }
        return f1318a;
    }

    public void a(boolean z) {
        this.b.f1319a = z;
    }

    public boolean b() {
        return this.b.f1319a;
    }

    public void a(int i) {
        this.b.b = i;
    }

    public int c() {
        return this.b.b;
    }

    /* access modifiers changed from: private */
    public class a {

        /* renamed from: a  reason: collision with root package name */
        boolean f1319a;
        int b;

        private a() {
            this.f1319a = false;
            this.b = -1;
        }
    }
}
