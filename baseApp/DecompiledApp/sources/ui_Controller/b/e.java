package ui_Controller.b;

import GeneralFunction.Player.player.h;
import GeneralFunction.e.d;
import android.graphics.Bitmap;
import android.net.Uri;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class e {
    public boolean A;
    public String B;
    public int C;
    public int D;
    public int E;
    public int F;
    public int G;
    public int H;
    public long I;
    public long J;
    public boolean K;
    public int L;
    public boolean M;
    public int N;
    public int O;
    public int P;
    public int Q;
    public int R;
    public a S;
    public GeneralFunction.q.a T;
    public String U;
    public int V;
    public boolean W;
    public h X;
    public int Y;
    public boolean Z;

    /* renamed from: a  reason: collision with root package name */
    public d f1320a;
    public Uri aa;
    public String ab;
    public String ac;
    public String ad;
    public long ae;
    public List<Integer> af;
    public List<Integer> ag;
    public int ah;
    public boolean ai;
    public int aj;
    public int ak;
    public boolean al;
    public ArrayList<File> am;
    public d b;
    public boolean c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public int i;
    public ArrayList<Integer> j;
    public ArrayList<Integer> k;
    public ArrayList<Integer> l;
    public ui_Controller.CustomWidget.a.a.a m;
    public ui_Controller.CustomWidget.a.a.d n;
    public ui_Controller.CustomWidget.a.a.d o;
    public ui_Controller.CustomWidget.a.a.d p;
    public GeneralFunction.a q;
    public boolean r;
    public int s;
    public int t;
    public int u;
    public boolean v;
    public boolean w;
    public boolean x;
    public boolean y;
    public boolean z;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public Bitmap f1321a;
        public int b;

        public a() {
            this.f1321a = null;
            this.b = -1;
            this.f1321a = null;
            this.b = -1;
        }
    }

    public e() {
        this.v = true;
        this.w = true;
        this.x = true;
        this.y = false;
        this.z = false;
        this.A = false;
        this.B = null;
        this.C = 0;
        this.D = 0;
        this.E = 0;
        this.F = 0;
        this.G = 0;
        this.H = 0;
        this.I = 0;
        this.J = 0;
        this.K = false;
        this.L = 0;
        this.M = false;
        this.N = 2;
        this.O = 0;
        this.P = 1;
        this.Q = 3;
        this.R = 0;
        this.S = null;
        this.T = null;
        this.U = null;
        this.V = 0;
        this.W = false;
        this.X = null;
        this.Y = 0;
        this.Z = false;
        this.aa = null;
        this.ab = null;
        this.ac = null;
        this.ad = null;
        this.ae = 0;
        this.af = null;
        this.ag = null;
        this.ai = false;
        this.aj = 0;
        this.ak = 0;
        this.al = false;
        this.am = null;
        this.f1320a = new d();
        this.b = new d();
        this.c = false;
        this.d = 1;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = new ArrayList<>();
        this.k = new ArrayList<>();
        this.l = new ArrayList<>();
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = new GeneralFunction.a();
        this.r = false;
        this.s = 0;
        this.t = 0;
        this.u = 0;
        this.v = true;
        this.C = 1;
        this.D = 0;
        this.E = 0;
        this.F = 0;
        this.G = 0;
        this.H = 0;
        this.I = 0;
        this.J = 0;
        this.K = false;
        this.L = 0;
        this.M = false;
        this.N = 2;
        this.O = 0;
        this.P = 1;
        this.Q = 3;
        this.R = 0;
        this.S = new a();
    }
}
