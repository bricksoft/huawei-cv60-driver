package ui_Controller.b;

import java.util.ArrayList;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public int f1315a;
    public ArrayList<Integer> b = null;
    public int c;
    public int d;

    public a(int i, ArrayList<Integer> arrayList, int i2, int i3) {
        this.f1315a = i;
        this.b = arrayList;
        this.c = i2;
        this.d = i3;
    }

    public String a() {
        String str = "lParentIndex:" + this.f1315a + "\r\nlActionType:" + this.c + "\r\nlImageType:" + this.d + "\r\n";
        if (this.b == null) {
            return str + "lChildIndex:" + this.b + "\r\n";
        }
        String str2 = str + "lChildIndex:";
        for (int i = 0; i < this.b.size(); i++) {
            str2 = str2 + this.b.get(i) + " ";
        }
        return str2;
    }
}
