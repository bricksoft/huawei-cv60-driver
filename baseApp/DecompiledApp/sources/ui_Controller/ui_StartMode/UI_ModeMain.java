package ui_Controller.ui_StartMode;

import GeneralFunction.d.c;
import GeneralFunction.e.e;
import GeneralFunction.g;
import GeneralFunction.h.b;
import GeneralFunction.k;
import a.c.d;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.PointerIconCompat;
import com.google.android.gms.auth.api.proxy.AuthApiStatusCodes;
import com.huawei.cvIntl60.R;
import java.io.File;
import java.util.ArrayList;
import ui_Controller.b.j;
import ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController;

public class UI_ModeMain extends Application {
    private static Context w;
    private boolean A = false;
    private BroadcastReceiver B = new BroadcastReceiver() {
        /* class ui_Controller.ui_StartMode.UI_ModeMain.AnonymousClass5 */

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void onReceive(Context context, Intent intent) {
            char c;
            String action = intent.getAction();
            switch (action.hashCode()) {
                case -1676458352:
                    if (action.equals("android.intent.action.HEADSET_PLUG")) {
                        c = 2;
                        break;
                    }
                    c = 65535;
                    break;
                case -19011148:
                    if (action.equals("android.intent.action.LOCALE_CHANGED")) {
                        c = 0;
                        break;
                    }
                    c = 65535;
                    break;
                case 505380757:
                    if (action.equals("android.intent.action.TIME_SET")) {
                        c = 1;
                        break;
                    }
                    c = 65535;
                    break;
                default:
                    c = 65535;
                    break;
            }
            switch (c) {
                case 0:
                    UI_ModeMain.this.c.g.e = true;
                    GeneralFunction.a.a.f62a = null;
                    return;
                case 1:
                    UI_ModeMain.this.a((UI_ModeMain) "ACTION_TIME_CHANGED", (String) 0);
                    g.a();
                    UI_ModeMain.this.d.e();
                    return;
                case 2:
                    int intExtra = intent.getIntExtra("state", -1);
                    a.c.a aVar = new a.c.a(12051);
                    aVar.a("state", intExtra);
                    UI_ModeMain.this.a(aVar);
                    return;
                default:
                    return;
            }
        }
    };
    private BroadcastReceiver C = new BroadcastReceiver() {
        /* class ui_Controller.ui_StartMode.UI_ModeMain.AnonymousClass6 */

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            char c = 65535;
            switch (action.hashCode()) {
                case -1665311200:
                    if (action.equals("android.intent.action.MEDIA_REMOVED")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1514214344:
                    if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                        c = 0;
                        break;
                    }
                    break;
                case -625887599:
                    if (action.equals("android.intent.action.MEDIA_EJECT")) {
                        c = 3;
                        break;
                    }
                    break;
                case 2045140818:
                    if (action.equals("android.intent.action.MEDIA_BAD_REMOVAL")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    UI_ModeMain.this.a((UI_ModeMain) "ACTION_MEDIA_MOUNTED", (String) 2);
                    UI_ModeMain.this.a(12046, 0);
                    return;
                case 1:
                    UI_ModeMain.this.a((UI_ModeMain) "ACTION_MEDIA_REMOVED", (String) 2);
                    UI_ModeMain.this.a(12047, 0);
                    return;
                case 2:
                    UI_ModeMain.this.a((UI_ModeMain) "ACTION_MEDIA_BAD_REMOVAL", (String) 2);
                    UI_ModeMain.this.a(12048, 0);
                    return;
                case 3:
                    UI_ModeMain.this.a((UI_ModeMain) "ACTION_MEDIA_EJECT", (String) 2);
                    UI_ModeMain.this.a(12049, 0);
                    return;
                default:
                    return;
            }
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public SharedPreferences f1526a;
    public SharedPreferences.Editor b;
    public j c;
    public d d = null;
    public c e = null;
    public boolean f = true;
    public boolean g = true;
    public boolean h = false;
    public Runnable i = new Runnable() {
        /* class ui_Controller.ui_StartMode.UI_ModeMain.AnonymousClass4 */

        public void run() {
            UI_ModeMain.this.c.k.e = new e(UI_ModeMain.w);
            UI_ModeMain.this.c.k.e.b();
            if (!UI_ModeMain.this.H()) {
                UI_ModeMain.this.I();
            }
            UI_ModeMain.this.c.k.e.a(UI_ModeMain.this.c.k.c, UI_ModeMain.this.c.k.f, UI_ModeMain.this.c.k.g, UI_ModeMain.this.c.k.h);
            UI_ModeMain.this.c.k.b.f1320a.a(UI_ModeMain.this.c.k.e.a(), UI_ModeMain.this.c.k.f, 1);
            UI_ModeMain.this.c.k.b.b.a(UI_ModeMain.this.c.k.e.a(), UI_ModeMain.this.c.k.f, 1);
            UI_ModeMain.this.c.k.b.f1320a.a();
            UI_ModeMain.this.c.k.b.f1320a.b();
            UI_ModeMain.this.c.k.b.f1320a.c();
            UI_ModeMain.this.a((UI_ModeMain) "CreateLocalDatabaseProcess done", (String) 2);
            UI_ModeMain.this.c.k.b.b.a();
            UI_ModeMain.this.c.k.b.b.b();
            UI_ModeMain.this.c.k.b.c = true;
        }
    };
    private boolean j = false;
    private boolean k = false;
    private boolean l = false;
    private HandlerThread m = null;
    private Handler n = null;
    private b o = null;
    private GeneralFunction.m.a p = null;
    private c q = null;
    private b r = null;
    private GeneralFunction.j.b s = null;
    private final String t = (GeneralFunction.g.a.a() + "/AutoDump/cv60config.txt");
    private final String u = (GeneralFunction.g.a.a() + "/AutoDump/config.txt");
    private GeneralFunction.b.a v = null;
    private ArrayList<Message> x = new ArrayList<>();
    private Handler y = new Handler(Looper.getMainLooper()) {
        /* class ui_Controller.ui_StartMode.UI_ModeMain.AnonymousClass1 */

        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (!UI_ModeMain.this.a(message)) {
                if (!(message.what == 1 || message.what == 18176 || message.what == 18464)) {
                    UI_ModeMain.this.a((UI_ModeMain) ("handleMessage: 0x" + Integer.toHexString(message.what) + " mode:0x" + Integer.toHexString(UI_ModeMain.this.c.c)), (String) 3);
                }
                if (message.what == 12032) {
                    UI_ModeMain.this.a((UI_ModeMain) "MSG_UI_SYSTEM_CLOSE_APP: force close APP", (String) 1);
                    Process.killProcess(Process.myPid());
                    return;
                }
                if (message.what == 18432) {
                    UI_ModeMain.this.a((UI_ModeMain) "MSG_REMOTE_USB_CORE_INITIAL done", (String) 1);
                    UI_ModeMain.this.k = true;
                }
                if (!UI_ModeMain.this.e.a(message)) {
                    UI_ModeMain.this.p.a(message);
                    UI_ModeMain.this.o.a(message);
                    if (!UI_ModeMain.this.q.a(message) && !UI_ModeMain.this.r.a(message)) {
                        if (UI_ModeMain.this.s != null && UI_ModeMain.this.s.a(message)) {
                            return;
                        }
                        if (UI_ModeMain.this.c.c == 0) {
                            if (UI_ModeMain.this.x.size() == 0) {
                                UI_ModeMain.this.a(new a.c.a(1));
                            }
                            if (message.what == 1) {
                                UI_ModeMain.this.a(new a.c.a(1));
                            } else {
                                UI_ModeMain.this.x.add(message);
                            }
                        } else {
                            while (UI_ModeMain.this.x.size() > 0) {
                                UI_ModeMain.this.c.b.a((Message) UI_ModeMain.this.x.get(0));
                                UI_ModeMain.this.x.remove(0);
                            }
                            UI_ModeMain.this.c.b.a(message);
                        }
                    }
                }
            }
        }
    };
    private d.a z = new d.a() {
        /* class ui_Controller.ui_StartMode.UI_ModeMain.AnonymousClass3 */

        @Override // a.c.d.a
        public void a(a.c.a aVar) {
            if (aVar.b().what == 18435) {
                UI_ModeMain.this.c.h.av = null;
            }
            UI_ModeMain.this.a(aVar);
        }
    };

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        GeneralFunction.d.a("UI_ModeMain", str, i2);
    }

    public int a() {
        return this.c.c;
    }

    public void a(int i2) {
        this.c.c = i2;
    }

    public void a(int i2, GeneralFunction.a.a aVar) {
        this.c.b = aVar;
        this.c.c = i2;
    }

    public boolean a(long j2) {
        if ((this.c.f1326a & j2) > 0) {
            return true;
        }
        return false;
    }

    public void b(long j2) {
        long j3 = 268435455 ^ j2;
        this.c.f1326a &= j3;
        a("UI_DisableKey:" + Long.toHexString(j3), 4);
    }

    public void c(long j2) {
        this.c.f1326a |= j2;
        a("UI_EnableKey:" + Long.toHexString(j2), 4);
    }

    public boolean a(Message message) {
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 12034:
                if (!g()) {
                    a(12042, 0);
                }
                return (this.c.c & MotionEventCompat.ACTION_POINTER_INDEX_MASK) != (aVar.b("mode") & MotionEventCompat.ACTION_POINTER_INDEX_MASK);
            case 12035:
            case 12036:
            case 12037:
            case 12038:
                return (this.c.c & MotionEventCompat.ACTION_POINTER_INDEX_MASK) != (aVar.b("mode") & MotionEventCompat.ACTION_POINTER_INDEX_MASK);
            default:
                return false;
        }
    }

    public void a(int i2, long j2) {
        a(new a.c.a(i2), j2);
    }

    public void a(a.c.a aVar) {
        a(aVar, 0);
    }

    public void a(a.c.a aVar, long j2) {
        a(aVar.b(), j2);
    }

    public void a(Message message, long j2) {
        this.y.sendMessageDelayed(message, j2);
    }

    public boolean b(int i2) {
        return this.y.hasMessages(i2);
    }

    private void z() {
        this.r = new b(this);
    }

    private void A() {
        this.e = new c(this);
    }

    public int b() {
        return 0;
    }

    private void B() {
        this.q = new c(this);
    }

    public void a(int i2, GeneralFunction.a.a aVar, Intent intent) {
        this.q.a(i2, aVar, intent);
    }

    public GeneralFunction.a.a c() {
        return this.c.b;
    }

    public void a(GeneralFunction.a.a aVar) {
        this.q.a(1024, aVar, new Intent(aVar, UI_PhoneGalleryController.class));
    }

    private void C() {
        this.m = new HandlerThread("AidServer");
        this.m.start();
        this.n = new Handler(this.m.getLooper()) {
            /* class ui_Controller.ui_StartMode.UI_ModeMain.AnonymousClass2 */

            public void handleMessage(Message message) {
                super.handleMessage(message);
                while (UI_ModeMain.this.c.c == 0) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                UI_ModeMain.this.c.b.b(message);
            }
        };
    }

    public void b(int i2, long j2) {
        b(new a.c.a(i2), j2);
    }

    public void b(a.c.a aVar) {
        b(aVar, 0);
    }

    public void b(a.c.a aVar, long j2) {
        b(aVar.b(), j2);
    }

    public void b(Message message, long j2) {
        this.n.sendMessageDelayed(message, j2);
    }

    private void D() {
        this.d = new d(this, this.z);
        this.d.a();
    }

    public void c(a.c.a aVar) {
        c(aVar, 0);
    }

    public void c(a.c.a aVar, long j2) {
        this.d.a(aVar, j2);
    }

    private void a(ui_Controller.b.b bVar) {
        this.o = new b(this);
        this.o.a(bVar);
    }

    public void d(a.c.a aVar) {
        d(aVar, 0);
    }

    public void d(a.c.a aVar, long j2) {
        this.o.a(aVar, j2);
    }

    public void a(ArrayList<ui_Controller.b.a> arrayList) {
        this.o.a(arrayList);
    }

    public void d() {
        this.o.b();
    }

    public void e() {
        this.o.c();
    }

    public int f() {
        return this.o.d();
    }

    private void E() {
        this.p = new GeneralFunction.m.a(this);
    }

    public boolean g() {
        String[] strArr = ui_Controller.a.c.r;
        for (String str : strArr) {
            if (ActivityCompat.checkSelfPermission(this, str) == -1) {
                a("checkHasAllPermissioins false: " + str, 0);
                return false;
            }
        }
        return true;
    }

    private void F() {
        this.s = new GeneralFunction.j.b(this);
    }

    public GeneralFunction.j.b h() {
        return this.s;
    }

    public void i() {
        a("CheckUSBDeviceHasPermission", 1);
        this.d.a(new a.c.a(18437));
    }

    public void j() {
        this.k = false;
        this.d.a(new a.c.a(18432));
    }

    public boolean k() {
        return this.k;
    }

    public void l() {
        if (!this.d.b()) {
            j();
        } else if (!this.d.d()) {
            a.c.a aVar = new a.c.a(18480);
            aVar.a("app_version", getResources().getString(R.string.internal_app_version));
            c(aVar);
        } else {
            a("Live view already start, get frame directly", 1);
            c(new a.c.a(18464), 0);
        }
    }

    public boolean m() {
        return this.d.c();
    }

    public boolean n() {
        return this.j;
    }

    public void o() {
        this.j = true;
    }

    public boolean p() {
        return this.l;
    }

    public void q() {
        a("APP auto launch by USB attach", 2);
        this.l = true;
    }

    public boolean r() {
        new GeneralFunction.g.b().a(getResources().openRawResource(R.raw.fw13b00), ui_Controller.a.c.p);
        if (!this.o.a(ui_Controller.a.c.p + "FPUPDATE.DAT")) {
            a("Unzip Fail! retry again", 0);
            new GeneralFunction.g.b().a(getResources().openRawResource(R.raw.fw13b00), ui_Controller.a.c.p);
        }
        if (!this.o.a(ui_Controller.a.c.p + "FPUPDATE.DAT")) {
            return false;
        }
        return true;
    }

    public static Context s() {
        return w;
    }

    /* access modifiers changed from: private */
    public class a extends Thread {
        private int b = 1000;

        a(int i) {
            this.b = i;
        }

        /* JADX WARNING: Removed duplicated region for block: B:13:0x003b A[SYNTHETIC, Splitter:B:13:0x003b] */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0040 A[SYNTHETIC, Splitter:B:16:0x0040] */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0045  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x011b A[RETURN] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            // Method dump skipped, instructions count: 291
            */
            throw new UnsupportedOperationException("Method not decompiled: ui_Controller.ui_StartMode.UI_ModeMain.a.run():void");
        }
    }

    public void t() {
        a("CreateAutoDumpInstance", 3);
        if (!this.A) {
            this.A = true;
            new a(AuthApiStatusCodes.AUTH_API_INVALID_CREDENTIALS).start();
        }
    }

    private void G() {
        a(ui_Controller.a.c.g);
        a(ui_Controller.a.c.h);
        a("Start Create Database Process", 3);
        new Thread(this.i).start();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean H() {
        return GeneralFunction.n.a.a(this).getBoolean("haveUserInstruction", false);
    }

    public boolean u() {
        return this.c.k.b.c;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void I() {
        String str = ui_Controller.a.c.c + "CV60_Photo_1.jpg";
        String substring = str.substring(0, str.lastIndexOf("/") + 1);
        String substring2 = str.substring(str.lastIndexOf("/") + 1, str.length());
        if (!this.c.k.e.b(substring, substring2)) {
            this.c.k.e.a(substring2, substring, 7411340, 0, 1, 5376, 2688, 0, 0, 0, 1000, 0, true, true, true, false, false, true, true, false, false);
        }
        String str2 = ui_Controller.a.c.c + "CV60_Photo_2.jpg";
        String substring3 = str2.substring(0, str2.lastIndexOf("/") + 1);
        String substring4 = str2.substring(str2.lastIndexOf("/") + 1, str2.length());
        if (!this.c.k.e.b(substring3, substring4)) {
            this.c.k.e.a(substring4, substring3, 7073281, 0, 1, 5376, 2688, 0, 0, 0, 1001, 0, true, true, true, false, false, true, true, false, false);
        }
        String str3 = ui_Controller.a.c.c + "CV60_Photo_3.jpg";
        String substring5 = str3.substring(0, str3.lastIndexOf("/") + 1);
        String substring6 = str3.substring(str3.lastIndexOf("/") + 1, str3.length());
        if (!this.c.k.e.b(substring5, substring6)) {
            this.c.k.e.a(substring6, substring5, 8288090, 0, 1, 5376, 2688, 0, 0, 0, PointerIconCompat.TYPE_HAND, 0, true, true, true, false, false, true, true, false, false);
        }
        String str4 = ui_Controller.a.c.c + "CV60_Video.mp4";
        String substring7 = str4.substring(0, str4.lastIndexOf("/") + 1);
        String substring8 = str4.substring(str4.lastIndexOf("/") + 1, str4.length());
        if (!this.c.k.e.b(substring7, substring8)) {
            this.c.k.e.a(substring8, substring7, 6389029, 1, 1, 5376, 2688, 4, 0, 0, PointerIconCompat.TYPE_HELP, 0, true, true, true, false, false, true, true, false, false);
        }
    }

    private void a(String str) {
        File file = new File(str);
        if (file.exists()) {
            a(file);
        }
    }

    private void a(File file) {
        if (file == null) {
            a("Input file is null", 0);
            return;
        }
        String[] list = file.list();
        if (list != null) {
            for (String str : list) {
                File file2 = new File(file, str);
                if (file2.isDirectory()) {
                    a(file2);
                }
                a("File : " + file2.getName() + " delete...", 3);
                file2.delete();
            }
        }
        a("Directory : " + file.getName() + " delete...", 3);
        file.delete();
    }

    private void J() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.LOCALE_CHANGED");
        intentFilter.addAction("android.intent.action.TIME_SET");
        intentFilter.addAction("android.intent.action.HEADSET_PLUG");
        registerReceiver(this.B, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter2.addAction("android.intent.action.MEDIA_REMOVED");
        intentFilter2.addAction("android.intent.action.MEDIA_BAD_REMOVAL");
        intentFilter2.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter2.addDataScheme("file");
        registerReceiver(this.C, intentFilter2);
    }

    public void v() {
    }

    public boolean w() {
        return GeneralFunction.n.a.a(this).getInt("launchAppTime", 0) == 1;
    }

    private void K() {
        SharedPreferences a2 = GeneralFunction.n.a.a(this);
        SharedPreferences.Editor b2 = GeneralFunction.n.a.b(this);
        if (!a2.getString("appVersionForCheckFisrtTimeAppLaunch", "").equals(getResources().getString(R.string.internal_app_version))) {
            b2.putString("appVersionForCheckFisrtTimeAppLaunch", getResources().getString(R.string.internal_app_version)).apply();
            b2.putInt("launchAppTime", 0).apply();
        }
        int i2 = a2.getInt("launchAppTime", 0);
        if (i2 < 10000) {
            i2++;
        }
        b2.putInt("launchAppTime", i2).apply();
    }

    public int x() {
        GeneralFunction.e.b bVar = new GeneralFunction.e.b();
        GeneralFunction.e.b bVar2 = new GeneralFunction.e.b();
        GeneralFunction.e.b bVar3 = new GeneralFunction.e.b();
        int a2 = this.c.k.e.a(this.c.k.c, bVar, bVar2, bVar3);
        this.c.k.f = bVar;
        this.c.k.g = bVar2;
        this.c.k.h = bVar3;
        return a2;
    }

    public void onCreate() {
        super.onCreate();
        a("Application create", 3);
        w = this;
        this.c = new j();
        this.c.g.f = Process.myPid();
        J();
        v();
        K();
        g.a(this);
        k.a();
        if (ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
            t();
        }
        z();
        B();
        E();
        a(this.c.k);
        C();
        D();
        A();
        F();
        G();
    }
}
