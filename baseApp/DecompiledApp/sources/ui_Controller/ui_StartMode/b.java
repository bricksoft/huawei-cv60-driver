package ui_Controller.ui_StartMode;

import GeneralFunction.d;
import android.os.Message;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private UI_ModeMain f1535a = null;

    private void a(String str, int i) {
        d.a("UI_MainHandler", str, i);
    }

    public b(UI_ModeMain uI_ModeMain) {
        this.f1535a = uI_ModeMain;
    }

    public boolean a(Message message) {
        switch (message.what) {
            case 12034:
            case 12036:
            default:
                return false;
            case 61441:
                if (ui_Controller.a.b.a(this.f1535a.c.c)) {
                    a("Liveview throughput:" + ((this.f1535a.c.h.R * 8) / 1024) + "Kbits/s", 3);
                }
                this.f1535a.c.h.R = 0;
                return true;
        }
    }
}
