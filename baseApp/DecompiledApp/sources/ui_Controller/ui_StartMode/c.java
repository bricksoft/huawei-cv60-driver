package ui_Controller.ui_StartMode;

import GeneralFunction.d;
import a.c.a;
import android.content.Intent;
import android.os.Message;
import ui_Controller.a.b;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private UI_ModeMain f1536a = null;
    private a b = null;

    private void a(String str, int i) {
        d.a("UI_ModeConrtoller", str, i);
    }

    public c(UI_ModeMain uI_ModeMain) {
        this.f1536a = uI_ModeMain;
        this.b = new a();
    }

    public boolean a(Message message) {
        a aVar = new a(message);
        switch (message.what) {
            case 18176:
                if (b.a(this.f1536a.c.c)) {
                    return false;
                }
                ((a.a.a.d) aVar.f("LiveViewFrame").a()).f169a.d();
                return true;
            default:
                return false;
        }
    }

    public void a(int i, GeneralFunction.a.a aVar, Intent intent) {
        a("ChangeMode lAppMode:" + i + " sModeControlData.lStatus:" + this.b.f1534a, 3);
        this.b.b = i;
        this.b.c = aVar;
        this.b.d = intent;
        a();
    }

    private boolean a() {
        a("ChangeAppMode:" + this.b.d, 3);
        if (this.b.d == null) {
            return false;
        }
        this.b.c.startActivity(this.b.d);
        this.b.c.finish();
        this.b.c = null;
        this.b.d = null;
        return true;
    }
}
