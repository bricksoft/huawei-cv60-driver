package ui_Controller.ui_StartMode;

import GeneralFunction.b.a;
import android.content.Context;
import android.os.Process;
import java.lang.Thread;

public class d implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private Context f1537a = null;
    private a b = null;
    private int c = -1;

    public static void a(Context context, a aVar, int i) {
        Thread.setDefaultUncaughtExceptionHandler(new d(context, aVar, i));
    }

    private d(Context context, a aVar, int i) {
        this.b = aVar;
        this.f1537a = context;
        this.c = i;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        GeneralFunction.d.a("UI_UncaughtException", "FATAL uncaughtException!", 0);
        th.printStackTrace();
        if (this.b != null) {
            new GeneralFunction.o.a().a(this.c);
            this.b.a(true, true);
            this.b.a();
        }
        Process.killProcess(Process.myPid());
        System.exit(10);
    }
}
