package ui_Controller.ui_Setting;

import GeneralFunction.o;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.gms.plus.PlusShare;
import com.huawei.cvIntl60.R;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tv.danmaku.ijk.media.player.IMediaPlayer;
import ui_Controller.b.k;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class UI_SettingMenuController extends GeneralFunction.a.a {
    private ArrayList<String> A = null;
    private ArrayList<String> B = null;
    private ArrayList<String> C = null;
    private ArrayList<String> D = null;
    private ArrayList<String> E = null;
    private ArrayList<String> F = null;
    private ArrayList<String> G = null;
    private ArrayList<String> H = null;
    private ArrayList<String> I = null;
    private ImageButton J = null;
    private ImageButton K = null;
    private ImageButton L = null;
    private ImageButton M = null;
    private ImageButton N = null;
    private ImageButton O = null;
    private LinearLayout P = null;
    private LinearLayout Q = null;
    private LinearLayout R = null;
    private LinearLayout S = null;
    private LinearLayout T = null;
    private LinearLayout U = null;
    private LinearLayout V = null;
    private ListView W = null;
    private ListView X = null;
    private ListView Y = null;
    private ListView Z = null;
    private ImageView aA = null;
    private TextView aB = null;
    private TextView aC = null;
    private Button aD = null;
    private Integer[] aE = null;
    private Integer[] aF = null;
    private Integer[] aG = null;
    private Integer[] aH = null;
    private Integer[] aI = null;
    private Integer[] aJ = null;
    private Integer[] aK = null;
    private Integer[] aL = null;
    private Integer[] aM = null;
    private Integer[] aN = null;
    private Integer[] aO = null;
    private Integer[] aP = null;
    private Integer[] aQ = null;
    private LinearLayout aR = null;
    private WebView aS = null;
    private Button aT = null;
    private LinearLayout aU = null;
    private View.OnClickListener aV = new View.OnClickListener() {
        /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass4 */

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.IB_ui_setting_menu_topBack:
                    UI_SettingMenuController.this.c.a(new a.c.a(8448));
                    return;
                case R.id.IB_ui_setting_submenu_topBack:
                    UI_SettingMenuController.this.P.setVisibility(0);
                    UI_SettingMenuController.this.Q.setVisibility(4);
                    UI_SettingMenuController.this.T.setVisibility(4);
                    return;
                case R.id.IB_ui_setting_about_topBack:
                    UI_SettingMenuController.this.P.setVisibility(0);
                    UI_SettingMenuController.this.R.setVisibility(4);
                    UI_SettingMenuController.this.T.setVisibility(4);
                    return;
                case R.id.IB_ui_setting_update_topBack:
                    UI_SettingMenuController.this.P.setVisibility(4);
                    UI_SettingMenuController.this.Q.setVisibility(4);
                    UI_SettingMenuController.this.R.setVisibility(0);
                    UI_SettingMenuController.this.U.setVisibility(4);
                    UI_SettingMenuController.this.T.setVisibility(4);
                    return;
                case R.id.IB_ui_setting_LegalInformation_sub_TopBack:
                    UI_SettingMenuController.this.V.setVisibility(4);
                    UI_SettingMenuController.this.R.setVisibility(0);
                    return;
                case R.id.IB_ui_setting_OpensourceLicense_topBack:
                    UI_SettingMenuController.this.P.setVisibility(4);
                    UI_SettingMenuController.this.Q.setVisibility(4);
                    UI_SettingMenuController.this.U.setVisibility(4);
                    UI_SettingMenuController.this.T.setVisibility(4);
                    UI_SettingMenuController.this.R.setVisibility(0);
                    return;
                default:
                    return;
            }
        }
    };
    private ListView aa = null;
    private Boolean[] ab = {true, true, true, true, true, true, false, false, false, false, false, false};
    private Boolean[] ac = {false, false, false, false, false, false, false, false, false, true, false, true};
    private int[] ad = null;
    private String[] ae = null;
    private ArrayList<Integer> af = null;
    private int[] ag = null;
    private int[] ah = null;
    private int[] ai = null;
    private int[] aj = null;
    private int[] ak = null;
    private int[] al = null;
    private int[] am = null;
    private int[] an = null;
    private int[] ao = null;
    private LinearLayout ap = null;
    private LinearLayout aq = null;
    private ArrayList<Integer> ar = null;
    private Context as = this;
    private TextView at = null;
    private WebView au = null;
    private Spanned av = null;
    private LinearLayout aw = null;
    private ImageView ax = null;
    private TextView ay = null;
    private LinearLayout az = null;
    public int b = 11;
    protected UI_ModeMain c = null;
    protected ui_Controller.b.f d = null;
    protected k e = null;
    protected PowerManager.WakeLock f = null;
    protected boolean g = false;
    private int h = 0;
    private int i = 0;
    private int j = -1;
    private a k = null;
    private int l = 0;
    private TextView m = null;
    private TextView n = null;
    private TextView o = null;
    private TextView p = null;
    private d q = null;
    private e r = null;
    private c s = null;
    private f t = null;
    private b u = null;
    private ArrayList<String> v = null;
    private ArrayList<String> w = null;
    private ArrayList<String> x = null;
    private ArrayList<String> y = null;
    private ArrayList<String> z = null;

    private void r() {
        this.aE = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        this.aF = new Integer[]{0, 1};
        this.aG = new Integer[]{0, 1};
        this.aH = new Integer[]{0, 1};
        this.aI = new Integer[]{0, 1};
        this.aJ = new Integer[]{0, 1};
        this.aK = new Integer[]{0, 1, 2, 3};
        this.aL = new Integer[]{0, 1};
        this.aM = new Integer[]{0, 1};
        this.aN = new Integer[]{0, 1};
        this.aO = new Integer[]{0, 1};
        this.aP = new Integer[]{0};
        this.aQ = new Integer[]{0, 1};
    }

    private void s() {
        this.v = new ArrayList<>();
        this.v.add(getResources().getString(R.string.store_location));
        this.v.add(getResources().getString(R.string.video_resolution));
        this.v.add(getResources().getString(R.string.photo_resolution));
        this.v.add(getResources().getString(R.string.live_view_mode));
        this.v.add(getResources().getString(R.string.recording_time));
        this.v.add(getResources().getString(R.string.self_timer));
        this.v.add(getResources().getString(R.string.pip_view));
        this.v.add(getResources().getString(R.string.shutter_sound));
        this.v.add(getResources().getString(R.string.water_mark));
        this.v.add(getResources().getString(R.string.user_instructions));
        this.v.add(getResources().getString(R.string.reset_all));
        this.v.add(getResources().getString(R.string.about));
    }

    public void c() {
        this.ad = new int[]{R.drawable.settings_arrow, R.drawable.settings_arrow, R.drawable.settings_arrow, R.drawable.settings_arrow, R.drawable.settings_arrow, R.drawable.settings_arrow, R.drawable.settings_toggle_on, R.drawable.settings_toggle_on, R.drawable.settings_toggle_on, 0, 0, 0};
        if (GeneralFunction.m.a.g() == 1) {
            this.ad[6] = R.drawable.settings_toggle_off;
        }
        if (GeneralFunction.m.a.h() == 1) {
            this.ad[7] = R.drawable.settings_toggle_off;
        }
        if (GeneralFunction.m.a.i() == 0) {
            this.ad[8] = R.drawable.settings_toggle_off;
        }
    }

    private void t() {
        s();
        u();
        w();
        y();
        D();
        j();
        c();
        ((TextView) findViewById(R.id.TV_ui_setting_menu_title)).setText(getResources().getString(R.string.settings));
        this.J = (ImageButton) findViewById(R.id.IB_ui_setting_menu_topBack);
        this.J.setOnClickListener(this.aV);
        this.P = (LinearLayout) findViewById(R.id.LL_ui_setting_menu);
        this.W = (ListView) findViewById(R.id.LV_ui_setting_menu);
        this.af = new ArrayList<>();
        this.af.addAll(Arrays.asList(this.aE));
        this.d.ac = this.c.d.b();
        this.q = new d(this, this.ad, this.v, this.ae, this.af, this.d.ac);
        this.W.setAdapter((ListAdapter) this.q);
        this.W.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass1 */

            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                UI_SettingMenuController.this.d.T = i;
                if (UI_SettingMenuController.this.d.ac && (UI_SettingMenuController.this.d.T == 6 || UI_SettingMenuController.this.d.T == 7)) {
                    if (UI_SettingMenuController.this.ae[UI_SettingMenuController.this.d.T].equals(UI_SettingMenuController.this.getResources().getString(R.string.on))) {
                        UI_SettingMenuController.this.d.U = 1;
                        UI_SettingMenuController.this.ad[UI_SettingMenuController.this.d.T] = R.drawable.settings_toggle_off;
                    } else {
                        UI_SettingMenuController.this.d.U = 0;
                        UI_SettingMenuController.this.ad[UI_SettingMenuController.this.d.T] = R.drawable.settings_toggle_on;
                    }
                    UI_SettingMenuController.this.q.a(i, view, UI_SettingMenuController.this.d.U);
                    UI_SettingMenuController.this.i();
                } else if (UI_SettingMenuController.this.d.ac && UI_SettingMenuController.this.d.T == 8) {
                    if (UI_SettingMenuController.this.ae[UI_SettingMenuController.this.d.T].equals(UI_SettingMenuController.this.getResources().getString(R.string.on))) {
                        UI_SettingMenuController.this.d.U = 1;
                        UI_SettingMenuController.this.ad[UI_SettingMenuController.this.d.T] = R.drawable.settings_toggle_on;
                    } else {
                        UI_SettingMenuController.this.d.U = 0;
                        UI_SettingMenuController.this.ad[UI_SettingMenuController.this.d.T] = R.drawable.settings_toggle_off;
                    }
                    UI_SettingMenuController.this.q.a(i, view, UI_SettingMenuController.this.d.U, true);
                    UI_SettingMenuController.this.i();
                } else if (UI_SettingMenuController.this.d.ac && (UI_SettingMenuController.this.d.T == 0 || UI_SettingMenuController.this.d.T == 1 || UI_SettingMenuController.this.d.T == 2 || UI_SettingMenuController.this.d.T == 3 || UI_SettingMenuController.this.d.T == 4 || UI_SettingMenuController.this.d.T == 5)) {
                    UI_SettingMenuController.this.d((UI_SettingMenuController) i);
                    ObjectAnimator ofFloat = ObjectAnimator.ofFloat(UI_SettingMenuController.this.Q, "translationX", 1600.0f, 0.0f);
                    ofFloat.addListener(new AnimatorListenerAdapter() {
                        /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass1.AnonymousClass1 */

                        public void onAnimationEnd(Animator animator) {
                            super.onAnimationEnd(animator);
                            UI_SettingMenuController.this.P.setVisibility(4);
                            UI_SettingMenuController.this.c((UI_SettingMenuController) false);
                        }
                    });
                    ofFloat.setDuration(300L);
                    UI_SettingMenuController.this.c((UI_SettingMenuController) true);
                    ofFloat.start();
                    UI_SettingMenuController.this.Q.setVisibility(0);
                } else if (i == 9) {
                    UI_SettingMenuController.this.c.a(4128, UI_SettingMenuController.this, new Intent(UI_SettingMenuController.this, UI_SettingUserInstructions.class));
                    UI_SettingMenuController.this.c.a(0, (GeneralFunction.a.a) null);
                    UI_SettingMenuController.this.finish();
                } else if (UI_SettingMenuController.this.d.ac && i == 10) {
                    UI_SettingMenuController.this.g = true;
                    o.b(UI_SettingMenuController.this);
                    o.a((Context) UI_SettingMenuController.this, true, true, UI_SettingMenuController.this.getResources().getString(R.string.reset), UI_SettingMenuController.this.getResources().getString(R.string.reset_all_check), new String[]{UI_SettingMenuController.this.getResources().getString(R.string.dialog_option_cancel), UI_SettingMenuController.this.getResources().getString(R.string.dialog_option_ok_for_two_button)}, new int[]{12039, 9220});
                } else if (i == 11) {
                    UI_SettingMenuController.this.A();
                    ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(UI_SettingMenuController.this.R, "translationX", 1600.0f, 0.0f);
                    ofFloat2.addListener(new AnimatorListenerAdapter() {
                        /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass1.AnonymousClass2 */

                        public void onAnimationEnd(Animator animator) {
                            super.onAnimationEnd(animator);
                            UI_SettingMenuController.this.P.setVisibility(4);
                            UI_SettingMenuController.this.c((UI_SettingMenuController) false);
                        }
                    });
                    ofFloat2.setDuration(300L);
                    UI_SettingMenuController.this.c((UI_SettingMenuController) true);
                    ofFloat2.start();
                    UI_SettingMenuController.this.R.setVisibility(0);
                }
            }
        });
    }

    private void u() {
        this.w = new ArrayList<>();
        this.w.add(getResources().getString(R.string.device_storage));
        this.w.add(getResources().getString(R.string.memory_card));
        this.x = new ArrayList<>();
        this.x.add(getResources().getString(R.string.video_resolution_1920_960_2_1));
        this.x.add(getResources().getString(R.string.video_resolution_1280_640_2_1));
        this.y = new ArrayList<>();
        this.y.add(getResources().getString(R.string.photo_resolution_5376_2688));
        this.y.add(getResources().getString(R.string.photo_resolution_3840_1920));
        this.z = new ArrayList<>();
        this.z.add(getResources().getString(R.string.live_view_mode_eco));
        this.z.add(getResources().getString(R.string.live_view_mode_hd));
        this.A = new ArrayList<>();
        this.A.add(getResources().getString(R.string.recording_time_short_movie));
        this.A.add(getResources().getString(R.string.recording_time_long_movie));
        this.B = new ArrayList<>();
        this.B.add(getResources().getString(R.string.self_timer_off));
        this.B.add(getResources().getString(R.string.self_timer_2_sec));
        this.B.add(getResources().getString(R.string.self_timer_5_sec));
        this.B.add(getResources().getString(R.string.self_timer_10_sec));
        this.C = new ArrayList<>();
        this.C.add(getResources().getString(R.string.pip_view_on));
        this.C.add(getResources().getString(R.string.pip_view_off));
        this.D = new ArrayList<>();
        this.D.add(getResources().getString(R.string.shutter_sound_on));
        this.D.add(getResources().getString(R.string.shutter_sound_off));
        this.E = new ArrayList<>();
        this.E.add(getResources().getString(R.string.water_mark_on));
        this.E.add(getResources().getString(R.string.water_mark_off));
    }

    private void v() {
        this.ag = new int[]{R.drawable.settings_radio_off, R.drawable.settings_radio_off};
        this.ah = new int[]{R.drawable.settings_radio_off, R.drawable.settings_radio_off};
        this.ai = new int[]{R.drawable.settings_radio_off, R.drawable.settings_radio_off};
        this.aj = new int[]{R.drawable.settings_radio_off, R.drawable.settings_radio_off};
        this.ak = new int[]{R.drawable.settings_radio_off, R.drawable.settings_radio_off};
        this.al = new int[]{R.drawable.settings_radio_off, R.drawable.settings_radio_off, R.drawable.settings_radio_off, R.drawable.settings_radio_off};
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void d(int i2) {
        v();
        this.ap = (LinearLayout) findViewById(R.id.LL_ui_setting_submenu_listview_text);
        this.aq = (LinearLayout) findViewById(R.id.LL_ui_setting_submenu_listview_icon);
        this.S = (LinearLayout) findViewById(R.id.LL_ui_setting_submenu2);
        this.m = (TextView) findViewById(R.id.TV_ui_setting_submenu_title);
        this.m.setText(this.v.get(i2));
        this.X = (ListView) findViewById(R.id.LV_ui_setting_submenu);
        this.K = (ImageButton) findViewById(R.id.IB_ui_setting_submenu_topBack);
        this.K.setOnClickListener(this.aV);
        this.ar = new ArrayList<>();
        switch (i2) {
            case 0:
                this.ar.addAll(Arrays.asList(this.aF));
                this.ag[GeneralFunction.m.a.a()] = R.drawable.settings_radio_on;
                this.r = new e(this, this.ag, this.w, this.ar);
                break;
            case 1:
                this.ar.addAll(Arrays.asList(this.aG));
                this.ah[GeneralFunction.m.a.b()] = R.drawable.settings_radio_on;
                this.r = new e(this, this.ah, this.x, this.ar);
                break;
            case 2:
                this.ar.addAll(Arrays.asList(this.aH));
                this.ai[GeneralFunction.m.a.c()] = R.drawable.settings_radio_on;
                this.r = new e(this, this.ai, this.y, this.ar);
                break;
            case 3:
                this.ar.addAll(Arrays.asList(this.aI));
                this.aj[GeneralFunction.m.a.d()] = R.drawable.settings_radio_on;
                this.r = new e(this, this.aj, this.z, this.ar);
                break;
            case 4:
                this.ar.addAll(Arrays.asList(this.aJ));
                this.ak[GeneralFunction.m.a.e()] = R.drawable.settings_radio_on;
                this.r = new e(this, this.ak, this.A, this.ar);
                break;
            case 5:
                this.ar.addAll(Arrays.asList(this.aK));
                this.al[GeneralFunction.m.a.f()] = R.drawable.settings_radio_on;
                this.r = new e(this, this.al, this.B, this.ar);
                break;
        }
        this.X.setAdapter((ListAdapter) this.r);
        this.X.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass5 */

            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                UI_SettingMenuController.this.d.U = i;
                if (UI_SettingMenuController.this.d.T == 0 && UI_SettingMenuController.this.d.U == 1 && !UI_SettingMenuController.this.d.ad) {
                    UI_SettingMenuController.this.a((UI_SettingMenuController) "MEMORY_CARD is disable", (String) 3);
                    return;
                }
                UI_SettingMenuController.this.r.a(i, view, adapterView);
                UI_SettingMenuController.this.i();
            }
        });
    }

    private void w() {
        this.G = new ArrayList<>();
        this.G.add(getResources().getString(R.string.version_update));
        this.G.add(getResources().getString(R.string.open_source_licenses));
    }

    private void x() {
        this.am = new int[]{R.drawable.settings_arrow, R.drawable.settings_arrow};
    }

    private void y() {
        this.H = new ArrayList<>();
        this.H.add(getResources().getString(R.string.version_update_camera));
    }

    private void z() {
        this.an = new int[]{R.drawable.settings_arrow};
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void A() {
        x();
        this.n = (TextView) findViewById(R.id.TV_ui_setting_about_title);
        this.n.setText(getResources().getString(R.string.about));
        this.Y = (ListView) findViewById(R.id.LV_ui_setting_about);
        this.L = (ImageButton) findViewById(R.id.IB_ui_setting_about_topBack);
        this.L.setOnClickListener(this.aV);
        this.at = (TextView) findViewById(R.id.TV_ui_setting_about_version);
        this.at.setText(F());
        this.ar = new ArrayList<>();
        this.ar.addAll(Arrays.asList(this.aO));
        this.s = new c(this, this.am, this.G, this.ar);
        this.Y.setAdapter((ListAdapter) this.s);
        this.Y.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass6 */

            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                UI_SettingMenuController.this.d.T = i;
                if (UI_SettingMenuController.this.d.T == 0) {
                    UI_SettingMenuController.this.C();
                    UI_SettingMenuController.this.T.setVisibility(0);
                } else if (UI_SettingMenuController.this.d.T == 1) {
                    UI_SettingMenuController.this.U.setVisibility(0);
                }
            }
        });
        findViewById(R.id.LL_ui_about_footer).setVisibility(0);
        B();
    }

    private void B() {
        Spanned fromHtml;
        TextView textView = (TextView) findViewById(R.id.TV_ui_about_footer);
        if (Build.VERSION.SDK_INT >= 24) {
            fromHtml = Html.fromHtml(getResources().getString(R.string.about_footer), 0);
        } else {
            fromHtml = Html.fromHtml(getResources().getString(R.string.about_footer));
        }
        textView.setText(fromHtml);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        if (fromHtml instanceof Spannable) {
            URLSpan[] uRLSpanArr = (URLSpan[]) fromHtml.getSpans(0, fromHtml.length(), URLSpan.class);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(fromHtml);
            spannableStringBuilder.clearSpans();
            for (URLSpan uRLSpan : uRLSpanArr) {
                spannableStringBuilder.setSpan(new a(uRLSpan.getURL()), fromHtml.getSpanStart(uRLSpan), fromHtml.getSpanEnd(uRLSpan), 33);
            }
            textView.setText(spannableStringBuilder);
        }
    }

    /* access modifiers changed from: private */
    public class a extends ClickableSpan {
        private String b;

        public a(String str) {
            this.b = str;
        }

        public void updateDrawState(TextPaint textPaint) {
            super.updateDrawState(textPaint);
            textPaint.setUnderlineText(false);
        }

        public void onClick(View view) {
            UI_SettingMenuController.this.c((UI_SettingMenuController) this.b);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void c(String str) {
        String string;
        String string2;
        char c2 = 65535;
        switch (str.hashCode()) {
            case 48:
                if (str.equals("0")) {
                    c2 = 0;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                string = getResources().getString(R.string.agreement_page_title);
                string2 = getResources().getString(R.string.user_agreement_url);
                break;
            default:
                string = null;
                string2 = null;
                break;
        }
        a(string, string2);
    }

    private void a(String str, String str2) {
        Intent intent = new Intent(this, UI_SettingInformation.class);
        intent.putExtra("url", str2);
        intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, str);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void C() {
        z();
        this.o = (TextView) findViewById(R.id.TV_ui_setting_update_title);
        this.o.setText(getResources().getString(R.string.version_update));
        this.Z = (ListView) findViewById(R.id.LV_ui_setting_update);
        this.M = (ImageButton) findViewById(R.id.IB_ui_setting_update_topBack);
        this.M.setOnClickListener(this.aV);
        this.ar = new ArrayList<>();
        this.ar.addAll(Arrays.asList(this.aP));
        this.t = new f(this, this.an, this.H, this.ar);
        this.Z.setAdapter((ListAdapter) this.t);
        this.Z.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass7 */

            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                UI_SettingMenuController.this.d.T = i;
                if (UI_SettingMenuController.this.d.T != 0 || !UI_SettingMenuController.this.c.d.b()) {
                    return;
                }
                if (UI_SettingMenuController.this.d.r) {
                    o.a((Context) UI_SettingMenuController.this, true, false, UI_SettingMenuController.this.getResources().getString(R.string.camera_update_for_newer_camera_program), UI_SettingMenuController.this.getResources().getString(R.string.camera_update_content), new String[]{UI_SettingMenuController.this.getResources().getString(R.string.dialog_option_later), UI_SettingMenuController.this.getResources().getString(R.string.dialog_option_update)}, new int[]{12039, 9257});
                    return;
                }
                o.a((Context) UI_SettingMenuController.this, true, true, UI_SettingMenuController.this.getResources().getString(R.string.camera_update), UI_SettingMenuController.this.getResources().getString(R.string.camera_update_check), UI_SettingMenuController.this.getResources().getString(R.string.dialog_option_ok), 12039);
            }
        });
    }

    private void D() {
        this.au = (WebView) findViewById(R.id.WV_ui_setting_OpensourceLicense_content);
        WebSettings settings = this.au.getSettings();
        settings.setAllowFileAccess(false);
        settings.setJavaScriptEnabled(false);
        settings.setAllowFileAccessFromFileURLs(false);
        settings.setAllowUniversalAccessFromFileURLs(false);
        this.au.loadUrl("file:///android_asset/OpenSourceLicense.htm");
        this.au.setVerticalScrollBarEnabled(false);
    }

    private String E() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public d d() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public class d extends BaseAdapter {
        private int[] b;
        private List<String> c;
        private List<Integer> d;
        private boolean[] e;
        private boolean f;

        public d(Context context, int[] iArr, List<String> list, String[] strArr, List<Integer> list2, boolean z) {
            this.c = list;
            this.b = iArr;
            this.d = list2;
            this.f = z;
            this.e = new boolean[list.size()];
            for (int i = 0; i < list.size(); i++) {
                this.e[i] = true;
            }
        }

        public void a(boolean z) {
            this.f = z;
        }

        public void a() {
            notifyDataSetChanged();
        }

        public void b() {
            this.b[6] = R.drawable.settings_toggle_off;
            this.b[7] = R.drawable.settings_toggle_on;
            this.b[8] = R.drawable.settings_toggle_off;
            notifyDataSetChanged();
        }

        public void a(int i, View view, int i2) {
            if (i2 == 1) {
                this.b[i] = R.drawable.settings_toggle_off;
            } else {
                this.b[i] = R.drawable.settings_toggle_on;
            }
        }

        public void a(int i, View view, int i2, boolean z) {
            if (!z) {
                a(i, view, i2);
            } else if (i2 == 1) {
                this.b[i] = R.drawable.settings_toggle_on;
            } else {
                this.b[i] = R.drawable.settings_toggle_off;
            }
        }

        public int getCount() {
            return this.d.size();
        }

        public Object getItem(int i) {
            return this.c.get((int) getItemId(i));
        }

        public long getItemId(int i) {
            return (long) this.d.get(i).intValue();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (UI_SettingMenuController.this.ab[i].booleanValue()) {
                View inflate = LayoutInflater.from(UI_SettingMenuController.this.getApplicationContext()).inflate(R.layout.setting_menu, (ViewGroup) null);
                ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_menu_listview_text)).getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
                ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_menu_listview_icon)).getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
                ((ImageView) inflate.findViewById(R.id.setting_menu_list_icon)).setImageResource(this.b[i]);
                TextView textView = (TextView) inflate.findViewById(R.id.textTitle);
                textView.setText((CharSequence) UI_SettingMenuController.this.v.get(i));
                TextView textView2 = (TextView) inflate.findViewById(R.id.textDetail);
                textView2.setText(UI_SettingMenuController.this.ae[i]);
                if (!this.f) {
                    if (UI_SettingMenuController.this.ac[i].booleanValue()) {
                        textView.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.gray2B));
                        textView2.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.gray88));
                    } else {
                        textView.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.grayC0));
                        textView2.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.grayC0));
                    }
                }
                return inflate;
            }
            View inflate2 = LayoutInflater.from(UI_SettingMenuController.this.getApplicationContext()).inflate(R.layout.setting_menu_2, (ViewGroup) null);
            ((LinearLayout) inflate2.findViewById(R.id.LL_ui_setting_menu2_listview_text)).getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
            ((LinearLayout) inflate2.findViewById(R.id.LL_ui_setting_menu2_listview_icon)).getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
            TextView textView3 = (TextView) inflate2.findViewById(R.id.textTitle);
            textView3.setText((CharSequence) UI_SettingMenuController.this.v.get(i));
            if (!this.f) {
                if (UI_SettingMenuController.this.ac[i].booleanValue()) {
                    textView3.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.gray2B));
                    if (this.b[i] == R.drawable.settings_toggle_on_disable) {
                        this.b[i] = R.drawable.settings_toggle_on;
                    } else if (this.b[i] == R.drawable.settings_toggle_off_disable) {
                        this.b[i] = R.drawable.settings_toggle_off;
                    }
                } else {
                    textView3.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.grayC0));
                    if (this.b[i] == R.drawable.settings_toggle_on) {
                        this.b[i] = R.drawable.settings_toggle_on_disable;
                    } else if (this.b[i] == R.drawable.settings_toggle_off) {
                        this.b[i] = R.drawable.settings_toggle_off_disable;
                    }
                }
            }
            ((ImageView) inflate2.findViewById(R.id.setting_menu_list_icon)).setImageResource(this.b[i]);
            if (i == 11) {
                ImageView imageView = (ImageView) inflate2.findViewById(R.id.IV_setting_menu2_reddot);
                imageView.setImageResource(R.drawable.settings_about_reddot);
                if (UI_SettingMenuController.this.d.n || UI_SettingMenuController.this.d.r) {
                    imageView.setVisibility(0);
                    return inflate2;
                }
                imageView.setVisibility(4);
            }
            return inflate2;
        }
    }

    /* access modifiers changed from: protected */
    public class e extends BaseAdapter {
        private int[] b;
        private List<String> c;
        private List<Integer> d;
        private boolean[] e;

        public e(Context context, int[] iArr, List<String> list, List<Integer> list2) {
            this.c = list;
            this.b = iArr;
            this.d = list2;
            this.e = new boolean[list.size()];
            for (int i = 0; i < list.size(); i++) {
                this.e[i] = true;
            }
        }

        public void a() {
            notifyDataSetChanged();
        }

        public void a(int i, View view, ViewGroup viewGroup) {
            for (int i2 = 0; i2 < getCount(); i2++) {
                if (i2 == i) {
                    ((ImageView) view.findViewById(R.id.setting_submenu_list_icon)).setImageResource(R.drawable.settings_radio_on);
                } else {
                    ((ImageView) viewGroup.getChildAt(i2).findViewById(R.id.setting_submenu_list_icon)).setImageResource(R.drawable.settings_radio_off);
                }
            }
        }

        public int getCount() {
            return this.d.size();
        }

        public Object getItem(int i) {
            return this.c.get((int) getItemId(i));
        }

        public long getItemId(int i) {
            return (long) this.d.get(i).intValue();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = LayoutInflater.from(UI_SettingMenuController.this.getApplicationContext()).inflate(R.layout.setting_submenu, (ViewGroup) null);
            ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_submenu_listview_text)).getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
            ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_submenu_listview_icon)).getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
            TextView textView = (TextView) inflate.findViewById(R.id.textDetail);
            textView.setText(this.c.get(i));
            if (this.c.get(i).equals(UI_SettingMenuController.this.getString(R.string.memory_card))) {
                if (!UI_SettingMenuController.this.d.ad) {
                    textView.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.grayC0));
                } else {
                    textView.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.gray2B));
                }
            }
            ((ImageView) inflate.findViewById(R.id.setting_submenu_list_icon)).setImageResource(this.b[i]);
            return inflate;
        }
    }

    /* access modifiers changed from: protected */
    public class c extends BaseAdapter {
        private int[] b;
        private List<String> c;
        private List<Integer> d;
        private boolean[] e;

        public c(Context context, int[] iArr, List<String> list, List<Integer> list2) {
            this.c = list;
            this.b = iArr;
            this.d = list2;
            this.e = new boolean[list.size()];
            for (int i = 0; i < list.size(); i++) {
                this.e[i] = true;
            }
        }

        public int getCount() {
            return this.d.size();
        }

        public Object getItem(int i) {
            return this.c.get((int) getItemId(i));
        }

        public long getItemId(int i) {
            return (long) this.d.get(i).intValue();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = LayoutInflater.from(UI_SettingMenuController.this.getApplicationContext()).inflate(R.layout.setting_about, (ViewGroup) null);
            LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.LL_ui_setting_about_listview_text);
            linearLayout.getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
            LinearLayout linearLayout2 = (LinearLayout) inflate.findViewById(R.id.LL_ui_setting_about_listview_icon);
            linearLayout2.getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(0, UI_SettingMenuController.this.i / UI_SettingMenuController.this.b, 600.0f));
            linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(0, UI_SettingMenuController.this.i / UI_SettingMenuController.this.b, 300.0f));
            ((TextView) inflate.findViewById(R.id.textTitle)).setText(this.c.get(i));
            ((ImageView) inflate.findViewById(R.id.setting_about_list_icon)).setImageResource(this.b[i]);
            if (i == 0) {
                ImageView imageView = (ImageView) inflate.findViewById(R.id.IV_setting_about_update);
                imageView.setImageResource(R.drawable.settings_about_reddot);
                if (UI_SettingMenuController.this.d.n || UI_SettingMenuController.this.d.r) {
                    imageView.setVisibility(0);
                } else {
                    imageView.setVisibility(4);
                }
            }
            return inflate;
        }
    }

    /* access modifiers changed from: protected */
    public class f extends BaseAdapter {
        private int[] b;
        private List<String> c;
        private List<Integer> d;
        private boolean[] e;

        public f(Context context, int[] iArr, List<String> list, List<Integer> list2) {
            this.c = list;
            this.b = iArr;
            this.d = list2;
            this.e = new boolean[list.size()];
            for (int i = 0; i < list.size(); i++) {
                this.e[i] = true;
            }
        }

        public void a() {
            notifyDataSetChanged();
        }

        public int getCount() {
            return this.d.size();
        }

        public Object getItem(int i) {
            return this.c.get((int) getItemId(i));
        }

        public long getItemId(int i) {
            return (long) this.d.get(i).intValue();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = LayoutInflater.from(UI_SettingMenuController.this.getApplicationContext()).inflate(R.layout.setting_update, (ViewGroup) null);
            ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_update_listview_text)).getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
            ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_update_listview_icon)).getLayoutParams().height = UI_SettingMenuController.this.i / UI_SettingMenuController.this.b;
            TextView textView = (TextView) inflate.findViewById(R.id.textTitle);
            textView.setText(this.c.get(i));
            textView.setMaxWidth(UI_SettingMenuController.this.h / 3);
            ((ImageView) inflate.findViewById(R.id.setting_update_list_icon)).setImageResource(this.b[i]);
            ImageView imageView = (ImageView) inflate.findViewById(R.id.IV_setting_update_reddot);
            imageView.setImageResource(R.drawable.settings_about_reddot);
            if (i == 0) {
                TextView textView2 = (TextView) inflate.findViewById(R.id.textDetail);
                if (UI_SettingMenuController.this.c.d.b()) {
                    textView.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.gray2B));
                    textView2.setText(UI_SettingMenuController.this.c.c.g.c);
                    if (UI_SettingMenuController.this.d.r) {
                        imageView.setVisibility(0);
                    } else {
                        imageView.setVisibility(4);
                    }
                } else {
                    textView.setTextColor(UI_SettingMenuController.this.getResources().getColor(R.color.grayC0));
                    textView2.setText("");
                }
            }
            return inflate;
        }
    }

    protected class b extends BaseAdapter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ UI_SettingMenuController f1513a;
        private int[] b;
        private List<String> c;
        private List<Integer> d;

        public int getCount() {
            return this.d.size();
        }

        public Object getItem(int i) {
            return this.c.get((int) getItemId(i));
        }

        public long getItemId(int i) {
            return (long) this.d.get(i).intValue();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = LayoutInflater.from(this.f1513a.getApplicationContext()).inflate(R.layout.setting_update, (ViewGroup) null);
            ViewGroup.LayoutParams layoutParams = ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_update_listview_text)).getLayoutParams();
            layoutParams.height = this.f1513a.i / this.f1513a.b;
            layoutParams.width = (this.f1513a.h * 800) / IMediaPlayer.MEDIA_INFO_TIMED_TEXT_ERROR;
            ViewGroup.LayoutParams layoutParams2 = ((LinearLayout) inflate.findViewById(R.id.LL_ui_setting_update_listview_icon)).getLayoutParams();
            layoutParams2.height = this.f1513a.i / this.f1513a.b;
            layoutParams2.width = (this.f1513a.h * 100) / IMediaPlayer.MEDIA_INFO_TIMED_TEXT_ERROR;
            int a2 = GeneralFunction.k.a(this.f1513a, (float) (this.f1513a.h / 27));
            TextView textView = (TextView) inflate.findViewById(R.id.textTitle);
            textView.setText(this.c.get(i));
            textView.setTextSize((float) a2);
            ((ImageView) inflate.findViewById(R.id.setting_update_list_icon)).setImageResource(this.b[i]);
            return inflate;
        }
    }

    private String F() {
        return "v" + E();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        GeneralFunction.d.a("UI_SettingMenuController", str, i2);
    }

    @Override // GeneralFunction.a.a
    public void a(int i2, long j2) {
        if (this.c != null) {
            this.c.a(new a.c.a(i2), j2);
        } else {
            a("SendUiMsg error!! Msg=" + i2, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(a.c.a aVar, long j2) {
        if (this.c != null) {
            this.c.a(aVar, j2);
        } else {
            a("SendUiMsg error!! MsgEx=" + aVar.b().what, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(int i2, long j2) {
        if (this.c != null) {
            this.c.b(i2, j2);
        } else {
            a("UI_SendAidMsg error!!", 0);
        }
    }

    @Override // GeneralFunction.a.a
    public boolean b() {
        if (this.j == 2) {
            return true;
        }
        return false;
    }

    public void onBackPressed() {
        if (!this.d.Y) {
            if (this.P.isShown()) {
                this.c.a(new a.c.a(8448));
            } else if (this.aS != null && this.aS.isShown()) {
            } else {
                if (this.U.isShown()) {
                    this.U.setVisibility(4);
                    this.R.setVisibility(4);
                    this.Q.setVisibility(4);
                    this.P.setVisibility(4);
                    this.R.setVisibility(0);
                } else if (this.V.isShown()) {
                    this.V.setVisibility(4);
                    this.R.setVisibility(0);
                } else if (this.Q.isShown()) {
                    this.Q.setVisibility(4);
                    this.P.setVisibility(0);
                } else if (this.T.isShown()) {
                    this.T.setVisibility(4);
                    this.R.setVisibility(0);
                    this.P.setVisibility(4);
                } else if (this.R.isShown()) {
                    this.R.setVisibility(4);
                    this.Q.setVisibility(4);
                    this.P.setVisibility(0);
                }
            }
        }
    }

    @Override // GeneralFunction.a.a
    public void b(Message message) {
        a("AidServer_Handler:0x" + Integer.toHexString(message.what), 3);
        this.k.c(message);
    }

    /* access modifiers changed from: protected */
    public void e() {
        SharedPreferences.Editor b2 = GeneralFunction.n.a.b(this.c);
        b2.putInt("storeLocation", GeneralFunction.m.a.a()).apply();
        b2.putInt("videoResolution", GeneralFunction.m.a.b()).apply();
        b2.putInt("photoResolution", GeneralFunction.m.a.c()).apply();
        b2.putInt("liveViewMode", GeneralFunction.m.a.d()).apply();
        b2.putInt("recordingTime", GeneralFunction.m.a.e()).apply();
        b2.putInt("getSelfTimer", GeneralFunction.m.a.f()).apply();
        b2.putInt("pipView", GeneralFunction.m.a.g()).apply();
        b2.putInt("shutterSound", GeneralFunction.m.a.h()).apply();
        b2.putInt("waterMark", GeneralFunction.m.a.i()).apply();
    }

    private void G() {
        GeneralFunction.m.a.a(GeneralFunction.n.a.a(this.c).getInt("storeLocation", 0));
        GeneralFunction.m.a.b(GeneralFunction.n.a.a(this.c).getInt("videoResolution", 0));
        GeneralFunction.m.a.c(GeneralFunction.n.a.a(this.c).getInt("photoResolution", 0));
        GeneralFunction.m.a.d(GeneralFunction.n.a.a(this.c).getInt("liveViewMode", 0));
        GeneralFunction.m.a.e(GeneralFunction.n.a.a(this.c).getInt("recordingTime", 0));
        GeneralFunction.m.a.f(GeneralFunction.n.a.a(this.c).getInt("getSelfTimer", 0));
        GeneralFunction.m.a.g(GeneralFunction.n.a.a(this.c).getInt("pipView", 1));
        GeneralFunction.m.a.h(GeneralFunction.n.a.a(this.c).getInt("shutterSound", 0));
        GeneralFunction.m.a.i(GeneralFunction.n.a.a(this.c).getInt("waterMark", 0));
        if (GeneralFunction.m.a.a() == 1 && o() == null) {
            GeneralFunction.m.a.a(0);
        }
        this.d.n = GeneralFunction.n.a.a(this.c).getBoolean("haveNewApkVersion", false);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (this.aw.getVisibility() == 0) {
            this.ay.setText(str);
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2, String str) {
        this.ax.setImageResource(R.drawable.anim_system_busy);
        if (z2) {
            this.d.Y = true;
            this.ay.setText(str);
            this.aw.setVisibility(0);
            ((AnimationDrawable) this.ax.getDrawable()).start();
            return;
        }
        this.d.Y = false;
        this.aw.setVisibility(4);
        ((AnimationDrawable) this.ax.getDrawable()).stop();
    }

    private void H() {
        this.aw = (LinearLayout) findViewById(R.id.LL_ui_Setting_BusyLayout);
        this.ax = (ImageView) findViewById(R.id.IV_ui_Setting_BusyIcon);
        this.ay = (TextView) findViewById(R.id.TV_ui_Setting_BusyText);
        this.aw.setVisibility(4);
        this.aw.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass8 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        this.aA.setImageResource(R.drawable.anim_system_busy);
        if (z2) {
            this.d.Y = true;
            this.aC.setText("0%");
            this.az.setVisibility(0);
            ((AnimationDrawable) this.aA.getDrawable()).start();
            return;
        }
        this.d.Y = false;
        this.az.setVisibility(4);
        this.aC.setText("0%");
        ((AnimationDrawable) this.aA.getDrawable()).stop();
    }

    private void I() {
        this.az = (LinearLayout) findViewById(R.id.LL_ui_Setting_AppUpdate_BusyLayout);
        this.aA = (ImageView) findViewById(R.id.IV_ui_Setting_AppUpdate_BusyIcon);
        this.aB = (TextView) findViewById(R.id.TV_ui_Setting_AppUpdate_BusyText);
        this.aC = (TextView) findViewById(R.id.TV_ui_Setting_AppUpdate_ProgressText);
        this.aD = (Button) findViewById(R.id.B_ui_Setting_AppUpdate_Cancel);
        this.az.setVisibility(4);
        this.aB.setText(getResources().getString(R.string.app_update_busy_string));
        this.aC.setText("0%");
        this.az.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass9 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.aD.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass2 */

            public void onClick(View view) {
                UI_SettingMenuController.this.a(9244);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a("onCreate", 3);
        this.c = (UI_ModeMain) getApplication();
        this.c.v();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.h = displayMetrics.widthPixels;
        this.i = displayMetrics.heightPixels;
        setContentView(R.layout.ui_setting_menu);
        r();
        this.k = new a(this);
        this.d = this.c.c.l;
        this.c.a(2064, this);
        L();
        G();
        h();
        t();
        H();
        I();
        this.c.c.k.b.ab = o();
        a("onCreate szSdCardPath = " + this.c.c.k.b.ab, 1);
        if (this.c.c.k.b.ab != null) {
            this.d.ad = true;
        } else {
            this.d.ad = false;
            K();
        }
        if (this.c.c.l.o) {
            J();
            a(9253);
            this.c.c.l.o = false;
        }
        if (this.c.c.l.s) {
            J();
            q();
            this.c.c.l.s = false;
        }
        O();
    }

    /* access modifiers changed from: protected */
    public void f() {
        this.d.ad = false;
        this.c.c.k.b.x = true;
        this.c.c.k.b.q = new GeneralFunction.a(getApplicationContext(), true, getResources().getString(R.string.sdcard_removed), 2000);
        K();
        if (this.Q.isShown() && this.d.T == 0) {
            this.ag[0] = R.drawable.settings_radio_on;
            this.ag[1] = R.drawable.settings_radio_off;
            this.r.a();
        }
        this.q.a();
    }

    /* access modifiers changed from: protected */
    public void g() {
        this.d.ad = true;
        if (o() != null) {
            this.c.c.k.b.ab = o();
        }
        if (this.Q.isShown() && this.d.T == 0) {
            this.r.a();
        }
    }

    private void J() {
        A();
        C();
        this.P.setVisibility(4);
        this.R.setVisibility(4);
        this.T.setVisibility(0);
    }

    private void K() {
        GeneralFunction.m.a.a(0);
        this.ae[0] = this.w.get(0);
    }

    public void c(int i2) {
        GeneralFunction.d.a(this, i2);
    }

    private void L() {
        this.e = this.c.c.h;
        if (this.e.ad) {
            if (this.e.ac == 9) {
                c(9);
            } else {
                c(1);
            }
        } else if (this.c.d.b()) {
            c(9);
        } else if (this.c.c.k.k == 9) {
            c(9);
        } else {
            c(1);
        }
    }

    public void h() {
        this.Q = (LinearLayout) findViewById(R.id.LL_ui_setting_submenu);
        this.R = (LinearLayout) findViewById(R.id.LL_ui_setting_about);
        this.T = (LinearLayout) findViewById(R.id.LL_ui_setting_update);
        this.U = (LinearLayout) findViewById(R.id.LL_ui_setting_OpensourceLicense);
        this.V = (LinearLayout) findViewById(R.id.LL_ui_setting_LegalInformation_submenu);
        this.N = (ImageButton) findViewById(R.id.IB_ui_setting_OpensourceLicense_topBack);
        this.N.setOnClickListener(this.aV);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onResume() {
        super.onResume();
        a.c.a aVar = new a.c.a(12034);
        aVar.a("mode", 2048);
        a(aVar, 0);
    }

    @Override // GeneralFunction.a.a
    public void onPause() {
        super.onPause();
        e();
        a.c.a aVar = new a.c.a(12035);
        aVar.a("mode", 2048);
        a(aVar, 0);
    }

    @Override // GeneralFunction.a.a
    public void onStop() {
        super.onStop();
        a.c.a aVar = new a.c.a(12036);
        aVar.a("mode", 2048);
        a(aVar, 0);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onDestroy() {
        super.onDestroy();
        a.c.a aVar = new a.c.a(12037);
        aVar.a("mode", 2048);
        a(aVar, 0);
    }

    @Override // GeneralFunction.a.a
    public void a(Message message) {
        this.k.a(message);
    }

    /* access modifiers changed from: protected */
    public void i() {
        a("nSelectOption: " + this.d.T + "  lSubSelectOption: " + this.d.U, 3);
        switch (this.d.T) {
            case 0:
                this.ae[this.d.T] = this.w.get(this.d.U);
                for (int i2 = 0; i2 < this.ag.length; i2++) {
                    if (i2 == this.d.U) {
                        this.ag[i2] = R.drawable.settings_radio_on;
                    } else {
                        this.ag[i2] = R.drawable.settings_radio_off;
                    }
                }
                if (this.d.U == 1) {
                    this.Q.setVisibility(4);
                    this.P.setVisibility(0);
                    GeneralFunction.m.a.a(this.d.U);
                    break;
                } else {
                    this.Q.setVisibility(4);
                    this.P.setVisibility(0);
                    GeneralFunction.m.a.a(this.d.U);
                    break;
                }
            case 1:
                GeneralFunction.m.a.b(this.d.U);
                GeneralFunction.m.a.G(a.c.b.f(this.d.U));
                this.ae[this.d.T] = this.x.get(this.d.U);
                for (int i3 = 0; i3 < this.ah.length; i3++) {
                    if (i3 == this.d.U) {
                        this.ah[i3] = R.drawable.settings_radio_on;
                    } else {
                        this.ah[i3] = R.drawable.settings_radio_off;
                    }
                }
                this.Q.setVisibility(4);
                this.P.setVisibility(0);
                break;
            case 2:
                GeneralFunction.m.a.c(this.d.U);
                GeneralFunction.m.a.F(a.c.b.e(this.d.U));
                this.ae[this.d.T] = this.y.get(this.d.U);
                for (int i4 = 0; i4 < this.ai.length; i4++) {
                    if (i4 == this.d.U) {
                        this.ai[i4] = R.drawable.settings_radio_on;
                    } else {
                        this.ai[i4] = R.drawable.settings_radio_off;
                    }
                }
                this.Q.setVisibility(4);
                this.P.setVisibility(0);
                break;
            case 3:
                GeneralFunction.m.a.d(this.d.U);
                this.ae[this.d.T] = this.z.get(this.d.U);
                for (int i5 = 0; i5 < this.aj.length; i5++) {
                    if (i5 == this.d.U) {
                        this.aj[i5] = R.drawable.settings_radio_on;
                    } else {
                        this.aj[i5] = R.drawable.settings_radio_off;
                    }
                }
                this.Q.setVisibility(4);
                this.P.setVisibility(0);
                break;
            case 4:
                GeneralFunction.m.a.e(this.d.U);
                this.ae[this.d.T] = this.A.get(this.d.U);
                for (int i6 = 0; i6 < this.ak.length; i6++) {
                    if (i6 == this.d.U) {
                        this.ak[i6] = R.drawable.settings_radio_on;
                    } else {
                        this.ak[i6] = R.drawable.settings_radio_off;
                    }
                }
                this.Q.setVisibility(4);
                this.P.setVisibility(0);
                break;
            case 5:
                GeneralFunction.m.a.f(this.d.U);
                this.ae[this.d.T] = this.B.get(this.d.U);
                for (int i7 = 0; i7 < this.al.length; i7++) {
                    if (i7 == this.d.U) {
                        this.al[i7] = R.drawable.settings_radio_on;
                    } else {
                        this.al[i7] = R.drawable.settings_radio_off;
                    }
                }
                this.Q.setVisibility(4);
                this.P.setVisibility(0);
                break;
            case 6:
                GeneralFunction.m.a.g(this.d.U);
                this.ae[this.d.T] = this.C.get(this.d.U);
                break;
            case 7:
                GeneralFunction.m.a.h(this.d.U);
                GeneralFunction.m.a.H(a.c.b.j(this.d.U));
                this.ae[this.d.T] = this.D.get(this.d.U);
                break;
            case 8:
                GeneralFunction.m.a.i(this.d.U);
                this.ae[this.d.T] = this.E.get(this.d.U);
                break;
        }
        this.q.a();
        if (this.r != null) {
            this.r.a();
        }
    }

    public void j() {
        this.ae = new String[this.v.size()];
        this.ae[0] = this.w.get(GeneralFunction.m.a.a());
        this.ae[1] = this.x.get(GeneralFunction.m.a.b());
        this.ae[2] = this.y.get(GeneralFunction.m.a.c());
        this.ae[3] = this.z.get(GeneralFunction.m.a.d());
        this.ae[4] = this.A.get(GeneralFunction.m.a.e());
        this.ae[5] = this.B.get(GeneralFunction.m.a.f());
        this.ae[6] = this.C.get(GeneralFunction.m.a.g());
        this.ae[7] = this.D.get(GeneralFunction.m.a.h());
        this.ae[8] = this.E.get(GeneralFunction.m.a.i());
        this.ae[9] = "";
        this.ae[10] = "";
        this.ae[11] = "";
    }

    public void k() {
        GeneralFunction.m.a.a(0);
        GeneralFunction.m.a.b(0);
        GeneralFunction.m.a.c(0);
        GeneralFunction.m.a.d(0);
        GeneralFunction.m.a.e(0);
        GeneralFunction.m.a.f(0);
        GeneralFunction.m.a.g(1);
        GeneralFunction.m.a.h(0);
        GeneralFunction.m.a.i(0);
        GeneralFunction.m.a.m(6);
        GeneralFunction.m.a.n(0);
        GeneralFunction.m.a.o(0);
        GeneralFunction.m.a.j(1);
        this.c.c.h.aQ = "";
        this.c.c.h.aR = "";
        this.c.c.h.aS = "";
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        switch (i2) {
            case 42:
                if (intent != null) {
                    this.c.c.k.b.aa = intent.getData();
                    a("objTreeUri: " + this.c.c.k.b.aa.toString(), 3);
                    getContentResolver().takePersistableUriPermission(this.c.c.k.b.aa, 3);
                    p();
                    return;
                }
                a(9237);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3) {
        if (i2 < 0 || i3 <= 0 || i2 > i3) {
            a("Invalid progress percentage! (" + i2 + "/" + i3 + ")", 0);
            return;
        }
        long j2 = (((long) i2) * 100) / ((long) i3);
        if (j2 <= 100) {
            this.aC.setText(Long.toString(j2) + "%");
            return;
        }
        a("Invalid percentage result: " + j2, 0);
    }

    /* access modifiers changed from: protected */
    public int l() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public void m() {
        if (this.Q.isShown()) {
            this.Q.setVisibility(4);
            this.P.setVisibility(0);
        } else if (this.T.isShown()) {
            this.t.a();
            if (this.c.c.h.ak != 0) {
                o.a((Context) this, true, true, getResources().getString(R.string.camera_update_for_update_failed), getResources().getString(R.string.camera_update_fail), getResources().getString(R.string.dialog_option_ok), 12039);
                this.c.c.h.ak = 0;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(boolean z2) {
        this.q.a(z2);
        this.q.a();
    }

    /* access modifiers changed from: protected */
    public void n() {
        Intent intent = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
        intent.addFlags(2);
        startActivityForResult(intent, 42);
    }

    private boolean M() {
        a("sLocalGallery.szSdCardPath" + this.c.c.k.b.ab, 3);
        if (new File(this.c.c.k.b.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/").exists()) {
            return true;
        }
        a("SD CV60 not exists", 3);
        return false;
    }

    private boolean N() {
        if (new File(this.c.c.k.b.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM").exists()) {
            return true;
        }
        a("SD DCIM not exists", 3);
        return false;
    }

    /* access modifiers changed from: protected */
    public String o() {
        return GeneralFunction.d.a(this.as);
    }

    private void d(String str) {
        a("CreateSDcardFolder" + str, 2);
        new File(str).mkdirs();
    }

    /* access modifiers changed from: protected */
    public void p() {
        a("checkFolder", 3);
        if (this.c.c.k.b.ab == null) {
            this.c.c.k.b.ab = o();
        }
        if (this.c.c.k.b.ab != null) {
            if (N()) {
                d(this.c.c.k.b.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM");
                d(this.c.c.k.b.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/");
                SharedPreferences.Editor b2 = GeneralFunction.n.a.b(this.c);
                b2.putString("objTreeUri", this.c.c.k.b.ab).apply();
                String str = this.c.c.k.b.ab;
                this.c.c.k.b.ac = str.substring(str.lastIndexOf("/") + 1, str.lastIndexOf("/") + 10);
                b2.putString("szSDCardId", this.c.c.k.b.ac).apply();
            } else if (!M()) {
                d(this.c.c.k.b.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/");
            }
            this.Q.setVisibility(4);
            this.P.setVisibility(0);
            GeneralFunction.m.a.a(this.d.U);
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
        if (this.c.c.h.ak != 0) {
            a("Warning: restart FW upgrade while lCurrentDataPartId = " + this.c.c.h.ak, 0);
        }
        if (this.c.c.g.c.compareTo("v1.3A00") < 0 || this.c.c.g.c.compareTo("v1.5000") == 0 || this.c.c.g.c.compareTo("v1.5100") == 0) {
            this.c.c.l.u = false;
        } else {
            this.c.c.l.u = true;
        }
        this.c.c.h.ak = 1;
        this.c.c(new a.c.a(18442));
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        try {
            this.c.c.h.af = new FileInputStream(str);
            this.c.c.h.ag = this.c.c.h.af.available();
            a("fileSize " + this.c.c.h.ag, 3);
            this.c.c.h.aj = this.c.c.h.ag / this.c.c.h.ah;
            if (this.c.c.h.ag % this.c.c.h.ah > 0) {
                this.c.c.h.aj++;
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a(FileInputStream fileInputStream) {
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void O() {
        this.aU = (LinearLayout) findViewById(R.id.LL_ui_Setting_protect_layout);
        this.aU.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Setting.UI_SettingMenuController.AnonymousClass3 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void c(boolean z2) {
        if (z2) {
            this.aU.setVisibility(0);
        } else {
            this.aU.setVisibility(4);
        }
    }
}
