package ui_Controller.ui_Setting;

import GeneralFunction.c;
import GeneralFunction.d;
import GeneralFunction.k;
import GeneralFunction.o;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import java.util.ArrayList;
import ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController;
import ui_Controller.ui_Liveview.UI_LiveViewController;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class UI_SettingUserInstructions extends GeneralFunction.a.a {
    protected UI_ModeMain b = null;
    private ViewPager c;
    private ArrayList<View> d;
    private ImageView e;
    private ImageView[] f;
    private ViewGroup g;
    private ViewGroup h;
    private TextView i = null;
    private TextView j = null;
    private TextView k = null;
    private TextView l = null;
    private TextView m = null;
    private TextView n = null;
    private ImageButton o = null;
    private ImageButton p = null;
    private OrientationEventListener q = null;
    private int r = -1;
    private Context s = this;
    private int t = 0;
    private int u = 0;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Log.e("UserInstructions", "onCreate");
        this.b = (UI_ModeMain) getApplication();
        this.b.v();
        this.b.a(4128, this);
        requestWindowFeature(1);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.t = displayMetrics.widthPixels;
        this.u = displayMetrics.heightPixels;
        e();
        c();
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onResume() {
        super.onResume();
        this.q.enable();
        a.c.a aVar = new a.c.a(12034);
        aVar.a("mode", 4128);
        a(aVar, 0);
    }

    @Override // GeneralFunction.a.a
    public void onPause() {
        super.onPause();
        this.q.disable();
    }

    public void onBackPressed() {
    }

    private void c() {
        int a2 = k.a(this, (float) (Math.min(this.t, this.u) / 21));
        LayoutInflater layoutInflater = getLayoutInflater();
        this.d = new ArrayList<>();
        View inflate = layoutInflater.inflate(R.layout.setting_instruction_page_1, (ViewGroup) null);
        this.i = (TextView) inflate.findViewById(R.id.TV_user_instruction_detail_page1);
        this.i.setTextSize((float) new c(a2).f((int) (((double) a2) * 0.9d)).a());
        this.d.add(inflate);
        View inflate2 = layoutInflater.inflate(R.layout.setting_instruction_page_2, (ViewGroup) null);
        this.j = (TextView) inflate2.findViewById(R.id.TV_user_instruction_detail_page2);
        this.j.setTextSize((float) new c(a2).n((int) (((double) a2) * 0.9d)).a());
        this.d.add(inflate2);
        View inflate3 = layoutInflater.inflate(R.layout.setting_instruction_page_3, (ViewGroup) null);
        this.k = (TextView) inflate3.findViewById(R.id.TV_user_instruction_detail_page3);
        this.k.setTextSize((float) a2);
        this.d.add(inflate3);
        View inflate4 = layoutInflater.inflate(R.layout.setting_instruction_page_4, (ViewGroup) null);
        this.l = (TextView) inflate4.findViewById(R.id.TV_user_instruction_detail_page4);
        this.l.setTextSize((float) a2);
        this.d.add(inflate4);
        this.f = new ImageView[this.d.size()];
        this.g = (ViewGroup) layoutInflater.inflate(R.layout.setting_instruction_main, (ViewGroup) null);
        this.h = (ViewGroup) this.g.findViewById(R.id.LL_ui_down);
        this.c = (ViewPager) this.g.findViewById(R.id.pager);
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            this.e = new ImageView(this);
            this.e.setLayoutParams(new ViewGroup.LayoutParams(40, 20));
            this.f[i2] = this.e;
            if (i2 == 0) {
                this.f[i2].setBackgroundResource(R.drawable.instruction_pageindex_green);
            } else {
                this.f[i2].setBackgroundResource(R.drawable.instruction_pageindex_gray);
            }
            this.h.addView(this.f[i2]);
        }
        setContentView(this.g);
        this.c.setAdapter(new a());
        this.c.addOnPageChangeListener(new b());
        this.o = (ImageButton) findViewById(R.id.IB_ui_user_instructions_leftArrow);
        this.o.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingUserInstructions.AnonymousClass1 */

            public void onClick(View view) {
                UI_SettingUserInstructions.this.c.setCurrentItem(UI_SettingUserInstructions.this.c.getCurrentItem() - 1);
            }
        });
        this.p = (ImageButton) findViewById(R.id.IB_ui_user_instructions_rightArrow);
        this.p.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingUserInstructions.AnonymousClass2 */

            public void onClick(View view) {
                UI_SettingUserInstructions.this.c.setCurrentItem(UI_SettingUserInstructions.this.c.getCurrentItem() + 1);
            }
        });
        this.m = (TextView) findViewById(R.id.TV_ui_user_instructions_skip_back);
        this.m.setTextSize((float) new c(a2).d((int) (((double) a2) * 0.9d)).a());
        this.m.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingUserInstructions.AnonymousClass3 */

            public void onClick(View view) {
                if (UI_SettingUserInstructions.this.c.getCurrentItem() == 0) {
                    UI_SettingUserInstructions.this.d();
                } else {
                    UI_SettingUserInstructions.this.c.setCurrentItem(UI_SettingUserInstructions.this.c.getCurrentItem() - 1);
                }
            }
        });
        this.n = (TextView) findViewById(R.id.TV_ui_user_instructions_next_start);
        this.n.setTextSize((float) new c(a2).g((int) (((double) a2) * 0.95d)).a());
        this.n.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingUserInstructions.AnonymousClass4 */

            public void onClick(View view) {
                if (UI_SettingUserInstructions.this.c.getCurrentItem() == UI_SettingUserInstructions.this.f.length - 1) {
                    UI_SettingUserInstructions.this.d();
                } else {
                    UI_SettingUserInstructions.this.c.setCurrentItem(UI_SettingUserInstructions.this.c.getCurrentItem() + 1);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void d() {
        if (this.b.d.b()) {
            this.b.a(256, this, new Intent(this, UI_LiveViewController.class));
        } else {
            this.b.a(1024, this, new Intent(this, UI_PhoneGalleryController.class));
        }
        this.b.a(0, (GeneralFunction.a.a) null);
    }

    /* access modifiers changed from: package-private */
    public class a extends PagerAdapter {
        a() {
        }

        @Override // android.support.v4.view.PagerAdapter
        public int getCount() {
            return UI_SettingUserInstructions.this.d.size();
        }

        @Override // android.support.v4.view.PagerAdapter
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override // android.support.v4.view.PagerAdapter
        public int getItemPosition(Object obj) {
            return super.getItemPosition(obj);
        }

        @Override // android.support.v4.view.PagerAdapter
        public void destroyItem(View view, int i, Object obj) {
            ((ViewPager) view).removeView((View) UI_SettingUserInstructions.this.d.get(i));
        }

        @Override // android.support.v4.view.PagerAdapter
        public Object instantiateItem(View view, int i) {
            ((ViewPager) view).addView((View) UI_SettingUserInstructions.this.d.get(i));
            return UI_SettingUserInstructions.this.d.get(i);
        }

        @Override // android.support.v4.view.PagerAdapter
        public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
        }

        @Override // android.support.v4.view.PagerAdapter
        public Parcelable saveState() {
            return null;
        }

        @Override // android.support.v4.view.PagerAdapter
        public void startUpdate(View view) {
        }

        @Override // android.support.v4.view.PagerAdapter
        public void finishUpdate(View view) {
        }
    }

    /* access modifiers changed from: package-private */
    public class b implements ViewPager.OnPageChangeListener {
        b() {
        }

        @Override // android.support.v4.view.ViewPager.OnPageChangeListener
        public void onPageScrollStateChanged(int i) {
        }

        @Override // android.support.v4.view.ViewPager.OnPageChangeListener
        public void onPageScrolled(int i, float f, int i2) {
        }

        @Override // android.support.v4.view.ViewPager.OnPageChangeListener
        public void onPageSelected(int i) {
            for (int i2 = 0; i2 < UI_SettingUserInstructions.this.f.length; i2++) {
                if (i == i2) {
                    UI_SettingUserInstructions.this.f[i].setBackgroundResource(R.drawable.instruction_pageindex_green);
                } else {
                    UI_SettingUserInstructions.this.f[i2].setBackgroundResource(R.drawable.instruction_pageindex_gray);
                }
                if (i == 0) {
                    UI_SettingUserInstructions.this.m.setVisibility(0);
                    UI_SettingUserInstructions.this.n.setVisibility(8);
                    UI_SettingUserInstructions.this.o.setVisibility(8);
                    UI_SettingUserInstructions.this.p.setVisibility(0);
                } else if (i == UI_SettingUserInstructions.this.f.length - 1) {
                    UI_SettingUserInstructions.this.m.setVisibility(8);
                    UI_SettingUserInstructions.this.n.setVisibility(0);
                    UI_SettingUserInstructions.this.o.setVisibility(0);
                    UI_SettingUserInstructions.this.p.setVisibility(8);
                } else {
                    UI_SettingUserInstructions.this.m.setVisibility(8);
                    UI_SettingUserInstructions.this.n.setVisibility(8);
                    UI_SettingUserInstructions.this.o.setVisibility(0);
                    UI_SettingUserInstructions.this.p.setVisibility(0);
                }
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        o.a();
    }

    @Override // GeneralFunction.a.a
    public void a(a.c.a aVar, long j2) {
        if (this.b != null) {
            this.b.a(aVar, j2);
        } else {
            Log.e("UserInstructions", "SendUiMsg error!! MsgEx = " + aVar.b().what);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(Message message) {
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 12034:
                if (this.b.p() && this.b.c.d) {
                    this.b.i();
                    this.b.c.d = false;
                    return;
                }
                return;
            case 12043:
                this.b.a(aVar.b("AppMode"), this, new Intent(this, (Class) aVar.c()));
                this.b.a(0, (GeneralFunction.a.a) null);
                return;
            case 18436:
                if (this.b.p()) {
                    this.b.i();
                    this.b.c.d = true;
                    return;
                }
                return;
            case 18437:
                if (aVar.a("usb_permission")) {
                    Log.d("UserInstructions", "has permission, need to switch to live view");
                    this.b.c.d = false;
                    this.b.a(256, this, new Intent(this, UI_LiveViewController.class));
                    this.b.a(0, (GeneralFunction.a.a) null);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void e() {
        this.q = new OrientationEventListener(this.s) {
            /* class ui_Controller.ui_Setting.UI_SettingUserInstructions.AnonymousClass5 */

            public void onOrientationChanged(int i) {
                if (i != -1) {
                    if (i > 350 || i < 10) {
                        if (UI_SettingUserInstructions.this.r != 1) {
                            UI_SettingUserInstructions.this.c(1);
                            UI_SettingUserInstructions.this.r = 1;
                        }
                    } else if (i > 170 && i < 190 && UI_SettingUserInstructions.this.r != 9) {
                        UI_SettingUserInstructions.this.c(9);
                        UI_SettingUserInstructions.this.r = 9;
                    }
                }
            }
        };
    }

    public void c(int i2) {
        d.a(this, i2);
    }
}
