package ui_Controller.ui_Setting;

import GeneralFunction.a.a;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import com.google.android.gms.plus.PlusShare;
import com.huawei.cvIntl60.R;

public class UI_SettingInformation extends a {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.ui_setting_info);
        String stringExtra = getIntent().getStringExtra("url");
        String stringExtra2 = getIntent().getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
        WebView webView = (WebView) findViewById(R.id.WV_ui_Information_content);
        webView.loadUrl(stringExtra);
        webView.getSettings().setAllowFileAccess(false);
        webView.getSettings().setJavaScriptEnabled(false);
        webView.getSettings().setAllowFileAccessFromFileURLs(false);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(false);
        webView.getSettings().setGeolocationEnabled(false);
        webView.getSettings().setAllowContentAccess(false);
        ((TextView) findViewById(R.id.TV_ui_Information_title)).setText(stringExtra2);
        findViewById(R.id.IB_ui_Information_topBack).setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Setting.UI_SettingInformation.AnonymousClass1 */

            public void onClick(View view) {
                UI_SettingInformation.this.onBackPressed();
            }
        });
    }

    public void onBackPressed() {
        super.onBackPressed();
    }
}
