package ui_Controller.ui_Setting;

import GeneralFunction.d;
import GeneralFunction.o;
import a.a.a.c;
import a.c.a;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import com.google.api.client.http.HttpStatusCodes;
import com.huawei.cvIntl60.R;
import java.io.IOException;
import ui_Controller.b.k;
import ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController;
import ui_Controller.ui_Liveview.UI_LiveViewController;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private UI_SettingMenuController f1525a = null;

    private void a(String str, int i) {
        d.a("UI_SettingMenuHandler", str, i);
    }

    public a(UI_SettingMenuController uI_SettingMenuController) {
        this.f1525a = uI_SettingMenuController;
    }

    public void a(Message message) {
        int i = message.what;
        switch (this.f1525a.c.a()) {
            case 2064:
                b(message);
                return;
            default:
                return;
        }
    }

    public void b(Message message) {
        int i;
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 8448:
                if (!this.f1525a.c.c.f) {
                    this.f1525a.a(8448, 50L);
                    return;
                }
                this.f1525a.c.a(256, this.f1525a, new Intent(this.f1525a, UI_LiveViewController.class));
                this.f1525a.c.a(0, (GeneralFunction.a.a) null);
                this.f1525a.c.b(268435455L);
                return;
            case 8449:
                this.f1525a.c.a(this.f1525a);
                this.f1525a.c.a(0, (GeneralFunction.a.a) null);
                this.f1525a.c.b(268435455L);
                return;
            case 8452:
                this.f1525a.c.a(1024, this.f1525a, new Intent(this.f1525a, UI_PhoneGalleryController.class));
                this.f1525a.c.a(0, (GeneralFunction.a.a) null);
                this.f1525a.c.b(268435455L);
                return;
            case 8468:
                try {
                    this.f1525a.startActivity(new Intent("android.settings.SETTINGS"));
                    return;
                } catch (ActivityNotFoundException e) {
                    a("Intent settings fail", 3);
                    return;
                }
            case 8752:
                this.f1525a.c.c.h.c = HttpStatusCodes.STATUS_CODE_FOUND;
                a("UI_SettingControl.GetCaptureType(): " + GeneralFunction.m.a.q(), 3);
                this.f1525a.c.a(256, this.f1525a, (Intent) null);
                this.f1525a.a(8715, 0L);
                return;
            case 9216:
                this.f1525a.i();
                return;
            case 9220:
                this.f1525a.g = false;
                this.f1525a.a(true, this.f1525a.getResources().getString(R.string.reset_all_busy_string));
                this.f1525a.k();
                SharedPreferences.Editor b = GeneralFunction.n.a.b(this.f1525a.c);
                b.putInt("currentMode", 0).apply();
                b.putInt("pipMode", 0).apply();
                this.f1525a.j();
                this.f1525a.c();
                this.f1525a.d().b();
                this.f1525a.c.a(9246, 500);
                return;
            case 9234:
                GeneralFunction.m.a.c(true);
                this.f1525a.a(8452, 0L);
                return;
            case 9237:
                o.b(this.f1525a);
                o.a((Context) this.f1525a, true, false, this.f1525a.getResources().getString(R.string.access_permission), this.f1525a.getResources().getString(R.string.access_permission_please_select), new String[]{this.f1525a.getResources().getString(R.string.dialog_option_cancel), this.f1525a.getResources().getString(R.string.dialog_option_continue)}, new int[]{9240, 9238});
                return;
            case 9238:
                this.f1525a.n();
                return;
            case 9240:
                o.b(this.f1525a);
                o.a((Context) this.f1525a, true, false, this.f1525a.getResources().getString(R.string.access_permission), this.f1525a.getResources().getString(R.string.access_permission_reset), this.f1525a.getResources().getString(R.string.dialog_option_ok), 12039);
                this.f1525a.d.T = 0;
                this.f1525a.d.U = 0;
                this.f1525a.i();
                return;
            case 9242:
                this.f1525a.a(this.f1525a.l(), GeneralFunction.n.a.a(this.f1525a.c).getInt("hotaApkSize", 0));
                return;
            case 9243:
                a();
                this.f1525a.a(false);
                this.f1525a.c.c.l.q = false;
                o.b(this.f1525a);
                o.a((Context) this.f1525a, true, true, this.f1525a.getResources().getString(R.string.version_update_update_now_title), this.f1525a.getResources().getString(R.string.version_update_update_message), new String[]{this.f1525a.getResources().getString(R.string.dialog_option_cancel), this.f1525a.getResources().getString(R.string.dialog_option_ok)}, new int[]{12039, 9245});
                return;
            case 9246:
                this.f1525a.a(false, "");
                return;
            case 9247:
                o.b(this.f1525a);
                o.a((Context) this.f1525a, true, true, this.f1525a.getResources().getString(R.string.unable_to_connect), this.f1525a.getResources().getString(R.string.unable_to_connect_message), new String[]{this.f1525a.getResources().getString(R.string.dialog_option_cancel), this.f1525a.getResources().getString(R.string.dialog_option_settings)}, new int[]{12039, 8468});
                return;
            case 9252:
                a();
                this.f1525a.a(false);
                this.f1525a.c.c.l.q = false;
                this.f1525a.a(9247);
                return;
            case 9254:
                o.a((Context) this.f1525a, true, false, this.f1525a.getResources().getString(R.string.application_update_opimization_title), this.f1525a.getResources().getString(R.string.application_update_opimization) + "\n" + GeneralFunction.n.a.a(this.f1525a.c).getString("hotaOptimization", "") + "\n", new String[]{this.f1525a.getResources().getString(R.string.dialog_option_later), this.f1525a.getResources().getString(R.string.dialog_option_update)}, new int[]{9249, 9250});
                return;
            case 9257:
                b();
                if (this.f1525a.c.d.b()) {
                    this.f1525a.d.t = false;
                    this.f1525a.q();
                    this.f1525a.a(true, this.f1525a.getResources().getString(R.string.camera_update_progress) + "\n\n0%");
                    this.f1525a.b(20481);
                    return;
                }
                return;
            case 9258:
                this.f1525a.c.c.h.ak = 0;
                this.f1525a.a(this.f1525a.c.c.h.af);
                this.f1525a.a(false, "");
                o.a((Context) this.f1525a, true, false, this.f1525a.getResources().getString(R.string.camera_update_for_update_failed), this.f1525a.getResources().getString(R.string.camera_update_fail), this.f1525a.getResources().getString(R.string.dialog_option_ok), 12039);
                a();
                return;
            case 9259:
                a("Start send FW, lCurrentDataPartId = " + this.f1525a.c.c.h.ak, 1);
                if (this.f1525a.c.c.h.ak <= 1) {
                    this.f1525a.a(this.f1525a.getResources().getString(R.string.camera_update_progress) + "\n\n" + 5 + "%");
                    this.f1525a.c.c.h.ai = 0;
                    this.f1525a.c.d.a(new a.c.a(18456));
                    return;
                } else if (this.f1525a.c.m()) {
                    a("Already start sending FW, ignore start sending msg", 0);
                    return;
                } else {
                    this.f1525a.a(9258);
                    return;
                }
            case 9260:
                byte[] bArr = new byte[this.f1525a.c.c.h.ah];
                if (this.f1525a.c.c.h.ak == 0) {
                    a("Send FW interrupted!", 0);
                    return;
                }
                a("filesize (" + this.f1525a.c.c.h.ag + "), SendDataSize (" + this.f1525a.c.c.h.ai + ")", 0);
                if (this.f1525a.c.c.h.ag <= this.f1525a.c.c.h.ai) {
                    this.f1525a.a(this.f1525a.getResources().getString(R.string.camera_update_progress) + "\n\n" + 85 + "%");
                    if (this.f1525a.c.c.l.u) {
                        this.f1525a.b(20509);
                        this.f1525a.b(20503);
                        return;
                    }
                    this.f1525a.b(20485);
                    return;
                }
                if (this.f1525a.c.c.h.ag - this.f1525a.c.c.h.ai >= this.f1525a.c.c.h.ah) {
                    i = this.f1525a.c.c.h.ah;
                } else {
                    i = this.f1525a.c.c.h.ag - this.f1525a.c.c.h.ai;
                }
                try {
                    this.f1525a.c.c.h.af.read(bArr, 0, i);
                } catch (IOException e2) {
                    this.f1525a.a(9258);
                    e2.printStackTrace();
                }
                this.f1525a.c.c.h.ai += i;
                a("fileSize " + this.f1525a.c.c.h.ag + " lCurrentSendDataSize " + this.f1525a.c.c.h.ai, 4);
                c cVar = new c();
                cVar.f = bArr;
                cVar.e = this.f1525a.c.c.h.aj;
                k kVar = this.f1525a.c.c.h;
                int i2 = kVar.ak;
                kVar.ak = i2 + 1;
                cVar.b = i2;
                cVar.d = this.f1525a.c.c.h.ag;
                cVar.c = i;
                cVar.f168a = this.f1525a.c.c.h.al;
                a("Send FW Data - TotalPart: " + cVar.e + ", PartID: " + cVar.b + ", TotalSize: " + cVar.d + ", TransferSize: " + cVar.c, 0);
                this.f1525a.a(this.f1525a.getResources().getString(R.string.camera_update_progress) + "\n\n" + Integer.toString(((cVar.b * 80) / cVar.e) + 5) + "%");
                a.c.a aVar2 = new a.c.a(18528);
                aVar2.a("FWImage", new a.C0010a(cVar));
                this.f1525a.c.c(aVar2);
                return;
            case 9261:
                if (this.f1525a.c.c.h.ak != 0) {
                    this.f1525a.a(this.f1525a.getResources().getString(R.string.camera_update_progress) + "\n\n" + Integer.toString((this.f1525a.c.c.h.ak - this.f1525a.c.c.h.aj) + 85) + "%");
                    return;
                }
                return;
            case 9262:
                this.f1525a.c.c(new a.c.a(18457));
                this.f1525a.a(this.f1525a.c.c.h.af);
                return;
            case 9263:
                this.f1525a.a(false, "");
                o.a((Context) this.f1525a, true, true, this.f1525a.getResources().getString(R.string.camera_update_complete_title), this.f1525a.getResources().getString(R.string.camera_update_complete), this.f1525a.getResources().getString(R.string.dialog_option_ok), 12039, true);
                return;
            case 12032:
                Process.killProcess(Process.myPid());
                return;
            case 12033:
                if (this.f1525a.getIntent().getBooleanExtra("CaptureSetting", false)) {
                    this.f1525a.d.X = true;
                } else {
                    this.f1525a.d.X = false;
                }
                this.f1525a.c.c(268435455);
                return;
            case 12034:
                if (this.f1525a.c.p() && this.f1525a.c.c.d) {
                    this.f1525a.c.i();
                    this.f1525a.c.c.d = false;
                    return;
                } else if (this.f1525a.c.d.b()) {
                    if (this.f1525a.c.c.h.ak != 0 && !this.f1525a.d.t) {
                        this.f1525a.c.c.h.ak = 0;
                        this.f1525a.a(9257);
                    }
                    this.f1525a.c.c(new a.c.a(18444));
                    return;
                } else {
                    return;
                }
            case 12035:
                if (this.f1525a.c.d.b()) {
                    this.f1525a.c.c(new a.c.a(18445));
                    if (!this.f1525a.d.t) {
                        this.f1525a.c.c(new a.c.a(18443));
                    }
                }
                a();
                return;
            case 12036:
            case 12037:
            case 12038:
            default:
                return;
            case 12039:
                this.f1525a.c.c.g.f1324a = 0;
                if (this.f1525a.g) {
                    this.f1525a.g = false;
                    return;
                }
                return;
            case 12040:
                o.b(this.f1525a);
                o.a((Context) this.f1525a, true, false, this.f1525a.getResources().getString(R.string.store_location), "Please select SD card and DCIM folder", this.f1525a.getResources().getString(R.string.dialog_option_ok), 12039);
                return;
            case 12042:
                o.a((Context) this.f1525a, true, false, this.f1525a.getResources().getString(R.string.warning), this.f1525a.getResources().getString(R.string.permission_always_deny_msg), this.f1525a.getResources().getString(R.string.dialog_option_ok), 12032);
                return;
            case 12043:
                if (!this.f1525a.c.c.l.q) {
                    this.f1525a.c.a(aVar.b("AppMode"), this.f1525a, new Intent(this.f1525a, (Class) aVar.c()));
                    this.f1525a.c.a(0, (GeneralFunction.a.a) null);
                    this.f1525a.c.b(268435455L);
                    return;
                }
                return;
            case 12046:
                this.f1525a.g();
                return;
            case 12047:
            case 12048:
                this.f1525a.d.ad = false;
                return;
            case 12049:
                this.f1525a.f();
                return;
            case 18435:
                a("MSG_REMOTE_USB_CORE_DETACH", 0);
                this.f1525a.b(false);
                this.f1525a.d.ac = this.f1525a.c.d.b();
                this.f1525a.d.t = false;
                this.f1525a.a(false, "");
                this.f1525a.m();
                this.f1525a.e();
                if (this.f1525a.g) {
                    o.b(this.f1525a);
                    this.f1525a.g = false;
                }
                a();
                return;
            case 18436:
                if (!this.f1525a.c.c.l.q) {
                    this.f1525a.c.c.e = false;
                    if (this.f1525a.c.p()) {
                        this.f1525a.c.i();
                        this.f1525a.c.c.d = true;
                        return;
                    }
                    return;
                }
                return;
            case 18437:
                if (aVar.a("usb_permission")) {
                    a("has permission, need to switch to live view", 1);
                    this.f1525a.c.c.d = false;
                    this.f1525a.c.a(8448, 0);
                    return;
                }
                return;
            case 18518:
            case 18523:
                this.f1525a.a(9262);
                return;
            case 18528:
                if (aVar.b("result") == 0) {
                    this.f1525a.a(9260);
                    return;
                } else if (aVar.b("result") != 5) {
                    a("*****Send FW data ERROR*****", 0);
                    this.f1525a.a(9258);
                    return;
                } else {
                    return;
                }
            case 18551:
                if (aVar.b("result") == 0) {
                    this.f1525a.b(ui_Controller.a.c.p + "FPUPDATE.DAT");
                    this.f1525a.a(9260);
                    return;
                } else if (aVar.b("result") == 1) {
                    this.f1525a.c.c(new a.c.a(18551), 5);
                    return;
                } else if (aVar.b("result") != 5) {
                    this.f1525a.a(false, "");
                    this.f1525a.c.c.h.ak = 0;
                    o.a((Context) this.f1525a, true, false, this.f1525a.getResources().getString(R.string.camera_update_for_newer_program), this.f1525a.getResources().getString(R.string.camera_update_failed_fw_upgrade_mode), this.f1525a.getResources().getString(R.string.dialog_option_ok), 12039);
                    a();
                    return;
                } else {
                    return;
                }
            case 18552:
                if (aVar.b("result") == 0) {
                    this.f1525a.d.t = true;
                    if (!this.f1525a.c.c.l.u) {
                        this.f1525a.b(20503);
                    }
                    this.f1525a.c.c(new a.c.a(18458));
                    return;
                } else if (aVar.b("result") == 1) {
                    this.f1525a.c.c(new a.c.a(18552), 100);
                    return;
                } else if (aVar.b("result") != 5) {
                    this.f1525a.a(false, "");
                    this.f1525a.c.c.h.ak = 0;
                    o.a((Context) this.f1525a, true, false, this.f1525a.getResources().getString(R.string.camera_update_for_newer_program), this.f1525a.getResources().getString(R.string.camera_update_failed_error_status) + aVar.b("result"), this.f1525a.getResources().getString(R.string.dialog_option_ok), 12039);
                    a();
                    return;
                } else {
                    return;
                }
            case 18553:
                if (aVar.b("result") == 0) {
                    this.f1525a.a(this.f1525a.getResources().getString(R.string.camera_update_progress) + "\n\n100%");
                    this.f1525a.c.c.h.ak = 0;
                    this.f1525a.a(9263, 800L);
                    a();
                    return;
                } else if (aVar.b("result") == 1) {
                    this.f1525a.c.c(new a.c.a(18553), 100);
                    return;
                } else if (aVar.b("result") != 5) {
                    a("FW update fail!", 0);
                    this.f1525a.c.c.h.ak = 0;
                    this.f1525a.a(false, "");
                    o.a((Context) this.f1525a, true, false, this.f1525a.getResources().getString(R.string.camera_update_for_newer_program), this.f1525a.getResources().getString(R.string.camera_update_failed_reconnect_camera), this.f1525a.getResources().getString(R.string.dialog_option_ok), 12039);
                    a();
                    return;
                } else {
                    return;
                }
            case 32768:
                if (this.f1525a.d.X) {
                    this.f1525a.c.a(8448, 0);
                    return;
                } else {
                    this.f1525a.c.a(8460, 0);
                    return;
                }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x009d A[SYNTHETIC, Splitter:B:28:0x009d] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x017d A[SYNTHETIC, Splitter:B:50:0x017d] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x018c  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(android.os.Message r14) {
        /*
        // Method dump skipped, instructions count: 604
        */
        throw new UnsupportedOperationException("Method not decompiled: ui_Controller.ui_Setting.a.c(android.os.Message):void");
    }

    private void a() {
        if (this.f1525a.f != null) {
            this.f1525a.f.release();
            this.f1525a.f = null;
        }
    }

    private void b() {
        PowerManager powerManager = (PowerManager) this.f1525a.getSystemService("power");
        if (this.f1525a.f != null) {
            a();
        }
        this.f1525a.f = powerManager.newWakeLock(536870922, "Lexiconda");
        this.f1525a.f.acquire();
    }
}
