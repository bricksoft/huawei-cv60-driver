package ui_Controller.ui_Gallery;

import GeneralFunction.Player.player.SphericalVideoPlayer;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;
import ui_Controller.CustomWidget.ListHorizontalScrollView.ListHorizontalScrollView;
import ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView;
import ui_Controller.CustomWidget.a;
import ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private c f1333a = null;
    private ui_Controller.CustomWidget.a b = null;
    private ui_Controller.CustomWidget.ListHorizontalScrollView.b.a c = null;
    private ui_Controller.CustomWidget.SingleHorizontalScrollView.b.a d = null;
    private int e = 0;
    private d f = new d();
    private d g = new d();
    private boolean h = false;
    private int i = -1;
    private GeneralFunction.a.a j = null;
    private UI_PhoneGalleryController k = null;
    private ListHorizontalScrollView.a l = new ListHorizontalScrollView.a() {
        /* class ui_Controller.ui_Gallery.b.AnonymousClass1 */

        @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.ListHorizontalScrollView.a
        public void a(int i, View view) {
            b.this.e = i;
            b.this.f1333a.g.a(i, b.this.f1333a.d.e(i).f());
            b.this.f1333a.d.b(i);
        }
    };
    private ListHorizontalScrollView.d m = new ListHorizontalScrollView.d() {
        /* class ui_Controller.ui_Gallery.b.AnonymousClass2 */

        @Override // ui_Controller.CustomWidget.ListHorizontalScrollView.ListHorizontalScrollView.d
        public void a(int i, MotionEvent motionEvent) {
            b.this.d();
        }
    };
    private SingleHorizontalScrollView.a n = new SingleHorizontalScrollView.a() {
        /* class ui_Controller.ui_Gallery.b.AnonymousClass3 */

        @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.a
        public void a(int i, View view) {
            b.this.e = i;
            b.this.f1333a.g.a(i, b.this.f1333a.d.e(i).f());
        }
    };
    private SingleHorizontalScrollView.b o = new SingleHorizontalScrollView.b() {
        /* class ui_Controller.ui_Gallery.b.AnonymousClass4 */

        @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.b
        public void a(int i, Bitmap bitmap) {
            b.this.f1333a.h.a(i, bitmap);
        }
    };
    private SingleHorizontalScrollView.d p = new SingleHorizontalScrollView.d() {
        /* class ui_Controller.ui_Gallery.b.AnonymousClass5 */

        @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.d
        public void a(int i, PointF pointF) {
            switch (i) {
                case 1:
                    switch (b.this.f1333a.f.a(b.this.e)) {
                        case 0:
                        case 4:
                        case 6:
                            b.this.j.a(8962, 0);
                            return;
                        case 1:
                        case 2:
                        case 3:
                        case 5:
                            if (b.this.b(pointF.x, pointF.y)) {
                                b.this.j.a(8963, 0);
                                return;
                            } else {
                                b.this.j.a(8962, 0);
                                return;
                            }
                        default:
                            return;
                    }
                case 2:
                    b.this.j.a(8964, 0);
                    return;
                default:
                    return;
            }
        }
    };
    private boolean q = false;
    private long r = 0;
    private PointF s = new PointF();
    private SingleHorizontalScrollView.e t = new SingleHorizontalScrollView.e() {
        /* class ui_Controller.ui_Gallery.b.AnonymousClass6 */

        @Override // ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView.e
        public void a(int i, MotionEvent motionEvent) {
            switch (motionEvent.getAction() & 255) {
                case 0:
                    b.this.q = false;
                    b.this.s.set(motionEvent.getX(), motionEvent.getY());
                    b.this.d();
                    return;
                case 1:
                case 3:
                case 4:
                default:
                    return;
                case 2:
                    if (b.this.a((b) motionEvent.getX(), motionEvent.getY())) {
                        b.this.q = true;
                        b.this.d();
                        return;
                    }
                    return;
                case 5:
                    b.this.e();
                    return;
            }
        }
    };

    public interface a {
        void a(int i, Bitmap bitmap);
    }

    /* renamed from: ui_Controller.ui_Gallery.b$b  reason: collision with other inner class name */
    public interface AbstractC0102b {
        void a(int i, Bitmap bitmap);
    }

    public static class c {

        /* renamed from: a  reason: collision with root package name */
        public int f1340a = 0;
        public int b = 0;
        public ListHorizontalScrollView c = null;
        public SingleHorizontalScrollView d = null;
        public SphericalVideoPlayer e = null;
        public GeneralFunction.e.d f;
        public a g = null;
        public AbstractC0102b h = null;
        public Boolean i = false;
    }

    /* access modifiers changed from: private */
    public class d {
        private int b;
        private int c;

        private d() {
            this.b = 0;
            this.c = 0;
        }
    }

    private void a(String str, int i2) {
        GeneralFunction.d.a("UI_GalleryViewWidgetHandler", str, i2);
    }

    public b(GeneralFunction.a.a aVar) {
        this.j = aVar;
    }

    public void a(UI_PhoneGalleryController uI_PhoneGalleryController) {
        this.k = uI_PhoneGalleryController;
    }

    public void a(c cVar, int i2, int i3) {
        this.f1333a = cVar;
        this.e = i2;
        a.b bVar = new a.b();
        bVar.f1311a = ui_Controller.a.c.e + "/TempThumbnail";
        bVar.b = 18096128;
        bVar.c = 810000;
        bVar.d = 3;
        bVar.e = i3;
        bVar.f = false;
        bVar.g = true;
        bVar.h = true;
        this.b = new ui_Controller.CustomWidget.a(bVar);
        this.f.b = cVar.d.getWidth();
        this.f.c = cVar.d.getHeight();
        this.d = new ui_Controller.CustomWidget.SingleHorizontalScrollView.b.a(this.j, cVar.f, this.f.b, this.f.c, cVar.i.booleanValue());
        cVar.d.a(this.n);
        cVar.d.a(this.o);
        cVar.d.a(this.p);
        cVar.d.a(this.t);
        SingleHorizontalScrollView.f fVar = new SingleHorizontalScrollView.f();
        fVar.f1285a = this.d;
        fVar.b = this.b;
        fVar.c = 1;
        fVar.d = this.f.b;
        fVar.e = this.f.c;
        cVar.d.setSingleHorizontalScrollViewCallback(this.k);
        cVar.d.a(fVar, i2);
        this.i = 0;
    }

    public void a() {
        if (this.f1333a != null && this.f1333a.d != null) {
            this.f1333a.d.b();
        }
    }

    public void b() {
        if (this.f1333a != null && this.f1333a.d != null) {
            this.f1333a.d.c();
        }
    }

    public void a(int i2, int i3) {
        if (!this.h) {
            this.g.b = i2;
            this.g.c = i3;
            this.h = true;
        }
    }

    public void a(int i2) {
        if (i2 == 2) {
            a(this.g);
        } else if (i2 == 1) {
            a(this.f);
        }
    }

    private void a(d dVar) {
        a("objSize.lWidth: " + dVar.b + " ,objSize.lHeight: " + dVar.c, 3);
        if (this.f1333a != null && this.i != -1) {
            this.d = new ui_Controller.CustomWidget.SingleHorizontalScrollView.b.a(this.j, this.f1333a.f, dVar.b, dVar.c, this.f1333a.i.booleanValue());
            this.f1333a.d.a(this.n);
            this.f1333a.d.a(this.o);
            this.f1333a.d.a(this.p);
            this.f1333a.d.a(this.t);
            SingleHorizontalScrollView.f fVar = new SingleHorizontalScrollView.f();
            fVar.f1285a = this.d;
            fVar.b = this.b;
            fVar.c = 1;
            fVar.d = dVar.b;
            fVar.e = dVar.c;
            this.f1333a.d.a(fVar, this.e);
        }
    }

    public void c() {
        this.i = -1;
        if (this.f1333a.d != null) {
            this.f1333a.d.setVisibility(4);
            this.f1333a.d.a();
            this.f1333a.d = null;
        }
        if (this.f1333a.e != null) {
            this.f1333a.e.setVisibility(4);
            this.f1333a.e = null;
        }
        if (this.b != null) {
            this.b.a();
            this.b = null;
        }
    }

    public void b(int i2) {
        this.f1333a.d.a(i2);
    }

    public void c(int i2) {
        this.f1333a.d.c(i2);
        this.b.a(i2);
        this.f1333a.d.d();
        a("DeleteImageViewItem:" + i2 + " " + this.e + " " + this.f1333a.f.e(), 4);
        if (this.e >= this.f1333a.f.e() - 1) {
            this.e--;
        }
    }

    public boolean d(int i2) {
        return this.d.f1287a.get(i2).g();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void d() {
        if (System.currentTimeMillis() - this.r > 100) {
            this.j.a(8960, 0);
            this.r = System.currentTimeMillis();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void e() {
        this.j.a(8961, 0);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean a(float f2, float f3) {
        if (Math.abs(f2 - this.s.x) >= ((float) this.f1333a.f1340a) / 160.0f || Math.abs(f3 - this.s.y) >= ((float) this.f1333a.f1340a) / 90.0f) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean b(float f2, float f3) {
        if (Math.abs(f2 - (((float) this.f1333a.f1340a) / 2.0f)) >= ((float) this.f1333a.f1340a) / 18.0f || Math.abs(f3 - (((float) this.f1333a.b) / 2.0f)) >= ((float) this.f1333a.f1340a) / 18.0f) {
            return false;
        }
        return true;
    }
}
