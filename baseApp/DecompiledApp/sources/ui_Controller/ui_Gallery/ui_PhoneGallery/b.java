package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.d;
import GeneralFunction.o;
import a.c.a;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.support.v4.media.session.PlaybackStateCompat;
import com.huawei.cvIntl60.R;
import ui_Controller.a.c;
import ui_Controller.b.j;
import ui_Controller.ui_Liveview.UI_LiveViewController;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private UI_NormalPicMakingController f1411a = null;

    private void a(String str, int i) {
        d.a("UI_NormalPicMakingHandler", str, i);
    }

    public b(UI_NormalPicMakingController uI_NormalPicMakingController) {
        this.f1411a = uI_NormalPicMakingController;
    }

    public void a(Message message) {
        a aVar = new a(message);
        switch (message.what) {
            case 8448:
                this.f1411a.k();
                this.f1411a.b.a(256, this.f1411a, new Intent(this.f1411a, UI_LiveViewController.class));
                this.f1411a.b.a(0, (GeneralFunction.a.a) null);
                this.f1411a.b.b(268435455L);
                this.f1411a.d.d = 1;
                return;
            case 8452:
                this.f1411a.k();
                this.f1411a.c(0);
                Intent intent = new Intent(this.f1411a, UI_PhoneGalleryController.class);
                if (this.f1411a.g) {
                    intent.putExtra("mode", 1024);
                    this.f1411a.g = false;
                    this.f1411a.d.q = new GeneralFunction.a((Context) this.f1411a.b, true, this.f1411a.getResources().getString(R.string.sdcard_removed), 2000);
                } else {
                    intent.putExtra("mode", 1056);
                }
                this.f1411a.b.a(1024, this.f1411a, intent);
                this.f1411a.b.a(0, (GeneralFunction.a.a) null);
                this.f1411a.b.b(268435455L);
                return;
            case 11041:
                this.f1411a.h();
                return;
            case 11042:
                long a2 = d.a(c.c, 0);
                a("dlAvailableSize: " + a2, 3);
                if (a2 < 53477376) {
                    this.f1411a.b(true);
                    this.f1411a.c(false);
                    this.f1411a.b.c(PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE);
                    return;
                }
                this.f1411a.b(20484);
                return;
            case 11043:
                this.f1411a.c(1);
                return;
            case 11044:
                this.f1411a.c(2);
                return;
            case 11045:
                this.f1411a.c(3);
                return;
            case 11048:
                this.f1411a.b(false);
                this.f1411a.b.a(3856, this.f1411a);
                return;
            case 11049:
                this.f1411a.c();
                return;
            case 11050:
                this.f1411a.c(0);
                return;
            case 11051:
                this.f1411a.c(4);
                return;
            case 12033:
                a("ulBrowseIndex " + this.f1411a.d.s, 3);
                a("ulBrowseSingleIndex " + this.f1411a.d.t, 3);
                this.f1411a.e();
                this.f1411a.l();
                this.f1411a.b.c(268435455);
                return;
            case 12034:
                a("ACTIVITY_RESUME", 4);
                j jVar = this.f1411a.b.c;
                if (this.f1411a.b.p() && this.f1411a.b.c.d) {
                    this.f1411a.b.i();
                    this.f1411a.b.c.d = false;
                    return;
                }
                return;
            case 12035:
                a("ACTIVITY_PAUSE", 4);
                j jVar2 = this.f1411a.b.c;
                return;
            case 12036:
                a("ACTIVITY_STOP", 4);
                return;
            case 12037:
                a("ACTIVITY_DESTROY", 4);
                this.f1411a.m();
                return;
            case 12039:
                this.f1411a.b.c.g.f1324a = 0;
                return;
            case 12042:
                o.a((Context) this.f1411a, true, false, this.f1411a.getResources().getString(R.string.warning), this.f1411a.getResources().getString(R.string.permission_always_deny_msg), this.f1411a.getResources().getString(R.string.dialog_option_ok), 12032);
                return;
            case 12043:
                if (!this.f1411a.d.z) {
                    this.f1411a.b.a(aVar.b("AppMode"), this.f1411a, new Intent(this.f1411a, (Class) aVar.c()));
                    this.f1411a.b.a(0, (GeneralFunction.a.a) null);
                    this.f1411a.b.b(268435455L);
                    this.f1411a.d.d = 1;
                    return;
                }
                return;
            case 12046:
            case 12047:
            case 12048:
                return;
            case 12049:
                this.f1411a.d();
                return;
            case 18436:
                if (!this.f1411a.d.z) {
                    this.f1411a.b.c.e = false;
                    if (this.f1411a.b.p()) {
                        this.f1411a.b.i();
                        this.f1411a.b.c.d = true;
                        return;
                    }
                    return;
                }
                return;
            case 18437:
                if (aVar.a("usb_permission")) {
                    a("has permission, need to switch to live view", 1);
                    this.f1411a.b.c.d = false;
                    this.f1411a.b.a(8448, 0);
                    return;
                }
                return;
            default:
                switch (this.f1411a.b.a()) {
                    case 3856:
                        b(message);
                        return;
                    case 3872:
                        c(message);
                        return;
                    default:
                        return;
                }
        }
    }

    public void b(Message message) {
        a("UI_NormalPicMakingMainPhoto handleMessage: 0x" + Integer.toHexString(message.what), 3);
        new a(message);
        switch (message.what) {
            case 11046:
                a(false);
                this.f1411a.a(true);
                return;
            case 32768:
                if (this.f1411a.i) {
                    this.f1411a.a(false);
                    return;
                }
                this.f1411a.g();
                this.f1411a.a(8452, 0);
                return;
            default:
                return;
        }
    }

    public void c(Message message) {
        a("UI_NormalPicMakingSave handleMessage: 0x" + Integer.toHexString(message.what), 3);
        new a(message);
        switch (message.what) {
            case 11047:
                this.f1411a.g();
                this.f1411a.j();
                this.f1411a.c(false);
                this.f1411a.a(8452, 0);
                return;
            case 11052:
                this.f1411a.b.a(3856, this.f1411a);
                this.f1411a.c(false);
                this.f1411a.h = false;
                return;
            case 32768:
                a("Normal pic is saving, block back key", 3);
                return;
            default:
                return;
        }
    }

    private void a(boolean z) {
        this.f1411a.c(z);
    }
}
