package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.d;
import GeneralFunction.e.b;
import GeneralFunction.e.e;
import GeneralFunction.o;
import a.c.a;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Message;
import android.os.Process;
import com.huawei.cvIntl60.R;
import java.util.ArrayList;
import ui_Controller.b.j;
import ui_Controller.ui_Liveview.UI_LiveViewController;
import ui_Controller.ui_Setting.UI_SettingMenuController;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private UI_PhoneGalleryController f1412a = null;
    private e b = null;

    private void a(String str, int i) {
        d.a("UI_PhoneGalleryHandler", str, i);
    }

    public c(UI_PhoneGalleryController uI_PhoneGalleryController) {
        this.f1412a = uI_PhoneGalleryController;
        this.b = new e(uI_PhoneGalleryController);
    }

    public void a(Message message) {
        a aVar = new a(message);
        switch (message.what) {
            case 8448:
                if (!this.f1412a.c.c.f) {
                    this.f1412a.a(8448, 50L);
                    return;
                }
                c();
                this.f1412a.af();
                this.f1412a.c.c.f = false;
                this.f1412a.c.a(256, this.f1412a, new Intent(this.f1412a, UI_LiveViewController.class));
                this.f1412a.c.b(268435455L);
                this.f1412a.e.d = 1;
                return;
            case 8454:
                c();
                this.f1412a.c.a(2048, this.f1412a, new Intent(this.f1412a, UI_SettingMenuController.class));
                this.f1412a.c.a(0, (GeneralFunction.a.a) null);
                this.f1412a.c.b(268435455L);
                this.f1412a.e.d = 1;
                return;
            case 8965:
                this.f1412a.g(true);
                this.f1412a.e.s = aVar.b("SelectIndex");
                a();
                return;
            case 8966:
                if (this.f1412a.ac() || this.f1412a.e.d == 3 || this.f1412a.e.b.e() <= 0) {
                    this.f1412a.o(false);
                    this.f1412a.D(false);
                    this.f1412a.C(false);
                    this.f1412a.e.O = 8;
                    this.f1412a.x(true);
                    this.f1412a.as();
                    this.f1412a.aa();
                    this.f1412a.c.b(268435455L);
                    this.f1412a.a(9146, 0L);
                    return;
                }
                a("Leaving single status:" + this.f1412a.ah() + " " + this.f1412a.e.d + " " + this.f1412a.e.b.e(), 1);
                this.f1412a.a(8966, 50L);
                return;
            case 8969:
                o.a((Context) this.f1412a, true, false, this.f1412a.getResources().getString(R.string.share_via), this.f1412a.getResources().getString(R.string.share_sns_line_dialog), new String[]{this.f1412a.getResources().getString(R.string.share_sns_line_confirm), this.f1412a.getResources().getString(R.string.dialog_option_close)}, new int[]{8970, 0});
                return;
            case 8970:
                this.f1412a.startActivity(this.f1412a.getPackageManager().getLaunchIntentForPackage("jp.naver.line.android"));
                if (this.f1412a.e.f > 0) {
                    this.f1412a.a(9054);
                    this.f1412a.e.j.clear();
                    return;
                }
                return;
            case 8975:
                if (this.f1412a.e.A) {
                    this.f1412a.a(this.f1412a.e.ah, this.f1412a.e.am);
                    return;
                } else if (this.f1412a.e.d == 3) {
                    this.f1412a.a(this.f1412a.e.ah, this.f1412a.e.am);
                    return;
                } else {
                    this.f1412a.b(this.f1412a.e.ah, this.f1412a.e.am);
                    return;
                }
            case 9146:
                if (this.f1412a.ab() || this.f1412a.e.d == 3) {
                    b();
                    return;
                } else {
                    this.f1412a.a(9146, 50L);
                    return;
                }
            case 9152:
                if (this.f1412a.ab()) {
                    this.f1412a.a(8448, 50L);
                    return;
                } else {
                    this.f1412a.a(9152, 50L);
                    return;
                }
            case 12033:
                GeneralFunction.m.a.v();
                this.f1412a.e.f = 0;
                this.f1412a.e();
                if (!this.f1412a.c.u()) {
                    a("IsDatabaseCreateDone false", 4);
                    this.f1412a.c.a(new a(9038));
                    return;
                } else if (aVar.b("mode") == 1056) {
                    this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                    a aVar2 = new a(8965);
                    if (this.f1412a.e.r) {
                        aVar2.a("SelectIndex", 0);
                    } else if (this.f1412a.e.d == 2) {
                        this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.g);
                        this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.g);
                        aVar2.a("SelectIndex", this.f1412a.e.s);
                    } else {
                        aVar2.a("SelectIndex", this.f1412a.e.s);
                    }
                    this.f1412a.u();
                    this.f1412a.c.a(aVar2);
                    this.f1412a.i();
                    this.f1412a.f();
                    return;
                } else if (this.f1412a.q) {
                    this.f1412a.u();
                    this.f1412a.i(false);
                    this.f1412a.i();
                    this.f1412a.g();
                    return;
                } else {
                    a("bEnterExtendSingle false", 4);
                    this.f1412a.e.r = false;
                    b bVar = this.f1412a.d.f;
                    this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), bVar);
                    this.f1412a.e.b.a(this.f1412a.d.e.a(), bVar);
                    this.f1412a.e.d = 1;
                    this.f1412a.i();
                    this.f1412a.u();
                    this.f1412a.e.O = 7;
                    this.f1412a.as();
                    this.f1412a.aa();
                    this.f1412a.c.c(268435455);
                    this.f1412a.g();
                    return;
                }
            case 12042:
                o.a((Context) this.f1412a, true, false, this.f1412a.getResources().getString(R.string.warning), this.f1412a.getResources().getString(R.string.permission_always_deny_msg), this.f1412a.getResources().getString(R.string.dialog_option_ok), 12032);
                return;
            case 12043:
                if (!this.f1412a.e.z) {
                    c();
                    this.f1412a.e.O = 8;
                    this.f1412a.aa();
                    this.f1412a.o();
                    this.f1412a.a(9152);
                    return;
                }
                return;
            case 12046:
                this.f1412a.aB();
                return;
            case 12047:
            case 12048:
                this.f1412a.c(false);
                return;
            case 12049:
                if (!GeneralFunction.m.a.t()) {
                    this.f1412a.aD();
                    return;
                } else {
                    GeneralFunction.m.a.b(true);
                    return;
                }
            case 12050:
                this.f1412a.startActivity(new Intent("android.settings.SETTINGS"));
                return;
            case 18436:
                if (!this.f1412a.e.z) {
                    this.f1412a.c.c.e = false;
                    if (this.f1412a.c.p()) {
                        this.f1412a.c.i();
                        this.f1412a.c.c.d = true;
                        return;
                    }
                    return;
                }
                return;
            case 18437:
                if (aVar.a("usb_permission")) {
                    a("has permission, need to switch to live view", 1);
                    this.f1412a.c.c.d = false;
                    c();
                    this.f1412a.e.O = 8;
                    this.f1412a.aa();
                    this.f1412a.o();
                    this.f1412a.a(9152);
                    return;
                }
                return;
            case 61444:
                this.f1412a.d();
                return;
            default:
                switch (this.f1412a.c.a()) {
                    case 1040:
                        b(message);
                        return;
                    case 1056:
                    case 1057:
                    case 1059:
                    case 1060:
                    case 1061:
                        this.b.a(message);
                        return;
                    default:
                        return;
                }
        }
    }

    public void b(Message message) {
        int i = 0;
        a("UI_PhoneGalleryMultiMain handleMessage: 0x" + Integer.toHexString(message.what), 3);
        a aVar = new a(message);
        switch (message.what) {
            case 8480:
                a("MSG_UI_MODE_CHANGE_MODE_DONE", 3);
                return;
            case 8968:
                this.f1412a.e.j.clear();
                this.f1412a.e.f = 0;
                this.f1412a.e.m.c(0);
                this.f1412a.e.m.c();
                this.f1412a.f(false);
                this.f1412a.c.c(268435455);
                return;
            case 8976:
                if (a.b.b.a.b(this.f1412a) == 2) {
                    o.a((Context) this.f1412a, true, true, this.f1412a.getResources().getString(R.string.accessing_mobile_network_title), this.f1412a.getResources().getString(R.string.accessing_mobile_network_title_content), new String[]{this.f1412a.getResources().getString(R.string.dialog_option_cancel), this.f1412a.getResources().getString(R.string.dialog_option_ok_for_two_button)}, new int[]{12039, 8975});
                    return;
                }
                this.f1412a.a(8975);
                return;
            case 8992:
                int i2 = this.f1412a.e.e;
                if (this.f1412a.e.k.size() != 0) {
                    ArrayList<ui_Controller.b.a> arrayList = new ArrayList<>();
                    this.f1412a.e.e = 5;
                    while (i < this.f1412a.e.k.size()) {
                        this.f1412a.a(this.f1412a.e.k.get(i).intValue(), true, 5, 1, arrayList);
                        i++;
                    }
                    this.f1412a.c.a(arrayList);
                } else if (i2 == 0) {
                }
                this.f1412a.r(true);
                return;
            case 9000:
                a("Press Delete Tab", 4);
                this.f1412a.e.f = 1;
                this.f1412a.e.m.c(1);
                this.f1412a.u();
                this.f1412a.c.c(268435455);
                return;
            case 9001:
                int e = this.f1412a.e.m.e();
                if (e > 1) {
                    o.a((Context) this.f1412a, true, true, this.f1412a.getResources().getString(R.string.delete_image_title), this.f1412a.getResources().getString(R.string.delete_more_image_confirm), new String[]{this.f1412a.getResources().getString(R.string.dialog_option_cancel), this.f1412a.getResources().getString(R.string.dialog_option_delete)}, new int[]{9004, 9003}, true, (int) R.color.red);
                    return;
                } else if (e == 1) {
                    o.a((Context) this.f1412a, true, true, this.f1412a.getResources().getString(R.string.delete_image_title), this.f1412a.getResources().getString(R.string.delete_one_image_confirm), new String[]{this.f1412a.getResources().getString(R.string.dialog_option_cancel), this.f1412a.getResources().getString(R.string.dialog_option_delete)}, new int[]{9004, 9003}, true, (int) R.color.red);
                    return;
                } else {
                    this.f1412a.c.c(268435455);
                    a("No Select", 4);
                    return;
                }
            case 9002:
                this.f1412a.e.f = 0;
                this.f1412a.e.m.c(0);
                this.f1412a.u();
                this.f1412a.e.m.c();
                this.f1412a.c.c(268435455);
                return;
            case 9003:
                this.f1412a.e.z = true;
                this.f1412a.g(true);
                this.f1412a.e.u = this.f1412a.j();
                this.f1412a.e.k.clear();
                while (i < this.f1412a.e.m.getCount()) {
                    if (this.f1412a.e.m.f(i)) {
                        this.f1412a.e.k.add(Integer.valueOf(i));
                    }
                    i++;
                }
                a("psDeleteList size " + this.f1412a.e.k.size(), 4);
                if (this.f1412a.e.d == 2) {
                    this.f1412a.c.a(new a(8992));
                    return;
                } else {
                    this.f1412a.c.a(new a(8992));
                    return;
                }
            case 9004:
                a("Delete Cancel", 4);
                this.f1412a.g(false);
                this.f1412a.c.c(268435455);
                return;
            case 9006:
                a("Press Share Tab", 4);
                this.f1412a.c.a(9007, 0);
                return;
            case 9007:
                this.f1412a.e.f = 2;
                this.f1412a.e.m.c(2);
                this.f1412a.e.m.b();
                this.f1412a.u();
                this.f1412a.c.c(268435455);
                return;
            case 9008:
                a("Press Share Confirm", 4);
                this.f1412a.s();
                if (a.b.b.a.a(this.f1412a)) {
                    this.f1412a.t(true);
                } else {
                    o.a((Context) this.f1412a, true, true, this.f1412a.getResources().getString(R.string.unable_to_connect), this.f1412a.getResources().getString(R.string.unable_to_connect_message), new String[]{this.f1412a.getResources().getString(R.string.dialog_option_cancel), this.f1412a.getResources().getString(R.string.dialog_option_settings)}, new int[]{12039, 12050});
                }
                this.f1412a.w(false);
                return;
            case 9009:
                this.f1412a.e.f = 0;
                this.f1412a.e.m.c(0);
                this.f1412a.u();
                this.f1412a.e.m.c();
                this.f1412a.c.c(268435455);
                return;
            case 9010:
            case 9013:
            case 9014:
            case 61440:
            case 61441:
            case 61442:
            default:
                return;
            case 9011:
                this.f1412a.a(8450);
                return;
            case 9012:
                this.f1412a.c.c(268435455);
                return;
            case 9015:
                if (this.f1412a.d.j) {
                    this.f1412a.d.c = 1;
                    this.f1412a.c.c.k.f1316a.f = 1;
                    this.f1412a.d.e.a(this.f1412a.d.c, this.f1412a.d.f, this.f1412a.d.g, this.f1412a.d.h);
                    this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                    this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                    this.f1412a.e.m.a();
                    this.f1412a.i();
                    this.f1412a.e.m.b();
                    this.f1412a.d.j = false;
                }
                if (this.f1412a.d.c != 1) {
                    this.f1412a.d.i = this.f1412a.d.a(this.f1412a.d.i);
                    this.f1412a.d.c = 1;
                    this.f1412a.c.c.k.f1316a.f = 1;
                    this.f1412a.d.e.a(this.f1412a.d.c, this.f1412a.d.f, this.f1412a.d.g, this.f1412a.d.h);
                    this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                    this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                    this.f1412a.e.m.a();
                    this.f1412a.i();
                    this.f1412a.e.m.b();
                } else {
                    a("Do not need sort", 4);
                }
                this.f1412a.c.c(268435455);
                this.f1412a.u();
                this.f1412a.e(true);
                this.f1412a.i(false);
                return;
            case 9016:
                if (this.f1412a.d.j) {
                    this.f1412a.d.c = 2;
                    this.f1412a.c.c.k.f1316a.f = 2;
                    this.f1412a.d.e.a(this.f1412a.d.c, this.f1412a.d.f, this.f1412a.d.g, this.f1412a.d.h);
                    this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                    this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                    this.f1412a.e.m.a();
                    this.f1412a.i();
                    this.f1412a.e.m.b();
                    this.f1412a.d.j = false;
                }
                if (this.f1412a.d.c != 2) {
                    this.f1412a.d.i = this.f1412a.d.a(this.f1412a.d.i);
                    this.f1412a.d.c = 2;
                    this.f1412a.c.c.k.f1316a.f = 2;
                    this.f1412a.d.e.a(this.f1412a.d.c, this.f1412a.d.f, this.f1412a.d.g, this.f1412a.d.h);
                    this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                    this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                    this.f1412a.e.m.a();
                    this.f1412a.i();
                    this.f1412a.e.m.b();
                } else {
                    a("Do not need sort", 4);
                }
                this.f1412a.c.c(268435455);
                this.f1412a.u();
                return;
            case 9017:
                if (this.f1412a.e.m.g() == 2) {
                    a("Can't Press Select All", 4);
                    this.f1412a.c.c(268435455);
                    return;
                }
                this.f1412a.e.m.a(true, true);
                this.f1412a.d(-1);
                this.f1412a.c.c(268435455);
                return;
            case 9018:
                if (this.f1412a.e.m.g() == 0) {
                    a("Cant Press Unselect All", 4);
                    this.f1412a.c.c(268435455);
                    return;
                }
                this.f1412a.e.m.a(false, true);
                this.f1412a.d(-1);
                this.f1412a.c.c(268435455);
                return;
            case 9019:
                this.f1412a.c.c(268435455);
                return;
            case 9020:
                if (this.f1412a.e.m.a(aVar.b("header id"), -1L, true, true)) {
                    this.f1412a.d(-1);
                    this.f1412a.c.c(268435455);
                    return;
                }
                return;
            case 9038:
                a("MSG_UI_GALLERY_MULTI_WAIT_DATABASE_INIT_DONE", 4);
                if (this.f1412a.c.u()) {
                    a("DatabaseCreateDone", 4);
                    this.f1412a.i();
                    this.f1412a.u();
                    this.f1412a.c.c(268435455);
                    return;
                }
                this.f1412a.c.a(new a(9038), 20);
                return;
            case 9039:
                this.f1412a.e.f = 5;
                this.f1412a.e.m.c(5);
                this.f1412a.e.m.b();
                this.f1412a.f(true);
                this.f1412a.r(false);
                return;
            case 9040:
                this.f1412a.e(this.f1412a.j());
                this.f1412a.e.d = 1;
                this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                this.f1412a.e.m.a();
                this.f1412a.i();
                this.f1412a.e.m.b();
                this.f1412a.c.c(268435455);
                this.f1412a.u();
                this.f1412a.e(true);
                this.f1412a.b(this.f1412a.o);
                this.f1412a.i(false);
                this.f1412a.f(this.f1412a.e.g);
                return;
            case 9041:
                this.f1412a.e(this.f1412a.j());
                this.f1412a.e.d = 2;
                this.f1412a.d(true);
                this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.g);
                this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.g);
                this.f1412a.e.m.a();
                this.f1412a.i();
                this.f1412a.e.m.b();
                this.f1412a.c.c(268435455);
                this.f1412a.u();
                this.f1412a.i(false);
                this.f1412a.f(this.f1412a.e.h);
                return;
            case 9042:
                this.f1412a.e(this.f1412a.j());
                this.f1412a.e.d = 3;
                this.f1412a.d(false);
                this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.h);
                this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.h);
                this.f1412a.e.m.a();
                this.f1412a.i();
                this.f1412a.e.m.b();
                this.f1412a.u();
                this.f1412a.f(this.f1412a.e.i);
                return;
            case 9043:
                this.f1412a.e.x = false;
                this.f1412a.c(true);
                this.f1412a.b(true);
                this.f1412a.r = false;
                this.f1412a.d.e.a(this.f1412a.d.c, this.f1412a.d.f, this.f1412a.d.g, this.f1412a.d.h);
                if (this.f1412a.p && !this.f1412a.q && !this.f1412a.r) {
                    this.f1412a.q(false);
                    this.f1412a.g(false);
                }
                GeneralFunction.m.a.b(false);
                return;
            case 9044:
                this.f1412a.q = false;
                this.f1412a.r = false;
                this.f1412a.d.e.a(this.f1412a.d.c, this.f1412a.d.f, this.f1412a.d.g, this.f1412a.d.h);
                if (this.f1412a.p) {
                    this.f1412a.g();
                    if (this.f1412a.aF() != null) {
                        this.f1412a.c(true);
                    }
                    if (!this.f1412a.q && !this.f1412a.r) {
                        this.f1412a.q(false);
                        this.f1412a.g(false);
                        return;
                    }
                    return;
                }
                return;
            case 9045:
                this.f1412a.e.f = 6;
                this.f1412a.o(true);
                this.f1412a.b(20486);
                return;
            case 9046:
                this.f1412a.e.f = 6;
                this.f1412a.o(true);
                this.f1412a.b(20489);
                return;
            case 9047:
                this.f1412a.e.f = 0;
                this.f1412a.w(false);
                this.f1412a.e.l.clear();
                this.f1412a.s(false);
                if (!this.f1412a.o) {
                    this.f1412a.aD();
                    return;
                } else {
                    this.f1412a.f();
                    return;
                }
            case 9048:
                this.f1412a.o(false);
                String str = null;
                switch (this.f1412a.e.d) {
                    case 1:
                        str = this.f1412a.getResources().getString(R.string.sdcard_not_enough_space);
                        break;
                    case 2:
                        str = this.f1412a.getResources().getString(R.string.phone_not_enough_space);
                        break;
                    default:
                        a("lLocalDatabaseSelectMode Wrong!" + this.f1412a.e.d, 1);
                        break;
                }
                o.a((Context) this.f1412a, true, true, this.f1412a.getResources().getString(R.string.move_full_title), str, this.f1412a.getResources().getString(R.string.dialog_option_ok), 9047, true);
                return;
            case 9049:
                this.f1412a.aE();
                if (this.f1412a.m()) {
                    this.f1412a.g(false);
                    return;
                }
                return;
            case 9053:
                a("MSG_UI_GALLERY_MULTI_DELETE_ERROR_FILE", 3);
                this.f1412a.c(this.f1412a.getResources().getString(R.string.file_not_exist));
                this.f1412a.a(ui_Controller.a.c.d, false);
                this.f1412a.aE();
                this.f1412a.a(ui_Controller.a.c.c, false, false);
                this.f1412a.aE();
                if (this.f1412a.aF() != null) {
                    this.f1412a.e.ab = this.f1412a.aF();
                    this.f1412a.a(this.f1412a.e.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/" + "/", true, false);
                    this.f1412a.aE();
                }
                this.f1412a.y.clear();
                this.f1412a.d.e.a(this.f1412a.d.c, this.f1412a.d.f, this.f1412a.d.g, this.f1412a.d.h);
                switch (this.f1412a.d.b.d) {
                    case 1:
                        this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                        this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.f);
                        break;
                    case 2:
                        this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.g);
                        this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.g);
                        break;
                    case 3:
                        this.f1412a.e.f1320a.a(this.f1412a.d.e.a(), this.f1412a.d.h);
                        this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.d.h);
                        break;
                }
                this.f1412a.e.m.a();
                this.f1412a.i();
                this.f1412a.e.m.b();
                this.f1412a.u();
                this.f1412a.e(true);
                this.f1412a.g(false);
                this.f1412a.z = false;
                return;
            case 9054:
            case 32768:
                if (this.f1412a.e.f == 0) {
                    this.f1412a.c();
                } else if (this.f1412a.q()) {
                    this.f1412a.e.y = true;
                    this.f1412a.c.d();
                } else if (!this.f1412a.r()) {
                    this.f1412a.s(true);
                } else {
                    return;
                }
                this.f1412a.c.c(268435455);
                return;
            case 9237:
                o.b(this.f1412a);
                o.a((Context) this.f1412a, true, true, this.f1412a.getResources().getString(R.string.access_permission), this.f1412a.getResources().getString(R.string.access_permission_please_select), new String[]{this.f1412a.getResources().getString(R.string.dialog_option_cancel), this.f1412a.getResources().getString(R.string.dialog_option_continue)}, new int[]{9240, 9238}, true);
                return;
            case 9238:
                this.f1412a.e.ab = this.f1412a.aF();
                this.f1412a.aG();
                return;
            case 9239:
                this.f1412a.r = true;
                this.f1412a.q(true);
                this.f1412a.g(true);
                this.f1412a.b(20498);
                return;
            case 9240:
                o.b(this.f1412a);
                this.f1412a.c.c.g.f1324a = 0;
                this.f1412a.e.aa = null;
                return;
            case 9248:
                this.f1412a.c.c.l.o = false;
                if (!GeneralFunction.n.a.a(this.f1412a.c).getBoolean("apkUpdateLater", false) && !this.f1412a.c.c.l.ag) {
                    this.f1412a.c.c.l.ag = true;
                    String string = GeneralFunction.n.a.a(this.f1412a.c).getString("hotaOptimization", "");
                    o.a((Context) this.f1412a, true, false, this.f1412a.getResources().getString(R.string.application_update_opimization_title), this.f1412a.getResources().getString(R.string.application_update_opimization) + "\n" + string + "\n", new String[]{this.f1412a.getResources().getString(R.string.dialog_option_later), this.f1412a.getResources().getString(R.string.dialog_option_update)}, new int[]{9249, 9250});
                    return;
                }
                return;
            case 9249:
                GeneralFunction.n.a.b(this.f1412a.c).putBoolean("apkUpdateLater", true).apply();
                this.f1412a.c.c.l.ag = false;
                return;
            case 9250:
                this.f1412a.c.c.l.o = true;
                if (!a.b.b.a.a(this.f1412a) || a.b.b.a.b(this.f1412a) != 2) {
                    this.f1412a.a(8454);
                } else {
                    o.a((Context) this.f1412a, true, false, this.f1412a.getResources().getString(R.string.version_update_title), this.f1412a.getResources().getString(R.string.version_update_update_not_wifi), new String[]{this.f1412a.getResources().getString(R.string.dialog_option_cancel), this.f1412a.getResources().getString(R.string.dialog_option_continue)}, new int[]{9254, 8454});
                }
                this.f1412a.c.c.l.ag = false;
                return;
            case 9254:
                this.f1412a.c.c.l.o = false;
                String string2 = GeneralFunction.n.a.a(this.f1412a.c).getString("hotaOptimization", "");
                o.a((Context) this.f1412a, true, false, this.f1412a.getResources().getString(R.string.application_update_opimization_title), this.f1412a.getResources().getString(R.string.application_update_opimization) + "\n" + string2 + "\n", new String[]{this.f1412a.getResources().getString(R.string.dialog_option_later), this.f1412a.getResources().getString(R.string.dialog_option_update)}, new int[]{9249, 9250});
                return;
            case 12032:
                Process.killProcess(Process.myPid());
                this.f1412a.finish();
                return;
            case 12034:
                a("ACTIVITY_RESUME", 3);
                this.f1412a.aC();
                j jVar = this.f1412a.c.c;
                if (this.f1412a.c.p() && this.f1412a.c.c.d) {
                    this.f1412a.c.i();
                    this.f1412a.c.c.d = false;
                }
                if (this.f1412a.c.d.b()) {
                    this.f1412a.c.c(new a(18444));
                    return;
                }
                return;
            case 12035:
                a("ACTIVITY_PAUSE", 3);
                GeneralFunction.m.a.a(true);
                if (this.f1412a.c.d.b()) {
                    this.f1412a.c.c(new a(18445));
                }
                if (this.f1412a.x()) {
                    this.f1412a.t(false);
                }
                j jVar2 = this.f1412a.c.c;
                return;
            case 12036:
                a("ACTIVITY_STOP", 4);
                return;
            case 12037:
                a("ACTIVITY_DESTROY", 4);
                return;
            case 12038:
                a("ACTIVITY_CONFIGURATION_CHANGED", 4);
                return;
            case 12039:
                o.b(this.f1412a);
                this.f1412a.c.c.g.f1324a = 0;
                return;
            case 12323:
                a("MSG_FILE_PHONE_DELETE_A_FILE_DONE", 4);
                this.f1412a.e.m.h();
                this.f1412a.e.f1320a.b();
                this.f1412a.e.b.a(this.f1412a.d.e.a(), this.f1412a.e.f1320a.d());
                a("New GridView List", 4);
                this.f1412a.e.m.h();
                this.f1412a.e.f1320a.b();
                return;
            case 12324:
                a("MSG_FILE_EXCUTE_ACTION_DONE lActionType " + this.f1412a.e.e, 4);
                this.f1412a.e.m.a();
                if (this.f1412a.e.e == 5) {
                    this.f1412a.e.k.clear();
                    this.f1412a.i();
                    this.f1412a.e.m.b();
                    this.f1412a.e.m.a(false, true);
                    this.f1412a.d(-1);
                    this.f1412a.e.f = 0;
                    this.f1412a.e.m.c(0);
                    this.f1412a.e.m.c();
                    this.f1412a.u();
                    this.f1412a.c.c(268435455);
                    this.f1412a.e.e = 0;
                    this.f1412a.c.a(new a(8992));
                }
                if (this.f1412a.k() == 0) {
                    this.f1412a.g(false);
                    this.f1412a.z = false;
                }
                this.f1412a.c(this.f1412a.e.u);
                this.f1412a.f(false);
                this.f1412a.e.z = false;
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void c(Message message) {
        switch (message.what) {
            case 20486:
                this.f1412a.aK();
                this.f1412a.aN();
                return;
            case 20487:
                this.f1412a.a(9047);
                return;
            case 20488:
                this.f1412a.a(9048);
                return;
            case 20489:
                this.f1412a.aK();
                this.f1412a.aL();
                return;
            case 20490:
                this.f1412a.a(9047);
                return;
            case 20491:
                this.f1412a.a(9048);
                return;
            case 20492:
            case 20493:
            case 20494:
            case 20495:
            case 20496:
            case 20497:
            default:
                this.b.f(message);
                return;
            case 20498:
                this.f1412a.e.ab = this.f1412a.aF();
                int a2 = this.f1412a.a(this.f1412a.e.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/" + "/", true, true);
                this.f1412a.aE();
                if (a2 != -1) {
                    this.f1412a.d.e.a(this.f1412a.d.c, this.f1412a.d.f, this.f1412a.d.g, this.f1412a.d.h);
                    this.f1412a.a(9043);
                    return;
                }
                return;
            case 20499:
                this.f1412a.q = true;
                this.f1412a.az();
                this.f1412a.a(ui_Controller.a.c.d, true);
                this.f1412a.aE();
                this.f1412a.a(ui_Controller.a.c.c, false, true);
                this.f1412a.aE();
                if (this.f1412a.aF() != null) {
                    this.f1412a.e.ab = this.f1412a.aF();
                    this.f1412a.aI();
                    this.f1412a.r = true;
                    this.f1412a.a(this.f1412a.e.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/" + "/", true, true);
                    this.f1412a.aE();
                    this.f1412a.e.x = false;
                }
                this.f1412a.d.e.a(this.f1412a.d.c, this.f1412a.d.f, this.f1412a.d.g, this.f1412a.d.h);
                this.f1412a.a(9044);
                return;
        }
    }

    private void a() {
        this.f1412a.d();
        a("EnterGallerySingleGroup " + this.f1412a.e.r + " " + this.f1412a.e.s + " " + this.f1412a.e.t, 4);
        this.f1412a.c.a(1056);
        this.f1412a.A(false);
        if (this.f1412a.e.d == 2 && this.f1412a.aF() == null) {
            this.f1412a.d(this.f1412a.getResources().getString(R.string.sdcard_removed));
            this.f1412a.e.x = true;
            this.f1412a.e(false);
            this.f1412a.c(false);
            this.f1412a.b(false);
            this.f1412a.x = true;
        }
        if (this.f1412a.e.r) {
            ArrayList<GeneralFunction.e.a> f = this.f1412a.e.b.f();
            e eVar = this.f1412a.d.e;
            this.f1412a.e.b.a(this.f1412a.d.e.a(), e.a(f, this.f1412a.e.t));
            this.f1412a.e.s = 0;
        }
        int i = this.f1412a.e.s;
        this.f1412a.e(this.f1412a.j());
        this.f1412a.h(i);
        this.f1412a.u(true);
        this.f1412a.c.c(268435455);
        this.f1412a.a(true);
        if (this.f1412a.e.b.i(i)) {
            if (this.f1412a.e.b.a(i) == 0) {
                this.f1412a.a(true, true, true);
            } else {
                this.f1412a.a(true, false, true);
            }
            this.f1412a.c.a(1059);
            a("EnterGallerySingleGroup:" + this.f1412a.ah(), 3);
            switch (this.f1412a.ah()) {
                case 0:
                    this.f1412a.ad();
                    this.b.a();
                    this.f1412a.ae();
                    break;
                case 1:
                    this.f1412a.ad();
                    this.b.a();
                    this.f1412a.ae();
                    a aVar = new a(9097);
                    aVar.a("360PlayerStatus", 1);
                    this.f1412a.a(aVar);
                    break;
            }
        } else {
            this.f1412a.a(true, false, false);
            this.f1412a.i(false);
        }
        if (this.f1412a.e.A) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            this.f1412a.v = BitmapFactory.decodeFile(this.f1412a.e.B, options);
            this.f1412a.a(this.f1412a.v);
            this.f1412a.m(true);
            this.f1412a.i(true);
        }
    }

    private void b() {
        this.f1412a.e.O = 7;
        this.f1412a.A(true);
        if (this.f1412a.aq() == 3) {
            this.f1412a.ap();
        }
        this.f1412a.o();
        this.f1412a.c.a(1040);
        if (this.f1412a.x) {
            this.f1412a.e.d = 1;
            this.f1412a.f();
            this.f1412a.x = false;
        }
        this.f1412a.u(false);
        this.f1412a.w(false);
        this.f1412a.B(false);
        this.f1412a.P();
        this.f1412a.af();
        this.f1412a.u();
        this.f1412a.c.c(268435455);
        this.f1412a.X();
        this.f1412a.V();
        this.f1412a.at();
        if (this.f1412a.w) {
            this.f1412a.aH();
            this.f1412a.w = false;
        }
        this.f1412a.i(false);
        this.f1412a.g(false);
        this.f1412a.a(false);
        this.f1412a.A = false;
        this.f1412a.D = false;
        this.f1412a.e.V = 0;
        this.f1412a.au();
        d.a((Context) this.f1412a, false);
    }

    private void c() {
        this.f1412a.d();
        this.f1412a.v();
        this.f1412a.P();
    }
}
