package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.a.a;
import GeneralFunction.d;
import GeneralFunction.k;
import GeneralFunction.o;
import ThirdParty.OpenCV.OpenCVFunc;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Space;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import ui_Controller.CustomWidget.CustomRecyclerView.CustomRecyclerView;
import ui_Controller.a.c;
import ui_Controller.b.b;
import ui_Controller.b.e;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class UI_NormalPicMakingController extends a {
    private LinearLayout A = null;
    private ImageView B = null;
    private ImageView C = null;
    private ImageView D = null;
    private ImageView E = null;
    private ImageView F = null;
    private ImageButton G = null;
    private ImageButton H = null;
    private ImageButton I = null;
    private ImageButton J = null;
    private ImageButton K = null;
    private LinearLayout L = null;
    private ImageView M = null;
    private CustomRecyclerView N = null;
    private LinearLayoutManager O = null;
    private LinearLayout P = null;
    private LinearLayout Q = null;
    private LinearLayout R = null;
    private LinearLayout S = null;
    private LinearLayout T = null;
    private TextView U = null;
    private TextView V = null;
    private LinearLayout W = null;
    private LinearLayout X = null;
    private ImageView Y = null;
    private LinearLayout Z = null;
    private Button aa = null;
    private float ab = 0.0f;
    private int ac = 0;
    private float ad = 0.0f;
    private FrameLayout ae = null;
    private LinearLayout af = null;
    private ImageButton ag = null;
    private ImageButton ah = null;
    private ImageView ai = null;
    private Space aj;
    private LinearLayout ak = null;
    private LinearLayout al = null;
    private ImageButton am = null;
    private ListView an = null;
    private TextView ao = null;
    private d ap = null;
    private View.OnTouchListener aq = new View.OnTouchListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_NormalPicMakingController.AnonymousClass6 */

        public boolean onTouch(View view, MotionEvent motionEvent) {
            int i;
            int i2 = R.drawable.gallery_button_save_press;
            switch (view.getId()) {
                case R.id.IB_ui_normalconvert_StampSelection1:
                    i = R.drawable.gallery_button_save_normal;
                    break;
                case R.id.IV_ui_normalconvert_StampSelection2:
                case R.id.IV_ui_normalconvert_StampSelection3:
                case R.id.IV_ui_normalconvert_StampSelection4:
                case R.id.IB_ui_normalconvert_StampSelection4:
                case R.id.TV_ui_normalconvert:
                case R.id.TV_ui_normalconvert_StampListIcon:
                default:
                    i = -1;
                    i2 = -1;
                    break;
                case R.id.IB_ui_normalconvert_StampSelection2:
                    i = R.drawable.gallery_button_save_normal;
                    break;
                case R.id.IB_ui_normalconvert_StampSelection3:
                    i = R.drawable.gallery_button_save_normal;
                    break;
                case R.id.IB_ui_normalconvert_ArrowIcon:
                    i = R.drawable.gallery_button_arrowback_normal;
                    i2 = R.drawable.gallery_button_arrowback_press;
                    break;
                case R.id.IB_ui_normalconvert_StampListIcon:
                    i = R.drawable.gallery_button_arrowdown_normal;
                    i2 = R.drawable.gallery_button_arrowdown_press;
                    break;
                case R.id.IB_ui_normalconvert_SaveIcon:
                    i = R.drawable.gallery_button_save_normal;
                    break;
            }
            if (motionEvent.getAction() == 0) {
                ((ImageButton) view).setImageResource(i2);
            }
            if (motionEvent.getAction() == 2 && (motionEvent.getX() < 0.0f || motionEvent.getX() > ((float) view.getWidth()) || motionEvent.getY() < 0.0f || motionEvent.getY() > ((float) view.getHeight()))) {
                ((ImageButton) view).setImageResource(i);
            }
            if (motionEvent.getAction() != 1) {
                return false;
            }
            ((ImageButton) view).setImageResource(i);
            return false;
        }
    };
    private View.OnClickListener ar = new View.OnClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_NormalPicMakingController.AnonymousClass7 */

        public void onClick(View view) {
            UI_NormalPicMakingController.this.a((UI_NormalPicMakingController) ("onclick: " + view.getId()), (String) 3);
            switch (view.getId()) {
                case R.id.LL_ui_BlackScreenLayout:
                    UI_NormalPicMakingController.this.i();
                    return;
                case R.id.IB_ui_normalconvert_StampSelection0:
                    UI_NormalPicMakingController.this.a(11050, 0);
                    return;
                case R.id.IB_ui_normalconvert_StampSelection1:
                    UI_NormalPicMakingController.this.a(11043, 0);
                    return;
                case R.id.IB_ui_normalconvert_StampSelection2:
                    UI_NormalPicMakingController.this.a(11044, 0);
                    return;
                case R.id.IB_ui_normalconvert_StampSelection3:
                    UI_NormalPicMakingController.this.a(11045, 0);
                    return;
                case R.id.IB_ui_normalconvert_StampSelection4:
                    UI_NormalPicMakingController.this.a(11051, 0);
                    return;
                case R.id.IB_ui_normalconvert_ArrowIcon:
                    if (UI_NormalPicMakingController.this.b.a(1L)) {
                        UI_NormalPicMakingController.this.a(32768, 0);
                        return;
                    }
                    return;
                case R.id.IB_ui_normalconvert_StampListIcon:
                    if (UI_NormalPicMakingController.this.b.a(PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE)) {
                        UI_NormalPicMakingController.this.a(11041, 0);
                        return;
                    }
                    return;
                case R.id.IB_ui_normalconvert_SaveIcon:
                    UI_NormalPicMakingController.this.b.a(3872, UI_NormalPicMakingController.this);
                    UI_NormalPicMakingController.this.i();
                    UI_NormalPicMakingController.this.c(true);
                    UI_NormalPicMakingController.this.b(20483);
                    return;
                case R.id.IB_ui_normalconvert_MoreIcon:
                    UI_NormalPicMakingController.this.ak.setVisibility(0);
                    UI_NormalPicMakingController.this.al.setVisibility(0);
                    return;
                case R.id.IB_ui_normalconvert_MoreSettingReturn:
                    UI_NormalPicMakingController.this.ak.setVisibility(4);
                    UI_NormalPicMakingController.this.al.setVisibility(4);
                    return;
                case R.id.IB_ui_Result_ArrowIcon:
                    UI_NormalPicMakingController.this.a(false);
                    return;
                case R.id.IB_ui_Result_SaveIcon:
                    if (UI_NormalPicMakingController.this.b.a(PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE)) {
                        UI_NormalPicMakingController.this.b.b(PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE);
                        UI_NormalPicMakingController.this.a(11042, 0);
                        return;
                    }
                    return;
                case R.id.B_ui_normalconvert_MemoryFullConfirm:
                    UI_NormalPicMakingController.this.a(11048, 0);
                    return;
                default:
                    UI_NormalPicMakingController.this.a((UI_NormalPicMakingController) "error press", (String) 3);
                    return;
            }
        }
    };
    protected UI_ModeMain b = null;
    protected b c = null;
    protected e d = null;
    protected int e = 0;
    protected boolean f = false;
    protected boolean g = false;
    protected boolean h = false;
    protected boolean i = false;
    private int j = 0;
    private int k = 0;
    private int l = -1;
    private int m = -1;
    private OrientationEventListener n = null;
    private b o = null;
    private Bitmap p = null;
    private int q = 0;
    private OpenCVFunc r;
    private ImageButton s = null;
    private ImageButton t = null;
    private ImageButton u = null;
    private ImageButton v = null;
    private TextView w = null;
    private TextView x = null;
    private LinearLayout y = null;
    private LinearLayout z = null;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        d.a("UI_NormalPicMaking", str, i2);
    }

    @Override // GeneralFunction.a.a
    public void a(int i2, long j2) {
        if (this.b != null) {
            this.b.a(new a.c.a(i2), j2);
        } else {
            a("SendUiMsg error!! Msg=" + i2, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(a.c.a aVar, long j2) {
        if (this.b != null) {
            this.b.a(aVar, j2);
        } else {
            a("SendUiMsg error!! MsgEx=" + aVar.b().what, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(int i2, long j2) {
        if (this.b != null) {
            this.b.b(i2, j2);
        } else {
            a("UI_SendAidMsg error!!", 0);
        }
    }

    @Override // GeneralFunction.a.a
    public boolean b() {
        if (this.l == 2) {
            return true;
        }
        return false;
    }

    public void onBackPressed() {
        if (this.b.a(1L)) {
            a(32768, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(Message message) {
        a("AidServer_Handler:0x" + Integer.toHexString(message.what), 3);
        switch (message.what) {
            case 20480:
                a.c.a aVar = new a.c.a(message);
                String d2 = aVar.d("path");
                long c2 = aVar.c("time");
                if (this.h) {
                    File file = new File(d2);
                    if (file.exists()) {
                        file.delete();
                    }
                    a(11052);
                    return;
                }
                String b2 = GeneralFunction.g.a.b(d2, "jpg");
                File file2 = new File(d2);
                File file3 = new File(b2);
                file2.renameTo(file3);
                int c3 = this.c.e.c();
                String substring = b2.substring(0, b2.lastIndexOf("/") + 1);
                this.c.e.a(b2.substring(b2.lastIndexOf("/") + 1, b2.length()), substring, file3.length(), 0, 1, GeneralFunction.b.a(b2), GeneralFunction.b.b(b2), 0, c2, c2, c3 + 1, 0, true, true, true, true, false, false, false, false, true);
                this.b.x();
                this.c.b.f1320a.a(this.c.e.a(), this.c.f);
                GeneralFunction.g.a.a(this, b2);
                this.N.y();
                a(11047);
                this.d.A = true;
                this.d.B = b2;
                return;
            case 20481:
            case 20482:
            default:
                return;
            case 20483:
                this.p = a(this.d.b.o(this.d.s));
                this.p = b(this.p);
                a(11042);
                return;
            case 20484:
                Long valueOf = Long.valueOf(System.currentTimeMillis());
                String b3 = GeneralFunction.g.a.b(c.d + GeneralFunction.g.a.a(valueOf, "tmp"));
                b(b3);
                a.c.a aVar2 = new a.c.a(20480);
                aVar2.a("path", b3);
                aVar2.a("time", valueOf.longValue());
                this.b.b(aVar2);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a("onCreate", 3);
        this.b = (UI_ModeMain) getApplication();
        this.b.v();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.j = displayMetrics.widthPixels;
        this.k = displayMetrics.heightPixels;
        if (this.k > this.j) {
            this.l = 1;
            setContentView(R.layout.ui_convertnormalpic_port);
        } else {
            this.l = 2;
            setContentView(R.layout.ui_convertnormalpic_port);
        }
        this.o = new b(this);
        this.c = this.b.c.k;
        this.d = this.b.c.k.b;
        this.b.a(3856, this);
        v();
        this.r = new OpenCVFunc();
        s();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        a(c(BitmapFactory.decodeFile(this.d.b.o(this.d.s), options)));
        a(12033, 100);
    }

    private void n() {
        this.ad = this.ab / ((float) this.j);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.ac = this.j;
        this.ab = ((float) this.ac) * this.ad;
        this.N.scrollBy((int) this.ab, 0);
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.d.x = true;
        if (this.d.d == 2) {
            this.g = true;
            a(8452, 100);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        a("onConfigurationChanged", 3);
        this.b.v();
        n();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.j = displayMetrics.widthPixels;
        this.k = displayMetrics.heightPixels;
        this.l = configuration.orientation;
        a.c.a aVar = new a.c.a(12038);
        aVar.a("mode", 3840);
        a(aVar, 200);
        o.a();
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onResume() {
        super.onResume();
        this.n.enable();
        a.c.a aVar = new a.c.a(12034);
        aVar.a("mode", 3840);
        a(aVar, 0);
        n();
        if (!f()) {
            a("Original File Not Exist!", 1);
            a(8452);
        }
    }

    @Override // GeneralFunction.a.a
    public void onPause() {
        super.onPause();
        this.n.disable();
        if (this.b.a() == 3872) {
            this.h = true;
        }
        a.c.a aVar = new a.c.a(12035);
        aVar.a("mode", 3840);
        a(aVar, 0);
    }

    @Override // GeneralFunction.a.a
    public void onStop() {
        super.onStop();
        a.c.a aVar = new a.c.a(12036);
        aVar.a("mode", 3840);
        a(aVar, 0);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onDestroy() {
        super.onDestroy();
        a.c.a aVar = new a.c.a(12037);
        aVar.a("mode", 3840);
        a(aVar, 0);
    }

    @Override // GeneralFunction.a.a
    public void a(Message message) {
        this.o.a(message);
    }

    /* access modifiers changed from: protected */
    public void e() {
        q();
        r();
        t();
        u();
        p();
        o();
    }

    private void o() {
        this.ak = (LinearLayout) findViewById(R.id.LL_ui_normalconvert_MoreSettingTouchArea);
        this.al = (LinearLayout) findViewById(R.id.LL_ui_normalconvert_MoreSettingTopLayout);
        this.am = (ImageButton) findViewById(R.id.IB_ui_normalconvert_MoreSettingReturn);
        this.an = (ListView) findViewById(R.id.LV_ui_normalconvert_MoreSettingListView);
        this.ao = (TextView) findViewById(R.id.TV_ui_normalconvert_MoreSetting);
        this.ao.setText((CharSequence) null);
        this.ap = new d(this, 4);
        this.ap.b(getResources().getColor(R.color.black));
        this.an.setAdapter((ListAdapter) this.ap);
        this.al.setVisibility(4);
        this.ak.setVisibility(4);
        this.am.setOnClickListener(this.ar);
        this.ak.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_NormalPicMakingController.AnonymousClass1 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                UI_NormalPicMakingController.this.a((UI_NormalPicMakingController) "AAA", (String) 0);
                UI_NormalPicMakingController.this.ak.setVisibility(4);
                UI_NormalPicMakingController.this.al.setVisibility(4);
                return true;
            }
        });
        this.an.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_NormalPicMakingController.AnonymousClass2 */

            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                switch ((int) j) {
                    case 3:
                        UI_NormalPicMakingController.this.a((UI_NormalPicMakingController) ("objMoreSettingAdapter.getItem(0) " + UI_NormalPicMakingController.this.ap.getItem(0)), (String) 3);
                        if (UI_NormalPicMakingController.this.ap.getItem(0).equals(UI_NormalPicMakingController.this.getResources().getString(R.string.panorama_hide))) {
                            UI_NormalPicMakingController.this.ap.a(0, UI_NormalPicMakingController.this.getResources().getString(R.string.panorama_show));
                            UI_NormalPicMakingController.this.U.setVisibility(4);
                            UI_NormalPicMakingController.this.V.setVisibility(4);
                        } else {
                            UI_NormalPicMakingController.this.ap.a(0, UI_NormalPicMakingController.this.getResources().getString(R.string.panorama_hide));
                            UI_NormalPicMakingController.this.U.setVisibility(0);
                            UI_NormalPicMakingController.this.V.setVisibility(0);
                        }
                        UI_NormalPicMakingController.this.ap.b();
                        return;
                    default:
                        return;
                }
            }
        });
    }

    private void p() {
        this.ae = (FrameLayout) findViewById(R.id.FL_ui_Result_Layout);
        this.af = (LinearLayout) findViewById(R.id.LL_ui_Result_TopLayout);
        this.ag = (ImageButton) findViewById(R.id.IB_ui_Result_ArrowIcon);
        this.ah = (ImageButton) findViewById(R.id.IB_ui_Result_SaveIcon);
        this.ai = (ImageView) findViewById(R.id.IV_ui_Result_Panorama);
        this.ah.setImageResource(R.drawable.gallery_top_save_black);
        this.ag.setImageResource(R.drawable.gallery_top_back_black);
        this.ag.setVisibility(4);
        this.ag.setOnClickListener(this.ar);
        this.ah.setOnClickListener(this.ar);
        this.ae.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_NormalPicMakingController.AnonymousClass3 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.ae.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        this.i = z2;
        if (z2) {
            if (this.p != null) {
                this.ai.setImageBitmap(this.p);
            }
            this.ae.setVisibility(0);
            return;
        }
        this.ae.setVisibility(4);
        m();
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        if (new File(this.d.b.o(this.d.s)).exists()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void g() {
        if (this.ai != null) {
            this.ai.setImageBitmap(null);
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (this.A.getVisibility() != 0) {
            this.A.setVisibility(0);
            this.t.setImageResource(R.drawable.gallery_top_logo_green);
            this.w.setTextColor(getResources().getColor(R.color.iconcolor));
            this.P.setOnClickListener(this.ar);
            return;
        }
        this.A.setVisibility(4);
        this.t.setImageResource(R.drawable.gallery_top_logo_black);
        this.w.setTextColor(getResources().getColor(R.color.white));
        this.P.setOnClickListener(null);
        this.P.setClickable(false);
    }

    /* access modifiers changed from: protected */
    public void i() {
        this.A.setVisibility(4);
        this.t.setImageResource(R.drawable.gallery_top_logo_black);
        this.w.setTextColor(getResources().getColor(R.color.white));
        this.P.setOnClickListener(null);
        this.P.setClickable(false);
    }

    /* access modifiers changed from: protected */
    public void c(int i2) {
        this.G.setImageResource(0);
        this.H.setImageResource(0);
        this.I.setImageResource(0);
        this.J.setImageResource(0);
        this.K.setImageResource(0);
        switch (i2) {
            case 0:
                this.M.setImageResource(0);
                this.G.setImageResource(R.drawable.gallery_logolist_cursor);
                this.q = 0;
                return;
            case 1:
                this.M.setImageResource(R.drawable.gallery_logolist_logo_1_500x70);
                this.H.setImageResource(R.drawable.gallery_logolist_cursor);
                this.q = 1;
                return;
            case 2:
                this.M.setImageResource(R.drawable.gallery_logolist_logo_2_500x70);
                this.I.setImageResource(R.drawable.gallery_logolist_cursor);
                this.q = 2;
                return;
            case 3:
                this.M.setImageResource(R.drawable.gallery_logolist_logo_3_500x70);
                this.J.setImageResource(R.drawable.gallery_logolist_cursor);
                this.q = 3;
                return;
            case 4:
                this.M.setImageResource(R.drawable.gallery_logolist_logo_4_500x70);
                this.K.setImageResource(R.drawable.gallery_logolist_cursor);
                this.q = 4;
                return;
            default:
                return;
        }
    }

    private void q() {
        this.t = (ImageButton) findViewById(R.id.IB_ui_normalconvert_StampListIcon);
        this.u = (ImageButton) findViewById(R.id.IB_ui_normalconvert_SaveIcon);
        this.v = (ImageButton) findViewById(R.id.IB_ui_normalconvert_MoreIcon);
        this.s = (ImageButton) findViewById(R.id.IB_ui_normalconvert_ArrowIcon);
        this.t.setImageResource(R.drawable.gallery_top_logo_black);
        this.u.setImageResource(R.drawable.gallery_top_save_black);
        this.v.setImageResource(R.drawable.gallery_top_more_black);
        this.s.setImageResource(R.drawable.gallery_top_back_black);
        this.s.setVisibility(4);
        this.s.setOnClickListener(this.ar);
        this.t.setOnClickListener(this.ar);
        this.u.setOnClickListener(this.ar);
        this.v.setOnClickListener(this.ar);
        this.w = (TextView) findViewById(R.id.TV_ui_normalconvert_StampListIcon);
        this.x = (TextView) findViewById(R.id.TV_ui_normalconvert_SaveIcon);
        int a2 = k.a(this, (float) (Math.min(this.j, this.k) / 40));
        this.w.setText(getResources().getString(R.string.edit_logo));
        this.x.setText(getResources().getString(R.string.edit_save));
        GeneralFunction.c f2 = new GeneralFunction.c(a2).d((int) (((double) a2) * 0.9d)).o((int) (((double) a2) * 0.9d)).f((int) (((double) a2) * 0.9d));
        this.w.setTextSize((float) f2.a());
        this.x.setTextSize((float) f2.a());
    }

    private void r() {
        this.L = (LinearLayout) findViewById(R.id.LL_ui_normalconvert_StampIcon);
        this.L.setVisibility(4);
        this.M = (ImageView) findViewById(R.id.IV_ui_normalconvert_StampIcon);
        this.M.setVisibility(4);
        this.aj = (Space) findViewById(R.id.SP_normalconvert_StampSpace);
        int min = Math.min(this.j, this.k);
        Math.max(this.j, this.k);
        if (this.P != null) {
            min = this.P.getWidth();
            this.P.getHeight();
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.M.getLayoutParams());
        layoutParams.height = (((min * 3) / 10) * 120) / 1612;
        layoutParams.leftMargin = (min * 120) / 5376;
        this.M.setLayoutParams(layoutParams);
        this.A = (LinearLayout) findViewById(R.id.LL_ui_normalconvert_StampSelection);
        this.A.setBackgroundResource(R.drawable.gallery_logolist);
        this.A.setVisibility(4);
        this.B = (ImageView) findViewById(R.id.IV_ui_normalconvert_StampSelection0);
        this.C = (ImageView) findViewById(R.id.IV_ui_normalconvert_StampSelection1);
        this.D = (ImageView) findViewById(R.id.IV_ui_normalconvert_StampSelection2);
        this.E = (ImageView) findViewById(R.id.IV_ui_normalconvert_StampSelection3);
        this.F = (ImageView) findViewById(R.id.IV_ui_normalconvert_StampSelection4);
        this.B.setImageResource(R.drawable.gallery_logolist_logo_0);
        this.C.setImageResource(R.drawable.gallery_logolist_logo_1);
        this.D.setImageResource(R.drawable.gallery_logolist_logo_2);
        this.E.setImageResource(R.drawable.gallery_logolist_logo_3);
        this.F.setImageResource(R.drawable.gallery_logolist_logo_4);
        this.G = (ImageButton) findViewById(R.id.IB_ui_normalconvert_StampSelection0);
        this.H = (ImageButton) findViewById(R.id.IB_ui_normalconvert_StampSelection1);
        this.I = (ImageButton) findViewById(R.id.IB_ui_normalconvert_StampSelection2);
        this.J = (ImageButton) findViewById(R.id.IB_ui_normalconvert_StampSelection3);
        this.K = (ImageButton) findViewById(R.id.IB_ui_normalconvert_StampSelection4);
        this.G.setOnClickListener(this.ar);
        this.H.setOnClickListener(this.ar);
        this.I.setOnClickListener(this.ar);
        this.J.setOnClickListener(this.ar);
        this.K.setOnClickListener(this.ar);
        this.y = (LinearLayout) findViewById(R.id.LL_ui_normalconvert_TopLayoutBlankArea);
        this.z = (LinearLayout) findViewById(R.id.LL_ui_normalconvert_TopLayout);
        c(0);
    }

    private void s() {
        this.P = (LinearLayout) findViewById(R.id.LL_ui_BlackScreenLayout);
        this.Q = (LinearLayout) findViewById(R.id.LL_ui_BlackScreen_TopLayout);
        this.R = (LinearLayout) findViewById(R.id.LL_ui_BlackScreen_BotLayout);
        this.S = (LinearLayout) findViewById(R.id.LL_ui_TransparentScreen);
        this.T = (LinearLayout) findViewById(R.id.LL_ui_Old_StampIcon);
        this.U = (TextView) findViewById(R.id.TV_ui_HintTextTop);
        this.V = (TextView) findViewById(R.id.TV_ui_HintTextBottom);
        this.W = (LinearLayout) findViewById(R.id.LL_ui_DottedLine);
        this.U.setText(R.string.panorama_hint_top);
        this.V.setText(R.string.panorama_hint);
        this.N = (CustomRecyclerView) findViewById(R.id.CRV_ui_normalconvert_Panorama);
        this.O = new LinearLayoutManager(getApplicationContext(), 0, false);
        this.N.setLayoutManager(this.O);
        this.N.setAdapter(this.N.getAdapter());
        this.ac = this.j;
    }

    /* access modifiers changed from: protected */
    public void j() {
        this.N.a((RecyclerView.a) null, true);
    }

    /* access modifiers changed from: protected */
    public void k() {
        this.N.y();
        j();
    }

    private void t() {
        this.Z = (LinearLayout) findViewById(R.id.LL_ui_normalconvert_MemoryFullLayout);
        this.Z.setVisibility(4);
        this.Z.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_NormalPicMakingController.AnonymousClass4 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.aa = (Button) findViewById(R.id.B_ui_normalconvert_MemoryFullConfirm);
        this.aa.setOnClickListener(this.ar);
    }

    /* access modifiers changed from: protected */
    public void b(boolean z2) {
        if (z2) {
            this.Z.setVisibility(0);
        } else {
            this.Z.setVisibility(4);
        }
    }

    private void u() {
        this.X = (LinearLayout) findViewById(R.id.LL_ui_normalconvert_ProgressLayout);
        this.X.setVisibility(4);
        this.X.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_NormalPicMakingController.AnonymousClass5 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.Y = (ImageView) findViewById(R.id.IV_ui_normalconvert_ProgressIcon);
    }

    /* access modifiers changed from: protected */
    public void c(boolean z2) {
        if (z2) {
            this.X.setVisibility(0);
            ((AnimationDrawable) this.Y.getDrawable()).start();
            this.d.z = true;
            return;
        }
        this.X.setVisibility(4);
        ((AnimationDrawable) this.Y.getDrawable()).stop();
        this.d.z = false;
    }

    /* access modifiers changed from: protected */
    public Bitmap a(String str) {
        a("path: " + str, 3);
        float f2 = 360.0f * (this.ab / ((float) this.ac));
        a("Panorama degree: " + f2, 3);
        long currentTimeMillis = System.currentTimeMillis();
        String str2 = c.e + "result.jpg";
        int c2 = d.c(str);
        if (c2 != 3) {
            c2 = 0;
        }
        this.r.PanoramaPath(str, str2, (double) f2, c2);
        a("Panorama get:" + (System.currentTimeMillis() - currentTimeMillis), 0);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        Bitmap decodeFile = BitmapFactory.decodeFile(str2, options);
        new File(str2).delete();
        return decodeFile;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        FileOutputStream fileOutputStream;
        Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.JPEG;
        try {
            fileOutputStream = new FileOutputStream(str);
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            fileOutputStream = null;
        }
        if (!(this.p == null || fileOutputStream == null)) {
            this.p.compress(compressFormat, 100, fileOutputStream);
        }
        if (fileOutputStream != null) {
            try {
                fileOutputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        m();
    }

    /* access modifiers changed from: protected */
    public void l() {
        this.L.setVisibility(0);
        this.M.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void m() {
        if (this.p != null) {
            this.p.recycle();
            this.p = null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Bitmap bitmap) {
        this.N.setBitmap(bitmap);
        this.N.a(new RecyclerView.n() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_NormalPicMakingController.AnonymousClass8 */

            @Override // android.support.v7.widget.RecyclerView.n
            public void a(RecyclerView recyclerView, int i) {
                super.a(recyclerView, i);
            }

            @Override // android.support.v7.widget.RecyclerView.n
            public void a(RecyclerView recyclerView, int i, int i2) {
                super.a(recyclerView, i, i2);
                recyclerView.f();
                UI_NormalPicMakingController.this.ab += (float) i;
                if (UI_NormalPicMakingController.this.ab > ((float) UI_NormalPicMakingController.this.ac)) {
                    UI_NormalPicMakingController.this.ab -= (float) UI_NormalPicMakingController.this.ac;
                }
                if (UI_NormalPicMakingController.this.ab < ((float) (-UI_NormalPicMakingController.this.ac))) {
                    UI_NormalPicMakingController.this.ab += (float) UI_NormalPicMakingController.this.ac;
                }
            }
        });
    }

    private Bitmap b(Bitmap bitmap) {
        Bitmap decodeResource;
        switch (this.q) {
            case 1:
                decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.gallery_logolist_logo_1_original);
                break;
            case 2:
                decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.gallery_logolist_logo_2_original);
                break;
            case 3:
                decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.gallery_logolist_logo_3_original);
                break;
            case 4:
                decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.gallery_logolist_logo_4_original);
                break;
            default:
                return bitmap;
        }
        if (decodeResource == null) {
            return bitmap;
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, (((bitmap.getHeight() * decodeResource.getWidth()) * 120) / 1612) / decodeResource.getHeight(), (bitmap.getHeight() * 120) / 1612, true);
        a("bmScaledWatermark Height: " + createScaledBitmap.getHeight() + ", Width: " + createScaledBitmap.getWidth(), 3);
        decodeResource.recycle();
        if (createScaledBitmap == null) {
            return bitmap;
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, new Matrix(), null);
        canvas.drawBitmap(createScaledBitmap, (float) ((bitmap.getWidth() * 120) / 5376), (float) ((bitmap.getHeight() * 1457) / 1612), (Paint) null);
        canvas.save();
        canvas.restore();
        bitmap.recycle();
        createScaledBitmap.recycle();
        return createBitmap;
    }

    private Bitmap c(Bitmap bitmap) {
        int c2 = d.c(this.d.b.o(this.d.s));
        Matrix matrix = new Matrix();
        if (c2 != 3) {
            return bitmap;
        }
        matrix.postRotate(180.0f);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap.recycle();
        return createBitmap;
    }

    private void v() {
        this.n = new OrientationEventListener(this) {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_NormalPicMakingController.AnonymousClass9 */

            public void onOrientationChanged(int i) {
                if (i != -1) {
                    if (i > 350 || i < 10) {
                        if (UI_NormalPicMakingController.this.m != 1) {
                            UI_NormalPicMakingController.this.d(1);
                            UI_NormalPicMakingController.this.m = 1;
                        }
                    } else if (i > 170 && i < 190 && UI_NormalPicMakingController.this.m != 9) {
                        UI_NormalPicMakingController.this.d(9);
                        UI_NormalPicMakingController.this.m = 9;
                    }
                }
            }
        };
    }

    public void d(int i2) {
        d.a(this, i2);
    }
}
