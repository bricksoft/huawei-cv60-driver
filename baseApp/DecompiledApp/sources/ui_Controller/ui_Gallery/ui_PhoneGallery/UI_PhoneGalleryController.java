package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.Player.player.SphericalVideoPlayer;
import GeneralFunction.Player.player.h;
import GeneralFunction.a.a;
import GeneralFunction.d;
import GeneralFunction.g;
import GeneralFunction.k;
import GeneralFunction.l.a;
import GeneralFunction.l.c;
import GeneralFunction.o;
import a.c.a;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.InputDeviceCompat;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.exoplayer.ExoPlayer;
import com.huawei.cvIntl60.R;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import ui_Controller.CustomWidget.ListHorizontalScrollView.ListHorizontalScrollView;
import ui_Controller.CustomWidget.SingleHorizontalScrollView.SingleHorizontalScrollView;
import ui_Controller.CustomWidget.ZoomableImageView;
import ui_Controller.CustomWidget.a;
import ui_Controller.CustomWidget.a.a.a;
import ui_Controller.CustomWidget.b;
import ui_Controller.b.e;
import ui_Controller.ui_Gallery.b;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class UI_PhoneGalleryController extends a {
    private static int G = 500;
    protected boolean A = false;
    protected a.AbstractC0003a B = new a.AbstractC0003a() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass1 */

        @Override // GeneralFunction.l.a.AbstractC0003a
        public void a(c cVar) {
            UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("ShareResultCallback result: " + cVar.f122a), (String) 3);
            if (UI_PhoneGalleryController.this.e.f > 0) {
                UI_PhoneGalleryController.this.a(9054, 500L);
                UI_PhoneGalleryController.this.e.j.clear();
            }
            if (UI_PhoneGalleryController.this.p()) {
                UI_PhoneGalleryController.this.a(9129);
            }
        }
    };
    protected boolean C = false;
    protected boolean D = false;
    protected PowerManager.WakeLock E = null;
    public PopupWindow.OnDismissListener F = new PopupWindow.OnDismissListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass31 */

        public void onDismiss() {
            UI_PhoneGalleryController.this.cl.setImageResource(R.drawable.gallery_bottom_edit);
            UI_PhoneGalleryController.this.cp.setTextColor(UI_PhoneGalleryController.this.getResources().getColor(R.color.white));
            UI_PhoneGalleryController.this.K(false);
            UI_PhoneGalleryController.this.L(false);
            UI_PhoneGalleryController.this.a(9125, 100L);
        }
    };
    private Context H = this;
    private Activity I = this;
    private boolean J = false;
    private int K = 0;
    private int L = 0;
    private int M = -1;
    private int N = -1;
    private Space O = null;
    private StickyGridHeadersGridView P = null;
    private TabLayout Q;
    private ImageButton R;
    private boolean S = false;
    private PopupWindow T;
    private PopupWindow U;
    private PopupWindow V;
    private ArrayList<String> W;
    private ArrayList<String> X;
    private ImageButton Y = null;
    private ImageButton Z = null;
    private ImageButton aA = null;
    private ImageButton aB = null;
    private ImageButton aC = null;
    private ImageButton aD = null;
    private ImageButton aE = null;
    private ImageButton aF = null;
    private ImageButton aG = null;
    private ImageButton aH = null;
    private TextView aI = null;
    private TextView aJ = null;
    private TextView aK = null;
    private TextView aL = null;
    private TextView aM = null;
    private TextView aN = null;
    private TextView aO = null;
    private TextView aP = null;
    private TextView aQ = null;
    private TextView aR = null;
    private TextView aS = null;
    private TextView aT = null;
    private LinearLayout aU = null;
    private TextView aV = null;
    private ArrayList<ImageButton> aW = null;
    private ArrayList<TextView> aX = null;
    private LinearLayout aY = null;
    private ArrayList<ImageButton> aZ = null;
    private ImageButton aa = null;
    private ImageButton ab = null;
    private ImageButton ac = null;
    private ImageButton ad = null;
    private ImageButton ae = null;
    private ImageButton af = null;
    private ImageButton ag = null;
    private ImageButton ah = null;
    private ImageButton ai = null;
    private ImageButton aj = null;
    private TextView ak = null;
    private TextView al = null;
    private TextView am = null;
    private TextView an = null;
    private TextView ao = null;
    private TextView ap = null;
    private TextView aq = null;
    private TextView ar = null;
    private TextView as = null;
    private TextView at = null;
    private TextView au = null;
    private TextView av = null;
    private ImageButton aw = null;
    private ImageButton ax = null;
    private ImageButton ay = null;
    private ImageButton az = null;
    protected c b = null;
    private ListView bA;
    private d bB;
    private boolean bC = false;
    private boolean bD = false;
    private final String[] bE = {"android.permission.WRITE_EXTERNAL_STORAGE"};
    private GeneralFunction.l.a bF;
    private String bG = null;
    private View.OnClickListener bH = new View.OnClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass11 */

        public void onClick(View view) {
            File file;
            if (UI_PhoneGalleryController.this.u((UI_PhoneGalleryController) view.getId())) {
                UI_PhoneGalleryController.this.t(false);
                ArrayList<File> arrayList = new ArrayList<>();
                if (UI_PhoneGalleryController.this.e.d == 3 || UI_PhoneGalleryController.this.e.A) {
                    if (UI_PhoneGalleryController.this.c.a() == 1056 || UI_PhoneGalleryController.this.e.A) {
                        if (UI_PhoneGalleryController.this.e.A) {
                            file = new File(UI_PhoneGalleryController.this.e.B);
                        } else {
                            file = new File(UI_PhoneGalleryController.this.e.b.o(UI_PhoneGalleryController.this.e.s));
                        }
                        UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("GetFullPath: " + UI_PhoneGalleryController.this.e.b.o(UI_PhoneGalleryController.this.e.s)), (String) 3);
                        arrayList.add(file);
                        UI_PhoneGalleryController.this.e.ah = view.getId();
                        UI_PhoneGalleryController.this.e.am = arrayList;
                        UI_PhoneGalleryController.this.a(8976);
                        return;
                    }
                    UI_PhoneGalleryController.this.aJ();
                    for (int i = 0; i < UI_PhoneGalleryController.this.e.j.size(); i++) {
                        arrayList.add(new File(UI_PhoneGalleryController.this.e.b.o(UI_PhoneGalleryController.this.e.j.get(i).intValue())));
                        if (!arrayList.get(i).exists()) {
                            UI_PhoneGalleryController.this.c(UI_PhoneGalleryController.this.getResources().getString(R.string.file_not_exist));
                            return;
                        }
                    }
                    UI_PhoneGalleryController.this.e.ah = view.getId();
                    UI_PhoneGalleryController.this.e.am = arrayList;
                    UI_PhoneGalleryController.this.a(8976);
                } else if (UI_PhoneGalleryController.this.c.a() == 1059) {
                    if (UI_PhoneGalleryController.this.bU.isPlaying()) {
                        UI_PhoneGalleryController.this.D();
                        UI_PhoneGalleryController.this.C();
                        UI_PhoneGalleryController.this.an();
                    }
                    File file2 = new File(UI_PhoneGalleryController.this.e.b.o(UI_PhoneGalleryController.this.e.s));
                    UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("GetFullPath: " + UI_PhoneGalleryController.this.e.b.o(UI_PhoneGalleryController.this.e.s)), (String) 3);
                    arrayList.add(file2);
                    UI_PhoneGalleryController.this.e.j.clear();
                    UI_PhoneGalleryController.this.e.j.add(Integer.valueOf(UI_PhoneGalleryController.this.e.s));
                    UI_PhoneGalleryController.this.e.ah = view.getId();
                    UI_PhoneGalleryController.this.e.am = arrayList;
                    UI_PhoneGalleryController.this.a(8976);
                } else {
                    UI_PhoneGalleryController.this.aJ();
                    for (int i2 = 0; i2 < UI_PhoneGalleryController.this.e.j.size(); i2++) {
                        arrayList.add(new File(UI_PhoneGalleryController.this.e.b.o(UI_PhoneGalleryController.this.e.j.get(i2).intValue())));
                        if (!arrayList.get(i2).exists()) {
                            UI_PhoneGalleryController.this.d(0);
                            UI_PhoneGalleryController.this.a(9053, 0L);
                            return;
                        }
                    }
                    UI_PhoneGalleryController.this.e.ah = view.getId();
                    UI_PhoneGalleryController.this.e.am = arrayList;
                    UI_PhoneGalleryController.this.a(8976);
                }
            }
        }
    };
    private StickyGridHeadersGridView.c bI = new StickyGridHeadersGridView.c() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass14 */

        @Override // com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView.c
        public void a(AdapterView<?> adapterView, View view, long j) {
            UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("click header " + j), (String) 4);
            if (UI_PhoneGalleryController.this.c.a(1024L) && UI_PhoneGalleryController.this.e.f != 0) {
                a.c.a aVar = new a.c.a(9020);
                aVar.a("header id", (int) j);
                UI_PhoneGalleryController.this.a(aVar, 0);
            }
        }
    };
    private AdapterView.OnItemLongClickListener bJ = new AdapterView.OnItemLongClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass15 */

        @Override // android.widget.AdapterView.OnItemLongClickListener
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
            UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) "ItemLongClick", (String) 3);
            if (UI_PhoneGalleryController.this.e.f != 5) {
                UI_PhoneGalleryController.this.e.f = 5;
                UI_PhoneGalleryController.this.e.m.c(5);
                UI_PhoneGalleryController.this.e.m.b();
                UI_PhoneGalleryController.this.f(true);
                UI_PhoneGalleryController.this.r(false);
            }
            UI_PhoneGalleryController.this.v((UI_PhoneGalleryController) i);
            return true;
        }
    };
    private AdapterView.OnItemClickListener bK = new AdapterView.OnItemClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass16 */

        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("click item " + i), (String) 4);
            if (UI_PhoneGalleryController.this.c.a(1024L)) {
                if (UI_PhoneGalleryController.this.e.f > 0) {
                    UI_PhoneGalleryController.this.v((UI_PhoneGalleryController) i);
                } else if (UI_PhoneGalleryController.this.c.a() == 1040) {
                    a.c.a aVar = new a.c.a(8965);
                    aVar.a("SelectIndex", i);
                    UI_PhoneGalleryController.this.a(aVar, 0);
                }
            }
        }
    };
    private View.OnClickListener bL = new View.OnClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass17 */

        public void onClick(View view) {
            String string;
            String string2;
            int i;
            switch (view.getId()) {
                case R.id.IB_ui_multiview_btnCancel:
                    UI_PhoneGalleryController.this.c.a(32768, 0);
                    return;
                case R.id.TV_ui_multiview_SelectCount:
                case R.id.SPACE_ui_multiview_MoveAndShare:
                case R.id.GV_ui_multiview_HeaderGridView:
                case R.id.LL_ui_multiview_NoFileLayout:
                case R.id.IV_ui_multiview_NoFileImage:
                case R.id.TV_ui_multiview_NoFileText:
                default:
                    UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) "Error Button", (String) 3);
                    return;
                case R.id.IB_ui_multiview_selectAll:
                    UI_PhoneGalleryController.this.t();
                    return;
                case R.id.IB_ui_multiview_btnMoveTab:
                    if (UI_PhoneGalleryController.this.e.d == 1) {
                        if (UI_PhoneGalleryController.this.e.m.e() == 1) {
                            string2 = UI_PhoneGalleryController.this.getResources().getString(R.string.move_single_to_sdcard_dialog);
                        } else if (UI_PhoneGalleryController.this.e.m.e() > 1) {
                            string2 = UI_PhoneGalleryController.this.getResources().getString(R.string.move_multi_to_sdcard_dialog);
                        } else {
                            return;
                        }
                        o.a(UI_PhoneGalleryController.this.H, true, true, UI_PhoneGalleryController.this.getResources().getString(R.string.move), string2, new String[]{UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_cancel), UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_ok_for_two_button)}, new int[]{0, 9045}, true);
                        return;
                    } else if (UI_PhoneGalleryController.this.e.d == 2) {
                        if (UI_PhoneGalleryController.this.e.m.e() == 1) {
                            string = UI_PhoneGalleryController.this.getResources().getString(R.string.move_single_to_phone_dialog);
                        } else if (UI_PhoneGalleryController.this.e.m.e() > 1) {
                            string = UI_PhoneGalleryController.this.getResources().getString(R.string.move_multi_to_phone_dialog);
                        } else {
                            return;
                        }
                        o.a(UI_PhoneGalleryController.this.H, true, true, UI_PhoneGalleryController.this.getResources().getString(R.string.move), string, new String[]{UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_cancel), UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_ok_for_two_button)}, new int[]{0, 9046}, true);
                        return;
                    } else {
                        return;
                    }
                case R.id.IB_ui_multiview_btnShareTab:
                    if (UI_PhoneGalleryController.this.e.m.e() >= 1) {
                        UI_PhoneGalleryController.this.aJ();
                        int i2 = 0;
                        int i3 = 0;
                        int i4 = 0;
                        while (i2 < UI_PhoneGalleryController.this.e.j.size()) {
                            if (UI_PhoneGalleryController.this.e.b.l(UI_PhoneGalleryController.this.e.j.get(i2).intValue())) {
                                i4++;
                                i = i3;
                            } else {
                                i = i3 + 1;
                            }
                            i2++;
                            i3 = i;
                        }
                        UI_PhoneGalleryController.this.e.j.clear();
                        if (i4 > 0 && i3 > 0) {
                            UI_PhoneGalleryController.this.a(true, UI_PhoneGalleryController.this.getResources().getString(R.string.cant_show_photo_video_description));
                            g.a(4, ExoPlayer.Factory.DEFAULT_MIN_BUFFER_MS);
                            return;
                        } else if (i3 > 9) {
                            UI_PhoneGalleryController.this.a(true, UI_PhoneGalleryController.this.getResources().getString(R.string.only_nine_photo_description));
                            g.a(4, ExoPlayer.Factory.DEFAULT_MIN_BUFFER_MS);
                            return;
                        } else if (i4 > 1) {
                            UI_PhoneGalleryController.this.a(true, UI_PhoneGalleryController.this.getResources().getString(R.string.only_one_video_description));
                            g.a(4, ExoPlayer.Factory.DEFAULT_MIN_BUFFER_MS);
                            return;
                        } else {
                            UI_PhoneGalleryController.this.c.a(new a.c.a(9008));
                            return;
                        }
                    } else {
                        return;
                    }
                case R.id.IB_ui_multiview_btnDeleteTab:
                    UI_PhoneGalleryController.this.c.a(new a.c.a(9001));
                    return;
                case R.id.IB_ui_ShootingModeButton:
                    UI_PhoneGalleryController.this.c.a(8448, 0);
                    return;
            }
        }
    };
    private LinearLayout bM = null;
    private LinearLayout bN = null;
    private b bO = null;
    private ui_Controller.ui_Gallery.b bP = null;
    private LinearLayout bQ = null;
    private ListHorizontalScrollView bR = null;
    private SingleHorizontalScrollView bS = null;
    private LinearLayout bT = null;
    private SphericalVideoPlayer bU = null;
    private LinearLayout bV = null;
    private ImageView bW = null;
    private TextView bX = null;
    private Button bY = null;
    private LinearLayout bZ = null;
    private ArrayList<TextView> ba = null;
    private LinearLayout bb = null;
    private ImageButton bc = null;
    private TextView bd = null;
    private Button be = null;
    private ArrayList<ImageView> bf = new ArrayList<>(9);
    private EditText bg = null;
    private ImageView bh = null;
    private TextView bi = null;
    private LinearLayout bj = null;
    private WebView bk = null;
    private Button bl = null;
    private Button bm = null;
    private LinearLayout bn = null;
    private TextView bo = null;
    private OrientationEventListener bp = null;
    private LinearLayout bq = null;
    private FrameLayout br = null;
    private ZoomableImageView bs = null;
    private LinearLayout bt;
    private ImageButton bu;
    private ImageButton bv;
    private LinearLayout bw;
    private View bx;
    private TextView by;
    private ImageButton bz;
    protected UI_ModeMain c = null;
    private d cA = null;
    private FrameLayout cB = null;
    private b.a cC = new b.a() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass24 */

        @Override // ui_Controller.ui_Gallery.b.a
        public void a(int i, Bitmap bitmap) {
            UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("onCurrentImgChanged " + i + " " + bitmap), (String) 4);
            a.c.a aVar = new a.c.a(9100);
            aVar.a("update index", i);
            UI_PhoneGalleryController.this.a(aVar, 0);
        }
    };
    private b.AbstractC0102b cD = new b.AbstractC0102b() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass25 */

        @Override // ui_Controller.ui_Gallery.b.AbstractC0102b
        public void a(int i, Bitmap bitmap) {
            int i2;
            UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("onMainImageLoadDone " + i + " " + UI_PhoneGalleryController.this.e.s), (String) 4);
            if (UI_PhoneGalleryController.this.e.d == 3 && !UI_PhoneGalleryController.this.bP.d(UI_PhoneGalleryController.this.e.s)) {
                UI_PhoneGalleryController.this.g(false);
            }
            if ((UI_PhoneGalleryController.this.c.a() == 1059 || UI_PhoneGalleryController.this.c.a() == 1056) && i == (i2 = UI_PhoneGalleryController.this.e.s) && UI_PhoneGalleryController.this.w((UI_PhoneGalleryController) i2)) {
                UI_PhoneGalleryController.this.g(true);
                if (!UI_PhoneGalleryController.this.A) {
                    UI_PhoneGalleryController.this.A = true;
                    UI_PhoneGalleryController.this.a(9139, 0L);
                }
            }
            a.c.a aVar = new a.c.a(9099);
            aVar.a("update index", i);
            aVar.a("update bitmap", new a.C0010a(bitmap));
            UI_PhoneGalleryController.this.a(aVar, 0);
        }
    };
    private SphericalVideoPlayer.a cE = new SphericalVideoPlayer.a() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass28 */

        @Override // GeneralFunction.Player.player.SphericalVideoPlayer.a
        public void a(int i) {
            switch (i) {
                case 0:
                    if (UI_PhoneGalleryController.this.C) {
                        UI_PhoneGalleryController.this.C = false;
                        break;
                    } else {
                        UI_PhoneGalleryController.this.a(9120, 0L);
                        break;
                    }
                case 1:
                    if (!UI_PhoneGalleryController.this.A()) {
                        if (!UI_PhoneGalleryController.this.m()) {
                            UI_PhoneGalleryController.this.g(true);
                            UI_PhoneGalleryController.this.b(20501, 0);
                            break;
                        }
                    } else {
                        UI_PhoneGalleryController.this.v(true);
                        break;
                    }
                    break;
                case 2:
                    if (UI_PhoneGalleryController.this.C) {
                        UI_PhoneGalleryController.this.C = false;
                        break;
                    } else {
                        UI_PhoneGalleryController.this.a(9118, 0L);
                        break;
                    }
                case 3:
                    UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) "MediaControllerAdditionalButton_Drag", (String) 2);
                    if (UI_PhoneGalleryController.this.C) {
                        UI_PhoneGalleryController.this.C = false;
                        break;
                    } else {
                        UI_PhoneGalleryController.this.a(9122, 0L);
                        break;
                    }
                case 4:
                    UI_PhoneGalleryController.this.a(32768);
                    break;
                case 5:
                    UI_PhoneGalleryController.this.a(32769);
                    break;
            }
            UI_PhoneGalleryController.this.bi();
        }

        @Override // GeneralFunction.Player.player.SphericalVideoPlayer.a
        public void a() {
            UI_PhoneGalleryController.this.onUserInteraction();
        }
    };
    private View.OnClickListener cF = new View.OnClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass30 */

        public void onClick(View view) {
            UI_PhoneGalleryController.this.onUserInteraction();
            switch (view.getId()) {
                case R.id.LL_ui_singleview_BottomList_Drag_Layout:
                case R.id.IB_ui_singleview_BottomList_Drag:
                    UI_PhoneGalleryController.this.X();
                    if (UI_PhoneGalleryController.this.e.N == 3) {
                        UI_PhoneGalleryController.this.z(false);
                    }
                    if (UI_PhoneGalleryController.this.e.N != 2) {
                        UI_PhoneGalleryController.this.b(2, false);
                        return;
                    }
                    return;
                case R.id.TV_ui_singleview_BottomList_Drag:
                case R.id.TV_ui_singleview_BottomList_Gyroscope:
                case R.id.TV_ui_singleview_BottomList_VR:
                case R.id.LL_ui_singleview_BottomList_ViewType_Layout:
                case R.id.TV_ui_singleview_BottomList_Fisheye:
                case R.id.TV_ui_singleview_BottomList_Perspective:
                case R.id.TV_ui_singleview_BottomList_CrystalBall:
                case R.id.TV_ui_singleview_BottomList_LittlePlanet:
                case R.id.LL_ui_singleview_EditTool_Layout:
                case R.id.TV_ui_singleview_EditTool_ConvertToGif:
                case R.id.TV_ui_singleview_EditTool_NormalPicOrMovieEdit:
                case R.id.CHSV_ui_singleview_ImageView:
                case R.id.IB_ui_singleview_btnMultiViewTab:
                case R.id.LL_ui_singleview_videoLayout:
                case R.id.VV_ui_singleview_videoFrame:
                case R.id.LL_ui_singleview_videoPlayButtonLayout:
                case R.id.B_ui_singleview_videoPlayButton:
                case R.id.LL_ui_singleview_Settinglayout:
                case R.id.TV_ui_singleview_MoreSettingText:
                case R.id.IV_ui_singleview_MoreSettingBtn:
                case R.id.LL_ui_singleview_Snapshot_Layout:
                case R.id.TV_ui_singleview_Snapshot_Cancel:
                default:
                    return;
                case R.id.LL_ui_singleview_BottomList_Gyroscope_Layout:
                case R.id.IB_ui_singleview_BottomList_Gyroscope:
                    UI_PhoneGalleryController.this.X();
                    if (UI_PhoneGalleryController.this.e.N == 3) {
                        UI_PhoneGalleryController.this.z(false);
                    }
                    if (UI_PhoneGalleryController.this.e.N != 1) {
                        UI_PhoneGalleryController.this.b(1, false);
                        return;
                    }
                    return;
                case R.id.LL_ui_singleview_BottomList_VR_Layout:
                case R.id.IB_ui_singleview_BottomList_VR:
                    UI_PhoneGalleryController.this.X();
                    if (UI_PhoneGalleryController.this.e.N != 3) {
                        UI_PhoneGalleryController.this.z(true);
                        return;
                    }
                    return;
                case R.id.LL_ui_singleview_BottomList_Fisheye_Layout:
                case R.id.IB_ui_singleview_BottomList_Fisheye:
                    UI_PhoneGalleryController.this.x((UI_PhoneGalleryController) 3);
                    UI_PhoneGalleryController.this.X();
                    return;
                case R.id.LL_ui_singleview_BottomList_Perspective_Layout:
                case R.id.IB_ui_singleview_BottomList_Perspective:
                    UI_PhoneGalleryController.this.x((UI_PhoneGalleryController) 2);
                    UI_PhoneGalleryController.this.X();
                    return;
                case R.id.LL_ui_singleview_BottomList_CrystalBall_Layout:
                case R.id.IB_ui_singleview_BottomList_CrystalBall:
                    UI_PhoneGalleryController.this.x((UI_PhoneGalleryController) 6);
                    UI_PhoneGalleryController.this.X();
                    return;
                case R.id.LL_ui_singleview_BottomList_LittlePlanet_Layout:
                case R.id.IB_ui_singleview_BottomList_LittlePlanet:
                    UI_PhoneGalleryController.this.x((UI_PhoneGalleryController) 5);
                    UI_PhoneGalleryController.this.X();
                    return;
                case R.id.LL_ui_singleview_EditTool_ConvertToGif_Layout:
                case R.id.IB_ui_singleview_EditTool_ConvertToGif:
                    UI_PhoneGalleryController.this.X();
                    UI_PhoneGalleryController.this.a(8466, 0L);
                    return;
                case R.id.LL_ui_singleview_EditTool_NormalPicOrMovieEdit_Layout:
                case R.id.IB_ui_singleview_EditTool_NormalPicOrMovieEdit:
                    UI_PhoneGalleryController.this.X();
                    if (UI_PhoneGalleryController.this.e.b.a(UI_PhoneGalleryController.this.e.s) == 1) {
                        UI_PhoneGalleryController.this.a(8465, 0L);
                        return;
                    } else if (UI_PhoneGalleryController.this.e.b.a(UI_PhoneGalleryController.this.e.s) == 0) {
                        UI_PhoneGalleryController.this.a(8467, 0L);
                        return;
                    } else {
                        return;
                    }
                case R.id.LL_ui_singleview_Snapshot_Cancel_Layout:
                case R.id.IB_ui_singleview_Snapshot_Cancel:
                    UI_PhoneGalleryController.this.a(9129);
                    return;
                case R.id.LL_ui_singleview_Snapshot_Confirm_Layout:
                case R.id.IB_ui_singleview_Snapshot_Confirm:
                    UI_PhoneGalleryController.this.X();
                    UI_PhoneGalleryController.this.a(9128);
                    return;
            }
        }
    };
    private SphericalVideoPlayer.f cG = new SphericalVideoPlayer.f() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass32 */

        @Override // GeneralFunction.Player.player.SphericalVideoPlayer.f
        public void a(int i) {
            UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("ReturnVideoStatus " + i), (String) 2);
            a.c.a aVar = new a.c.a(9097);
            aVar.a("360PlayerStatus", i);
            UI_PhoneGalleryController.this.a(aVar, 0);
        }
    };
    private View.OnClickListener cH = new View.OnClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass33 */

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.LL_ui_singleview_LayoutViewType:
                    if (!UI_PhoneGalleryController.this.C) {
                        UI_PhoneGalleryController.this.K(true);
                        UI_PhoneGalleryController.this.a(9121, 0L);
                        return;
                    }
                    UI_PhoneGalleryController.this.C = false;
                    return;
                case R.id.LL_ui_singleview_LayoutSnapshot:
                    if (UI_PhoneGalleryController.this.A()) {
                        UI_PhoneGalleryController.this.v(true);
                        return;
                    }
                    UI_PhoneGalleryController.this.h(false);
                    UI_PhoneGalleryController.this.g(true);
                    UI_PhoneGalleryController.this.b(20501, 0);
                    return;
                case R.id.LL_ui_singleview_LayoutEdit:
                    if (!UI_PhoneGalleryController.this.C) {
                        UI_PhoneGalleryController.this.cl.setImageResource(R.drawable.gallery_bottom_edit_green);
                        UI_PhoneGalleryController.this.cp.setTextColor(UI_PhoneGalleryController.this.getResources().getColor(R.color.iconcolor));
                        UI_PhoneGalleryController.this.a(9119, 0L);
                        return;
                    }
                    UI_PhoneGalleryController.this.C = false;
                    return;
                case R.id.LL_ui_singleview_LayoutDrag:
                    if (!UI_PhoneGalleryController.this.C) {
                        UI_PhoneGalleryController.this.L(true);
                        UI_PhoneGalleryController.this.a(9123, 0L);
                        return;
                    }
                    UI_PhoneGalleryController.this.C = false;
                    return;
                default:
                    UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) "error press", (String) 3);
                    return;
            }
        }
    };
    private View.OnClickListener cI = new View.OnClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass35 */

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.B_ui_MemoryFullConfirm:
                    UI_PhoneGalleryController.this.v(false);
                    return;
                case R.id.IB_EditPreview_Share:
                    UI_PhoneGalleryController.this.s();
                    if (a.b.b.a.a(UI_PhoneGalleryController.this.I)) {
                        UI_PhoneGalleryController.this.t(true);
                        return;
                    }
                    o.a(UI_PhoneGalleryController.this.H, true, true, UI_PhoneGalleryController.this.getResources().getString(R.string.unable_to_connect), UI_PhoneGalleryController.this.getResources().getString(R.string.unable_to_connect_message), new String[]{UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_cancel), UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_settings)}, new int[]{12039, 12050});
                    return;
                case R.id.IB_EditPreview_DeleteOrMore:
                    if (UI_PhoneGalleryController.this.bC) {
                        UI_PhoneGalleryController.this.m(false);
                        UI_PhoneGalleryController.this.k(true);
                        return;
                    }
                    o.a(UI_PhoneGalleryController.this.H, true, true, UI_PhoneGalleryController.this.getResources().getString(R.string.delete_image_title), UI_PhoneGalleryController.this.getResources().getString(R.string.delete_one_image_confirm), new String[]{UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_cancel), UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_delete)}, new int[]{0, 9092}, true, (int) R.color.red);
                    return;
                case R.id.IB_ui_singleview_btnReturn:
                    UI_PhoneGalleryController.this.a(32768, 0L);
                    return;
                case R.id.IB_ui_singleview_btnShareTab:
                    UI_PhoneGalleryController.this.s();
                    if (a.b.b.a.a(UI_PhoneGalleryController.this.I)) {
                        UI_PhoneGalleryController.this.t(true);
                    } else {
                        o.a(UI_PhoneGalleryController.this.H, true, true, UI_PhoneGalleryController.this.getResources().getString(R.string.unable_to_connect), UI_PhoneGalleryController.this.getResources().getString(R.string.unable_to_connect_message), new String[]{UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_cancel), UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_settings)}, new int[]{12039, 12050});
                    }
                    UI_PhoneGalleryController.this.w(false);
                    return;
                case R.id.IB_ui_singleview_btnMoreTab:
                    UI_PhoneGalleryController.this.B(true);
                    UI_PhoneGalleryController.this.w(false);
                    return;
                case R.id.IB_ui_singleview_MoreSettingReturn:
                    UI_PhoneGalleryController.this.w(true);
                    UI_PhoneGalleryController.this.cv.setVisibility(4);
                    return;
                case R.id.IB_ui_singleview_Previous:
                    if (UI_PhoneGalleryController.this.c.a(67108864L)) {
                        UI_PhoneGalleryController.this.g(true);
                        UI_PhoneGalleryController.this.c.b(268435455L);
                        int a2 = UI_PhoneGalleryController.this.c.a();
                        if (a2 == 1059) {
                            UI_PhoneGalleryController.this.a(9093, 0L);
                            return;
                        } else if (a2 == 1056) {
                            UI_PhoneGalleryController.this.a(9124, 0L);
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                case R.id.IB_ui_singleview_Next:
                    if (UI_PhoneGalleryController.this.c.a(67108864L)) {
                        UI_PhoneGalleryController.this.g(true);
                        UI_PhoneGalleryController.this.c.b(268435455L);
                        int a3 = UI_PhoneGalleryController.this.c.a();
                        if (a3 == 1059) {
                            UI_PhoneGalleryController.this.a(9094, 0L);
                            return;
                        } else if (a3 == 1056) {
                            UI_PhoneGalleryController.this.a(9126, 0L);
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                case R.id.IB_ui_singleview_btnViewType:
                    if (!UI_PhoneGalleryController.this.C) {
                        UI_PhoneGalleryController.this.K(true);
                        UI_PhoneGalleryController.this.a(9121, 0L);
                        return;
                    }
                    UI_PhoneGalleryController.this.C = false;
                    return;
                case R.id.IB_ui_singleview_btnSnapshot:
                    if (UI_PhoneGalleryController.this.A()) {
                        UI_PhoneGalleryController.this.v(true);
                        return;
                    }
                    UI_PhoneGalleryController.this.h(false);
                    UI_PhoneGalleryController.this.g(true);
                    UI_PhoneGalleryController.this.b(20501, 0);
                    return;
                case R.id.IB_ui_singleview_btnEdit:
                    if (!UI_PhoneGalleryController.this.C) {
                        UI_PhoneGalleryController.this.cl.setImageResource(R.drawable.gallery_bottom_edit_green);
                        UI_PhoneGalleryController.this.cp.setTextColor(UI_PhoneGalleryController.this.getResources().getColor(R.color.iconcolor));
                        UI_PhoneGalleryController.this.a(9119, 0L);
                        return;
                    }
                    UI_PhoneGalleryController.this.C = false;
                    return;
                case R.id.IB_ui_singleview_btnDrag:
                    if (!UI_PhoneGalleryController.this.C) {
                        UI_PhoneGalleryController.this.L(true);
                        UI_PhoneGalleryController.this.a(9123, 0L);
                        return;
                    }
                    UI_PhoneGalleryController.this.C = false;
                    return;
                case R.id.IB_ui_singleview_btnDeleteTab:
                    UI_PhoneGalleryController.this.B(false);
                    UI_PhoneGalleryController.this.a(9088, 0L);
                    return;
                default:
                    UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) "error press", (String) 3);
                    return;
            }
        }
    };
    private ImageButton ca = null;
    private TextView cb = null;
    private ImageButton cc = null;
    private ImageButton cd = null;
    private FrameLayout ce = null;
    private ImageButton cf = null;
    private ImageButton cg = null;
    private Space ch = null;
    private LinearLayout ci = null;
    private ImageButton cj = null;
    private ImageButton ck = null;
    private ImageButton cl = null;
    private ImageButton cm = null;
    private TextView cn = null;
    private TextView co = null;
    private TextView cp = null;
    private TextView cq = null;
    private LinearLayout cr = null;
    private LinearLayout cs = null;
    private LinearLayout ct = null;
    private LinearLayout cu = null;
    private LinearLayout cv = null;
    private ImageButton cw = null;
    private ListView cx = null;
    private TextView cy = null;
    private View cz = null;
    protected ui_Controller.b.b d = null;
    protected e e = null;
    protected ImageButton f = null;
    protected ImageButton g = null;
    protected ImageButton h = null;
    protected TextView i = null;
    protected ImageButton j = null;
    protected ImageButton k = null;
    protected LinearLayout l = null;
    protected LinearLayout m = null;
    protected ImageView n = null;
    protected boolean o = true;
    protected boolean p = false;
    protected boolean q = false;
    protected boolean r = false;
    protected LinearLayout s = null;
    protected LinearLayout t = null;
    protected ImageView u = null;
    protected Bitmap v = null;
    protected boolean w = false;
    protected boolean x = false;
    protected ArrayList<Integer> y = null;
    protected boolean z = false;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        d.a("UI_PhoneGalleryController", str, i2);
    }

    @Override // GeneralFunction.a.a
    public void a(int i2, long j2) {
        if (this.c != null) {
            this.c.a(new a.c.a(i2), j2);
        } else {
            a("SendUiMsg error!! Msg=" + i2, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(a.c.a aVar, long j2) {
        if (this.c != null) {
            this.c.a(aVar, j2);
        } else {
            a("SendUiMsg error!! MsgEx=" + aVar.b().what, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(int i2, long j2) {
        if (this.c != null) {
            this.c.b(i2, j2);
        } else {
            a("UI_SendAidMsg error!!", 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(a.c.a aVar, long j2) {
        if (this.c != null) {
            this.c.b(aVar, j2);
        } else {
            a("UI_SendAidMsg error!!", 0);
        }
    }

    @Override // GeneralFunction.a.a
    public boolean b() {
        if (this.M == 2) {
            return true;
        }
        return false;
    }

    public void onBackPressed() {
        if (this.c.a(1L)) {
            if (!m() || p()) {
                this.c.b(268435455L);
                a(32768, 0L);
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        boolean z2 = keyEvent.getRepeatCount() == 0;
        if (i2 == 79 && z2 && this.bU != null) {
            an();
            a(32769);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public void c() {
        if (System.currentTimeMillis() - this.c.c.h.X > 2000) {
            Toast.makeText(this, (int) R.string.press_again_to_quit, 0).show();
            this.c.c.h.X = System.currentTimeMillis();
            return;
        }
        Process.killProcess(Process.myPid());
        a(12032, 0L);
    }

    @Override // GeneralFunction.a.a
    public void b(Message message) {
        a("AidServer_Handler:0x" + Integer.toHexString(message.what), 3);
        this.b.c(message);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a("ACTIVITY_CREATE", 3);
        this.c = (UI_ModeMain) getApplication();
        this.c.v();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.K = displayMetrics.widthPixels;
        this.L = displayMetrics.heightPixels;
        a("ulScreenWidth: " + this.K + " ,ulScreenHeight: " + this.L, 3);
        if (this.L > this.K) {
            this.M = 1;
            setContentView(R.layout.ui_gallery_mode);
        } else {
            this.M = 2;
            setContentView(R.layout.ui_gallery_mode);
        }
        this.b = new c(this);
        this.d = this.c.c.k;
        this.d.d = 0;
        this.e = this.c.c.k.b;
        int intExtra = getIntent().getIntExtra("mode", -1);
        a("mode 111 " + intExtra, 3);
        if (intExtra == 1056) {
            if (this.K > this.L) {
                int i2 = this.K;
                this.K = this.L;
                this.L = i2;
            }
            this.c.a(1056, this);
            this.cB = (FrameLayout) findViewById(R.id.FL_ui_singleview_SingleViewPlayerLayout);
            this.cB.setVisibility(0);
        } else {
            if (this.K > this.L) {
                int i3 = this.K;
                this.K = this.L;
                this.L = i3;
            }
            l(ui_Controller.b.d.a().c());
            this.c.a(1040, this);
            this.e.W = false;
        }
        this.bF = new GeneralFunction.l.a();
        this.bF.a(this);
        this.bF.a(this.B);
        bl();
        aU();
        i(true);
        if (this.e.w) {
            if (ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == -1) {
                ActivityCompat.requestPermissions(this, this.bE, 300);
            } else {
                b(20499);
            }
        }
        this.e.Z = false;
        this.c.b(1L);
        a.c.a aVar = new a.c.a(12033);
        aVar.a("mode", intExtra);
        a(aVar, 200);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onResume() {
        super.onResume();
        if (this.bU != null && this.c.a() == 1059) {
            be();
        }
        if (this.bP != null) {
            this.bP.b();
        }
        a.c.a aVar = new a.c.a(12034);
        aVar.a("mode", 1024);
        a(aVar, 0);
        this.bp.enable();
    }

    @Override // GeneralFunction.a.a
    public void onPause() {
        super.onPause();
        X();
        if (this.bU != null && this.c.a() == 1059) {
            if (this.bU.isPlaying()) {
                D();
                C();
            }
            if (!this.e.Z) {
                am();
                Z();
            }
            if (p()) {
                if (this.D) {
                    this.D = false;
                    C();
                }
                if (!this.e.A) {
                    a(9129);
                }
            }
        }
        if (this.e.f == 5) {
            this.c.d();
        }
        if (this.bP != null) {
            this.bP.a();
        }
        a.c.a aVar = new a.c.a(12035);
        aVar.a("mode", 1024);
        a(aVar, 0);
        this.bp.disable();
    }

    @Override // GeneralFunction.a.a
    public void onStop() {
        super.onStop();
        a.c.a aVar = new a.c.a(12036);
        aVar.a("mode", 1024);
        a(aVar, 0);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onDestroy() {
        super.onDestroy();
        l();
        if (ui_Controller.a.b.b(this.c.a())) {
            o();
            P();
            a(false);
            au();
        }
        this.c.c.f = true;
        a.c.a aVar = new a.c.a(12037);
        aVar.a("mode", 1024);
        a(aVar, 0);
    }

    public void onUserInteraction() {
        super.onUserInteraction();
        if (ui_Controller.a.b.b(this.c.a())) {
            a(true);
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        if (z2) {
            g.a(5);
            g.a(5, ExoPlayer.Factory.DEFAULT_MIN_REBUFFER_MS);
            return;
        }
        g.a(5);
    }

    /* access modifiers changed from: protected */
    public void d() {
        a(false, "");
        g.a(4);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.bF.a(0, 0, intent, this.I);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        a("onActivityResult requestCode:" + i2, 3);
        switch (i2) {
            case 1:
                if (this.e.f > 0) {
                    a(32768);
                }
                this.e.j.clear();
                break;
            case 2:
                break;
            case 42:
                if (intent != null) {
                    this.d.b.aa = intent.getData();
                    a("get TreeUri:" + intent.getData(), 2);
                    getContentResolver().takePersistableUriPermission(this.d.b.aa, 3);
                    aM();
                    break;
                }
            default:
                this.bF.a(i2, i3, intent, this.I);
                break;
        }
        super.onActivityResult(i2, i3, intent);
    }

    @Override // GeneralFunction.a.a
    public void a(Message message) {
        this.b.a(message);
    }

    /* access modifiers changed from: protected */
    public void e() {
        a("InitAllUiWidget", 4);
        if (!this.p) {
            aR();
            y();
            this.p = true;
        }
        if (aF() == null) {
            this.o = false;
            c(false);
        } else {
            this.o = true;
            c(true);
        }
        if (this.e.w) {
            if (this.q || this.r) {
                q(true);
                g(true);
            }
            this.e.w = false;
        }
        if (this.e.x) {
            aH();
        }
    }

    private void aP() {
        this.Q = (TabLayout) findViewById(R.id.TL_ui_multiview_HeaderTabLayout);
        this.Q.setTabMode(1);
        this.Q.setTabGravity(0);
        this.Q.a(this.Q.a().d(R.string.phone));
        this.Q.a(this.Q.a().d(R.string.sdcard));
        this.Q.a(this.Q.a().d(R.string.my_album));
        try {
            Field declaredField = this.Q.getClass().getDeclaredField("mTabTextSize");
            declaredField.setAccessible(true);
            float floatValue = ((Float) declaredField.get(this.Q)).floatValue();
            declaredField.set(this.Q, Integer.valueOf(new GeneralFunction.c((int) floatValue).n((int) (((double) floatValue) * 0.85d)).c((int) (((double) floatValue) * 0.85d)).f((int) (((double) floatValue) * 0.85d)).g((int) (((double) floatValue) * 0.85d)).l((int) (((double) floatValue) * 0.85d)).d((int) (((double) floatValue) * 0.85d)).h((int) (((double) floatValue) * 0.9d)).i((int) (((double) floatValue) * 0.9d)).p((int) (((double) floatValue) * 0.9d)).j((int) (((double) floatValue) * 0.9d)).q((int) (((double) floatValue) * 0.85d)).a()));
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        }
        this.Q.a(new TabLayout.c() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass12 */

            @Override // android.support.design.widget.TabLayout.b
            public void a(TabLayout.f fVar) {
                if (UI_PhoneGalleryController.this.P != null) {
                    UI_PhoneGalleryController.this.P.fling(0);
                }
                switch (fVar.c()) {
                    case 0:
                        UI_PhoneGalleryController.this.c.a(9040, 50);
                        break;
                    case 1:
                        UI_PhoneGalleryController.this.cA.a(1);
                        UI_PhoneGalleryController.this.cA.b();
                        UI_PhoneGalleryController.this.c.a(9041, 50);
                        break;
                    case 2:
                        UI_PhoneGalleryController.this.cA.a(2);
                        UI_PhoneGalleryController.this.cA.b();
                        UI_PhoneGalleryController.this.c.a(9042, 50);
                        break;
                }
                UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("TabLayout:" + fVar.c()), (String) 2);
            }

            @Override // android.support.design.widget.TabLayout.b
            public void b(TabLayout.f fVar) {
            }

            @Override // android.support.design.widget.TabLayout.b
            public void c(TabLayout.f fVar) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void b(boolean z2) {
        this.o = z2;
        switch (this.e.d) {
            case 1:
                if (!z2) {
                    this.cA.a(3);
                    this.cA.b();
                    break;
                } else {
                    this.cA.a(0);
                    this.cA.b();
                    break;
                }
            case 2:
                this.cA.a(1);
                this.cA.b();
                break;
        }
        if (this.e.d != 3) {
            d(z2);
        }
    }

    /* access modifiers changed from: protected */
    public void c(boolean z2) {
        if (this.Q != null) {
            View childAt = ((LinearLayout) this.Q.getChildAt(0)).getChildAt(1);
            if (z2) {
                childAt.setVisibility(0);
            } else {
                childAt.setVisibility(8);
            }
            if (!z2 && this.c.a() == 1040) {
                if (this.e.d == 2 || this.e.d == 3) {
                    this.Q.a(0).e();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void f() {
        int i2 = 0;
        switch (this.e.d) {
            case 1:
                a(9040);
                break;
            case 2:
                i2 = 1;
                a(9041);
                break;
            case 3:
                d(false);
                a(9042);
                i2 = 2;
                break;
        }
        this.Q.a(i2).e();
    }

    /* access modifiers changed from: protected */
    public void g() {
        GeneralFunction.m.a.a(GeneralFunction.n.a.a(this.c).getInt("storeLocation", 0));
        switch (GeneralFunction.m.a.a()) {
            case 0:
                this.e.d = 1;
                break;
            case 1:
                this.e.d = 2;
                break;
        }
        f();
    }

    /* access modifiers changed from: protected */
    public void d(boolean z2) {
        if (z2) {
            this.h.setVisibility(0);
            this.O.setVisibility(0);
            return;
        }
        this.h.setVisibility(8);
        this.O.setVisibility(8);
    }

    private void aQ() {
        this.R = (ImageButton) findViewById(R.id.IB_ui_ShootingModeButton);
        this.R.setBackgroundResource(R.drawable.gallery_nxn_shootingmode);
        this.R.setOnClickListener(this.bL);
    }

    private void aR() {
        a("InitAllUiWidget", 4);
        aP();
        aQ();
        aS();
        aZ();
        ba();
        h();
        aY();
        aT();
    }

    /* access modifiers changed from: protected */
    public void h() {
        this.bn = (LinearLayout) findViewById(R.id.LL_Phone_Gallery_Warning_Layout);
        this.bo = (TextView) findViewById(R.id.TV_Phone_Gallery_Warning_Content);
        this.bo.setTextSize((float) new GeneralFunction.c(14).l((int) (((double) 14) * 0.9d)).o((int) (((double) 14) * 0.9d)).a());
        this.bn.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass23 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2, String str) {
        if (z2) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.bn, "alpha", 0.0f, 1.0f);
            ofFloat.setDuration(500L);
            ofFloat.start();
            this.bn.setVisibility(0);
            this.bo.setText(str);
            return;
        }
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.bn, "alpha", 1.0f, 0.0f);
        ofFloat2.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass34 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                if (UI_PhoneGalleryController.this.bn != null) {
                    UI_PhoneGalleryController.this.bn.setVisibility(4);
                }
            }
        });
        ofFloat2.setDuration(500L);
        ofFloat2.start();
    }

    /* access modifiers changed from: protected */
    public void i() {
        int i2;
        int min = Math.min(this.K, this.L);
        int max = (int) (((double) Math.max(this.K, this.L)) * 0.00625d);
        if (this.e.d == 3) {
            i2 = 3;
        } else {
            i2 = 1;
        }
        this.P = (StickyGridHeadersGridView) findViewById(R.id.GV_ui_multiview_HeaderGridView);
        a.b bVar = new a.b();
        bVar.b = 18096128;
        bVar.c = 810000;
        bVar.d = 3;
        if (this.e.d == 3) {
            bVar.f1311a = ui_Controller.a.a.b;
            bVar.e = 50;
        } else {
            bVar.f1311a = ui_Controller.a.a.f1313a;
            bVar.e = 15;
        }
        bVar.f = false;
        bVar.g = true;
        if (this.e.d == 3) {
            bVar.i = 15;
        } else {
            bVar.i = 5;
        }
        ui_Controller.CustomWidget.a aVar = new ui_Controller.CustomWidget.a(bVar);
        if (this.e.d == 3) {
            this.e.m = new ui_Controller.CustomWidget.a.a.a(this, this.e.f1320a, (int) (((double) min) * 0.3135d), this.e.f, true, aVar);
        } else {
            this.e.m = new ui_Controller.CustomWidget.a.a.a(this, this.e.f1320a, (int) (((double) min) * 0.3555d), this.e.f, true, aVar);
        }
        this.e.m.b(this.e.d);
        this.e.m.a(new a.AbstractC0101a() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass37 */

            @Override // ui_Controller.CustomWidget.a.a.a.AbstractC0101a
            public void a() {
                UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) "Callback LoadThumbnailDone()", (String) 4);
            }

            @Override // ui_Controller.CustomWidget.a.a.a.AbstractC0101a
            public void a(int i) {
                UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) ("Callback LoadedErrorFile():" + i), (String) 2);
                UI_PhoneGalleryController.this.y.add(Integer.valueOf(i));
                UI_PhoneGalleryController.this.g(true);
                if (!UI_PhoneGalleryController.this.z && UI_PhoneGalleryController.this.c.a() == 1040 && !UI_PhoneGalleryController.this.q) {
                    UI_PhoneGalleryController.this.a(9053, 0L);
                    UI_PhoneGalleryController.this.z = true;
                }
            }
        });
        this.P.setAdapter((ListAdapter) this.e.m);
        this.P.setNumColumns(i2);
        this.P.setPadding(0, 0, 0, (int) (((double) this.L) * 0.2125d));
        this.P.setClipToPadding(false);
        this.P.setHorizontalSpacing(max);
        this.P.setVerticalSpacing(max);
        this.P.setOnHeaderClickListener(this.bI);
        this.P.setOnItemClickListener(this.bK);
        this.P.setOnItemLongClickListener(this.bJ);
        this.P.setAreHeadersSticky(true);
        this.P.setOverScrollMode(2);
        this.y = new ArrayList<>();
        w();
    }

    /* access modifiers changed from: protected */
    public int j() {
        return this.P.getFirstVisiblePosition();
    }

    /* access modifiers changed from: protected */
    public void c(int i2) {
        this.P.setSelection(i2);
        this.e.u = 0;
    }

    /* access modifiers changed from: protected */
    public int k() {
        return this.y.size();
    }

    /* access modifiers changed from: protected */
    public void l() {
        if (this.e.m != null) {
            this.e.m.a();
            this.e.m = null;
            this.P = null;
        }
    }

    /* access modifiers changed from: protected */
    public void e(boolean z2) {
        if (z2) {
            this.P.setVisibility(0);
        } else {
            this.P.setVisibility(4);
        }
    }

    private void aS() {
        this.l = (LinearLayout) findViewById(R.id.LL_ui_multiview_NormalTopLayout);
        this.l.setVisibility(4);
        this.f = (ImageButton) findViewById(R.id.IB_ui_multiview_btnDeleteTab);
        this.g = (ImageButton) findViewById(R.id.IB_ui_multiview_btnShareTab);
        this.i = (TextView) findViewById(R.id.TV_ui_multiview_SelectCount);
        this.j = (ImageButton) findViewById(R.id.IB_ui_multiview_btnCancel);
        this.k = (ImageButton) findViewById(R.id.IB_ui_multiview_selectAll);
        this.h = (ImageButton) findViewById(R.id.IB_ui_multiview_btnMoveTab);
        this.O = (Space) findViewById(R.id.SPACE_ui_multiview_MoveAndShare);
        this.j.setImageResource(R.drawable.system_close_black);
        this.k.setImageResource(R.drawable.gallery_top_selectall);
        this.h.setImageResource(R.drawable.gallery_top_move);
        this.f.setImageResource(R.drawable.gallery_top_delete);
        this.f.setImageAlpha(100);
        this.g.setImageResource(R.drawable.gallery_top_share_gray);
        this.i.setTextSize((float) k.a(this, (float) (this.K / 22)));
        this.i.setText(R.string.selected);
        this.i.setTextColor(getResources().getColor(R.color.black));
        this.f.setOnClickListener(this.bL);
        this.g.setOnClickListener(this.bL);
        this.j.setOnClickListener(this.bL);
        this.k.setOnClickListener(this.bL);
        this.h.setOnClickListener(this.bL);
        this.l.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass38 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        d(this.o);
    }

    public void f(boolean z2) {
        if (z2) {
            this.l.setVisibility(0);
        } else {
            this.l.setVisibility(4);
        }
    }

    private void aT() {
        this.V = new PopupWindow(new View(this), -1, -1);
    }

    /* access modifiers changed from: protected */
    public void g(boolean z2) {
        a("ShowProtectLayout: " + z2, 3);
        if (z2) {
            this.V.showAtLocation(this.bV, 17, 0, 0);
        } else {
            this.V.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public boolean m() {
        if (this.V != null && this.V.isShowing()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void h(boolean z2) {
        this.ck.setEnabled(z2);
    }

    private void aU() {
        this.bq = (LinearLayout) findViewById(R.id.LL_ui_BlackScreen);
        this.bq.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass39 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void i(boolean z2) {
        a("ShowBlackScreenLayout:" + z2, 3);
        if (z2) {
            this.bq.setVisibility(0);
        } else {
            this.bq.setVisibility(4);
        }
    }

    private void aV() {
        this.br = (FrameLayout) findViewById(R.id.FL_ui_EditPreview);
        this.bs = (ZoomableImageView) findViewById(R.id.ZIV_ui_EditPreview);
        aW();
        aX();
        this.bs.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass40 */

            public void onClick(View view) {
                if (UI_PhoneGalleryController.this.n()) {
                    UI_PhoneGalleryController.this.k(false);
                    UI_PhoneGalleryController.this.m(true);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void j(boolean z2) {
        if (z2) {
            this.bs.setMaxZoom(0.0f);
            this.bs.a(true);
            return;
        }
        this.bs.setMaxZoom(1.0f);
        this.bs.a(false);
    }

    private void aW() {
        this.bt = (LinearLayout) findViewById(R.id.LL_EditPreview_TopLayout);
        this.bu = (ImageButton) findViewById(R.id.IB_EditPreview_Share);
        this.bv = (ImageButton) findViewById(R.id.IB_EditPreview_DeleteOrMore);
        this.bu.setImageResource(R.drawable.gallery_top_share_white);
        this.bv.setImageResource(R.drawable.gallery_tool_delete);
        this.bu.setOnClickListener(this.cI);
        this.bv.setOnClickListener(this.cI);
    }

    private void aX() {
        this.bw = (LinearLayout) findViewById(R.id.LL_EditPreview_MoreSettingTopLayout);
        this.by = (TextView) findViewById(R.id.TV_EditPreview_MoreSetting);
        this.bz = (ImageButton) findViewById(R.id.IB_EditPreview_MoreSettingReturn);
        this.bA = (ListView) findViewById(R.id.LV_EditPreview_MoreSettingListView);
        this.bx = findViewById(R.id.V_EditPreview_MoreSetting);
        this.bw.setVisibility(4);
        this.by.setTextSize((float) k.a(this, (float) (this.L / 35)));
        this.by.setText(R.string.more_setting);
        this.by.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass2 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.bx.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass3 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.bz.setImageResource(R.drawable.system_close_white);
        this.bz.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass4 */

            public void onClick(View view) {
                UI_PhoneGalleryController.this.k(false);
                UI_PhoneGalleryController.this.m(true);
            }
        });
        this.bB = new d(this, 2);
        this.bA.setAdapter((ListAdapter) this.bB);
        this.bA.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass5 */

            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                switch ((int) j) {
                    case 2:
                        UI_PhoneGalleryController.this.k(false);
                        UI_PhoneGalleryController.this.m(true);
                        o.a(UI_PhoneGalleryController.this.H, true, true, UI_PhoneGalleryController.this.getResources().getString(R.string.delete_image_title), UI_PhoneGalleryController.this.getResources().getString(R.string.delete_one_image_confirm), new String[]{UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_cancel), UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_delete)}, new int[]{0, 9092}, true, (int) R.color.red);
                        return;
                    case 3:
                    default:
                        return;
                    case 4:
                        UI_PhoneGalleryController.this.k(false);
                        UI_PhoneGalleryController.this.m(true);
                        UI_PhoneGalleryController.this.e(UI_PhoneGalleryController.this.e.B);
                        return;
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void k(boolean z2) {
        if (z2) {
            this.bw.setVisibility(0);
        } else {
            this.bw.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public boolean n() {
        if (this.bw.getVisibility() == 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(Bitmap bitmap) {
        this.bs.setImageBitmap(bitmap);
        this.br.setVisibility(0);
        this.bs.setVisibility(0);
        if (this.e.B != null && this.e.B.endsWith("gif")) {
            com.a.a.c.b(getApplication()).a(this.e.B).a((ImageView) this.bs);
        }
    }

    /* access modifiers changed from: protected */
    public void o() {
        if (this.br != null && this.bs != null) {
            this.br.setVisibility(4);
            this.bs.setVisibility(4);
            this.bs.a();
            this.bs.setImageResource(0);
            if (this.v != null) {
                this.v.recycle();
                this.v = null;
            }
            if (this.e.B != null && this.e.B.endsWith("gif")) {
                com.a.a.c.b(getApplication()).a((View) this.bs);
            }
            n(false);
            l(false);
            m(false);
            k(false);
            this.e.A = false;
            this.e.B = null;
        }
    }

    /* access modifiers changed from: protected */
    public void l(boolean z2) {
        if (z2) {
            this.bv.setImageResource(R.drawable.gallery_tool_delete);
            this.bt.setVisibility(0);
            j(false);
            return;
        }
        this.bt.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void m(boolean z2) {
        if (z2) {
            this.bv.setImageResource(R.drawable.gallery_top_more);
            this.bt.setVisibility(0);
            this.bC = true;
            j(true);
            return;
        }
        this.bt.setVisibility(4);
        this.bC = false;
    }

    /* access modifiers changed from: protected */
    public void n(boolean z2) {
        if (z2) {
            this.br.setScaleX(0.85f);
            this.br.setScaleY(0.85f);
            return;
        }
        this.br.setScaleX(1.0f);
        this.br.setScaleY(1.0f);
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        if (this.br.getVisibility() == 0) {
            return true;
        }
        return false;
    }

    private void aY() {
        this.bV = (LinearLayout) findViewById(R.id.LL_ui_Gallery_BusyLayout);
        this.bW = (ImageView) findViewById(R.id.IV_ui_Gallery_BusyIcon);
        this.bX = (TextView) findViewById(R.id.TV_ui_Gallery_BusyText);
        this.bY = (Button) findViewById(R.id.B_ui_Gallery_Busy_Button);
        this.bY.setBackgroundResource(R.drawable.btn_bg_blue);
        this.bY.setEnabled(false);
        this.bX.setText(getResources().getString(R.string.moving_files));
        this.bV.setVisibility(4);
        this.bV.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass6 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.bY.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass7 */

            public void onClick(View view) {
                o.a(UI_PhoneGalleryController.this.H, true, false, UI_PhoneGalleryController.this.getResources().getString(R.string.share), UI_PhoneGalleryController.this.getResources().getString(R.string.share_cancel), new String[]{UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_no), UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_yes)}, new int[]{0, 8971}, true);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void o(boolean z2) {
        this.bW.setImageResource(R.drawable.anim_system_busy);
        this.bX.setText(getResources().getString(R.string.moving_files));
        this.bY.setVisibility(4);
        this.bY.setEnabled(false);
        if (z2) {
            this.bV.setVisibility(0);
            ((AnimationDrawable) this.bW.getDrawable()).start();
            this.e.z = true;
            return;
        }
        this.bV.setVisibility(4);
        ((AnimationDrawable) this.bW.getDrawable()).stop();
        this.e.z = false;
    }

    /* access modifiers changed from: protected */
    public boolean q() {
        if (this.bV != null && this.bV.getVisibility() == 0 && this.bX.getText().equals(getResources().getString(R.string.moving_files))) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void p(boolean z2) {
        this.bW.setImageResource(R.drawable.anim_system_busy);
        this.bX.setText(getResources().getString(R.string.saving_files));
        this.bY.setVisibility(4);
        this.bY.setEnabled(false);
        if (z2) {
            this.bV.setVisibility(0);
            ((AnimationDrawable) this.bW.getDrawable()).start();
            return;
        }
        this.bV.setVisibility(4);
        ((AnimationDrawable) this.bW.getDrawable()).stop();
    }

    /* access modifiers changed from: protected */
    public void q(boolean z2) {
        this.bW.setImageResource(R.drawable.anim_system_busy);
        this.bX.setText(getResources().getString(R.string.scanning_files));
        this.bY.setVisibility(4);
        this.bY.setEnabled(false);
        if (z2) {
            this.bV.setVisibility(0);
            ((AnimationDrawable) this.bW.getDrawable()).start();
            this.e.z = true;
            return;
        }
        this.bV.setVisibility(4);
        ((AnimationDrawable) this.bW.getDrawable()).stop();
        this.e.z = false;
    }

    /* access modifiers changed from: protected */
    public boolean r() {
        if (this.bV != null && this.bV.getVisibility() == 0) {
            return true;
        }
        return false;
    }

    private void aZ() {
        this.m = (LinearLayout) findViewById(R.id.LL_ui_multiview_NoFileLayout);
        this.m.setVisibility(4);
        this.n = (ImageView) findViewById(R.id.IV_ui_multiview_NoFileImage);
        this.n.setImageResource(R.drawable.gallery_nxn_noimage);
    }

    private void ba() {
        this.s = (LinearLayout) findViewById(R.id.LL_ui_view_shareLayout);
        this.t = (LinearLayout) findViewById(R.id.LL_ui_blank_shareLayout);
        t(false);
        this.u = (ImageView) findViewById(R.id.IB_ui_multiview_btnShareCancel);
        this.u.setImageResource(R.drawable.system_close_white);
        this.d.b.af = aO();
        this.Y = (ImageButton) findViewById(R.id.IB_ui_Share_Button1);
        this.Z = (ImageButton) findViewById(R.id.IB_ui_Share_Button2);
        this.aa = (ImageButton) findViewById(R.id.IB_ui_Share_Button3);
        this.ab = (ImageButton) findViewById(R.id.IB_ui_Share_Button4);
        this.ac = (ImageButton) findViewById(R.id.IB_ui_Share_Button5);
        this.ad = (ImageButton) findViewById(R.id.IB_ui_Share_Button6);
        this.ae = (ImageButton) findViewById(R.id.IB_ui_Share_Button7);
        this.af = (ImageButton) findViewById(R.id.IB_ui_Share_Button8);
        this.ag = (ImageButton) findViewById(R.id.IB_ui_Share_Button9);
        this.ah = (ImageButton) findViewById(R.id.IB_ui_Share_Button10);
        this.ai = (ImageButton) findViewById(R.id.IB_ui_Share_Button11);
        this.aj = (ImageButton) findViewById(R.id.IB_ui_Share_Button12);
        this.ak = (TextView) findViewById(R.id.TV_ui_Share_Button1);
        this.al = (TextView) findViewById(R.id.TV_ui_Share_Button2);
        this.am = (TextView) findViewById(R.id.TV_ui_Share_Button3);
        this.an = (TextView) findViewById(R.id.TV_ui_Share_Button4);
        this.ao = (TextView) findViewById(R.id.TV_ui_Share_Button5);
        this.ap = (TextView) findViewById(R.id.TV_ui_Share_Button6);
        this.aq = (TextView) findViewById(R.id.TV_ui_Share_Button7);
        this.ar = (TextView) findViewById(R.id.TV_ui_Share_Button8);
        this.as = (TextView) findViewById(R.id.TV_ui_Share_Button9);
        this.at = (TextView) findViewById(R.id.TV_ui_Share_Button10);
        this.au = (TextView) findViewById(R.id.TV_ui_Share_Button11);
        this.av = (TextView) findViewById(R.id.TV_ui_Share_Button12);
        int a2 = k.a(this, (float) (this.L / 50));
        this.ak.setTextSize((float) a2);
        this.al.setTextSize((float) a2);
        this.am.setTextSize((float) a2);
        this.an.setTextSize((float) a2);
        this.ao.setTextSize((float) a2);
        this.ap.setTextSize((float) a2);
        this.aq.setTextSize((float) a2);
        this.ar.setTextSize((float) a2);
        this.as.setTextSize((float) a2);
        this.at.setTextSize((float) a2);
        this.au.setTextSize((float) a2);
        this.av.setTextSize((float) a2);
        this.aw = (ImageButton) findViewById(R.id.IB_ui_Share_Button1_Landscape);
        this.ax = (ImageButton) findViewById(R.id.IB_ui_Share_Button2_Landscape);
        this.ay = (ImageButton) findViewById(R.id.IB_ui_Share_Button3_Landscape);
        this.az = (ImageButton) findViewById(R.id.IB_ui_Share_Button4_Landscape);
        this.aA = (ImageButton) findViewById(R.id.IB_ui_Share_Button5_Landscape);
        this.aB = (ImageButton) findViewById(R.id.IB_ui_Share_Button6_Landscape);
        this.aC = (ImageButton) findViewById(R.id.IB_ui_Share_Button7_Landscape);
        this.aD = (ImageButton) findViewById(R.id.IB_ui_Share_Button8_Landscape);
        this.aE = (ImageButton) findViewById(R.id.IB_ui_Share_Button9_Landscape);
        this.aF = (ImageButton) findViewById(R.id.IB_ui_Share_Button10_Landscape);
        this.aG = (ImageButton) findViewById(R.id.IB_ui_Share_Button11_Landscape);
        this.aH = (ImageButton) findViewById(R.id.IB_ui_Share_Button12_Landscape);
        this.aI = (TextView) findViewById(R.id.TV_ui_Share_Button1_Landscape);
        this.aJ = (TextView) findViewById(R.id.TV_ui_Share_Button2_Landscape);
        this.aK = (TextView) findViewById(R.id.TV_ui_Share_Button3_Landscape);
        this.aL = (TextView) findViewById(R.id.TV_ui_Share_Button4_Landscape);
        this.aM = (TextView) findViewById(R.id.TV_ui_Share_Button5_Landscape);
        this.aN = (TextView) findViewById(R.id.TV_ui_Share_Button6_Landscape);
        this.aO = (TextView) findViewById(R.id.TV_ui_Share_Button7_Landscape);
        this.aP = (TextView) findViewById(R.id.TV_ui_Share_Button8_Landscape);
        this.aQ = (TextView) findViewById(R.id.TV_ui_Share_Button9_Landscape);
        this.aR = (TextView) findViewById(R.id.TV_ui_Share_Button10_Landscape);
        this.aS = (TextView) findViewById(R.id.TV_ui_Share_Button11_Landscape);
        this.aT = (TextView) findViewById(R.id.TV_ui_Share_Button12_Landscape);
        this.aI.setTextSize((float) a2);
        this.aJ.setTextSize((float) a2);
        this.aK.setTextSize((float) a2);
        this.aL.setTextSize((float) a2);
        this.aM.setTextSize((float) a2);
        this.aN.setTextSize((float) a2);
        this.aO.setTextSize((float) a2);
        this.aP.setTextSize((float) a2);
        this.aQ.setTextSize((float) a2);
        this.aR.setTextSize((float) a2);
        this.aS.setTextSize((float) a2);
        this.aT.setTextSize((float) a2);
        this.Y.setOnClickListener(this.bH);
        this.Z.setOnClickListener(this.bH);
        this.aa.setOnClickListener(this.bH);
        this.ab.setOnClickListener(this.bH);
        this.ac.setOnClickListener(this.bH);
        this.ad.setOnClickListener(this.bH);
        this.ae.setOnClickListener(this.bH);
        this.af.setOnClickListener(this.bH);
        this.ag.setOnClickListener(this.bH);
        this.ah.setOnClickListener(this.bH);
        this.ai.setOnClickListener(this.bH);
        this.aj.setOnClickListener(this.bH);
        this.aw.setOnClickListener(this.bH);
        this.ax.setOnClickListener(this.bH);
        this.ay.setOnClickListener(this.bH);
        this.az.setOnClickListener(this.bH);
        this.aA.setOnClickListener(this.bH);
        this.aB.setOnClickListener(this.bH);
        this.aC.setOnClickListener(this.bH);
        this.aD.setOnClickListener(this.bH);
        this.aE.setOnClickListener(this.bH);
        this.aF.setOnClickListener(this.bH);
        this.aG.setOnClickListener(this.bH);
        this.aH.setOnClickListener(this.bH);
        this.aW = new ArrayList<>();
        this.aW.add(this.Y);
        this.aW.add(this.Z);
        this.aW.add(this.aa);
        this.aW.add(this.ab);
        this.aW.add(this.ac);
        this.aW.add(this.ad);
        this.aW.add(this.ae);
        this.aW.add(this.af);
        this.aW.add(this.ag);
        this.aW.add(this.ah);
        this.aW.add(this.ai);
        this.aW.add(this.aj);
        this.aX = new ArrayList<>();
        this.aX.add(this.ak);
        this.aX.add(this.al);
        this.aX.add(this.am);
        this.aX.add(this.an);
        this.aX.add(this.ao);
        this.aX.add(this.ap);
        this.aX.add(this.aq);
        this.aX.add(this.ar);
        this.aX.add(this.as);
        this.aX.add(this.at);
        this.aX.add(this.au);
        this.aX.add(this.av);
        this.aZ = new ArrayList<>();
        this.aZ.add(this.aw);
        this.aZ.add(this.ax);
        this.aZ.add(this.ay);
        this.aZ.add(this.az);
        this.aZ.add(this.aA);
        this.aZ.add(this.aB);
        this.aZ.add(this.aC);
        this.aZ.add(this.aD);
        this.aZ.add(this.aE);
        this.aZ.add(this.aF);
        this.aZ.add(this.aG);
        this.aZ.add(this.aH);
        this.ba = new ArrayList<>();
        this.ba.add(this.aI);
        this.ba.add(this.aJ);
        this.ba.add(this.aK);
        this.ba.add(this.aL);
        this.ba.add(this.aM);
        this.ba.add(this.aN);
        this.ba.add(this.aO);
        this.ba.add(this.aP);
        this.ba.add(this.aQ);
        this.ba.add(this.aR);
        this.ba.add(this.aS);
        this.ba.add(this.aT);
        this.aU = (LinearLayout) findViewById(R.id.LL_ui_view_SNSLayout);
        this.aV = (TextView) findViewById(R.id.TV_ui_share_no_sns);
        this.aY = (LinearLayout) findViewById(R.id.LL_ui_view_SNSLayout_Landscape);
        this.t.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass8 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    UI_PhoneGalleryController.this.t(false);
                    if (UI_PhoneGalleryController.this.c.a() == 1056) {
                        UI_PhoneGalleryController.this.a(true, false, false);
                    }
                }
                if (motionEvent.getAction() == 2) {
                }
                if (motionEvent.getAction() == 1) {
                }
                return true;
            }
        });
        this.s.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass9 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                }
                if (motionEvent.getAction() == 2) {
                }
                if (motionEvent.getAction() == 1) {
                }
                return true;
            }
        });
        this.u.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass10 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                }
                if (motionEvent.getAction() == 2) {
                }
                if (motionEvent.getAction() == 1) {
                    UI_PhoneGalleryController.this.t(false);
                    if (UI_PhoneGalleryController.this.c.a() == 1056 || UI_PhoneGalleryController.this.e.A) {
                        UI_PhoneGalleryController.this.a(true, false, false);
                    }
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void s() {
        this.d.b.af = aO();
        this.d.b.al = false;
        for (int i2 = 0; i2 < 11; i2++) {
            this.aW.get(i2).setImageResource(0);
            this.aZ.get(i2).setImageResource(0);
            this.aX.get(i2).setText("");
            this.ba.get(i2).setText("");
        }
        if (this.d.b.af.size() == 0) {
            this.d.b.al = true;
            this.aU.setVisibility(8);
            this.aY.setVisibility(8);
            this.aV.setVisibility(0);
            return;
        }
        if (getResources().getConfiguration().orientation == 1) {
            this.aU.setVisibility(0);
            this.aY.setVisibility(4);
        } else {
            this.aU.setVisibility(4);
            this.aY.setVisibility(0);
        }
        this.aV.setVisibility(8);
        if (this.e.d == 3 || this.e.A) {
            ArrayList arrayList = new ArrayList();
            boolean z2 = false;
            for (int i3 = 0; i3 < this.d.b.af.size(); i3++) {
                if (this.d.b.af.get(i3).intValue() == 14 || this.d.b.af.get(i3).intValue() == 9 || this.d.b.af.get(i3).intValue() == 13) {
                    if (this.e.m.e() > 1) {
                        z2 = true;
                    } else {
                        arrayList.add(this.d.b.af.get(i3));
                    }
                } else if (this.d.b.af.get(i3).intValue() == 0 || this.d.b.af.get(i3).intValue() == 11) {
                    z2 = true;
                } else {
                    arrayList.add(this.d.b.af.get(i3));
                }
            }
            if (z2) {
                this.d.b.af = arrayList;
            }
        } else if (this.c.a() == 1040) {
            int i4 = 0;
            while (true) {
                if (i4 >= this.e.m.getCount()) {
                    break;
                }
                if (this.e.m.f(i4)) {
                    if (this.e.b.k(i4)) {
                        ArrayList arrayList2 = new ArrayList();
                        boolean z3 = false;
                        for (int i5 = 0; i5 < this.d.b.af.size(); i5++) {
                            if (this.d.b.af.get(i5).intValue() == 14 || this.d.b.af.get(i5).intValue() == 9 || this.d.b.af.get(i5).intValue() == 13) {
                                if (this.e.m.e() > 1) {
                                    z3 = true;
                                } else {
                                    arrayList2.add(this.d.b.af.get(i5));
                                }
                            } else if (this.d.b.af.get(i5).intValue() == 0 || this.d.b.af.get(i5).intValue() == 11) {
                                z3 = true;
                            } else {
                                arrayList2.add(this.d.b.af.get(i5));
                            }
                        }
                        if (z3) {
                            this.d.b.af = arrayList2;
                        }
                    } else if (this.e.b.l(i4)) {
                        ArrayList arrayList3 = new ArrayList();
                        boolean z4 = false;
                        for (int i6 = 0; i6 < this.d.b.af.size(); i6++) {
                            if (this.d.b.af.get(i6).intValue() == 3 || this.d.b.af.get(i6).intValue() == 6 || this.d.b.af.get(i6).intValue() == 13) {
                                z4 = true;
                            } else {
                                arrayList3.add(this.d.b.af.get(i6));
                            }
                        }
                        if (z4) {
                            this.d.b.af = arrayList3;
                        }
                    }
                }
                i4++;
            }
        } else if (this.e.b.k(this.e.s)) {
            ArrayList arrayList4 = new ArrayList();
            boolean z5 = false;
            for (int i7 = 0; i7 < this.d.b.af.size(); i7++) {
                if (this.d.b.af.get(i7).intValue() == 0 || this.d.b.af.get(i7).intValue() == 11) {
                    z5 = true;
                } else {
                    arrayList4.add(this.d.b.af.get(i7));
                }
            }
            if (z5) {
                this.d.b.af = arrayList4;
            }
        } else if (this.e.b.l(this.e.s)) {
            ArrayList arrayList5 = new ArrayList();
            boolean z6 = false;
            for (int i8 = 0; i8 < this.d.b.af.size(); i8++) {
                if (this.d.b.af.get(i8).intValue() == 3 || this.d.b.af.get(i8).intValue() == 6 || this.d.b.af.get(i8).intValue() == 13) {
                    z6 = true;
                } else {
                    arrayList5.add(this.d.b.af.get(i8));
                }
            }
            if (z6) {
                this.d.b.af = arrayList5;
            }
        }
        this.c.c.k.l = this.d.b.af.size();
        for (int i9 = 0; i9 < this.d.b.af.size(); i9++) {
            GeneralFunction.c cVar = new GeneralFunction.c((int) (((double) k.a(this, (float) (this.L / 50))) * 0.9d));
            this.aX.get(i9).setTextSize((float) cVar.a());
            this.ba.get(i9).setTextSize((float) cVar.a());
            switch (this.d.b.af.get(i9).intValue()) {
                case 0:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_youtube_english);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_youtube_english);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_youtube));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_youtube));
                    break;
                case 1:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_fb_english);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_fb_english);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_facebook));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_facebook));
                    break;
                case 7:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_line_english);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_line_english);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_line));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_line));
                    break;
                case 8:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_whatsapp_english);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_whatsapp_english);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_whatsapp));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_whatsapp));
                    break;
                case 9:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_twitter_english);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_twitter_english);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_twitter));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_twitter));
                    break;
                case 10:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_googlephoto);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_googlephoto);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_googlephoto));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_googlephoto));
                    break;
                case 11:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_youku_english);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_youku_english);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_youku));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_youku));
                    break;
                case 12:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_vk_english);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_vk_english);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_vk));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_vk));
                    break;
                case 13:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_snapchat_english);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_snapchat_english);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_snapchat));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_snapchat));
                    break;
                case 14:
                    this.aW.get(i9).setImageResource(R.drawable.gallery_share_instagram_english);
                    this.aZ.get(i9).setImageResource(R.drawable.gallery_share_instagram_english);
                    this.aX.get(i9).setText(getResources().getString(R.string.share_sns_instagram));
                    this.ba.get(i9).setText(getResources().getString(R.string.share_sns_instagram));
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, ArrayList<File> arrayList) {
        switch (i2) {
            case R.id.IB_ui_Share_Button1:
            case R.id.IB_ui_Share_Button1_Landscape:
                if (this.d.b.af.size() > 0) {
                    d(this.d.b.af.get(0).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.TV_ui_Share_Button1:
            case R.id.TV_ui_Share_Button2:
            case R.id.TV_ui_Share_Button3:
            case R.id.TV_ui_Share_Button4:
            case R.id.TV_ui_Share_Button5:
            case R.id.TV_ui_Share_Button6:
            case R.id.TV_ui_Share_Button7:
            case R.id.TV_ui_Share_Button8:
            case R.id.TV_ui_Share_Button9:
            case R.id.TV_ui_Share_Button10:
            case R.id.TV_ui_Share_Button11:
            case R.id.TV_ui_Share_Button12:
            case R.id.LL_ui_view_SNSLayout_Landscape:
            case R.id.TV_ui_Share_Button1_Landscape:
            case R.id.TV_ui_Share_Button2_Landscape:
            case R.id.TV_ui_Share_Button3_Landscape:
            case R.id.TV_ui_Share_Button4_Landscape:
            case R.id.TV_ui_Share_Button5_Landscape:
            case R.id.TV_ui_Share_Button6_Landscape:
            case R.id.TV_ui_Share_Button7_Landscape:
            case R.id.TV_ui_Share_Button8_Landscape:
            case R.id.TV_ui_Share_Button9_Landscape:
            case R.id.TV_ui_Share_Button10_Landscape:
            case R.id.TV_ui_Share_Button11_Landscape:
            default:
                return;
            case R.id.IB_ui_Share_Button2:
            case R.id.IB_ui_Share_Button2_Landscape:
                if (this.d.b.af.size() > 1) {
                    d(this.d.b.af.get(1).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button3:
            case R.id.IB_ui_Share_Button3_Landscape:
                if (this.d.b.af.size() > 2) {
                    d(this.d.b.af.get(2).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button4:
            case R.id.IB_ui_Share_Button4_Landscape:
                if (this.d.b.af.size() > 3) {
                    d(this.d.b.af.get(3).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button5:
            case R.id.IB_ui_Share_Button5_Landscape:
                if (this.d.b.af.size() > 4) {
                    d(this.d.b.af.get(4).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button6:
            case R.id.IB_ui_Share_Button6_Landscape:
                if (this.d.b.af.size() > 5) {
                    d(this.d.b.af.get(5).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button7:
            case R.id.IB_ui_Share_Button7_Landscape:
                if (this.d.b.af.size() > 6) {
                    d(this.d.b.af.get(6).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button8:
            case R.id.IB_ui_Share_Button8_Landscape:
                if (this.d.b.af.size() > 7) {
                    d(this.d.b.af.get(7).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button9:
            case R.id.IB_ui_Share_Button9_Landscape:
                if (this.d.b.af.size() > 8) {
                    d(this.d.b.af.get(8).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button10:
            case R.id.IB_ui_Share_Button10_Landscape:
                if (this.d.b.af.size() > 9) {
                    d(this.d.b.af.get(9).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button11:
            case R.id.IB_ui_Share_Button11_Landscape:
                if (this.d.b.af.size() > 10) {
                    d(this.d.b.af.get(10).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button12:
            case R.id.IB_ui_Share_Button12_Landscape:
                if (this.d.b.af.size() > 11) {
                    d(this.d.b.af.get(11).intValue(), arrayList);
                    return;
                }
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i2, ArrayList<File> arrayList) {
        switch (i2) {
            case R.id.IB_ui_Share_Button1:
            case R.id.IB_ui_Share_Button1_Landscape:
                if (this.d.b.af.size() > 0) {
                    c(this.d.b.af.get(0).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.TV_ui_Share_Button1:
            case R.id.TV_ui_Share_Button2:
            case R.id.TV_ui_Share_Button3:
            case R.id.TV_ui_Share_Button4:
            case R.id.TV_ui_Share_Button5:
            case R.id.TV_ui_Share_Button6:
            case R.id.TV_ui_Share_Button7:
            case R.id.TV_ui_Share_Button8:
            case R.id.TV_ui_Share_Button9:
            case R.id.TV_ui_Share_Button10:
            case R.id.TV_ui_Share_Button11:
            case R.id.TV_ui_Share_Button12:
            case R.id.LL_ui_view_SNSLayout_Landscape:
            case R.id.TV_ui_Share_Button1_Landscape:
            case R.id.TV_ui_Share_Button2_Landscape:
            case R.id.TV_ui_Share_Button3_Landscape:
            case R.id.TV_ui_Share_Button4_Landscape:
            case R.id.TV_ui_Share_Button5_Landscape:
            case R.id.TV_ui_Share_Button6_Landscape:
            case R.id.TV_ui_Share_Button7_Landscape:
            case R.id.TV_ui_Share_Button8_Landscape:
            case R.id.TV_ui_Share_Button9_Landscape:
            case R.id.TV_ui_Share_Button10_Landscape:
            case R.id.TV_ui_Share_Button11_Landscape:
            default:
                return;
            case R.id.IB_ui_Share_Button2:
            case R.id.IB_ui_Share_Button2_Landscape:
                if (this.d.b.af.size() > 1) {
                    c(this.d.b.af.get(1).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button3:
            case R.id.IB_ui_Share_Button3_Landscape:
                if (this.d.b.af.size() > 2) {
                    c(this.d.b.af.get(2).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button4:
            case R.id.IB_ui_Share_Button4_Landscape:
                if (this.d.b.af.size() > 3) {
                    c(this.d.b.af.get(3).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button5:
            case R.id.IB_ui_Share_Button5_Landscape:
                if (this.d.b.af.size() > 4) {
                    c(this.d.b.af.get(4).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button6:
            case R.id.IB_ui_Share_Button6_Landscape:
                if (this.d.b.af.size() > 5) {
                    c(this.d.b.af.get(5).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button7:
            case R.id.IB_ui_Share_Button7_Landscape:
                if (this.d.b.af.size() > 6) {
                    c(this.d.b.af.get(6).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button8:
            case R.id.IB_ui_Share_Button8_Landscape:
                if (this.d.b.af.size() > 7) {
                    c(this.d.b.af.get(7).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button9:
            case R.id.IB_ui_Share_Button9_Landscape:
                if (this.d.b.af.size() > 8) {
                    c(this.d.b.af.get(8).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button10:
            case R.id.IB_ui_Share_Button10_Landscape:
                if (this.d.b.af.size() > 9) {
                    c(this.d.b.af.get(9).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button11:
            case R.id.IB_ui_Share_Button11_Landscape:
                if (this.d.b.af.size() > 10) {
                    c(this.d.b.af.get(10).intValue(), arrayList);
                    return;
                }
                return;
            case R.id.IB_ui_Share_Button12:
            case R.id.IB_ui_Share_Button12_Landscape:
                if (this.d.b.af.size() > 11) {
                    c(this.d.b.af.get(11).intValue(), arrayList);
                    return;
                }
                return;
        }
    }

    private boolean t(int i2) {
        if (i2 == 4 || i2 == 5 || i2 == 3 || i2 == 2 || i2 == 6) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean u(int i2) {
        for (int i3 = 0; i3 < this.c.c.k.l; i3++) {
            if (i2 == this.aW.get(i3).getId() || i2 == this.aZ.get(i3).getId()) {
                return true;
            }
        }
        return false;
    }

    public void r(boolean z2) {
        if (z2) {
            this.R.setVisibility(0);
            this.R.setEnabled(true);
            return;
        }
        this.R.setVisibility(4);
        this.R.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void d(int i2) {
        int d2 = this.e.m.d();
        if (this.e.f == 5) {
            if (i2 == -1) {
                i2 = this.e.m.e();
            }
            this.i.setText(getResources().getString(R.string.selected) + " " + i2 + " ");
            if (i2 > 1) {
                this.g.setEnabled(false);
                this.g.setImageResource(R.drawable.gallery_top_share_gray);
                this.f.setEnabled(true);
                this.f.setImageAlpha(255);
                if (this.o) {
                    this.h.setEnabled(true);
                    this.h.setImageAlpha(255);
                } else {
                    this.h.setEnabled(false);
                    this.h.setImageAlpha(100);
                }
            } else {
                this.g.setEnabled(true);
                this.g.setImageResource(R.drawable.gallery_top_share_black);
                this.f.setEnabled(true);
                this.f.setImageAlpha(255);
                if (this.o) {
                    this.h.setEnabled(true);
                    this.h.setImageAlpha(255);
                } else {
                    this.h.setEnabled(false);
                    this.h.setImageAlpha(100);
                }
            }
            if (i2 != d2 || d2 == 0) {
                this.S = false;
                this.k.setImageResource(R.drawable.gallery_top_selectall);
            } else {
                this.S = true;
                this.k.setImageResource(R.drawable.gallery_top_selectall_black);
            }
            if (i2 == 0) {
                this.f.setEnabled(false);
                this.f.setImageAlpha(100);
                this.g.setEnabled(false);
                this.g.setImageResource(R.drawable.gallery_top_share_gray);
                this.h.setEnabled(false);
                this.h.setImageAlpha(100);
                return;
            }
            bb();
        }
    }

    private void bb() {
        if (this.y.size() > 0) {
            this.g.setEnabled(false);
            this.g.setImageResource(R.drawable.gallery_top_share_gray);
            this.h.setEnabled(false);
            this.h.setImageAlpha(100);
            return;
        }
        this.g.setEnabled(true);
        this.g.setImageResource(R.drawable.gallery_top_share_black);
        this.h.setEnabled(true);
        this.h.setImageAlpha(255);
    }

    /* access modifiers changed from: protected */
    public void t() {
        if (!this.S) {
            this.c.a(new a.c.a(9017));
            this.S = true;
            return;
        }
        this.c.a(new a.c.a(9018));
        this.S = false;
    }

    /* access modifiers changed from: protected */
    public void a(int i2, boolean z2, int i3, int i4, ArrayList<ui_Controller.b.a> arrayList) {
        ArrayList arrayList2 = new ArrayList();
        GeneralFunction.e.a r2 = this.e.b.r(i2);
        int i5 = r2.b;
        if (z2) {
            for (int i6 = 0; i6 < r2.c.size(); i6++) {
                arrayList2.add(r2.c.get(i6));
            }
        } else {
            arrayList2.add(r2.c.get(0));
        }
        arrayList.add(new ui_Controller.b.a(i5, arrayList2, i3, i4));
    }

    /* access modifiers changed from: protected */
    public void s(boolean z2) {
        o(false);
        if (x()) {
            t(false);
            return;
        }
        if (z2) {
            this.e.m.a(false, true);
            this.e.m.c(0);
            this.e.m.c();
        }
        d(0);
        this.e.f = 0;
        f(false);
        r(true);
    }

    /* access modifiers changed from: protected */
    public void u() {
        switch (this.e.f) {
            case 0:
                if (this.e.b.e() > 0) {
                    this.m.setVisibility(4);
                    return;
                } else {
                    this.m.setVisibility(0);
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void e(int i2) {
        switch (this.e.d) {
            case 1:
                this.e.g = i2;
                return;
            case 2:
                this.e.h = i2;
                return;
            case 3:
                this.e.i = i2;
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void v() {
        this.e.g = 0;
        this.e.h = 0;
        this.e.i = 0;
    }

    /* access modifiers changed from: protected */
    public void f(int i2) {
        this.P.setSelection(i2);
    }

    /* access modifiers changed from: protected */
    public void w() {
        switch (this.e.d) {
            case 1:
                this.P.setSelection(this.e.g);
                return;
            case 2:
                this.P.setSelection(this.e.h);
                return;
            case 3:
                this.P.setSelection(this.e.i);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean x() {
        if (this.s != null && this.s.getVisibility() == 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void t(boolean z2) {
        if (z2) {
            this.t.setVisibility(0);
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.s, "translationY", 1600.0f, 0.0f);
            ofFloat.setDuration(500L);
            ofFloat.start();
            this.s.setVisibility(0);
        } else if (this.t.isShown()) {
            this.t.setVisibility(4);
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.s, "translationY", 0.0f, 1600.0f);
            ofFloat2.addListener(new AnimatorListenerAdapter() {
                /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass13 */

                public void onAnimationEnd(Animator animator) {
                    super.onAnimationEnd(animator);
                    UI_PhoneGalleryController.this.s.setVisibility(4);
                }
            });
            ofFloat2.setDuration(500L);
            ofFloat2.start();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void v(int i2) {
        if (this.e.m.h(i2)) {
            if (this.y.contains(Integer.valueOf(i2))) {
                this.y.remove(Integer.valueOf(i2));
            } else {
                this.y.add(Integer.valueOf(i2));
            }
        }
        this.e.m.a(i2, !this.e.m.f(i2));
        this.e.m.b();
        d(-1);
    }

    /* access modifiers changed from: protected */
    public void u(boolean z2) {
        if (z2) {
            this.cB.setVisibility(0);
        } else {
            this.cB.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void y() {
        this.cB = (FrameLayout) findViewById(R.id.FL_ui_singleview_SingleViewPlayerLayout);
        this.cB.setVisibility(4);
        bc();
        bg();
        Q();
        bh();
        bf();
        bd();
        aV();
    }

    private void bc() {
        View inflate = LayoutInflater.from(this).inflate(R.layout.style_memory_full_layout, (ViewGroup) null);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.LL_MemoryFullLayout);
        linearLayout.setVisibility(0);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass18 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        ((Button) inflate.findViewById(R.id.B_ui_MemoryFullConfirm)).setOnClickListener(this.cI);
        this.U = new PopupWindow(inflate, -1, -1);
    }

    /* access modifiers changed from: protected */
    public void v(boolean z2) {
        if (z2) {
            this.U.showAtLocation(this.cB, 17, 0, 0);
        } else {
            this.U.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public boolean z() {
        return this.U.isShowing();
    }

    /* access modifiers changed from: protected */
    public boolean A() {
        long a2 = d.a(ui_Controller.a.c.c, 0);
        a("dlAvailableSize: " + a2, 3);
        if (a2 < 53477376) {
            return true;
        }
        return false;
    }

    private void bd() {
        this.cv = (LinearLayout) findViewById(R.id.LL_ui_singleview_MoreSettingTopLayout);
        this.cw = (ImageButton) findViewById(R.id.IB_ui_singleview_MoreSettingReturn);
        this.cx = (ListView) findViewById(R.id.LV_ui_singleview_MoreSettingListView);
        this.cy = (TextView) findViewById(R.id.TV_ui_singleview_MoreSetting);
        this.cz = findViewById(R.id.V_ui_singleview_MoreSetting);
        this.cy.setTextSize((float) k.a(this, (float) (this.L / 35)));
        this.cy.setText(R.string.more_setting);
        this.cy.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass19 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.cz.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass20 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.cw.setImageResource(R.drawable.system_close_white);
        if (this.o) {
            this.cA = new d(this, 0);
        } else {
            this.cA = new d(this, 3);
        }
        this.cx.setAdapter((ListAdapter) this.cA);
        this.cv.setVisibility(4);
        this.cw.setOnClickListener(this.cI);
        this.cx.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass21 */

            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                switch ((int) j) {
                    case 0:
                        UI_PhoneGalleryController.this.H();
                        return;
                    case 1:
                        UI_PhoneGalleryController.this.w(false);
                        UI_PhoneGalleryController.this.B(false);
                        if (UI_PhoneGalleryController.this.e.d == 1) {
                            o.a(UI_PhoneGalleryController.this.H, true, true, UI_PhoneGalleryController.this.getResources().getString(R.string.move), UI_PhoneGalleryController.this.getResources().getString(R.string.move_single_to_sdcard_dialog), new String[]{UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_cancel), UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_ok_for_two_button)}, new int[]{9135, 9132}, true);
                            return;
                        }
                        o.a(UI_PhoneGalleryController.this.H, true, true, UI_PhoneGalleryController.this.getResources().getString(R.string.move), UI_PhoneGalleryController.this.getResources().getString(R.string.move_single_to_phone_dialog), new String[]{UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_cancel), UI_PhoneGalleryController.this.getResources().getString(R.string.dialog_option_ok_for_two_button)}, new int[]{9135, 9133}, true);
                        return;
                    case 2:
                        UI_PhoneGalleryController.this.w(false);
                        UI_PhoneGalleryController.this.B(false);
                        UI_PhoneGalleryController.this.a(9088, 0L);
                        return;
                    case 3:
                    default:
                        return;
                    case 4:
                        UI_PhoneGalleryController.this.w(false);
                        UI_PhoneGalleryController.this.B(false);
                        UI_PhoneGalleryController.this.a(9145, 0L);
                        return;
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void w(boolean z2) {
        if (z2) {
            int a2 = this.e.b.a(this.e.s);
            a("showAllWidget, lFileType:" + a2 + ", bShow:" + z2, 2);
            if (a2 == -1) {
                x(true);
            } else if (a2 == 6) {
                x(true);
                if (this.e.d != 3) {
                    a(true, true, true);
                } else {
                    a(true, false, false);
                }
            } else if (a2 != 0) {
                a(true, false, true);
                x(false);
            } else if (this.e.d != 3) {
                a(true, true, true);
            } else {
                a(true, false, false);
            }
        } else {
            a(false, false, false);
            x(true);
        }
    }

    /* access modifiers changed from: protected */
    public void x(boolean z2) {
        this.bU.setMediaControllerHide(z2);
    }

    /* access modifiers changed from: protected */
    public void B() {
        this.bU.seekTo(0);
        this.bU.b(0);
    }

    /* access modifiers changed from: protected */
    public void g(int i2) {
        this.bU.seekTo(i2);
        this.bU.b(i2);
    }

    /* access modifiers changed from: protected */
    public void C() {
        this.bU.h();
    }

    /* access modifiers changed from: protected */
    public void D() {
        if (this.bU != null) {
            this.bU.pause();
        }
    }

    /* access modifiers changed from: protected */
    public void E() {
        if (this.bU != null) {
            this.bU.e();
        }
    }

    /* access modifiers changed from: protected */
    public void F() {
        if (this.bU != null) {
            this.bU.b();
        }
    }

    private void be() {
        String o2 = this.e.b.o(this.e.s);
        if (o2 != null && !new File(o2).exists()) {
            g(true);
            if (!this.A) {
                this.A = true;
                a(9139, 0L);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void G() {
        if (this.bU != null) {
            String o2 = this.e.b.o(this.e.s);
            int a2 = this.e.b.a(this.e.s);
            if (!new File(o2).exists()) {
                g(true);
                if (!this.A) {
                    this.A = true;
                    a(9139, 0L);
                }
            } else if (a2 == 1) {
                this.bU.start();
                C();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void H() {
        int i2;
        int i3;
        if (this.cA.a()) {
            y(false);
            a(9138);
            this.cA.b();
            ar();
            return;
        }
        a(true, false, true);
        y(true);
        l(6);
        this.M = 2;
        this.N = 6;
        if (this.L > this.K) {
            i2 = this.L;
            i3 = this.K;
        } else {
            i2 = this.K;
            i3 = this.L;
        }
        this.bU.a(i2, i3);
        this.e.Y = this.bU.getInteractiveMode();
        a(9137);
        this.cA.b();
        x(true);
    }

    /* access modifiers changed from: protected */
    public void y(boolean z2) {
        this.cA.a(z2);
    }

    private void J(boolean z2) {
        if (z2) {
            this.cr.setEnabled(false);
            this.cj.setEnabled(false);
            this.cj.setImageAlpha(100);
            this.cn.setAlpha(0.4f);
            this.cs.setEnabled(false);
            this.ck.setEnabled(false);
            this.ck.setImageAlpha(100);
            this.co.setAlpha(0.4f);
            this.ct.setEnabled(false);
            this.cl.setEnabled(false);
            this.cl.setImageAlpha(100);
            this.cp.setAlpha(0.4f);
            return;
        }
        this.cr.setEnabled(true);
        this.cj.setEnabled(true);
        this.cj.setImageAlpha(255);
        this.cn.setAlpha(1.0f);
        this.cs.setEnabled(true);
        this.ck.setEnabled(true);
        this.ck.setImageAlpha(255);
        this.co.setAlpha(1.0f);
        this.ct.setEnabled(true);
        this.cl.setEnabled(true);
        this.cl.setImageAlpha(255);
        this.cp.setAlpha(1.0f);
    }

    /* access modifiers changed from: protected */
    public void z(boolean z2) {
        if (z2) {
            this.e.N = 3;
            J(true);
            l(6);
            this.M = 2;
            this.bU.a(this.L, this.K);
            this.e.Y = this.bU.getInteractiveMode();
            a(9137);
            I(false);
            return;
        }
        J(false);
        a(9138);
    }

    /* access modifiers changed from: protected */
    public void I() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.bU.a(Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels), Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels));
        this.bU.setViewMode(1);
        this.e.R = 1;
        j(3);
    }

    /* access modifiers changed from: protected */
    public void J() {
        this.bU.setViewMode(0);
        this.e.R = 0;
        bk();
    }

    /* access modifiers changed from: protected */
    public void A(boolean z2) {
        if (z2) {
            this.M = 1;
            switch (this.N) {
                case 1:
                    l(1);
                    this.N = 1;
                    this.d.k = 1;
                    return;
                case 9:
                    l(9);
                    this.N = 9;
                    this.d.k = 9;
                    return;
                default:
                    l(1);
                    this.N = 1;
                    this.d.k = 1;
                    return;
            }
        }
    }

    private void bf() {
        this.ci = (LinearLayout) findViewById(R.id.LL_ui_singleview_BottomNormalLayout);
        this.cj = (ImageButton) findViewById(R.id.IB_ui_singleview_btnViewType);
        this.ck = (ImageButton) findViewById(R.id.IB_ui_singleview_btnSnapshot);
        this.cl = (ImageButton) findViewById(R.id.IB_ui_singleview_btnEdit);
        this.cm = (ImageButton) findViewById(R.id.IB_ui_singleview_btnDrag);
        this.cn = (TextView) findViewById(R.id.TV_ui_singleview_btnViewType);
        this.co = (TextView) findViewById(R.id.TV_ui_singleview_btnSnapshot);
        this.cp = (TextView) findViewById(R.id.TV_ui_singleview_btnEdit);
        this.cq = (TextView) findViewById(R.id.TV_ui_singleview_btnDrag);
        this.cr = (LinearLayout) findViewById(R.id.LL_ui_singleview_LayoutViewType);
        this.cs = (LinearLayout) findViewById(R.id.LL_ui_singleview_LayoutSnapshot);
        this.ct = (LinearLayout) findViewById(R.id.LL_ui_singleview_LayoutEdit);
        this.cu = (LinearLayout) findViewById(R.id.LL_ui_singleview_LayoutDrag);
        this.ci.setVisibility(4);
        this.ci.setBackgroundColor(getResources().getColor(R.color.blackt50));
        this.ck.setImageResource(R.drawable.gallery_bottom_snapshot);
        this.cl.setImageResource(R.drawable.gallery_bottom_edit);
        int a2 = k.a(this, (float) (this.K / 30));
        GeneralFunction.c q2 = new GeneralFunction.c(a2).c((int) (((double) a2) * 0.8d)).n((int) (((double) a2) * 0.8d)).g((int) (((double) a2) * 0.9d)).o((int) (((double) a2) * 0.8d)).i((int) (((double) a2) * 0.9d)).j((int) (((double) a2) * 0.8d)).q((int) (((double) a2) * 0.9d));
        this.cn.setTextSize((float) q2.a());
        this.co.setTextSize((float) q2.a());
        this.cp.setTextSize((float) q2.a());
        this.cq.setTextSize((float) q2.a());
        bi();
        this.cj.setOnClickListener(this.cI);
        this.ck.setOnClickListener(this.cI);
        this.cl.setOnClickListener(this.cI);
        this.cm.setOnClickListener(this.cI);
        this.cr.setOnClickListener(this.cH);
        this.cs.setOnClickListener(this.cH);
        this.ct.setOnClickListener(this.cH);
        this.cu.setOnClickListener(this.cH);
        this.ci.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass22 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2, boolean z3, boolean z4) {
        if (!this.e.A) {
            a("UI_ShowInformation " + z2 + " " + z3 + " " + z4 + " " + this.e.v, 4);
            if (z2) {
                this.e.v = true;
            } else {
                this.e.v = false;
            }
            C(z2);
            if (z3) {
                E(true);
            } else {
                E(false);
            }
            if (z4) {
                D(true);
            } else {
                D(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void B(boolean z2) {
        if (this.cv != null) {
            if (z2) {
                this.cv.setVisibility(0);
            } else {
                this.cv.setVisibility(4);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean K() {
        if (this.cv != null && this.cv.getVisibility() == 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean L() {
        if (!K() && !o.a(this) && !r()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void C(boolean z2) {
        if (z2) {
            this.bZ.setVisibility(0);
        } else {
            this.bZ.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void M() {
        if (this.ce.getVisibility() == 0) {
            if (ax()) {
                this.cf.setVisibility(0);
            } else {
                this.cf.setVisibility(4);
            }
            if (aw()) {
                this.cg.setVisibility(0);
            } else {
                this.cg.setVisibility(4);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void D(boolean z2) {
        if (z2) {
            this.ce.setVisibility(0);
        } else {
            this.ce.setVisibility(4);
        }
        M();
    }

    /* access modifiers changed from: protected */
    public void E(boolean z2) {
        if (z2) {
            this.ci.setVisibility(0);
        } else {
            this.ci.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void h(int i2) {
        if (this.e.b.i(i2)) {
            a(i2, 0);
        } else {
            a(i2, 20);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3) {
        this.bR = (ListHorizontalScrollView) findViewById(R.id.LHSV_ui_singleview_ListView);
        this.bS = (SingleHorizontalScrollView) findViewById(R.id.LHSV_ui_singleview_ImageView);
        b.c cVar = new b.c();
        cVar.f1340a = this.K;
        cVar.b = this.L;
        cVar.d = this.bS;
        cVar.e = this.bU;
        cVar.f = this.e.b;
        cVar.g = this.cC;
        cVar.h = this.cD;
        this.bP = new ui_Controller.ui_Gallery.b(this);
        this.bP.a(this);
        this.bP.a(cVar, i2, i3);
        a(this.e.b.a(this.e.s), this.e.b.i(this.e.s) && !this.e.b.j(this.e.s));
        this.bS.setEnableScrolling(true);
    }

    /* access modifiers changed from: protected */
    public void N() {
        this.bS.e();
    }

    /* access modifiers changed from: protected */
    public void O() {
        this.bS.f();
    }

    /* access modifiers changed from: protected */
    public void P() {
        if (this.bP != null) {
            this.bP.c();
        }
    }

    /* access modifiers changed from: protected */
    public void i(int i2) {
        this.bP.c(i2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean w(int i2) {
        File file = new File(this.e.b.o(i2));
        if (file.exists() && !this.bP.d(i2)) {
            return false;
        }
        a("Error file:" + i2 + ", " + file.exists(), 1);
        return true;
    }

    private void bg() {
        this.ce = (FrameLayout) findViewById(R.id.FL_ui_singleview_btnPreAndNext);
        this.cf = (ImageButton) findViewById(R.id.IB_ui_singleview_Previous);
        this.cg = (ImageButton) findViewById(R.id.IB_ui_singleview_Next);
        this.ch = (Space) findViewById(R.id.Space_Anchor);
        this.cf.setOnClickListener(this.cI);
        this.cg.setOnClickListener(this.cI);
        this.ce.setVisibility(4);
        int i2 = this.L / 20;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(i2, i2);
        layoutParams.setMargins(0, 0, this.K / 15, 0);
        this.cg.setLayoutParams(layoutParams);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(i2, i2);
        layoutParams2.setMargins(this.K / 15, 0, 0, 0);
        this.cf.setLayoutParams(layoutParams2);
        this.cf.setImageResource(R.drawable.gallery_arrowleft);
        this.cg.setImageResource(R.drawable.gallery_singleview_arrow_right_normal);
    }

    private void bh() {
        this.bZ = (LinearLayout) findViewById(R.id.LL_ui_singleview_TopLayout);
        this.bZ.setVisibility(4);
        this.cb = (TextView) findViewById(R.id.TV_ui_singleview_information);
        this.cb.setTextSize((float) k.a(this, (float) (this.L / 44)));
        this.cb.setGravity(16);
        this.cb.setTextColor(-1);
        this.cb.setVisibility(4);
        this.ca = (ImageButton) findViewById(R.id.IB_ui_singleview_btnReturn);
        this.cc = (ImageButton) findViewById(R.id.IB_ui_singleview_btnShareTab);
        this.cd = (ImageButton) findViewById(R.id.IB_ui_singleview_btnMoreTab);
        this.cc.setOnClickListener(this.cI);
        this.cd.setOnClickListener(this.cI);
        this.bZ.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass26 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.cc.setImageResource(R.drawable.gallery_top_share_white);
        this.cd.setImageResource(R.drawable.gallery_top_more);
        this.ca.setVisibility(4);
    }

    public void a(int i2, boolean z2) {
        a("GallerySingle_UpdateTopLayoutButtons " + i2 + " " + z2, 4);
        if (this.e.N == 0) {
            int i3 = this.e.O;
            if (i3 == 4 || i3 != 5) {
            }
        } else {
            int i4 = this.e.O;
            if (i4 == 4 || i4 == 5) {
                this.bU.setEnable(true);
            } else if (this.e.r) {
                this.bU.setEnable(true);
            } else if (i2 == 1 || i2 == 5) {
                this.bU.setEnable(true);
            } else if (i2 == 2) {
                this.bU.setEnable(false);
            } else if (i2 == 3) {
                this.bU.setEnable(false);
            } else {
                this.bU.setEnable(true);
            }
        }
        if (this.e.r) {
            int i5 = this.e.s;
            GeneralFunction.e.d dVar = this.e.b;
            this.ca.setVisibility(0);
            if (this.bP == null || !dVar.p(i5)) {
                this.cc.setVisibility(4);
            } else if (this.bP.d(i5)) {
                this.cc.setVisibility(4);
            } else {
                this.cc.setVisibility(0);
            }
        } else {
            int i6 = this.e.s;
            GeneralFunction.e.d dVar2 = this.e.b;
            this.ca.setVisibility(0);
            if (this.e.m == null || !dVar2.p(i6)) {
                this.cc.setVisibility(4);
            } else if (this.e.m.h(i6)) {
                this.cc.setVisibility(4);
            } else {
                this.cc.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void Q() {
        a("GallerySingle_VideoPlayer_Init", 4);
        this.bT = (LinearLayout) findViewById(R.id.LL_ui_singleview_VideoPlayer);
        this.bU = (SphericalVideoPlayer) findViewById(R.id.TV_ui_singleview_VideoPlayer);
        this.bU.setPlayerStatusListener(this.cG);
        this.bU.setAdditionalButtonCallback(this.cE);
        this.bU.setOnClickListener(new SphericalVideoPlayer.d() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass27 */

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.d
            public void a(MotionEvent motionEvent) {
                UI_PhoneGalleryController.this.a((UI_PhoneGalleryController) "TV_ui_singleview_VideoPlayer onClick", (String) 4);
                UI_PhoneGalleryController.this.a(8962);
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void bi() {
        int i2 = this.e.Q;
        int i3 = this.e.P;
        switch (i2) {
            case 2:
                this.cj.setImageResource(R.drawable.gallery_bottom_perspective_white);
                this.cn.setText(R.string.perspective);
                break;
            case 3:
                this.cj.setImageResource(R.drawable.gallery_bottom_fisheye_white);
                this.cn.setText(R.string.fisheye);
                break;
            case 5:
                this.cj.setImageResource(R.drawable.gallery_bottom_littleplanet_white);
                this.cn.setText(R.string.little_planet);
                break;
            case 6:
                this.cj.setImageResource(R.drawable.gallery_bottom_crystalball_white);
                this.cn.setText(R.string.crystal_ball);
                break;
        }
        switch (i3) {
            case 1:
                this.cm.setImageResource(R.drawable.gallery_bottom_drag_white);
                this.cq.setText(R.string.drag);
                return;
            case 2:
            default:
                return;
            case 3:
                this.cm.setImageResource(R.drawable.gallery_bottom_gyro_white);
                this.cq.setText(R.string.gyro);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void R() {
        this.bU.m();
    }

    /* access modifiers changed from: protected */
    public void F(boolean z2) {
        View inflate = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.ui_singleview_bottom_interactive_tool, (ViewGroup) null);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_BottomList_interactive_Layout);
        ImageButton imageButton = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_BottomList_Drag);
        TextView textView = (TextView) inflate.findViewById(R.id.TV_ui_singleview_BottomList_Drag);
        ImageButton imageButton2 = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_BottomList_Gyroscope);
        TextView textView2 = (TextView) inflate.findViewById(R.id.TV_ui_singleview_BottomList_Gyroscope);
        ImageButton imageButton3 = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_BottomList_VR);
        TextView textView3 = (TextView) inflate.findViewById(R.id.TV_ui_singleview_BottomList_VR);
        int a2 = k.a(this.H, (float) (Math.min(this.K, this.L) / 33));
        linearLayout.setBackgroundResource(R.drawable.gallery_menu_viewcontrol);
        imageButton.setImageResource(R.drawable.gallery_bottom_drag_white);
        textView.setText(R.string.drag);
        textView.setTextColor(getResources().getColor(R.color.white));
        imageButton2.setImageResource(R.drawable.gallery_bottom_gyro_white);
        textView2.setText(R.string.gyro);
        textView2.setTextColor(getResources().getColor(R.color.white));
        imageButton3.setImageResource(R.drawable.gallery_bottom_vr_white);
        textView3.setText(R.string.vr);
        textView3.setTextColor(getResources().getColor(R.color.white));
        GeneralFunction.c q2 = new GeneralFunction.c(a2).b((int) (((double) a2) * 0.8d)).c((int) (((double) a2) * 0.8d)).n((int) (((double) a2) * 0.8d)).g((int) (((double) a2) * 0.8d)).d((int) (((double) a2) * 0.8d)).k((int) (((double) a2) * 0.8d)).h((int) (((double) a2) * 0.8d)).o((int) (((double) a2) * 0.8d)).i((int) (((double) a2) * 0.9d)).j((int) (((double) a2) * 0.8d)).q((int) (((double) a2) * 0.9d));
        textView.setTextSize((float) q2.a());
        textView2.setTextSize((float) q2.a());
        textView3.setTextSize((float) q2.a());
        ((LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_BottomList_Drag_Layout)).setOnClickListener(this.cF);
        imageButton.setOnClickListener(this.cF);
        ((LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_BottomList_Gyroscope_Layout)).setOnClickListener(this.cF);
        imageButton2.setOnClickListener(this.cF);
        ((LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_BottomList_VR_Layout)).setOnClickListener(this.cF);
        imageButton3.setOnClickListener(this.cF);
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        layoutParams.width = (int) (((double) this.K) * 0.5d);
        layoutParams.height = (int) (((double) this.L) * 0.1d);
        this.T = new PopupWindow(inflate, -2, -2);
        this.T.setTouchable(true);
        this.T.setOutsideTouchable(true);
        this.T.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        this.T.setOnDismissListener(this.F);
        this.T.setAnimationStyle(R.style.PopupAnimation);
        if (z2) {
            int i2 = this.L / 20;
            if (ui_Controller.b.d.a().b()) {
                i2 = 0;
            }
            this.T.showAsDropDown(this.ch, (this.ch.getWidth() / 2) - (layoutParams.width / 3), -((layoutParams.height + this.bU.getMediaControllerHeight()) - i2));
        } else {
            this.T.showAsDropDown(this.cm, (int) (((double) (this.cm.getWidth() / 3)) - (((double) layoutParams.width) / 1.3d)), -((int) (((double) this.cm.getHeight()) + (((double) layoutParams.height) * 1.1d))));
        }
        this.T.update();
        this.C = true;
    }

    /* access modifiers changed from: protected */
    public void G(boolean z2) {
        View inflate = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.ui_singleview_bottom_viewtype_tool, (ViewGroup) null);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_BottomList_ViewType_Layout);
        LinearLayout linearLayout2 = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_BottomList_Perspective_Layout);
        ImageButton imageButton = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_BottomList_Perspective);
        TextView textView = (TextView) inflate.findViewById(R.id.TV_ui_singleview_BottomList_Perspective);
        LinearLayout linearLayout3 = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_BottomList_Fisheye_Layout);
        ImageButton imageButton2 = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_BottomList_Fisheye);
        TextView textView2 = (TextView) inflate.findViewById(R.id.TV_ui_singleview_BottomList_Fisheye);
        LinearLayout linearLayout4 = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_BottomList_CrystalBall_Layout);
        ImageButton imageButton3 = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_BottomList_CrystalBall);
        TextView textView3 = (TextView) inflate.findViewById(R.id.TV_ui_singleview_BottomList_CrystalBall);
        LinearLayout linearLayout5 = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_BottomList_LittlePlanet_Layout);
        ImageButton imageButton4 = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_BottomList_LittlePlanet);
        TextView textView4 = (TextView) inflate.findViewById(R.id.TV_ui_singleview_BottomList_LittlePlanet);
        int a2 = k.a(this.H, (float) (Math.min(this.K, this.L) / 33));
        if (z2) {
            linearLayout.setBackgroundResource(R.drawable.gallery_menu_viewcontrol);
        } else {
            linearLayout.setBackgroundResource(R.drawable.gallery_menu_viewmode);
        }
        imageButton.setImageResource(R.drawable.gallery_bottom_perspective_white);
        textView.setText(R.string.perspective);
        textView.setTextColor(getResources().getColor(R.color.white));
        imageButton2.setImageResource(R.drawable.gallery_bottom_fisheye_white);
        textView2.setText(R.string.fisheye);
        textView2.setTextColor(getResources().getColor(R.color.white));
        imageButton3.setImageResource(R.drawable.gallery_bottom_crystalball_white);
        textView3.setText(R.string.crystal_ball);
        textView3.setTextColor(getResources().getColor(R.color.white));
        imageButton4.setImageResource(R.drawable.gallery_bottom_littleplanet_white);
        textView4.setText(R.string.little_planet);
        textView4.setTextColor(getResources().getColor(R.color.white));
        GeneralFunction.c q2 = new GeneralFunction.c(a2).b((int) (((double) a2) * 0.8d)).c((int) (((double) a2) * 0.8d)).n((int) (((double) a2) * 0.8d)).g((int) (((double) a2) * 0.8d)).d((int) (((double) a2) * 0.8d)).k((int) (((double) a2) * 0.8d)).h((int) (((double) a2) * 0.8d)).o((int) (((double) a2) * 0.8d)).i((int) (((double) a2) * 0.9d)).p((int) (((double) a2) * 0.9d)).j((int) (((double) a2) * 0.8d)).q((int) (((double) a2) * 0.9d));
        textView.setTextSize((float) q2.a());
        textView2.setTextSize((float) q2.a());
        textView3.setTextSize((float) q2.a());
        textView4.setTextSize((float) q2.a());
        linearLayout2.setOnClickListener(this.cF);
        imageButton.setOnClickListener(this.cF);
        linearLayout3.setOnClickListener(this.cF);
        imageButton2.setOnClickListener(this.cF);
        linearLayout4.setOnClickListener(this.cF);
        imageButton3.setOnClickListener(this.cF);
        linearLayout5.setOnClickListener(this.cF);
        imageButton4.setOnClickListener(this.cF);
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        layoutParams.width = (int) (((double) this.K) * 0.7d);
        layoutParams.height = (int) (((double) this.L) * 0.1d);
        this.T = new PopupWindow(inflate, -2, -2);
        this.T.setTouchable(true);
        this.T.setOutsideTouchable(true);
        this.T.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        this.T.setOnDismissListener(this.F);
        this.T.setAnimationStyle(R.style.PopupAnimation);
        if (z2) {
            int i2 = this.L / 20;
            if (ui_Controller.b.d.a().b()) {
                i2 = 0;
            }
            this.T.showAsDropDown(this.ch, (int) (((double) (this.ch.getWidth() / 2)) - (((double) layoutParams.width) * 1.1d)), -((layoutParams.height + this.bU.getMediaControllerHeight()) - i2));
        } else {
            this.T.showAsDropDown(this.cj, (this.cj.getWidth() / 3) - (layoutParams.width / 8), -((int) (((double) this.cj.getHeight()) + (((double) layoutParams.height) * 1.1d))));
        }
        this.T.update();
        this.C = true;
    }

    /* access modifiers changed from: protected */
    public void S() {
        View inflate = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.ui_singleview_edit_tools, (ViewGroup) null);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_EditTool_Layout);
        LinearLayout linearLayout2 = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_EditTool_ConvertToGif_Layout);
        ImageButton imageButton = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_EditTool_ConvertToGif);
        TextView textView = (TextView) inflate.findViewById(R.id.TV_ui_singleview_EditTool_ConvertToGif);
        LinearLayout linearLayout3 = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_EditTool_NormalPicOrMovieEdit_Layout);
        ImageButton imageButton2 = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_EditTool_NormalPicOrMovieEdit);
        TextView textView2 = (TextView) inflate.findViewById(R.id.TV_ui_singleview_EditTool_NormalPicOrMovieEdit);
        int a2 = k.a(this.H, (float) (Math.min(this.K, this.L) / 33));
        linearLayout.setBackgroundResource(R.drawable.gallery_editingtools_l2);
        imageButton.setImageResource(R.drawable.gallery_bottom_gif);
        textView.setText(R.string.edit_tool_gif);
        textView.setTextSize((float) a2);
        textView.setTextColor(getResources().getColor(R.color.white));
        imageButton2.setImageResource(R.drawable.gallery_bottom_moviecut);
        textView2.setText(R.string.edit_tool_movie_edit);
        textView2.setTextColor(getResources().getColor(R.color.white));
        GeneralFunction.c q2 = new GeneralFunction.c(a2).b((int) (((double) a2) * 0.8d)).c((int) (((double) a2) * 0.8d)).n((int) (((double) a2) * 0.8d)).g((int) (((double) a2) * 0.8d)).d((int) (((double) a2) * 0.8d)).k((int) (((double) a2) * 0.8d)).h((int) (((double) a2) * 0.8d)).o((int) (((double) a2) * 0.8d)).i((int) (((double) a2) * 0.9d)).j((int) (((double) a2) * 0.8d)).q((int) (((double) a2) * 0.9d));
        textView.setTextSize((float) q2.a());
        textView2.setTextSize((float) q2.a());
        linearLayout2.setOnClickListener(this.cF);
        imageButton.setOnClickListener(this.cF);
        linearLayout3.setOnClickListener(this.cF);
        imageButton2.setOnClickListener(this.cF);
        if (this.e.b.d(this.e.s) <= 2) {
            linearLayout2.setEnabled(false);
            imageButton.setEnabled(false);
            imageButton.setImageAlpha(100);
            textView.setAlpha(0.4f);
            linearLayout3.setEnabled(false);
            imageButton2.setEnabled(false);
            imageButton2.setImageAlpha(100);
            textView2.setAlpha(0.4f);
        } else {
            linearLayout2.setEnabled(true);
            imageButton.setEnabled(true);
            imageButton.setImageAlpha(255);
            textView.setAlpha(1.0f);
            linearLayout3.setEnabled(true);
            imageButton2.setEnabled(true);
            imageButton2.setImageAlpha(255);
            textView2.setAlpha(1.0f);
        }
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        layoutParams.width = (int) (((double) this.K) * 0.4d);
        layoutParams.height = (int) (((double) this.L) * 0.1d);
        this.T = new PopupWindow(inflate, -2, -2);
        this.T.setTouchable(true);
        this.T.setOutsideTouchable(true);
        this.T.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        this.T.setOnDismissListener(this.F);
        this.T.setAnimationStyle(R.style.PopupAnimation);
        int i2 = this.L / 20;
        if (ui_Controller.b.d.a().b()) {
            i2 = 0;
        }
        this.T.showAsDropDown(this.ch, (int) (((double) (this.ch.getWidth() / 2)) - (((double) layoutParams.width) / 1.5d)), -((layoutParams.height + this.bU.getMediaControllerHeight()) - i2));
        this.T.update();
        this.C = true;
    }

    /* access modifiers changed from: protected */
    public void T() {
        View inflate = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.ui_singleview_edit_tools, (ViewGroup) null);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_EditTool_Layout);
        ImageButton imageButton = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_EditTool_ConvertToGif);
        TextView textView = (TextView) inflate.findViewById(R.id.TV_ui_singleview_EditTool_ConvertToGif);
        ImageButton imageButton2 = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_EditTool_NormalPicOrMovieEdit);
        TextView textView2 = (TextView) inflate.findViewById(R.id.TV_ui_singleview_EditTool_NormalPicOrMovieEdit);
        int a2 = k.a(this.H, (float) (Math.min(this.K, this.L) / 33));
        linearLayout.setBackgroundResource(R.drawable.gallery_editingtools_l1);
        imageButton.setImageResource(R.drawable.gallery_bottom_gif);
        textView.setText(R.string.edit_tool_gif);
        textView.setTextColor(getResources().getColor(R.color.white));
        imageButton2.setImageResource(R.drawable.gallery_bottom_covert);
        textView2.setText(R.string.edit_tool_nomal_pic);
        textView2.setTextColor(getResources().getColor(R.color.white));
        GeneralFunction.c q2 = new GeneralFunction.c(a2).b((int) (((double) a2) * 0.8d)).c((int) (((double) a2) * 0.8d)).n((int) (((double) a2) * 0.8d)).g((int) (((double) a2) * 0.8d)).d((int) (((double) a2) * 0.8d)).k((int) (((double) a2) * 0.8d)).h((int) (((double) a2) * 0.8d)).o((int) (((double) a2) * 0.8d)).i((int) (((double) a2) * 0.9d)).j((int) (((double) a2) * 0.8d)).q((int) (((double) a2) * 0.9d));
        textView.setTextSize((float) q2.a());
        textView2.setTextSize((float) q2.a());
        ((LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_EditTool_ConvertToGif_Layout)).setOnClickListener(this.cF);
        imageButton.setOnClickListener(this.cF);
        ((LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_EditTool_NormalPicOrMovieEdit_Layout)).setOnClickListener(this.cF);
        imageButton2.setOnClickListener(this.cF);
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        layoutParams.width = (int) (((double) this.K) * 0.4d);
        layoutParams.height = (int) (((double) this.L) * 0.1d);
        this.T = new PopupWindow(inflate, -2, -2);
        this.T.setTouchable(true);
        this.T.setOutsideTouchable(true);
        this.T.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        this.T.setOnDismissListener(this.F);
        this.T.setAnimationStyle(R.style.PopupAnimation);
        this.T.showAsDropDown(this.cl, (this.cl.getWidth() / 3) - (layoutParams.width / 4), -((int) (((double) this.cl.getHeight()) + (((double) layoutParams.height) * 1.1d))));
        this.T.update();
        this.C = true;
    }

    /* access modifiers changed from: protected */
    public void U() {
        View inflate = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.ui_singleview_snapshot_tools, (ViewGroup) null);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_Snapshot_Layout);
        ImageButton imageButton = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_Snapshot_Confirm);
        TextView textView = (TextView) inflate.findViewById(R.id.TV_ui_singleview_Snapshot_Confirm);
        ImageButton imageButton2 = (ImageButton) inflate.findViewById(R.id.IB_ui_singleview_Snapshot_Cancel);
        TextView textView2 = (TextView) inflate.findViewById(R.id.TV_ui_singleview_Snapshot_Cancel);
        int a2 = k.a(this.H, (float) (Math.min(this.K, this.L) / 33));
        linearLayout.setBackgroundColor(getResources().getColor(R.color.blackt50));
        imageButton.setImageResource(R.drawable.gallery_snapshot_yes);
        textView.setText(R.string.edit_save);
        textView.setTextSize((float) a2);
        textView.setTextColor(getResources().getColor(R.color.white));
        imageButton2.setImageResource(R.drawable.gallery_snapshot_no);
        textView2.setText(R.string.edit_cancel);
        textView2.setTextSize((float) a2);
        textView2.setTextColor(getResources().getColor(R.color.white));
        ((LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_Snapshot_Confirm_Layout)).setOnClickListener(this.cF);
        imageButton.setOnClickListener(this.cF);
        ((LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_Snapshot_Cancel_Layout)).setOnClickListener(this.cF);
        imageButton2.setOnClickListener(this.cF);
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        layoutParams.width = (int) (((double) this.K) * 0.4d);
        layoutParams.height = (int) (((double) this.L) * 0.1d);
        this.T = new PopupWindow(inflate, -2, -2);
        this.T.setTouchable(true);
        this.T.setOutsideTouchable(false);
        this.T.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) null));
        this.T.setOnDismissListener(this.F);
        this.T.setAnimationStyle(R.style.PopupAnimation);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.T.showAtLocation(this.ci, 0, (displayMetrics.widthPixels / 2) - (layoutParams.width / 2), (displayMetrics.heightPixels * 3) / 4);
        this.T.update();
        this.C = true;
    }

    private void bj() {
        this.br.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass29 */

            public void onGlobalLayout() {
                UI_PhoneGalleryController.this.a(9144);
                UI_PhoneGalleryController.this.br.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void V() {
        this.C = false;
        this.bU.n();
    }

    public boolean W() {
        if (this.T == null || !this.T.isShowing()) {
            return false;
        }
        return true;
    }

    public void X() {
        if (this.T != null) {
            this.T.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void Y() {
        a("GallerySingle_VideoPlayer_PlayPhoto", 4);
        this.bU.setPlayerMode(3);
        this.bU.setViewMode(this.e.R);
        if (this.e.X != null) {
            this.e.Q = this.e.X.f51a;
            this.e.P = this.e.X.s;
        }
        this.bU.setInteractiveMode(this.e.P);
        this.bU.setViewType(this.e.Q);
        if (this.e.X != null) {
            this.bU.setSphericalParameter(this.e.X);
        }
        int d2 = this.bU.d();
        if (d2 != 1) {
            a("PlayerStatus error:" + d2, 2);
        } else if (this.bU.g() == -1) {
            a("Play fail", 2);
            aa();
        }
        H(false);
        ar();
    }

    /* access modifiers changed from: protected */
    public boolean a(Bitmap bitmap, int i2, int i3) {
        a("GallerySingle_VideoPlayer_UpdateFrame " + bitmap, 4);
        int d2 = this.bU.d();
        int a2 = this.e.b.a(this.e.s);
        String o2 = this.e.b.o(this.e.s);
        if (d2 == 2) {
            this.bU.a(bitmap, d.c(o2), true);
            if (a2 != 0) {
                return true;
            }
            if (!L()) {
                a(true, true, true);
                return true;
            }
            a(false, false, false);
            return true;
        }
        a("360 Player Can't Update Frame " + d2, 4);
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        a("GallerySingle_VideoPlayer_PlayVideo:" + str, 2);
        this.bU.setPlayerMode(1);
        this.bU.setViewMode(this.e.R);
        this.bU.setVideoFilePath(str);
        if (this.e.X != null) {
            this.e.Q = this.e.X.f51a;
            this.e.P = this.e.X.s;
        }
        this.bU.setInteractiveMode(this.e.P);
        this.bU.setViewType(this.e.Q);
        if (this.e.X != null) {
            this.bU.setSphericalParameter(this.e.X);
        }
        int d2 = this.bU.d();
        if (d2 != 1) {
            a("PlayerStatus error:" + d2, 2);
        } else if (this.bU.g() == -1) {
            a("Play fail", 2);
            aa();
        }
        H(false);
        ar();
    }

    /* access modifiers changed from: protected */
    public void Z() {
        if (this.bU.getSphericalParameter().s != 0) {
            this.e.X = new h();
            this.e.X.m = this.bU.getSphericalParameter().m;
            this.e.X.l = this.bU.getSphericalParameter().l;
            this.e.X.f51a = this.bU.getSphericalParameter().f51a;
            this.e.X.q = this.bU.getSphericalParameter().q;
            this.e.X.r = this.bU.getSphericalParameter().r;
            this.e.X.s = this.bU.getSphericalParameter().s;
            this.e.X.e = this.bU.getSphericalParameter().e;
            if (this.e.X.f51a == 0) {
                this.e.X = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void j(int i2) {
        this.bU.setInteractiveMode(i2);
    }

    /* access modifiers changed from: protected */
    public void aa() {
        if (ai()) {
            x(true);
            this.bU.k();
            this.bU.i();
        } else if (this.bU == null) {
            a("Can Not Destroy:TV_ui_singleview_VideoPlayer = null", 0);
        } else {
            a("Can Not Destroy:" + this.bU.d(), 0);
        }
    }

    /* access modifiers changed from: protected */
    public boolean ab() {
        if (this.bU == null) {
            return false;
        }
        return this.bU.d() == 0 || this.bU.d() == 5 || this.bU.d() == 1;
    }

    /* access modifiers changed from: protected */
    public boolean ac() {
        return this.bU.d() == 2 || this.bU.d() == 3;
    }

    /* access modifiers changed from: protected */
    public void ad() {
        this.bU.f();
    }

    /* access modifiers changed from: protected */
    public void ae() {
        a("[Video Player]GallerySingle_VideoPlayer_Enable " + ah(), 4);
        this.bT.setVisibility(0);
        this.bU.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void af() {
        a("[Video Player]GallerySingle_VideoPlayer_Disable", 4);
        this.bT.setVisibility(4);
        this.bU.setVisibility(4);
        R();
    }

    /* access modifiers changed from: protected */
    public void b(int i2, boolean z2) {
        a("GallerySingle_SetDisplayMode " + i2 + " " + ah(), 4);
        if (i2 == 1) {
            this.e.N = 1;
            this.e.P = 3;
        } else {
            this.e.N = 2;
            this.e.P = 1;
        }
        j(this.e.P);
        a(this.e.b.a(this.e.s), false);
        if (z2) {
            I(true);
        } else {
            ar();
        }
    }

    /* access modifiers changed from: protected */
    public h ag() {
        return this.bU.getSphericalParameter();
    }

    /* access modifiers changed from: protected */
    public int ah() {
        if (this.bU != null) {
            return this.bU.d();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public void k(int i2) {
        this.bP.b(i2);
    }

    /* access modifiers changed from: protected */
    public boolean ai() {
        if (this.bU == null) {
            return false;
        }
        if (this.bU.d() == 2 || this.bU.d() == 3 || this.bU.d() == 4 || this.bU.d() == 5) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void aj() {
        this.v = Bitmap.createBitmap(ak());
    }

    /* access modifiers changed from: protected */
    public Bitmap ak() {
        return this.bU.getBitmap();
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        return a(str, this.v);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0021 A[SYNTHETIC, Splitter:B:18:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x002e A[SYNTHETIC, Splitter:B:25:0x002e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r4, android.graphics.Bitmap r5) {
        /*
            r3 = this;
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0019, all -> 0x002a }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0019, all -> 0x002a }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x0039 }
            r2 = 100
            r5.compress(r0, r2, r1)     // Catch:{ Exception -> 0x0039 }
            r0 = 1
            if (r1 == 0) goto L_0x0013
            r1.close()     // Catch:{ IOException -> 0x0014 }
        L_0x0013:
            return r0
        L_0x0014:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0013
        L_0x0019:
            r0 = move-exception
            r1 = r2
        L_0x001b:
            r0.printStackTrace()     // Catch:{ all -> 0x0037 }
            r0 = 0
            if (r1 == 0) goto L_0x0013
            r1.close()     // Catch:{ IOException -> 0x0025 }
            goto L_0x0013
        L_0x0025:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0013
        L_0x002a:
            r0 = move-exception
            r1 = r2
        L_0x002c:
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0032 }
        L_0x0031:
            throw r0
        L_0x0032:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0031
        L_0x0037:
            r0 = move-exception
            goto L_0x002c
        L_0x0039:
            r0 = move-exception
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.a(java.lang.String, android.graphics.Bitmap):boolean");
    }

    /* access modifiers changed from: protected */
    public void al() {
        this.e.V = this.bU.getCurrentPosition();
        a("GallerySingle_saveVideoCurrentPosition: " + this.e.V, 3);
    }

    /* access modifiers changed from: protected */
    public void am() {
        this.e.W = true;
        if (this.bU.getCurrentPosition() >= 0) {
            this.e.V = this.bU.getCurrentPosition();
        }
    }

    /* access modifiers changed from: protected */
    public void an() {
        if (this.d.b.b.a(this.e.s) == 1) {
            int i2 = 0;
            if (this.bU.getCurrentPosition() >= 0) {
                i2 = this.bU.getCurrentPosition();
            }
            g(i2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean ao() {
        return this.bU.isPlaying();
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        ap();
        this.e.q = new GeneralFunction.a(this.c, str, 2000);
    }

    /* access modifiers changed from: protected */
    public void d(String str) {
        ap();
        this.e.q = new GeneralFunction.a((Context) this.c, true, str, 2000);
    }

    /* access modifiers changed from: protected */
    public void ap() {
        if (this.e.q != null) {
            this.e.q.a();
        }
    }

    /* access modifiers changed from: protected */
    public int aq() {
        return this.e.q.b();
    }

    /* access modifiers changed from: protected */
    public void H(boolean z2) {
        int i2 = this.e.Q;
        switch (i2) {
            case 2:
                if (z2) {
                    i2 = 3;
                    break;
                }
                break;
            case 3:
                if (z2) {
                    i2 = 6;
                    break;
                }
                break;
            case 4:
            default:
                i2 = 0;
                break;
            case 5:
                if (z2) {
                    i2 = 2;
                    break;
                }
                break;
            case 6:
                if (z2) {
                    i2 = 5;
                    break;
                }
                break;
        }
        d(i2, z2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void x(int i2) {
        d(i2, true);
    }

    private void d(int i2, boolean z2) {
        new GeneralFunction.c(k.a(this, (float) (this.K / 30)));
        switch (i2) {
            case 2:
                this.cj.setImageResource(R.drawable.gallery_bottom_perspective_white);
                this.cn.setText(R.string.perspective);
                this.e.Q = 2;
                if (z2) {
                    this.cj.setImageResource(R.drawable.gallery_bottom_perspective_green);
                    this.bU.setViewType(2);
                }
                this.bU.b(2, z2);
                return;
            case 3:
                this.cj.setImageResource(R.drawable.gallery_bottom_fisheye_white);
                this.cn.setText(R.string.fisheye);
                this.e.Q = 3;
                if (z2) {
                    this.cj.setImageResource(R.drawable.gallery_bottom_fisheye_green);
                    this.bU.setViewType(3);
                }
                this.bU.b(3, z2);
                return;
            case 4:
            default:
                return;
            case 5:
                this.cj.setImageResource(R.drawable.gallery_bottom_littleplanet_white);
                this.cn.setText(R.string.little_planet);
                this.e.Q = 5;
                if (z2) {
                    this.cj.setImageResource(R.drawable.gallery_bottom_littleplanet_green);
                    this.bU.setViewType(5);
                }
                this.bU.b(5, z2);
                return;
            case 6:
                this.cj.setImageResource(R.drawable.gallery_bottom_crystalball_white);
                this.cn.setText(R.string.crystal_ball);
                this.e.Q = 6;
                if (z2) {
                    this.cj.setImageResource(R.drawable.gallery_bottom_crystalball_green);
                    this.bU.setViewType(6);
                }
                this.bU.b(6, z2);
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void K(boolean z2) {
        switch (this.e.Q) {
            case 2:
                if (z2) {
                    this.cj.setImageResource(R.drawable.gallery_bottom_perspective_green);
                    this.cn.setTextColor(getResources().getColor(R.color.iconcolor));
                    return;
                }
                this.cj.setImageResource(R.drawable.gallery_bottom_perspective_white);
                this.cn.setTextColor(getResources().getColor(R.color.white));
                return;
            case 3:
                if (z2) {
                    this.cj.setImageResource(R.drawable.gallery_bottom_fisheye_green);
                    this.cn.setTextColor(getResources().getColor(R.color.iconcolor));
                    return;
                }
                this.cj.setImageResource(R.drawable.gallery_bottom_fisheye_white);
                this.cn.setTextColor(getResources().getColor(R.color.white));
                return;
            case 4:
            default:
                return;
            case 5:
                if (z2) {
                    this.cj.setImageResource(R.drawable.gallery_bottom_littleplanet_green);
                    this.cn.setTextColor(getResources().getColor(R.color.iconcolor));
                    return;
                }
                this.cj.setImageResource(R.drawable.gallery_bottom_littleplanet_white);
                this.cn.setTextColor(getResources().getColor(R.color.white));
                return;
            case 6:
                if (z2) {
                    this.cj.setImageResource(R.drawable.gallery_bottom_crystalball_green);
                    this.cn.setTextColor(getResources().getColor(R.color.iconcolor));
                    return;
                }
                this.cj.setImageResource(R.drawable.gallery_bottom_crystalball_white);
                this.cn.setTextColor(getResources().getColor(R.color.white));
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void ar() {
        I(false);
    }

    /* access modifiers changed from: protected */
    public void I(boolean z2) {
        switch (this.e.N) {
            case 1:
                this.cq.setText(R.string.gyro);
                if (z2) {
                    this.cm.setImageResource(R.drawable.gallery_bottom_gyro_green);
                } else {
                    this.cm.setImageResource(R.drawable.gallery_bottom_gyro_white);
                }
                this.bU.a(this.e.N, z2);
                break;
            case 2:
                this.cq.setText(R.string.drag);
                if (z2) {
                    this.cm.setImageResource(R.drawable.gallery_bottom_drag_green);
                } else {
                    this.cm.setImageResource(R.drawable.gallery_bottom_drag_white);
                }
                this.bU.a(this.e.N, z2);
                break;
            case 3:
                this.cq.setText(R.string.vr);
                if (z2) {
                    this.cm.setImageResource(R.drawable.gallery_bottom_vr_green);
                } else {
                    this.cm.setImageResource(R.drawable.gallery_bottom_vr_white);
                }
                this.bU.a(this.e.N, z2);
                break;
        }
        if (z2) {
            this.cq.setTextColor(getResources().getColor(R.color.iconcolor));
        } else {
            this.cq.setTextColor(getResources().getColor(R.color.white));
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void L(boolean z2) {
        switch (this.e.N) {
            case 0:
            default:
                return;
            case 1:
                if (z2) {
                    this.cm.setImageResource(R.drawable.gallery_bottom_gyro_green);
                    this.cq.setTextColor(getResources().getColor(R.color.iconcolor));
                    return;
                }
                this.cm.setImageResource(R.drawable.gallery_bottom_gyro_white);
                this.cq.setTextColor(getResources().getColor(R.color.white));
                return;
            case 2:
                if (z2) {
                    this.cm.setImageResource(R.drawable.gallery_bottom_drag_green);
                    this.cq.setTextColor(getResources().getColor(R.color.iconcolor));
                    return;
                }
                this.cm.setImageResource(R.drawable.gallery_bottom_drag_white);
                this.cq.setTextColor(getResources().getColor(R.color.white));
                return;
            case 3:
                if (z2) {
                    this.cm.setImageResource(R.drawable.gallery_bottom_vr_green);
                    this.cq.setTextColor(getResources().getColor(R.color.iconcolor));
                    return;
                }
                this.cm.setImageResource(R.drawable.gallery_bottom_vr_white);
                this.cq.setTextColor(getResources().getColor(R.color.white));
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void as() {
        this.e.N = 2;
        this.e.P = 1;
        this.e.R = 0;
        this.e.Q = 3;
        this.bU.a();
        this.bU.a(this.K, this.L);
        this.e.X = null;
    }

    /* access modifiers changed from: protected */
    public void at() {
        this.cj.setImageResource(R.drawable.gallery_bottom_fisheye_white);
        this.cn.setText(R.string.fisheye);
        this.cm.setImageResource(R.drawable.gallery_bottom_drag_white);
        this.cq.setText(R.string.drag);
        J(false);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.c.v();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        if (!(this.d.b.al || this.aU == null || this.aY == null)) {
            if (configuration.orientation == 2) {
                this.aU.setVisibility(4);
                this.aY.setVisibility(0);
            } else if (configuration.orientation == 1) {
                this.aU.setVisibility(0);
                this.aY.setVisibility(4);
            }
        }
        if (!(this.bU == null || this.e.N == 3)) {
            this.bU.a(displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
        if (this.bP != null && ui_Controller.a.b.b(this.c.a())) {
            if (configuration.orientation == 2) {
                this.bP.a(displayMetrics.widthPixels, displayMetrics.heightPixels);
                this.bP.a(2);
            } else if (configuration.orientation == 1) {
                this.bP.a(1);
            }
            if (o.b() != 3) {
                o.a();
            } else if (o.a(this.H)) {
                a(9145);
            }
            if (!p()) {
                return;
            }
            if (this.e.A) {
                this.bs.a();
            } else {
                bj();
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void bk() {
        switch (this.N) {
            case 0:
                l(0);
                this.N = 0;
                this.d.k = 0;
                return;
            case 1:
                if (this.e.N != 3) {
                    l(1);
                    this.N = 1;
                    this.d.k = 1;
                    return;
                }
                return;
            case 8:
                l(8);
                this.N = 8;
                this.d.k = 8;
                return;
            case 9:
                if (this.e.N != 3) {
                    l(9);
                    this.N = 9;
                    this.d.k = 9;
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void bl() {
        this.bp = new OrientationEventListener(this.H) {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.AnonymousClass36 */

            public void onOrientationChanged(int i) {
                if (i == -1) {
                    if (ui_Controller.a.b.b(UI_PhoneGalleryController.this.c.a())) {
                        UI_PhoneGalleryController.this.bk();
                    }
                } else if (i > 350 || i < 10) {
                    if (UI_PhoneGalleryController.this.N == 1) {
                        return;
                    }
                    if (UI_PhoneGalleryController.this.cA == null || UI_PhoneGalleryController.this.e.N != 3) {
                        UI_PhoneGalleryController.this.l(1);
                        UI_PhoneGalleryController.this.N = 1;
                        UI_PhoneGalleryController.this.d.k = 1;
                    }
                } else if (i <= 80 || i >= 100) {
                    if (i <= 170 || i >= 190) {
                        if (i > 260 && i < 280 && UI_PhoneGalleryController.this.N != 0 && ui_Controller.a.b.b(UI_PhoneGalleryController.this.c.a())) {
                            UI_PhoneGalleryController.this.l(0);
                            UI_PhoneGalleryController.this.N = 0;
                            UI_PhoneGalleryController.this.d.k = 0;
                        }
                    } else if (UI_PhoneGalleryController.this.N == 9) {
                    } else {
                        if (UI_PhoneGalleryController.this.cA == null || UI_PhoneGalleryController.this.e.N != 3) {
                            UI_PhoneGalleryController.this.l(9);
                            UI_PhoneGalleryController.this.N = 9;
                            UI_PhoneGalleryController.this.d.k = 9;
                        }
                    }
                } else if (UI_PhoneGalleryController.this.N != 8 && ui_Controller.a.b.b(UI_PhoneGalleryController.this.c.a())) {
                    UI_PhoneGalleryController.this.l(8);
                    UI_PhoneGalleryController.this.N = 8;
                    UI_PhoneGalleryController.this.d.k = 8;
                }
            }
        };
    }

    public void l(int i2) {
        d.a(this, i2);
    }

    /* access modifiers changed from: protected */
    public void au() {
        if (this.E != null) {
            this.E.release();
            this.E = null;
        }
    }

    /* access modifiers changed from: protected */
    public void av() {
        PowerManager powerManager = (PowerManager) getSystemService("power");
        if (this.E != null) {
            au();
        }
        this.E = powerManager.newWakeLock(536870922, "Lexiconda");
        this.E.acquire();
    }

    /* access modifiers changed from: protected */
    public boolean aw() {
        int a2 = ui_Controller.ui_Gallery.a.a(this.e.b, this.e.s);
        if (a2 == -1 || this.e.O == 3) {
            return false;
        }
        boolean i2 = this.e.b.i(a2);
        boolean j2 = this.e.b.j(a2);
        if (!i2 || !j2) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean ax() {
        int b2 = ui_Controller.ui_Gallery.a.b(this.e.b, this.e.s);
        if (b2 == -1 || this.e.O == 3) {
            return false;
        }
        boolean i2 = this.e.b.i(b2);
        boolean j2 = this.e.b.j(b2);
        if (!i2 || !j2) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void ay() {
        a("deleteErrorFile", 3);
        i(true);
        c(getResources().getString(R.string.file_not_exist));
        this.d.e.a(this.e.b.n(this.e.s), String.valueOf(this.e.b.e(this.e.s)));
        aA();
        i();
    }

    /* access modifiers changed from: protected */
    public void az() {
        GeneralFunction.m.a.a(GeneralFunction.n.a.a(this.c).getInt("storeLocation", 0));
        switch (GeneralFunction.m.a.a()) {
            case 0:
                this.e.d = 1;
                break;
            case 1:
                this.e.d = 2;
                break;
        }
        aA();
    }

    /* access modifiers changed from: protected */
    public void aA() {
        this.d.e.a(this.d.c, this.d.f, this.d.g, this.d.h);
        switch (this.d.b.d) {
            case 1:
                this.e.f1320a.a(this.d.e.a(), this.d.f);
                this.e.b.a(this.d.e.a(), this.d.f);
                return;
            case 2:
                this.e.f1320a.a(this.d.e.a(), this.d.g);
                this.e.b.a(this.d.e.a(), this.d.g);
                return;
            case 3:
                this.e.f1320a.a(this.d.e.a(), this.d.h);
                this.e.b.a(this.d.e.a(), this.d.h);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void aB() {
        if (ui_Controller.a.b.b(this.c.a())) {
            this.w = true;
        }
        if (aF() != null) {
            this.e.ab = aF();
            if (!bs()) {
                bt();
                bu();
            } else if (!br()) {
                bu();
            }
            a(9239);
        }
    }

    /* access modifiers changed from: protected */
    public void aC() {
        if (GeneralFunction.m.a.t() && GeneralFunction.m.a.u()) {
            aD();
        }
        GeneralFunction.m.a.v();
    }

    /* access modifiers changed from: protected */
    public void aD() {
        if (this.e.f == 6) {
            this.e.y = true;
            this.o = false;
            return;
        }
        if (this.e.f == 1) {
            this.c.d();
        }
        GeneralFunction.m.a.a(0);
        GeneralFunction.n.a.b(this.c).putInt("storeLocation", GeneralFunction.m.a.a()).apply();
        if (a() == 0) {
            d(getResources().getString(R.string.sdcard_removed));
        }
        o.b(this);
        c(false);
        b(false);
        if (this.e.d == 2) {
            e(false);
            if (ui_Controller.a.b.b(this.c.a())) {
                i(true);
                a(8966, 0L);
                this.x = true;
            }
        }
        this.e.x = true;
    }

    /* access modifiers changed from: protected */
    public void a(String str, boolean z2) {
        int c2 = this.d.e.c();
        this.W = new ArrayList<>();
        this.X = new ArrayList<>();
        for (int i2 = 0; i2 <= c2 / G; i2++) {
            this.W.addAll(this.d.e.a("FileName", G * i2, ((i2 + 1) * G) - 1));
            this.X.addAll(this.d.e.a("FileSize", G * i2, ((i2 + 1) * G) - 1));
        }
        if (!bn()) {
            a("no Phone Dcim Folder", 3);
        } else if (!bp()) {
            a("no Phone Edit Folder", 3);
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            File[] listFiles = new File(str).listFiles();
            if (listFiles != null) {
                a("scan scanForEditFiles:" + str + ", " + listFiles.length, 3);
                int length = listFiles.length;
                for (int i3 = 0; i3 < length; i3++) {
                    File file = listFiles[i3];
                    if (this.J) {
                        a("scan File:" + file.getAbsolutePath(), 3);
                    }
                    if (!a(file) && z2) {
                        if (this.J) {
                            a("scan File not added:" + file.getAbsolutePath(), 3);
                        }
                        if (f(file.getName()) && file.getName().toLowerCase().endsWith("tmp")) {
                            file.delete();
                        } else if (!h(file.getName()) || file.length() <= 0) {
                            if (f(file.getName()) && file.length() > 0) {
                                if (file.getName().toLowerCase().endsWith("jpg")) {
                                    int i4 = 0;
                                    int i5 = 1;
                                    try {
                                        ExifInterface exifInterface = new ExifInterface(file.getAbsolutePath());
                                        i4 = exifInterface.getAttributeInt("ImageWidth", 0);
                                        i5 = exifInterface.getAttributeInt("ImageLength", 0);
                                    } catch (IOException e2) {
                                        a("scanForEditFiles Exception:" + e2, 3);
                                    }
                                    long lastModified = file.lastModified();
                                    long currentTimeMillis = System.currentTimeMillis();
                                    c2++;
                                    try {
                                        Date parse = simpleDateFormat.parse(file.getName().substring(4));
                                        lastModified = parse.getTime();
                                        currentTimeMillis = parse.getTime();
                                    } catch (Exception e3) {
                                        e3.printStackTrace();
                                    }
                                    if (this.J) {
                                        a("scan File add:" + file.getAbsolutePath(), 3);
                                    }
                                    this.d.e.a(file.getName(), file.getParent() + "/", file.length(), 0, 1, i4, i5, 0, lastModified, currentTimeMillis, c2, 0, true, true, true, true, false, false, false, false, true);
                                } else if (file.getName().toLowerCase().endsWith("gif")) {
                                    int i6 = -1;
                                    int i7 = -1;
                                    long lastModified2 = file.lastModified();
                                    long currentTimeMillis2 = System.currentTimeMillis();
                                    c2++;
                                    try {
                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                        options.inJustDecodeBounds = true;
                                        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                                        i6 = options.outWidth;
                                        i7 = options.outHeight;
                                        Date parse2 = simpleDateFormat.parse(file.getName().substring(4));
                                        lastModified2 = parse2.getTime();
                                        currentTimeMillis2 = parse2.getTime();
                                    } catch (ParseException e4) {
                                        e4.printStackTrace();
                                    }
                                    if (this.J) {
                                        a("scan File add:" + file.getAbsolutePath(), 3);
                                    }
                                    this.d.e.a(file.getName(), file.getParent() + "/", file.length(), 6, 1, i6, i7, 0, lastModified2, currentTimeMillis2, c2, 0, true, true, true, true, false, false, false, false, true);
                                }
                            }
                        } else if (file.getName().toLowerCase().endsWith("jpg")) {
                            int i8 = 0;
                            int i9 = 1;
                            try {
                                ExifInterface exifInterface2 = new ExifInterface(file.getAbsolutePath());
                                i8 = exifInterface2.getAttributeInt("ImageWidth", 0);
                                i9 = exifInterface2.getAttributeInt("ImageLength", 0);
                            } catch (IOException e5) {
                                a("scanForEditFiles Exception:" + e5, 3);
                            }
                            c2++;
                            if (this.J) {
                                a("scan My Album Sample File add:" + file.getAbsolutePath(), 3);
                            }
                            this.d.e.a(file.getName(), file.getParent() + "/", file.length(), 0, 1, i8, i9, 0, 0, 0, c2, 0, true, true, true, true, false, false, false, false, true);
                        }
                    }
                }
                a("scan scanForEditFiles Done", 3);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x01a0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01db  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(java.lang.String r38, boolean r39, boolean r40) {
        /*
        // Method dump skipped, instructions count: 1523
        */
        throw new UnsupportedOperationException("Method not decompiled: ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController.a(java.lang.String, boolean, boolean):int");
    }

    private boolean f(String str) {
        if (str.matches("PIC_[0-9]{8}_[0-9]{6}.[a-zA-z0-9]{3}")) {
            return true;
        }
        return false;
    }

    private boolean g(String str) {
        if (str.equals("CV60_Photo_1.jpg") || str.equals("CV60_Photo_2.jpg") || str.equals("CV60_Photo_3.jpg") || str.equals("CV60_Video.mp4")) {
            return true;
        }
        return false;
    }

    private boolean h(String str) {
        if (str.equals("CV60_Edit_1.jpg") || str.equals("CV60_Edit_2.jpg") || str.equals("CV60_Edit_3.jpg")) {
            return true;
        }
        return false;
    }

    private boolean i(String str) {
        String str2;
        int i2;
        try {
            str2 = new ExifInterface(str).getAttribute("Model");
        } catch (IOException e2) {
            e2.printStackTrace();
            str2 = null;
        }
        if (str2 == null) {
            i2 = -3;
        } else if (str2.toLowerCase().equals("cv60")) {
            i2 = 0;
        } else {
            i2 = -2;
        }
        if (i2 == 0) {
            return true;
        }
        a("checkIfOur360Photo :" + str + ", " + i2, 2);
        return false;
    }

    private boolean j(String str) {
        GeneralFunction.s.c a2 = new GeneralFunction.s.a().a(str);
        if (a2.f152a == 0) {
            return a2.b.matches("[0-9].[0-9]{2}.[0-9]{2}");
        }
        a("sVersionData.lResult :" + str + ", " + a2.f152a, 0);
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(File file) {
        for (int i2 = 0; i2 < this.W.size(); i2++) {
            if (this.J) {
                a("dbNameList:" + this.W.get(i2), 3);
                a("file:" + file.getName(), 3);
            }
            if (file.getName().equals(this.W.get(i2))) {
                String valueOf = String.valueOf(file.length());
                if (this.J) {
                    a("dbSizeList:" + this.X.get(i2), 3);
                    a("file size:" + file.length(), 3);
                }
                if (valueOf.equals(this.X.get(i2))) {
                    this.W.remove(i2);
                    this.X.remove(i2);
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void aE() {
        a("deleteNotExistFile:" + this.W.size(), 3);
        for (int i2 = 0; i2 < this.W.size(); i2++) {
            a("delete:" + this.W.get(i2) + " " + i2, 3);
            this.d.e.a(this.W.get(i2), this.X.get(i2));
        }
    }

    /* access modifiers changed from: protected */
    public String aF() {
        return d.a(this.H);
    }

    /* access modifiers changed from: protected */
    public void aG() {
        Intent intent = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
        intent.addFlags(2);
        startActivityForResult(intent, 42);
    }

    private boolean bm() {
        if (!new File(ui_Controller.a.c.f1314a + "/Android/data/com.huawei.cvIntl60/VideoThumbnail/").exists()) {
            return false;
        }
        return true;
    }

    private boolean bn() {
        if (!new File(ui_Controller.a.c.f1314a + "/", "DCIM").exists()) {
            return false;
        }
        return true;
    }

    private boolean bo() {
        if (!new File(ui_Controller.a.c.c).exists()) {
            return false;
        }
        return true;
    }

    private boolean bp() {
        if (!new File(ui_Controller.a.c.d).exists()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void aH() {
        if (this.e.ac == null) {
            if (aF() != null) {
                this.e.ab = aF();
                bt();
                bu();
                a(9239);
                return;
            }
            c(false);
            b(false);
        } else if (aF() != null) {
            this.e.ab = aF();
            this.e.ac = this.e.ab.substring(this.e.ab.lastIndexOf("/") + 1, this.e.ab.lastIndexOf("/") + 10);
            if (!bs()) {
                bt();
                bu();
            } else if (!br()) {
                bu();
            }
            a(9239);
        }
    }

    /* access modifiers changed from: protected */
    public void aI() {
        if (!bs()) {
            bt();
            bu();
        } else if (!br()) {
            bu();
        }
    }

    private boolean bq() {
        if (!new File(this.e.ab + "/Android/data/com.huawei.cvIntl60/VideoThumbnail/").exists()) {
            return false;
        }
        return true;
    }

    private boolean br() {
        a("sLocalGallery.szSdCardPath" + this.e.ab, 3);
        if (new File(this.e.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/").exists()) {
            return true;
        }
        a("SD CV60 not exists", 3);
        return false;
    }

    private boolean bs() {
        a("sLocalGallery.szSdCardPath" + this.e.ab, 3);
        if (new File(this.e.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM").exists()) {
            return true;
        }
        a("SD DCIM not exists", 3);
        return false;
    }

    private void k(String str) {
        a("CreateSDcardFolder" + str + ",Status:" + new File(str).mkdirs(), 2);
    }

    private void bt() {
        k(this.e.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM");
    }

    private void bu() {
        k(this.e.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/");
    }

    /* access modifiers changed from: protected */
    public void aJ() {
        this.e.j.clear();
        for (int i2 = 0; i2 < this.e.m.getCount(); i2++) {
            if (this.e.m.f(i2)) {
                this.e.j.add(Integer.valueOf(i2));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void aK() {
        this.e.l.clear();
        for (int i2 = 0; i2 < this.e.m.getCount(); i2++) {
            if (this.e.m.f(i2)) {
                this.e.l.add(Integer.valueOf(i2));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void m(int i2) {
        this.e.l.clear();
        this.e.l.add(Integer.valueOf(i2));
    }

    /* access modifiers changed from: protected */
    public void n(int i2) {
        String str = this.e.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/" + this.d.b.b.n(i2);
        a("deleteFile:" + str, 2);
        File file = new File(str);
        file.delete();
        GeneralFunction.g.a.a(this.H, file.getAbsolutePath());
    }

    /* access modifiers changed from: protected */
    public boolean o(int i2) {
        String str = this.e.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/" + this.e.b.n(i2);
        try {
            GeneralFunction.g.a.c(this.e.b.o(i2), str);
            GeneralFunction.g.a.a(this.I, str);
            return this.d.b.b.a(i2) != 1 || p(i2);
        } catch (Exception e2) {
            a("moveFileToSD fail!", 3);
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean p(int i2) {
        String m2 = this.e.b.m(i2);
        String str = this.e.ab + "/Android/data/com.huawei.cvIntl60/VideoThumbnail/" + GeneralFunction.g.a.d(this.e.b.n(i2));
        if (!bq()) {
            new File(this.e.ab + "/Android/data/com.huawei.cvIntl60/VideoThumbnail/").mkdir();
        }
        try {
            GeneralFunction.g.a.c(m2, str);
            GeneralFunction.g.a.a(m2, this.H);
            return true;
        } catch (Exception e2) {
            a("moveThumbnailToSD fail!", 3);
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean q(int i2) {
        String str = ui_Controller.a.c.c + this.e.b.n(i2);
        try {
            GeneralFunction.g.a.c(this.e.b.o(i2), str);
            GeneralFunction.g.a.a(this.I, str);
            return this.d.b.b.a(i2) != 1 || r(i2);
        } catch (Exception e2) {
            a("moveSDFileToPhone fail!", 3);
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean r(int i2) {
        String m2 = this.e.b.m(i2);
        String str = ui_Controller.a.c.f1314a + "/Android/data/com.huawei.cvIntl60/VideoThumbnail/" + GeneralFunction.g.a.d(this.e.b.n(i2));
        if (!bm()) {
            new File(ui_Controller.a.c.f1314a + "/Android/data/com.huawei.cvIntl60/VideoThumbnail/").mkdir();
        }
        try {
            GeneralFunction.g.a.c(m2, str);
            GeneralFunction.g.a.a(m2, this.H);
            return true;
        } catch (Exception e2) {
            a("moveSDThumbnailToPhone fail!", 3);
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void aL() {
        boolean z2;
        boolean z3;
        boolean z4;
        a("Move SDCard File To Phone", 4);
        this.e.y = false;
        if (bn()) {
            if (!bo()) {
                new File(ui_Controller.a.c.c).mkdir();
            }
            if (this.e.l.size() != 0) {
                z3 = false;
                z2 = true;
                for (int i2 = 0; i2 < this.e.l.size(); i2++) {
                    a("Move:" + this.e.b.n(this.e.l.get(i2).intValue()), 3);
                    if (this.e.y) {
                        a("Move canceled", 3);
                        if (z3) {
                            this.d.e.a(this.d.c, this.d.f, this.d.g, this.d.h);
                        }
                        if (this.c.a() == 1040) {
                            b(20490);
                            return;
                        } else {
                            b(20496);
                            return;
                        }
                    } else {
                        long e2 = this.e.b.e(this.e.l.get(i2).intValue());
                        a("FileSize: " + e2, 4);
                        if (this.d.b.b.a(this.e.l.get(i2).intValue()) == 1) {
                            e2 += new File(this.e.b.m(this.e.l.get(i2).intValue())).length();
                            a("FileSize + thm: " + e2, 4);
                        }
                        long a2 = d.a(ui_Controller.a.c.f1314a, -1);
                        if (a2 == -1) {
                            a("dlAvailableSize error", 3);
                            this.e.y = true;
                        } else if (a2 < e2 || a2 < 52428800) {
                            a("Not enough storage space on phone memory: " + a2, 3);
                            z2 = false;
                        } else {
                            a("Moving file", 4);
                            if (this.e.s >= this.e.b.e() - 1) {
                                e eVar = this.e;
                                eVar.s--;
                                z4 = true;
                            } else {
                                z4 = false;
                            }
                            if (q(this.e.l.get(i2).intValue())) {
                                a("Move file success:" + this.e.b.n(this.e.l.get(i2).intValue()), 3);
                                n(this.e.l.get(i2).intValue());
                                a("Delete file", 4);
                                this.d.e.a(this.e.b.s(this.e.l.get(i2).intValue()), this.e.b.t(this.e.l.get(i2).intValue()), false, ui_Controller.a.c.c);
                                a("DB added", 4);
                                z3 = true;
                            } else {
                                a("Move file fail:" + this.e.b.n(this.e.l.get(i2).intValue()), 3);
                                if (z4) {
                                    this.e.s++;
                                }
                            }
                        }
                    }
                }
            } else {
                z3 = false;
                z2 = true;
            }
            if (z3) {
                this.d.e.a(this.d.c, this.d.f, this.d.g, this.d.h);
            }
            if (z2) {
                a("All move done, success:" + z2, 4);
                if (this.c.a() == 1040) {
                    b(20490);
                } else {
                    b(20496);
                }
            } else {
                a("All move done, success:" + z2, 4);
                if (this.c.a() == 1040) {
                    b(20491);
                } else {
                    b(20497);
                }
            }
        } else {
            new File(ui_Controller.a.c.f1314a + "/DCIM/").mkdir();
        }
    }

    /* access modifiers changed from: protected */
    public void aM() {
        a("checkPermissionAndFolders", 3);
        if (this.e.ab == null) {
            this.e.ab = aF();
        }
        if (this.e.ab != null) {
            if (bs()) {
                bt();
                bu();
            } else if (!br()) {
                bu();
            }
            SharedPreferences.Editor b2 = GeneralFunction.n.a.b(this.c);
            b2.putString("objTreeUri", this.e.ab).apply();
            String str = this.e.ab;
            this.e.ac = str.substring(str.lastIndexOf("/") + 1, str.lastIndexOf("/") + 10);
            b2.putString("szSDCardId", this.e.ac).apply();
        }
        a(9239);
    }

    /* access modifiers changed from: protected */
    public void aN() {
        boolean z2;
        boolean z3;
        boolean z4;
        a("Move Phone File To SDCard", 4);
        this.e.y = false;
        if (!bs()) {
            bt();
            bu();
        } else if (!br()) {
            bu();
        }
        if (this.e.l.size() != 0) {
            z3 = false;
            z2 = true;
            for (int i2 = 0; i2 < this.e.l.size(); i2++) {
                a("Move:" + this.e.b.n(this.e.l.get(i2).intValue()), 3);
                if (this.e.y) {
                    a("Move canceled.", 3);
                    if (z3) {
                        this.d.e.a(this.d.c, this.d.f, this.d.g, this.d.h);
                    }
                    if (this.c.a() == 1040) {
                        b(20487);
                        return;
                    } else {
                        b(20493);
                        return;
                    }
                } else {
                    long e2 = this.e.b.e(this.e.l.get(i2).intValue());
                    a("FileSize: " + e2, 4);
                    if (this.d.b.b.a(this.e.l.get(i2).intValue()) == 1) {
                        e2 += new File(this.e.b.m(this.e.l.get(i2).intValue())).length();
                        a("FileSize + thm: " + e2, 4);
                    }
                    long a2 = d.a(this.e.ab, -1);
                    if (a2 == -1) {
                        a("dlAvailableSize error", 3);
                        this.e.y = true;
                    } else if (a2 < e2 || a2 < 52428800) {
                        a("Not enough storage space on SDCard memory: " + a2, 3);
                        z2 = false;
                    } else {
                        a("Moving file", 4);
                        if (this.e.s >= this.e.b.e() - 1) {
                            e eVar = this.e;
                            eVar.s--;
                            z4 = true;
                        } else {
                            z4 = false;
                        }
                        if (o(this.e.l.get(i2).intValue())) {
                            a("Move file success:" + this.e.b.n(this.e.l.get(i2).intValue()), 3);
                            GeneralFunction.g.a.a(this.e.b.o(this.e.l.get(i2).intValue()), this.H);
                            a("Delete file", 4);
                            this.d.e.a(this.e.b.s(this.e.l.get(i2).intValue()), this.e.b.t(this.e.l.get(i2).intValue()), true, this.d.b.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/");
                            a("DB added", 4);
                            z3 = true;
                        } else {
                            a("Move file fail:" + this.e.b.n(this.e.l.get(i2).intValue()), 3);
                            if (z4) {
                                this.e.s++;
                            }
                        }
                    }
                }
            }
        } else {
            z3 = false;
            z2 = true;
        }
        if (z3) {
            this.d.e.a(this.d.c, this.d.f, this.d.g, this.d.h);
        }
        if (z2) {
            a("All move done, success:" + z2, 4);
            if (this.c.a() == 1040) {
                b(20487);
            } else {
                b(20493);
            }
        } else {
            a("All move done, success:" + z2, 4);
            if (this.c.a() == 1040) {
                b(20488);
            } else {
                b(20494);
            }
        }
    }

    /* access modifiers changed from: protected */
    public List<Integer> aO() {
        ArrayList arrayList = new ArrayList();
        for (ApplicationInfo applicationInfo : getPackageManager().getInstalledApplications(0)) {
            if (applicationInfo.packageName.equals("com.google.android.youtube")) {
                arrayList.add(0);
            } else if (applicationInfo.packageName.equals("com.facebook.katana")) {
                arrayList.add(1);
            } else if (!applicationInfo.packageName.equals("com.sina.weibo") && !applicationInfo.packageName.equals("com.tencent.mm") && !applicationInfo.packageName.equals("com.tencent.mobileqq")) {
                if (applicationInfo.packageName.equals("com.twitter.android")) {
                    arrayList.add(9);
                } else if (!applicationInfo.packageName.equals("com.qzone")) {
                    if (applicationInfo.packageName.equals("jp.naver.line.android")) {
                        arrayList.add(7);
                    } else if (applicationInfo.packageName.equals("com.whatsapp")) {
                        arrayList.add(8);
                    } else if (applicationInfo.packageName.equals("com.google.android.apps.photos")) {
                        arrayList.add(10);
                    } else if (!applicationInfo.packageName.equals("com.youku.phone")) {
                        if (applicationInfo.packageName.equals("com.vkontakte.android")) {
                            arrayList.add(12);
                        } else if (applicationInfo.packageName.equals("com.snapchat.android")) {
                            arrayList.add(13);
                        } else if (applicationInfo.packageName.equals("com.instagram.android")) {
                            arrayList.add(14);
                        }
                    }
                }
            }
        }
        if (this.e.d != 3 && arrayList.contains(5) && !arrayList.contains(4)) {
            arrayList.remove((Object) 5);
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    private Cursor l(String str) {
        String[] strArr = {"_id"};
        Uri contentUri = MediaStore.Files.getContentUri("external");
        a("CURSOR TEST changeCursor: " + contentUri.toString(), 3);
        return getContentResolver().query(contentUri, strArr, "_data LIKE ?", new String[]{str}, null);
    }

    private Uri m(String str) {
        Uri contentUri;
        Cursor query;
        long j2;
        a("CURSOR TEST getUriFromPath: " + str, 3);
        String[] strArr = {"_id"};
        String c2 = GeneralFunction.g.a.c(str);
        if (c2 == null) {
            contentUri = MediaStore.Video.Media.getContentUri("external");
            query = getContentResolver().query(contentUri, strArr, "_data LIKE ?", new String[]{str}, null);
        } else if (c2.equals("jpg") || c2.equals("gif")) {
            contentUri = MediaStore.Images.Media.getContentUri("external");
            query = getContentResolver().query(contentUri, strArr, "_data LIKE ?", new String[]{str}, null);
        } else {
            contentUri = MediaStore.Video.Media.getContentUri("external");
            query = getContentResolver().query(contentUri, strArr, "_data LIKE ?", new String[]{str}, null);
        }
        if (query.getCount() == 0) {
            contentUri = MediaStore.Files.getContentUri("external");
            query.close();
            query = l(str);
        }
        query.moveToFirst();
        try {
            j2 = query.getLong(query.getColumnIndex(strArr[0]));
        } catch (CursorIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            j2 = -1;
        }
        query.close();
        a("CURSOR TEST getUriFromPath: " + contentUri.toString() + "/" + j2, 3);
        return Uri.parse(contentUri.toString() + "/" + j2);
    }

    /* access modifiers changed from: protected */
    public void c(int i2, ArrayList<File> arrayList) {
        int i3 = Build.VERSION.SDK_INT;
        GeneralFunction.l.b bVar = new GeneralFunction.l.b();
        bVar.m = new ArrayList<>();
        bVar.n = new ArrayList<>();
        bVar.m.clear();
        bVar.n.clear();
        bVar.k = new ArrayList<>();
        bVar.l = new ArrayList<>();
        bVar.k.clear();
        bVar.l.clear();
        if (t(i2)) {
        }
        bVar.c = 12288;
        if (i2 == 0) {
            for (int i4 = 0; i4 < arrayList.size(); i4++) {
                if (this.e.b.k(this.e.j.get(i4).intValue())) {
                    Toast.makeText(this, getResources().getString(R.string.youtube_does_not_support_photo), 0).show();
                    this.e.j.clear();
                    return;
                }
            }
        }
        if (arrayList.size() > 1) {
            bVar.b = 4101;
            for (int i5 = 0; i5 < arrayList.size(); i5++) {
                bVar.m.add(arrayList.get(i5).getPath());
                bVar.n.add(m(arrayList.get(i5).getPath()));
            }
        } else if (this.e.b.l(this.e.j.get(0).intValue())) {
            bVar.b = InputDeviceCompat.SOURCE_TOUCHSCREEN;
            bVar.i = arrayList.get(0).getPath();
            bVar.l.add(m(arrayList.get(0).getPath()));
        } else {
            bVar.b = FragmentTransaction.TRANSIT_FRAGMENT_OPEN;
            bVar.h = arrayList.get(0).getPath();
            if (i3 >= 24) {
                bVar.n.add(m(arrayList.get(0).getPath()));
            } else {
                bVar.n.add(Uri.fromFile(arrayList.get(0)));
            }
        }
        switch (i2) {
            case 0:
                bVar.f121a = 8193;
                break;
            case 1:
                bVar.f121a = 8192;
                break;
            case 2:
                bVar.f121a = 8201;
                break;
            case 3:
                bVar.f121a = 8197;
                break;
            case 4:
                bVar.f121a = 8199;
                break;
            case 5:
                bVar.f121a = 8200;
                break;
            case 6:
                bVar.f121a = 8198;
                break;
            case 7:
                a(8969);
                return;
            case 8:
                bVar.f121a = 8202;
                break;
            case 9:
                bVar.f121a = 8195;
                break;
            case 10:
                bVar.f121a = 8206;
                break;
            case 12:
                bVar.f121a = 8204;
                break;
            case 13:
                bVar.f121a = 8205;
                break;
            case 14:
                bVar.f121a = 8196;
                break;
        }
        this.bF.a(bVar, this.I);
    }

    /* access modifiers changed from: protected */
    public void d(int i2, ArrayList<File> arrayList) {
        int i3 = Build.VERSION.SDK_INT;
        GeneralFunction.l.b bVar = new GeneralFunction.l.b();
        bVar.m = new ArrayList<>();
        bVar.n = new ArrayList<>();
        bVar.m.clear();
        bVar.n.clear();
        bVar.c = 12288;
        if (arrayList.size() > 1) {
            bVar.b = 4101;
            for (int i4 = 0; i4 < arrayList.size(); i4++) {
                bVar.m.add(arrayList.get(i4).getPath());
                bVar.n.add(m(arrayList.get(i4).getPath()));
            }
        } else {
            bVar.b = FragmentTransaction.TRANSIT_FRAGMENT_OPEN;
            bVar.h = arrayList.get(0).getPath();
            if (i3 >= 24) {
                bVar.n.add(m(arrayList.get(0).getPath()));
            } else {
                bVar.n.add(Uri.fromFile(arrayList.get(0)));
            }
        }
        switch (i2) {
            case 1:
                bVar.f121a = 8192;
                break;
            case 2:
                bVar.f121a = 8201;
                break;
            case 3:
                bVar.f121a = 8197;
                break;
            case 4:
                bVar.f121a = 8199;
                break;
            case 5:
                bVar.f121a = 8200;
                break;
            case 6:
                bVar.f121a = 8198;
                break;
            case 7:
                bVar.f121a = 8194;
                break;
            case 8:
                bVar.f121a = 8202;
                break;
            case 9:
                bVar.f121a = 8195;
                break;
            case 10:
                bVar.f121a = 8206;
                break;
            case 11:
                bVar.f121a = 8203;
                break;
            case 12:
                bVar.f121a = 8204;
                break;
            case 13:
                bVar.f121a = 8205;
                break;
            case 14:
                bVar.f121a = 8196;
                break;
        }
        this.bF.a(bVar, this.I);
    }

    /* access modifiers changed from: protected */
    public void e(String str) {
        ArrayList arrayList = new ArrayList();
        File file = new File(str);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String format = new DecimalFormat("#.##").format((((double) file.length()) / 1024.0d) / 1024.0d);
        String name = file.getName();
        long lastModified = file.lastModified();
        arrayList.add(getResources().getString(R.string.name) + name.substring(0, name.lastIndexOf(".")));
        arrayList.add(getResources().getString(R.string.time) + simpleDateFormat.format(Long.valueOf(lastModified)));
        arrayList.add(getResources().getString(R.string.wide) + options.outWidth);
        arrayList.add(getResources().getString(R.string.height) + options.outHeight);
        arrayList.add(getResources().getString(R.string.size) + format + "MB");
        arrayList.add(getResources().getString(R.string.path) + file.getAbsolutePath());
        o.a(this.H, true, true, getResources().getString(R.string.file_information), (ArrayList<String>) arrayList, getResources().getString(R.string.dialog_option_ok), 0);
    }

    /* access modifiers changed from: protected */
    public void c(int i2, boolean z2) {
        IOException e2;
        String str;
        String str2;
        String str3 = null;
        ArrayList arrayList = new ArrayList();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String format = new DecimalFormat("#.##").format((((double) this.e.b.e(i2)) / 1024.0d) / 1024.0d);
        String n2 = this.e.b.n(i2);
        long g2 = this.e.b.g(i2);
        arrayList.add(getResources().getString(R.string.name) + " " + n2.substring(0, n2.lastIndexOf(".")));
        arrayList.add(getResources().getString(R.string.time) + " " + simpleDateFormat.format(Long.valueOf(g2)));
        arrayList.add(getResources().getString(R.string.wide) + " " + this.e.b.b(i2));
        arrayList.add(getResources().getString(R.string.height) + " " + this.e.b.c(i2));
        arrayList.add(getResources().getString(R.string.size) + " " + format + "MB");
        arrayList.add(getResources().getString(R.string.path) + " " + this.e.b.o(i2));
        if (z2) {
            try {
                ExifInterface exifInterface = new ExifInterface(this.e.b.o(i2));
                str = exifInterface.getAttribute("ExposureBiasValue");
                try {
                    int parseInt = Integer.parseInt(exifInterface.getAttribute("LightSource"));
                    if (str == null) {
                        str = "0";
                    } else if (str.contains("/")) {
                        String[] split = str.split("/");
                        str = "" + (Double.parseDouble(split[0]) / Double.parseDouble(split[1]));
                    }
                    switch (parseInt) {
                        case 0:
                            str2 = getResources().getString(R.string.white_balance_auto);
                            str3 = exifInterface.getAttribute("ISOSpeedRatings");
                            break;
                        case 1:
                            str2 = getResources().getString(R.string.white_balance_sunny);
                            str3 = exifInterface.getAttribute("ISOSpeedRatings");
                            break;
                        case 3:
                            str2 = getResources().getString(R.string.white_balance_tungsten);
                            str3 = exifInterface.getAttribute("ISOSpeedRatings");
                            break;
                        case 10:
                            str2 = getResources().getString(R.string.white_balance_cloudy);
                            str3 = exifInterface.getAttribute("ISOSpeedRatings");
                            break;
                        case 14:
                            str2 = getResources().getString(R.string.white_balance_fluorescent);
                            str3 = exifInterface.getAttribute("ISOSpeedRatings");
                            break;
                        default:
                            str2 = getResources().getString(R.string.white_balance_auto);
                            try {
                                a("TAG_LIGHT_SOURCE get:" + parseInt, 2);
                                str3 = exifInterface.getAttribute("ISOSpeedRatings");
                            } catch (IOException e3) {
                                e2 = e3;
                                e2.printStackTrace();
                                arrayList.add(getResources().getString(R.string.ev) + " " + str);
                                arrayList.add(getResources().getString(R.string.wb) + " " + str2);
                                arrayList.add(getResources().getString(R.string.iso) + " " + str3);
                                o.a(this.H, true, true, getResources().getString(R.string.file_information), (ArrayList<String>) arrayList, getResources().getString(R.string.dialog_option_ok), 0);
                            }
                    }
                } catch (IOException e4) {
                    e2 = e4;
                    str2 = null;
                    e2.printStackTrace();
                    arrayList.add(getResources().getString(R.string.ev) + " " + str);
                    arrayList.add(getResources().getString(R.string.wb) + " " + str2);
                    arrayList.add(getResources().getString(R.string.iso) + " " + str3);
                    o.a(this.H, true, true, getResources().getString(R.string.file_information), (ArrayList<String>) arrayList, getResources().getString(R.string.dialog_option_ok), 0);
                }
            } catch (IOException e5) {
                e2 = e5;
                str2 = null;
                str = null;
                e2.printStackTrace();
                arrayList.add(getResources().getString(R.string.ev) + " " + str);
                arrayList.add(getResources().getString(R.string.wb) + " " + str2);
                arrayList.add(getResources().getString(R.string.iso) + " " + str3);
                o.a(this.H, true, true, getResources().getString(R.string.file_information), (ArrayList<String>) arrayList, getResources().getString(R.string.dialog_option_ok), 0);
            }
            arrayList.add(getResources().getString(R.string.ev) + " " + str);
            arrayList.add(getResources().getString(R.string.wb) + " " + str2);
            arrayList.add(getResources().getString(R.string.iso) + " " + str3);
        }
        o.a(this.H, true, true, getResources().getString(R.string.file_information), (ArrayList<String>) arrayList, getResources().getString(R.string.dialog_option_ok), 0);
    }

    /* access modifiers changed from: protected */
    public void s(int i2) {
        ArrayList arrayList = new ArrayList();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        long e2 = this.e.b.e(i2);
        String format = decimalFormat.format((((double) e2) / 1024.0d) / 1024.0d);
        String n2 = this.e.b.n(i2);
        int d2 = this.e.b.d(i2);
        String format2 = String.format("%02d:%02d", Integer.valueOf(d2 / 60), Integer.valueOf(d2 % 60));
        long g2 = this.e.b.g(i2);
        arrayList.add(getResources().getString(R.string.name) + " " + n2.substring(0, n2.lastIndexOf(".")));
        arrayList.add(getResources().getString(R.string.time) + " " + simpleDateFormat.format(Long.valueOf(g2)));
        arrayList.add(getResources().getString(R.string.wide) + " " + this.e.b.b(i2));
        arrayList.add(getResources().getString(R.string.height) + " " + this.e.b.c(i2));
        if ((e2 / 1024) / 1024 < 1) {
            arrayList.add(getResources().getString(R.string.size) + " " + decimalFormat.format(((double) e2) / 1024.0d) + "KB");
        } else {
            arrayList.add(getResources().getString(R.string.size) + " " + format + "MB");
        }
        arrayList.add(getResources().getString(R.string.path) + " " + this.e.b.o(i2));
        arrayList.add(getResources().getString(R.string.file_type) + " " + n2.substring(n2.lastIndexOf(".") + 1, n2.length()));
        arrayList.add(getResources().getString(R.string.duration) + " " + format2);
        o.a(this.H, true, true, getResources().getString(R.string.file_information), (ArrayList<String>) arrayList, getResources().getString(R.string.dialog_option_ok), 0);
    }
}
