package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.Player.GifGenerate.GifGenerate;
import GeneralFunction.a.a;
import GeneralFunction.c;
import GeneralFunction.d;
import GeneralFunction.i.a.a;
import GeneralFunction.k;
import GeneralFunction.o;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import java.io.File;
import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IjkMediaCodecInfo;
import ui_Controller.b.b;
import ui_Controller.b.e;
import ui_Controller.ui_Liveview.UI_LiveViewController;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class UI_GifMakingController extends a {
    private FrameLayout A = null;
    private TextView B = null;
    private TextView C = null;
    private TextView D = null;
    private TextView E = null;
    private FrameLayout F = null;
    private FrameLayout G = null;
    private LinearLayout H = null;
    private LinearLayout I = null;
    private Button J = null;
    private FrameLayout K = null;
    private LinearLayout L = null;
    private ImageButton M = null;
    private ImageButton N = null;
    private ImageButton O = null;
    private ImageButton P = null;
    private ImageButton Q = null;
    private ImageView R = null;
    private Space S;
    private LinearLayout T = null;
    private Button U = null;
    private LinearLayout V = null;
    private ImageView W = null;
    private a.AbstractC0002a X = new a.AbstractC0002a() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass6 */

        @Override // GeneralFunction.i.a.a.AbstractC0002a
        public void a(int i, int i2) {
            if (UI_GifMakingController.this.b.a(4194304L)) {
                UI_GifMakingController.this.a((UI_GifMakingController) "ACTION CLICK", (String) 3);
                switch (i) {
                    case 1:
                    case 2:
                        a.c.a aVar = new a.c.a(11017);
                        aVar.a("SelectPointer", i);
                        aVar.a("Index", i2);
                        UI_GifMakingController.this.a(aVar, 0);
                        return;
                    default:
                        UI_GifMakingController.this.b.c(268435455);
                        return;
                }
            }
        }
    };
    private a.b Y = new a.b() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass7 */

        @Override // GeneralFunction.i.a.a.b
        public boolean a(int i, int i2, MotionEvent motionEvent) {
            switch (motionEvent.getAction() & 255) {
                case 0:
                case 5:
                    if (UI_GifMakingController.this.b.a(4194304L)) {
                        UI_GifMakingController.this.a((UI_GifMakingController) "ACTION DOWN", (String) 3);
                        break;
                    } else {
                        return true;
                    }
                case 1:
                case 6:
                    if (UI_GifMakingController.this.b.a(4194304L)) {
                        UI_GifMakingController.this.a((UI_GifMakingController) "ACTION UP", (String) 3);
                        break;
                    } else {
                        return true;
                    }
                case 2:
                    if (UI_GifMakingController.this.b.a(4194304L)) {
                        UI_GifMakingController.this.a((UI_GifMakingController) "ACTION MOVE", (String) 3);
                        switch (i) {
                            case 1:
                            case 2:
                                a.c.a aVar = new a.c.a(11016);
                                aVar.a("SelectPointer", i);
                                aVar.a("Index", i2);
                                UI_GifMakingController.this.a(aVar, 0);
                                break;
                        }
                    } else {
                        return true;
                    }
                case 3:
                case 4:
                default:
                    if (!UI_GifMakingController.this.b.a(4194304L)) {
                        return true;
                    }
                    break;
            }
            return false;
        }
    };
    private View.OnTouchListener Z = new View.OnTouchListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass11 */

        public boolean onTouch(View view, MotionEvent motionEvent) {
            int i = R.drawable.gallery_button_arrowdown_press;
            int i2 = R.drawable.gallery_button_save_normal;
            switch (view.getId()) {
                case R.id.IB_ui_gifmaking_ArrowIcon:
                    i2 = R.drawable.gallery_button_arrowback_normal;
                    i = R.drawable.gallery_button_arrowback_press;
                    break;
                case R.id.FL_ui_gifmaking_DirectionIconLayout:
                case R.id.TV_ui_gifmaking_DirectionIcon:
                case R.id.TV_ui_gifmaking_StampListIcon:
                case R.id.TV_ui_gifmaking_PlayIcon:
                case R.id.TV_ui_gifmaking_SaveIcon:
                case R.id.SP_gifmaking_StampSpace:
                case R.id.IV_ui_gifmaking_StampIcon:
                case R.id.LL_ui_gifmaking_StampSelection:
                case R.id.IB_ui_gifmaking_StampSelection0:
                default:
                    i2 = -1;
                    i = -1;
                    break;
                case R.id.IB_ui_gifmaking_DirectionIcon:
                    i2 = R.drawable.gallery_button_arrowdown_normal;
                    break;
                case R.id.IB_ui_gifmaking_StampListIcon:
                    i2 = R.drawable.gallery_button_arrowdown_normal;
                    break;
                case R.id.IB_ui_gifmaking_PlayIcon:
                    i2 = R.drawable.gallery_button_arrowdown_normal;
                    break;
                case R.id.IB_ui_gifmaking_SaveIcon:
                    i = R.drawable.gallery_button_save_press;
                    break;
                case R.id.IB_ui_gifmaking_StampSelection1:
                    i = R.drawable.gallery_button_save_press;
                    break;
                case R.id.IB_ui_gifmaking_StampSelection2:
                    i = R.drawable.gallery_button_save_press;
                    break;
                case R.id.IB_ui_gifmaking_StampSelection3:
                    i = R.drawable.gallery_button_save_press;
                    break;
            }
            if (motionEvent.getAction() == 0) {
                ((ImageButton) view).setImageResource(i);
            }
            if (motionEvent.getAction() == 2 && (motionEvent.getX() < 0.0f || motionEvent.getX() > ((float) view.getWidth()) || motionEvent.getY() < 0.0f || motionEvent.getY() > ((float) view.getHeight()))) {
                ((ImageButton) view).setImageResource(i2);
            }
            if (motionEvent.getAction() != 1) {
                return false;
            }
            ((ImageButton) view).setImageResource(i2);
            return false;
        }
    };
    private View.OnClickListener aa = new View.OnClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass2 */

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.FL_ui_gifmaking_VideoPlayer:
                    UI_GifMakingController.this.n();
                    return;
                case R.id.IB_ui_gifmaking_ArrowIcon:
                    if (UI_GifMakingController.this.b.a(1L)) {
                        UI_GifMakingController.this.a(32768, 0L);
                        return;
                    }
                    return;
                case R.id.IB_ui_gifmaking_DirectionIcon:
                    if (UI_GifMakingController.this.b.a(PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE)) {
                        UI_GifMakingController.this.a(11008, 0L);
                        return;
                    }
                    return;
                case R.id.IB_ui_gifmaking_StampListIcon:
                    if (UI_GifMakingController.this.b.a(PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE)) {
                        UI_GifMakingController.this.a(11010, 0L);
                        return;
                    }
                    return;
                case R.id.IB_ui_gifmaking_PlayIcon:
                    if (UI_GifMakingController.this.b.a(PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE)) {
                        UI_GifMakingController.this.a(11011, 0L);
                        return;
                    }
                    return;
                case R.id.IB_ui_gifmaking_SaveIcon:
                    if (UI_GifMakingController.this.b.a(8388608L)) {
                        UI_GifMakingController.this.a(11012, 0L);
                        return;
                    }
                    return;
                case R.id.IB_ui_gifmaking_StampSelection0:
                    UI_GifMakingController.this.a(11022, 0L);
                    return;
                case R.id.IB_ui_gifmaking_StampSelection1:
                    UI_GifMakingController.this.a(11013, 0L);
                    return;
                case R.id.IB_ui_gifmaking_StampSelection2:
                    UI_GifMakingController.this.a(11014, 0L);
                    return;
                case R.id.IB_ui_gifmaking_StampSelection3:
                    UI_GifMakingController.this.a(11015, 0L);
                    return;
                case R.id.IB_ui_gifmaking_StampSelection4:
                    UI_GifMakingController.this.a(11023, 0L);
                    return;
                case R.id.B_ui_gifmaking_MemoryFullConfirm:
                    UI_GifMakingController.this.a(11021, 0L);
                    return;
                default:
                    UI_GifMakingController.this.a((UI_GifMakingController) "error press", (String) 3);
                    return;
            }
        }
    };
    protected UI_ModeMain b = null;
    protected b c = null;
    protected e d = null;
    protected int e = 0;
    protected boolean f = false;
    protected boolean g = false;
    protected boolean h = false;
    protected boolean i = false;
    protected boolean j = false;
    protected int k = 0;
    protected GifGenerate l = null;
    protected GeneralFunction.i.a.a m = null;
    protected int n = 0;
    private int o = 0;
    private int p = 0;
    private int q = -1;
    private int r = -1;
    private int s = 0;
    private OrientationEventListener t = null;
    private a u = null;
    private ImageButton v = null;
    private ImageButton w = null;
    private ImageButton x = null;
    private ImageButton y = null;
    private ImageButton z = null;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        d.a("UI_GifMakingController", str, i2);
    }

    @Override // GeneralFunction.a.a
    public void a(int i2, long j2) {
        if (this.b != null) {
            this.b.a(new a.c.a(i2), j2);
        } else {
            a("SendUiMsg error!! Msg=" + i2, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(a.c.a aVar, long j2) {
        if (this.b != null) {
            this.b.a(aVar, j2);
        } else {
            a("SendUiMsg error!! MsgEx=" + aVar.b().what, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(int i2, long j2) {
        if (this.b != null) {
            this.b.b(i2, j2);
        } else {
            a("UI_SendAidMsg error!!", 0);
        }
    }

    @Override // GeneralFunction.a.a
    public boolean b() {
        if (this.q == 2) {
            return true;
        }
        return false;
    }

    public void onBackPressed() {
        if (this.b.a(1L)) {
            a(32768, 0L);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(Message message) {
        a("AidServer_Handler:0x" + Integer.toHexString(message.what), 3);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a("onCreate", 3);
        this.b = (UI_ModeMain) getApplication();
        this.b.v();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.o = displayMetrics.widthPixels;
        this.p = displayMetrics.heightPixels;
        this.c = this.b.c.k;
        this.d = this.b.c.k.b;
        if (this.p > this.o) {
            this.q = 1;
            if (this.d.X.f51a == 6) {
                setContentView(R.layout.ui_gifmaking_port_crystalball);
            } else {
                setContentView(R.layout.ui_gifmaking_port);
            }
        } else {
            this.q = 2;
            if (this.d.X.f51a == 6) {
                setContentView(R.layout.ui_gifmaking_port_crystalball);
            } else {
                setContentView(R.layout.ui_gifmaking_port);
            }
        }
        this.u = new a(this);
        switch (this.d.b.a(this.d.s)) {
            case 0:
                this.g = true;
                this.b.a(3600, this);
                break;
            case 1:
            case 5:
                this.b.a(3601, this);
                this.g = false;
                break;
            case 2:
            case 3:
            case 4:
            default:
                a("Unknow file type: " + this.d.b.a(this.d.s), 3);
                break;
        }
        A();
        if (this.b.g()) {
            a(12033, 100L);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        a("onConfigurationChanged", 3);
        this.b.v();
        if (this.q != configuration.orientation) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            this.o = displayMetrics.widthPixels;
            this.p = displayMetrics.heightPixels;
            if (configuration.orientation == 2) {
                a("ui_phonegallery_land", 3);
                setContentView(R.layout.ui_gifmaking_port);
            } else {
                a("ui_phonegallery_port", 3);
            }
            this.q = configuration.orientation;
            a.c.a aVar = new a.c.a(12038);
            aVar.a("mode", 3584);
            a(aVar, 200);
            o.a();
        }
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onResume() {
        super.onResume();
        this.t.enable();
        if (!r()) {
            this.i = true;
            a(8452, 0L);
        }
        a.c.a aVar = new a.c.a(12034);
        aVar.a("mode", 3584);
        a(aVar, 0);
    }

    @Override // GeneralFunction.a.a
    public void onPause() {
        super.onPause();
        this.t.disable();
        if (this.b.a() == 3632) {
            d();
            c(false);
            this.b.c(268435455);
        }
        a.c.a aVar = new a.c.a(12035);
        aVar.a("mode", 3584);
        a(aVar, 0);
    }

    @Override // GeneralFunction.a.a
    public void onStop() {
        super.onStop();
        a.c.a aVar = new a.c.a(12036);
        aVar.a("mode", 3584);
        a(aVar, 0);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onDestroy() {
        super.onDestroy();
        a.c.a aVar = new a.c.a(12037);
        aVar.a("mode", 3584);
        a(aVar, 0);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.h = true;
        this.d.x = true;
        if (this.d.d != 2) {
            return;
        }
        if (this.b.a() == 3632) {
            a("remove SD card when generating", 2);
            d();
        } else if (this.b.a() == 3616) {
            a("remove SD card when previewing", 2);
            d();
        } else {
            this.j = true;
            a(8452, 100L);
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.l.a();
        if (this.g) {
            this.b.a(3600);
        } else {
            this.b.a(3601);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(Message message) {
        this.u.a(message);
    }

    /* access modifiers changed from: protected */
    public void e() {
        w();
        u();
        x();
        y();
        z();
        c(1);
    }

    private void u() {
        this.F = (FrameLayout) findViewById(R.id.FL_ui_gifmaking_VideoPlayer);
        this.l = (GifGenerate) findViewById(R.id.TV_ui_gifmaking_VideoPlayer);
        this.s = (this.o * 890) / IMediaPlayer.MEDIA_INFO_TIMED_TEXT_ERROR;
        if (this.d.X.f51a == 6) {
            this.l.setLayoutParams(new LinearLayout.LayoutParams(this.s, this.s));
        } else {
            this.l.setLayoutParams(new LinearLayout.LayoutParams(this.s, this.s / 2));
        }
        this.l.setPlayerStatusListener(new GifGenerate.d() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass1 */

            @Override // GeneralFunction.Player.GifGenerate.GifGenerate.d
            public void a(int i) {
                UI_GifMakingController.this.a((UI_GifMakingController) ("ReturnVideoStatus " + i), (String) 3);
                a.c.a aVar = new a.c.a(11020);
                aVar.a("360PlayerStatus", i);
                UI_GifMakingController.this.a(aVar, 0);
            }
        });
        this.l.setGifGenerateStatusListener(new GifGenerate.g() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass4 */

            @Override // GeneralFunction.Player.GifGenerate.GifGenerate.g
            public void a(int i) {
                UI_GifMakingController.this.a((UI_GifMakingController) ("gifGenerateStatusChange: " + i), (String) 3);
                a.c.a aVar = new a.c.a(11019);
                aVar.a("GifGenerateStatus", i);
                UI_GifMakingController.this.a(aVar, 0);
            }
        });
        if (this.b.a() == 3600) {
            g();
        } else if (this.b.a() == 3601) {
            f();
            this.d.F = -1;
            this.d.G = -1;
        }
        this.F.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass5 */

            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                UI_GifMakingController.this.a((UI_GifMakingController) Math.min(i3, i4), Math.max(i3, i4));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void f() {
        this.l.d();
        this.l.setEnable(true);
        this.l.setSphericalParameter(this.d.X);
        if (this.d.X.f51a == 6) {
            this.l.a(450, 450);
        }
        this.l.setPlayerMode(1);
        this.l.setVideoFilePath(this.d.b.o(this.d.s));
        this.l.e();
    }

    /* access modifiers changed from: protected */
    public void g() {
        this.l.d();
        this.l.setEnable(true);
        this.l.setSphericalParameter(this.d.X);
        if (this.d.X.f51a == 6) {
            this.l.a(450, 450);
        }
        this.l.setPlayerMode(3);
        this.l.e();
        this.l.setGifFromImageDirect(1);
    }

    /* access modifiers changed from: protected */
    public void h() {
        String o2 = this.d.b.o(this.d.s);
        int b2 = this.d.b.b(this.d.s);
        int c2 = this.d.b.c(this.d.s);
        int c3 = d.c(o2);
        a("lOrientation: " + c3, 3);
        this.l.a(ui_Controller.CustomWidget.a.b(o2, 18096128, b2, c2), c3, true);
    }

    /* access modifiers changed from: protected */
    public int i() {
        if (this.l != null) {
            return this.l.c();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public boolean j() {
        if (this.l.c() == 5 || this.l.c() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void k() {
        if (this.l != null) {
            this.l.b();
        }
    }

    private void v() {
        int i2;
        int i3 = 10;
        this.e = this.l.getDuration() / 1000;
        if (this.e < 1) {
            this.e = 1;
        }
        if (this.d.V / 1000 >= this.e) {
            this.d.V = (this.e - 1) * 1000;
        }
        if (this.e <= 10) {
            this.d.F = this.d.V / 1000 >= this.e ? this.e - 1 : this.d.V / 1000;
            e eVar = this.d;
            if (this.d.F + 4 >= this.e) {
                i2 = this.e;
            } else {
                i2 = this.d.F + 4;
            }
            eVar.G = i2;
        } else if (this.d.V / 1000 > this.e - 10) {
            this.n = this.e - 10;
            this.d.F = (this.d.V / 1000) - (this.e - 10);
            e eVar2 = this.d;
            if (this.d.F + 4 < 10) {
                i3 = this.d.F + 4;
            }
            eVar2.G = i3;
        } else {
            this.n = this.d.V / 1000;
            this.d.F = 0;
            this.d.G = 4;
        }
    }

    /* access modifiers changed from: protected */
    public void l() {
        if (this.d.F < 0 || this.d.G < 0) {
            v();
        }
        this.K.removeAllViews();
        this.m = new GeneralFunction.i.a.a(this, this.K, true, this.e <= 10 ? this.e + 1 : 11, 1, this.d.F, this.d.G, true);
        this.m.a(this.X);
        this.m.a(this.Y);
        if (this.e <= 10) {
            this.G.setVisibility(4);
            e(this.d.F * 1000);
            return;
        }
        this.G.setVisibility(0);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.I.getLayoutParams());
        a("AAA" + this.I.getWidth(), 3);
        layoutParams.width = (this.o / this.e) * 10;
        this.I.setLayoutParams(layoutParams);
        m();
    }

    /* access modifiers changed from: protected */
    public void m() {
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(this.I.getLayoutParams());
        marginLayoutParams.setMargins((this.n * this.o) / this.e, 0, 0, 0);
        this.I.setLayoutParams(new FrameLayout.LayoutParams(marginLayoutParams));
        e(this.d.F * 1000);
    }

    /* access modifiers changed from: protected */
    public void c(int i2) {
        switch (i2) {
            case 1:
                this.w.setAlpha(1.0f);
                this.B.setAlpha(1.0f);
                this.x.setAlpha(1.0f);
                this.C.setAlpha(1.0f);
                this.x.setEnabled(true);
                this.y.setImageResource(R.drawable.gallery_top_play_black);
                this.D.setText(getResources().getString(R.string.edit_preview));
                this.z.setAlpha(1.0f);
                this.E.setAlpha(1.0f);
                if (!this.g) {
                    this.K.setVisibility(0);
                    if (this.e <= 10) {
                        this.G.setVisibility(4);
                    } else {
                        this.G.setVisibility(0);
                    }
                    this.A.setVisibility(8);
                    return;
                }
                this.K.setVisibility(4);
                this.G.setVisibility(4);
                this.A.setVisibility(0);
                return;
            case 2:
                this.w.setAlpha(0.3f);
                this.B.setAlpha(0.3f);
                this.x.setAlpha(0.3f);
                this.C.setAlpha(0.3f);
                this.x.setEnabled(false);
                this.y.setImageResource(R.drawable.gallery_top_play_stop);
                this.D.setText(getResources().getString(R.string.edit_stop));
                this.z.setAlpha(0.3f);
                this.E.setAlpha(0.3f);
                if (!this.g) {
                    this.K.setVisibility(4);
                    this.G.setVisibility(4);
                    this.A.setVisibility(8);
                    return;
                }
                this.K.setVisibility(4);
                this.G.setVisibility(4);
                this.A.setVisibility(0);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void n() {
        this.L.setVisibility(4);
        this.x.setImageResource(R.drawable.gallery_top_logo_black);
        this.C.setTextColor(getResources().getColor(R.color.white));
        this.F.setOnClickListener(null);
    }

    /* access modifiers changed from: protected */
    public void o() {
        if (this.L.getVisibility() != 0) {
            this.L.setVisibility(0);
            this.x.setImageResource(R.drawable.gallery_top_logo_green);
            this.C.setTextColor(getResources().getColor(R.color.iconcolor));
            this.F.setOnClickListener(this.aa);
            return;
        }
        this.L.setVisibility(4);
        this.x.setImageResource(R.drawable.gallery_top_logo_black);
        this.C.setTextColor(getResources().getColor(R.color.white));
        this.F.setOnClickListener(null);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        if (this.R == null) {
            return;
        }
        if (z2) {
            this.R.setVisibility(0);
        } else {
            this.R.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void d(int i2) {
        this.k = i2;
        this.M.setImageResource(0);
        this.N.setImageResource(0);
        this.O.setImageResource(0);
        this.P.setImageResource(0);
        this.Q.setImageResource(0);
        switch (i2) {
            case 0:
                this.R.setImageResource(0);
                this.M.setImageResource(R.drawable.gallery_logolist_cursor);
                return;
            case 1:
                this.R.setImageResource(R.drawable.gallery_logolist_logo_1_500x70);
                this.N.setImageResource(R.drawable.gallery_logolist_cursor);
                return;
            case 2:
                this.R.setImageResource(R.drawable.gallery_logolist_logo_2_500x70);
                this.O.setImageResource(R.drawable.gallery_logolist_cursor);
                return;
            case 3:
                this.R.setImageResource(R.drawable.gallery_logolist_logo_3_500x70);
                this.P.setImageResource(R.drawable.gallery_logolist_cursor);
                return;
            case 4:
                this.R.setImageResource(R.drawable.gallery_logolist_logo_4_500x70);
                this.Q.setImageResource(R.drawable.gallery_logolist_cursor);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void p() {
        if (this.w.getTag().equals(Integer.valueOf((int) R.drawable.gallery_top_lr_black))) {
            this.w.setTag(Integer.valueOf((int) R.drawable.gallery_top_rl_black));
            this.w.setImageResource(R.drawable.gallery_top_rl_black);
            this.l.setGifFromImageDirect(0);
            return;
        }
        this.w.setTag(Integer.valueOf((int) R.drawable.gallery_top_lr_black));
        this.w.setImageResource(R.drawable.gallery_top_lr_black);
        this.l.setGifFromImageDirect(1);
    }

    /* access modifiers changed from: protected */
    public void e(int i2) {
        a("Position: " + i2 + " lCurrentVideoLengthBarStartPoint: " + this.n, 3);
        if (this.l != null) {
            a("Current Position S: " + this.l.getCurrentPosition(), 3);
            this.l.seekTo((this.n * 1000) + i2);
            a("Current Position E: " + this.l.getCurrentPosition(), 3);
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
        this.l.pause();
    }

    private void w() {
        this.K = (FrameLayout) findViewById(R.id.FL_ui_gifmaking_SelectScrollBar);
        this.G = (FrameLayout) findViewById(R.id.FL_ui_gifmaking_VideoLengthBar);
        this.H = (LinearLayout) findViewById(R.id.LL_ui_gifmaking_UnselectedVideoLengthBar);
        this.H.setBackgroundColor(getResources().getColor(R.color.white));
        this.I = (LinearLayout) findViewById(R.id.LL_ui_gifmaking_SelectedVideoLengthBar);
        this.I.setBackgroundColor(getResources().getColor(R.color.turquoise));
        this.J = (Button) findViewById(R.id.B_ui_gifmaking_VideoLengthBarTouchArea);
        this.J.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass8 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction() & 255) {
                }
                int x = (((int) motionEvent.getX()) * UI_GifMakingController.this.e) / UI_GifMakingController.this.o;
                UI_GifMakingController.this.a((UI_GifMakingController) ("AAA t " + ((int) motionEvent.getX()) + " / " + UI_GifMakingController.this.o), (String) 3);
                if (x > UI_GifMakingController.this.e - 5) {
                    UI_GifMakingController.this.n = UI_GifMakingController.this.e - 10;
                } else if (x < 5) {
                    UI_GifMakingController.this.n = 0;
                } else {
                    UI_GifMakingController.this.n = x - 5;
                }
                UI_GifMakingController.this.m();
                return false;
            }
        });
        this.v = (ImageButton) findViewById(R.id.IB_ui_gifmaking_ArrowIcon);
        this.v.setImageResource(R.drawable.gallery_top_back_black);
        this.v.setVisibility(4);
        this.w = (ImageButton) findViewById(R.id.IB_ui_gifmaking_DirectionIcon);
        this.w.setImageResource(R.drawable.gallery_top_lr_black);
        this.w.setTag(Integer.valueOf((int) R.drawable.gallery_top_lr_black));
        this.x = (ImageButton) findViewById(R.id.IB_ui_gifmaking_StampListIcon);
        this.x.setImageResource(R.drawable.gallery_top_logo_black);
        this.y = (ImageButton) findViewById(R.id.IB_ui_gifmaking_PlayIcon);
        this.y.setImageResource(R.drawable.gallery_top_play_black);
        this.z = (ImageButton) findViewById(R.id.IB_ui_gifmaking_SaveIcon);
        this.z.setImageResource(R.drawable.gallery_top_save_black);
        this.v.setOnClickListener(this.aa);
        this.w.setOnClickListener(this.aa);
        this.x.setOnClickListener(this.aa);
        this.y.setOnClickListener(this.aa);
        this.z.setOnClickListener(this.aa);
        this.A = (FrameLayout) findViewById(R.id.FL_ui_gifmaking_DirectionIconLayout);
        this.B = (TextView) findViewById(R.id.TV_ui_gifmaking_DirectionIcon);
        this.C = (TextView) findViewById(R.id.TV_ui_gifmaking_StampListIcon);
        this.D = (TextView) findViewById(R.id.TV_ui_gifmaking_PlayIcon);
        this.E = (TextView) findViewById(R.id.TV_ui_gifmaking_SaveIcon);
        int a2 = k.a(this, (float) (Math.min(this.o, this.p) / 40));
        this.B.setText(getResources().getString(R.string.edit_direction));
        this.C.setText(getResources().getString(R.string.edit_logo));
        this.D.setText(getResources().getString(R.string.edit_preview));
        this.E.setText(getResources().getString(R.string.edit_save));
        c f2 = new c(a2).d((int) (((double) a2) * 0.9d)).o((int) (((double) a2) * 0.9d)).f((int) (((double) a2) * 0.9d));
        this.B.setTextSize((float) f2.a());
        this.C.setTextSize((float) f2.a());
        this.D.setTextSize((float) f2.a());
        this.E.setTextSize((float) f2.a());
    }

    private void x() {
        this.R = (ImageView) findViewById(R.id.IV_ui_gifmaking_StampIcon);
        this.S = (Space) findViewById(R.id.SP_gifmaking_StampSpace);
        this.R.setImageResource(0);
        a(false);
        int min = Math.min(this.o, this.p);
        int max = Math.max(this.o, this.p);
        if (this.F != null) {
            max = this.F.getHeight();
        }
        a(min, max);
        this.L = (LinearLayout) findViewById(R.id.LL_ui_gifmaking_StampSelection);
        this.L.setBackgroundResource(R.drawable.gallery_logolist);
        this.L.setVisibility(4);
        this.M = (ImageButton) findViewById(R.id.IB_ui_gifmaking_StampSelection0);
        this.M.setBackgroundResource(R.drawable.gallery_logolist_logo_0);
        this.M.setImageResource(R.drawable.gallery_logolist_cursor);
        this.N = (ImageButton) findViewById(R.id.IB_ui_gifmaking_StampSelection1);
        this.N.setBackgroundResource(R.drawable.gallery_logolist_logo_1);
        this.O = (ImageButton) findViewById(R.id.IB_ui_gifmaking_StampSelection2);
        this.O.setBackgroundResource(R.drawable.gallery_logolist_logo_2);
        this.P = (ImageButton) findViewById(R.id.IB_ui_gifmaking_StampSelection3);
        this.P.setBackgroundResource(R.drawable.gallery_logolist_logo_3);
        this.Q = (ImageButton) findViewById(R.id.IB_ui_gifmaking_StampSelection4);
        this.Q.setBackgroundResource(R.drawable.gallery_logolist_logo_4);
        this.M.setOnClickListener(this.aa);
        this.N.setOnClickListener(this.aa);
        this.O.setOnClickListener(this.aa);
        this.P.setOnClickListener(this.aa);
        this.Q.setOnClickListener(this.aa);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(int i2, int i3) {
        this.S.setVisibility(8);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.R.getLayoutParams());
        if (this.d.X.f51a == 6) {
            layoutParams.height = (int) ((((double) i2) * 58.75d) / 900.0d);
            layoutParams.leftMargin = ((i2 * 5) / IMediaPlayer.MEDIA_INFO_TIMED_TEXT_ERROR) + ((((i2 * 890) / IMediaPlayer.MEDIA_INFO_TIMED_TEXT_ERROR) * 10) / IjkMediaCodecInfo.RANK_LAST_CHANCE);
            layoutParams.topMargin = (int) (((((((double) ((float) i3)) * 222.5d) / 1600.0d) + ((double) this.s)) - ((double) layoutParams.height)) - ((double) ((int) (((double) i2) * 0.0757d))));
        } else {
            layoutParams.height = (int) ((((double) i2) * 44.5d) / 900.0d);
            layoutParams.leftMargin = ((i2 * 5) / IMediaPlayer.MEDIA_INFO_TIMED_TEXT_ERROR) + ((((i2 * 890) / IMediaPlayer.MEDIA_INFO_TIMED_TEXT_ERROR) * 10) / IjkMediaCodecInfo.RANK_LAST_CHANCE);
            layoutParams.topMargin = (int) (((((((double) ((float) i3)) * 577.5d) / 1600.0d) + ((double) (this.s / 2))) - ((double) layoutParams.height)) - ((double) ((int) (((double) i2) * 0.0215d))));
        }
        this.R.setLayoutParams(layoutParams);
    }

    private void y() {
        this.T = (LinearLayout) findViewById(R.id.LL_ui_gifmaking_MemoryFullLayout);
        this.T.setVisibility(4);
        this.T.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass9 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.U = (Button) findViewById(R.id.B_ui_gifmaking_MemoryFullConfirm);
        this.U.setOnClickListener(this.aa);
    }

    /* access modifiers changed from: protected */
    public void b(boolean z2) {
        if (z2) {
            this.T.setVisibility(0);
        } else {
            this.T.setVisibility(4);
        }
    }

    private void z() {
        this.V = (LinearLayout) findViewById(R.id.LL_ui_gifmaking_ProgressLayout);
        this.V.setVisibility(4);
        this.V.setAlpha(0.8f);
        this.V.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass10 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.W = (ImageView) findViewById(R.id.IV_ui_gifmaking_ProgressIcon);
    }

    /* access modifiers changed from: protected */
    public void c(boolean z2) {
        if (z2) {
            this.V.setVisibility(0);
            ((AnimationDrawable) this.W.getDrawable()).start();
            this.d.z = true;
            return;
        }
        this.V.setVisibility(4);
        ((AnimationDrawable) this.W.getDrawable()).stop();
        this.d.z = false;
    }

    /* access modifiers changed from: protected */
    public boolean r() {
        if (new File(this.d.b.o(this.d.s)).exists()) {
            return true;
        }
        return false;
    }

    public void f(int i2) {
        d.a(this, i2);
    }

    private void A() {
        this.t = new OrientationEventListener(this) {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_GifMakingController.AnonymousClass3 */

            public void onOrientationChanged(int i) {
                if (i != -1) {
                    if (i > 350 || i < 10) {
                        if (UI_GifMakingController.this.r != 1) {
                            UI_GifMakingController.this.f(1);
                            UI_GifMakingController.this.r = 1;
                        }
                    } else if (i > 170 && i < 190 && UI_GifMakingController.this.r != 9) {
                        UI_GifMakingController.this.f(9);
                        UI_GifMakingController.this.r = 9;
                    }
                }
            }
        };
    }

    /* access modifiers changed from: protected */
    public void s() {
        this.l = null;
        a(false);
        Intent intent = new Intent(this, UI_PhoneGalleryController.class);
        if (this.j) {
            intent.putExtra("mode", 1024);
            this.j = false;
            this.d.q = new GeneralFunction.a((Context) this.b, true, getResources().getString(R.string.sdcard_removed), 2000);
        } else {
            intent.putExtra("mode", 1056);
        }
        this.b.a(1024, this, intent);
        this.b.a(0, (GeneralFunction.a.a) null);
        this.b.b(268435455L);
    }

    /* access modifiers changed from: protected */
    public void t() {
        this.l = null;
        this.b.a(256, this, new Intent(this, UI_LiveViewController.class));
        this.b.a(0, (GeneralFunction.a.a) null);
        this.b.b(268435455L);
        this.d.d = 1;
    }
}
