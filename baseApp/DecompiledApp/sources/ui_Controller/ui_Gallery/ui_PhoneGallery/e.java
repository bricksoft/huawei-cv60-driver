package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.Player.player.h;
import GeneralFunction.d;
import GeneralFunction.e.b;
import GeneralFunction.o;
import GeneralFunction.q.a;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.os.Parcelable;
import com.huawei.cvIntl60.R;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import ui_Controller.a.c;
import ui_Controller.b.j;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private UI_PhoneGalleryController f1414a = null;
    private a.b b = new a.b() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.e.AnonymousClass1 */
    };

    private void a(String str, int i) {
        d.a("UI_PhoneGallerySingleHandler", str, i);
    }

    public e(UI_PhoneGalleryController uI_PhoneGalleryController) {
        this.f1414a = uI_PhoneGalleryController;
    }

    public void a(Message message) {
        new a.c.a(message);
        switch (message.what) {
            case 8976:
                if (a.b.b.a.b(this.f1414a) == 2) {
                    o.a((Context) this.f1414a, true, true, this.f1414a.getResources().getString(R.string.accessing_mobile_network_title), this.f1414a.getResources().getString(R.string.accessing_mobile_network_title_content), new String[]{this.f1414a.getResources().getString(R.string.dialog_option_cancel), this.f1414a.getResources().getString(R.string.dialog_option_ok_for_two_button)}, new int[]{12039, 8975});
                    return;
                }
                this.f1414a.a(8975);
                return;
            case 9099:
                a("MSG_UI_GALLERY_SINGLE_UPDATE_CURRENT_IMAGE", 4);
                a.c.a aVar = new a.c.a(message);
                int b2 = aVar.b("update index");
                Bitmap bitmap = (Bitmap) aVar.f("update bitmap").a();
                if (b2 == this.f1414a.e.s) {
                    this.f1414a.e.S.b = b2;
                    this.f1414a.e.S.f1321a = bitmap;
                    if (this.f1414a.e.O == 3) {
                        this.f1414a.a(9098, 0L);
                        return;
                    }
                    return;
                }
                return;
            case 9100:
                int b3 = new a.c.a(message).b("update index");
                int a2 = this.f1414a.e.b.a(b3);
                this.f1414a.e.s = b3;
                this.f1414a.a(a2, d());
                return;
            case 12034:
                a("ACTIVITY_RESUME", 3);
                this.f1414a.aC();
                j jVar = this.f1414a.c.c;
                if (this.f1414a.c.p() && this.f1414a.c.c.d) {
                    this.f1414a.c.i();
                    this.f1414a.c.c.d = false;
                }
                this.f1414a.a(true);
                if (this.f1414a.ah() == 2) {
                    this.f1414a.F();
                    return;
                }
                return;
            case 12035:
                a("ACTIVITY_PAUSE", 3);
                GeneralFunction.m.a.a(true);
                this.f1414a.a(false);
                if (this.f1414a.x()) {
                    this.f1414a.t(false);
                }
                if (!this.f1414a.L()) {
                    j jVar2 = this.f1414a.c.c;
                    if (this.f1414a.e.N == 0) {
                        int a3 = this.f1414a.e.b.a(this.f1414a.e.s);
                        this.f1414a.e.O = 6;
                        this.f1414a.a(a3, d());
                        this.f1414a.a(true, false, false);
                        return;
                    } else if (this.f1414a.p && !this.f1414a.e.A && this.f1414a.e.O != 0 && this.f1414a.e.O != 8) {
                        boolean i = this.f1414a.e.b.i(this.f1414a.e.s);
                        int a4 = this.f1414a.e.b.a(this.f1414a.e.s);
                        this.f1414a.a(a4, false);
                        if (!i) {
                            this.f1414a.a(true, false, false);
                            return;
                        } else if (this.f1414a.q()) {
                            this.f1414a.i(true);
                            return;
                        } else if (a4 == 0) {
                            this.f1414a.a(true, true, true);
                            return;
                        } else {
                            this.f1414a.a(true, false, true);
                            this.f1414a.x(false);
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 12036:
                a("ACTIVITY_STOP", 4);
                return;
            case 12037:
                a("ACTIVITY_DESTROY", 4);
                return;
            case 12038:
                a("ACTIVITY_CONFIGURATION_CHANGED", 4);
                return;
            case 12042:
                o.a((Context) this.f1414a, true, false, this.f1414a.getResources().getString(R.string.warning), this.f1414a.getResources().getString(R.string.permission_always_deny_msg), this.f1414a.getResources().getString(R.string.dialog_option_ok), 12032);
                return;
            case 61445:
                if (!this.f1414a.p() && !this.f1414a.e.A) {
                    this.f1414a.X();
                }
                this.f1414a.B(false);
                this.f1414a.w(false);
                this.f1414a.a(false);
                return;
            default:
                switch (this.f1414a.c.a()) {
                    case 1056:
                        b(message);
                        return;
                    case 1057:
                    case 1060:
                        c(message);
                        return;
                    case 1058:
                    default:
                        return;
                    case 1059:
                        d(message);
                        return;
                    case 1061:
                        e(message);
                        return;
                }
        }
    }

    public void b(Message message) {
        switch (message.what) {
            case 8960:
            case 8963:
            default:
                return;
            case 8961:
            case 8964:
            case 61444:
                if (message.what == 8961) {
                    a("MSG_UI_GALLERY_VIEW_TOUCH_ZOOM", 4);
                } else if (message.what == 8964) {
                    a("MSG_UI_GALLERY_VIEW_DOUBLE_CLICK", 4);
                } else if (message.what == 61444) {
                    a("MSG_UI_GALLERY_SINGLE_HIDE_INFO", 4);
                }
                this.f1414a.a(false, false, false);
                return;
            case 8962:
                a("MSG_UI_GALLERY_VIEW_CLICK", 4);
                if (this.f1414a.e.v) {
                    this.f1414a.a(false, false, false);
                    return;
                } else if (this.f1414a.K()) {
                    this.f1414a.B(false);
                    return;
                } else {
                    this.f1414a.a(true, false, false);
                    return;
                }
            case 8968:
                this.f1414a.c.c(268435455);
                return;
            case 9088:
                a("MSG_UI_GALLERY_SINGLE_DELETE_PRESS wFileCountInGroup" + this.f1414a.e.b.q(this.f1414a.e.s), 4);
                this.f1414a.c.b(268435455L);
                this.f1414a.c.c(1);
                if (this.f1414a.e.r) {
                    o.a((Context) this.f1414a, true, true, this.f1414a.getResources().getString(R.string.delete_image_title), this.f1414a.getResources().getString(R.string.delete_one_image_confirm), new String[]{this.f1414a.getResources().getString(R.string.dialog_option_cancel), this.f1414a.getResources().getString(R.string.dialog_option_delete)}, new int[]{9091, 9090}, true, (int) R.color.red);
                    return;
                } else if (this.f1414a.e.b.q(this.f1414a.e.s) > 1) {
                    o.a((Context) this.f1414a, true, true, this.f1414a.getResources().getString(R.string.delete_image_title), this.f1414a.getResources().getString(R.string.delete_more_image_confirm), new String[]{this.f1414a.getResources().getString(R.string.dialog_option_cancel), this.f1414a.getResources().getString(R.string.dialog_option_delete)}, new int[]{9091, 9090}, true, (int) R.color.red);
                    return;
                } else {
                    o.a((Context) this.f1414a, true, true, this.f1414a.getResources().getString(R.string.delete_image_title), this.f1414a.getResources().getString(R.string.delete_one_image_confirm), new String[]{this.f1414a.getResources().getString(R.string.dialog_option_cancel), this.f1414a.getResources().getString(R.string.dialog_option_delete)}, new int[]{9091, 9090}, true, (int) R.color.red);
                    return;
                }
            case 9089:
                ArrayList<String> arrayList = new ArrayList<>();
                GeneralFunction.e.a r = this.f1414a.e.b.r(this.f1414a.e.s);
                int i = r.b;
                for (int i2 = 0; i2 < r.c.size(); i2++) {
                    arrayList.add(this.f1414a.e.b.h(i, r.c.get(i2).intValue()));
                }
                this.f1414a.startActivityForResult(a(arrayList), 1);
                return;
            case 9090:
                this.f1414a.c.a(1057);
                this.f1414a.a(9090);
                return;
            case 9091:
                this.f1414a.w(true);
                this.f1414a.c.c(268435455);
                return;
            case 9107:
                this.f1414a.a(9108, 0L);
                return;
            case 9124:
                this.f1414a.c.c(268435455);
                this.f1414a.N();
                return;
            case 9126:
                this.f1414a.c.c(268435455);
                this.f1414a.O();
                return;
            case 9139:
                a("MSG_UI_GALLERY_SINGLE_DELETE_ERROR_FILE", 3);
                if (this.f1414a.e.f == 1 || this.f1414a.e.f == 6) {
                    a("break by select mode:" + this.f1414a.e.f, 3);
                    return;
                }
                this.f1414a.ay();
                this.f1414a.a(8966);
                return;
            case 9145:
                this.f1414a.e.b.a(this.f1414a.e.s);
                this.f1414a.c(this.f1414a.e.s, false);
                return;
            case 12039:
                this.f1414a.c.c.g.f1324a = 0;
                return;
            case 32768:
                a("MSG_KEY_BACK " + this.f1414a.e.r, 4);
                this.f1414a.e.S.b = -1;
                this.f1414a.e.S.f1321a = null;
                if (this.f1414a.e.r) {
                    this.f1414a.P();
                    b();
                    this.f1414a.h(this.f1414a.e.s);
                } else if (this.f1414a.x()) {
                    this.f1414a.t(false);
                    this.f1414a.a(true, false, false);
                } else {
                    this.f1414a.a(8966, 0L);
                    this.f1414a.C(false);
                    this.f1414a.E(false);
                    this.f1414a.B(false);
                }
                this.f1414a.c.c(268435455);
                return;
        }
    }

    public void c(Message message) {
        b a2;
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 9090:
                this.f1414a.e.z = true;
                this.f1414a.e.S.f1321a = null;
                this.f1414a.e.S.b = -1;
                this.f1414a.e.k.add(Integer.valueOf(this.f1414a.e.s));
                c();
                return;
            case 9091:
                if (this.f1414a.c.a() == 1060) {
                    this.f1414a.c.a(1059);
                } else {
                    this.f1414a.c.a(1056);
                }
                this.f1414a.w(true);
                this.f1414a.c.c(268435455);
                return;
            case 12323:
                a("MSG_FILE_PHONE_DELETE_A_FILE_DONE", 4);
                this.f1414a.e.m.h();
                this.f1414a.e.f1320a.b();
                b d = this.f1414a.e.f1320a.d();
                this.f1414a.e.m.a(d.f74a);
                if (this.f1414a.e.r) {
                    int i = this.f1414a.e.t;
                    if (aVar.a("delete group")) {
                        a2 = new b();
                    } else {
                        a2 = GeneralFunction.e.e.a(d.f74a, i);
                    }
                    this.f1414a.e.b.a(this.f1414a.d.e.a(), a2);
                } else {
                    this.f1414a.e.b.a(this.f1414a.d.e.a(), d);
                }
                a("New GridView List", 4);
                this.f1414a.e.m.h();
                return;
            case 12324:
                this.f1414a.e.W = false;
                if (this.f1414a.c.a() == 1060) {
                    this.f1414a.c.a(1059);
                    int i2 = this.f1414a.e.s;
                    if (i2 >= this.f1414a.e.b.e() || i2 < 0) {
                        a("360 Delete: No File To Update", 4);
                        this.f1414a.e.O = 8;
                    } else {
                        a();
                        this.f1414a.aa();
                    }
                } else {
                    this.f1414a.c.a(1056);
                }
                o.b(this.f1414a);
                a("MSG_FILE_PHONE_DELETE_ALL_COMPLETE 1 ulBrowseIndex:" + this.f1414a.e.s + " objActivity.sLocalGallery.ulBrowseSingleIndex " + this.f1414a.e.t, 4);
                if (this.f1414a.e.s >= 0) {
                    this.f1414a.c.c(268435455);
                    if (this.f1414a.e.d == 3) {
                        this.f1414a.a(true, false, false);
                    }
                } else if (this.f1414a.e.r) {
                    this.f1414a.P();
                    b();
                    if (this.f1414a.e.s == -1) {
                        this.f1414a.e.s = 0;
                        this.f1414a.a(8966, 0L);
                        return;
                    }
                    this.f1414a.h(this.f1414a.e.s);
                    this.f1414a.c.c(268435455);
                } else {
                    this.f1414a.e.s = 0;
                    this.f1414a.a(8966, 0L);
                    return;
                }
                a("MSG_FILE_PHONE_DELETE_ALL_COMPLETE 2 ulBrowseIndex:" + this.f1414a.e.s + " objActivity.sLocalGallery.ulBrowseSingleIndex " + this.f1414a.e.t, 4);
                this.f1414a.a(this.f1414a.e.b.a(this.f1414a.e.s), d());
                this.f1414a.e.z = false;
                return;
            default:
                return;
        }
    }

    public void d(Message message) {
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 8465:
                this.f1414a.e.Z = true;
                this.f1414a.e.O = 8;
                this.f1414a.P();
                this.f1414a.aa();
                this.f1414a.e.X = null;
                this.f1414a.a(9149, 50L);
                this.f1414a.as();
                return;
            case 8466:
                this.f1414a.e.X = new h();
                this.f1414a.e.X.m = this.f1414a.ag().m;
                this.f1414a.e.X.l = this.f1414a.ag().l;
                this.f1414a.e.X.f51a = this.f1414a.ag().f51a;
                this.f1414a.e.X.q = this.f1414a.ag().q;
                this.f1414a.e.X.r = this.f1414a.ag().r;
                this.f1414a.e.X.e = this.f1414a.ag().e;
                this.f1414a.e.X.s = this.f1414a.ag().s;
                System.arraycopy(this.f1414a.ag().z, 0, this.f1414a.e.X.z, 0, this.f1414a.ag().z.length);
                this.f1414a.al();
                this.f1414a.e.Z = true;
                this.f1414a.e.O = 8;
                this.f1414a.P();
                this.f1414a.aa();
                this.f1414a.a(9147, 0L);
                return;
            case 8467:
                this.f1414a.e.Z = true;
                this.f1414a.e.O = 8;
                this.f1414a.P();
                this.f1414a.aa();
                this.f1414a.e.X = null;
                this.f1414a.a(9148, 0L);
                this.f1414a.as();
                return;
            case 8962:
            case 8977:
                a("MSG_UI_GALLERY_VIEW_CLICK 360", 4);
                if (this.f1414a.p()) {
                    return;
                }
                if (!this.f1414a.e.v) {
                    int a2 = this.f1414a.e.b.a(this.f1414a.e.s);
                    if (this.f1414a.e.O == 4 || this.f1414a.e.O == 5) {
                        if (this.f1414a.K()) {
                            this.f1414a.B(false);
                            this.f1414a.x(true);
                            return;
                        }
                        this.f1414a.a(true, false, true);
                        this.f1414a.x(false);
                        return;
                    } else if (a2 == 0) {
                        if (this.f1414a.K()) {
                            this.f1414a.B(false);
                            return;
                        } else {
                            this.f1414a.a(true, true, true);
                            return;
                        }
                    } else if (this.f1414a.K()) {
                        this.f1414a.B(false);
                        this.f1414a.w(true);
                        return;
                    } else {
                        this.f1414a.a(true, false, true);
                        this.f1414a.x(false);
                        return;
                    }
                } else {
                    this.f1414a.X();
                    this.f1414a.a(false, false, false);
                    this.f1414a.x(true);
                    return;
                }
            case 8963:
            default:
                return;
            case 8968:
                this.f1414a.c.c(268435455);
                return;
            case 9088:
                a("MSG_UI_GALLERY_SINGLE_DELETE_PRESS wFileCountInGroup" + this.f1414a.e.b.q(this.f1414a.e.s), 4);
                this.f1414a.c.b(268435455L);
                this.f1414a.c.c(1);
                if (this.f1414a.e.r) {
                    o.a((Context) this.f1414a, true, true, this.f1414a.getResources().getString(R.string.delete_image_title), this.f1414a.getResources().getString(R.string.delete_one_image_confirm), new String[]{this.f1414a.getResources().getString(R.string.dialog_option_cancel), this.f1414a.getResources().getString(R.string.dialog_option_delete)}, new int[]{9091, 9090}, true, (int) R.color.red);
                    return;
                } else if (this.f1414a.e.b.q(this.f1414a.e.s) > 1) {
                    o.a((Context) this.f1414a, true, true, this.f1414a.getResources().getString(R.string.delete_image_title), this.f1414a.getResources().getString(R.string.delete_more_image_confirm), new String[]{this.f1414a.getResources().getString(R.string.dialog_option_cancel), this.f1414a.getResources().getString(R.string.dialog_option_delete)}, new int[]{9091, 9090}, true, (int) R.color.red);
                    return;
                } else {
                    o.a((Context) this.f1414a, true, true, this.f1414a.getResources().getString(R.string.delete_image_title), this.f1414a.getResources().getString(R.string.delete_one_image_confirm), new String[]{this.f1414a.getResources().getString(R.string.dialog_option_cancel), this.f1414a.getResources().getString(R.string.dialog_option_delete)}, new int[]{9091, 9090}, true, (int) R.color.red);
                    return;
                }
            case 9089:
                ArrayList<String> arrayList = new ArrayList<>();
                GeneralFunction.e.a r = this.f1414a.e.b.r(this.f1414a.e.s);
                int i = r.b;
                for (int i2 = 0; i2 < r.c.size(); i2++) {
                    arrayList.add(this.f1414a.e.b.h(i, r.c.get(i2).intValue()));
                }
                this.f1414a.startActivityForResult(a(arrayList), 1);
                return;
            case 9090:
                this.f1414a.Z();
                this.f1414a.c.a(1060);
                this.f1414a.a(9090);
                return;
            case 9091:
                this.f1414a.w(true);
                this.f1414a.c.c(268435455);
                return;
            case 9092:
                File file = new File(this.f1414a.e.B);
                if (file.exists()) {
                    file.delete();
                }
                this.f1414a.d.e.a(this.f1414a.d.e.c(), 0);
                this.f1414a.c.x();
                this.f1414a.a(9129);
                return;
            case 9093:
                if (!this.f1414a.ac()) {
                    a("MSG_UI_GALLERY_SINGLE_360_PRESS_PREVIOUS denied", 0);
                    return;
                }
                this.f1414a.e.W = false;
                GeneralFunction.e.d dVar = this.f1414a.e.b;
                int i3 = this.f1414a.e.s;
                int b2 = ui_Controller.ui_Gallery.a.b(dVar, i3);
                a("MSG_UI_GALLERY_SINGLE_360_PRESS_PREVIOUS lPrevious360FileIndex " + b2, 4);
                if (b2 == -1) {
                    a("Can Not Press Previous Key", 4);
                    this.f1414a.c.c(268435455);
                    return;
                } else if (this.f1414a.e.O == 3) {
                    a("Can't Press Previous: Waitting For Image Load", 4);
                    return;
                } else {
                    boolean i4 = this.f1414a.e.b.i(b2);
                    boolean j = this.f1414a.e.b.j(b2);
                    int a3 = this.f1414a.e.b.a(i3);
                    int a4 = this.f1414a.e.b.a(b2);
                    if (i4 && j) {
                        if (a3 == 0) {
                            this.f1414a.a(true, true, true);
                        } else {
                            this.f1414a.a(true, false, true);
                        }
                        a(a4);
                        this.f1414a.Z();
                        this.f1414a.aa();
                        this.f1414a.a(9151, 0L);
                        return;
                    }
                    return;
                }
            case 9094:
                if (!this.f1414a.ac()) {
                    a("MSG_UI_GALLERY_SINGLE_360_PRESS_NEXT denied", 0);
                    return;
                }
                this.f1414a.e.W = false;
                GeneralFunction.e.d dVar2 = this.f1414a.e.b;
                int i5 = this.f1414a.e.s;
                int a5 = ui_Controller.ui_Gallery.a.a(dVar2, i5);
                if (a5 == -1) {
                    a("Can Not Press Previous Key", 4);
                    this.f1414a.c.c(268435455);
                    return;
                } else if (this.f1414a.e.O == 3) {
                    a("Can't Press Next: Waitting For Image Load", 4);
                    return;
                } else {
                    boolean i6 = this.f1414a.e.b.i(a5);
                    boolean j2 = this.f1414a.e.b.j(a5);
                    int a6 = this.f1414a.e.b.a(i5);
                    int a7 = this.f1414a.e.b.a(a5);
                    if (i6 && j2) {
                        if (a6 == 0) {
                            this.f1414a.a(true, true, true);
                        } else {
                            this.f1414a.a(true, false, true);
                        }
                        a(a7);
                        this.f1414a.Z();
                        this.f1414a.aa();
                        this.f1414a.a(9150, 0L);
                        return;
                    }
                    return;
                }
            case 9095:
                int a8 = this.f1414a.e.b.a(this.f1414a.e.s);
                this.f1414a.e.O = 4;
                this.f1414a.aa();
                this.f1414a.a(a8, d());
                if (!this.f1414a.L()) {
                    this.f1414a.a(true, false, true);
                } else {
                    this.f1414a.a(false, false, false);
                    this.f1414a.x(true);
                }
                this.f1414a.c.c(268435455);
                this.f1414a.c.a(9097, 0);
                return;
            case 9096:
                int a9 = this.f1414a.e.b.a(this.f1414a.e.s);
                this.f1414a.e.O = 1;
                this.f1414a.aa();
                this.f1414a.a(a9, d());
                this.f1414a.a(true, false, true);
                return;
            case 9097:
                int b3 = aVar.b("360PlayerStatus");
                int i7 = this.f1414a.e.O;
                int i8 = this.f1414a.e.s;
                int i9 = this.f1414a.e.P;
                int a10 = this.f1414a.e.b.a(this.f1414a.e.s);
                a("MSG_UI_GALLERY_SINGLE_360_PLAYER_STATUS_UPDATE " + b3 + " " + i7 + " " + i9, 2);
                if (i8 != -1) {
                    switch (i7) {
                        case 0:
                        case 1:
                            if (b3 == 1) {
                                this.f1414a.Y();
                            } else if (b3 == 2) {
                                if (a10 == 1) {
                                    this.f1414a.a(9095, 0L);
                                } else if (a10 == 0) {
                                    this.f1414a.a(9098, 0L);
                                }
                            }
                            if (b3 == 5) {
                                this.f1414a.ad();
                                return;
                            }
                            return;
                        case 2:
                            if (b3 != 2) {
                                a();
                                return;
                            }
                            return;
                        case 3:
                            if (b3 == 0) {
                                a();
                                return;
                            }
                            return;
                        case 4:
                            if (b3 == 1 || b3 == 0) {
                                this.f1414a.a(this.f1414a.e.b.o(i8));
                            } else if (b3 == 2) {
                                if (this.f1414a.m()) {
                                    this.f1414a.g(false);
                                }
                                if (this.f1414a.e.W) {
                                    this.f1414a.a(9136, 50L);
                                } else {
                                    this.f1414a.B();
                                }
                                this.f1414a.e.O = 5;
                                this.f1414a.c.c(268435455);
                                if (!this.f1414a.L() && !this.f1414a.e.A) {
                                    this.f1414a.a(true, false, true);
                                    this.f1414a.x(false);
                                }
                                if (!this.f1414a.q() && !this.f1414a.e.A) {
                                    this.f1414a.i(false);
                                }
                            }
                            if (b3 == 5) {
                                this.f1414a.ad();
                                return;
                            }
                            return;
                        case 5:
                            if (b3 == 2) {
                                d.a((Context) this.f1414a, true);
                                this.f1414a.av();
                                int a11 = this.f1414a.a();
                                UI_PhoneGalleryController uI_PhoneGalleryController = this.f1414a;
                                if (a11 == 0) {
                                    this.f1414a.e.W = false;
                                    return;
                                }
                                return;
                            } else if (b3 == 3) {
                                this.f1414a.e.O = 5;
                                this.f1414a.C();
                                this.f1414a.B();
                                this.f1414a.au();
                                this.f1414a.e.V = 0;
                                return;
                            } else {
                                a();
                                return;
                            }
                        case 6:
                            if (b3 == 1) {
                                this.f1414a.af();
                                this.f1414a.c.a(1056);
                                this.f1414a.c.c(268435455);
                                return;
                            }
                            return;
                        case 7:
                            if (b3 == 1) {
                                this.f1414a.e.S.b = -1;
                                this.f1414a.e.S.f1321a = null;
                                this.f1414a.af();
                                this.f1414a.c.a(1056);
                                this.f1414a.a(32768, 0L);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                } else {
                    return;
                }
            case 9098:
                a("MSG_UI_GALLERY_SINGLE_360_PLAYER_PHOTO_UPDATE", 4);
                Bitmap bitmap = this.f1414a.e.S.f1321a;
                int i10 = this.f1414a.e.S.b;
                int ah = this.f1414a.ah();
                if (ah != 2) {
                    a("Photo Update Player Status Error " + ah, 4);
                } else if (bitmap == null || bitmap.isRecycled()) {
                    if (bitmap == null) {
                        a("Photo Update Bitmap Null", 4);
                    } else {
                        a("Photo Update Bitmap Is Recycled", 4);
                    }
                    this.f1414a.e.O = 3;
                } else if (i10 != this.f1414a.e.s) {
                    a("Photo Update Index Fault " + i10 + " " + this.f1414a.e.s, 4);
                    this.f1414a.e.O = 3;
                } else {
                    a("Photo Update " + i10 + " " + bitmap, 4);
                    int i11 = this.f1414a.e.s;
                    int b4 = this.f1414a.e.b.b(i11);
                    int c = this.f1414a.e.b.c(i11);
                    this.f1414a.e.O = 2;
                    this.f1414a.a(bitmap, b4, c);
                    if (this.f1414a.m()) {
                        this.f1414a.g(false);
                    }
                }
                this.f1414a.c.c(268435455);
                if (!this.f1414a.e.A) {
                    this.f1414a.i(false);
                    return;
                }
                return;
            case 9118:
                this.f1414a.X();
                this.f1414a.S();
                return;
            case 9119:
                this.f1414a.X();
                this.f1414a.T();
                return;
            case 9120:
                this.f1414a.X();
                this.f1414a.G(true);
                return;
            case 9121:
                this.f1414a.X();
                this.f1414a.G(false);
                return;
            case 9122:
                this.f1414a.X();
                this.f1414a.F(true);
                return;
            case 9123:
                this.f1414a.X();
                this.f1414a.F(false);
                return;
            case 9125:
                this.f1414a.V();
                return;
            case 9127:
                this.f1414a.X();
                this.f1414a.w(false);
                this.f1414a.a(this.f1414a.v);
                this.f1414a.U();
                this.f1414a.i(true);
                this.f1414a.D = this.f1414a.ao();
                if (this.f1414a.D) {
                    this.f1414a.D();
                    return;
                }
                return;
            case 9128:
                String b5 = GeneralFunction.g.a.b(c.d + GeneralFunction.g.a.a(Long.valueOf(System.currentTimeMillis()), "jpg"));
                a.c.a aVar2 = new a.c.a(20502);
                aVar2.a("FilePath", b5);
                this.f1414a.b(aVar2);
                this.f1414a.p(true);
                return;
            case 9129:
                this.f1414a.X();
                this.f1414a.i(false);
                this.f1414a.o();
                this.f1414a.w(true);
                this.f1414a.a(9131, 0L);
                return;
            case 9130:
                if (aVar.a("bSuccess")) {
                    Long valueOf = Long.valueOf(System.currentTimeMillis());
                    String d = aVar.d("FilePath");
                    this.f1414a.e.B = d;
                    File file2 = new File(d);
                    int c2 = this.f1414a.d.e.c();
                    this.f1414a.d.e.a(d.substring(d.lastIndexOf("/") + 1, d.length()), d.substring(0, d.lastIndexOf("/") + 1), file2.length(), 0, 1, GeneralFunction.b.a(d), GeneralFunction.b.b(d), 0, valueOf.longValue(), valueOf.longValue(), c2 + 1, 0, true, true, true, true, false, false, false, false, true);
                    this.f1414a.c.x();
                    GeneralFunction.g.a.a(this.f1414a, d);
                    this.f1414a.d.b.ae = valueOf.longValue();
                    this.f1414a.p(false);
                    this.f1414a.l(true);
                    this.f1414a.n(true);
                    this.f1414a.g(false);
                    this.f1414a.e.A = true;
                    return;
                }
                this.f1414a.g(false);
                this.f1414a.h(true);
                this.f1414a.p(false);
                if (this.f1414a.D) {
                    this.f1414a.G();
                    this.f1414a.D = false;
                    return;
                }
                return;
            case 9131:
                this.f1414a.g(false);
                this.f1414a.h(true);
                if (this.f1414a.D) {
                    this.f1414a.E();
                    this.f1414a.D = false;
                    return;
                }
                return;
            case 9132:
                this.f1414a.e.f = 6;
                this.f1414a.D();
                this.f1414a.C();
                this.f1414a.an();
                this.f1414a.Z();
                this.f1414a.aj();
                this.f1414a.a(this.f1414a.v);
                this.f1414a.o(true);
                this.f1414a.b(20492);
                this.f1414a.i(this.f1414a.e.s);
                return;
            case 9133:
                this.f1414a.e.f = 6;
                this.f1414a.D();
                this.f1414a.C();
                this.f1414a.an();
                this.f1414a.Z();
                this.f1414a.aj();
                this.f1414a.a(this.f1414a.v);
                this.f1414a.o(true);
                this.f1414a.b(20495);
                this.f1414a.i(this.f1414a.e.s);
                return;
            case 9134:
                this.f1414a.e.f = 0;
                this.f1414a.o(false);
                this.f1414a.o();
                this.f1414a.w(false);
                this.f1414a.e.W = false;
                if (!this.f1414a.o) {
                    this.f1414a.aD();
                    return;
                }
                if (this.f1414a.e.d == 1) {
                    this.f1414a.c(this.f1414a.getString(R.string.move_to_sdcard));
                    this.f1414a.e.f1320a.a(this.f1414a.d.e.a(), this.f1414a.d.f);
                    this.f1414a.e.b.a(this.f1414a.d.e.a(), this.f1414a.d.f);
                } else if (this.f1414a.e.d == 2) {
                    this.f1414a.c(this.f1414a.getString(R.string.move_to_phone));
                    this.f1414a.e.f1320a.a(this.f1414a.d.e.a(), this.f1414a.d.g);
                    this.f1414a.e.b.a(this.f1414a.d.e.a(), this.f1414a.d.g);
                }
                this.f1414a.e.m.h();
                this.f1414a.e.f1320a.b();
                this.f1414a.e.m.a(this.f1414a.e.f1320a.d().f74a);
                this.f1414a.w(true);
                int i12 = this.f1414a.e.s;
                int e = this.f1414a.e.b.e();
                if (i12 >= e || i12 < 0) {
                    a("360 Move done: No File To Update", 3);
                    this.f1414a.e.O = 8;
                } else {
                    a();
                    this.f1414a.aa();
                }
                a("MSG_UI_GALLERY_SINGLE_PRESS_MOVE_FILE_DONE 0 ulBrowseIndex:" + this.f1414a.e.s + " LinkTable_GetSize " + e, 4);
                if (this.f1414a.e.s >= e) {
                    ui_Controller.b.e eVar = this.f1414a.e;
                    eVar.s--;
                }
                a("MSG_UI_GALLERY_SINGLE_PRESS_MOVE_FILE_DONE 1 ulBrowseIndex:" + this.f1414a.e.s + " objActivity.sLocalGallery.ulBrowseSingleIndex " + this.f1414a.e.t, 4);
                if (this.f1414a.e.s >= 0) {
                    a();
                    this.f1414a.c.c(268435455);
                } else if (this.f1414a.e.r) {
                    this.f1414a.P();
                    b();
                    if (this.f1414a.e.s == -1) {
                        this.f1414a.e.s = 0;
                        this.f1414a.a(8966, 0L);
                        return;
                    }
                    this.f1414a.h(this.f1414a.e.s);
                    this.f1414a.c.c(268435455);
                } else {
                    this.f1414a.e.s = 0;
                    this.f1414a.a(8966, 0L);
                    return;
                }
                a("MSG_UI_GALLERY_SINGLE_PRESS_MOVE_FILE_DONE 2 ulBrowseIndex:" + this.f1414a.e.s + " objActivity.sLocalGallery.ulBrowseSingleIndex " + this.f1414a.e.t, 4);
                this.f1414a.a(this.f1414a.e.b.a(this.f1414a.e.s), d());
                return;
            case 9135:
                this.f1414a.e.f = 0;
                this.f1414a.i(false);
                this.f1414a.o();
                this.f1414a.o(false);
                this.f1414a.w(true);
                return;
            case 9136:
                this.f1414a.g(this.f1414a.e.V);
                return;
            case 9137:
                this.f1414a.I();
                return;
            case 9138:
                this.f1414a.J();
                return;
            case 9139:
                a("MSG_UI_GALLERY_SINGLE_DELETE_ERROR_FILE", 3);
                if (this.f1414a.e.f == 1 || this.f1414a.e.f == 6) {
                    a("break by select mode:" + this.f1414a.e.f, 3);
                    return;
                }
                String o = this.f1414a.e.b.o(this.f1414a.e.s);
                String d2 = GeneralFunction.g.a.d(o);
                File file3 = new File(o);
                if (!file3.exists() || file3.length() <= 0) {
                    this.f1414a.ay();
                    this.f1414a.a(8966);
                    return;
                } else if (!o.toLowerCase().endsWith("mp4")) {
                    this.f1414a.ay();
                    this.f1414a.a(8966);
                    return;
                } else if (!new File(d2).exists()) {
                    a("recreate thm", 3);
                    GeneralFunction.g.a.d(o, d2);
                    this.f1414a.g(false);
                    return;
                } else {
                    return;
                }
            case 9143:
                this.f1414a.o(false);
                this.f1414a.o();
                String str = null;
                switch (this.f1414a.e.d) {
                    case 1:
                        str = this.f1414a.getResources().getString(R.string.sdcard_not_enough_space);
                        break;
                    case 2:
                        str = this.f1414a.getResources().getString(R.string.phone_not_enough_space);
                        break;
                    default:
                        a("lLocalDatabaseSelectMode Wrong!" + this.f1414a.e.d, 1);
                        break;
                }
                o.a((Context) this.f1414a, true, true, this.f1414a.getResources().getString(R.string.move_full_title), str, this.f1414a.getResources().getString(R.string.dialog_option_ok), 9135, true);
                return;
            case 9144:
                this.f1414a.X();
                this.f1414a.U();
                return;
            case 9145:
                int a12 = this.f1414a.e.b.a(this.f1414a.e.s);
                if (a12 == 1) {
                    this.f1414a.s(this.f1414a.e.s);
                    return;
                } else if (a12 == 0) {
                    this.f1414a.c(this.f1414a.e.s, true);
                    return;
                } else {
                    return;
                }
            case 9147:
                if (this.f1414a.ab() || this.f1414a.e.d == 3) {
                    this.f1414a.c.a(3584, this.f1414a, new Intent(this.f1414a, UI_GifMakingController.class));
                    this.f1414a.c.b(268435455L);
                    return;
                }
                this.f1414a.a(9147, 50L);
                return;
            case 9148:
                if (this.f1414a.ab() || this.f1414a.e.d == 3) {
                    this.f1414a.c.a(3840, this.f1414a, new Intent(this.f1414a, UI_NormalPicMakingController.class));
                    this.f1414a.c.b(268435455L);
                    return;
                }
                this.f1414a.a(9148, 50L);
                return;
            case 9149:
                a("MSG_UI_GALLERY_SINGLE_TO_VIDEO_CUT", 2);
                if (this.f1414a.ab()) {
                    a("TO_VIDEO_CUT Release_Done", 2);
                    this.f1414a.c.a(3328, this.f1414a, new Intent(this.f1414a, UI_VideoCutController.class));
                    this.f1414a.c.b(268435455L);
                    return;
                }
                this.f1414a.a(9149, 50L);
                return;
            case 9150:
                if (this.f1414a.ab()) {
                    int a13 = ui_Controller.ui_Gallery.a.a(this.f1414a.e.b, this.f1414a.e.s);
                    a("TO_NEXT_FILE Release_Done:" + a13, 2);
                    this.f1414a.k(a13);
                    this.f1414a.e.s = a13;
                    this.f1414a.a(true, false, true);
                    this.f1414a.c.c(268435455);
                    return;
                }
                this.f1414a.a(9150);
                return;
            case 9151:
                a("MSG_UI_GALLERY_SINGLE_TO_PREVIOUS_FILE", 4);
                if (this.f1414a.ab()) {
                    int b6 = ui_Controller.ui_Gallery.a.b(this.f1414a.e.b, this.f1414a.e.s);
                    a("TO_PREVIOUS_FILE Release_Done:" + b6, 2);
                    this.f1414a.k(b6);
                    this.f1414a.e.s = b6;
                    this.f1414a.a(true, false, true);
                    this.f1414a.c.c(268435455);
                    return;
                }
                this.f1414a.a(9151);
                return;
            case 12039:
                this.f1414a.c.c.g.f1324a = 0;
                if (!this.f1414a.e.A) {
                    this.f1414a.w(true);
                    return;
                }
                return;
            case 12051:
                switch (aVar.b("state")) {
                    case 0:
                        if (this.f1414a.ao()) {
                            this.f1414a.D();
                            this.f1414a.C();
                            this.f1414a.an();
                            return;
                        }
                        return;
                    case 1:
                    default:
                        return;
                }
            case 32768:
                a("MSG_KEY_BACK " + this.f1414a.e.r, 4);
                if (this.f1414a.r()) {
                    return;
                }
                if (this.f1414a.p()) {
                    if (this.f1414a.n()) {
                        this.f1414a.k(false);
                        this.f1414a.m(true);
                    } else if (this.f1414a.x()) {
                        this.f1414a.t(false);
                    } else {
                        this.f1414a.a(9129);
                    }
                    this.f1414a.c.c(268435455);
                    return;
                }
                if (this.f1414a.z()) {
                    this.f1414a.v(false);
                }
                if (this.f1414a.W()) {
                    this.f1414a.X();
                } else if (this.f1414a.x()) {
                    this.f1414a.t(false);
                    this.f1414a.a(8962);
                    if (this.f1414a.e.b.a(this.f1414a.e.s) == 1) {
                        this.f1414a.x(false);
                    }
                } else if (this.f1414a.K()) {
                    this.f1414a.B(false);
                } else {
                    this.f1414a.a(8966, 0L);
                }
                this.f1414a.c.c(268435455);
                return;
            case 32769:
                this.f1414a.x(false);
                if (this.f1414a.ao()) {
                    this.f1414a.D();
                    this.f1414a.C();
                    return;
                }
                this.f1414a.G();
                return;
        }
    }

    public void e(Message message) {
        new a.c.a(message);
        switch (message.what) {
            case 9103:
                String str = this.f1414a.e.U;
                this.f1414a.e.U = null;
                a("Add video:" + GeneralFunction.b.a(str) + "x" + GeneralFunction.b.b(str), 3);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.US);
                long j = 0;
                try {
                    j = simpleDateFormat.parse(simpleDateFormat.format(new Date(System.currentTimeMillis()))).getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                int g = this.f1414a.d.b.f1320a.g();
                String substring = str.substring(0, str.lastIndexOf("/") + 1);
                this.f1414a.d.e.a(str.substring(str.lastIndexOf("/") + 1, str.length()), substring, -1, 1, 1, GeneralFunction.b.a(str), GeneralFunction.b.b(str), 0, j, j, g + 1, 0, true, true, true, true, false, true, true);
                return;
            case 9104:
                this.f1414a.c.x();
                b bVar = this.f1414a.d.f;
                this.f1414a.d.b.f1320a.a(this.f1414a.d.e.a(), bVar);
                int g2 = this.f1414a.e.f1320a.g();
                int u = this.f1414a.e.f1320a.u(g2);
                a("LeaveVideoMakingGroup " + g2 + " " + u, 4);
                this.f1414a.e.s = u;
                this.f1414a.P();
                this.f1414a.e.m.a();
                this.f1414a.e.b.a(this.f1414a.d.e.a(), bVar);
                this.f1414a.i();
                this.f1414a.e.m.b();
                this.f1414a.h(this.f1414a.e.s);
                this.f1414a.a(this.f1414a.e.b.a(this.f1414a.e.s), d());
                this.f1414a.c.a(1056);
                return;
            case 9105:
                this.f1414a.e.T.a();
                return;
            case 9106:
                this.f1414a.c.a(1056);
                return;
            default:
                return;
        }
    }

    private void b() {
        this.f1414a.e.r = false;
        this.f1414a.e.b.a(this.f1414a.d.e.a(), this.f1414a.d.b.f1320a.d());
        if (this.f1414a.e.t >= this.f1414a.e.b.e()) {
            this.f1414a.e.s = this.f1414a.e.t - 1;
        } else {
            this.f1414a.e.s = this.f1414a.e.t;
        }
        this.f1414a.e.t = 0;
    }

    private Intent a(ArrayList<String> arrayList) {
        Intent intent;
        if (arrayList.size() > 1) {
            intent = new Intent("android.intent.action.SEND_MULTIPLE");
        } else {
            intent = new Intent("android.intent.action.SEND");
        }
        intent.setType("image/*");
        if (arrayList.size() > 1) {
            ArrayList<? extends Parcelable> arrayList2 = new ArrayList<>();
            arrayList2.clear();
            for (int i = 0; i < arrayList.size(); i++) {
                arrayList2.add(Uri.fromFile(new File(arrayList.get(i))));
            }
            intent.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList2);
        } else {
            intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(arrayList.get(0))));
        }
        return intent;
    }

    private void c() {
        ArrayList<ui_Controller.b.a> arrayList = new ArrayList<>();
        if (this.f1414a.e.r) {
            int s = this.f1414a.e.b.s(this.f1414a.e.s);
            int i = this.f1414a.e.b.i(this.f1414a.e.s, 0);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Integer.valueOf(i));
            arrayList.add(new ui_Controller.b.a(s, arrayList2, 5, 1));
        } else {
            GeneralFunction.e.a r = this.f1414a.e.b.r(this.f1414a.e.s);
            ArrayList arrayList3 = new ArrayList();
            for (int i2 = 0; i2 < r.c.size(); i2++) {
                arrayList3.add(r.c.get(i2));
            }
            arrayList.add(new ui_Controller.b.a(r.b, arrayList3, 5, 1));
        }
        this.f1414a.c.a(arrayList);
        this.f1414a.i(this.f1414a.e.s);
        if (this.f1414a.e.s >= this.f1414a.e.b.e() - 1) {
            ui_Controller.b.e eVar = this.f1414a.e;
            eVar.s--;
        }
    }

    private boolean d() {
        return b(this.f1414a.e.s);
    }

    private boolean b(int i) {
        if (this.f1414a.e.N != 0) {
            return false;
        }
        boolean i2 = this.f1414a.e.b.i(i);
        boolean j = this.f1414a.e.b.j(i);
        if (!i2 || j) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void f(Message message) {
        boolean z;
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 20492:
                this.f1414a.m(this.f1414a.e.s);
                this.f1414a.aN();
                return;
            case 20493:
                this.f1414a.e.l.clear();
                this.f1414a.a(9134);
                return;
            case 20494:
                this.f1414a.e.l.clear();
                this.f1414a.a(9143);
                return;
            case 20495:
                this.f1414a.m(this.f1414a.e.s);
                this.f1414a.aL();
                return;
            case 20496:
                this.f1414a.e.l.clear();
                this.f1414a.a(9134);
                return;
            case 20497:
                this.f1414a.e.l.clear();
                this.f1414a.a(9143);
                return;
            case 20498:
            case 20499:
            case 20500:
            default:
                return;
            case 20501:
                this.f1414a.aj();
                this.f1414a.a(9127, 0L);
                return;
            case 20502:
                String d = aVar.d("FilePath");
                if (this.f1414a.e.O == 1 || this.f1414a.e.O == 4 || this.f1414a.e.O == 5 || this.f1414a.e.O == 2) {
                    z = this.f1414a.b(d);
                } else {
                    a("Get snapshot fail: 360 Player not ready!", 2);
                    z = false;
                }
                a.c.a aVar2 = new a.c.a(9130);
                aVar2.a("FilePath", d);
                aVar2.a("bSuccess", z);
                this.f1414a.a(aVar2);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        a(this.f1414a.e.b.a(this.f1414a.e.s));
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (i == 0) {
            this.f1414a.e.O = 1;
        } else {
            this.f1414a.e.O = 4;
        }
    }
}
