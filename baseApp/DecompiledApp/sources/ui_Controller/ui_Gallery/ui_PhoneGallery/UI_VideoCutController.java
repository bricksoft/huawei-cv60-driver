package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.Player.player.SphericalVideoPlayer;
import GeneralFunction.a.a;
import GeneralFunction.d;
import GeneralFunction.f.a.c;
import GeneralFunction.i.a.a;
import GeneralFunction.k;
import GeneralFunction.o;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import java.io.File;
import java.text.DecimalFormat;
import ui_Controller.b.b;
import ui_Controller.b.e;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class UI_VideoCutController extends a {
    private TextView A = null;
    private LinearLayout B = null;
    private LinearLayout C = null;
    private LinearLayout D = null;
    private Button E = null;
    private LinearLayout F = null;
    private ImageView G = null;
    private LinearLayout H = null;
    private a.AbstractC0002a I = new a.AbstractC0002a() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_VideoCutController.AnonymousClass3 */

        @Override // GeneralFunction.i.a.a.AbstractC0002a
        public void a(int i, int i2) {
            if (UI_VideoCutController.this.b.a(4194304L)) {
                switch (i) {
                    case 1:
                    case 2:
                        a.c.a aVar = new a.c.a(11073);
                        aVar.a("SelectPointer", i);
                        aVar.a("Index", i2);
                        UI_VideoCutController.this.a(aVar, 0);
                        return;
                    default:
                        UI_VideoCutController.this.b.c(268435455);
                        return;
                }
            }
        }
    };
    private a.b J = new a.b() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_VideoCutController.AnonymousClass4 */

        /*  JADX ERROR: JadxRuntimeException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Failed to find switch 'out' block
            	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:786)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:130)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:88)
            	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:696)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:125)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:88)
            	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:825)
            	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:130)
            	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:88)
            	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:50)
            */
        @Override // GeneralFunction.i.a.a.b
        public boolean a(int r5, int r6, android.view.MotionEvent r7) {
            /*
            // Method dump skipped, instructions count: 112
            */
            throw new UnsupportedOperationException("Method not decompiled: ui_Controller.ui_Gallery.ui_PhoneGallery.UI_VideoCutController.AnonymousClass4.a(int, int, android.view.MotionEvent):boolean");
        }
    };
    private View.OnTouchListener K = new View.OnTouchListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_VideoCutController.AnonymousClass6 */

        public boolean onTouch(View view, MotionEvent motionEvent) {
            int i;
            int i2;
            switch (view.getId()) {
                case R.id.IB_ui_videocut_ArrowIcon:
                    i = R.drawable.gallery_button_arrowback_press;
                    i2 = R.drawable.gallery_button_arrowback_normal;
                    break;
                case R.id.IB_ui_videocut_SaveIcon:
                    i = R.drawable.gallery_button_save_press;
                    i2 = R.drawable.gallery_button_save_normal;
                    break;
                case R.id.IB_ui_videocut_PlayIcon:
                    i = R.drawable.gallery_button_arrowdown_press;
                    i2 = R.drawable.gallery_button_arrowdown_normal;
                    break;
                default:
                    i2 = -1;
                    i = -1;
                    break;
            }
            if (motionEvent.getAction() == 0) {
                ((ImageButton) view).setImageResource(i);
            }
            if (motionEvent.getAction() == 2 && (motionEvent.getX() < 0.0f || motionEvent.getX() > ((float) view.getWidth()) || motionEvent.getY() < 0.0f || motionEvent.getY() > ((float) view.getHeight()))) {
                ((ImageButton) view).setImageResource(i2);
            }
            if (motionEvent.getAction() != 1) {
                return false;
            }
            ((ImageButton) view).setImageResource(i2);
            return false;
        }
    };
    private View.OnClickListener L = new View.OnClickListener() {
        /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_VideoCutController.AnonymousClass7 */

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.IB_ui_videocut_ArrowIcon:
                    if (UI_VideoCutController.this.b.a(1L)) {
                        UI_VideoCutController.this.a(32768, 0);
                        return;
                    }
                    return;
                case R.id.IB_ui_videocut_SaveIcon:
                    if (UI_VideoCutController.this.b.a(8388608L)) {
                        UI_VideoCutController.this.b.b(268435455L);
                        UI_VideoCutController.this.a(11075, 0);
                        return;
                    }
                    return;
                case R.id.IB_ui_videocut_PlayIcon:
                    if (UI_VideoCutController.this.b.a(PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE)) {
                        UI_VideoCutController.this.a(11074, 0);
                        return;
                    }
                    return;
                case R.id.B_ui_videocut_MemoryFullConfirm:
                    UI_VideoCutController.this.a(11077, 0);
                    return;
                default:
                    UI_VideoCutController.this.a((UI_VideoCutController) "error press", (String) 3);
                    return;
            }
        }
    };
    protected UI_ModeMain b = null;
    protected b c = null;
    protected e d = null;
    protected boolean e = false;
    protected boolean f = false;
    protected c g;
    protected SphericalVideoPlayer h = null;
    private int i = 0;
    private int j = 0;
    private int k = -1;
    private int l = -1;
    private OrientationEventListener m = null;
    private f n = null;
    private long o = -1;
    private ImageButton p = null;
    private TextView q = null;
    private TextView r = null;
    private ImageButton s = null;
    private FrameLayout t = null;
    private TextView u = null;
    private ImageButton v = null;
    private FrameLayout w = null;
    private GeneralFunction.i.a.a x = null;
    private TextView y = null;
    private TextView z = null;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        d.a("UI_VideoCutController", str, i2);
    }

    @Override // GeneralFunction.a.a
    public void a(int i2, long j2) {
        if (this.b != null) {
            this.b.a(new a.c.a(i2), j2);
        } else {
            a("SendUiMsg error!! Msg=" + i2, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(a.c.a aVar, long j2) {
        if (this.b != null) {
            this.b.a(aVar, j2);
        } else {
            a("SendUiMsg error!! MsgEx=" + aVar.b().what, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(int i2, long j2) {
        if (this.b != null) {
            this.b.b(i2, j2);
        } else {
            a("UI_SendAidMsg error!!", 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(a.c.a aVar, long j2) {
        if (this.b != null) {
            this.b.b(aVar, j2);
        } else {
            a("UI_SendAidMsg error!!", 0);
        }
    }

    @Override // GeneralFunction.a.a
    public boolean b() {
        if (this.k == 2) {
            return true;
        }
        return false;
    }

    public void onBackPressed() {
        if (this.b.a(1L)) {
            a(32768, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(Message message) {
        a("AidServer_Handler:0x" + Integer.toHexString(message.what), 3);
        this.n.a(message);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a("onCreate", 3);
        this.b = (UI_ModeMain) getApplication();
        this.b.v();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.i = displayMetrics.widthPixels;
        this.j = displayMetrics.heightPixels;
        if (this.j > this.i) {
            this.k = 1;
            setContentView(R.layout.ui_videocut_port);
        } else {
            this.k = 2;
            setContentView(R.layout.ui_videocut_port);
        }
        this.n = new f(this);
        this.g = new c();
        this.c = this.b.c.k;
        this.d = this.b.c.k.b;
        this.b.a(3344, this);
        A();
        a(12033, 100);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        a("onConfigurationChanged", 3);
        this.b.v();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.i = displayMetrics.widthPixels;
        this.j = displayMetrics.heightPixels;
        if (this.h != null) {
            this.h.a(displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
        if (configuration.orientation == 2) {
            a("ui_phonegallery_land", 3);
        } else {
            a("ui_phonegallery_port", 3);
        }
        c(configuration.orientation);
        i();
        this.k = configuration.orientation;
        a.c.a aVar = new a.c.a(12038);
        aVar.a("mode", 3328);
        a(aVar, 200);
        o.a();
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onResume() {
        super.onResume();
        this.m.enable();
        a.c.a aVar = new a.c.a(12034);
        aVar.a("mode", 3328);
        a(aVar, 0);
        if (!t()) {
            a(8452);
        }
    }

    @Override // GeneralFunction.a.a
    public void onPause() {
        super.onPause();
        if (this.b.a() == 3360) {
            a(32768);
        }
        this.m.disable();
        a.c.a aVar = new a.c.a(12035);
        aVar.a("mode", 3328);
        a(aVar, 0);
    }

    @Override // GeneralFunction.a.a
    public void onStop() {
        super.onStop();
        a.c.a aVar = new a.c.a(12036);
        aVar.a("mode", 3328);
        a(aVar, 0);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onDestroy() {
        super.onDestroy();
        a.c.a aVar = new a.c.a(12037);
        aVar.a("mode", 3328);
        a(aVar, 0);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.d.x = true;
        if (this.d.d == 2) {
            this.e = true;
            this.b.b(268435455L);
            if (this.b.a() == 3360) {
                a(32768);
            } else if (this.g != null) {
                this.g.a();
                this.g = null;
            }
            a(8452, 100);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(Message message) {
        this.n.b(message);
    }

    /* access modifiers changed from: protected */
    public void d() {
        v();
        w();
        x();
        y();
        z();
        d(1);
        c(this.k);
    }

    private void v() {
        this.h = (SphericalVideoPlayer) findViewById(R.id.TV_ui_videocut_VideoPlayer);
        this.h.setPlayerStatusListener(new SphericalVideoPlayer.f() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_VideoCutController.AnonymousClass1 */

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.f
            public void a(int i) {
                UI_VideoCutController.this.a((UI_VideoCutController) ("ReturnVideoStatus " + i), (String) 3);
                a.c.a aVar = new a.c.a(9097);
                aVar.a("360PlayerStatus", i);
                UI_VideoCutController.this.a(aVar, 0);
            }
        });
        e();
        this.d.F = -1;
        this.d.G = -1;
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.h.f();
        this.h.a(false);
        this.h.setEnable(true);
        this.h.setPlayerMode(1);
        this.h.setViewType(3);
        this.h.setVideoFilePath(this.d.b.o(this.d.s));
        this.h.setInteractiveMode(1);
        this.h.g();
    }

    /* access modifiers changed from: protected */
    public int f() {
        if (this.h != null) {
            return this.h.d();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public void g() {
        if (this.h != null) {
            this.h.b();
        }
    }

    private void w() {
        this.C = (LinearLayout) findViewById(R.id.LL_ui_videocut_VideoControlBar);
        this.w = (FrameLayout) findViewById(R.id.FL_ui_videocut_SelectScrollBar);
        this.v = (ImageButton) findViewById(R.id.IB_ui_videocut_PlayIcon);
        this.z = (TextView) findViewById(R.id.TV_ui_videocut_MovieStartTime);
        this.A = (TextView) findViewById(R.id.TV_ui_videocut_MovieEndTime);
        this.y = (TextView) findViewById(R.id.TV_ui_videocut_MovieLength);
        this.v.setOnClickListener(this.L);
    }

    /* access modifiers changed from: protected */
    public void h() {
        boolean z2;
        if (this.d.F < 0 || this.d.G < 0) {
            this.d.F = 0;
            this.d.G = this.h.getDuration() / 1000;
        }
        if (this.x != null) {
            z2 = this.x.f102a;
        } else {
            z2 = true;
        }
        l();
        if (this.w.getChildCount() <= 0) {
            this.x = new GeneralFunction.i.a.a(this, this.w, true, (this.h.getDuration() / 1000) + 1, 1, this.d.F, this.d.G, z2, 2);
            this.x.a(this.I);
            this.x.a(this.J);
        }
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.x != null) {
            final int width = this.w.getWidth();
            this.w.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_VideoCutController.AnonymousClass2 */

                public void onGlobalLayout() {
                    UI_VideoCutController.this.a((UI_VideoCutController) ("AAA Widget Width:" + UI_VideoCutController.this.w.getWidth() + " Height:" + UI_VideoCutController.this.w.getHeight()), (String) 0);
                    if (width == UI_VideoCutController.this.w.getWidth()) {
                        UI_VideoCutController.this.a((UI_VideoCutController) "Widget Width not changed!", (String) 3);
                        return;
                    }
                    boolean z = UI_VideoCutController.this.x.f102a;
                    UI_VideoCutController.this.x.a();
                    UI_VideoCutController.this.w.removeAllViews();
                    UI_VideoCutController.this.x = new GeneralFunction.i.a.a(UI_VideoCutController.this, UI_VideoCutController.this.w, true, (UI_VideoCutController.this.h.getDuration() / 1000) + 1, 1, UI_VideoCutController.this.d.F, UI_VideoCutController.this.d.G, z, 2);
                    UI_VideoCutController.this.x.a(UI_VideoCutController.this.I);
                    UI_VideoCutController.this.x.a(UI_VideoCutController.this.J);
                    UI_VideoCutController.this.w.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void j() {
        this.r.setText("" + new DecimalFormat("#.##").format((double) (((float) Math.round(((k() / 1024.0f) / 1024.0f) * 100.0f)) / 100.0f)) + "MB");
    }

    /* access modifiers changed from: protected */
    public float k() {
        if (this.o <= 0) {
            this.o = new File(this.d.b.o(this.d.s)).length();
        }
        return (float) ((this.o * ((long) (this.d.G - this.d.F))) / ((long) (this.h.getDuration() / 1000)));
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        if (z2) {
            this.s.setImageResource(R.drawable.gallery_top_save_black);
            this.u.setTextColor(getResources().getColor(R.color.white));
            this.u.setText(getResources().getString(R.string.edit_save));
        } else {
            this.s.setImageResource(R.drawable.gallery_top_save_gray);
            this.u.setTextColor(getResources().getColor(R.color.gray50));
            this.u.setText(getResources().getString(R.string.edit_save));
        }
        this.s.setEnabled(z2);
    }

    /* access modifiers changed from: protected */
    public void c(int i2) {
        int min = Math.min(this.i, this.j);
        int max = Math.max(this.i, this.j);
        if (this.h != null) {
            min = this.h.getWidth();
            max = this.h.getHeight();
        }
        if (this.B != null && this.C != null) {
            if (i2 == 2) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.B.getLayoutParams());
                layoutParams.height = (min * 140) / 1600;
                this.B.setLayoutParams(layoutParams);
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(this.C.getLayoutParams());
                layoutParams2.height = (min * 210) / 1600;
                this.C.setLayoutParams(layoutParams2);
                return;
            }
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(this.B.getLayoutParams());
            layoutParams3.height = (max * 140) / 1600;
            this.B.setLayoutParams(layoutParams3);
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(this.C.getLayoutParams());
            layoutParams4.height = (max * 210) / 1600;
            this.C.setLayoutParams(layoutParams4);
        }
    }

    /* access modifiers changed from: protected */
    public void d(int i2) {
        switch (i2) {
            case 1:
                this.v.setImageResource(R.drawable.gallery_videocut_play);
                this.w.setVisibility(0);
                this.z.setVisibility(0);
                this.A.setVisibility(0);
                this.y.setVisibility(0);
                this.r.setVisibility(0);
                this.s.setVisibility(0);
                this.u.setVisibility(0);
                this.B.setVisibility(0);
                return;
            case 2:
                this.v.setImageResource(R.drawable.gallery_videocut_stop);
                this.w.setVisibility(4);
                this.z.setVisibility(4);
                this.A.setVisibility(4);
                this.y.setVisibility(4);
                this.r.setVisibility(4);
                this.s.setVisibility(4);
                this.u.setVisibility(4);
                this.B.setVisibility(4);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void l() {
        boolean z2 = true;
        int i2 = this.d.F;
        int i3 = this.d.G;
        int i4 = i3 - i2;
        this.z.setText(String.format("%02d", Integer.valueOf(i2 / 60)) + ":" + String.format("%02d", Integer.valueOf(i2 % 60)));
        this.A.setText(String.format("%02d", Integer.valueOf(i3 / 60)) + ":" + String.format("%02d", Integer.valueOf(i3 % 60)));
        this.y.setText(String.format("%02d", Integer.valueOf(i4 / 60)) + ":" + String.format("%02d", Integer.valueOf(i4 % 60)));
        if (this.h.getDuration() / 1000 <= i4) {
            z2 = false;
        }
        a(z2);
        j();
    }

    /* access modifiers changed from: protected */
    public void e(int i2) {
        if (this.h != null) {
            this.h.seekTo(i2);
            a("Current Position: " + this.h.getCurrentPosition(), 3);
        }
    }

    /* access modifiers changed from: protected */
    public void m() {
        this.h.start();
    }

    /* access modifiers changed from: protected */
    public void n() {
        this.h.pause();
    }

    /* access modifiers changed from: protected */
    public void o() {
        s();
        this.h.k();
        this.h.i();
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        if (this.h.d() == 5 || this.h.d() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void q() {
        switch (this.d.C) {
            case 3:
                a(8448);
                return;
            case 4:
                a(8452);
                return;
            default:
                return;
        }
    }

    private void x() {
        int a2;
        this.B = (LinearLayout) findViewById(R.id.LL_ui_videocut_TopLayout);
        this.B.setVisibility(0);
        this.p = (ImageButton) findViewById(R.id.IB_ui_videocut_ArrowIcon);
        this.p.setImageResource(R.drawable.gallery_top_back_black);
        this.s = (ImageButton) findViewById(R.id.IB_ui_videocut_SaveIcon);
        this.u = (TextView) findViewById(R.id.TV_ui_videocut_SaveIcon);
        this.q = (TextView) findViewById(R.id.TV_ui_videocut_VideoCut);
        this.r = (TextView) findViewById(R.id.TV_ui_videocut_FileSize);
        if (b()) {
            a2 = k.a(this, (float) (this.j / 23));
        } else {
            a2 = k.a(this, (float) (this.j / 44));
        }
        this.q.setTextSize((float) a2);
        this.r.setTextSize((float) a2);
        this.q.setText(getString(R.string.video_cut));
        this.q.setTextColor(getResources().getColor(R.color.black));
        this.r.setTextColor(getResources().getColor(R.color.white));
        this.p.setOnClickListener(this.L);
        this.s.setOnClickListener(this.L);
    }

    private void y() {
        this.D = (LinearLayout) findViewById(R.id.LL_ui_videocut_MemoryFullLayout);
        this.D.setVisibility(4);
        this.D.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_VideoCutController.AnonymousClass5 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.E = (Button) findViewById(R.id.B_ui_videocut_MemoryFullConfirm);
        this.E.setOnClickListener(this.L);
    }

    /* access modifiers changed from: protected */
    public void b(boolean z2) {
        if (z2) {
            this.D.setVisibility(0);
        } else {
            this.D.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public int r() {
        long a2;
        int i2 = 0;
        long k2 = 52428800 + ((long) k());
        switch (this.d.d) {
            case 1:
                a2 = d.a(ui_Controller.a.c.c, -1);
                break;
            case 2:
                a2 = d.a(this.b.c.k.b.ab, -1);
                break;
            default:
                a("Error database select mode:" + this.d.d, 0);
                a2 = -1;
                break;
        }
        if (a2 == -1) {
            i2 = -1;
        } else if (a2 < k2) {
            i2 = 1;
        }
        a("dlAvailableSize: " + a2 + ", ApproximateFileSize: " + ((long) k()), 3);
        a("checkMemoryFull: " + i2 + ", database mode: " + this.d.d, 3);
        return i2;
    }

    private void z() {
        this.F = (LinearLayout) findViewById(R.id.LL_ui_videocut_ProgressLayout);
        this.F.setVisibility(4);
        this.G = (ImageView) findViewById(R.id.IV_ui_videocut_ProgressIcon);
        this.H = (LinearLayout) findViewById(R.id.LL_ui_videocut_BlackScreen);
        this.H.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void s() {
        this.H.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void c(boolean z2) {
        if (z2) {
            this.F.setVisibility(0);
            ((AnimationDrawable) this.G.getDrawable()).start();
            this.d.z = true;
            return;
        }
        this.F.setVisibility(4);
        ((AnimationDrawable) this.G.getDrawable()).stop();
        this.d.z = false;
    }

    /* access modifiers changed from: protected */
    public boolean t() {
        if (new File(this.d.b.o(this.d.s)).exists()) {
            return true;
        }
        return false;
    }

    private void A() {
        this.m = new OrientationEventListener(this) {
            /* class ui_Controller.ui_Gallery.ui_PhoneGallery.UI_VideoCutController.AnonymousClass8 */

            public void onOrientationChanged(int i) {
                if (i != -1) {
                    if (i > 350 || i < 10) {
                        if (UI_VideoCutController.this.l != 1) {
                            UI_VideoCutController.this.f(1);
                            UI_VideoCutController.this.l = 1;
                        }
                    } else if (i <= 80 || i >= 100) {
                        if (i <= 170 || i >= 190) {
                            if (i > 260 && i < 280 && UI_VideoCutController.this.l != 0) {
                                UI_VideoCutController.this.f(0);
                                UI_VideoCutController.this.l = 0;
                            }
                        } else if (UI_VideoCutController.this.l != 9) {
                            UI_VideoCutController.this.f(9);
                            UI_VideoCutController.this.l = 9;
                        }
                    } else if (UI_VideoCutController.this.l != 8) {
                        UI_VideoCutController.this.f(8);
                        UI_VideoCutController.this.l = 8;
                    }
                }
            }
        };
    }

    public void f(int i2) {
        d.a(this, i2);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (new File(str).exists()) {
            GeneralFunction.s.a aVar = new GeneralFunction.s.a();
            GeneralFunction.s.b bVar = new GeneralFunction.s.b();
            bVar.f151a = getResources().getString(R.string.internal_app_version);
            bVar.b = this.b.c.g.c;
            int a2 = aVar.a(str, bVar);
            if (a2 != 0) {
                a("addMetaDataInfo fail: " + a2, 1);
                return;
            }
            return;
        }
        a("Original File Not Exist!", 1);
    }

    /* access modifiers changed from: protected */
    public void u() {
        switch (this.d.d) {
            case 1:
                this.d.g = 0;
                return;
            case 2:
                this.d.h = 0;
                return;
            default:
                return;
        }
    }
}
