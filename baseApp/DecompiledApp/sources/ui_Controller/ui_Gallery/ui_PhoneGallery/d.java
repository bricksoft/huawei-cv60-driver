package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.k;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import java.util.ArrayList;

public class d extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1413a = false;
    private Context b;
    private ArrayList<String> c = null;
    private ArrayList<Integer> d = null;
    private ArrayList<Integer> e = null;
    private int f = R.drawable.setting_button_null;
    private int g = 0;
    private int h = -1;

    public d(Context context, int i) {
        this.b = context;
        this.g = i;
        this.c = new ArrayList<>();
        this.d = new ArrayList<>();
        this.e = new ArrayList<>();
        c();
    }

    private void c() {
        switch (this.g) {
            case 0:
                this.c.clear();
                this.c.add(this.b.getString(R.string.move_to_sdcard));
                this.c.add(this.b.getString(R.string.delete));
                this.c.add(this.b.getString(R.string.file_information));
                this.d.clear();
                this.d.add(Integer.valueOf(this.f));
                this.d.add(Integer.valueOf(this.f));
                this.d.add(Integer.valueOf(this.f));
                this.e.clear();
                this.e.add(1);
                this.e.add(2);
                this.e.add(4);
                return;
            case 1:
                this.c.clear();
                this.c.add(this.b.getString(R.string.move_to_phone));
                this.c.add(this.b.getString(R.string.delete));
                this.c.add(this.b.getString(R.string.file_information));
                this.d.clear();
                this.d.add(Integer.valueOf(this.f));
                this.d.add(Integer.valueOf(this.f));
                this.d.add(Integer.valueOf(this.f));
                this.e.clear();
                this.e.add(1);
                this.e.add(2);
                this.e.add(4);
                return;
            case 2:
                this.c.clear();
                this.c.add(this.b.getString(R.string.delete));
                this.c.add(this.b.getString(R.string.file_information));
                this.d.clear();
                this.d.add(Integer.valueOf(this.f));
                this.d.add(Integer.valueOf(this.f));
                this.e.clear();
                this.e.add(2);
                this.e.add(4);
                return;
            case 3:
                this.c.clear();
                this.c.add(this.b.getString(R.string.delete));
                this.c.add(this.b.getString(R.string.file_information));
                this.d.clear();
                this.d.add(Integer.valueOf(this.f));
                this.d.add(Integer.valueOf(this.f));
                this.e.clear();
                this.e.add(2);
                this.e.add(4);
                return;
            case 4:
                this.c.clear();
                this.c.add(this.b.getString(R.string.panorama_hide));
                this.d.clear();
                this.d.add(Integer.valueOf(this.f));
                this.e.clear();
                this.e.add(3);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.g = i;
        c();
    }

    public void a(int i, String str) {
        this.c.set(i, str);
    }

    public void b(int i) {
        this.h = i;
    }

    public void a(boolean z) {
        this.f1413a = z;
    }

    public boolean a() {
        return this.f1413a;
    }

    public void b() {
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.c.size();
    }

    public Object getItem(int i) {
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) this.e.get(i).intValue();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(this.b).inflate(R.layout.ui_singleview_moresetting_menu, (ViewGroup) null);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.IV_ui_singleview_MoreSettingBtn);
        TextView textView = (TextView) inflate.findViewById(R.id.TV_ui_singleview_MoreSettingText);
        ViewGroup.LayoutParams layoutParams = ((LinearLayout) inflate.findViewById(R.id.LL_ui_singleview_Settinglayout)).getLayoutParams();
        DisplayMetrics displayMetrics = this.b.getResources().getDisplayMetrics();
        int i2 = displayMetrics.widthPixels;
        int i3 = displayMetrics.heightPixels;
        layoutParams.height = Math.max(i2, i3) / 10;
        textView.setTextSize((float) k.a(this.b, (float) (Math.max(i2, i3) / 40)));
        textView.setText(this.c.get(i));
        if (this.h != -1) {
            textView.setTextColor(this.h);
        }
        imageView.setVisibility(4);
        inflate.setLayoutParams(layoutParams);
        return inflate;
    }
}
