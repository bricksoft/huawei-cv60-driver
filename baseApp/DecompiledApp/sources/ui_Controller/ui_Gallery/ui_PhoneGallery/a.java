package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.d;
import GeneralFunction.o;
import android.content.Context;
import android.os.Message;
import com.huawei.cvIntl60.R;
import java.io.File;
import tv.danmaku.ijk.media.player.IjkMediaCodecInfo;
import ui_Controller.a.c;
import ui_Controller.b.j;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private UI_GifMakingController f1410a = null;
    private String b = null;
    private long c = 0;

    private void a(String str, int i) {
        d.a("UI_GifMakingHandler", str, i);
    }

    public a(UI_GifMakingController uI_GifMakingController) {
        this.f1410a = uI_GifMakingController;
    }

    public void a(Message message) {
        int i;
        int i2;
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 8448:
                this.f1410a.b.a(3648);
                this.f1410a.l.g();
                this.f1410a.l.f();
                a.c.a aVar2 = new a.c.a(11024);
                aVar2.a("nextActivity", 256);
                this.f1410a.a(aVar2);
                return;
            case 8452:
                this.f1410a.b.a(3648);
                this.f1410a.l.g();
                this.f1410a.l.f();
                this.f1410a.d(0);
                a.c.a aVar3 = new a.c.a(11024);
                aVar3.a("nextActivity", 1024);
                this.f1410a.a(aVar3);
                return;
            case 11010:
                this.f1410a.o();
                return;
            case 11013:
                this.f1410a.d(1);
                return;
            case 11014:
                this.f1410a.d(2);
                return;
            case 11015:
                this.f1410a.d(3);
                return;
            case 11019:
                if (aVar.b("GifGenerateStatus") == 4) {
                    String str = this.b;
                    if (this.f1410a.d.X.f51a == 6) {
                        i = 450;
                        i2 = 450;
                    } else {
                        i = IjkMediaCodecInfo.RANK_LAST_CHANCE;
                        i2 = 300;
                    }
                    File file = new File(str);
                    int c2 = this.f1410a.c.e.c();
                    String substring = str.substring(0, str.lastIndexOf("/") + 1);
                    this.f1410a.c.e.a(str.substring(str.lastIndexOf("/") + 1, str.length()), substring, file.length(), 6, 1, i, i2, 0, this.c, this.c, c2 + 1, 0, true, true, true, true, false, false, false, false, true);
                    this.f1410a.b.x();
                    this.f1410a.c.b.f1320a.a(this.f1410a.c.e.a(), this.f1410a.c.f);
                    GeneralFunction.g.a.a(this.f1410a, str);
                    a(false);
                    this.f1410a.a(8452, 0L);
                    this.f1410a.d.A = true;
                    this.f1410a.d.B = str;
                    return;
                } else if (aVar.b("GifGenerateStatus") == 3) {
                    a("GifGenerateStatus: GET_GIF_GENERATOR_FAIL", 2);
                    if (this.f1410a.b.a() != 3648) {
                        if (this.f1410a.g) {
                            this.f1410a.b.a(3600);
                        } else {
                            this.f1410a.b.a(3601);
                        }
                        this.f1410a.c(1);
                        a(false);
                        if (this.b != null) {
                            File file2 = new File(this.b);
                            if (file2.exists()) {
                                file2.delete();
                            }
                        }
                        if (this.f1410a.h && this.f1410a.d.d == 2) {
                            a("GIF fail by card removed", 2);
                            this.f1410a.j = true;
                            this.f1410a.a(8452, 0L);
                            return;
                        }
                        return;
                    }
                    return;
                } else if (aVar.b("GifGenerateStatus") == 0) {
                    if (this.f1410a.g) {
                        this.f1410a.b.a(3600);
                    } else {
                        this.f1410a.b.a(3601);
                    }
                    this.f1410a.c(1);
                    return;
                } else {
                    a("Generate Start", 3);
                    return;
                }
            case 11021:
                this.f1410a.b(false);
                return;
            case 11022:
                this.f1410a.d(0);
                return;
            case 11023:
                this.f1410a.d(4);
                return;
            case 11024:
                int b2 = aVar.b("nextActivity");
                if (this.f1410a.j()) {
                    switch (b2) {
                        case 256:
                            this.f1410a.t();
                            return;
                        case 1024:
                            this.f1410a.s();
                            return;
                        default:
                            return;
                    }
                } else {
                    this.f1410a.a(aVar, 50);
                    return;
                }
            case 12033:
                a("ulBrowseIndex " + this.f1410a.d.s, 3);
                a("ulBrowseSingleIndex " + this.f1410a.d.t, 3);
                this.f1410a.d.C = 0;
                this.f1410a.d.E = 1;
                this.f1410a.d.F = 0;
                this.f1410a.e();
                this.f1410a.b.c(268435455);
                return;
            case 12034:
                a("ACTIVITY_RESUME", 4);
                j jVar = this.f1410a.b.c;
                if (this.f1410a.b.p() && this.f1410a.b.c.d) {
                    this.f1410a.b.i();
                    this.f1410a.b.c.d = false;
                }
                if (this.f1410a.i() == 2) {
                    this.f1410a.k();
                    return;
                }
                return;
            case 12035:
                a("ACTIVITY_PAUSE", 4);
                j jVar2 = this.f1410a.b.c;
                return;
            case 12036:
                a("ACTIVITY_STOP", 4);
                return;
            case 12037:
                a("ACTIVITY_DESTROY", 4);
                return;
            case 12039:
                this.f1410a.b.c.g.f1324a = 0;
                return;
            case 12042:
                o.a((Context) this.f1410a, true, false, this.f1410a.getResources().getString(R.string.warning), this.f1410a.getResources().getString(R.string.permission_always_deny_msg), this.f1410a.getResources().getString(R.string.dialog_option_ok), 12032);
                return;
            case 12043:
                if (!this.f1410a.d.z) {
                    this.f1410a.l.f();
                    a.c.a aVar4 = new a.c.a(11024);
                    aVar4.a("nextActivity", 256);
                    this.f1410a.a(aVar4);
                    return;
                }
                return;
            case 12046:
            case 12047:
            case 12048:
                return;
            case 12049:
                this.f1410a.c();
                return;
            case 18436:
                if (!this.f1410a.d.z) {
                    this.f1410a.b.c.e = false;
                    if (this.f1410a.b.p()) {
                        this.f1410a.b.i();
                        this.f1410a.b.c.d = true;
                        return;
                    }
                    return;
                }
                return;
            case 18437:
                if (aVar.a("usb_permission")) {
                    a("has permission, need to switch to live view", 1);
                    this.f1410a.b.c.d = false;
                    this.f1410a.b.a(8448, 0);
                    return;
                }
                return;
            default:
                switch (this.f1410a.b.a()) {
                    case 3600:
                        c(message);
                        return;
                    case 3601:
                        b(message);
                        return;
                    case 3616:
                        d(message);
                        return;
                    case 3632:
                        e(message);
                        return;
                    default:
                        return;
                }
        }
    }

    public void b(Message message) {
        a("UI_GifMakingMainVideo handleMessage: 0x" + Integer.toHexString(message.what), 3);
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 11011:
                this.f1410a.b.a(3616);
                int i = (this.f1410a.d.F * 1000) + (this.f1410a.n * 1000);
                int i2 = (this.f1410a.n * 1000) + (this.f1410a.d.G * 1000);
                if (i2 > this.f1410a.l.getDuration()) {
                    i2 = this.f1410a.l.getDuration();
                }
                a("PrePlay lVideoStartTime:" + i + ", lVideoEndTime:" + i2, 3);
                this.f1410a.l.a(i / 1000, i2 / 1000, 1);
                this.f1410a.c(2);
                this.f1410a.n();
                return;
            case 11012:
                long a2 = d.a(c.c, 0);
                a("dlAvailableSize: " + a2, 3);
                if (a2 < 57671680) {
                    this.f1410a.b(true);
                    return;
                }
                a(true);
                this.f1410a.b.a(3632);
                int i3 = (this.f1410a.d.F * 1000) + (this.f1410a.n * 1000);
                int i4 = (this.f1410a.n * 1000) + (this.f1410a.d.G * 1000);
                if (i4 > this.f1410a.l.getDuration()) {
                    i4 = this.f1410a.l.getDuration();
                }
                a("Generator lVideoStartTime:" + i3 + ", lVideoEndTime:" + i4, 3);
                this.c = System.currentTimeMillis();
                this.b = GeneralFunction.g.a.b(c.d + GeneralFunction.g.a.a(Long.valueOf(this.c), "gif"));
                this.f1410a.l.a(this.b, this.f1410a.k);
                this.f1410a.l.a(i3 / 1000, i4 / 1000, 2);
                this.f1410a.n();
                return;
            case 11016:
                this.f1410a.d.E = aVar.b("SelectPointer");
                switch (this.f1410a.d.E) {
                    case 1:
                        this.f1410a.d.F = aVar.b("Index");
                        this.f1410a.e(this.f1410a.d.F * 1000);
                        if (this.f1410a.d.G - this.f1410a.d.F > 4) {
                            this.f1410a.d.G = this.f1410a.d.F + 4;
                            this.f1410a.m.b(this.f1410a.d.G);
                            return;
                        }
                        return;
                    case 2:
                        this.f1410a.d.G = aVar.b("Index");
                        if (this.f1410a.d.G - this.f1410a.d.F > 4) {
                            this.f1410a.d.F = this.f1410a.d.G - 4;
                            this.f1410a.m.a(this.f1410a.d.F);
                            this.f1410a.e(this.f1410a.d.F * 1000);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            case 11017:
            default:
                return;
            case 11020:
                if (aVar.b("360PlayerStatus") == 2) {
                    this.f1410a.q();
                    this.f1410a.l();
                    this.f1410a.a(true);
                } else if (aVar.b("360PlayerStatus") == 1) {
                    if (this.f1410a.l != null) {
                        this.f1410a.c(1);
                        a(false);
                        this.f1410a.f();
                    }
                } else if (aVar.b("360PlayerStatus") == 5) {
                    this.f1410a.l.setPlayerStatusListener(null);
                }
                if (aVar.b("360PlayerStatus") == 0) {
                    this.f1410a.a(false);
                    return;
                }
                return;
            case 32768:
                this.f1410a.a(8452, 0L);
                return;
        }
    }

    public void c(Message message) {
        a("UI_GifMakingMainPhoto handleMessage: 0x" + Integer.toHexString(message.what), 3);
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 11008:
                this.f1410a.p();
                this.f1410a.n();
                return;
            case 11011:
                this.f1410a.b.a(3616);
                this.f1410a.l.a(1);
                this.f1410a.c(2);
                this.f1410a.n();
                return;
            case 11012:
                long a2 = d.a(c.c, 0);
                a("dlAvailableSize: " + a2, 3);
                if (a2 < 57671680) {
                    this.f1410a.b(true);
                    return;
                }
                this.f1410a.b.b(268435455L);
                a(true);
                this.f1410a.b.a(3632);
                this.c = System.currentTimeMillis();
                this.b = GeneralFunction.g.a.b(c.d + GeneralFunction.g.a.a(Long.valueOf(this.c), "gif"));
                this.f1410a.l.a(this.b, this.f1410a.k);
                this.f1410a.l.a(2);
                this.f1410a.n();
                return;
            case 11020:
                if (this.f1410a.i) {
                    this.f1410a.i = false;
                    return;
                }
                if (aVar.b("360PlayerStatus") == 2) {
                    this.f1410a.h();
                    this.f1410a.a(true);
                    this.f1410a.b.c(268435455);
                } else if (aVar.b("360PlayerStatus") == 1) {
                    if (this.f1410a.l != null) {
                        this.f1410a.c(1);
                        a(false);
                        this.f1410a.g();
                    }
                } else if (aVar.b("360PlayerStatus") == 5) {
                    this.f1410a.l.setPlayerStatusListener(null);
                }
                if (aVar.b("360PlayerStatus") == 0) {
                    this.f1410a.a(false);
                    return;
                }
                return;
            case 32768:
                this.f1410a.a(8452, 0L);
                return;
            default:
                return;
        }
    }

    public void d(Message message) {
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 11011:
            case 32768:
                this.f1410a.d();
                this.f1410a.c(1);
                this.f1410a.n();
                return;
            case 11020:
                if (aVar.b("360PlayerStatus") == 0) {
                    this.f1410a.d();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void e(Message message) {
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 11020:
                if (aVar.b("360PlayerStatus") == 0) {
                    this.f1410a.d();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(boolean z) {
        this.f1410a.c(z);
    }
}
