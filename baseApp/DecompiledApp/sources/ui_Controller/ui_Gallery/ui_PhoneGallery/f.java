package ui_Controller.ui_Gallery.ui_PhoneGallery;

import GeneralFunction.d;
import GeneralFunction.e.b;
import GeneralFunction.f.a.c;
import GeneralFunction.o;
import a.c.a;
import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.os.Message;
import com.huawei.cvIntl60.R;
import java.io.File;
import ui_Controller.a.c;
import ui_Controller.b.j;
import ui_Controller.ui_Liveview.UI_LiveViewController;

public class f {

    /* renamed from: a  reason: collision with root package name */
    String f1416a = null;
    String b = null;
    private UI_VideoCutController c = null;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i) {
        d.a("UI_VideoCutHandler", str, i);
    }

    public f(UI_VideoCutController uI_VideoCutController) {
        this.c = uI_VideoCutController;
    }

    public void a(Message message) {
        int i;
        int i2;
        b bVar;
        switch (message.what) {
            case 20500:
                boolean z = this.c.d.d == 2;
                int c2 = this.c.c.e.c();
                String str = this.b;
                File file = new File(str);
                long length = file.length();
                a aVar = new a(message);
                int b2 = aVar.b("lStartTime");
                int b3 = aVar.b("lEndTime");
                long c3 = aVar.c("dlCurrentTime");
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                try {
                    mediaMetadataRetriever.setDataSource(file.getAbsolutePath());
                    i = Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
                    i2 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    i = 0;
                    i2 = 1;
                }
                this.c.c.e.a(str.substring(str.lastIndexOf("/") + 1, str.length()), str.substring(0, str.lastIndexOf("/") + 1), length, 1, 1, i, i2, (long) (b3 - b2), c3, c3, c2 + 1, 0, true, true, true, true, false, true, true, z, false);
                this.c.b.x();
                GeneralFunction.g.a.a(this.c, str);
                this.c.c.e.a(this.c.c.c, this.c.c.f, this.c.c.g, this.c.c.h);
                if (z) {
                    bVar = this.c.c.g;
                } else {
                    bVar = this.c.c.f;
                }
                this.c.c.b.f1320a.a(this.c.c.e.a(), bVar);
                this.c.c.b.b.a(this.c.c.e.a(), bVar);
                this.c.d.s = this.c.d.f1320a.u(c2 + 1);
                this.c.u();
                GeneralFunction.g.a.d(this.b, GeneralFunction.g.a.d(this.b));
                this.c.a(11079);
                return;
            default:
                return;
        }
    }

    public void b(Message message) {
        new a(message);
        switch (message.what) {
            case 8448:
                this.c.b.a(256, this.c, new Intent(this.c, UI_LiveViewController.class));
                this.c.b.a(0, (GeneralFunction.a.a) null);
                this.c.b.b(268435455L);
                this.c.d.d = 1;
                return;
            case 8452:
                Intent intent = new Intent(this.c, UI_PhoneGalleryController.class);
                if (this.c.e) {
                    intent.putExtra("mode", 1024);
                    this.c.e = false;
                    this.c.d.q = new GeneralFunction.a((Context) this.c.b, true, this.c.getResources().getString(R.string.sdcard_removed), 2000);
                } else {
                    intent.putExtra("mode", 1056);
                }
                this.c.b.a(1024, this.c, intent);
                this.c.b.a(0, (GeneralFunction.a.a) null);
                this.c.b.b(268435455L);
                return;
            case 11080:
                if (this.c.p()) {
                    this.c.q();
                    return;
                } else {
                    this.c.a(11080, 50);
                    return;
                }
            case 12033:
                a("ulBrowseIndex " + this.c.d.s, 3);
                a("ulBrowseSingleIndex " + this.c.d.t, 3);
                this.c.d.C = 0;
                this.c.d.E = 1;
                this.c.d.F = 0;
                this.c.d();
                this.c.b.c(268435455);
                return;
            case 12043:
                if (!this.c.d.z) {
                    this.c.d.C = 3;
                    this.c.o();
                    this.c.a(11080);
                    return;
                }
                return;
            case 12046:
            case 12047:
            case 12048:
                return;
            case 12049:
                this.c.c();
                return;
            default:
                switch (this.c.b.a()) {
                    case 3344:
                        c(message);
                        return;
                    case 3360:
                        d(message);
                        return;
                    default:
                        return;
                }
        }
    }

    public void c(Message message) {
        a("UI_MakingVideoMain handleMessage: 0x" + Integer.toHexString(message.what), 3);
        a aVar = new a(message);
        switch (message.what) {
            case 9097:
                if (aVar.b("360PlayerStatus") == 2) {
                    this.c.n();
                    this.c.e(this.c.d.F * 1000);
                    this.c.h();
                    return;
                } else if (aVar.b("360PlayerStatus") != 1) {
                    if (aVar.b("360PlayerStatus") == 5) {
                    }
                    return;
                } else if (this.c.d.C != 4 && this.c.d.C != 3 && this.c.h != null) {
                    this.c.e();
                    return;
                } else {
                    return;
                }
            case 11072:
            case 11073:
                this.c.d.E = aVar.b("SelectPointer");
                switch (this.c.d.E) {
                    case 1:
                        this.c.d.F = aVar.b("Index");
                        this.c.e(this.c.d.F * 1000);
                        break;
                    case 2:
                        this.c.d.G = aVar.b("Index");
                        break;
                }
                this.c.l();
                return;
            case 11074:
                this.c.b.a(3360);
                this.c.d.C = 2;
                this.c.d(2);
                this.c.a(11076);
                this.c.m();
                return;
            case 11075:
                if (this.c.r() == -1) {
                    return;
                }
                if (this.c.r() == 1) {
                    this.c.b(true);
                    this.c.b.c(268435455);
                    return;
                }
                a(true);
                final int i = this.c.d.F;
                final int i2 = this.c.d.G;
                this.f1416a = this.c.d.b.o(this.c.d.s);
                final Long valueOf = Long.valueOf(System.currentTimeMillis());
                String a2 = GeneralFunction.g.a.a(valueOf, "mp4");
                if (this.c.d.d == 2) {
                    this.b = GeneralFunction.g.a.b(this.c.d.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/" + a2);
                    new File(this.c.d.ab + "/Android/data/com.huawei.cvIntl60/" + "DCIM/CV60/").mkdirs();
                    File file = new File(this.b);
                    if (file.exists()) {
                        file.delete();
                    }
                } else {
                    this.b = GeneralFunction.g.a.b(c.c + a2);
                    File file2 = new File(this.b);
                    if (file2.exists()) {
                        file2.delete();
                    }
                }
                a("Cut video Start", 3);
                if (this.c.g == null) {
                    this.c.g = new GeneralFunction.f.a.c();
                    this.c.f = false;
                }
                this.c.g.a(this.f1416a, this.b, i, i2, new c.a() {
                    /* class ui_Controller.ui_Gallery.ui_PhoneGallery.f.AnonymousClass1 */

                    @Override // GeneralFunction.f.a.c.a
                    public void a(int i) {
                        f.this.a("Cut video End", 3);
                        f.this.c.g = null;
                        if (!f.this.c.e) {
                            a aVar = new a(11078);
                            aVar.a("lStartTime", i);
                            aVar.a("lEndTime", i2);
                            aVar.a("dlCurrentTime", valueOf.longValue());
                            f.this.c.a(aVar);
                        }
                    }
                });
                return;
            case 11077:
                this.c.b(false);
                return;
            case 11078:
                if (this.c.f) {
                    this.c.f = false;
                    File file3 = new File(this.b);
                    if (file3.exists()) {
                        file3.delete();
                    }
                    this.b = null;
                    return;
                }
                a("addMetaDataInfo Start: " + System.currentTimeMillis(), 3);
                this.c.a(this.b);
                a("addMetaDataInfo End: " + System.currentTimeMillis(), 3);
                a aVar2 = new a(message);
                int b2 = aVar2.b("lStartTime");
                int b3 = aVar2.b("lEndTime");
                long c2 = aVar2.c("dlCurrentTime");
                a aVar3 = new a(20500);
                aVar3.a("lStartTime", b2);
                aVar3.a("lEndTime", b3);
                aVar3.a("dlCurrentTime", c2);
                this.c.b(aVar3);
                return;
            case 11079:
                this.c.d.C = 4;
                this.c.o();
                a(false);
                this.c.a(11080);
                return;
            case 12034:
                a("ACTIVITY_RESUME", 4);
                j jVar = this.c.b.c;
                if (this.c.b.p() && this.c.b.c.d) {
                    this.c.b.i();
                    this.c.b.c.d = false;
                }
                if (this.c.f() == 2) {
                    this.c.g();
                    return;
                }
                return;
            case 12035:
                a("ACTIVITY_PAUSE", 4);
                if (this.c.g != null) {
                    this.c.f = true;
                    a(false);
                    this.c.g.a();
                    this.c.g = null;
                    this.c.b.c(268435455);
                }
                j jVar2 = this.c.b.c;
                return;
            case 12036:
                a("ACTIVITY_STOP", 4);
                return;
            case 12037:
                a("ACTIVITY_DESTROY", 4);
                return;
            case 12039:
                this.c.b.c.g.f1324a = 0;
                return;
            case 12042:
                o.a((Context) this.c, true, false, this.c.getResources().getString(R.string.warning), this.c.getResources().getString(R.string.permission_always_deny_msg), this.c.getResources().getString(R.string.dialog_option_ok), 12032);
                return;
            case 18436:
                if (!this.c.d.z) {
                    this.c.b.c.e = false;
                    if (this.c.b.p()) {
                        this.c.b.i();
                        this.c.b.c.d = true;
                        return;
                    }
                    return;
                }
                return;
            case 18437:
                if (aVar.a("usb_permission")) {
                    a("has permission, need to switch to live view", 1);
                    this.c.b.c.d = false;
                    this.c.d.C = 3;
                    this.c.o();
                    this.c.a(11080);
                    return;
                }
                return;
            case 32768:
                this.c.d.C = 4;
                this.c.o();
                this.c.a(11080);
                return;
            default:
                return;
        }
    }

    public void d(Message message) {
        a aVar = new a(message);
        switch (message.what) {
            case 9097:
                if (aVar.b("360PlayerStatus") == 0) {
                    this.c.a(11074, 0);
                    return;
                } else if (aVar.b("360PlayerStatus") == 3) {
                    this.c.a(11074, 0);
                    return;
                } else {
                    return;
                }
            case 11074:
            case 32768:
                this.c.b.a(3344);
                this.c.d.C = 1;
                this.c.n();
                this.c.e(this.c.d.F * 1000);
                this.c.d(1);
                return;
            case 11076:
                if (this.c.h.getCurrentPosition() > this.c.d.G * 1000) {
                    this.c.a(32768);
                    return;
                } else {
                    this.c.a(11076, 500);
                    return;
                }
            default:
                return;
        }
    }

    private void a(boolean z) {
        this.c.c(z);
    }
}
