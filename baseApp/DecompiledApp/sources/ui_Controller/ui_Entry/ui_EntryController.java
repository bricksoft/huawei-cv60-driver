package ui_Controller.ui_Entry;

import GeneralFunction.a.a;
import GeneralFunction.d;
import android.content.Intent;
import android.os.Bundle;
import ui_Controller.ui_LaunchScreen.UI_LaunchScreenController;
import ui_Controller.ui_Liveview.UI_LiveViewController;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class ui_EntryController extends a {
    private UI_ModeMain b;

    private void a(String str, int i) {
        d.a("ui_EntryController", str, i);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = (UI_ModeMain) getApplication();
        this.b.v();
        a("onCreate:" + this.b.n(), 2);
        if (GeneralFunction.n.a.a(this.b).getBoolean("haveUserInstruction", false)) {
            this.b.j();
            if (!this.b.n()) {
                this.b.q();
                this.b.a(4096, this, new Intent(this, UI_LaunchScreenController.class));
                this.b.a(0, (a) null);
                return;
            }
            a.c.a aVar = new a.c.a(12043);
            aVar.a(UI_LiveViewController.class);
            aVar.a("AppMode", 256);
            this.b.a(aVar);
            finish();
        } else if (!this.b.n()) {
            this.b.q();
            a("Go to launch screen", 2);
            this.b.a(4096, this, new Intent(this, UI_LaunchScreenController.class));
            this.b.a(0, (a) null);
        } else {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        a("onNewIntent should not happen!!", 0);
    }
}
