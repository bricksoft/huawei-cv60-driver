package ui_Controller.ui_LaunchScreen;

import GeneralFunction.Player.player.SphericalVideoPlayer;
import GeneralFunction.a.a;
import GeneralFunction.h;
import GeneralFunction.k;
import GeneralFunction.l;
import GeneralFunction.o;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.huawei.cvIntl60.R;
import java.util.ArrayList;
import java.util.Calendar;
import ui_Controller.a.c;
import ui_Controller.b.d;
import ui_Controller.ui_Setting.UI_SettingUserInstructions;
import ui_Controller.ui_StartMode.UI_ModeMain;

public class UI_LaunchScreenController extends a {
    protected UI_ModeMain b = null;
    protected ImageView c = null;
    protected LinearLayout d = null;
    protected SphericalVideoPlayer e = null;
    protected Bitmap f = null;
    protected ImageView g;
    private int h = 0;
    private int i = 0;
    private int j = -1;
    private a k = null;
    private OrientationEventListener l = null;
    private Context m = this;
    private LinearLayout n = null;
    private WebView o = null;
    private Button p = null;
    private Button q = null;
    private ArrayList<String> r = null;
    private int s = 0;
    private LinearLayout t = null;
    private TextView u = null;
    private TextView v = null;
    private Button w = null;
    private d x;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void a(String str, int i2) {
        GeneralFunction.d.a("UI_LaunchScreenController", str, i2);
    }

    @Override // GeneralFunction.a.a
    public void a(int i2, long j2) {
        if (this.b != null) {
            this.b.a(new a.c.a(i2), j2);
        } else {
            a("SendUiMsg error!! Msg=" + i2, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(a.c.a aVar, long j2) {
        if (this.b != null) {
            this.b.a(aVar, j2);
        } else {
            a("SendUiMsg error!! MsgEx=" + aVar.b().what, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(int i2, long j2) {
        if (this.b != null) {
            this.b.b(i2, j2);
        } else {
            a("UI_SendAidMsg error!!", 0);
        }
    }

    @Override // GeneralFunction.a.a
    public boolean b() {
        if (this.j == 2) {
            return true;
        }
        return false;
    }

    public void onBackPressed() {
        if (this.b.a(1L)) {
            a(32768, 0);
        }
    }

    @Override // GeneralFunction.a.a
    public void b(Message message) {
        a("AidServer_Handler:0x" + Integer.toHexString(message.what), 3);
        this.k.d(message);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = (UI_ModeMain) getApplication();
        this.b.v();
        this.x = d.a();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.h = displayMetrics.widthPixels;
        this.i = displayMetrics.heightPixels;
        if (this.b.n() && (this.b.w() || this.b.c() != null)) {
            a("Skip launch onCreate in special case:" + this.b.c().a(), 2);
            if (this.b.c().a() != 3) {
                finish();
                return;
            }
        }
        this.b.o();
        this.j = 2;
        setContentView(R.layout.ui_launchscreen_land);
        this.k = new a(this);
        this.b.a(3088, this);
        u();
        a(12033, 0);
        String str = "";
        try {
            str = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        a("APP ver. info: " + ("v" + str + " (" + getResources().getString(R.string.internal_app_version) + ")"), 3);
        String str2 = Build.MANUFACTURER;
        String str3 = Build.MODEL;
        String str4 = Build.PRODUCT;
        a("Device info: " + str2 + ", " + str3 + ", " + str4 + ", " + Build.VERSION.RELEASE, 3);
        r();
    }

    private void r() {
        String a2 = l.a("ro.config.hw_notch_size", "");
        if (a2.isEmpty()) {
            this.x.a(false);
            return;
        }
        a("checkNeedGap:" + a2, 1);
        this.x.a(true);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onResume() {
        super.onResume();
        if (this.b.a() == 3104) {
            this.e.start();
        }
        a.c.a aVar = new a.c.a(12034);
        aVar.a("mode", 3072);
        a(aVar, 0);
        this.l.enable();
    }

    @Override // GeneralFunction.a.a
    public void onPause() {
        super.onPause();
        if (this.b.a() == 3104) {
            this.e.pause();
        }
        a.c.a aVar = new a.c.a(12035);
        aVar.a("mode", 3072);
        a(aVar, 0);
        this.l.disable();
    }

    @Override // GeneralFunction.a.a
    public void onStop() {
        super.onStop();
        a.c.a aVar = new a.c.a(12036);
        aVar.a("mode", 3072);
        a(aVar, 0);
    }

    /* access modifiers changed from: protected */
    @Override // GeneralFunction.a.a
    public void onDestroy() {
        super.onDestroy();
        a.c.a aVar = new a.c.a(12037);
        aVar.a("mode", 3072);
        a(aVar, 0);
        if (this.c != null) {
            this.c.setImageResource(0);
        }
    }

    @Override // GeneralFunction.a.a
    public void a(Message message) {
        this.k.a(message);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.c = (ImageView) findViewById(R.id.IV_ui_launchImage);
        this.c.setImageResource(R.drawable.splash_screen_huawei);
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.g = (ImageView) findViewById(R.id.IV_VideoTransFrame);
        this.g.setImageResource(R.drawable.splash_screen_huawei);
        this.t = (LinearLayout) findViewById(R.id.LL_VideoCover);
        this.u = (TextView) findViewById(R.id.TV_VideoCover_Title);
        this.v = (TextView) findViewById(R.id.TV_VideoCover_Dialog);
        this.w = (Button) findViewById(R.id.BT_VideoCover);
        this.t.setOnTouchListener(new View.OnTouchListener() {
            /* class ui_Controller.ui_LaunchScreen.UI_LaunchScreenController.AnonymousClass1 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.t.setVisibility(4);
        this.u.setTextSize((float) k.a(this, (float) (this.i / 30)));
        this.u.setText("");
        this.v.setText(getResources().getString(R.string.videocover_dialog1));
        this.w.setBackgroundResource(R.drawable.system_go);
        this.w.setText(getResources().getString(R.string.lunch_video_button_go));
        this.w.setTextColor(getResources().getColor(R.color.white));
        this.w.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_LaunchScreen.UI_LaunchScreenController.AnonymousClass2 */

            public void onClick(View view) {
                UI_LaunchScreenController.this.a(false);
                UI_LaunchScreenController.this.g();
                UI_LaunchScreenController.this.a(10503);
            }
        });
    }

    private void s() {
        switch (this.s) {
            case 0:
                this.v.setText(getResources().getString(R.string.videocover_dialog1));
                return;
            case 1:
                this.v.setText(getResources().getString(R.string.videocover_dialog2));
                return;
            case 2:
                this.v.setText(getResources().getString(R.string.videocover_dialog3));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (z) {
            this.t.setVisibility(0);
        } else {
            this.t.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.r = new ArrayList<>();
        this.r.add(c.m);
        this.r.add(c.n);
        this.r.add(c.o);
        this.d = (LinearLayout) findViewById(R.id.LL_ui_VideoPlayer);
        this.e = (SphericalVideoPlayer) findViewById(R.id.TV_ui_VideoPlayer);
        this.d.setVisibility(4);
        this.e.setPlayerStatusListener(new SphericalVideoPlayer.f() {
            /* class ui_Controller.ui_LaunchScreen.UI_LaunchScreenController.AnonymousClass3 */

            @Override // GeneralFunction.Player.player.SphericalVideoPlayer.f
            public void a(int i) {
                UI_LaunchScreenController.this.a((UI_LaunchScreenController) ("ReturnVideoStatus " + i), (String) 3);
                a.c.a aVar = new a.c.a(10502);
                aVar.a("360PlayerStatus", i);
                UI_LaunchScreenController.this.a(aVar, 0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void f() {
        this.d.setVisibility(0);
        this.e.f();
        this.e.a(false);
        this.e.setEnable(true);
        this.e.setVideoAutoPlay(true);
        this.e.b(false);
        this.e.setPlayerMode(1);
        if (this.s == 2) {
            this.e.setViewType(5);
        } else {
            this.e.setViewType(3);
        }
        this.e.setVideoFilePath(this.r.get(this.s));
        this.e.setInteractiveMode(2);
        this.e.g();
    }

    /* access modifiers changed from: protected */
    public void g() {
        this.d.setVisibility(4);
        this.e.k();
        this.e.i();
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        if (this.e.d() == 0 || this.e.d() == 5 || this.e.d() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void i() {
        this.e.pause();
        this.e.k();
        this.e.i();
        if (this.s == 2) {
            this.s = 0;
        } else {
            this.s++;
        }
        s();
    }

    /* access modifiers changed from: protected */
    public void b(boolean z) {
        if (z) {
            this.f = this.e.getBitmap();
            this.g.setImageBitmap(this.f);
            this.g.setBackgroundColor(getResources().getColor(R.color.black));
            return;
        }
        this.g.setImageResource(0);
        this.g.setBackgroundColor(getResources().getColor(R.color.transparent));
        if (this.f != null) {
            this.f.recycle();
            this.f = null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean j() {
        Calendar instance = Calendar.getInstance();
        Long valueOf = Long.valueOf(GeneralFunction.n.a.a(this.b).getLong("previousTimeShowLaunchScreenVideo", -1));
        long j2 = (long) instance.get(6);
        if (valueOf.longValue() == -1) {
        }
        if (j2 - valueOf.longValue() > 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void k() {
        for (String str : c.r) {
            if (ActivityCompat.checkSelfPermission(this, str) == -1) {
                ActivityCompat.requestPermissions(this, c.r, 300);
                return;
            }
        }
        t();
    }

    /* access modifiers changed from: protected */
    public void l() {
        if (GeneralFunction.n.a.a(this.b).getInt("storeLocation", 0) == 1 && GeneralFunction.d.a(this) == null) {
            GeneralFunction.n.a.b(this.b).putInt("storeLocation", 0).apply();
            GeneralFunction.m.a.a(0);
        }
    }

    /* access modifiers changed from: protected */
    public void m() {
        this.b.o();
        this.b.a(4128, this, new Intent(this, UI_SettingUserInstructions.class));
        this.b.a(0, (a) null);
        SharedPreferences.Editor b2 = GeneralFunction.n.a.b(this.b);
        b2.putBoolean("haveUserInstruction", true).apply();
        b2.putBoolean("apkUpdateLater", false).apply();
        b2.putString("presentVersion", getResources().getString(R.string.internal_app_version)).apply();
        finish();
    }

    /* access modifiers changed from: protected */
    public void n() {
        if (!GeneralFunction.n.a.a(this.b).getString("presentVersion", "0").equals(getResources().getString(R.string.internal_app_version))) {
            SharedPreferences.Editor b2 = GeneralFunction.n.a.b(this.b);
            b2.putString("presentVersion", getResources().getString(R.string.internal_app_version)).apply();
            b2.putBoolean("apkUpdateLater", false).apply();
            b2.putBoolean("haveNewApkVersion", false).apply();
        }
    }

    /* access modifiers changed from: protected */
    public void o() {
        if (!GeneralFunction.n.a.a(this.b).getBoolean("haveUserInstruction", false)) {
            GeneralFunction.n.a.b(this.b).putLong("apkInspectTime", 0).apply();
            m();
        } else if (this.b.d.b()) {
            a(8448);
        } else {
            a(8452);
        }
    }

    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        a("onRequestPermissionsResult() " + i2, 4);
        if (strArr.length != 0) {
            for (int i3 = 0; i3 < strArr.length; i3++) {
                if (iArr[i3] != 0) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, strArr[i3])) {
                        o.a((Context) this, true, false, getResources().getString(R.string.warning), getResources().getString(R.string.permission_deny_msg), new String[]{getResources().getString(R.string.dialog_option_cancel), getResources().getString(R.string.dialog_option_ok)}, new int[]{12032, 10496});
                        return;
                    } else {
                        o.a((Context) this, true, false, getResources().getString(R.string.warning), getResources().getString(R.string.permission_always_deny_msg), getResources().getString(R.string.dialog_option_ok), 12032);
                        return;
                    }
                }
            }
        }
        t();
    }

    private void t() {
        if (!GeneralFunction.n.a.a(this.b).getBoolean("haveUnzipSample", false)) {
            b(20482);
        }
        a("Send MSG_AID_SPLASH_USB_INIT", 1);
        b(20506);
    }

    /* access modifiers changed from: protected */
    public void p() {
        this.n = (LinearLayout) findViewById(R.id.LL_ui_User_Agreement);
        this.o = (WebView) findViewById(R.id.WV_ui_User_Agreement_Content);
        this.p = (Button) findViewById(R.id.B_ui_User_Agreement_Disallow);
        this.p.setBackgroundResource(R.drawable.btn_bg);
        this.q = (Button) findViewById(R.id.B_ui_User_Agreement_Allow);
        this.q.setBackgroundResource(R.drawable.btn_bg);
        WebSettings settings = this.o.getSettings();
        settings.setAllowFileAccess(false);
        settings.setJavaScriptEnabled(false);
        settings.setAllowFileAccessFromFileURLs(false);
        settings.setAllowUniversalAccessFromFileURLs(false);
        settings.setGeolocationEnabled(false);
        settings.setAllowContentAccess(false);
        this.o.loadUrl(getResources().getString(R.string.user_agreement_url));
        this.p.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_LaunchScreen.UI_LaunchScreenController.AnonymousClass4 */

            public void onClick(View view) {
                UI_LaunchScreenController.this.c(false);
                UI_LaunchScreenController.this.a(12032);
            }
        });
        this.q.setOnClickListener(new View.OnClickListener() {
            /* class ui_Controller.ui_LaunchScreen.UI_LaunchScreenController.AnonymousClass5 */

            public void onClick(View view) {
                UI_LaunchScreenController.this.c(false);
                UI_LaunchScreenController.this.a(10501);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void c(boolean z) {
        if (z) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.n, "translationX", 1600.0f, 0.0f);
            ofFloat.setDuration(300L);
            ofFloat.start();
            this.n.setVisibility(0);
            return;
        }
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.n, "translationX", 0.0f, 1600.0f);
        ofFloat2.setDuration(300L);
        ofFloat2.start();
        ofFloat2.addListener(new AnimatorListenerAdapter() {
            /* class ui_Controller.ui_LaunchScreen.UI_LaunchScreenController.AnonymousClass6 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                UI_LaunchScreenController.this.n.setVisibility(4);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void q() {
        new h.a(this).a(R.string.button_agree, new DialogInterface.OnClickListener() {
            /* class ui_Controller.ui_LaunchScreen.UI_LaunchScreenController.AnonymousClass8 */

            public void onClick(DialogInterface dialogInterface, int i) {
                UI_LaunchScreenController.this.a(10512);
            }
        }).b(R.string.button_disagree, new DialogInterface.OnClickListener() {
            /* class ui_Controller.ui_LaunchScreen.UI_LaunchScreenController.AnonymousClass7 */

            public void onClick(DialogInterface dialogInterface, int i) {
                UI_LaunchScreenController.this.a(12032);
            }
        }).a().show(getFragmentManager(), "dialog");
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        o.a();
    }

    private void u() {
        this.l = new OrientationEventListener(this.m) {
            /* class ui_Controller.ui_LaunchScreen.UI_LaunchScreenController.AnonymousClass9 */

            public void onOrientationChanged(int i) {
                if (i != -1) {
                    if (i > 350 || i < 10) {
                        if (UI_LaunchScreenController.this.j != 1) {
                            UI_LaunchScreenController.this.setRequestedOrientation(1);
                            UI_LaunchScreenController.this.j = 1;
                        }
                    } else if (i > 170 && i < 190 && UI_LaunchScreenController.this.j != 9) {
                        UI_LaunchScreenController.this.setRequestedOrientation(9);
                        UI_LaunchScreenController.this.j = 9;
                    }
                }
            }
        };
    }
}
