package ui_Controller.ui_LaunchScreen;

import GeneralFunction.d;
import GeneralFunction.g.b;
import GeneralFunction.o;
import android.content.Intent;
import android.os.Message;
import android.os.Process;
import com.huawei.cvIntl60.R;
import java.io.File;
import java.util.Calendar;
import ui_Controller.a.c;
import ui_Controller.ui_Gallery.ui_PhoneGallery.UI_PhoneGalleryController;
import ui_Controller.ui_Liveview.UI_LiveViewController;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private UI_LaunchScreenController f1427a = null;

    private void a(String str, int i) {
        d.a("UI_LaunchScreenHandler", str, i);
    }

    public a(UI_LaunchScreenController uI_LaunchScreenController) {
        this.f1427a = uI_LaunchScreenController;
    }

    public void a(Message message) {
        int i = message.what;
        switch (this.f1427a.b.a()) {
            case 3088:
                b(message);
                return;
            case 3104:
                c(message);
                return;
            default:
                return;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void b(Message message) {
        new a.c.a(message);
        switch (message.what) {
            case 8448:
                if (!this.f1427a.h()) {
                    this.f1427a.a(8448, 50);
                }
                this.f1427a.b.o();
                this.f1427a.b.a(256, this.f1427a, new Intent(this.f1427a, UI_LiveViewController.class));
                return;
            case 8452:
                if (!this.f1427a.h()) {
                    this.f1427a.a(8452, 50);
                }
                this.f1427a.b.o();
                this.f1427a.b.a(1024, this.f1427a, new Intent(this.f1427a, UI_PhoneGalleryController.class));
                return;
            case 10496:
                this.f1427a.k();
                return;
            case 10497:
                a("MSG_UI_LAUNCH_SCREEN_USB_INIT_DONE. Start checkNeedShowInstruction", 1);
                this.f1427a.b.t();
                break;
            case 10498:
                break;
            case 10499:
                this.f1427a.l();
                return;
            case 10500:
                o.b(this.f1427a);
                this.f1427a.c(true);
                return;
            case 10501:
                GeneralFunction.n.a.b(this.f1427a.b);
                if (this.f1427a.j()) {
                    this.f1427a.b.a(3104);
                    this.f1427a.f();
                    return;
                }
                this.f1427a.o();
                this.f1427a.n();
                return;
            case 10505:
                if (!GeneralFunction.n.a.a(this.f1427a.b).getBoolean("bPermissionAgreementChecked", false)) {
                    this.f1427a.q();
                    return;
                } else {
                    this.f1427a.a(10496);
                    return;
                }
            case 10512:
                GeneralFunction.n.a.b(this.f1427a.b).putBoolean("bPermissionAgreementChecked", true).apply();
                this.f1427a.a(10496);
                return;
            case 12032:
                Process.killProcess(Process.myPid());
                this.f1427a.finish();
                return;
            case 12033:
                this.f1427a.c();
                this.f1427a.e();
                this.f1427a.d();
                this.f1427a.p();
                this.f1427a.a(10505);
                this.f1427a.a(10499);
                return;
            case 12034:
            case 12035:
            case 12036:
            case 12037:
            case 12038:
            default:
                return;
        }
        if (this.f1427a.b.u()) {
            GeneralFunction.n.a.a(this.f1427a.b);
            if (this.f1427a.j()) {
                this.f1427a.b.a(3104);
                this.f1427a.f();
                return;
            }
            this.f1427a.o();
            this.f1427a.n();
            return;
        }
        this.f1427a.a(10498);
    }

    public void c(Message message) {
        a.c.a aVar = new a.c.a(message);
        switch (message.what) {
            case 10502:
                int b = aVar.b("360PlayerStatus");
                a("MSG_UI_LAUNCH_SCREEN_360_PLAYER_STATUS_UPDATE:" + b, 2);
                if (this.f1427a.d.getVisibility() != 4) {
                    switch (b) {
                        case 1:
                            if (this.f1427a.e != null) {
                                this.f1427a.f();
                                return;
                            }
                            return;
                        case 2:
                            this.f1427a.a(true);
                            this.f1427a.b(false);
                            return;
                        case 3:
                            this.f1427a.i();
                            this.f1427a.b(true);
                            return;
                        default:
                            return;
                    }
                } else {
                    return;
                }
            case 10503:
                GeneralFunction.n.a.b(this.f1427a.b).putLong("previousTimeShowLaunchScreenVideo", (long) Calendar.getInstance().get(6)).apply();
                this.f1427a.b.a(3088);
                this.f1427a.o();
                this.f1427a.n();
                return;
            default:
                return;
        }
    }

    public void d(Message message) {
        switch (message.what) {
            case 20482:
                new b().a(this.f1427a.getResources().openRawResource(R.raw.samplegallery), c.q);
                GeneralFunction.n.a.b(this.f1427a.b).putBoolean("haveUnzipSample", true).apply();
                GeneralFunction.g.a.a(this.f1427a, new File(c.m).getAbsolutePath());
                GeneralFunction.g.a.a(this.f1427a, new File(c.n).getAbsolutePath());
                GeneralFunction.g.a.a(this.f1427a, new File(c.o).getAbsolutePath());
                GeneralFunction.g.a.a(this.f1427a, new File(c.c + "CV60_Photo_1.jpg").getAbsolutePath());
                GeneralFunction.g.a.a(this.f1427a, new File(c.c + "CV60_Photo_2.jpg").getAbsolutePath());
                GeneralFunction.g.a.a(this.f1427a, new File(c.c + "CV60_Photo_3.jpg").getAbsolutePath());
                GeneralFunction.g.a.a(this.f1427a, new File(c.c + "CV60_Video.mp4").getAbsolutePath());
                GeneralFunction.g.a.a(this.f1427a, new File(c.d + "CV60_Edit_1.jpg").getAbsolutePath());
                GeneralFunction.g.a.a(this.f1427a, new File(c.d + "CV60_Edit_2.jpg").getAbsolutePath());
                GeneralFunction.g.a.a(this.f1427a, new File(c.d + "CV60_Edit_3.jpg").getAbsolutePath());
                return;
            case 20506:
                this.f1427a.b.j();
                while (!this.f1427a.b.k()) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                this.f1427a.a(10497);
                return;
            default:
                return;
        }
    }
}
