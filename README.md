# Huawei CV60 driver

## APP
[Huawei 360 Camera](https://play.google.com/store/apps/details?id=com.huawei.cvIntl60&hl=fr&gl=US)

## Status of reverse engineering
__baseApp/DecompiledApp/sources/__
<pre>
📦a
 ┣ 📦a
 ┃ ┗ 📦a
 ┃ ┃ ┣ 📜a.java
 ┃ ┃ ┣ 📜b.java
 ┃ ┃ ┣ 📜c.java
 ┃ ┃ ┣ 📜d.java
 ┃ ┃ ┗ 📜e.java
 ┣ 📦b
 ┃ ┣ 📦a
 ┃ ┃ ┣ 📜a.java
 ┃ ┃ ┣ 📜b.java --> __WIP__
 ┃ ┃ ┣ 📜c.java
 ┃ ┃ ┗ 📜d.java
 ┃ ┗ 📦b
 ┃ ┃ ┗ 📜a.java
 ┣ 📦c
 ┃ ┣ 📜a.java
 ┃ ┣ 📜b.java
 ┃ ┣ 📜c.java
 ┃ ┗ 📜d.java
 ┗ 📜a.java
 </pre>

## USB infos
Device Info 

Device Path: /dev/bus/usb/001/004

Device Class: Use class information in the Interface Descriptors (0x0)

Vendor ID:  12d1

Vendor Name (reported):  ABILITY

Vendor Name (from DB):  

Product ID:  109b

Product Name (reported):  HA CAM

Product Name (from DB):  

Additional Info 

Interface #0 

Class: Vendor Specific (0xff)

Endpoint: #0

Address        : 0x82 (10000010)

Number         : 2

Direction      : Inbound (0x80)

Type           : Bulk (0x2)

Poll Interval  : 0

Max Packet Size: 512

Attributes     : 00000010

Endpoint: #1

Address        : 0x03 (00000011)

Number         : 3

Direction      : Outbound (0x0)

Type           : Bulk (0x2)

Poll Interval  : 0

Max Packet Size: 512

Attributes     : 00000010
